<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
function dd($var){
    die(var_dump($var));
}
require 'bootstrap.php';
date_default_timezone_set('America/Sao_Paulo');
session_start();

foreach (glob(__DIR__ . '/routes/system/*.php') as $filename) {
    require $filename;
}
foreach (glob(__DIR__ . '/routes/api/*.php') as $filename) {
    require $filename;
}
foreach (glob(__DIR__ . '/routes/espaco-novo/*.php') as $filename) {
    require $filename;
}



//liberar cors
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->run();

