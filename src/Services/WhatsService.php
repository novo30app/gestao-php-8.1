<?php

namespace App\Services; 

class WhatsService
{
    public static function messageIndicationOfAffiliation($firstName, $phone, $link)
    {
		$url = "https://api.painel.zapfacil.com/api/webhooks/LDEbMcAGScLSTaTjVVqgrYThD83x9kol";
        $params = array(
                        "name" => $firstName,
                        "phone" => '+55' . $phone,
                        "link" => $link
                    );
        self::exeCurl($url, $params);		
    }

    private static function exeCurl($url, $params)
    {
        $data_json = json_encode($params);
        $headers = array('Content-Type: application/json');
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        echo $response;
    }
}