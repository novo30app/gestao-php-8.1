<?php

namespace App\Services;
use App\Models\Entities\StoreSalesDetails;

class Loja
{    
    private static $token = 'CA67FC9B18A370321B0C5AAFB2F0DD1B85A83027639F9040071905A7C5253DD4';
    public static function checkInvoices(): array
    {
        $headers = [];
        $headers[] = 'Accept: application/json';
        $params['api_token'] = self::$token;
        $params['year'] = date('Y');
        $params['month'] = date('m');
        $params['status'] = 'pending';
        $params = http_build_query($params);
        $ch = curl_init("https://api.iugu.com/v1/accounts/invoices?{$params}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function checkInvoicesDetails($id): array
    {
        $headers = [];
        $headers[] = 'Accept: application/json';
        $params['api_token'] = self::$token;
        $params = http_build_query($params);
        $ch = curl_init("https://api.iugu.com/v1/invoices/{$id}?{$params}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function createsOrder(StoreSalesDetails $details, array $items): array
    {
        $headers = [];
        $headers[] = 'Accept: application/json';
        $headers[] = 'api-key: 66e899a5a31f3d4fed1d9988095c3238'; 
        //$headers[] = 'api-key: 4f72e11982e87240d29cacb41a96d94e';
        $itemsArray = [];
        foreach($items as $item) {
            if(strpos($item->getDescription(), "Entrega via Taxa fixa") !== false) continue;
            $itemsArray[] = ["name" => $item->getDescription(), "sku" => "12345", "qty" => $item->getQuantity()];
        }
        $data['shipping_speed'] = 'taxa fixa';
        $data['order_id'] = $details->getInvoiceId() . 333;
        $data['customer_name'] = $details->getPayer_name();
        $data['customer_document'] = $details->getPayer_cpf_cnpj();
        $data['customer_email'] = $details->getPayer_email();
        $data['items'] = $itemsArray;
        $data['shipping_speed'] = 'taxa_fixa';
        $data['address']['name'] = $details->getPayer_name();
        $data['address']['street'] = $details->getPayer_address_street();
        $data['address']['number'] = $details->getPayer_address_number();
        $data['address']['complement'] = $details->getPayer_address_complement() ?: '';
        $data['address']['city'] = $details->getPayer_address_city();
        $data['address']['state'] = $details->getPayer_address_state();
        $data['address']['zipcode'] = $details->getPayer_address_zip_code();
        $data['address']['neighborhood'] = $details->getPayer_address_district();
        $data['address']['phone'] = $details->getPayer_phone_prefix() . $details->getPayer_phone();
        $data['address']['country'] = 'BR';
        $headers = ['Content-Type: application/json', 'Accept: application/json', 'api-key: 66e899a5a31f3d4fed1d9988095c3238'];
        $ch = curl_init("https://camisadimona.com.br/api/v2/order");
        curl_setopt_array($ch, [
            CURLOPT_URL => "https://camisadimona.com.br/api/v2/order",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        return json_decode($response, true);
    }
}