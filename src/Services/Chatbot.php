<?php

namespace App\Services;

class Chatbot
{
    public static function send(array $data): void
    {
        $headers = [];
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        $data['remoteJid'] = '';
        $data['variables'] = [
            ['name' => "pushName", 'value' => $data['name']],
            ['name' => "usuario_cpf", 'value' => $data['cpf']],
            ['name' => "usuario_tempo_inadimplencia", 'value' => $data['compliance']],
            ['name' => "usuario_codigo_boleto", 'value' => $data["digitableLine"]],
            ['name' => "usuario_url_pagamento", 'value' => $data['url']],
            ['name' => "usuario_forma_pagamento", 'value' => $data['paymentMethod']],
            ['name' => "usuario_vencimento_cartao", 'value' => $data['dueDate']]
        ];       
        $url = "";
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        echo $response;
    }
}