<?php

namespace App\Services;

use App\Helpers\Utils;

class Rede
{
//    private static $clientId = '477a27ab-ed90-4373-909c-127af2a203ef';
//    private static $clientSecret = 'VdZaCAoJag';
//    private static $username = 'rodrigo@lifecode.dev';
//    private static $password = 'MXPStymo1-';
//    private static $baseEndpoint = 'https://rl7-sandbox-api.useredecloud.com.br/';

    private static $clientId = '17a19704-9b0f-44f3-84e3-252f1d854131';
    private static $clientSecret = '2ZwixWBQEP';
    private static $username = 'renan.almeida@novo.org.br';
    private static $password = 'T3vETUT]A+e6';
    private static $baseEndpoint = 'https://api.userede.com.br/redelabs/';


    public static function refreshToken()
    {
        $headers = ["Authorization: Basic " . base64_encode(self::$clientId . ':' . self::$clientSecret)];
        $params = [
            "refresh_token" => self::generateAccessToken(),
            "grant_type" => 'refresh_token',
            'username' => self::$username,
            'password' => self::$password,
        ];
        $cURL = curl_init(self::$baseEndpoint . 'oauth/token');
        curl_setopt($cURL, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($cURL);
        $arr = json_decode($result, true);
        curl_close($cURL);
        if ($arr['error']) throw new \Exception("{$arr['error']} : {$arr['error_description']}");
        return $arr['access_token'];
    }

    private static function generateAccessToken()
    {
        $headers = ["Authorization: Basic " . base64_encode(self::$clientId . ':' . self::$clientSecret)];
        $data = [
            'grant_type' => 'password',
            'username' => self::$username,
            'password' => self::$password,
        ];
        $ch = curl_init(self::$baseEndpoint . 'oauth/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result['refresh_token'];
    }

    public static function getSales(string $pv)
    {
        $headers = ['Authorization: Bearer ' . self::refreshToken()];
        $pageKey = '';
        $start = (new \DateTime())->sub(new \DateInterval('P1D'))->format('Y-m-d');
        $transactions = [];
        do {
            $params = http_build_query(['parentCompanyNumber' => $pv, 'subsidiaries' => $pv,
                'startDate' => $start, 'endDate' => date('Y-m-d'), 'pageKey' => $pageKey]);
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => self::$baseEndpoint . "merchant-statement/v1/sales?{$params}",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => $headers,
            ]);
            $sales = curl_exec($curl);
            $sales = json_decode($sales, true);
            curl_close($curl);
            $pageKey = $sales['cursor']['nextKey'];
            foreach ($sales['content']['transactions'] as $transaction) {
                $transactions[] = [
                    'orderNumber' => $transaction['orderNumber'],
                    'amount' => $transaction['amount'],
                    'mdrAmount' => $transaction['mdrAmount'],
                    'netAmount' => $transaction['netAmount'],
                    'tid' => $transaction['tid'],
                    'nsu' => $transaction['nsu'],
                ];
            }
        } while ((bool)$sales['cursor']['hasNextKey'] === true);
        return $transactions;
    }

    public static function getPayments(string $pv)
    {
        $headers = ['Authorization: Bearer ' . self::refreshToken()];
        $pageKey = '';
        $start = (new \DateTime())->sub(new \DateInterval('P10D'))->format('Y-m-d');
        $payments = [];
        do {
            $params = http_build_query(['parentCompanyNumber' => $pv,
                'startDate' => $start, 'endDate' => date('Y-m-d'), 'pageKey' => $pageKey]);
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => self::$baseEndpoint . "/merchant-statement/v2/payments/daily?{$params}",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => $headers,
            ]);
            $sales = curl_exec($curl);
            $sales = json_decode($sales, true);
            curl_close($curl);
            $pageKey = $sales['cursor']['nextKey'];
            foreach ($sales['content']['paymentsDaily'] as $paymentsDaily) {
                foreach ($paymentsDaily['payments'] as $payment) {
                    $payments[] = [
                        'paymentId' => $payment['paymentId'],
                        'paymentDate' => $payment['paymentDate'],
                        'creditOrderParent' => $payment['creditOrderParent'],
                        'accountNumber' => $payment['accountNumber'],
                        'netAmount' => $payment['netAmount'],
                    ];
                }
            }
        } while ((bool)$sales['cursor']['hasNextKey'] === true);
        return $payments;
    }

    public static function findPayment(string $pv, string $paymentId)
    {
        $headers = ['Authorization: Bearer ' . self::refreshToken()];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => self::$baseEndpoint . "/merchant-statement/v1/payments/{$pv}/{$paymentId}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $sales = curl_exec($curl);
        echo $sales;
        die();
        $sales = json_decode($sales, true);
        curl_close($curl);
        return $sales;
    }

    public static function getAcesso(string $requestId)
    {
        $headers = ['Authorization: Bearer ' . self::refreshToken(),];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => self::$baseEndpoint . "/partner/v1/organizations/requests/{$requestId}/features/merchant-statement",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        echo $result;
        die();
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public static function solicitarAcessoExtrato(int $pv)
    {
        $headers = ['Authorization: Bearer ' . self::refreshToken(), 'Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => self::$baseEndpoint . '/partner/v1/organizations/requests/features/merchant-statement',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => json_encode([
                'requestType' => 'P',
                'permissions' => 'R',
                'requestCompanyNumber' => $pv,
                'companyNumbers' => [77059611, 88283461, 72122927, 88391191, 68028300, 77716108, 77716264]
            ])
        ]);
        $result = curl_exec($curl);
        echo $result;
        die();
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public static function getReceivables(string $start, string $pv): array
    {
        $headers = ['Authorization: Bearer ' . self::refreshToken(), 'Content-Type: application/json', 'Accept: application/json'];
        $params = http_build_query(['parentCompanyNumber' => $pv, 'subsidiaries' => $pv, 
                                        'startDate' => $start, 'endDate' => $start]);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => self::$baseEndpoint . "merchant-statement/v1/payments?{$params}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $receivables = curl_exec($curl);
        $receivables = json_decode($receivables, true);
        curl_close($curl);
        return $receivables['content']['payments'];
    }

    public static function getSalesCanceled(string $start, string $pv): array
    {
        $startDate = date('Y-m-d', strtotime('-32 days', strtotime($start)));
        $endDate = date('Y-m-d', strtotime('-29 days', strtotime($start)));
        $headers = ['Authorization: Bearer ' . self::refreshToken()];
        $params = http_build_query(['parentCompanyNumber' => $pv, 'subsidiaries' => $pv, 'startDate' => $startDate, 'endDate' => $endDate, 
            'size' => 100, 'status' => 'CANCELLED']);
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => self::$baseEndpoint . "merchant-statement/v1/sales?{$params}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $sales = curl_exec($curl);
        $sales = json_decode($sales, true);
        curl_close($curl);
        return $sales['content']['transactions'];
    }
}