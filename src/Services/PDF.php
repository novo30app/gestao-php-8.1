<?php

namespace App\Services;

use App\Helpers\Utils;
use Dompdf\Dompdf;
use Dompdf\Options;

class PDF
{
    private static $template = BASEURL .'/assets/receipt/receipt.phtml';

    public static function generateReceipt($data, $idFormat, $transaction, $file)
    {
        $options = new Options();
        $options->setChroot(self::$template);
        $options->setIsRemoteEnabled(true);
        $domPDF = new Dompdf($options);
        $templateFile = file_get_contents(self::$template);
        $templateFile = str_replace('«Numeração_TSE»', $transaction['numero_recibo'], $templateFile);
        $templateFile = str_replace('«Diretório_Código»', $transaction['nomeDiretorio'], $templateFile);
        $templateFile = str_replace('«CNPJ_Diretório»', $transaction['cnpj'], $templateFile);
        $templateFile = str_replace('«Cliente»', $transaction['nome'], $templateFile);
        $templateFile = str_replace('«CPF»', $transaction['cpf'], $templateFile);
        $templateFile = str_replace('«N_DOCTEDOperação»', $transaction['invoice_id'], $templateFile);
        $templateFile = str_replace('«Tipo»', $transaction['paymentString'], $templateFile);
        $templateFile = str_replace('«DC»', $data['account'] == 'DC' ? 'X' : '', $templateFile);
        $templateFile = str_replace('«OR»', $data['account'] == 'OR' ? 'X' : '', $templateFile);
        $templateFile = str_replace('«FP»', $data['account'] == 'FP' ? 'X' : '', $templateFile);
        $templateFile = str_replace('«FE»', $data['account'] == 'FE' ? 'X' : '', $templateFile);
        $templateFile = str_replace('«CPF_Presidente»', $transaction['cpf_presidente'], $templateFile);
        $templateFile = str_replace('«Presidente»', $transaction['nome_presidente'], $templateFile);
        $templateFile = str_replace('«Valor_pago»', number_format($transaction['valor'], 2, ',', '.'), $templateFile);
        $templateFile = str_replace('«Extenso»', mb_convert_case(Utils::describeValue($transaction['valor']), MB_CASE_TITLE, "UTF-8"), $templateFile);
        $templateFile = str_replace('«Dt_Pagamento»', $transaction['data_pago'], $templateFile);
        $domPDF->load_html($templateFile);
        $domPDF->setPaper('A4', 'portrait');
        $domPDF->render();       
        $path = UPLOAD_FOLDER."recibos/$file/$idFormat.pdf";
        file_put_contents($path, $domPDF->output());      
    }
}
