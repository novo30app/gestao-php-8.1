<?php

namespace App\Services;

use App\Helpers\Utils;

class ExatoDigital
{

    const EXATO_API_URL = 'https://api.exato.digital/';
//    const TOKEN = '31a4998a1a85447686299377e2600106';
    const TOKEN = '486e7131a22c42608b7c68327a5ac99f';

    public static function startAsync(string $cpf, string $url)
    {
        $params = [
            'token' => self::TOKEN,
            'cpf' => Utils::onlyNumbers($cpf),
            'options' => '{async:true}&{generate_result_pdf:1}',
            'format' => 'json',
        ];
        $params = http_build_query($params);
        $url = self::EXATO_API_URL . $url . '?' . $params;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function getAsyncResult(string $id, string $url)
    {
        $params = [
            'token' => self::TOKEN,
            'uid' => $id,
            'format' => 'json',
        ];
        $params = http_build_query($params);
        $url = self::EXATO_API_URL . $url . '?' . $params;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function getAsyncResultKeys(string $id, string $url)
    {
        $params = [
            'token' => self::TOKEN,
            'uid' => $id,
            'format' => 'json',
        ];
        $params = http_build_query($params);
        $url = self::EXATO_API_URL . $url . '?' . $params;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        $result = json_decode($result['Result']['ResultJson']);
        if (!$result) return false;
        $array = get_object_vars($result);
        $keys = $resultArray = [];
        foreach ($array as $key => $value) $keys[] = $key;
        foreach ($keys as $key) {
            $resultArray[] = ["{$key}" => $result->$key->validation_result_risk_indicator];
        }
        curl_close($ch);
        return $resultArray;
    }


}