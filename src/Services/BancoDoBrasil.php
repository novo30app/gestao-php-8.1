<?php

namespace App\Services;

use App\Models\Entities\CredentialsBB;
use App\Models\Entities\DirectoryAccounts;
use App\Helpers\Utils;
use App\Models\Entities\ExtractBB;
use App\Models\Entities\PaymentRequirements;

class BancoDoBrasil
{
    private static function accessToken(CredentialsBB $credentials)
    {
        $url = 'https://oauth.bb.com.br/oauth/token';
        $clientId = $credentials->getClientId();
        $clientSecret = $credentials->getClientSecret();
        $token = $clientId . ':' . $clientSecret;
        $token = base64_encode($token);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&scope=extrato-info");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded',
            "Authorization: Basic {$token}"]);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result->access_token;
    }

    public static function find($begin, $end, DirectoryAccounts $account, CredentialsBB $credentials): array
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessToken($credentials);
        $headers[] = 'Accept: application/json';
        $params['gw-dev-app-key'] = $account->getApplicationkey();
        $params['quantidadeRegistroPaginaSolicitacao'] = 200;
        $params['dataInicioSolicitacao'] = $begin; //DDMMAAAA
        $params['dataFimSolicitacao'] = $end; //DDMMAAAA
        $conta = $account->getAccount();
        $agencia = $account->getAgency();
        $page = 1;
        $result = [];
        $result['error'] = false;
        do {
            $params['numeroPaginaSolicitacao'] = $page;
            $paramsBuild = http_build_query($params);
            $ch = curl_init("https://api-extratos.bb.com.br/extratos/v1/conta-corrente/agencia/{$agencia}/conta/{$conta}?{$paramsBuild}");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
            curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
            curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
            if (ENV != 'prod') {
                curl_setopt($ch, CURLOPT_SSLCERT, $_SERVER['DOCUMENT_ROOT'] . 'certificates/' . $credentials->getCertificatePem(false));
                curl_setopt($ch, CURLOPT_SSLKEY, $_SERVER['DOCUMENT_ROOT'] . 'certificates/' . $credentials->getCertificateKey(false));
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            if ($err) {
                $contaStr = "{$account->getDirectory()->getName()} - {$agencia} - {$conta} | " . date('Y-m-d');
                $result['error'] = true;
                Email::send('rodrigo@lifecode.dev', 'Rodrigo', "Erro API Banco do Brasil {$contaStr}", $err, null, null, null, ['renan.almeida@novo.org.br']);
            }
//            dd($err);
//            die($response);
            $response = json_decode($response, true);
            $page++;
            if (is_array($response['listaLancamento'])) $result = array_merge($result, $response['listaLancamento']);
        } while ($response['numeroPaginaAtual'] < $response['quantidadeTotalPagina']);
        return $result;
    }

    private static function accessTokenBankTransactions($credentials)
    {
        $url = "https://oauth.bb.com.br/oauth/token?grant_type=client_credentials&gw-dev-app-key={$credentials->getDeveloperApplicationKey()}&client_id={$credentials->getClientId()}&client_secret={$credentials->getClientSecret()}";
        $token = $credentials->getBasic();
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic {$token}"]);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result->access_token;
    }

    public static function transfersBB(array $arr, PaymentRequirements $payment, DirectoryAccounts $account, 
        CredentialsBB $credentials, array $creditData, int $request): array
    {       
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $params['gw-dev-app-key'] = $credentials->getDeveloperApplicationKey();
        $params = http_build_query($params);
        $cpf = (int)Utils::onlyNumbers($payment->getPaymentDoc());
        $cnpj = 0;
        if(strlen((int)Utils::onlyNumbers($payment->getPaymentDoc())) > 11) {
            $cnpj = (int)Utils::onlyNumbers($payment->getPaymentDoc());
            $cpf = 0;
        }
        $data['numeroRequisicao'] = (int)$request;
        $data['numeroContratoPagamento'] = 0;
        $data['agenciaDebito'] = $account->getAgency();
        $data['contaCorrenteDebito'] = $account->getAccount();
        $data['digitoVerificadorContaCorrente'] = "{$account->getAccountDigit()}";
        $data['tipoPagamento'] = 126;
        $data['listaTransferencias'] = [
            [
                'numeroCOMPE' => (int)$payment->getPaymentBank()->getCod(),
                'numeroISPB' => 0,
                'agenciaCredito' => $creditData['agency'],
                'contaCorrenteCredito' => $creditData['account'],
                'digitoVerificadorContaCorrente' => $creditData['accountDigit'],
                'contaPagamentoCredito' => "",
                'cpfBeneficiario' => $cpf,
                'cnpjBeneficiario' => $cnpj,
                'dataTransferencia' => (int)date_format(new \Datetime($arr['scheduleDate']), 'dmY'),
                'valorTransferencia' => $payment->getPaymentValue(),
                'documentoDebito' => (int)Utils::onlyNumbers($payment->getPaymentDoc()),
                'documentoCredito' => (int)Utils::onlyNumbers($payment->getDirectory()->getCnpj()),
                //'codigoFinalidadeDOC' => 7,
                'numeroDepositoJudicial' => "",
                'descricaoTransferencia' => "Pagamento: ID {$payment->getId()}"
            ]
        ];
        if($payment->getPaymentBank()->getCod() != 001) $data['listaTransferencias'][0]['codigoFinalidadeTED'] = 5;
        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/lotes-transferencias?{$params}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);
        //$err = curl_error($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function pix(array $arr, DirectoryAccounts $account, PaymentRequirements $payment, 
        CredentialsBB $credentials, int $request): array
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $params['gw-dev-app-key'] = $credentials->getDeveloperApplicationKey();
        $params = http_build_query($params);

        if (empty($payment->getPixKey())) {
            throw new \InvalidArgumentException("Chave PIX não informada");
        }

        $pixType = $phone = $cnpj = $cpf = $phoneDDD = 0;
        $email = $random = "";
        if($payment->getPixType() == 1) {
            $pixType = 3;
            $cpf = Utils::onlyNumbers($payment->getPixKey());
            $cnpj = 0;
            if(strlen(Utils::onlyNumbers($payment->getPixKey())) > 11) {
                $cnpj = Utils::onlyNumbers($payment->getPixKey());
                $cpf = 0;
            }
        } else if($payment->getPixType() == 2) {
            $email = $payment->getPixKey();
            $pixType = 2;
        } else if($payment->getPixType() == 3) {
            $phone = $payment->getPixKey();
            $phone = preg_replace('/[^0-9]/', '', $phone);
            $phoneDDD = substr($phone, 0, 2);
            $phone = substr($phone, 2);
            $pixType = 1;
        } else if($payment->getPixType() == 4) {
            $random = $payment->getPixKey();
            $pixType = 4;
        }
        
        if ($payment->getPaymentValue() <= 0) {
            throw new \InvalidArgumentException("Valor do PIX inválido");
        }
        if (!isset($arr['scheduleDate'])) {
            throw new \InvalidArgumentException("Data não informada");
        }

        $data = [
            "numeroRequisicao" => $request,
            "agenciaDebito" => (int)$account->getAgency(),
            "contaCorrenteDebito" => (int)$account->getAccount(),
            "digitoVerificadorContaCorrente" => "{$account->getAccountDigit()}",
            "tipoPagamento" => 126,
            "listaTransferencias" => [
                [
                    "data" => (int)date_format(new \Datetime($arr['scheduleDate']), 'dmY'),
                    "valor" => $payment->getPaymentValue(),
                    "descricaoPagamento" => "Pagamento: ID {$payment->getId()}",
                    "descricaoPagamentoInstantaneo" => "Pagamento: ID {$payment->getId()}",
                    "formaIdentificacao" => $pixType,
                    "dddTelefone" => $phoneDDD,
                    "telefone" => $phone,
                    "email" => $email,
                    "cpf" => $cpf,
                    "cnpj" => $cnpj,
                    "identificacaoAleatoria" => $random,
                    "numeroCOMPE" => 0,
                    "numeroISPB" => 0,
                    "tipoConta" => 1,
                    "agencia" => 0,
                    "conta" => 0,
                    "digitoVerificadorConta" => "s",
                    "contaPagamento" => ""
                ]
            ]
        ];

        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/lotes-transferencias-pix?{$params}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);

        if ($error = curl_error($ch)) {
            throw new \Exception("Erro na requisição PIX: " . $error);
        }

        curl_close($ch);
        return json_decode($response, true);
    }

    public static function bankSlip(array $arr, DirectoryAccounts $account, PaymentRequirements $payment,
        CredentialsBB $credentials, int $request): array
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $params['gw-dev-app-key'] = $credentials->getDeveloperApplicationKey();
        $params = http_build_query($params);
        $data = [
            "numeroRequisicao" => $request,
            "codigoContrato" => 0,
            "numeroAgenciaDebito" => "{$account->getAgency()}",
            "numeroContaCorrenteDebito" => $account->getAccount(),
            "digitoVerificadorContaCorrenteDebito" => "{$account->getAccountDigit()}",
            "lancamentos" => [
                [
                    "numeroDocumentoDebito" => (int)Utils::onlyNumbers($payment->getProvider()->getCpfCnpj()),
                    "numeroCodigoBarras" => str_replace(' ', '', $arr['digitableLine']),
                    "dataPagamento" => (int)date_format(new \Datetime($arr['scheduleDate']), 'dmY'),
                    "valorPagamento" => $payment->getPaymentValue(),
                    "descricaoPagamento" => "Pagamento: ID {$payment->getId()}",
                    "codigoSeuDocumento" => "",
                    "codigoNossoDocumento" => "",
                    "valorNominal" => $payment->getPaymentValue(),
                    "valorDesconto" => 0,
                    "valorMoraMulta" => 0,
                    "codigoTipoPagador" => 2,
                    "documentoPagador" => (int)Utils::onlyNumbers($payment->getDirectory()->getCnpj()),
                    "codigoTipoBeneficiario" => $payment->getProvider()->getType(),
                    "documentoBeneficiario" => (int)Utils::onlyNumbers($payment->getProvider()->getCpfCnpj()),
                    "codigoTipoAvalista" => 0,
                    "documentoAvalista" => 0
                ]
            ]
        ];
        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/lotes-boletos?{$params}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);
        //$err = curl_error($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function bankSlipGuides(array $arr, DirectoryAccounts $account, PaymentRequirements $payment,
        CredentialsBB $credentials, int $request): array
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $params['gw-dev-app-key'] = $credentials->getDeveloperApplicationKey();
        $params = http_build_query($params);
        $data = [
            "numeroRequisicao" => $request,
            "codigoContrato" => 0,
            "numeroAgenciaDebito" => "{$account->getAgency()}",
            "numeroContaCorrenteDebito" => $account->getAccount(),
            "digitoVerificadorContaCorrenteDebito" => "{$account->getAccountDigit()}",
            "lancamentos" => [
                [
                    "codigoBarras" => str_replace(' ', '', $arr['digitableLine']),
                    "dataPagamento" => (int)date_format(new \Datetime($arr['scheduleDate']), 'dmY'),
                    "valorPagamento" => $payment->getPaymentValue(),
                    "numeroDocumentoDebito" => (int)Utils::onlyNumbers($payment->getProvider()->getCpfCnpj()),
                    "codigoSeuDocumento" => "",
                    "descricaoPagamento" => "Pagamento: ID {$payment->getId()}"
                ]
            ]
        ];
        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/lotes-guias-codigo-barras?{$params}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);
        //$err = curl_error($ch);     
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function liberation(PaymentRequirements $payment, CredentialsBB $credentials, ExtractBB $consultPayment): array 
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Content-Type: application/json';
        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/liberar-pagamentos?gw-dev-app-key={$credentials->getDeveloperApplicationKey()}");
        $data = [
            "numeroRequisicao" => $consultPayment->getRequest(),
            "indicadorFloat" => "S" //ciência das possíveis cobranças das tarifas futuras
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);
        //$err = curl_error($ch);     
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function consultPayments(string $request, CredentialsBB $credentials): array
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Content-Type: application/json';
        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/$request?gw-dev-app-key={$credentials->getDeveloperApplicationKey()}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);
        //$err = curl_error($ch);     
        curl_close($ch);
        return json_decode($response, true);
    }

    public static function cancelPayment(PaymentRequirements $payment, CredentialsBB $credentials, 
        DirectoryAccounts $account, ExtractBB $consultPayment): array 
    {
        $headers = [];
        $headers[] = 'Authorization: Bearer ' . self::accessTokenBankTransactions($credentials);
        $headers[] = 'Content-Type: application/json';
        $ch = curl_init("https://api-ip.bb.com.br/pagamentos-lote/v1/cancelar-pagamentos?gw-dev-app-key={$credentials->getDeveloperApplicationKey()}");
        $identifyer = json_decode($consultPayment->getConsult());
        $data['agenciaDebito'] = $account->getAgency();
        $data['contaCorrenteDebito'] = $account->getAccount();
        $data['digitoVerificadorContaCorrente'] = "{$account->getAccountDigit()}";
        $data['listaPagamentos'][]['codigoPagamento'] = $identifyer->pagamentos[0]->identificadorPagamento;
        $data['numeroContratoPagamento'] = 0;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $credentials->getCertificatePassword());
        curl_setopt($ch, CURLOPT_SSLCERT, $credentials->getCertificatePem());
        curl_setopt($ch, CURLOPT_SSLKEY, $credentials->getCertificateKey());
        $response = curl_exec($ch);
        //$err = curl_error($ch);     
        curl_close($ch);
        return json_decode($response, true);
    }
}