<?php

namespace App\Services;

use App\Helpers\Utils;

class IuguService
{
    private static $key = '9029F2B1B1FB39C18BC6E7E9F5874D36D4FAA5437DA1F00A69BDE3C32DFCB9A9'; // PROD2
    //private static $key = '473900DCC93F1CF5329439FDA1EE86A494F5B694ADF7AD70F1A8224971FF9B59'; PROD
    //private static $key = 'AE54672FA6DFE513E178719FC62BABF9724A456B214155E89A71AD91E76E0C73'; TEST

    public static function createCliente(string $name, string $email)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'name' => $name,
            'email' => $email,
        ];
        $ch = curl_init('https://api.iugu.com/v1/customers');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result['id'];
    }

    public static function slip(string $email, string $cpf, string $name, array $address,
                                float  $value, string $description = 'Doação', int $extraDays = 5, string $key = null)
    {
        if(!$key) $key = self::$key;
        $authorization = sprintf('Basic %s', base64_encode($key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'method' => 'bank_slip',
            'email' => $email,
            'bank_slip_extra_days' => $extraDays,
            'due_date' => (new \DateTime())->add(new \DateInterval("P{$extraDays}D"))->format('Y-m-d'),
            'order_id' => uniqid(),
            'payer' => [
                'cpf_cnpj' => Utils::onlyNumbers($cpf),
                'name' => $name,
                'email' => $email,
                'address' => $address
            ],
            'items' => [
                [
                    'description' => $description,
                    'quantity' => 1,
                    'price_cents' => intval(100 * $value)
                ],
            ],
        ];
        $ch = curl_init('https://api.iugu.com/v1/invoices');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }
   
    public static function find(string $id)
    {
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => "https://api.iugu.com/v1/invoices/".$id."?api_token=". self::$key,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public static function conciliation($data, $key)
    {
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => "https://api.iugu.com/v1/accounts/invoices?api_token={$key}&year=".date('Y')."&month={$data['month']}&status={$data['status']}",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;    
    }

    public static function withdraw_conciliations($data)
    {
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => "https://api.iugu.com/v1/withdraw_conciliations?api_token=".self::$key."&status={$data['status']}&from={$data['from']}&to={$data['to']}",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;    
    }

    public static function withdraw_conciliationsRescue($rescue, $key)
    {
        if(!$key) $key = self::$key;
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => "https://api.iugu.com/v1/withdraw_requests/$rescue?api_token={$key}",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => [
            "accept: application/json"
          ],
        ]);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public static function charge(string $cusomerId, string $cardToken, string $email, string $cpf, string $name, array $address,
                                  float  $value, string $description = '', string $key = null)
    {
        if(!$key) $key = self::$key;
        $description = trim($description) == '' ? 'Doação' : $description;
        $authorization = sprintf('Basic %s', base64_encode($key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'payable_with' => ['credit_card'],
            'email' => $email,
            'due_date' => date('Y-m-d'),
            'order_id' => '',
            'payer' => [
                'cpf_cnpj' => Utils::onlyNumbers($cpf),
                'name' => $name,
                'email' => $email,
                'address' => $address
            ],
            'items' => [
                [
                    'description' => $description,
                    'quantity' => 1,
                    'price_cents' => intval(100 * $value)
                ],
            ],
        ];
        $ch = curl_init('https://api.iugu.com/v1/invoices');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return self::chargeCreditCard($result['id'], $cardToken, $key);
    }

    public static function chargeCreditCard($id, $cardToken, $key)
    {
        $authorization = sprintf('Basic %s', base64_encode($key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'invoice_id' => $id,
            'customer_payment_method_id' => $cardToken,
        ];
        $ch = curl_init('https://api.iugu.com/v1/charge');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);   
        $result = json_decode($result, true);
        curl_close($ch);
        if ($result['errors']) throw new \Exception($result['errors']);
        if ($result['status'] == 'unauthorized') throw new \Exception($result['message']);
        return $result;
    }
}