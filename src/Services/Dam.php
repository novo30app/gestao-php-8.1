<?php

namespace App\Services;

class Dam
{
    const TOKEN = 'LkRuOVjuLbGffgFKVxTY';
    const URL_CABINETS = 'https://dam.novo.org.br/api/adminNovo/gabinetes';
    const URL_CABINET = 'https://dam.novo.org.br/api/adminNovo/gabinete';
    const URL_USERS = 'https://dam.novo.org.br/api/adminNovo/assessores';
    const URL_TAGS = 'https://dam.novo.org.br/api/adminNovo/tags';
    const URL_PROJECTS = 'https://dam.novo.org.br/api/adminNovo/projetos';
    const URL_PROJECT = 'https://dam.novo.org.br/api/adminNovo/projeto';

//    const URL_CABINETS = 'https://dam2.novo.org.br/api/adminNovo/gabinetes';
//    const URL_CABINET = 'https://dam2.novo.org.br/api/adminNovo/gabinete';
//    const URL_USERS = 'https://dam2.novo.org.br/api/adminNovo/assessores';
//    const URL_TAGS = 'https://dam2.novo.org.br/api/adminNovo/tags';
//    const URL_PROJECTS = 'https://dam2.novo.org.br/api/adminNovo/projetos';
//    const URL_PROJECT = 'https://dam2.novo.org.br/api/adminNovo/projeto';

//    const URL_CABINETS = 'http://dam.com.br/api/adminNovo/gabinetes';
//    const URL_CABINET = 'http://dam.com.br/api/adminNovo/gabinete';
//    const URL_USERS = 'http://dam.com.br/api/adminNovo/assessores';
//    const URL_TAGS = 'http://dam.com.br/api/adminNovo/tags';
//    const URL_PROJECTS = 'http://dam.com.br/api/adminNovo/projetos';
//    const URL_PROJECT = 'http://dam.com.br/api/adminNovo/projeto';

    public static function getCabinets() {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINETS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getUsers() {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_USERS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function saveUser(array $data) {
        $headers = ['Token' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_USERS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function saveTag(array $data) {
        $headers = ['Token' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_TAGS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function saveCabinet(array $data) {
        $headers = ['Token' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINETS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function getUsersByCabinetId(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINET . '/assessores/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getPollsByCabinetId(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINET . '/votacoes/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getSpendingsByCabinetId(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINET . '/gastos/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getPresencesByCabinetId(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINET . '/presencas/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getReportsByCabinetId(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINET . '/relatorios/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getTags() {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_TAGS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getCabinetById(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_CABINET . '/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getProjectsById(int $cabinetId) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_PROJECTS . '/' . $cabinetId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getProjects() {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_PROJECTS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getProjectById($id) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_PROJECT . '/' . $id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }
}
