<?php

namespace App\Services;

class RdStation
{
    private static $token = "2ae428e7a41c7fb2611e729c5055990f";

    public static function conversion(array $data)
    {
        $data["token_rdstation"] = self::$token;
        $data["traffic_source"] = 1;
        $url = 'https://www.rdstation.com.br/api/1.3/conversions';
        $dataStr = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: ' . strlen($dataStr)]);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $result = curl_exec($ch);
        curl_close($ch);
    }
}