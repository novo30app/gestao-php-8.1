<?php

namespace App\Services;

class Mailchimp
{
    private static $token = "dc4d3379c4d267970971e4e391b3e3e2-us17";

    private static function exeCurl($method, $url, $body)
    {
        $data_json = json_encode($body);
        $headers = array(
            'Authorization: Bearer ' . self::$token, 
            'Content-Type: application/json'
        );
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
        $response = json_decode($response, true);
        return $response;
    }

    public static function createContact(array $body)
    {
        $url = "https://us17.api.mailchimp.com/3.0/lists/560af7438b/members";
        $method = "POST";
        self::exeCurl($method, $url, $body);
    }

    public static function updateContact(array $body)
    {
        $email = md5($body['email_address']);
        $url = "https://us17.api.mailchimp.com/3.0/lists/560af7438b/members/$email";
        $method = "PUT";
        self::exeCurl($method, $url, $body);
    }
}