<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\SignatureDoc;
use App\Models\Entities\SignatureDocSigned;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class SignatureController extends Controller
{
    public function docs(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'signature/docs.phtml', 'section' => 'signature',
            'subMenu' => 'docs', 'user' => $user,]);
    }

    public function signatures(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
        $id = $request->getAttribute('id');
        $doc = $this->em->getRepository(SignatureDoc::class)->find($id ?? 0);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'signature/signatures.phtml', 'section' => 'signature',
            'subMenu' => 'docs', 'user' => $user, 'doc' => $doc]);
    }
	
	public function signaturesList(Request $request, Response $response)
	{
		$user = $this->getLogged(true);
		$id = $request->getAttribute('id');
		$index = $request->getQueryParam('index');
		$name = $request->getQueryParam('name');
		$email = $request->getQueryParam('email');
		$limit = $request->getQueryParam('limit');
		$docs = $this->em->getRepository(SignatureDocSigned::class)->list($id, $name, $email, $limit, $index * $limit);
		$total = $this->em->getRepository(SignatureDocSigned::class)->listTotal($id, $name, $email)['total'];
		$partial = ($index * $limit) + sizeof($docs);
		$partial = $partial <= $total ? $partial : $total;
		return $response->withJson([
			'status' => 'ok',
			'message' => $docs,
			'total' => (int)$total,
			'partial' => $partial,
		], 200)
			->withHeader('Content-type', 'application/json');
	}

    public function signature(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $signature = $this->em->getRepository(SignatureDocSigned::class)->find($id ?? 0);
        if (!$signature || $signature->getHash() != $request->getAttribute('hash') || $signature->getSigned() == 2) {
            echo "Link inválido";
            die();
        }
        return $this->renderer->render($response, 'public/default.phtml', ['page' => 'signature.phtml', 'signature' => $signature,]);
    }

    public function signaturePdf(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $signature = $this->em->getRepository(SignatureDocSigned::class)->find($id ?? 0);
        if (!$signature || $signature->getHash() != $request->getAttribute('hash')) $this->redirectByPermissions();
        $mpdf = new \Mpdf\Mpdf(['margin-header' => 10, 'margin-right' => 10, 'margin-left' => 10, 'margin-footer' => 10]);
        $mpdf->WriteHTML($signature->getText());
        if ($signature->getSigned() == 1) {
            $text = "<h3>Assinatura</h3>
                    <ul>
                        <li>Número do Documento: {$signature->getId()}</li>
                        <li>Hash do Documento: <a href='{$this->baseUrl}assinatura-eletronica/documentos/assinar/{$signature->getId()}/{$signature->getHash()}'>{$signature->getHash()}</a></li>                        
                        <li>Assinante: {$signature->getName()}</li>
                        <li>Documento assinado em: {$signature->getSignedIn()->format('d/m/Y - H:i:s')}.</li>
                        <li>E-mail: {$signature->getEmail()}</li>
                        <li>IP: {$signature->getIp()}</li>                    
                    </ul>";
            $mpdf->AddPage();
            $mpdf->WriteHTML($text);
        }
        $mpdf->Output("{$signature->getName()}-{$signature->getSignatureDoc()->getTitle()}.pdf", \Mpdf\Output\Destination::INLINE);
        return $response->withHeader('Content-Type', 'application/pdf');
    }

    public function list(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $title = $request->getQueryParam('title');
        $limit = $request->getQueryParam('limit');
        $docs = $this->em->getRepository(SignatureDoc::class)->list($title, $limit, $index * $limit);
        $total = $this->em->getRepository(SignatureDoc::class)->listTotal($title)['total'];
        $partial = ($index * $limit) + sizeof($docs);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $docs,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function register(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
        $id = $request->getAttribute('id');
        $doc = $this->em->getRepository(SignatureDoc::class)->find($id ?? 0);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'signature/register.phtml', 'section' => 'signature',
            'subMenu' => 'docs', 'user' => $user, 'doc' => $doc]);
    }

    public function sendDoc(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
        $id = $request->getAttribute('id');
        $doc = $this->em->getRepository(SignatureDoc::class)->find($id ?? 0);
        $regrex = '/\#(.*?)\#/';
        preg_match_all($regrex, $doc->getText(), $variables);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'signature/sendDoc.phtml', 'section' => 'signature',
            'subMenu' => 'docs', 'user' => $user, 'doc' => $doc, 'variables' => $variables[1]]);
    }

    public function import(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
        $id = $request->getAttribute('id');
        $doc = $this->em->getRepository(SignatureDoc::class)->find($id ?? 0);
        $regrex = '/\#(.*?)\#/';
        preg_match_all($regrex, $doc->getText(), $variables);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'signature/import.phtml', 'section' => 'signature',
            'user' => $user, 'doc' => $doc, 'variables' => $variables[1]]);
    }

    public function saveDoc(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'title' => 'Assunto',
                'description' => 'Descrição',
            ];
            Validator::requireValidator($fields, $data);
            $doc = new SignatureDoc();
            if ($data['id']) {
                $doc = $this->em->getRepository(SignatureDoc::class)->find($data['id']);
            }
            $doc->setText($data['description'])
                ->setUser($user)
                ->setTitle($data['title']);
            $this->em->getRepository(SignatureDoc::class)->save($doc);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Documento cadastrado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveDocSigned(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $doc = $this->em->getRepository(SignatureDoc::class)->find($data['id']);
            $fields = [
                'email' => 'E-mail',
                'name' => 'Nome',
            ];
            $regrex = '/\#(.*?)\#/';
            preg_match_all($regrex, utf8_decode($doc->getText()), $variables);
            $text = $doc->getText();
            for ($i = 0; $i < sizeof($variables[1]); $i++) {
                $fields[$variables[1][$i]] = $variables[1][$i];
                $text = str_replace($variables[0][$i], $data[$variables[1][$i]], $text);
            }
            Validator::requireValidator($fields, $data);
            $docSigned = new SignatureDocSigned();
            $docSigned->setText($text)
                ->setName($data['name'])
                ->setEmail($data['email'])
                ->setSignatureDoc($doc);
            $docSigned = $this->em->getRepository(SignatureDocSigned::class)->save($docSigned);
            $msgMail = "<p>Olá {$data['name']},</p>
            <p>Um documento foi gerado para você assinar.</p>
            <p>Para visualizar o mesmo <a href='{$this->baseUrl}assinatura-eletronica/documentos/assinar/{$docSigned->getId()}/{$docSigned->getHash()}' target='_blank'>clique aqui.</a></p>
            <p>Atenção: este é um e-mail automático; por favor não responda.</p>";
            Email::send($data['email'], $data['name'], 'Assinatura de documento - Partido NOVO', $msgMail);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Documento enviado para assinatura",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveDocSignedImportation(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            if (!$user->isAdminNational() && !$user->getAdminAccess()) $this->redirectByPermissions();
            $this->em->beginTransaction();
            set_time_limit(0);
            ini_set("memory_limit", -1);
            ignore_user_abort(1);
            $handle = fopen($_FILES['file']['tmp_name'], "r");
            $data = (array)$request->getParams();
            $doc = $this->em->getRepository(SignatureDoc::class)->find($data['id']);
            $regrex = '/\#(.*?)\#/';
            preg_match_all($regrex, utf8_decode($doc->getText()), $variables);
            $listEmail = [];
            if ($handle !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if (count($data) < 2) $data = explode(';', $data[0]);
                    $text = $doc->getText();
                    for ($i = 0; $i < sizeof($variables[1]); $i++) {
                        $text = str_replace($variables[0][$i], $data[$i + 2], $text);
                    }
                    $docSigned = new SignatureDocSigned();
                    $docSigned->setText($text)
                        ->setName($data[0])
                        ->setEmail($data[1])
                        ->setSignatureDoc($doc);
                    $docSigned = $this->em->getRepository(SignatureDocSigned::class)->save($docSigned);
                    $listEmail[] = $docSigned->getId();
                }
            }
            foreach ($listEmail as $email) {
                $docSigned = $this->em->getRepository(SignatureDocSigned::class)->find($email);
                $msgMail = "<p>Olá {$data['name']},</p>
                        <p>Um documento foi gerado para você assinar.</p>
                        <p>Para visualizar o mesmo <a href='{$this->baseUrl}assinatura-eletronica/documentos/assinar/{$docSigned->getId()}/{$docSigned->getHash()}' target='_blank'>clique aqui.</a></p>
                        <p>Atenção: este é um e-mail automático; por favor não responda.</p>";
                Email::send($docSigned->getEmail(), $docSigned->getName(), 'Assinatura de documento - Partido NOVO', $msgMail);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Documento enviado para assinatura",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch
        (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function duplicate(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $originalDoc = $this->em->getRepository(SignatureDocSigned::class)->findOneBy(['id' => $data['doc'], 'signed' => 0]);
            if (!$originalDoc) throw new \Exception('Documento inválido');
            $originalDoc->setSigned(2);
            $this->em->getRepository(SignatureDocSigned::class)->save($originalDoc);
            $docSigned = new SignatureDocSigned();
            $docSigned->setText($originalDoc->getText())
                ->setName($originalDoc->getName())
                ->setEmail($originalDoc->getEmail())
                ->setSignatureDoc($originalDoc->getSignatureDoc());
            $docSigned = $this->em->getRepository(SignatureDocSigned::class)->save($docSigned);
            $msgMail = "<p>Olá {$data['name']},</p>
            <p>Um documento foi gerado para você assinar.</p>
            <p>Para visualizar o mesmo <a href='{$this->baseUrl}assinatura-eletronica/documentos/assinar/{$docSigned->getId()}/{$docSigned->getHash()}' target='_blank'>clique aqui.</a></p>
            <p>Atenção: este é um e-mail automático; por favor não responda.</p>";
            Email::send($originalDoc->getEmail(), $originalDoc->getName(), 'Segunda via - Assinatura de documento - Partido NOVO', $msgMail);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Documento enviado para assinatura",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveSignature(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'signature' => 'Assinar Documento',
            ];
            Validator::requireValidator($fields, $data);
            $docSigned = $this->em->getRepository(SignatureDocSigned::class)->findOneBy(['id' => $data['id'],
                'hash' => $data['hash'], 'signed' => 0]);
            if (!$docSigned) throw new \Exception('Documento inválido!');
            $acessData = Utils::getAcessData();
            $docSigned->setSignedIn(new \DateTime())
                ->setSigned(1)
                ->setSo($acessData['platform'])
                ->setDevice($acessData['name'])
                ->setIp($acessData['ip']);
            $this->em->getRepository(SignatureDocSigned::class)->save($docSigned);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Documento assinado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}