<?php 

namespace App\Controllers;

use App\Helpers\Session;
use App\Helpers\Utils;
use App\Models\Entities\Communicated;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CommunicatedController extends Controller
{
    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();      
            $linkConfirm = $data['linkConfirm'] ?? null;
            $linkContent = $data['linkCommunicated'] ?? null;
            $process = $data['processCommunicated'] ?? null;
            $directory = $data['directory'] ?? null;
            $way = null;
            if($linkConfirm != 1){
                $file = $request->getUploadedFiles() ? $request->getUploadedFiles(): null;
                if($file['path_file']->getClientFilename() != ""){
                    $file = $file['path_file'];
                    $way = $file->getClientFilename();
                    $name = explode(".", $file->getClientFilename());
                    $ext = array_pop($name);
                    $ext = strtolower($ext);
                    if (!in_array($ext, ["pdf", "doc", "docx", "zip"])) {
                        return $response->withJson([
                        'status' => 'error',
                            'message' => "Somente são aceitos arquivos em pdf, doc, docx ou zip!"
                        ])->withStatus(500);
                    }   
                    if(!is_dir("uploads/comunicados/")) mkdir('uploads/comunicados/', 0777, true);
                    $folder = 'uploads/comunicados/';
                    $file->moveTo($folder . $file->getClientFilename());
                }
            }
            $DataCommunicated = !$data['custId'] ? new Communicated() : $this->em->getRepository(Communicated::class)->findOneBy(['id' => $data['custId']]);
            $DataCommunicated->setDate(\DateTime::createFromFormat('d/m/Y', $data['dateCommunicated']))
                                ->setProcess($process)
                                ->setTitle($data['titleCommunicated'])
                                ->setArchive($way)
                                ->setLink($linkConfirm)
                                ->setType($data['type'])
                                ->setLinkContent($linkContent)
                                ->setDirectory($directory)
                                ->setActive(1);
            $this->em->getRepository(Communicated::class)->save($DataCommunicated);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Comunicado salvo com sucesso",
        ], 201)
            ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
    public function DisableCommunicated(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $DataCommunicated = $this->em->getRepository(Communicated::class)->findOneBy(['id' => $id]);
            $DataCommunicated->setActive(0);
            $this->em->getRepository(Communicated::class)->save($DataCommunicated);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Comunicado desativado com sucesso",
        ], 201)
            ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }    
}

?>