<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\User;
use App\Models\Entities\CepMembers;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CepController extends Controller
{
    public function members(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $this->redirectByPermissions();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'cep/members.phtml', 'section' => 'cepMembers', 
            'user' => $user, 'subMenu' => 'cep', 'title' => 'Membros da CEP']);
    }

    public function refreshStatus(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $member = $this->em->getRepository(CepMembers::class)->findOneBy(['id' => $id]);
            $status = $member->getActive() == 1 ? 0 : 1;
            $member->setActive($status);
            $this->em->getRepository(CepMembers::class)->save($member);
            return $response->withJson([
            'status' => 'ok',
            'message' => 'Registro atualizado com sucesso',
        ], 201)
            ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getMember(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $member = $this->em->getRepository(CepMembers::class)->findOneBy(['id' => $id]);
            if(!$member) throw new \Exception("Solicitação inválida!");
            $arrayMember = ['id' => $member->getId(), 'name' => $member->getName(), 'dateElected' => DATE_FORMAT($member->getDateElected(), 'd/m/Y'), 'file' => $member->getFile()];
            return $response->withJson([
            'status' => 'ok',
            'message' => $arrayMember,
        ], 201)
            ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function register(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['nameModal' => 'Nome', 'dateElectedModal' => 'Data eleito'];
            Validator::requireValidator($fields, $data);
            $file = $request->getUploadedFiles()['termModal'];
            $update = "| user:" . $user->getId() . " - " . date('d/m/Y H:i:s') . " |";
            if($data['memberId']){
                $member = $this->em->getRepository(CepMembers::class)->findOneBy(['id' => $data['memberId']]);
                if($member->getLastUpdate()) $update = $member->getLastUpdate() . " | user:" . $user->getId() . " - " . date('d/m/Y H:i:s') . " | ";
            } else {
                $member = new CepMembers();
                if(!$file->getClientFilename()) throw new \Exception('Atenção, é obrigatório o anexo de um Termo de Nomeação e Posse');
            }
            if($file->getClientFilename()){
                $extension = explode('.', $file->getClientFilename());
                if (!in_array(end($extension), array('pdf', 'docx'))) throw new \Exception('Somente são aceitos arquivos em .pdf ou .docx');
                if($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
                $target = time() . '.' . end($extension);
                $file->moveTo("uploads/cep/" . $target);    
            }
            $member->setName($data['nameModal'])
                   ->setDateElected(new \Datetime(date('Y-m-d', strtotime(str_replace('/', '-', $data['dateElectedModal'])))))
                   ->setActive(1)
                   ->setLastUpdate($update);
                   if($file->getClientFilename()) $member->setFile($target);
            $this->em->getRepository(CepMembers::class)->save($member);
            $this->em->commit();
            return $response->withJson([
            'status' => 'ok',
            'message' => 'Registro salvo com sucesso!',
        ], 201)
            ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}