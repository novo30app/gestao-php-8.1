<?php

namespace App\Controllers;

use App\Models\Entities\CredentialsBB;
use App\Models\Entities\AffiliateEvolution;
use App\Models\Entities\City;
use App\Models\Entities\Contract;
use App\Models\Entities\DirectoryAccounts;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\OfxFiles;
use App\Models\Entities\OfxLines;
use App\Models\Entities\PaymentRequerimentsApportionment;
use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\State;
use App\Models\Entities\TesteRodrigo;
use App\Models\Entities\TokensBB;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\IuguTokens;
use App\Models\Entities\Directory;
use App\Models\Entities\Candidate;
use App\Models\Entities\CivilState;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\Country;
use App\Models\Entities\Phone;
use App\Models\Entities\Address;
use App\Models\Entities\UserAdmin;
use App\Models\Repository\AffiliateEvolutionRepository;
use App\Models\Entities\TransactionsAbsent;
use App\Models\Entities\StoreSales;
use App\Models\Entities\StoreSalesDetails;
use App\Models\Entities\StoreSalesItems;
use App\Models\Entities\ExtractBB;
use App\Services\BancoDoBrasil;
use App\Services\Email;
use App\Services\Rede;
use App\Services\IuguService;
use App\Services\Loja;
use App\Services\Chatbot;
use App\Services\Hubspot;
use App\Helpers\Messages;
use App\Helpers\Utils;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CronCrontoller extends Controller
{
    public function evolution(Request $request, Response $response)
    {
        $today = new \DateTime();
        $states = $this->em->getRepository(State::class)->findAll();
        $states = $this->em->getRepository(State::class)->findBy(['sigla' => 'SC']);
        //$cities = $this->em->getRepository(City::class)->findAll();
        $affiliateEvolutionRepository = $this->em->getRepository(AffiliateEvolution::class);
        $affiliateEvolutionRepository->deleteByPeriod($today->format('m/Y'));
        die();
        //nacional
        $start = new \DateTime();
        $start->sub(new \DateInterval('P5Y'));
        while ($start < $today) {
            $start->setDate($start->format('Y'), $start->format('m'), 1);
            $end = clone $start;
            $end->add(new \DateInterval('P1M'));
            $end->sub(new \DateInterval('P1D'));
            $plus = $this->em->getRepository(User::class)->evolutionPlus($start, $end);
            $minus = $this->em->getRepository(User::class)->evolutionMinus($start, $end);
            $total = $this->em->getRepository(User::class)->evolutionTotal($start, $end);
            $affiliateEvolution = new AffiliateEvolution();
            $affiliateEvolution->setAffiliations($plus)->setDisaffiliation($minus)->setTotal($total)->setPeriod($start->format('m/Y'));
            $affiliateEvolutionRepository->save($affiliateEvolution);
            $start->add(new \DateInterval('P1M'));
        }
        //fim nacional
        //estadual
        $start = new \DateTime();
        $start->sub(new \DateInterval('P5Y'));
        while ($start < $today) {
            $start->setDate($start->format('Y'), $start->format('m'), 1);
            $end = clone $start;
            $end->add(new \DateInterval('P1M'));
            $end->sub(new \DateInterval('P1D'));
            for ($i = 0; $i < sizeof($states); $i++) {
                $plus = $this->em->getRepository(User::class)->evolutionPlus($start, $end, $states[$i]->getId());
                $minus = $this->em->getRepository(User::class)->evolutionMinus($start, $end, $states[$i]->getId());
                $total = $this->em->getRepository(User::class)->evolutionTotal($start, $end, $states[$i]->getId());
                $affiliateEvolution = new AffiliateEvolution();
                $affiliateEvolution->setAffiliations($plus)->setDisaffiliation($minus)->setTotal($total)->setPeriod($start->format('m/Y'))->setState($states[$i]->getId());
                $affiliateEvolutionRepository->save($affiliateEvolution);
            }
            $start->add(new \DateInterval('P1M'));
        }
        //fim estadual
        return $response->withJson([
            'status' => 'ok',
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function bancoDoBrasilExtrato(Request $request, Response $response)
    {
        $pendencies = $this->em->getRepository(PaymentRequerimentsApportionment::class)->getPendents();
        foreach ($pendencies as $pendency) {
            $paymentRequerimentsApportionment = new PaymentRequerimentsApportionment();
            $paymentRequerimentsApportionment->setPaymentRequirements($this->em->getReference(PaymentRequirements::class, $pendency['id']))
                ->setValue($pendency['nfValue']);
            $this->em->getRepository(PaymentRequerimentsApportionment::class)->save($paymentRequerimentsApportionment);
        }
        $start = new \DateTime();
        $end = clone $start;
        $end->sub(new \DateInterval('P1D'));
        $start->sub(new \DateInterval('P1D'));
        $today = new \DateTime();

        $accounts = $this->em->getRepository(DirectoryAccounts::class)->findBy(['apiExtract' => 1]);
        foreach ($accounts as $account) {

            // verificando se não existe um cadastro para esse intervalo de data e conta do diretório
            $verify = $this->em->getRepository(OfxFiles::class)->verifyDate($account, $start, $end);
            // se existir algum cadastro exibe mensagem de erro e exclui o documento que acabou de fazer upload
            if ($verify) continue;

            $credentials = $this->em->getRepository(CredentialsBB::class)->findOneBy(['directory' => $account->getDirectory()]);
            if (!$credentials) continue;
            $transactions = BancoDoBrasil::find((int)$start->format('dmY'), (int)$end->format('dmY'), $account, $credentials);
            if ($transactions['error']) continue;

            array_shift($transactions);//remover primeiro elemento o saldo do dia anterior

            $ofxFile = new OfxFiles();
            $ofxFile->setDirectoryAccount($account)
                ->setFile("API {$today->format('Y-m-d')}")
                ->setStart($start)
                ->setEnd($end);
            $ofxFile = $this->em->getRepository(OfxFiles::class)->save($ofxFile);

            // Salvando as informações dos OFX's
            foreach ($transactions as $transaction) {
                //1 – lançamento contabilizado
                //2 – lançamento futuro
                if ($transaction['indicadorTipoLancamento'] != 1 || $transaction['numeroDocumento'] == 0) continue;
                $line = new OfxLines();
                $line->setUser($this->em->getReference(UserAdmin::class, 19))//rodrigo
                ->setOfxFiles($ofxFile)
                    ->setDate(\DateTime::createFromFormat('dmY', str_pad($transaction['dataLancamento'], 8, '0', STR_PAD_LEFT)))
                    ->setType($transaction['indicadorSinalLancamento'] == 'C' ? 'Cŕedito' : 'Débito')
                    ->setValue($transaction['valorLancamento'])
                    ->setUniqueIdentifier('API')
                    ->setExtractNumber($transaction['numeroDocumento'])
                    ->setDirectoryAccount($account)
                    ->setDirectory($account->getDirectory())
                    ->setDescription("{$transaction['textoDescricaoHistorico']} {$transaction['textoInformacaoComplementar']}")
                    ->setOfx($ofxFile)
                    ->setCategory($transaction['indicadorSinalLancamento'] == 'C' ? OfxLines::REVENUE : OfxLines::EXPENSE);
                $this->em->getRepository(OfxLines::class)->save($line);
            }
        }
    }


    public function rede()
    {
        $pv = 77059611;
//        $payments = Rede::getPayments($pv);
//        foreach ($payments as $payment) {
//            Rede::findPayment($pv, $payment['paymentId']);
//        }
        $transactionsApi = Rede::getSales($pv);
        $totalApi = count($transactionsApi);
        $start = (new \DateTime())->sub(new \DateInterval('P1D'));
        $transactionsBd = $this->em->getRepository(Transaction::class)->getConciliationRede($start, $start, 1);
        $totalBd = count($transactionsBd);
        $differentValues = $correctValues = $apiNotFind = $bdNotFound = $transactionsApiInvoices = [];
        $i = 0;
        foreach ($transactionsApi as $transaction) {
            $i++;
//            if ($i == 3) $transaction['orderNumber'] = $transaction['orderNumber'] . '123';
//            if ($i == 5) $transaction['amount'] = $transaction['amount'] - 1;
            $transactionsApiInvoices[] = $transaction['orderNumber'];
            $transactionBd = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $transaction['orderNumber']]);
            if (!$transactionBd) {
                $apiNotFind[] = $transaction;
            } else if ($transaction['amount'] != $transactionBd->getValor()) {
                $transaction['bdValue'] = $transactionBd->getValor();
                $differentValues[] = $transaction;
            } else {
                $transaction['bdValue'] = $transactionBd->getValor();
                $correctValues[] = $transaction;
                $depositSale = $this->getWorkingDay($start->format('Y-m-d'));
                $transactionBd->setValorSemTaxas($transaction['netAmount'])->setDataDeposito($depositSale)->setNsu($transaction['nsu']);
                $this->em->getRepository(Transaction::class)->save($transactionBd);
            }
        }
        foreach ($transactionsBd as $item) {
            if (!in_array($item->getInvoiceId(), $transactionsApiInvoices)) {
                $bdNotFound[] = [
                    'id' => $item->getId(),
                    'invoice' => $item->getInvoiceId(),
                    'valor' => $item->getValor(),
                    'pessoa' => $this->em->getRepository(User::class)->find($item->getTbPessoaId())->getName()
                ];
            }
        }
        $msg = "<b>PV: </b>{$pv} <br>";
        $msg .= "<b>Transações API: </b>{$totalApi} <br>";
        $msg .= "<b>Transações Banco de Dados: </b>{$totalBd} <br>";
        $msg .= '<b>Sincronizados: </b>' . count($correctValues) . '<br>';

        $msg .= '<hr> <b>Não encontradas no banco de dados: </b>' . count($apiNotFind) . '<br>';
        $msg .= '<table style="border: 1px solid black !important; text-align: center !important;border-collapse: collapse;">
                        <tr style="border: 1px solid black !important; text-align: center !important;border-collapse: collapse;">
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Invoice</td>
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Valor</td>
                        </tr>';
        foreach ($apiNotFind as $item) {
            $msg .= "<tr style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>
                        <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$item['orderNumber']}</td>
                        <td style='border: 1px solid black !important; text-align: center !important; border-collapse: collapse;'>{$item['amount']}</td>
                     </tr>";
        }
        $msg .= '</table>';

        $msg .= '<hr> <b>Não encontradas na API: </b>' . count($bdNotFound) . '<br>';
        $msg .= '<table style="border: 1px solid black !important; text-align: center !important;border-collapse: collapse;">
                        <tr style="border: 1px solid black !important; text-align: center !important;border-collapse: collapse;">
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">ID</td>
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Pessoa</td>
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Invoice</td>
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Valor</td>
                        </tr>';
        foreach ($bdNotFound as $item) {
            $msg .= "<tr style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$item['id']}</td>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$item['pessoa']}</td>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$item['invoice']}</td>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$item['valor']}</td>
                    </tr>";
        }
        $msg .= '</table>';

        $msg .= '<hr><b>Valores diferentes: </b>' . count($differentValues) . '<br>';
        $msg .= '<table style="border: 1px solid black !important; text-align: center !important;border-collapse: collapse;">
                        <tr style="border: 1px solid black !important; text-align: center !important;border-collapse: collapse;">
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Invoice</td>
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Valor API</td>
                        <td style="border: 1px solid black !important; text-align: center !important; border-collapse: collapse;">Valor Banco de Dados</td>
                        </tr>';
        foreach ($differentValues as $diferentsValue) {
            $msg .= "<tr style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$diferentsValue['orderNumber']}</td>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$diferentsValue['amount']}</td>
                    <td style='border: 1px solid black !important; text-align: center !important;border-collapse: collapse;'>{$diferentsValue['bdValue']}</td>
                    </tr>";
        }
        $msg .= '</table>';
//        echo $msg;
        $copyTo = ['deliane@novo.org.br'];
        if ($apiNotFind || $bdNotFound || $differentValues) array_push($copyTo, 'ti@novo.org.br');
        Email::send('jessica.costa@novo.org.br', 'Jessica', "Conciliação REDE PV {$pv} | {$start->format('Y-m-d')}",
            $msg, null, null, null, $copyTo);
    }

    public function verifyJourney2024(): void
    {
        $transactionsAbsent = $this->em->getRepository(TransactionsAbsent::class)->getTransactionsAbsent();
        foreach ($transactionsAbsent as $t) {
            $checkTransaction = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $t['invoiceId']]);
            if ($checkTransaction) continue;
            $transaction = $this->em->getRepository(Candidate::class)->getCandidateJourney2024($t['invoiceId']);
            if ($transaction) {
                $transaction = $transaction[0];
                $cpf = Utils::formatCpf($transaction['cpf']);
                $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
                if (!$user) {
                    $user = new User();
                    $user->setName($transaction['name'])->setEmail($transaction['email'])->setDataNascimento(new \Datetime($transaction['birthDate']))->setCpf($cpf)
                        ->setEstadoCivil($this->em->getReference(CivilState::class, 6))->setGenero($this->em->getReference(Gender::class, 3))
                        ->setEscolaridade($this->em->getReference(Schooling::class, 7))->setTituloEleitoralPaisId($this->em->getReference(Country::class, 7))
                        ->setTermoAceite0(1)->setTermoAceite1(1)->setTermoAceite2(1)->setTermoAceite3(1)->setDatacriacao(new \DateTime())->setFiliado(0)->setStatus(0)
                        ->setEmailConfirmado(0)->setPendenciaTitulo(0)->setMembroDiretorio(0);
                    $this->em->getRepository(User::class)->save($user);
                    
                    /*-----Hubspot/Mailchimp-----*/
                    Hubspot::createContact($user);
                    $this->mailchimpCreateUser($user, null, $transaction['whats']);

                    $phone = new Phone();
                    $whats = str_replace('55', '', $transaction['whats']);
                    $phone->setTbPessoaId($user->getId())->setTbTipoTelefoneId(3)->setPais('br')->setDdi(55)->setTelefone($whats);
                    $this->em->getRepository(Phone::class)->save($phone);
                    $address = new Address();
                    $address->setTbPessoaId($user->getId())->setTbTipoEnderecoId(3)->setResideExterior(0)->setCountryId(33)->setStateId($transaction['state'])
                        ->setCityId($transaction['city'])->setCep($transaction['zipCode'])->setCepStatus(2)->setEndereco($transaction['address'])
                        ->setNumero($transaction['number'])->setComplemento($transaction['complement'])->setBairro($transaction['neighborhood']);
                    $this->em->getRepository(Address::class)->save($address);
                }
                $paymentMethod = match ($t['payment_method']) {
                    'iugu_pix' => 11,
                    'iugu_bank_slip' => 2,
                    'iugu_credit_card' => 1,
                    default => ''
                };
                $t = $this->populateTransaction($user, $t['paid_value'], new \DateTime($t['occurrence_date']), new \DateTime($t['due_date']), new \DateTime($t['paid_at']), $paymentMethod,
                    $t['status'], '', 0, 1, 1, 'Jornada 2024', '', $t['invoiceId'], '', 1, 1, '', '', '', '');
                $this->em->getRepository(Transaction::class)->save($t);
            }
        }
    }

    public function conciliationIugu()
    {
        $arr = ['month' => date('m'), 'status' => 'paid'];
        $keyDirectories = $this->em->getRepository(IuguTokens::class)->findAll(['id' => 'DESC']);
        foreach ($keyDirectories as $key) {
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $key->getDirectory()]);
            $data = IuguService::conciliation($arr, $key->getToken());
            if (!empty($data)) {
                $exists = $absent = 0;
                $transactionsAbsent = [];
                foreach ($data as $d) {
                    $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['gatewayPagamento' => 1, 'invoiceId' => $d['id']]);
                    if (!$transaction) {
                        $paid_value = str_replace('.', '', $d['paid_value']);
                        $paid_value = str_replace(',', '.', $paid_value);
                        $paid_value = str_replace('R$', ' ', $paid_value);
                        $register = new TransactionsAbsent();
                        $register->setInvoiceId($d['id'])->setPayment_method($d['payment_method'])->setStatus($d['status'])->setPaid_value($paid_value)
                            ->setOccurrence_date(new \DateTime($d['occurrence_date']))->setDue_date(new \DateTime($d['due_date']))->setPaid_at(new \DateTime($d['paid_at']));
                        $this->em->getRepository(TransactionsAbsent::class)->save($register);
                        $absent++;
                        array_push($transactionsAbsent, $d['id']);
                    } else {
                        $exists++;
                    }
                }
                $msg = '';
                $resume = ['total' => count($data), 'exists' => $exists, 'absent' => $absent];
                if (sizeof($transactionsAbsent) > 0) {
                    //$copyTo = ['jessica.costa@novo.org.br'];
                    $msg = Messages::conciliationIugu($resume, $transactionsAbsent);
                    Email::send('renan@novo.org.br', 'Renan Souza de Almeida', $directory->getName() . ' - Conciliação diária - IUGU', $msg, null, null, null, null);
                }
            }
        }
    }

    public function getReceivables()
    {
        $start = (new \DateTime())->sub(new \DateInterval('P1D'));
        /** BASE */
        $directory = 1;
        $transactionsBase = $this->em->getRepository(Transaction::class)->listConciliationReceivables($start->format('Y-m-d'), $directory)[0];
        $sumBase = number_format($transactionsBase['value'], 2, ',', '.');
        /** REDE */
        $transactions = Rede::getReceivables($start->format('Y-m-d'), 77059611);
        $sumRede = 0;
        foreach ($transactions as $t) {
            $sumRede += $t['netAmount'];
        }
        $sumRede = number_format($sumRede, 2, ',', '.');
        $copyTo = ['deliane@novo.org.br'];
        // ACTION
        if ($sumBase === $sumRede) {
            $transactions = [];
            $msg = Messages::receivablesSuccess($sumBase, $sumRede, $transactions, $start->format('d/m/Y'));
        } else {
            array_push($copyTo, 'ti@novo.org.br');
            /** GET CANCELED TRANSACTION */
            $transactions = Rede::getSalesCanceled($start->format('Y-m-d'), 77059611);
            $msg = Messages::receivablesSuccess($sumBase, $sumRede, $transactions, $start->format('d/m/Y'));
            if ($transactions) {
                foreach ($transactions as $transaction) {
                    $t = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $transaction['orderNumber']]);
                    foreach ($transaction['tracking'] as $tracking) {
                        if ($tracking['status'] === 'CANCELLED') {
                            $t->setStatus('canceled')->setDataCancelamento(new \Datetime($tracking['date']));
                            $this->em->getRepository(Transaction::class)->save($t);
                        }
                    }
                }
            }
        }
        /** SEND EMAIL */
        Email::send('jessica.costa@novo.org.br', 'Jessica', "Conciliação recebíveis REDE - dia " . $start->format('d/m/Y'), $msg, null, null, null, $copyTo);
        echo 'Rotina concluída!';
    }

    public function changeStatusContract(Request $request, Response $response)
    {
        $contracts = $this->em->getRepository(Contract::class)->searchSituationContract();
        foreach ($contracts as $contract) {
            $additive = $this->em->getRepository(Contract::class)->verifyAdditive($contract['id']);
            if (!$additive) {
                $situation = $this->em->getRepository(Contract::class)->find($contract['id']);
                $situation->setSituation(Contract::SITUATION_EXPIRADO);
                $this->em->getRepository(Contract::class)->save($situation);
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => 'Status alterados com sucesso!'
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function salesConsultation(Request $request, Response $response)
    {
        $sales = Loja::checkInvoices();
        foreach ($sales as $sale) {
            if ($this->em->getRepository(StoreSales::class)->findOneBy(['invoiceId' => $sale['id']])) continue;
            $register = new StoreSales();
            $register->setRegister(date('Y-m-d'))
                ->setCreatedAt(new \Datetime($sale['created_at']))
                ->setInvoiceId($sale['id'])
                ->setStatus($sale['status'])
                ->setPaid_at(new \Datetime($sale['due_date'])) //$sale['paid_at']
                ->setPaid_value(Utils::moneyToFloat($sale['pending_value'])); //$sale['paid_value']
            $this->em->getRepository(StoreSales::class)->save($register);
        }
        echo 'Rotina finalizada!';
    }

    public function salesDetails()
    {
        $sales = $this->em->getRepository(StoreSales::class)->findBy(['register' => date('Y-m-d')]);
        foreach ($sales as $sale) {
            $details = Loja::checkInvoicesDetails($sale->getInvoiceId());
            $register = new StoreSalesDetails();
            $register->setCreatedAt(new \Datetime($details['created_at_iso']))
                ->setSale($sale)
                ->setInvoiceId($details['id'])
                ->setTotal(Utils::moneyToFloat($details['total']))
                ->setStatus($details['status'])
                ->setPaid_at(new \Datetime($details['due_date'])) //paid_at
                ->setPayable_with($details['payable_with'])
                ->setPayer_name($details['payer_name'])
                ->setPayer_email($details['payer_email'])
                ->setPayer_cpf_cnpj($details['payer_cpf_cnpj'])
                ->setPayer_phone($details['payer_phone'])
                ->setPayer_phone_prefix($details['payer_phone_prefix'])
                ->setPayer_address_zip_code($details['payer_address_zip_code'])
                ->setPayer_address_street($details['payer_address_street'])
                ->setPayer_address_district($details['payer_address_district'])
                ->setPayer_address_city($details['payer_address_city'])
                ->setPayer_address_state($details['payer_address_state'])
                ->setPayer_address_number($details['payer_address_number'])
                ->setPayer_address_complement($details['payer_address_complement']);
            $this->em->getRepository(StoreSalesDetails::class)->save($register);
            foreach ($details['items'] as $item) {
                $newItem = new StoreSalesItems();
                $newItem->setSale($sale)
                    ->setDescription($item['description'])
                    ->setQuantity($item['quantity'])
                    ->setCreatedAt(new \Datetime($item['created_at']))
                    ->setPrice(Utils::moneyToFloat($item['price']));
                $this->em->getRepository(StoreSalesItems::class)->save($newItem);
            }
        }
    }

    public function salesCreateOrder()
    {
        $sales = $this->em->getRepository(StoreSales::class)->findBy(['register' => date('Y-m-d')]);
        foreach ($sales as $sale) {
            $details = $this->em->getRepository(StoreSalesDetails::class)->findOneBy(['sale' => $sale->getId()]);
            $items = $this->em->getRepository(StoreSalesItems::class)->findBy(['sale' => $sale->getId()]);
            $order = Loja::createsOrder($details, $items);
            $order = $order['order'] ?: $order['error'];
            $sale->setOrderDimona($order);
            $this->em->getRepository(StoreSales::class)->save($sale);
        }
        echo 'Rotina concluída!';
    }

    public function chatbot(Request $request, Response $response)
    {
        die();
        $data = $this->em->getRepository(User::class)->listChatBot();
        foreach ($data as $data) {
            Chatbot::send($data);
        }
        echo 'Rotina concluída!';
    }

    public function consultPayments(Request $request, Response $response)
    {
        $records = $this->em->getRepository(ExtractBB::class)->consultPayments();
        foreach($records as $record) {
            $request = $record['request'];
            $payment = $this->em->getRepository(PaymentRequirements::class)->find($record['payment']);
            $credentials = $this->em->getRepository(CredentialsBB::class)
                ->findOneBy(['directory' => $payment->getDirectory(), 'type' => 2]);
            $response = BancoDoBrasil::consultPayments($request, $credentials);
            $status = $response['estadoRequisicao'];
            $statusString = $response['pagamentos'][0]['estadoPagamento'];
            $beneficiaryName = $response['pagamentos'][0]['nomeBeneficiario'];
            $return = json_encode($response, true);
            $register = $this->em->getRepository(ExtractBB::class)->findOneBy(['request' => $request, 'payment' => $record['payment']]);
            $register->setStatus($status ?? '')
                    ->setStatusString($statusString ?? '')
                    ->setBeneficiaryName($beneficiaryName ?? '')
                    ->setConsult($return);
            $this->em->getRepository(ExtractBB::class)->save($register);
            if($statusString == 'PAGO') {
                $payment->setRequestStatus(3)
                    ->setPayDay($payment->getPaymentDueDate());
            } else if (in_array($statusString, ['AGENDADO', 'DEBITADO'])) {
                $payment->setRequestStatus(2)
                    ->setScheduledDate($payment->getScheduledDate());
            }
            $this->em->getRepository(PaymentRequirements::class)->save($payment);
        }
        echo 'Rotina concluída!';
    }
}