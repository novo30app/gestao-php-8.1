<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Commons\Entities\LibertasCourse;
use App\Models\Commons\Entities\LibertasCourseCoupon;
use App\Models\Entities\SystemFeatures;
use DateTime;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class LibertasCourseCouponController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
       Validator::validatePermission(SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_CUPONS_VISUALIZACAO, true);
        $courses = $this->em->getRepository(LibertasCourse::class)->findBy([], ['title' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-course-coupon/index.phtml',
            'user' => $user, 'subMenu' => 'education', 'subSectionActive' => 'libertas', 'subSection' => 'libertasCourseCoupon', 
            'courses' => $courses
        ]);
    }

    public function list(Request $request, Response $response)
    {
        $id = $request->getAttribute('route')->getArgument('id');
        $index = $request->getQueryParam('index') ?? 0;
        $limit = $request->getQueryParam('limit') ?? 1;
        $filter = $request->getQueryParams();
        if ($id) $filter['id'] = $id;
        $docs = $this->em->getRepository(LibertasCourseCoupon::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(LibertasCourseCoupon::class)->listTotal($filter);
        $partial = ($index * $limit) + sizeof($docs);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $docs,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'course' => 'Curso',
                'code' => 'Código',
                'limitUse' => 'Limite',
                'type' => 'Tipo',
            ];
            if ($data['type'] == LibertasCourseCoupon::PERCENTAGE) $fields['porcent'] = 'Percentagem';
            else $fields['price'] = 'Valor';
            Validator::requireValidator($fields, $data);
            $coupon = new LibertasCourseCoupon();
            if ($data['autoEnrollmentId']) {
                $coupon = $this->em->getRepository(LibertasCourseCoupon::class)->find($data['autoEnrollmentId']);
            }
            $coupon->setUser($user)
                ->setUpdateAt(new DateTime())
                ->setLibertasCourse($this->em->getReference(LibertasCourse::class, $data['course']))
                ->setLimitUse($data['limitUse'])
                ->setType($data['type'])
                ->setCode($data['code'])
                ->setPrice($data['type'] == LibertasCourseCoupon::PERCENTAGE ? (int)$data['porcent'] : Utils::moneyToFloat($data['price']));
            $this->em->getRepository(LibertasCourseCoupon::class)->save($coupon);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Cadastrado realizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $coupon = $this->em->getRepository(LibertasCourseCoupon::class)->find($id);
            $coupon->changeActive();
            $this->em->getRepository(LibertasCourseCoupon::class)->save($coupon);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Status alterado com sucesso',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

}