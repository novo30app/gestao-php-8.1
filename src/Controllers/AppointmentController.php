<?php

namespace App\Controllers;

use App\Models\Entities\AccessAdmin;
use App\Models\Entities\AffiliatedStatus;
use App\Models\Entities\Appointment;
use App\Models\Entities\State;
use App\Models\Entities\User;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AppointmentController extends Controller
{
    public function appointment(Request $request, Response $response) {
        $user = $this->getLogged();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $affiliateds = $this->em->getRepository(User::class)->appointmentAffiliated($date);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/appointment.phtml', 'cities' => $cities,
            'section' => 'appointment', 'subMenu' => 'affiliated', 'user' => $user, 'affiliateds' => $affiliateds]);
    }

    public function makeAnAppointment(Request $request, Response $response) {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $appointment = new Appointment();
            $appointment->setAffiliated($this->em->getReference(User::class, $data['affiliated'] ?? 567));
            if($data['appointmentId'] > 0) {
                $appointment = $this->em->getRepository(Appointment::class)->find($data['appointmentId']);
            }
            $appointment->setStatus($data['status'])
                ->setAppointment(\DateTime::createFromFormat('d/m/Y H:i', $data['appointment']))
                ->setLink($data['link'] ?? '');
            $this->em->getRepository(Appointment::class)->save($appointment);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Agendamento realizado com sucesso',
            ], 201)->withHeader('Content-Type', 'application/json');

        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getAppointments(Request $request, Response $response) {
        $id = $request->getAttribute('route')->getArgument('id');
        $name = $request->getParam('name');
        $status = $request->getParam('status');
        $appointments = $this->em->getRepository(Appointment::class)->list($id, $name, $status);
        $appointment = '';
        $array = [];
        foreach ($appointments as $a) {
            $appointment = \DateTime::createFromFormat('Y-m-d H:i:s', $a['APPOINTMENT']);
            $appointment = $appointment->format('d/m/Y H:i');
            $array[] = ['id' => $a['ID'], 'affiliated' => $a['USER'], 'appointment' => $appointment, 'userId' => $a['USERID'],
                'status' => $a['STATUS'], 'link' => $a['LINK'], 'obs' => $a['OBS'], 'aux' => $a['AUX']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $array,
        ])->withHeader('Content-Type','application/json');
    }
}