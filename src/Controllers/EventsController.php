<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\Events;
use App\Models\Entities\EventsRegistrations;
use App\Models\Entities\EventsTransactions;
use App\Models\Entities\EventsQuestions;
use App\Models\Entities\EventsPrices;
use App\Models\Entities\EventsDocuments;
use App\Models\Entities\EventsCoupons;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\Directory;
use App\Models\Entities\VaccineVoucher;
use App\Models\Entities\User;
use App\Models\Entities\EventsPress;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class EventsController extends Controller
{
    public function events(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_VISUALIZACAO, true);
        $directorys = $this->accessEventsDirectory();
        $states = $this->accessEventState();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/events.phtml', 'section' => 'events', 'cities' => $cities,
            'user' => $user, 'states' => $states, 'directorys' => $directorys, 'section' => 'events', 'title' => 'listagem de eventos do NOVO',
            'subMenu' => 'directories'
        ]);
    }

    public function NewEvent(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_CRIACAO_EDICAO, true);
        $states = $this->accessEventState();
        $directorys = $this->accessEventsDirectory();
        $id = $request->getAttribute('route')->getArgument('id');
        $cities = $this->em->getRepository(City::class)->findAll();
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/registerEvent.phtml', 'directories' => $directories,
            'user' => $user, 'cities' => $cities, 'states' => $states, 'prices' => null, 'directorys' => $directorys, 'section' => 'events',
            'type' => 'Live', 'title' => 'Registro de novo Evento', 'subMenu' => 'directories']);
    }

    public function editEvent(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $events = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $responsible = $this->em->getRepository(UserAdmin::class)->findOneBy(['id' => $events->getUserId()]);
        $responsible = $responsible ? $responsible->getName() : '';
        $states = $this->accessEventState();
        $cities = $this->em->getRepository(City::class)->findBy(['state' => $events->getStateId()]);
        $registrations = $this->em->getRepository(EventsRegistrations::class)->findBy(['tbEventos' => $id]);
        $questions = $this->em->getRepository(EventsQuestions::class)->findBy(['tbEventosId' => $id, 'status' => 'ativo']);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/registerEvent.phtml', 'user' => $user, 'responsible' => $responsible,
            'cities' => $cities, 'questions' => $questions, 'states' => $states, 'directories' => $directories, 'section' => 'events', 'events' => $events,
            'subMenu' => 'directories'
        ]);
    }

    public function infomationsEvent(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $events = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $message = [
            'specie' => $events->getSpecie(),
            'type' => $events->getType(),
            'oficial' => $events->getOficial(),
        ];
        $prices = $this->em->getRepository(EventsPrices::class)->findBy(['tbEventosId' => $id, 'status' => 'ativo']);
        $pricesArr = [];
        foreach ($prices as $price) {
            $pricesArr[] = [
                'description' => $price->getDescription(),
                'valueFormated' => $price->getValueFormated(),
                'dateBegin' => $price->getDateBegin()->format('d/m/Y H:i'),
                'dateFinal' => $price->getDateFinal()->format('d/m/Y H:i'),
                'limit' => $price->getLimit(),
                'limitTotal' => $price->getLimitTotal(),
                'publicString' => $price->getPublicString(),

            ];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $message,
            'prices' => $pricesArr
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function SaveEventPaid(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $dataEvent = new Events();
            if ($data['eventId']) {
                $dataEvent = $this->em->getRepository(Events::class)->findOneBy(['id' => $data['eventId']]);
                $data['specie'] = $dataEvent->getSpecie();
                $data['type'] = $dataEvent->getType();
                $data['category'] = $dataEvent->getCategory();
                $data['oficial'] = $dataEvent->getOficial();
            } else {
                switch ($data['specie']) {
                    case 1:
                    case 6:
                        $data['category'] = 1;
                        $data['oficial'] = 1;
                        $data['type'] = 'pago';
                        $data['responsible-affiliate'] = null;
                        $data['link'] = null;
                        break;
                    case 2:
                    case 4:
                        $data['category'] = 4;
                        $data['type'] = 'gratuito';
                        $data['link'] = null;
                        break;
                    case 3:
                    case 5:
                        $data['link'] = null;
                        $data['category'] = 4;
                        if ($data['type'] != 'pago') $data['category'] = 1;
                        $data['responsible-affiliate'] = null;
                        break;
                    default:
                        $data['category'] = $data['oficial'] = $data['type'] = $data['oficial'] = $data['responsible-affiliate'] = $data['link'] = null;
                        break;
                }
            }

            $fields = [
                'specie' => 'Modelo',
                'type' => 'Tipo',
                'status' => 'Status',
                'category' => 'Categoria',
                'directory' => 'Diretório',
                'ufId' => 'Estado',
                'cityId' => 'Cidade',
                'name' => 'Name',
                'description' => 'Descrição',
                'datebegin' => 'Data de inicio',
                'datefinal' => 'Data final',
                'limit' => 'Limite Inscritos',
                'public' => 'Público',
            ];
            Validator::requireValidator($fields, $data);
            if (!isset($data["descriptionValue"]) && in_array($data['specie'], [1, 3, 5, 6, 7])) {
                throw new \Exception("Para este tipo de evento você deve preencher os campos do ingresso acima e Adicionar!");
            }
            if (in_array($data['specie'], array(1, 3, 6))) $data['street'] = $data['local'] = $data['zipCode'] = $data['district'] = $data['complement'] = null;
            $banner = $logo = $informative = null;
            $files = $request->getUploadedFiles();
            if ($data['eventId']) {
                $today = new \Datetime();
                $today = strtotime($today->format('Y-m-d'));
                $begin = $dataEvent->getStartDate();
                $begin->modify('-15 days');
                $begin = strtotime($begin->format('Y-m-d'));
                if ($today >= $begin) {
                    if (!$files['informative']->getClientFilename() && in_array($dataEvent->getSpecie(), array(3, 5)) && $dataEvent->getStatus() != 'ativo' &&
                        $data['status'] == 'ativo' && !$dataEvent->getPathInformative() && $data['type'] == 'pago')
                        throw new \Exception("Para este evento ser ativado, você precisa anexar o comunicado ao Cartório Eleitoral!");
                }
                $this->em->getRepository(EventsPrices::class)->disabledPrices($data['eventId']);
                $banner = $dataEvent->getPathBanner();
                $logo = $dataEvent->getPathLogo();
                $informative = $dataEvent->getPathInformative();
            }
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);
            $banner = $files['path_banner']->getClientFilename() ? $this->saveMedia($files['path_banner']) : $banner;
            $logo = $files['path_logo']->getClientFilename() ? $this->saveMedia($files['path_logo']) : $logo;
            $informative = $files['informative']->getClientFilename() ? $this->saveMedia($files['informative']) : $informative;
            $dataEvent->setSpecie($data['specie'])
                ->setType($data['type'])
                ->setStatus($data['status'])
                ->setCategory($data['category'])
                ->setOficial($data['oficial'] ?? 0)
                ->setShow($data['show'] ?? 0)
                ->setName($data['name'])
                ->setDescription($data['description'])
                ->setAddress($data['street'])
                ->setLocal($data['local'])
                ->setCep($data['zipCode'])
                ->setNeighborhood($data['district'])
                ->setComplement($data['complement'])
                ->setStartDate(\DateTime::createFromFormat('d/m/Y H:i', $data['datebegin']))
                ->setFinishDate(\DateTime::createFromFormat('d/m/Y H:i', $data['datefinal']))
                ->setLimit($data['limit'])
                ->setPublic($data['public'])
                //->setBilletdays($data['billet'] ?? 0)
                ->setBilletdays(0)
                ->setDirectory($directory)
                ->setStateId($this->em->getReference(State::class, $data['ufId']))
                ->setCity($this->em->getReference(City::class, $data['cityId']))
                ->setUserId($user)
                ->setDataCreation(date("Y-m-d H:i"))
                ->setDataEvent(\DateTime::createFromFormat('d/m/Y H:i', $data['datebegin']))
                ->setPathBanner($banner)
                ->setPathLogo($logo)
                ->setPathInformative($informative);
            if (in_array($data['specie'], array(2, 4))) $dataEvent->setResponsibleAffiliate($data['responsible-affiliate']);
            if ($data['specie'] == 3) $dataEvent->setLinkOnline($data['link']);
            $this->em->getRepository(Events::class)->save($dataEvent)->getId();
            if (isset($data["descriptionValue"])) {
                $tickets = array_map(null, $data["descriptionValue"], $data["value"], $data['datebeginValue'], $data['datefinalValue'], $data['amount'], $data['range'], $data['publicValue']);
                foreach ($tickets as $ticket) {
                    $publicValue = $ticket[6] == 'Valor para filiados' ? 1 : 0;
                    $ticketValue = str_replace('.', '', $ticket[1]);
                    $ticketValue = str_replace(',', '.', $ticketValue);
                    $DataEventsPrices = new EventsPrices();
                    $DataEventsPrices->setTbEventId($dataEvent->getId())
                        ->setDescription($ticket[0])
                        ->setValue($ticketValue)
                        ->setDateBegin(\DateTime::createFromFormat('d/m/Y H:i', $ticket[2]))
                        ->setdateFinal(\DateTime::createFromFormat('d/m/Y H:i', $ticket[3]))
                        ->setLimit($ticket[4])
                        ->setLimitTotal($ticket[5])
                        ->setPublic($publicValue)
                        ->setStatus('ativo');
                    $this->em->getRepository(EventsPrices::class)->save($DataEventsPrices);
                }
            }
            if ($files['informative']->getClientFilename()) {
                $document = new EventsDocuments();
                $document->seCreated(new \Datetime)->setUser($user)->setEvent($dataEvent)->setType(2)->setName($informative);
                $this->em->getRepository(EventsDocuments::class)->save($document);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Evento salvo com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveMedia($file)
    {
        $extension = explode('.', $file->getClientFilename());
        if ($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
        $target = time() . '.' . end($extension);
        $file->moveTo("uploads/eventos/" . $target);
        return $target;
    }

    public function checkin(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $events = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $tickets = $this->em->getRepository(EventsPrices::class)->getTickets($id);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/checkin.phtml', 'tickets' => $tickets,
            'user' => $user, 'section' => 'events', 'events' => $events, 'title' => 'Tela de Check-in - Evento ' . $events->getName(),
            'subMenu' => 'directories'
        ]);
    }

    public function registrations(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_VISUALIZACAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $events = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $valuesPending = $this->em->getRepository(EventsRegistrations::class)->getvalues($id, 'pendente');
        $valuesConfirmed = $this->em->getRepository(EventsRegistrations::class)->getvalues($id, 'confirmado');
        $tickets = $this->em->getRepository(EventsPrices::class)->getTickets($id);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/participants.phtml', 'tickets' => $tickets, 'user' => $user,
            'valuesPending' => $valuesPending, 'valuesConfirmed' => $valuesConfirmed, 'section' => 'events', 'events' => $events,
            'title' => 'Relatório de participantes do evento - ' . $events->getName(), 'subMenu' => 'directories']);
    }

    public function transactions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $events = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $transactions = $this->em->getRepository(EventsTransactions::class)->findBy(['tbEventosId' => $id]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/transactions.phtml',
            'user' => $user, 'section' => 'events', 'events' => $events, 'transactions' => $transactions, 'title' => 'Relatório de transações']);
    }

    public function CancelEvents(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $event->setStatus('cancelado');
        $this->em->getRepository(Events::class)->save($event);
        return $response->withJson([
            'status' => 'ok',
            'message' => 'Evento cancelado com sucesso!',
        ], 201)
            ->withHeader('Content-type', 'application/json');
    }

    public function doCheckin(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $checkin = $this->em->getRepository(EventsRegistrations::class)->findOneBy(['id' => $id]);
            if ($checkin->getDataCheckin() != null) throw new \Exception("Checkin já realizado para " . $checkin->getNome());
            $checkin->setDataCheckin(new \Datetime);
            $this->em->getRepository(EventsRegistrations::class)->save($checkin);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Checkin realizado com sucesso!',
            ], 201)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function accessEventsDirectory()
    {
        $user = $this->getLogged();
        $directorys = $this->em->getRepository(Directory::class)->findBy(['ativo' => 'S'], ['nome' => 'ASC']);
        if ($user->getLevel() != 3) $directorys = $this->em->getRepository(Directory::class)->findBy(['tbEstadoId' => $user->getState(), 'ativo' => 'S'], ['nome' => 'ASC']);
        return $directorys;
    }

    private function accessEventState()
    {
        $user = $this->getLogged();
        $location = $this->em->getRepository(State::class)->findAll();
        if ($user->getLevel() != 3) $location = $this->em->getRepository(State::class)->findBy(['id' => $user->getState()]);
        return $location;
    }

    public function validadeVoucher(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $validateVoucher = $this->em->getRepository(VaccineVoucher::class)->findOneBy(['user' => $id]);
            if (!$validateVoucher) throw new \Exception("Comprovante ainda não anexado!");
            $validateVoucher->setValidation(1);
            $this->em->getRepository(VaccineVoucher::class)->save($validateVoucher);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Comprovante validado com sucesso!',
            ], 201)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function NewParticipant(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'eventId' => 'Evento',
                'cpf' => 'CPF',
                'name' => 'Nome',
                'email' => 'E-mail',
                'phone' => 'Telefone'
            ];
            Validator::requireValidator($fields, $data);
            $event = $this->em->getRepository(Events::class)->find($data['eventId']);
            if (!$event) throw new \Exception("Solicitação Inválida!");
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);
            $participant = new EventsRegistrations();
            $participant->setTbEventos($event)
                ->setNome($data['name'])
                ->setEmail($data['email'])
                ->setCpf($data['cpf'])
                ->setCode(strtoupper(substr(bin2hex(random_bytes(4)), 1)))
                ->setTbEventosPrecosId(0)
                ->setStatus('confirmado')
                ->setData(new \Datetime());
            if ($user) $participant->setTbPessoaId($user);
            $this->em->getRepository(EventsRegistrations::class)->save($participant);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Participante inserido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function documents(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $id]);
        $documents = $this->em->getRepository(EventsDocuments::class)->findBy(['event' => $id]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/documents.phtml', 'subMenu' => 'directories',
            'user' => $user, 'section' => 'events', 'event' => $event, 'documents' => $documents, 'title' => 'Documentos do evento']);
    }

    public function saveDocuments(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $logged = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['type' => 'Tipo'];
            Validator::requireValidator($fields, $data);
            $files = $request->getUploadedFiles();
            $fileName = $this->saveMedia($files['doc']);
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $data['event']]);
            $document = new EventsDocuments();
            $document->seCreated(new \Datetime)
                ->setUser($logged)
                ->setEvent($event)
                ->setType($data['type'])
                ->setDescription($data['description'])
                ->setName($fileName);
            $this->em->getRepository(EventsDocuments::class)->save($document);
            if ($data['type'] == 2) {
                $event->setPathInformative($fileName);
                $this->em->getRepository(Events::class)->save($event);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function coupons(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $coupons = $this->em->getRepository(EventsCoupons::class)->findBy(['event' => $id]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/coupons.phtml', 'event' => $id,
            'coupons' => $coupons, 'user' => $user, 'section' => 'events', 'title' => 'Cupons do evento', 'subMenu' => 'directories']);
    }

    public function saveCoupons(Request $request, Response $response)
    {
        try {
            $logged = $this->getLogged();
            $date = new \Datetime();
            $data = (array)$request->getParams();
            $fields = [
                'eventId' => 'Evento',
                'name' => 'Nome',
                'code' => 'Código',
                'status' => 'Status',
                'type' => 'Tipo',
                'max' => 'Limite',
                'minPerson' => 'Limite Min por Pessoa',
                'maxPerson' => 'Limite Max por pessoa'
            ];
            if ($data['type'] == 1) {
                $fields = array_merge($fields, ['porcent' => 'Porcentagem']);
            } else {
                $fields = array_merge($fields, ['value' => 'Valor']);
            }
            Validator::requireValidator($fields, $data);
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $data['eventId']]);
            if (!$event) throw new \Exception("Evento não encontrado!");
            $coupons = new EventsCoupons();
            $coupons->setName($data['name'])
                ->setCode($data['code'])
                ->setEvent($event)
                ->setUser($logged)
                ->setStatus($data['status'])
                ->setType($data['type'])
                ->setMax($data['max'])
                ->setMinPerPerson($data['minPerson'])
                ->setMaxPerPerson($data['maxPerson'])
                ->setDateBegin(new \Datetime($data['dateBegin']));
            if ($data['dateFinal']) $coupons->setDateFinal(new \Datetime($data['dateFinal']));
            $data['value'] = str_replace('.', '', $data['value']);
            $data['value'] = str_replace(',', '.', $data['value']);
            $data['value'] = str_replace('R$', '', $data['value']);
            $data['value'] = (float)trim($data['value']);
            match ($data['type']) {
                '1' => $coupons->setPorcent($data['porcent']),
                '2' => $coupons->setValue(floatVal($data['value']))
            };
            $this->em->getRepository(EventsCoupons::class)->save($coupons);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cupom salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeCoupons(Request $request, Response $response)
    {
        try {
            $logged = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $status = $request->getAttribute('route')->getArgument('status');
            $coupon = $this->em->getRepository(EventsCoupons::class)->findOneBy(['id' => $id]);
            $coupon->setStatus($status);
            $this->em->getRepository(EventsCoupons::class)->save($coupon);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Alteração salva com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function checkCoupon(Request $request, Response $response)
    {
        try {
            $today = new \Datetime();
            $coupon = $request->getAttribute('route')->getArgument('coupon');
            $event = $request->getAttribute('route')->getArgument('event');
            $coupon = $this->em->getRepository(EventsCoupons::class)->findOneBy(['code' => $coupon, 'event' => $event, 'status' => 1]);
            if (!$coupon) throw new \Exception("Cupom não encontrado!");
            if ($coupon->getStatus() != 1) throw new \Exception("Este cupom não está mais disponível!");
            if ($coupon->getDateBegin() > $today) throw new \Exception("Este cupom ainda não está diponível!");
            $countCuponsUseds = $this->em->getRepository(EventsTransactions::class)->findBy(['coupon' => $coupon]);
            if (count($countCuponsUseds) >= $coupon->getMax()) throw new \Exception("Este cupom não pode mais ser usado!");
            $array = [
                'type' => $coupon->getType(),
                'value' => $coupon->getType() == 1 ? $coupon->getPorcent() : $coupon->getValue()
            ];
            return $response->withJson([
                'status' => 'ok',
                'discount' => $array,
                'message' => 'Cupom validado com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function press(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $press = $this->em->getRepository(EventsPress::class)->findBy(['eventsId' => $id]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/press.phtml', 'event' => $id,
            'press' => $press, 'user' => $user, 'section' => 'events', 'title' => 'Inscrições Imprensa']);
    }

    public function allRegistrations(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $events = $this->em->getRepository(Events::class)->list($user);
        $states = $this->em->getRepository(State::class)->findAll();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_EVENTOS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/allRegistrations.phtml', 'events' => $events,
            'user' => $user, 'section' => 'events', 'title' => 'Listagem Geral de Participantes', 'states' => $states, 'subMenu' => 'directories']);
    }
}