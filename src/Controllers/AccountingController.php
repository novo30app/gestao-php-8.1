<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Models\Entities\ChartOfAccounts;
use App\Models\Entities\AccountingRecord;
use App\Models\Entities\AccountingAccounts;
use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\Directory;
use App\Models\Entities\SystemFeatures;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class AccountingController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_CRIACAO_EDICAO, true);
        $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($request->getAttribute('route')->getArgument('id'));
        if (!$requeriment) throw new \Exception("Pagamento não encontrado");
        if (in_array($requeriment->getRequestStatus(), [3, 7])) $this->registerAccountRecord($user, $requeriment);
        $accounts = $this->em->getRepository(ChartOfAccounts::class)->findBy(['category' => 2]);
        $accountingRecord = $this->em->getRepository(AccountingRecord::class)->findOneBy(['payment' => $requeriment, 'formAccounting' => 1]);
        $accountingRecord2 = $this->em->getRepository(AccountingRecord::class)->findOneBy(['payment' => $requeriment, 'formAccounting' => 2]);
        $accountingRecordForms = $this->em->getRepository(AccountingRecord::class)->getAccounts($requeriment->getId());
        $group = array();
        foreach ($accountingRecordForms as $value) {
            $group[$value["formAccounting"]][] = $value;
        }
        $accountsForm1 = $accountsForm2 = $arr1 = $arr2 = [];
        foreach ($group as $key => $values) {
            if ($key == 1) {
                if ($accountingRecord) {
                    foreach ($values as $v) {
                        if ($v['debit'] == 'M' && $v['credit'] == 'M') continue;
                        $arr1 = [
                            'debit' => $v['debit'],
                            'credit' => $v['credit'],
                            'value' => $v['value']
                        ];
                        array_push($accountsForm1, $arr1);
                    }
                }
            } else {
                if ($accountingRecord2) {
                    foreach ($values as $v) {
                        if ($v['debit'] == 'M' && $v['credit'] == 'M') continue;
                        $arr2 = [
                            'debit' => $v['debit'],
                            'credit' => $v['credit'],
                            'value' => $v['value']
                        ];
                        array_push($accountsForm2, $arr2);
                    }
                }
            }
        }
        $comment2 = '';
        if ($requeriment->getType() != 16 && $requeriment->getTypeDocument() != 4 && !preg_match("/Pró-Labore/", $requeriment->getDescription()) &&
            !preg_match("/Salário/", $requeriment->getDescription()) && !preg_match("/Vale Transporte/", $requeriment->getDescription())) {
            if ($accountingRecord) $comment2 = 'Pagamento - ' . $accountingRecord->getComment();
            if ($accountingRecord2) $comment2 = $accountingRecord2->getComment();
        }
        if ($requeriment->getType() == 16) {
            if ($accountingRecord) $comment2 = "Transferência para {$requeriment->getDirectoryTransfer()->getName()} - {$requeriment->getDirectoryTransfer()->getCnpj()}";
            if ($accountingRecord2) $comment2 = "Transferência para {$requeriment->getDirectoryTransfer()->getName()} - {$requeriment->getDirectoryTransfer()->getCnpj()}";
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/accounting.phtml', 'subMenu' => 'financial', 'user' => $user,
            'section' => 'billsToPay', 'requeriment' => $requeriment, 'accountingRecord' => $accountingRecord, 'accountingRecord2' => $accountingRecord2,
            'accounts' => $accounts, 'accountsForm1' => $accountsForm1, 'accountsForm2' => $accountsForm2, 'comment2' => $comment2
        ]);
    }

    public function accountingRegister(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            if ($data['formAccountingForm2']) {
                $data['requerimentId'] = $data['requerimentIdForm2'];
                $data['formAccounting'] = $data['formAccountingForm2'];
                $data['accountingStatus'] = $data['accountingStatusForm2'];
                $data['accountingValue'] = $data['accountingValueForm2'];
                $data['competitionDate'] = $data['competitionDateForm2'];
                $data['accountingComment'] = $data['accountingCommentForm2'];
                $data['accountingDebit'] = $data['accountingDebitForm2'];
                $data['valueAccountingDebit'] = $data['valueAccountingDebitForm2'];
                $data['accountingCredit'] = $data['accountingCreditForm2'];
                $data['valueAccountingCredit'] = $data['valueAccountingCreditForm2'];
                $data['typeAccounting'] = $data['typeAccountingForm2'];
                $data['typeFiscal'] = $data['typeFiscalForm2'];
                $data['multiple'] = $data['multipleForm2'];
                $data['accountingMainM'] = $data['accountingMainMForm2'];
                $data['valueM'] = $data['valueMForm2'];
                $data['accounting'] = $data['accountingForm2'];
                $data['valueAccounting'] = $data['valueAccountingForm2'];
                if ($data['multipleForm2'] == 3) {
                    $data['accountingMainM'] = $data['accountingMainCreditMForm2'];
                    $data['valueM'] = $data['valueCreditMForm2'];
                }
            } else {
                if ($data['multiple'] == 3) {
                    $data['accountingMainM'] = $data['accountingMainCreditM'];
                    $data['valueM'] = $data['valueCreditM'];
                }
            }
            $fields = [
                'accountingStatus' => 'Status',
                'accountingValue' => 'Valor',
                'competitionDate' => 'Data da Competência',
                'accountingComment' => 'Comentário'
            ];
            Validator::requireValidator($fields, $data);
            if (!$data['typeFiscal'] && !$data['typeAccounting']) throw new \Exception("Você precisa selecionar se o registro é Contábil ou Fiscal!");
            $accountingValue = str_replace('.', '', $data['accountingValue']);
            $accountingValue = str_replace(',', '.', $accountingValue);
            $requeriment = $this->em->getRepository(PaymentRequirements::class)->findOneBy(['id' => $data['requerimentId']]);
            if (!$requeriment) throw new \Exception("Solicitação inválida!");
            $accountingRecord = $this->em->getRepository(AccountingRecord::class)->findOneBy(['payment' => $data['requerimentId'], 'formAccounting' => $data['formAccounting']]);
            if (!$accountingRecord) $accountingRecord = new AccountingRecord();
            $accountingRecord->setUser($user)
                ->setPayment($requeriment)
                ->setStatus($data['accountingStatus'])
                ->setValue((float)$accountingValue)
                ->setCompetition(new \Datetime($data['competitionDate']))
                ->setComment($data['accountingComment'])
                ->setAccounting($data['typeAccounting'] ?? 0)
                ->setFiscal($data['typeFiscal'] ?? 0)
                ->setFormAccounting($data['formAccounting']);
            $this->em->getRepository(AccountingRecord::class)->save($accountingRecord);
            if ($data['formAccounting'] == 1) {
                $accountingRecordForm2 = $this->em->getRepository(AccountingRecord::class)->findOneBy(['payment' => $data['requerimentId'], 'formAccounting' => 2]);
                if ($accountingRecordForm2) {
                    $comment2 = 'Pagamento - ' . $accountingRecord->getComment();
                    if ($requeriment->getType() == 16) {
                        $comment2 = "Transferência para {$requeriment->getDirectoryTransfer()->getName()} - {$requeriment->getDirectoryTransfer()->getCnpj()}";
                    }
                    $accountingRecordForm2->setComment($comment2);
                    $this->em->getRepository(AccountingRecord::class)->save($accountingRecordForm2);
                }
            }
            if ($data['multiple']) {
                $this->em->getRepository(AccountingAccounts::class)->deleteAccounts($accountingRecord);
                if ($data['multiple'] == 1) {
                    $accounts = array_map(null, $data['accountingDebit'], $data['valueAccountingDebit'], $data['accountingCredit'], $data['valueAccountingCredit']);
                    foreach ($accounts as $a) {
                        /** Conta Débito */
                        $debitValue = explode('/', $a[0]);
                        $debitAccount = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($debitValue[0])]);
                        if (!$debitAccount) throw new \Exception("Conta Débito não encontrada, realize o cadastro dessa conta na tela Plano de Contas!");
                        $debitValue = str_replace('.', '', $a[1]);
                        $debitValue = str_replace(',', '.', $debitValue);
                        /** Conta Crédito */
                        $creditValue = explode('/', $a[2]);
                        $creditAccount = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($creditValue[0])]);
                        if (!$creditAccount) throw new \Exception("Conta Crédito não encontrada, realize o cadastro dessa conta na tela Plano de Contas!");
                        $creditValue = str_replace('.', '', $a[3]);
                        $creditValue = str_replace(',', '.', $creditValue);
                        /** Registro */
                        $accountsDebit = new AccountingAccounts();
                        $accountsDebit->setAccountingRecord($accountingRecord)
                            ->setAccount($debitAccount->getId())
                            ->setValue($debitValue)
                            ->setCreditAccount($creditAccount->getId())
                            ->setCreditValue($creditValue);
                        $this->em->getRepository(AccountingAccounts::class)->save($accountsDebit);
                    }
                } else {
                    if (!$data['valueM'] || !$data['accountingMainM']) throw new \Exception("Verifique se você digitou todos os campos da conta Multiplo.");
                    $accountingMainM = explode('/', $data['accountingMainM']);
                    $accountingMainM = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($accountingMainM[0])]);
                    $valueM = str_replace('.', '', $data['valueM']);
                    $valueM = str_replace(',', '.', $valueM);
                    $accounts = new AccountingAccounts();
                    $accounts->setAccountingRecord($accountingRecord);
                    match ((int)$data['multiple']) {
                        2 => $accounts->setCreditAccount($accountingMainM->getId())->setCreditValue($valueM),
                        3 => $accounts->setAccount($accountingMainM->getId())->setValue($valueM),
                    };
                    $this->em->getRepository(AccountingAccounts::class)->save($accounts);
                    $accounts = array_map(null, $data['accounting'], $data['valueAccounting']);
                    $i = 0;
                    foreach ($accounts as $a) {
                        if (!$a[1] || !$a[0]) throw new \Exception("Verifique se você digitou todos os campos das contas Multiplas.");
                        $accounting = explode('/', $a[0]);
                        $accounting = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($accounting[0])]);
                        if (!$accounting) throw new \Exception("Conta não encontrada, realize o cadastro dessa conta na tela Plano de Contas!");
                        $value = str_replace('.', '', $a[1]);
                        $value = str_replace(',', '.', $value);
                        $accounts = new AccountingAccounts();
                        $accounts->setAccountingRecord($accountingRecord);
                        match ((int)$data['multiple']) {
                            2 => $accounts->setCreditAccount($accountingMainM->getId())->setCreditValue($value)->setAccount($accounting->getId())->setValue($value),
                            3 => $accounts->setAccount($accountingMainM->getId())->setValue($value)->setCreditAccount($accounting->getId())->setCreditValue($value),
                        };
                        $this->em->getRepository(AccountingAccounts::class)->save($accounts);
                        $i++;
                    }
                }
            }
            if ($data['accountingStatus'] == 2) {
                $msg = Messages::accountingRegisterPaymentReproved($data['requerimentId']);
                $subject = "Solicitação de pagamento recusada pela contabilidade";
                Email::send('financeiro@novo.org.br', 'Departamento Financeiro', $subject, $msg);
            }
            $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_ACCOUNTING);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registros salvos com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function exportAccountReport(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_EXPORTACAO, true);
        $id = $request->getQueryParam('id');
        $dateType = $request->getQueryParam('dateType');
        $dateBegin = $request->getQueryParam('dateBegin');
        $dateEnd = $request->getQueryParam('dateEnd');
        $accounting = $request->getQueryParam('accounting');
        $status = intVal($request->getQueryParam('status'));
        $origin = $request->getQueryParam('origin');
        $cpfCnpj = $request->getQueryParam('cpfCnpj');
        $provider = $request->getQueryParam('provider');
        $type = $request->getQueryParam('type');
        $docNumber = $request->getQueryParam('docNumber');
        $costCenter = $request->getQueryParam('costCenter');
        $user = $request->getQueryParam('user');
        $directory = $request->getQueryParam('directory');
        $directoryPaying = $request->getQueryParam('directoryPaying');
        $value = $request->getQueryParam('value');
        $paymentValue = $request->getQueryParam('paymentValue');
        $list = $this->em->getRepository(PaymentRequirements::class)->exportAccountReport($userAdmin, $id, $dateType, $dateBegin, $dateEnd, $accounting,
            $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $user, $directory, $directoryPaying, $value, $paymentValue);
        $filename = "Relatório Contábil Contas a Pagar - " . date('d-m-Y') . ".csv";
        $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $directory]);
        if ($directory) $filename = $directory->getDirectorySheetName() ? $directory->getDirectorySheetName() . ".csv" : $directory;
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['CNPJ Prestador', 'Nome/Razão Social Fornecedor', 'Esfera Partidária', 'Data Emissão/Contratação', 'Tipo Documento', 'Descrição Gasto',
            'Descrição Outros', 'Lançamento', 'Data', 'Débito', 'Crédito', 'Valor', 'Histórico Padrão', 'Complemento', 'CCDB', 'CCCR', 'CNPJ', 'Status Contábil', 'N° NF',
            'Multa', 'Juros', 'INSS', 'IRRF', 'CSLL', 'COFINS', 'PIS/PASEP', 'Nome Conta Contábil', 'Nome Conta SPCA', 'Conta reduzida', 'Descrição do Documento', 'Série do Documento', 'Quantidade/ itens da nota',
            'Fonte de Recurso', 'N° de documento do Pagamento', 'Conta Bancária', 'Forma de pagamento', 'Tipo de Transferência', 'Data de pagamento', 'Valor de pagamento'
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportFiscalReport(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_EXPORTACAO, true);
        $id = $request->getQueryParam('id');
        $dateType = $request->getQueryParam('dateType');
        $dateBegin = $request->getQueryParam('dateBegin');
        $dateEnd = $request->getQueryParam('dateEnd');
        $accounting = $request->getQueryParam('accounting');
        $status = intVal($request->getQueryParam('status'));
        $origin = $request->getQueryParam('origin');
        $cpfCnpj = $request->getQueryParam('cpfCnpj');
        $provider = $request->getQueryParam('provider');
        $type = $request->getQueryParam('type');
        $docNumber = $request->getQueryParam('docNumber');
        $costCenter = $request->getQueryParam('costCenter');
        $user = $request->getQueryParam('user');
        $directory = $request->getQueryParam('directory');
        $directoryPaying = $request->getQueryParam('directoryPaying');
        $value = $request->getQueryParam('value');
        $paymentValue = $request->getQueryParam('paymentValue');
        $list = $this->em->getRepository(PaymentRequirements::class)->exportFiscalReport($userAdmin, $id, $dateType, $dateBegin, $dateEnd, $accounting,
            $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $user, $directory, $directoryPaying, $value, $paymentValue);
        $filename = "Relatório Fiscal Contas a Pagar - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Tipo de Registro', 'Nº NFS-e', 'Data Hora NFE', 'Código de Verificação da NFS-e', 'Tipo de RPS', 'Série do RPS', 'Número do RPS', 'Data do Fato Gerador',
            'Inscrição Municipal do Prestador', 'Indicador de CPF/CNPJ do Prestador', 'CPF/CNPJ do Prestador', 'Razão Social do Prestador', 'Tipo do Endereço do Prestador',
            'Endereço do Prestador', 'Número do Endereço do Prestador', 'Complemento do Endereço do Prestador', 'Bairro do Prestador', 'Cidade do Prestador', 'UF do Prestador',
            'CEP do Prestador', 'Email do Prestador', 'Opção Pelo Simples', 'Situação da Nota Fiscal', 'Data de Cancelamento', 'Nº da Guia', 'Data de Quitação da Guia Vinculada a Nota Fiscal',
            'Valor dos Serviços', 'Valor das Deduções', 'Código do Serviço Prestado na Nota Fiscal', 'Alíquota', 'ISS devido', 'Valor do Crédito', 'ISS Retido',
            'Indicador de CPF/CNPJ do Tomador', 'CPF/CNPJ do Tomador', 'Inscrição Municipal do Tomador', 'Inscrição Estadual do Tomador', 'Razão Social do Tomador',
            'Tipo do Endereço do Tomador', 'Endereço do Tomador', 'Número do Endereço do Tomador', 'Complemento do Endereço do Tomador', 'Bairro do Tomador',
            'Cidade do Tomador', 'UF do Tomador', 'CEP do Tomador', 'Email do Tomador', 'Nº NFS-e Substituta', 'ISS recolhido', 'ISS a recolher', 'Indicador de CPF/CNPJ do Intermediário',
            'CPF/CNPJ do Intermediário', 'Inscrição Municipal do Intermediário', 'Razão Social do Intermediário', 'Repasse do Plano de Saúde', 'PIS/PASEP', 'COFINS',
            'INSS', 'IR', 'CSLL', 'Carga tributária: Valor', 'Carga tributária: Porcentagem', 'Carga tributária: Fonte', 'CEI', 'Matrícula da Obra',
            'Município Prestação - cód. IBGE', 'Situação do Aceite', 'Encapsulamento', 'Valor Total Recebido', 'Tipo de Consolidação', 'Nº NFS-e Consolidada', 'Campo Reservado',
            'Discriminação dos Serviços', 'Multa', 'Juros', 'IRRF',];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function deleteRegister(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $register = $this->em->getRepository(AccountingRecord::class)->find($id);
            $register->setFormAccounting(0);
            $this->em->getRepository(AccountingRecord::class)->save($register);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registro removido com sucesso!'
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}