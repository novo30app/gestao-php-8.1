<?php

namespace App\Controllers;

use App\Helpers\Email;
use App\Helpers\Validator;
use App\Models\Commons\Entities\Benefit;
use App\Models\Commons\Entities\Mission;
use App\Models\Commons\Entities\UserPoint;
use App\Models\Entities\Directory;
use App\Models\Entities\SystemFeatures;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UserPointController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_VISUALIZACAO, true);
        $benefits = $this->em->getRepository(Benefit::class)->findBy(['active' => 1, 'deleted_at' => null], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/userPoint.phtml', 'subMenu' => 'affiliateSuccess',
            'subSectionActive' => 'mobilize', 'user' => $user, 'title' => 'Solicitações de Benefícios', 'benefits' => $benefits,
            'subSection' => 'mobilize-request-benefits'
        ]);
    }

    public function missions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_VISUALIZACAO, true);
        $missions = $this->em->getRepository(Mission::class)->findBy(['deleted_at' => null]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/missions-completes.phtml', 'subMenu' => 'affiliateSuccess',
            'subSectionActive' => 'mobilize', 'user' => $user, 'title' => 'Missões Realizadas', 'missions' => $missions,
            'subSection' => 'mobilize-missions-completes'
        ]);
    }

    public function list(Request $request, Response $response)
    {
        try {
            Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_VISUALIZACAO, true);
            $filter = $request->getQueryParams();
            $index = $request->getQueryParam('index') ?: 0;
            $limit = $request->getQueryParam('limit') ?: 25;
            $result = $this->em->getRepository(UserPoint::class)->list($filter, $limit, $index);
            $total = $this->em->getRepository(UserPoint::class)->listTotal($filter);
            $partial = ($index * $limit) + sizeof($result);
            $partial = $partial <= $total ? $partial : $total;
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
                'total' => (int)$total,
                'partial' => (int)$partial,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeStatus(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $data = (array)$request->getParams();
            $this->em->beginTransaction();

            $result = $this->em->getRepository(UserPoint::class)->changeStatus($data);


            if ($data['status'] == 5) {
                foreach ($data['benefitRequest'] as $item) {
                    $userPoint = $this->em->getRepository(UserPoint::class)->find($item);
                    Email::sendBenefit($userPoint);
                }
            }


            $this->em->commit();

            return $this->responseJson($result, $response);

        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function show(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $data = $this->em->getRepository(UserPoint::class)->find($id);
            return $response->withJson([
                'status' => 'ok',
                'message' => $data->toArray(),
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function export(Request $request, Response $response)
    {
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_EXPORTACAO, true);
        $filter = $request->getQueryParams();

        $results = $this->em->getRepository(UserPoint::class)->list($filter, 1000000, 0);

        $filename = "Relatório de Missões Realizadas - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Usuário', 'Missão', 'Data', 'CPF', 'Status'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($results as $result) {

            fputcsv($out, [
                $result['user'],
                $result['mission'],
                $result['data']['created_at']->format('d/m/Y'),
                $result['cpf'],
                $this->getStatusStr($result['data']['status']),
            ], ';', '"');
        }
        fclose($out);
        exit;
    }

    private function getStatusStr(int $status)
    {
        switch ($status) {
            case 1:
                return 'Aguardando Aprovação';
            case 2:
                return 'Cancelada';
            case 3:
                return 'Realizada';
        }
    }

    public function csv(Request $request, Response $response)
    {
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_EXPORTACAO, true);
        $filter = $request->getQueryParams();
        $results = $this->em->getRepository(UserPoint::class)->list($filter, 1000000, 0);

        $filename = "Relatório de Solicitações de Benefícios - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Usuário', 'Benefício', 'Data', 'CPF', 'Endereço', 'Status'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($results as $result) {
            $address = '';
            if ($result['data']['address'][0]['zipCode']) {
                $complement = $result['data']['address'][0]['complement'] ? " {$result['data']['address'][0]['complement']}," : '';
                $address = "{$result['data']['address'][0]['zipCode']}, {$result['data']['address'][0]['address']}, {$complement} {$result['data']['address'][0]['number']}, {$result['data']['address'][0]['neighborhood']}, {$result['data']['address'][0]['city']}/{$result['data']['address'][0]['uf']}";
            }

            fputcsv($out, [
                $result['user'],
                $result['benefit'],
                $result['data']['created_at']->format('d/m/Y'),
                $result['cpf'],
                $address,
                $this->getStatusStrBenefit($result['data']['status']),
            ], ';', '"');
        }
        fclose($out);
        exit;
    }

    private function getStatusStrBenefit(int $status)
    {
        switch ($status) {
            case 1:
                return 'Aguardando Aprovação';
            case 2:
                return 'Cancelada';
            case 3:
                return 'Em Preparo';
            case 4:
                return 'A Caminho';
            case 5:
                return 'Entregue';
        }
    }

}
