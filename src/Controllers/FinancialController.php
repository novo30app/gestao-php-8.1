<?php

namespace App\Controllers;

use App\Helpers\Session;
use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Helpers\Messages;
use App\Models\Entities\Address;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Area;
use App\Models\Entities\Bank;
use App\Models\Entities\Budget;
use App\Models\Entities\BudgetValues;
use App\Models\Entities\Contract;
use App\Models\Entities\CostCenter;
use App\Models\Entities\CredentialsBB;
use App\Models\Entities\Events;
use App\Models\Entities\ExtractBB;
use App\Models\Entities\LogApproved;
use App\Models\Entities\OfxFiles;
use App\Models\Entities\OfxLinePaymentRequirement;
use App\Models\Entities\OfxLines;
use App\Models\Entities\OriginatingOwner;
use App\Models\Entities\PaymentRequerimentsApportionment;
use App\Models\Entities\PaymentRequerimentsTravelPackage;
use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\PaymentRequirementsFiles;
use App\Models\Entities\Provider;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\SubArea;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\PersonRede;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\Directory;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\ChartOfAccounts;
use App\Models\Entities\Country;
use App\Models\Entities\CivilState;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\DirectoryAccounts;
use App\Models\Entities\TransferSplit;
use App\Models\Entities\OriginatingTransaction;
use App\Models\Entities\AccountingRecord;
use App\Models\Entities\AccountingAccounts;
use App\Models\Entities\CnpjGru;
use App\Models\Entities\Mesoregions;
use App\Models\Entities\IuguTokens;
use App\Models\Entities\DirectoryExtract;
use App\Models\Entities\Autonomous;
use App\Services\Email;
use App\Services\IuguService;
use App\Services\PDF;
use App\Services\MaxiPago;
use App\Services\BancoDoBrasil;
use App\Services\Hubspot;
use Exception;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class FinancialController extends Controller
{
    public function defaulters(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_ADIMP_INADIMP_VISUALIZACAO, true);
        $states = $this->accessEventState();
        $this->newAccessLog($user, 'defaulters');
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - defaulters']);
        $mesoregion = $this->em->getRepository(Mesoregions::class)->getMyMesos($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/defaulters.phtml', 'section' => 'defaulters',
            'user' => $user, 'subMenu' => 'search', 'states' => $states, 'cities' => $cities, 'term' => $term, 'mesoregion' => $mesoregion]);
    }

    public function billsToPay(Request $request, Response $response, int $type = 2)
    {
        $user = $this->getLogged(false, $type == 3);
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $title = $type == 2 ? 'Contas a Pagar' : 'Pagamentos';
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $users = $this->em->getRepository(PaymentRequirements::class)->getUsers();
        $providers = $this->em->getRepository(Provider::class)->findAll();
        $costCenter = $this->em->getRepository(CostCenter::class)->findAll();
        $areas = $this->em->getRepository(Area::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/billsToPay.phtml', 'section' => 'billsToPay', 'users' => $users,
            'costCenter' => $costCenter, 'user' => $user, 'subMenu' => 'financial', 'title' => $title, 'states' => $states, 'type' => $type,
            'directories' => $directories, 'providers' => $providers, 'areas' => $areas]);
    }

    public function billsToReceive(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->havePermissionBillsToPay()) {
            $this->redirectByPermissions();
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/billsToReceive.phtml', 'section' => 'billsToReceive',
            'user' => $user, 'subMenu' => 'financial', 'title' => 'Contas a Receber']);
    }

    public function approved(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_APROVACOESPENDENTES_VISUALIZACAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $users = $this->em->getRepository(PaymentRequirements::class)->getUsers();
        $costCenter = $this->em->getRepository(CostCenter::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/approved.phtml', 'section' => 'approved',
            'user' => $user, 'subMenu' => 'financial', 'title' => 'Aprovações Pendentes', 'directories' => $directories,
            'users' => $users, 'costCenter' => $costCenter]);
    }

    public function addPayments(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_CRIACAO_EDICAO, true);
        $banks = $this->em->getRepository(Bank::class)->findBy([], ['cod' => 'asc']);
        $events = $this->getEventsByLocalAndLevel($user);
        $allDirectories = $this->em->getRepository(Directory::class)->findBy(['ativo' => 'S']);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $costCenters = $this->em->getRepository(CostCenter::class)->findBy(['status' => 1], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/paymentRequirement.phtml', 'section' => 'billsToPay', 'allDirectories' => $allDirectories,
            'user' => $user, 'subMenu' => 'financial', 'title' => 'Novo Pagamento', 'banks' => $banks, 'events' => $events, 'directories' => $directories, 'costCenters' => $costCenters]);
    }

    private function getEventsByLocalAndLevel(UserAdmin $user)
    {
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) {
            return $this->em->getRepository(Events::class)->findBy(['cidadeId' => $user->getCity()->getId()], ['id' => 'DESC']);
        }
        if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            return $this->em->getRepository(Events::class)->findBy(['estadoId' => $user->getState()->getId()], ['id' => 'DESC']);
        }
        return $this->em->getRepository(Events::class)->findBy([], ['id' => 'DESC']);
    }

    private function moveAttachmentsContractPayment($files, PaymentRequirements $requeriment): PaymentRequirements
    {
        $folder = UPLOAD_FOLDER . 'financeiro/pagamentos/';
        $attachmentContract = $files['attachmentContract'];
        $campaignMaterial = $files['campaignMaterial'];
        $attachmentNf = $files['attachmentNf'];
        $attachmentBillet = $files['attachmentBillet'];
        $attachmentPayment = $files['attachmentPayment'];
        $beneficiaryFile = $files['beneficiaryFile'];
        $accommodationBeneficiaryFile = $files['accommodationBeneficiaryFile'];
        $thrustFile = $files['thrustFile'];
        $serviceDeliveryReport = $files['serviceDeliveryReport'];
        if ($attachmentBillet && $attachmentBillet->getClientFilename()) {
            $time = time();
            $extension = explode('.', $attachmentBillet->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}attachmentBillet.{$extension}";
            $attachmentBillet->moveTo($target);
            $requeriment->setAttachmentBillet($target);
        }
        if ($campaignMaterial && $campaignMaterial->getClientFilename()) {
            $time = time();
            $extension = explode('.', $campaignMaterial->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}campaignMaterial.{$extension}";
            $campaignMaterial->moveTo($target);
            $requeriment->setCampaignMaterial($target);
        }
        if ($attachmentContract && $attachmentContract->getClientFilename()) {
            $time = time();
            $extension = explode('.', $attachmentContract->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}attachmentContract.{$extension}";
            $attachmentContract->moveTo($target);
            $requeriment->setAttachmentContract($target);
        }
        if ($attachmentNf && $attachmentNf->getClientFilename()) {
            $time = time();
            $extension = explode('.', $attachmentNf->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}attachmentNf.{$extension}";
            $attachmentNf->moveTo($target);
            $requeriment->setAttachmentNf($target);
        }
        if ($attachmentPayment && $attachmentPayment->getClientFilename()) {
            $time = time();
            $extension = explode('.', $attachmentPayment->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}attachmentPayment.{$extension}";
            $attachmentPayment->moveTo($target);
            $requeriment->setAttachmentPayment($target);
        }
        if ($beneficiaryFile && $beneficiaryFile->getClientFilename()) {
            $time = time();
            $extension = explode('.', $beneficiaryFile->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}beneficiaryFile.{$extension}";
            $beneficiaryFile->moveTo($target);
            $requeriment->setBeneficiaryFile($target);
        }
        if ($accommodationBeneficiaryFile && $accommodationBeneficiaryFile->getClientFilename()) {
            $time = time();
            $extension = explode('.', $accommodationBeneficiaryFile->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}accommodationBeneficiaryFile.{$extension}";
            $accommodationBeneficiaryFile->moveTo($target);
            $requeriment->setBeneficiaryFile($target);
        }
        if ($thrustFile && $thrustFile->getClientFilename()) {
            $time = time();
            $extension = explode('.', $thrustFile->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}thrustFile.{$extension}";
            $thrustFile->moveTo($target);
            $requeriment->setThrustFile($target);
        }
        if ($serviceDeliveryReport && $serviceDeliveryReport->getClientFilename()) {
            $time = time();
            $extension = explode('.', $serviceDeliveryReport->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}serviceDeliveryReport.{$extension}";
            $serviceDeliveryReport->moveTo($target);
            $requeriment->setAttachmentServiceReport($target);
        }
        return $requeriment;
    }

    private function moveOtherFilesRequirements(PaymentRequirements $requeriment, array $othersFiles): void
    {
        $folder = UPLOAD_FOLDER . 'financeiro/pagamentos/';
        foreach ($othersFiles['othersFiles'] as $othersFile) {
            if ($othersFile && $othersFile->getClientFilename()) {
                $count++;
                $othersFileTable = new PaymentRequirementsFiles();
                $time = time();
                $extension = explode('.', $othersFile->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}{$othersFile->getClientFilename()}.{$extension}";
                $othersFile->moveTo($target);
                $othersFileTable->setRequirement($requeriment->getId())->setFile($target);
                $this->em->getRepository(PaymentRequirementsFiles::class)->save($othersFileTable);
            }
        }
    }

    public function approvePayment(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            //if ($user->getType() == 1) {
            //    throw new \Exception('Você não tem permissão para realizar está operação');
            //}
            $id = $request->getAttribute('route')->getArgument('id');
            $requeriment = $this->em->getRepository(PaymentRequirements::class)->findOneBy(['id' => $id, 'requestStatus' => 4]);
            if (!$requeriment) {
                throw new \Exception('Solicitação inválida');
            }
            $requeriment->setRequestStatus(0);
            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Solicitação de pagamento aprovada com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function discountsValue($value, $discounts)
    {
        $discounts = Utils::moneyToFloat($discounts);
        if ($discounts > 0) {
            if ($discounts > $value) throw new \Exception("O valor do desconto não pode ser superior ao valor total.");
            $value = $value - $discounts;
        }
        return round($value, 2);
    }

    private function changeMoneyValues(&$data)
    {
        $data['nfValue'] = Utils::moneyToFloat($data['nfValue']);
        $data['taxiValue'] = Utils::moneyToFloat($data['taxiValue']);
        $data['fuelValue'] = Utils::moneyToFloat($data['fuelValue']);
        $data['billAmount'] = Utils::moneyToFloat($data['billAmount']);
        $data['depositAmount'] = Utils::moneyToFloat($data['depositAmount']);
        $data['pixAmount'] = Utils::moneyToFloat($data['pixAmount']);
        $data['specieAmount'] = Utils::moneyToFloat($data['specieAmount']);
    }

    public function registerAccountingRecord(UserAdmin $user, array $data, PaymentRequirements $requeriment): void
    {
        $comment = "Prestação de Serviço - {$requeriment->getProvider()->getName()} - CNPJ {$requeriment->getProvider()->getCpfCnpj()} - NF {$requeriment->getNfNumber()}";

        if ($requeriment->getType() == 16) {
            $comment = "Transferência para {$requeriment->getDirectoryTransfer()->getName()} - {$requeriment->getDirectoryTransfer()->getCnpj()}";
        }

        $formAccounting = 2;
        if ($requeriment->getType() == 16) $formAccounting = 1;
        if ($requeriment->getTypeDocument() == 4) $formAccounting = 1;
        if (preg_match("/Vale Transporte/", $requeriment->getDescription())) $formAccounting = 1;
        if (preg_match("/Pró-Labore/", $requeriment->getDescription())) $formAccounting = 1;
        if (preg_match("/Salário/", $requeriment->getDescription())) $formAccounting = 1;
        $accountingRecord = $this->em->getRepository(AccountingRecord::class)->findOneBy(['payment' => $requeriment, 'formAccounting' => $formAccounting]);

        if ($formAccounting == 2) $comment = "Pagamento - $comment";

        if (!$accountingRecord) $accountingRecord = new AccountingRecord();
        $accountingRecord->setUser($user)
            ->setPayment($requeriment)
            ->setStatus(1)
            ->setValue($requeriment->getPaymentValue())
            ->setCompetition(\DateTime::createFromFormat('d/m/Y', $data['payDay']))
            ->setComment($comment)
            ->setAccounting(1)
            ->setFiscal(0)
            ->setFormAccounting($formAccounting);
        $this->em->getRepository(AccountingRecord::class)->save($accountingRecord);
        $account = $requeriment->getProvider()->getAccountCredit();
        $creditAccount = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['id' => $data['directoryAccount']]);
        if ($creditAccount) $creditAccount = $creditAccount->getAccountingAccount();
        if (preg_match("/Vale Transporte/", $requeriment->getDescription())) {
            $account = $this->em->getReference(ChartOfAccounts::class, 8179);
            $creditAccount = $requeriment->getDirectoryAccount()->getAccountingAccount();
        }
        if (preg_match("/Pró-Labore/", $requeriment->getDescription())) {
            $account = $this->em->getReference(ChartOfAccounts::class, 8248);
            $creditAccount = $requeriment->getDirectoryAccount()->getAccountingAccount();
        }
        if (preg_match("/Salário/", $requeriment->getDescription())) {
            $account = $this->em->getReference(ChartOfAccounts::class, 7751);
            $creditAccount = $requeriment->getDirectoryAccount()->getAccountingAccount();
        }
        $accountingAccounts = $this->em->getRepository(AccountingAccounts::class)->findBy(['accountingRecord' => $accountingRecord]);
        if (!$accountingAccounts && $account && $creditAccount) {
            $accountsAccounts = new AccountingAccounts();
            $accountsAccounts->setAccountingRecord($accountingRecord)
                ->setAccount($account->getId())
                ->setValue($requeriment->getPaymentValue())
                ->setCreditAccount($creditAccount->getId());
            if ($formAccounting == 2) $accountsAccounts->setCreditValue($requeriment->getPaymentValue());
            $this->em->getRepository(AccountingAccounts::class)->save($accountsAccounts);
        }
        $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_ACCOUNTING);
    }

    private function moveAttachmentsAutonomous($files, Autonomous $autonomous): Autonomous
    {
        $folder = UPLOAD_FOLDER . 'financeiro/autonomos/';

        $attachments = [
            'attachmentResidenceAutonomous' => [
                'name' => 'Comprovante de Residência',
                'setter' => 'setAttachmentResidenceAutonomous',
                'column' => $autonomous->getAttachmentResidenceAutonomous()
            ],
            'attachmentRgCnhAutonomous' => [
                'name' => 'Cópia do RG/CNH',
                'setter' => 'setAttachmentRgCnhAutonomous',
                'column' => $autonomous->getAttachmentRgCnhAutonomous()
            ],
        ];

        try {
            // Validação de arquivos obrigatórios
            foreach ($attachments as $fileKey => $config) {
                if ($config['column'] == null && (!isset($files[$fileKey]) || !$files[$fileKey]->getClientFilename())) {
                    throw new \Exception("O arquivo {$config['name']} é obrigatório");
                }
            }

            // Processamento dos arquivos
            foreach ($attachments as $fileKey => $config) {
                if (isset($files[$fileKey]) && $files[$fileKey]->getClientFilename()) {
                    $file = $files[$fileKey];
                    $time = time();
                    $extension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
                    $target = "{$folder}{$time}{$fileKey}.{$extension}";
                    $file->moveTo($target);

                    $setterMethod = $config['setter'];
                    $autonomous->$setterMethod($target);
                }
            }

            return $autonomous;

        } catch (\Exception $e) {
            throw new \Exception("Erro ao processar arquivos: " . $e->getMessage());
        }
    }

    /**
     * Cadastra um novo prestador autônomo
     *
     * @param array $data Dados do autônomo
     * @param PaymentRequirements $requeriment Requisitos de pagamento
     * @return void
     * @throws \Exception Em caso de erro no cadastro
     */
    private function setAutonomous($data, Provider $provider, $files): Autonomous
    {
        try {
            $this->em->beginTransaction();

            $fields = [
                'pisPasepNumber' => 'Número do PIS/PASEP',
                'civilStatus' => 'Situação Civil',
                'instructionLevel' => 'Grau de Instrução',
                'occupation' => 'Cargo a ser Cadastrado'
            ];
            Validator::requireValidator($fields, $data);

            $autonomous = $this->em->getRepository(Autonomous::class)->findOneBy(['provider' => $provider]);
            if (!$autonomous) {
                $autonomous = new Autonomous();
                $autonomous->setCreatedAt(new \DateTime());
            }
            $autonomous->setProvider($provider)
                ->setPisPasep($data['pisPasepNumber'])
                ->setCivilStatus($this->em->getRepository(CivilState::class)->findOneBy(['id' => $data['civilStatus']]))
                ->setInstructionLevel($this->em->getRepository(Schooling::class)->findOneBy(['id' => $data['instructionLevel']]))
                ->setOccupation($data['occupation'])
                ->setUpdatedAt(new \DateTime());
            $autonomous = $this->moveAttachmentsAutonomous($files, $autonomous);

            $this->em->getRepository(Autonomous::class)->save($autonomous);

            $provider->setAutonomous($autonomous);
            $this->em->getRepository(Provider::class)->save($provider);

            $this->em->commit();
            return $autonomous;

        } catch (\Exception $e) {
            $this->em->rollback();
            throw new \Exception("Erro ao cadastrar autônomo: " . $e->getMessage());
        }
    }

    public function savePayment(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();

            $dueDate = match ((int)$data['paymentMethod']) {
                1 => $data['paymentSlipDueDate'],
                2 => $data['depositMaturityDueDate'],
                3 => $data['pixDueDate'],
                4 => $data['specieDueDate'],
                default => $data['depositMaturityDueDate']
            };
            if ($data['type'] == 18) return $this->saveGRU($request, $response, $userAdmin, $data);
            if ($data['type'] == 16) return $this->saveOriginating($request, $response, $userAdmin, $data);
            if ($data['type'] == 22) return $this->saveOriginatingCandidate($request, $response, $userAdmin, $data);
            if (!$userAdmin->isAdminNational() && !in_array($data['requestStatus'], [2, 3, 5, 7, 9])) {
                $this->checkValidPaymentDate($userAdmin, $dueDate);
            }
            $arr = ['7', '14', '15'];
            if (in_array($data['type'], $arr)) {
                $providerCnpj = Utils::onlyNumbers($data['laborCPF']);
                $providerName = $data['laborName'];
            } else {
                $providerCnpj = Utils::onlyNumbers($data['providerDoc']);
                $providerName = $data['providerName'];
            }
            $fields = ['description' => 'Descrição da despesa', 'directory' => 'Diretório'];
            Validator::requireValidator($fields, $data);
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);
            if (!$directory) throw new \Exception("Solicitação Inválida, selecione um diretório!");
            $this->changeMoneyValues($data);
            if ($data['type'] == 21) {
                if (!in_array($directory->getId(), [1, 2, 4, 26])) throw new \Exception("Você não tem permissão para realizar esta ação!");
                if ($data['specieAmount'] > 400) throw new \Exception("Limite máximo é de R$ 400,00 para Fluxo de Caixa.");
                if ($directory->getBalanceCashFlow() < $data['specieAmount']) throw new \Exception("O valor solicitado é superior ao saldo disponível.");
                if (!$data['id']) {
                    $newValue = $directory->getBalanceCashFlow() - $data['specieAmount'];
                    $directory->setBalanceCashFlow($newValue);
                    $this->em->getRepository(Directory::class)->save($directory);
                }
            }
            $files = $request->getUploadedFiles();
            if ($data['id'] > 0) {
                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['id']);
            } else {
                $requeriment = new PaymentRequirements();
                $requeriment->setUser($userAdmin);
                $this->em->persist($requeriment);
            }
            $requeriment = $this->moveAttachmentsContractPayment($files, $requeriment);

            $requeriment->setOrigin($data['origin'])
                ->setOfxFiles($data['ofxId'] > 0 ? $this->em->getReference(OfxFiles::class, $data['ofxId']) : null)
                ->setDescription($data['description'])
                ->setType($data['type'])
                ->setCategory(PaymentRequirements::EXPENSE)
                ->setDirectory($directory);
            // carimbos de cadastro novo
            if ($data['id'] == 0) {
                $requeriment->setApproved(PaymentRequirements::APPROVED_USER);
                $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_USER);

                // se cadastro de contrato e usuário responsável pelo centro de custo do contrato
                // carimbo de centro de custo
                if ($data['contract'] != '') {
                    $contract = $this->em->getRepository(Contract::class)->find($data['contract']);
                    if ($contract?->getCostCenter()->getResponsible() === $userAdmin) {
                        $requeriment->setApproved(PaymentRequirements::APPROVED_MANAGER);
                        $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_MANAGER);
                    }
//                    if ($contract && $data['contractCostCenter']) {
//                        if ($contract->getCostCenter()->getId() != $data['contractCostCenter']) {
//                            $contract->setCostCenter($this->em->getReference(CostCenter::class, $data['contractCostCenter']));
//                            $this->em->getRepository(Contract::class)->save($contract);
//                        }
//                    }
                }

                // se cadastro de centro de custo e usuário responsável pelo mesmo
                // carimbo de centro de custo
                if ($data['costCenter'] != '') {
                    $costCenter = $this->em->getRepository(CostCenter::class)->find($data['costCenter']);
                    if ($costCenter?->getResponsible() === $userAdmin) {
                        $requeriment->setApproved(PaymentRequirements::APPROVED_MANAGER);
                        $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_MANAGER);
                    }
                }

                // Se não for diretório Nacional
                if ($directory->getId() != 1) {
                    $requeriment->setApproved(PaymentRequirements::APPROVED_MANAGER);
                    $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_MANAGER);
                }

            }

            if ($data['requestStatus']) {
                $requeriment->setRequestStatus($data['requestStatus']);
                if ($data['requestStatus'] == 1) $requeriment->setPendency($data['pendency']);

                // se status agendado carimbo do financeiro
                if ($data['requestStatus'] == 2) {
                    $requeriment->setApproved(PaymentRequirements::APPROVED_FINANCIAL);
                    $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_FINANCIAL);
                }
                if (in_array($data['requestStatus'], [3, 8])) {
                    //carimbos iniciais
                    $logApprovedManager = $this->em->getRepository(LogApproved::class)->findOneBy(['paymentRequirements' => $requeriment, 'type' => PaymentRequirements::APPROVED_MANAGER]);
                    if (!$logApprovedManager) $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_MANAGER);
                    $logApprovedFinancial = $this->em->getRepository(LogApproved::class)->findOneBy(['paymentRequirements' => $requeriment, 'type' => PaymentRequirements::APPROVED_FINANCIAL]);
                    if (!$logApprovedFinancial) $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_FINANCIAL);

                    if ($data['type'] == 21) {
                        $data['payDay'] = $data['specieDueDate'];
                    } else {
                        if ($data['payDay'] == '' || $data['directoryAccount'] == '' || !$data['directoryAccount']) {
                            throw new Exception('Preencha todos os dados necessários!');
                        }
                    }

                    $requeriment->setPayDay(\DateTime::createFromFormat('d/m/Y', $data['payDay']))
                        // se status efetuado carimbo da tesouraria
                        ->setApproved(PaymentRequirements::APPROVED_TREASURY)
                        ->setPenalty(Utils::moneyToFloat($data['penalty']))
                        ->setFees(Utils::moneyToFloat($data['fees']))
                        ->setINSS(Utils::moneyToFloat($data['inss']))
                        ->setDiscounts(Utils::moneyToFloat($data['discounts']))
                        ->setIRRF(Utils::moneyToFloat($data['irrf']))
                        ->setCSLL(Utils::moneyToFloat($data['csll']))
                        ->setCofins(Utils::moneyToFloat($data['cofins']))
                        ->setPISPASEP(Utils::moneyToFloat($data['pispasep']))
                        ->setISS(Utils::moneyToFloat($data['iss']));

                    if ($data['type'] != 21) $requeriment->setDirectoryAccount($this->em->getReference(DirectoryAccounts::class, $data['directoryAccount']));

                    if ($data['requestStatus'] == 8) $requeriment->setRefundDate(new \Datetime($data['refundDate']));

                    $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_TREASURY);
                    if ($requeriment->getProvider()) $this->registerAccountingRecord($userAdmin, $data, $requeriment);
                }
            }
            switch ((int)$data['type']) {
                case 1:
                    $requeriment->setAccommodationHotel($data['accommodationHotel'])
                        ->setAccommodationStart(\DateTime::createFromFormat('d/m/Y', $data['accommodationStart']))
                        ->setAccommodationEnd(\DateTime::createFromFormat('d/m/Y', $data['accommodationEnd']))
                        ->setBond($data['accommodationBond'])
                        ->setReason($data['accommodationReason']);
                    //->setBeneficiary($data['accommodationBeneficiary']);
                    break;
                case 2:
                    $requeriment->setTaxiRoute($data['taxiRoute'])
                        //->setBeneficiary($data['taxiBeneficiary'])
                        //->setCpf($data['taxiCpf'])
                        ->setValue($data['taxiValue'])
                        ->setReason($data['taxiReason']);
                    break;
                case 4:
                    $requeriment->setBeneficiary($data['fuelName'])
                        ->setFuelPlate($data['fuelPlate'])
                        ->setBond($data['fuelBond'])
                        ->setValue($data['fuelValue']);
                    break;
                case 5:
                    $requeriment->setEvent($this->em->getReference(Events::class, $data['eventName']));
                    break;
                case 6:
                    $requeriment->setActiveType($data['buyAssets']);
                    break;
                case 7:
                case 14:
                case 15:
                    $requeriment->setBeneficiary($data['laborName'])
                        ->setCpf($data['laborCPF']);
                    //->setLaborOccupation($data['laborOccupation']);
                    break;
                case 9:
                    $requeriment
                        ->setAccountConsumer($data['accountConsumer']);
                    break;
                case 12:
                    $requeriment->setLocationDirectory((bool)$data['locationDirectory']);
                    if ($data['locationDirectory'] == 1) {
                        $requeriment->setLocationCommission((bool)$data['locationCommission']);
                        if ($data['locationCommission'] == 1) $requeriment->setLocationCommissionDescription($data['locationCommissionDescription']);
                    }
                    break;
                case 16:
                    $directoryTransfer = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directoryTransfer']]);
                    $requeriment->setDirectoryTransfer($directoryTransfer);
                    break;
                case 19:
                    $requeriment->setLocationDirectory((bool)$data['locationDirectory']);
                    if ($data['locationDirectory'] == 1) {
                        $requeriment->setLocationCommission((bool)$data['locationCommission']);
                        if ($data['locationCommission'] == 1) $requeriment->setLocationCommissionDescription($data['locationCommissionDescription']);
                    }
                    $data['paymentMethod'] = 0;
                    break;
            }
            if ($userAdmin->getLevel() == 3) {
                $state = $city = null;
                $state = $directory->getEstadual() == 'S' ? $this->em->getReference(State::class, $directory->getStateId()) : null;
                $city = $directory->getMunicipal() == 'S' ? $this->em->getReference(City::class, $directory->getCityId()) : null;
                $requeriment->setState($state)->setCity($city);
            } elseif ($userAdmin->getLevel() == 2) {
                $state = $this->em->getRepository(State::class)->findOneBy(['id' => $userAdmin->getState()]);
                $requeriment->setState($state)->setCity(null);
            } else {
                $state = $this->em->getRepository(State::class)->findOneBy(['id' => $userAdmin->getState()]);
                $city = $this->em->getRepository(City::class)->findOneBy(['id' => $userAdmin->getCity()]);
                $requeriment->setCity($city)->setState($state);
            }
            $provider = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => $providerCnpj]);
            if (!$provider) $provider = new Provider();
            $provider->setName($providerName)
                ->setCpfCnpj($providerCnpj)
                ->setType($data['providerType'])
                ->setPaymentMethod((int)$data['paymentMethod']);

            $paymentBankAgency = $data['paymentBankAgency'] . "-" . $data['paymentBankAgencyDigit'];
            $paymentBankAccount = $data['paymentBankAccount'] . "-" . $data['paymentBankAccountDigit'];

            if ($data['paymentMethod'] === '2') {
                $provider->setBank($this->em->getReference(Bank::class, $data['paymentBank']) ?: null)
                    ->setAgency($paymentBankAgency)
                    ->setAccount($paymentBankAccount);
            }
            $paymentValue = match ((int)$data['paymentMethod']) {
                1 => $data['billAmount'],
                2 => $data['depositAmount'],
                3 => $data['pixAmount'],
                4 => $data['specieAmount'],
                default => $data['nfValue']
            };
            $paymentValue = $this->discountsValue($paymentValue, $data['discounts']);
            $requeriment->setPaymentValue($paymentValue);
            $requeriment->setProvider($this->em->getRepository(Provider::class)->save($provider))
                ->setArea($data['area'] ? $this->em->getReference(Area::class, $data['area']) : null)
                ->setTypeDocument((int)$data['typeDocument'])
                ->setNfNumber($data['nfNumber'])
                ->setNfDate(new \Datetime($data['nfDate']))
                ->setBeneficiary($providerName)
                ->setCpf($providerCnpj)
                //->setLaborOccupation($data['laborOccupation'])
                ->setNfValue((float)$data['nfValue'])
                ->setContract($data['contract'] != '' ? $this->em->getReference(Contract::class, $data['contract']) : null)
                ->setCostCenter($data['costCenter'] != '' ? $this->em->getReference(CostCenter::class, $data['costCenter']) : null)
                ->setPaymentMethod($data['paymentMethod']);

            switch ((int)$data['paymentMethod']) {
                case 1:
                    $requeriment->setPaymentDueDate(\DateTime::createFromFormat('d/m/Y', $data['paymentSlipDueDate']))
                        ->setBarCode($data['barCode'] != '' ? Utils::onlyNumbers($data['barCode']) : null);
                    $requeriment->setInstallmentsNumber($data['installmentsNumber']);
                    break;
                case 2:
                    $requeriment->setPaymentDueDate(\DateTime::createFromFormat('d/m/Y', $data['depositMaturityDueDate']))
                        ->setPaymentDoc($data['paymentDoc'])
                        ->setPaymentName($data['paymentName'])
                        ->setPaymentBank($this->em->getReference(Bank::class, $data['paymentBank']))
                        ->setPaymentBankAgency($paymentBankAgency)
                        ->setPaymentBankAccount($paymentBankAccount)
                        ->setPaymentBankAccountType($data['paymentBankAccountType']);
                    break;
                case 3:
                    $requeriment->setPaymentDueDate(\DateTime::createFromFormat('d/m/Y', $data['pixDueDate']))
                        ->setPixType($data['pixType'])
                        ->setPixKey($data['pixKey']);
                    break;
                case 4:
                    $requeriment->setPaymentDueDate(\DateTime::createFromFormat('d/m/Y', $data['specieDueDate']));
                    break;
                default:
                    $requeriment->setPaymentDueDate(new \DateTime($data['nfDate']));
                    break;
            }
            if (!$files['attachmentNf']->getClientFilename() && in_array($data['requestStatus'], [0, 1])) {
                $requeriment->setRequestStatus(0);
                $requeriment->setPendency('Anexar Nota Fiscal');
            } /* elseif ($userAdmin->getType() == UserAdmin::TYPE_ASSISTANT) {
                $requeriment->setRequestStatus(PaymentRequirements::REQUEST_WAITING_APPROVAL);
                $requeriment->setPendency('Liberação de Admin ou Líder');
            } */

            //Se for diretório Nacional status = 4
            $logApprovedManager = $this->em->getRepository(LogApproved::class)->findOneBy(['paymentRequirements' => $requeriment, 'type' => PaymentRequirements::APPROVED_MANAGER]);
            if ($directory->getId() === 1
                && $costCenter?->getResponsible() !== $userAdmin
                && !in_array($data['requestStatus'], [3, 5])
                && (!$data['id'] || !$logApprovedManager)) {
                $requeriment->setRequestStatus(4);
            }

            //se for o lucas de albuquerque carimba automaticamente independente do centro de custo
            //para adicionar mais é só colocar os ids no array, o ideal é criar um cadastro no sistema com esses usuarios
            if (in_array($userAdmin->getId(), [60]) && $requeriment->getRequestStatus() == 4) {
                $requeriment->setRequestStatus(0);
                $requeriment->setApproved(PaymentRequirements::APPROVED_MANAGER);
                $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_MANAGER);
            }

            if ($data['autonomous']) {
                $this->setAutonomous($data, $provider, $files);
                $requeriment->setIsAutonomous(true);
            };

            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            // verificando se existe alguma pendência de OFX cadastrada com essas informações e fazer a vinculação
            if (isset($data['ofxPendency']) && $data['ofxPendency'] !== '') {
                $pendency = $this->em->getRepository(OfxLines::class)->find($data['ofxPendency']);
                $pendency->setRequirements($requeriment);
                $this->em->getRepository(OfxLines::class)->save($pendency);
                $requeriment->setStatementNumber($pendency->getExtractNumber())
                    ->setRequestStatus(PaymentRequirements::REQUEST_STATUS_CONCILIATION);
                $this->em->getRepository(PaymentRequirements::class)->save($requeriment);
                $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_CONCILIATION);
            }

            $this->moveOtherFilesRequirements($requeriment, $files);
            if ($data['type'] == 17) $this->saveTravelPackage($requeriment, $files, $data);

            if ($data['id'] == 0 && $requeriment->getApproved() === 1 && ($requeriment->getContract() !== null || $requeriment->getCostCenter() !== null)) {
                $costCenter = $requeriment->getCostCenter();

                $subject = "Solicitação de Aprovação";
                $msg = "<p>Prezado(a) {$costCenter->getResponsible()->getName()}.</p>
                    <p>O Centro de Custo {$costCenter->getName()} recebeu uma nova solicitação e aguarda sua aprovação. Para mais detalhes
                    <a href='{$this->baseUrl}financeiro/contas-a-pagar/{$requeriment->getId()}' target='_blank'>Clique aqui!</a>.</p>";
                Email::send($costCenter->getResponsible()->getEmail(), $costCenter->getResponsible()->getName(), $subject, $msg);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Solicitação de pagamento cadastrada com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            //$this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveApportionment(PaymentRequirements $requeriment, array $data): void
    {
        $this->em->getRepository(PaymentRequerimentsApportionment::class)->delete($requeriment);

        $destiny = isset($data['directories']) ? $data['directories'] : $data['costCenters'];
        $type = isset($data['percentage']) ? $data['percentage'] : $data['values'];

        foreach ($destiny as $key => $d) {
            $apportionment = new PaymentRequerimentsApportionment();
            if ($data['destinyType'] == 1) {
                $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $d]);
                $apportionment->setDirectory($directory);
            } else {
                $apportionment->setCostCenter($this->em->getReference(CostCenter::class, $d));
            }

            if ($data['calcType'] == 1) {
                $apportionment->setPercent(str_replace(',', '.', $type[$key]));
            } else {
                $apportionment->setValue(Utils::moneyToFloat($type[$key]));
            }

            $apportionment->setPaymentRequirements($requeriment);

            $this->em->getRepository(PaymentRequerimentsApportionment::class)->save($apportionment);

        }
    }

    private function validateTravelPackageFields(array $data, int $key, int $i): void
    {
        $requiredFields = [
            'packageName' => [
                'field' => 'Nome Beneficiário',
                'transform' => fn($value) => $value
            ],
            'packageReason' => [
                'field' => 'Motivo',
                'transform' => fn($value) => $value
            ],
            'packageType' => [
                'field' => 'Tipo',
                'transform' => fn($value) => $value
            ],
            'packageValue' => [
                'field' => 'Valor',
                'transform' => fn($value) => Utils::moneyToFloat($value)
            ],
            'packageCpf' => [
                'field' => 'CPF',
                'transform' => fn($value) => Utils::onlyNumbers($value)
            ],
            'packageBond' => [
                'field' => 'Vínculo com o NOVO',
                'transform' => fn($value) => $value
            ],
            'packageCostCenter' => [
                'field' => 'Centro de Custo',
                'transform' => fn($value) => $value
            ],
        ];

        if ($data['packageType'][$key] == 1) {
            $requiredFields += [
                'packageAirlineCompany' => [
                    'field' => 'Empresa (Companhia aérea)',
                    'transform' => fn($value) => $value
                ],
                'packageTicketLocator' => [
                    'field' => 'Nº Bilhete/Localizador (Loc)',
                    'transform' => fn($value) => $value
                ],
                'packageFlightRoute' => [
                    'field' => 'Trecho',
                    'transform' => fn($value) => $value
                ],
                'packageFlightDate' => [
                    'field' => 'Data da viagem',
                    'transform' => fn($value) => $value
                ],
            ];
        } else if ($data['packageType'][$key] == 2) {
            $requiredFields += [
                'packageHotelCompany' => [
                    'field' => 'Empresa (nome hotel)',
                    'transform' => fn($value) => $value
                ],
                'packageInvoiceNumber' => [
                    'field' => 'Título (nota fiscal)',
                    'transform' => fn($value) => $value
                ],
                'packageStartDate' => [
                    'field' => 'Data de início',
                    'transform' => fn($value) => $value
                ],
                'packageEndDate' => [
                    'field' => 'Data de término',
                    'transform' => fn($value) => $value
                ],
            ];
        }

        $errors = [];
        foreach ($requiredFields as $fieldName => $config) {
            if (empty($data[$fieldName][$key])) {
                $errors[] = "Campo {$config['field']} é obrigatório";
                continue;
            }
        }

        if (!empty($errors)) {
            throw new \Exception("Erro no pacote " . ($i + 1) . ": " . implode(',<br> ', $errors));
        }
    }

    public function saveTravelPackage(PaymentRequirements $requirement, $files, $data)
    {
        // Desativar pacotes salvos anteriormente
        $packages = $this->em->getRepository(PaymentRequerimentsTravelPackage::class)->findBy(['paymentRequirements' => $requirement, 'active' => 1]);
        foreach ($packages as $item) {
            $item->setActive(0);
            $this->em->getRepository(PaymentRequerimentsTravelPackage::class)->save($item);
        }
        $i = 0;
        foreach ($data['packageName'] as $key => $item) {

            $this->validateTravelPackageFields($data, $key, $i);

            $i++;

            $package = new PaymentRequerimentsTravelPackage();

            if (!$files['packageFile'][$key] || !$files['packageFile'][$key]->getClientFilename()) {
                if ($data['packageFileInputHidden'][$key] && $data['packageFileInputHidden'][$key] != '') {
                    $package->setFile($data['packageFileInputHidden'][$key]);
                } else {
                    throw new \Exception("Erro no pacote " . ($i) . ": Arquivo comprobatório é obrigatório");
                }
            } else {
                $package = $this->moveTravelPackageFiles($package, $files['packageFile'][$key], $i);
            }

            $package->setName($data['packageName'][$key])
                ->setReason($data['packageReason'][$key])
                ->setType($data['packageType'][$key])
                ->setValue(Utils::moneyToFloat($data['packageValue'][$key]))
                ->setPaymentRequirements($requirement)
                ->setCpf(Utils::onlyNumbers($data['packageCpf'][$key]))
                ->setBond($data['packageBond'][$key])
                ->setActive(1)
                ->setCostCenter($this->em->getReference(CostCenter::class, $data['packageCostCenter'][$key]));

            if ($data['packageType'][$key] == 1) {
                $package->setAirlineCompany($data['packageAirlineCompany'][$key])
                    ->setTicketLocator($data['packageTicketLocator'][$key])
                    ->setFlightRoute($data['packageFlightRoute'][$key])
                    ->setFlightDate(new \DateTime($data['packageFlightDate'][$key]));
            } else if ($data['packageType'][$key] == 2) {
                $package->setHotelCompany($data['packageHotelCompany'][$key])
                    ->setInvoiceNumber($data['packageInvoiceNumber'][$key])
                    ->setStartDate(new \DateTime($data['packageStartDate'][$key]))
                    ->setEndDate(new \DateTime($data['packageEndDate'][$key]));
            }

            $this->em->getRepository(PaymentRequerimentsTravelPackage::class)->save($package);

        }
    }

    private function moveTravelPackageFiles(PaymentRequerimentsTravelPackage $package, $file, $i)
    {
        $folder = UPLOAD_FOLDER . 'financeiro/';

        if (empty($file) || !$file->getClientFilename()) {
            throw new \Exception("Erro no pacote " . ($i) . ": Arquivo comprobatório é obrigatório");
        }

        if ($file && $file->getClientFilename()) {
            $time = time() . $i;
            $extension = explode('.', $file->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}package.{$extension}";
            $file->moveTo($target);
            $package->setFile($target);
        }
        return $package;
    }

    public function updatePaymentStatus(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $id = $request->getAttribute('route')->getArgument('id');
            $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($id);
            $files = $request->getUploadedFiles();
            $requeriment = $this->moveAttachmentsContractPayment($files, $requeriment);
            $requeriment->setPayDay(\DateTime::createFromFormat('d/m/Y', $data['payDay']) ? \DateTime::createFromFormat('d/m/Y', $data['payDay']) : null)
                ->setScheduledDate(\DateTime::createFromFormat('d/m/Y', $data['scheduledDate']) ? \DateTime::createFromFormat('d/m/Y', $data['scheduledDate']) : null)
                ->setPendency($data['pendency'])
                ->setRequestStatus($data['requestStatus']);

            // se status agendado carimbo do financeiro
            if ($data['requestStatus'] == 2) {
                $requeriment->setApproved(PaymentRequirements::APPROVED_FINANCIAL);
                $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_FINANCIAL);
            }
            if (in_array($data['requestStatus'], [3, 8])) {

                if ($data['payDay'] == '' || $data['directoryAccount'] == '' || !$data['directoryAccount']) throw new Exception('Preencha todos os dados necessários!');

                $requeriment->setPayDay(\DateTime::createFromFormat('d/m/Y', $data['payDay']))
                    // se status efetuado carimbo da tesouraria
                    ->setDirectoryAccount($this->em->getReference(DirectoryAccounts::class, $data['directoryAccount']))
                    ->setApproved(PaymentRequirements::APPROVED_TREASURY)
                    ->setPenalty(Utils::moneyToFloat($data['penalty']))
                    ->setFees(Utils::moneyToFloat($data['fees']))
                    ->setINSS(Utils::moneyToFloat($data['inss']))
                    ->setDiscounts(Utils::moneyToFloat($data['discounts']))
                    ->setIRRF(Utils::moneyToFloat($data['irrf']))
                    ->setCSLL(Utils::moneyToFloat($data['csll']))
                    ->setCofins(Utils::moneyToFloat($data['cofins']))
                    ->setPISPASEP(Utils::moneyToFloat($data['pispasep']))
                    ->setISS(Utils::moneyToFloat($data['iss']));

                if ($data['requestStatus'] == 8) $requeriment->setRefundDate(new \Datetime($data['refundDate']));

                $this->saveApportionment($requeriment, $data);
                $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_TREASURY);
                if ($requeriment->getProvider()) $this->registerAccountingRecord($userAdmin, $data, $requeriment);
            }

            if ($data['requestStatus'] == 7) $this->saveApportionment($requeriment, $data);

            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);
            $requeriment instanceof PaymentRequirements;
            $subject = "Atualização de status - Solicitação de pagamento";
            $msg = "<p>Prezado(a) {$requeriment->getUser()->getName()}.</p>
                    <p>Sua solicitação de pagamente teve o status alterado para <b>{$requeriment->getRequestStatusString()}</b>, para visualizar acesse o sistema
                    <a href='{$this->baseUrl}financeiro/pagamentos/{$requeriment->getId()}' target='_blank'>Clicando aqui!</a>.</p>";

            if ($data['pendency'] != "") {
                $msg .= "<p>Observação:</p> 
                <p>{$data['pendency']}</p>";
            }

            if ($requeriment->getUser()->getId() != 60 && $requeriment->getType() != 22) {
                Email::send($requeriment->getUser()->getEmail(), $requeriment->getUser()->getName(), $subject, $msg);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Status atualizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function paymenstList(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(true, $request->getQueryParam('userType') == 5);
        $index = $request->getQueryParam('index');
        $id = $request->getQueryParam('id');
        $limit = $request->getQueryParam('limit');
        $origin = (int)$request->getQueryParam('origin');
        $cpfCnpj = $request->getQueryParam('cpfCnpj');
        $provider = $request->getQueryParam('provider');
        $docNumber = $request->getQueryParam('docNumber');
        $type = $request->getQueryParam('type');
        $status = $request->getQueryParam('status');
        $costCenter = $request->getQueryParam('costCenter');
        $directory = $request->getQueryParam('directory');
        $directoryPaying = $request->getQueryParam('directoryPaying');
        $dateType = $request->getQueryParam('dateType');
        $dateBegin = $request->getQueryParam('dateBegin');
        $dateEnd = $request->getQueryParam('dateEnd');
        $accounting = $request->getQueryParam('accounting');
        $user = $request->getQueryParam('user');
        $value = $request->getQueryParam('value');
        $paymentValue = $request->getQueryParam('paymentValue');
        $conciliation = $request->getQueryParam('conciliation');
        $area = $request->getQueryParam('area');
        $accounted = $request->getQueryParam('accounted');
        $bbStatus = $request->getQueryParam('bbStatus');
        $paymentRequirements = $this->em->getRepository(PaymentRequirements::class)->billsToPayList($userAdmin, $id, $status, $origin, $cpfCnpj,
            $provider, $docNumber, $type, $costCenter, $directory, $directoryPaying, $dateType, $dateBegin, $dateEnd, $accounting, $user, $value, $paymentValue,
            $area, $accounted, $bbStatus, $limit, $index * $limit, $conciliation);
        $total = $this->em->getRepository(PaymentRequirements::class)->billsToPayListTotal($userAdmin, $id, $status, $origin, $cpfCnpj, $provider,
            $docNumber, $type, $costCenter, $directory, $directoryPaying, $dateType, $dateBegin, $dateEnd, $accounting, $user, $value, $paymentValue, $conciliation,
            $area, $accounted, $bbStatus)['total'];
        $partial = ($index * $limit) + sizeof($paymentRequirements);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $paymentRequirements,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function viewRequeriment(Request $request, Response $response, int $type)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $banks = $this->em->getRepository(Bank::class)->findBy([], ['cod' => 'asc']);
        $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($id);
        $duplicate = $type == 2;
        $events = $this->em->getRepository(Events::class)->findBy([], ['id' => 'DESC']);
        $editMode = in_array($requeriment->getRequestStatus(), [PaymentRequirements::REQUEST_STATUS_PENDENCY, PaymentRequirements::REQUEST_STATUS_REQUESTED,
            PaymentRequirements::REQUEST_STATUS_SCHEDULED, PaymentRequirements::REQUEST_WAITING_APPROVAL, PaymentRequirements::REQUEST_STATUS_APPROVED]);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $requerimentDirectory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $requeriment->getDirectory()]);
        $othersfiles = $this->em->getRepository(PaymentRequirementsFiles::class)->findBy(['requirement' => $id]);
        $originatingTransaction = $this->em->getRepository(OriginatingTransaction::class)->findOneBy(['requirement' => $requeriment]);
        $costsCenter = $this->em->getRepository(CostCenter::class)->findBy(['status' => 1, 'directory' => $requeriment->getDirectory()], ['name' => 'asc']);
        $areas = $this->em->getRepository(Area::class)->areaByDirectory($requerimentDirectory);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/viewRequeriment.phtml', 'section' => 'billsToPay', 'editMode' => $editMode, 'directories' => $directories,
            'user' => $user, 'subMenu' => 'financial', 'title' => 'Novo Pagamento', 'requeriment' => $requeriment, 'banks' => $banks, 'duplicate' => $duplicate, 'events' => $events,
            'originatingTransaction' => $originatingTransaction, 'othersfiles' => $othersfiles, 'requerimentDirectory' => $requerimentDirectory, 'costsCenter' => $costsCenter,
            'areas' => $areas]);
    }

    public function viewBillToPay(Request $request, Response $response)
    {
        $user = $this->getLogged(false, $request->getAttribute('route')->getArgument('diretiva'));
        $id = $request->getAttribute('route')->getArgument('id');
        $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($id ?? 0);
        if (($requeriment && $requeriment->getUser() === !$user)
            && !Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_VISUALIZACAO)) $this->redirectByPermissions();
        $accountsDebit = $accountsCredit = $this->em->getRepository(ChartOfAccounts::class)->findBy(['category' => 2]);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $othersfiles = $this->em->getRepository(PaymentRequirementsFiles::class)->findBy(['requirement' => $id]);
        $originatingTransaction = $this->em->getRepository(OriginatingTransaction::class)->findOneBy(['requirement' => $requeriment]);
        $logsArr = $this->em->getRepository(LogApproved::class)->showActiveLogsBypayment($requeriment);
        if (isset($logsArr[6]) && !isset($logsArr[5])) {
            $width = '58%';
        } else {
            $width = $logsArr['last'] * 16.6 - 8.4 . '%';
        }
        $show = false;
        if ($requeriment->getContract()?->getCostCenter()->getResponsible() === $user || $requeriment->getCostCenter()?->getResponsible() === $user) $show = true;
        $areas = $this->em->getRepository(Area::class)->areaByDirectory($requeriment->getDirectory());
        $directoryAccount = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['directory' => $requeriment->getDirectory(), 'nickname' => $requeriment->getOrigin()]);
        $extractBB = $this->em->getRepository(ExtractBB::class)->findOneBy(['payment' => $id], ['id' => 'DESC']);
        $bbCredentials = $this->em->getRepository(CredentialsBB::class)->findOneBy(['directory' => $requeriment->getDirectory(), 'type' => 2]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/viewBillToPay.phtml', 'section' => 'billsToPay', 'user' => $user,
            'subMenu' => 'financial', 'title' => 'Contas a Pagar', 'requeriment' => $requeriment, 'accountsDebit' => $accountsDebit, 'accountsCredit' => $accountsCredit,
            'directories' => $directories, 'othersfiles' => $othersfiles, 'areas' => $areas, 'directoryAccount' => $directoryAccount, 'bbCredentials' => $bbCredentials,
            'originatingTransaction' => $originatingTransaction, 'width' => $width, 'logs' => $logsArr, 'show' => $show, 'extractBB' => $extractBB
        ]);
    }

    public function transactions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_RELATORIODETRANSACOES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - transactions']);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/transactions.phtml', 'section' => 'transactions', 'term' => $term,
            'user' => $user, 'states' => $states, 'subMenu' => 'financial', 'title' => 'Relatório de Transações', 'cities' => $cities, 'directories' => $directories]);
    }

    private function accessEventState()
    {
        $user = $this->getLogged();
        $location = null;
        if ($user->getLevel() == 3) {
            $location = $this->em->getRepository(State::class)->findAll();
        } elseif ($user->getState()) {
            $location = $this->em->getRepository(State::class)->findBy(['id' => $user->getState()]);
        }
        return $location;
    }

    public function stopCard(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['id' => $id]);
            if (!$card) {
                throw new \Exception('Cartão não encontrado');
            }
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(
                ['status' => 'created', 'tokenCartaoCredito' => $card->getToken(), 'periodicidade' => [1, 6, 12], 'formaPagamento' => 1]);
            if ($subscription) {
                $origin = $subscription->getOrigemTransacao() == 2 ? 'Filiação' : 'Doação';
                throw new \Exception("Este cartão está vinculado à sua assinatura de {$origin}, você deve primeiro alterar a forma de pagamento ou um novo cartão deverá ser cadastrado e vinculado a assinatura.");
            }
            $card->setStatus(0);
            $this->em->getRepository(PersonCreditCard::class)->save($card);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cartão removido com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }

    }

    public function activateCard(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $user = $request->getAttribute('route')->getArgument('user');
            $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['id' => $id]);
            if (!$card) {
                throw new \Exception('Cartão não encontrado');
            }
            $card->setStatus(1);
            $this->em->getRepository(PersonCreditCard::class)->save($card);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cartão habilitado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function apiSubscriptions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) {
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['id' => $id]);
            if (!$subscription) {
                throw new \Exception('Assinatura não encontrada');
            }
            $subscriptionsArray = $this->em->getRepository(PersonSignature::class)->find($id)->toArray();
        } else {
            $subscriptionsArray = [];
            $subscriptions = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $user]);
            foreach ($subscriptions as $subscription) {
                $subscriptionsArray[] = $subscription->toArray();
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $subscriptionsArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function apiExtract(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id]);
        if (!$transaction) {
            throw new \Exception('Transação não encontrada');
        }
        $transactionArray = $this->em->getRepository(Transaction::class)->find($id)->toArray();
        return $response->withJson([
            'status' => 'ok',
            'message' => $transactionArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function saveSubscription(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'value' => 'Valor',
                'frequency' => 'Periodocidade',
                'paymentMethodSignature' => 'Forma de Pagamento',
                'day' => 'Dia de cobrança',
                'dateCharge' => 'Data Gera Fatura',
                'status' => 'Status'
            ];
            $data['value'] = str_replace('.', '', $data['value']);
            $data['value'] = str_replace(',', '.', $data['value']);
            $data['value'] = str_replace('R$', '', $data['value']);
            $data['value'] = (float)trim($data['value']);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['id' => $data['subscriptionId']]);
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $subscription->getTbPessoaId()]);
            if (!$subscription) throw new \Exception('Assinatura não encontrada!');
            $today = new \DateTime();
            if ($subscription->getDiaCiclo() > $today->format('d') && $data['day'] <= $today->format('d')) {
                throw new \Exception('Você só pode realizar está alteração após o pagamento da contribuição do mês vigente!');
            }
            if ($data['value'] > MAXVALUE) throw new \Exception("Valor máximo para filiação é R$ " . number_format(MAXVALUE, 2, ',', '.'));
            if ($subscription->getOrigemTransacao() == 2) {
                if ($data['frequency'] == 1 && $data['value'] < MINMONTHLY) {
                    throw new \Exception('Valor mínimo mensal é R$ ' . number_format(MINMONTHLY, 2, ',', '.'));
                } elseif ($data['frequency'] == 12 && $data['value'] < MINYEARLY) {
                    throw new \Exception('Valor mínimo anual é R$' . number_format(MINYEARLY, 2, ',', '.'));
                } elseif ($data['frequency'] == 6) {
                    if ($data['value'] < MINSEMESTER) {
                        throw new \Exception('Valor mínimo semestral é R$ ' . number_format(MINSEMESTER, 2, ',', '.'));
                    } else if ($data['paymentMethodSignature'] != 2) {
                        throw new \Exception('Para plano semestral é obrigatório forma de pagamento por Boleto!');
                    }
                }
            }
            $nextCharge = str_replace('/', '-', $data['dateCharge']);
            $nextCharge = date("Y-m-d", strtotime($nextCharge));
            $nextCharge = new \DateTime($nextCharge);
            if (!$nextCharge) $nextCharge = $subscription->getDataGeraFatura();
            if ($data['card'] && $data['paymentMethodSignature'] == 1) {
                $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['id' => $data['card']]);
                if ($card->getGatewayPagamento() == 2) {
                    $customer = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                    $tokenCard = $card->getTokenCartaoCredito();
                    $gateway = 2;
                } else {
                    $gateway = 1;
                    $customer = $this->getCustomerIdIUGU($user);
                    $tokenCard = $card->getTokenCartaoCredito();
                }
            } else {
                $gateway = 1;
                $tokenCard = null;
                $customer = $this->getCustomerIdIUGU($user);
            }
            $subscription->setValor($data['value'])
                ->setPaymentForm($data['paymentMethodSignature'])
                ->setCustomerId($customer->getCustomerId())
                ->setGatewayPagamento($gateway)
                ->setPeriodicidade($data['frequency'])
                ->setDiaCiclo($data['day'])
                ->setStatus($data['status'])
                ->setDataGeraFatura($nextCharge)
                ->setTokenCartaoCredito($tokenCard);
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Assinatura alterada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function saveExtract(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'transactionvalue' => 'Valor',
                'status' => 'Status'
            ];
            Validator::requireValidator($fields, $data);
            $value = str_replace('.', '', $data['transactionvalue']);
            $value = str_replace(',', '.', $value);
            $value = str_replace('R$', '', $value);
            $value = (float)trim($value);
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $data['extractId']]);
            if (!$transaction) throw new \Exception('Transação não encontrada');
            $transaction->setValor($value)->setStatus($data['status']);
            if ($data['cancellationDate']) $transaction->setDataCancelamento(new \Datetime($data['cancellationDate']));
            $this->em->getRepository(Transaction::class)->save($transaction);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Transação alterada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function chargeTransaction(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $today = new \DateTime();
            $fields = [
                'paymentMethod' => 'Forma de Pagamento',
                'transaction' => 'Transação',
            ];
            if ($data['paymentMethod'] == 1) $fields['card'] = 'Cartão de crédito';
            Validator::requireValidator($fields, $data);
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $data['transaction']]);
            if (!$transaction) throw new \Exception("Transação inválida!");
            $keyIUGU = $this->getIuguTokenDirectory($transaction->getTbDiretorioId());
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $transaction->getTbPessoaId()]);
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $transaction->getTbPessoaId()]));
            if ($data['paymentMethod'] == 1) { // cartao de credito
                $rederId = '';
                $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                if ($card->getGatewayPagamento() == 2) {
                    $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                    $transactionRede = MaxiPago::transaction($personRede, $card->getToken(), $transaction->getValor(), '', null, $user);
                    $invoiceId = $transactionRede['transactionID'];
                    $rederId = $transactionRede['orderId'];
                } else {
                    $personIugu = $this->getCustomerIdIUGU($user);
                    $transactionIUGU = IuguService::charge($personIugu->getCustomerId(), $card->getTokenCartaoCredito(), $user->getEmail(), $user->getCpf(), $user->getName(),
                        $address, $transaction->getValor(), $transaction->getOriginTypeString(), $keyIUGU['key']);
                    $invoiceId = $transactionIUGU['invoice_id'];
                }
                $transaction->setTokenCartaoCredito($card->getToken())
                    ->setFormaPagamento(1)
                    ->setGatewayPagamento($card->getGatewayPagamento())
                    ->setDataPago($today)
                    ->setStatus('paid')
                    ->setRedeId($rederId)
                    ->setInvoiceId($invoiceId)
                    ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
                $this->em->getRepository(Transaction::class)->save($transaction);
                $this->validLastTransaction($user, $transaction, 4);
                if ($user->getFiliado() == 1 || $user->getFiliado() == 3) {
                    $solicitation = $user->getFiliado() == 1 ? $user->getDataSolicitacaoFiliacao()->format('Y-m-d H:i:s') : $user->getDataSolicitacaoRefiliacao()->format('Y-m-d H:i:s');
                    if ($transaction->getOrigemTransacao() == 2 and $transaction->getDataCreation()->format('Y-m-d H:i:s') > $solicitation) {
                        $userChange = $this->em->getRepository(User::class)->findOneBy(['id' => $user->getId()]);
                        if ($userChange->getFiliado() == 1) {
                            $userChange->setFiliado(2);
                            //->setDataSolicitacaoFiliacao($date);
                        } elseif ($userChange->getFiliado() == 3) {
                            $userChange->setFiliado(14);
                            //->setDataSolicitacaoRefiliacao($date);
                        }
                        $userChange->setStatus(0);
                        $this->em->getRepository(User::class)->save($userChange);
                    }
                }
                return $response->withJson([
                    'status' => 'ok',
                    'message' => 'Pagamento realizado com sucesso',
                ], 201)
                    ->withHeader('Content-type', 'application/json');
            }
            //BOLETO
            if ($address['resides_outside'] == true) throw new \Exception('Atenção, pessoa residente no exterior, Tente realizar o pagamento com o Cartão de Crédito ou atualize o endereço para um nacional!');
            $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 5, $keyIUGU['key']);
            $transaction->setDataVencimento($today->add(new \DateInterval('P5D')))
                ->setFormaPagamento(2)
                ->setGatewayPagamento(1)
                ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                ->setStatus('pending')
                ->setCodigoBarras($slip['bank_slip']['digitable_line'])
                ->setInvoiceId($slip['id']);
            $this->em->getRepository(Transaction::class)->save($transaction);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Boleto gerado com sucesso. <br>
                            Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                            <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                            <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a>",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function receipt(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $transaction = $this->em->getRepository(Transaction::class)->find($id);
        $person = $this->em->getRepository(User::class)->findOneBy(['id' => $transaction->getTbPessoaId()]);
        if (!$transaction || $transaction->getTbPessoaId() != $person->getId()) {
            Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo');
            $this->redirect();
        }
        $directory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $transaction->getTbDiretorioId()]);
        $title = in_array($transaction->getOrigemTransacao(), [2, 15]) ?
            'RECIBO DE CONTRIBUIÇÂO DE FILIAÇÃO' : 'RECIBO DE DOAÇAO - VIA DOADOR';
        return $this->renderer->render($response, 'financial/receipts/receipt.phtml', ['subMenu' => 'affiliated',
            'transaction' => $transaction, 'directory' => $directory, 'person' => $person, 'user' => $user, 'title' => $title]);
    }

    public function financialReport(Request $request, Response $response)
    {
        $this->redirectByPermissions(BASEURL . 'financeiro/transacoes/');
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $states = $this->accessEventState();
        $directories = $this->em->getRepository(Directory::class)->findBy(['ativo' => 'S'], ['nome' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/financialReport.phtml', 'section' => 'financialReport',
            'user' => $user, 'subMenu' => 'financial', 'states' => $states, 'title' => 'Relatório Financeiro', 'directories' => $directories]);
    }

    public function antecipation(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $date = new \DateTime();
            $data = (array)$request->getParams();
            $affiliated = $this->em->getRepository(User::class)->findOneBy(['id' => $data['userId']]);
            $fields = ['paymentMethod' => 'Forma de Pagamento', 'parcel' => 'Parcelas'];
            Validator::requireValidator($fields, $data);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $affiliated, 'origin' => 2]);
            $value = $subscription->getValor() * $data['parcel'];
            if ($value > MAXVALUE) throw new \Exception("Valor máximo para antecipação diária é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".");
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $affiliated->getId()]));
            if ($data['paymentMethod'] == 1) {
                $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                $url = $barCode = $qrcode = $dueDate = '';
                $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $affiliated->getId()], ['id' => 'DESC']);
                $directoryGatway = $this->em->getRepository(DirectoryGatway::class)->findOneBy(['tbDiretorioId' => 1, 'formaPagamento' => 1, 'gatwayPagamento' => 2]);
                $gateway = 2;
                $transactionRede = MaxiPago::transaction($personRede, $card->getToken(), $value, 'Filiacao - antecipada', $directoryGatway, $affiliated);
                $invoiceId = $transactionRede['transactionID'];
                $rederId = $transactionRede['orderId'];
                $status = 'paid';
                $message = "Antecipação realizada com sucesso, o NOVO agradece sua contribuição!";
                $datePaid = $date;
            } else {
                $slip = IuguService::slip($affiliated->getEmail(), $affiliated->getCpf(), $affiliated->getName(), $address, $value, 'Filiação antecipada', 1);
                $message = "Boleto gerado com sucesso.<br>
                            Código de Barras: {$slip['bank_slip']['digitable_line']}<br> 
                            <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                            <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a>";
                $status = 'pending';
                $url = $slip['bank_slip']['bank_slip_pdf_url'];
                $barCode = $slip['bank_slip']['digitable_line'];
                $gateway = 1;
                $invoiceId = $slip['id'];
                $qrcode = $slip['pix']['qrcode'];
                $dueDate = $slip['due_date'];
                $datePaid = null;
            }
            $transaction = $this->populateTransaction($affiliated, $value, $date, new \Datetime($dueDate), $datePaid, $data['paymentMethod'],
                $status, '', 0, $gateway, 2, 'Filiacao antecipada', '', $invoiceId, '', 1, 1,
                $url, $barCode, $subscription->getId(), $qrcode);
            $this->em->getRepository(Transaction::class)->save($transaction);
            $dateLastTransaction = date('Y-m', strtotime($this->em->getRepository(Transaction::class)->getDateLastTransaction($affiliated->getId())['data_criacao']));
            for ($i = 1; $i <= $data['parcel']; $i++) {
                $day = $subscription->getDiaCiclo();
                $dateTransaction = date("Y-m", strtotime($dateLastTransaction . "+" . $i . "month")) . '-' . $day;
                $explode = explode('-', $dateTransaction);
                $valid = checkdate($explode[1], $day, $explode[0]);
                while ($valid == false) {
                    $day--;
                    $dateTransaction = date("Y-m", strtotime($dateLastTransaction . "+" . $i . "month")) . '-' . $day;
                    $explode = explode('-', $dateTransaction);
                    $valid = checkdate($explode[1], $day, $explode[0]);
                }
                $transactionDate = new \Datetime($dateTransaction);
                $save = $this->populateTransaction($affiliated, $subscription->getValor(), $transactionDate, $date, null,
                    $data['paymentMethod'], 'accumulated', '', 1, $gateway, 2, 'Ref - ' . $transaction->getId(), '',
                    $slip['id'], '', 1, 1, '', '', $subscription->getId(), '');
                $this->em->getRepository(Transaction::class)->save($save);
            }
            $nextCharge = new \DateTime(str_replace('/', '-', $data['nextCharge']));
            $subscription->setDataGeraFatura($nextCharge);
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function payoff(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $date = new \DateTime();
            $data = (array)$request->getParams();

            // Calcula o total das transações selecionadas
            $total = null;
            foreach ($data['transactionPayOff'] as $t) {
                $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $t]);
                $total += $transaction->getValor();
            }

            // Calcula o valor por parcela
            $valuePerParcel = $total / $data['parcel'];

            // Obtém o endereço do filiado e identifica o filiado
            $affiliated = $this->em->getRepository(User::class)->findOneBy(['id' => $data['user']]);
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $affiliated->getId()]));

            // Verifica se o valor total excede o limite diário
            if ($valuePerParcel > MAXVALUE) { 
                throw new \Exception("Valor máximo para pagamento diário é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".");
            }

            if($data['parcel'] <= 0) {
                throw new \Exception('Número de parcelas inválido');
            }

            for ($i = 1; $i <= $data['parcel']; $i++) {

                // Calcula a data de vencimento
                $dueDate = new \DateTime();
                if($i == 1) {
                    $dueDate = $date->add(new \DateInterval('P5D'));
                } else {
                    $dueDate = $dueDate->add(interval: new \DateInterval('P' . ($i - 1) . 'M'));
                }

                // calcula vencimento em dias
                $days = (new \DateTime())->diff($dueDate)->days;
                
                // Verifica se a forma de pagamento é válida
                if(!in_array($data['paymentFormPayoff'], [1, 2, 11])) {
                    throw new \Exception('Forma de pagamento inválida');
                }

                // Gera o boleto
                if($data['paymentFormPayoff'] == 2 || $data['paymentFormPayoff'] == 11) {

                    $charge = IuguService::slip($affiliated->getEmail(), 
                        $affiliated->getCpf(), 
                        $affiliated->getName(), 
                        $address, 
                        $valuePerParcel, 
                        'Acerto de contribuições pendentes', 
                        $days
                    );

                    if($data['paymentFormPayoff'] == 2) {
                    $message = "Boleto gerado com sucesso. <br>
                                Código de Barras: {$charge['bank_slip']['digitable_line']} <br>
                                <button class='btn btn-primary copy-button' type='button' 
                                        data-clipboard-text='{$charge['bank_slip']['digitable_line']}'>
                                    <i class='fa fa-copy'></i> Copiar Código de Barras
                                </button>
                                <a class='btn btn-success' 
                                    href='{$charge['bank_slip']['bank_slip_pdf_url']}' 
                                    target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a>";
                    } else {
                        $message = "<div class='card'>
                                        <div class='card-header bg-success text-white'>
                                            <i class='fa fa-check-circle'></i> Pix gerado com sucesso
                                        </div>
                                        <div class='card-body'>
                                            <div class='mb-4'>
                                                <label class='form-label fw-bold mb-2'>Pix copia e cola:</label>
                                                <div class='alert alert-light border d-flex align-items-center'>
                                                    <span class='text-break flex-grow-1'>{$charge['pix']['qrcode_text']}</span>
                                                </div>
                                            </div>
                                            <div class='mb-4'>
                                                <button class='btn btn-primary copy-button' type='button' 
                                                        data-clipboard-text='{$charge['pix']['qrcode_text']}'>
                                                    <i class='fa fa-copy'></i> Copiar Pix copia e cola
                                                </button>
                                            </div>
                                            <div class='text-center'>
                                                <p class='fw-bold mb-2'>Ou escaneie o QR Code:</p>
                                                <div class='bg-light p-4 d-inline-block rounded'>
                                                    <img src='{$charge['pix']['qrcode']}' 
                                                        alt='QR Code Pix' 
                                                        class='img-fluid' 
                                                        style='max-width: 400px;'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";
                    }

                    $status = 'pending';
                    $url = $charge['bank_slip']['bank_slip_pdf_url'];
                    $barCode = $charge['bank_slip']['digitable_line'];
                    $qrcode = $charge['pix']['qrcode'];
                    $dueDate = $charge['due_date'];
                    $datePaid = null;

                // Pagamento com cartão de crédito
                } elseif($data['paymentFormPayoff'] == 1) {

                    $message = "Pagamento realizado com sucesso.";
                    $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                    $personIugu = $this->getCustomerIdIUGU($affiliated);
                    $keyIUGU = $this->getIuguTokenDirectory(1);

                    $charge = IuguService::charge(
                        $personIugu->getCustomerId(),
                        $card->getTokenCartaoCredito(),
                        $affiliated->getEmail(),
                        $affiliated->getCpf(),
                        $affiliated->getName(),
                        $address,
                        $valuePerParcel,
                        'Acerto de contribuições pendentes',
                        0,
                        $keyIUGU['key']
                    );
                        
                    $status = 'paid';
                    $url = $barCode = $qrcode = '';
                    $dueDate = $charge['due_date'];
                    $datePaid = $date;
                }

                // Cria a transação principal
                $transaction = $this->populateTransaction(
                    $affiliated, 
                    $valuePerParcel, 
                    new \DateTime($dueDate), 
                    new \Datetime($dueDate), 
                    $datePaid, 
                    $data['paymentFormPayoff'],
                    $status, 
                    '', 
                    0, 
                    1, 
                    2, 
                    'Parcela ' . $i . ' de ' . $data['parcel'], 
                    '', 
                    $charge['id'], 
                    '', 
                    1, 
                    1,
                    $url, 
                    $barCode, 
                    null, 
                    $qrcode);
                $this->em->getRepository(Transaction::class)->save($transaction);

                // Adiciona a transação ao array
                $arrayTransaction[] = $transaction->getId();

            }

            // Converte o array em uma string separada por vírgulas
            $idsString = implode(',', $arrayTransaction);

            // Substitui as transações acumuladas pela transação principal
            foreach ($data['transactionPayOff'] as $id) {
                $transactionAccumulated = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id]);
                $transactionAccumulated
                    ->setStatus('accumulated')
                    ->setObs('Transação parcelada por ' . $user->getName() . ' (' . $idsString . ')')
                    ->setInvoiceId(null);
                $this->em->getRepository(Transaction::class)->save($transactionAccumulated);
            };

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function importFinancialReport(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!in_array($user->getId(), array(23, 418, 371, 21))) die;
        $file = $request->getUploadedFiles();
        $file = $file['file'];
        $row = 1;
        if ($file && $file->getClientFilename()) {
            $folder = UPLOAD_FOLDER . 'financeiro/';
            $time = time();
            $extension = explode('.', $file->getClientFilename());
            $extension = end($extension);
            if ($extension != 'csv') die;
            $target = "{$folder}{$time}.{$extension}";
            $file->moveTo($target);
        }
        if (($handle = fopen($target, "r")) !== FALSE) {
            $separator = ',';
            $data = fgetcsv($handle, 9000, ",");
            if (count($data) == 1) $separator = ';';
            $count = 0;
            while (($data = fgetcsv($handle, 9000, $separator)) !== FALSE) {
                if ($count <= 9000) {
                    $value = str_replace('.', '', $data[1]);
                    $value = str_replace(',', '.', $value);
                    $date = $data[2] != '' ? new \Datetime(date('Y-m-d', strtotime(str_replace('/', '-', $data[2])))) : null;
                    $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $data[0], 'status' => ['paid', 'reversed']]);
                    if ($data[0] != '') {
                        if ($transaction) {
                            $transaction->setValorSemTaxas($value);
                            if ($date) $transaction->setDataDeposito($date);
                            $this->em->getRepository(Transaction::class)->save($transaction);
                        }
                    }
                }
                $count++;
            }
            fclose($handle);
        }
    }

    public function conciliationRede(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_REDE_CRIACAO_EDICAO, true);
        $file = $request->getUploadedFiles();
        $analyse = false;
        $totalSystemTransactions = $totalSystemValue = $totalFileLines = $totalFileValue = $divergenceFileValue = $divergenceSystemValue = 0;
        $divergenceSystem = $divergenceFile = $invoicesFile = [];
        $analyseTitle = '';
        if ($file && $file['file']->getClientFilename()) {
            $analyse = true;
            $data = (array)$request->getParsedBody();
            $start = \DateTime::createFromFormat('d/m/Y', $data['start']);
            $end = \DateTime::createFromFormat('d/m/Y', $data['end']);
            $directory = $this->em->getRepository(Directory::class)->find($data['directory']);
            $analyseTitle = "{$directory->getName()}: {$data['start']} - {$data['end']}";
            $transactions = $this->em->getRepository(Transaction::class)->getConciliationRede($start, $end, $data['directory']);
            $transactionsArray = [];
            foreach ($transactions as $transaction) {
                $totalSystemTransactions++;
                $totalSystemValue += Utils::moneyToFloat($transaction->getValue());
                $transactionsArray[$transaction->getInvoiceId()] = $transaction;
            }
            $file = $file['file'];
            $target = UPLOAD_FOLDER . time() . 'csv';
            $file->moveTo($target);
            if (($handle = fopen($target, "r")) !== FALSE) {
                $separator = ',';
                $data = fgetcsv($handle, 9000, ",");
                if (count($data) == 1) $separator = ';';
                while (($data = fgetcsv($handle, 9000, $separator)) !== FALSE) {
                    $invoicesFile[$data[29]] = Utils::moneyToFloat($data[3]);
                    if (!array_key_exists($data[29], $transactionsArray) ||
                        Utils::moneyToFloat($data[3]) != Utils::moneyToFloat($transactionsArray[$data[29]]->getValue())) {
                        $t = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $data[29]]);
                        $systemValue = $systemDate = $destiny = $status = $id = $personId = $paymentTypeString = $person = $origin = '';
                        if ($t) {
                            $id = $t->getId();
                            $personId = $t->getTbPessoaId();
                            $paymentTypeString = $t->getPaymentTypeString();
                            $origin = $t->getOriginTypeString();
                            $person = $this->em->getRepository(User::class)->find($t->getTbPessoaId())->getName();
                            $systemValue = $t->getValue();
                            $systemDate = $t->getDataPaid();
                            $destiny = $this->em->getRepository(Directory::class)->find($t->getDirectoryOrigin());
                            $destiny = $destiny->getName();
                            $status = $t->getStatus();
                        }
                        $divergenceFile[] = ['invoice' => $data[29], 'date' => "{$data[0]} {$data[1]}", 'systemDate' => $systemDate,
                            'valueFile' => $data[3], 'valueSystem' => $systemValue, 'card' => $data[23], 'destiny' => $destiny,
                            'status' => $status, 'id' => $id, 'personId' => $personId, 'paymentTypeString' => $paymentTypeString,
                            'origin' => $origin, 'personName' => $person];
                        $divergenceFileValue += Utils::moneyToFloat($data[3]);
                    }
                    $totalFileLines++;
                    $totalFileValue += Utils::moneyToFloat($data[3]);
                }
                fclose($handle);
                foreach ($transactionsArray as $transaction) {
                    if (!array_key_exists($transaction->getInvoiceId(), $invoicesFile)) {
                        $divergenceSystemValue += $transaction->getValue();
                        $person = $this->em->getRepository(User::class)->find($transaction->getTbPessoaId());
                        $destiny = $this->em->getRepository(Directory::class)->find($transaction->getDirectoryOrigin());
                        $divergenceSystem[] = ['id' => $transaction->getId(), 'date' => $transaction->getDataPaid(),
                            'date' => $transaction->getDataPaid(), 'origin' => $transaction->getOriginTypeString(),
                            'invoice' => $transaction->getInvoiceId(), 'value' => $transaction->getValue(),
                            'personId' => $person->getId(), 'personName' => $person->getName(), 'destiny' => $destiny->getName(),
                            'paymentTypeString' => $transaction->getPaymentTypeString()];
                    }
                }
            }
        }
        $directories = $this->em->getRepository(Directory::class)->findBy([], ['nome' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/conciliationRede.phtml',
            'subMenu' => 'financial', 'subSectionActive' => 'conciliation', 'subSection' => 'conciliationRede',
            'analyse' => $analyse, 'totalSystemTransactions' => $totalSystemTransactions,
            'totalSystemValue' => $totalSystemValue, 'totalFileLines' => $totalFileLines, 'totalFileValue' => $totalFileValue,
            'user' => $user, 'subMenu' => 'financial', 'divergenceSystem' => $divergenceSystem, 'divergenceFile' => $divergenceFile,
            'divergenceFileValue' => $divergenceFileValue, 'divergenceSystemValue' => $divergenceSystemValue,
            'directories' => $directories, 'analyseTitle' => $analyseTitle]);
    }

    public function ofx(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_IMPORTACAO_CRIACAO_EDICAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/ofx.phtml', 'directories' => $directories,
            'user' => $user, 'subMenu' => 'financial', 'subSectionActive' => 'conciliation', 'subSection' => 'import']);
    }

    public function listByOfx(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $status = $request->getAttribute('route')->getArgument('status');
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);

        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/ofxList.phtml', 'directories' => $directories,
            'user' => $user, 'subMenu' => 'financial', 'subSectionActive' => 'conciliation', 'subSection' => $status]);
    }

    public function ofxPendency(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);

        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/ofxPendency.phtml', 'directories' => $directories,
            'user' => $user, 'subMenu' => 'financial', 'subSectionActive' => 'conciliation', 'subSection' => 'pendentes']);
    }

    public function saveOfx(Request $request, Response $response)
    {
        try {

            $user = $this->getLogged();
            Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO, true);
            $this->em->beginTransaction();
            $file = $request->getUploadedFiles();
            $message = [];
            if ($file && $file['file']->getClientFilename()) {
                $accountAnalyse = (int)$request->getParsedBody()['account'];
                // Pegando a conta que está sendo feito o vínculoi
                $accountAnalyse = $this->em->getRepository(DirectoryAccounts::class)->find($accountAnalyse);
                $file = $file['file'];
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                if ($extension != 'ofx') throw new Exception('Formato de documento inválido!');
                // endereço para realizar o upload
                $time = time();
                $path = UPLOAD_FOLDER . "financeiro/ofx/";
                $target = "{$path}{$time}ofx.{$extension}";
                $file->moveTo($target);
                // fazendo a leitura do documento
                $ofxParser = new \OfxParser\Parser();
                $ofx = $ofxParser->loadFromFile($target);
                $ofxFile = reset($ofx->bankAccounts);
                $transactions = $ofxFile->statement->transactions;


                // Salvando o endereço do ofxFile no Banco de Dados
                $ofxFileSave = new OfxFiles();
                $ofxFileSave->setDirectoryAccount($accountAnalyse)
                    ->setFile($target)
                    ->setUser($user);
                $ofxFileSave = $this->em->getRepository(OfxFiles::class)->save($ofxFileSave);

                $startDate = $endDate = null;

                // Salvando as informações dos OFX's
                foreach ($transactions as $transaction) {
                    $transaction = get_object_vars($transaction);
                    $endDate = $transaction['date'];
                    if (!$startDate) $startDate = $transaction['date'];
                    $line = new OfxLines();
                    $line->setUser($user)
                        ->setOfxFiles($ofxFileSave)
                        ->setDate($transaction['date'])
                        ->setType($transaction['type'])
                        ->setValue(abs($transaction['amount']))
                        ->setUniqueIdentifier($transaction['uniqueId'])
                        ->setExtractNumber($transaction['checkNumber'][0])
                        ->setDirectoryAccount($accountAnalyse)
                        ->setDirectory($accountAnalyse->getDirectory())
                        ->setDescription($transaction['memo'])
                        ->setOfx($ofxFileSave)
                        ->setCategory($transaction['amount'] > 0 ? OfxLines::REVENUE : OfxLines::EXPENSE);
                    $this->em->getRepository(OfxLines::class)->save($line);
                }

                // verificando se não existe um cadastro para esse intervalo de data e conta do diretório
                $verify = $this->em->getRepository(OfxFiles::class)->verifyDate($accountAnalyse, $startDate, $endDate);
                // se existir algum cadastro exibe mensagem de erro e exclui o documento que acabou de fazer upload
                if ($verify) {
                    throw new Exception("Já existe um arquivo cadastrado para essa conta nesta data");
                }

                $ofxFileSave->setStart($startDate)
                    ->setEnd($endDate);
                $this->em->getRepository(OfxFiles::class)->save($ofxFileSave);


                $message['id'] = $ofxFileSave->getId();
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function refreshOfx(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO, true);
            $id = $request->getAttribute('route')->getArgument('id');
            $file = $request->getUploadedFiles();
            $transactionsArray = [];
            $message = [];
            $accountAnalyse = (int)$request->getParsedBody()['account'];
            // Pegando a conta que está sendo feito o vínculoi
            $accountAnalyse = $this->em->getRepository(DirectoryAccounts::class)->find($accountAnalyse);
            // Buscando arquivo
            $refresh = $this->em->getRepository(OfxFiles::class)->find($id);
            // fazendo a leitura do documento
            $ofxParser = new \OfxParser\Parser();
            $ofx = $ofxParser->loadFromFile($refresh->getFileOfx());
            $ofxFile = reset($ofx->bankAccounts);
            $transactions = $ofxFile->statement->transactions;

            // carregando as informações que serão enviadas ao front end
            $message = [
                'directoryName' => $accountAnalyse->getDirectory()->getName(),
                'originStr' => $accountAnalyse->getNicknameString(),
                'start' => $ofxFile->statement->startDate->format('d/m/Y'),
                'end' => $ofxFile->statement->endDate->format('d/m/Y'),
                'bank' => $accountAnalyse->getBank()->getName(),
                'account' => $accountAnalyse->getAccountString(),
                'agency' => $accountAnalyse->getAgencyString(),
                'id' => $id,
            ];

            // Buscando os requerimentos vinculados a cada transação
            foreach ($transactions as $transaction) {
                $transaction = get_object_vars($transaction);
                if ((float)$transaction['amount'] > 0 || $transaction['type'] == 'DEP') continue;
                if ($transaction['date'] < '2024-01-01') {
                    $requeriments = $this->em->getRepository(PaymentRequirements::class)->findBy(
                        ['directory' => $accountAnalyse->getDirectory(), 'payDay' => $transaction['date'], 'paymentValue' => (float)$transaction['amount'] * -1]);
                } else {
                    $requeriments = $this->em->getRepository(PaymentRequirements::class)->findBy(
                        ['directoryAccount' => $accountAnalyse->getId(), 'payDay' => $transaction['date'], 'paymentValue' => (float)$transaction['amount'] * -1]);
                }
                if ($requeriments) {
                    // carregando as informações dos requerimentos para enviar ao front end
                    foreach ($requeriments as $requeriment) {
                        $transaction['requeriments'][] = [
                            'id' => $requeriment->getId(),
                            'user' => $requeriment->getUser()->getName(),
                            'origin' => $requeriment->getOriginStr(),
                            'type' => $requeriment->getTypeStr(),
                            'beneficiary' => $requeriment->getBeneficiary() ? $requeriment->getBeneficiary() : $requeriment->getProvider()->getName(),
                            'attachments' => $requeriment->getOfxAttachments(),
                            'statusStr' => $requeriment->getRequestStatusString(),
                            'status' => $requeriment->getRequestStatus(),
                        ];
                    }
                }
                $transactionsArray[] = $transaction;
            }

            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
                'transactions' => $transactionsArray,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function linkOfxRequeriments(Request $request, Response $response)
    {
        try {

            $user = $this->getLogged();
            Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO, true);
            $this->em->beginTransaction();

            $data = (array)$request->getParams();
            $ofxFile = $this->em->getRepository(OfxFiles::class)->find($data['ofxFileId']);
            if (!$ofxFile) throw new Exception('Arquivo inexistente!');

            foreach ($data['requeriments'] as $item) {

                $arr = explode(',', $item);

                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($arr[1]);

                $i = $requeriment->getApproved() + 1;
                $requeriment->setOfxFiles($ofxFile)
                    ->setApproved(PaymentRequirements::APPROVED_CONCILIATION)
                    ->setRequestStatus(PaymentRequirements::REQUEST_STATUS_CONCILIATION)
                    ->setStatementNumber($arr[2]);
                $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

                $line = $this->em->getRepository(OfxLines::class)->find($arr[0]);
                $line->setRequirements($requeriment);

                // gerar carimbos de Conciliação
                for ($i; $i <= PaymentRequirements::APPROVED_CONCILIATION; $i++) {
                    $this->saveLogApproved($requeriment, $i);
                }
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Conciliações realizadas com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function removeConciliation(Request $request, Response $response)
    {
        try {

            $user = $this->getLogged();
            Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO, true);
            $this->em->beginTransaction();

            $data = (array)$request->getParams();
            //voltar para pagamento efetuado
            $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['id']);
            $requeriment->setOfxFiles(null)
                ->setRequestStatus(PaymentRequirements::REQUEST_STATUS_PAID)
                ->setStatementNumber('');
            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            //remover carimbo
            $this->em->getRepository(LogApproved::class)->deleteByPaymentAndType($requeriment, PaymentRequirements::APPROVED_CONCILIATION);


            //remover link
            $lineRequeriment = $this->em->getRepository(OfxLines::class)->findOneBy(['requirements' => $requeriment]);
            if ($lineRequeriment) {
                $lineRequeriment->setRequirements(null)
                    ->setMultiple(false);
                $this->em->getRepository(OfxLines::class)->save($lineRequeriment);
            }

            //verifica se esta na tabela de vinculo multiplo
            $line = $this->em->getRepository(OfxLinePaymentRequirement::class)->findOneBy(['requirements' => $requeriment]);
            if ($line) {
                $line = $line->getOfxLine();
                $line->setRequirements(null)
                    ->setMultiple(false);
                $this->em->getRepository(OfxLines::class)->save($line);
                $allRequirements = $this->em->getRepository(OfxLinePaymentRequirement::class)->findBy(['ofxLine' => $line]);
                foreach ($allRequirements as $requeriment) {
                    $requeriment = $requeriment->getRequirements();
                    $requeriment->setOfxFiles(null)
                        ->setRequestStatus(PaymentRequirements::REQUEST_STATUS_PAID)
                        ->setStatementNumber('');
                    $this->em->getRepository(PaymentRequirements::class)->save($requeriment);
                    $this->em->getRepository(LogApproved::class)->deleteByPaymentAndType($requeriment, PaymentRequirements::APPROVED_CONCILIATION);
                }
                $this->em->getRepository(OfxLinePaymentRequirement::class)->dropByOfxLine($line);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Conciliações removida com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public
    function chartOfAccounts(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_PLANODECONTAS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/chartOfAccounts.phtml', 'section' => 'chartOfAccounts',
            'user' => $user, 'subMenu' => 'records', 'subSectionActive' => 'register']);
    }

    public
    function SaveAccounts(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'accountModal' => 'Conta',
                'descriptionModal' => 'Descrição',
                'typeModal' => 'Tipo',
                'categoryModal' => 'Categoria'
            ];
            Validator::requireValidator($fields, $data);
            $account = new ChartOfAccounts();
            if ($data['accountId']) $account = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['id' => $data['accountId']]);
            $account->setActive(1)
                ->setAccount($data['accountModal'])
                ->setDescription($data['descriptionModal'])
                ->setType($data['typeModal'])
                ->setCategory($data['categoryModal']);
            if ($data['reducedModal']) $account->setReduced($data['reducedModal']);
            if ($data['surnameModal']) $account->setSurname($data['surnameModal']);
            if (!$data['accountId']) $account->setCreated_at(new \DateTime());
            $this->em->getRepository(ChartOfAccounts::class)->save($account);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registro realizado com sucesso!'
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function changeAccount(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $status = $request->getAttribute('route')->getArgument('status');
            $account = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['id' => $id]);
            if (!$account) throw new \Exception('Conta não encontrada!');
            $account->setActive($status);
            $this->em->getRepository(ChartOfAccounts::class)->save($account);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Conta atualizada com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function getAccount(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $account = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['id' => $id]);
            if (!$account) throw new \Exception('Conta não encontrada!');
            $array = [
                'id' => $account->getId(), 'reduced' => $account->getReduced(), 'surname' => $account->getSurname(),
                'type' => $account->getType(), 'account' => $account->getAccount(), 'category' => $account->getCategory(),
                'description' => $account->getDescription()
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $array,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function importAccounts(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $file = $request->getUploadedFiles();
            $file = $file['file'];
            $row = 1;
            if ($file && $file->getClientFilename()) {
                $folder = UPLOAD_FOLDER . 'financeiro/plano-de-contas/';
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                if ($extension != 'csv') die;
                $target = "{$folder}{$time}.{$extension}";
                $file->moveTo($target);
            }
            if (($handle = fopen($target, "r")) !== FALSE) {
                $separator = ',';
                $data = fgetcsv($handle, 100, ",");
                if (count($data) == 1) $separator = ';';
                $count = 0;
                while (($data = fgetcsv($handle, 100, $separator)) !== FALSE) {
                    $type = $data[4] == 'Debito' ? 1 : 2;
                    $category = $data[5] == 'Receita' ? 1 : 2;
                    if ($count <= 100) {
                        $account = new ChartOfAccounts();
                        if ($data[0] != '') {
                            $account->setReduced($data[0])
                                ->setSurname($data[1])
                                ->setAccount($data[2])
                                ->setDescription($data[3])
                                ->setType($type)
                                ->setCategory($category)
                                ->setActive(1)
                                ->setCreated_at(new \DateTime);
                            $this->em->getRepository(ChartOfAccounts::class)->save($account);
                        }
                    }
                    $count++;
                }
                fclose($handle);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Contas importadas com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function conciliationIugu(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_IUGU_CRIACAO_EDICAO, true);
        $directories = $this->em->getRepository(Directory::class)->findBy([], ['nome' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/conciliationIugu.phtml', 'user' => $user,
            'subMenu' => 'financial', 'subSectionActive' => 'conciliation', 'subSection' => 'conciliationIugu', 'directories' => $directories]);
    }

    public
    function conciliationIugu2(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $data = IuguService::conciliation($data);
            $exists = $absent = 0;
            $transactionsAbsent = [];
            foreach ($data as $d) {
                $value = str_replace('R$ ', '', $d['paid_value']);
                $value = str_replace('.', '', $value);
                $value = str_replace(',', '.', $value);
                $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['gatewayPagamento' => 1, 'valor' => $value, 'invoiceId' => $d['id']]);
                if (!$transaction) {
                    $absent++;
                    array_push($transactionsAbsent, $d['id']);
                } else {
                    $exists++;
                }
            }
            $msg = '';
            if (sizeof($transactionsAbsent) > 0) {
                $resume = ['total' => count($data), 'exists' => $exists, 'absent' => $absent];
                $msg = Messages::conciliationIugu($resume, $transactionsAbsent);
                Email::send('jessica.costa@novo.org.br', '', 'Conciliação IUGU', $msg);
                $msg = "Um e-mail será enviado para você com os detalhes do processamento!";
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => "Conciliação realizada com sucesso!<br>" . $msg,
                'total' => count($data),
                'exists' => $exists,
                'absent' => $absent
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function conciliationRescue(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);
            $data = IuguService::withdraw_conciliations($data);
            $message = "";
            foreach ($data['withdraw_requests'] as $d) {
                $account = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => $directory]);
                if ($d['account_id'] != $account->getAccount()) continue;
                $rescue = IuguService::withdraw_conciliationsRescue($d['id'], $account->getToken());
                $created_at = new \Datetime($rescue['created_at']);
                $paying_at = new \Datetime($rescue['paying_at']);
                $amount = number_format($d['amount'], 2, ',', '.');
                switch ($d['status']) {
                    case 'accepted':
                        $status = "Aceito";
                        break;
                    case 'pending':
                        $status = "Pendente";
                        break;
                    case 'processing';
                        $status = "Processando";
                        break;
                    case 'rejected';
                        $status = "Rejeitado";
                        break;
                    default:
                        $status = "";
                        break;
                }
                $message .= "<ul>
                                <li><b>Código: {$d['id']}</b></li>
                                <li>Data Prevista de Pagamento: <b>{$paying_at->format('d/m/Y')}</b></li>
                                <li>Data Solicitação: <b>{$created_at->format('d/m/Y')}</b></li>
                                <li>Status: <b>{$status}</b></li>
                                <li>Valor: <b>{$amount}</b></li>
                                <li>Transações: <a href='https://gestao.novo.org.br/api/financeiro/transacoes-resgate/{$d['id']}/' target='_blank'><i class='fa fa-file-download' aria-hidden='true'></i> Download</a></li>
                            </ul><hr>";
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function receipts(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_RECIBOS_CRIACAO_EDICAO, true);
        $directories = $this->em->getRepository(Directory::class)->findBy(['ativo' => 'S'], ['nome' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/receipts.phtml', 'section' => 'receipts',
            'user' => $user, 'subMenu' => 'financial', 'directories' => $directories]);
    }

    public
    function generateReceipts(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'directory' => 'Diretório',
                'account' => 'Conta',
                'type' => 'Pesquisar por'
            ];
            if ($data['type'] == 'number') $fields = array_merge($fields, ['begin' => 'Início', 'end' => 'Fim']);
            if ($data['type'] == 'cpf') $fields = array_merge($fields, ['cpf' => 'CPF', 'year' => 'Ano']);
            Validator::requireValidator($fields, $data);
            $fileName = time();
            $file = mkdir(UPLOAD_FOLDER . "recibos/$fileName", 0755, true);
            $numbersReceipt = [];
            if ($data['type'] == 'number') {
                if ($data['begin'] > $data['end']) throw new \Exception("O campo Início não pode ser maior do que o campo Fim!");
                $count = count(range($data['end'], $data['begin']));
                if ($count >= 15) throw new \Exception("Limite de recebibos excedido, tente com um intervalor de no máximo 15 recibos!");
                for ($i = $data['begin']; $i <= $data['end']; $i++) {
                    $idFormat = str_pad($i, 6, '0', STR_PAD_LEFT);
                    array_push($numbersReceipt, strVal($idFormat));
                    $transaction = $this->em->getRepository(Transaction::class)->findReceiptByNumber($data, $idFormat)[0];
                    if ($transaction) PDF::generateReceipt($data, $idFormat, $transaction, $fileName);
                }
            } else if ($data['type'] == 'cpf') {
                $transactions = $this->em->getRepository(Transaction::class)->findReceiptByCpf($data);
                if (!$transactions) throw new \Exception("Não foram encontrados recibos emitidos no ano selecionado!");
                foreach ($transactions as $transaction) {
                    array_push($numbersReceipt, $transaction['reciboCurto']);
                    PDF::generateReceipt($data, $transaction['reciboCurto'], $transaction, $fileName);
                }
            } else {
                throw new \Exception("Solicitação Inválida");
            }
            $message = $this->zip($fileName, $numbersReceipt);
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function zip(string $fileName, array $numbersReceipt): string
    {
        $zip = new \ZipArchive;
        $path = UPLOAD_FOLDER . "recibos"; //local onde o arquivo será salvo
        $fullPath = $path . DIRECTORY_SEPARATOR . "$fileName/$fileName.zip";
        if ($zip->open($fullPath, \ZipArchive::CREATE)) {
            foreach ($numbersReceipt as $n) {
                $zip->addFile(
                    $path . "/$fileName/$n.pdf", "$n.pdf"
                );
            }
            $zip->close();
        }
        return "Recibos gerados com sucesso, <a href='{$this->baseUrl}uploads/recibos/$fileName/$fileName.zip' target='_blank'>Clique aqui</a> para baixar!";
    }

    public
    function importReceipts(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!in_array($user->getId(), array(23, 418, 371, 21))) die;
            $file = $request->getUploadedFiles();
            $file = $file['file'];
            $row = 1;
            if ($file && $file->getClientFilename()) {
                $folder = UPLOAD_FOLDER . 'recibos/lancados/';
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                if ($extension != 'csv') die;
                $target = "{$folder}{$time}.{$extension}";
                $file->moveTo($target);
            }
            if (($handle = fopen($target, "r")) !== FALSE) {
                $separator = ',';
                $data = fgetcsv($handle, 100, ",");
                if (count($data) == 1) $separator = ';';
                $count = 0;
                while (($data = fgetcsv($handle, 100, $separator)) !== FALSE) {
                    if ($count <= 100) {
                        $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $data[0], 'status' => ['paid', 'reversed']]);
                        if ($data[0] != '') {
                            if ($transaction) {
                                $transaction->setNumeroRecibo($data[1]);
                                $this->em->getRepository(Transaction::class)->save($transaction);
                            }
                        }
                    }
                    $count++;
                }
                fclose($handle);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Recibos lançados com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function importDonations(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!in_array($user->getId(), array(23, 418, 371, 21))) die;
            $dataParams = (array)$request->getParams();
            Validator::requireValidator(['directory' => 'Diretório'], $dataParams);
            $file = $request->getUploadedFiles();
            $file = $file['fileDonations'];
            $row = 1;
            if ($file && $file->getClientFilename()) {
                $folder = UPLOAD_FOLDER . 'financeiro/doacoes/';
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                if ($extension != 'csv') die;
                $target = "{$folder}{$time}.{$extension}";
                $file->moveTo($target);
            }
            if (($handle = fopen($target, "r")) !== FALSE) {
                $separator = ',';
                $data = fgetcsv($handle, 500, ",");
                if (count($data) == 1) $separator = ';';
                $count = 0;
                while (($data = fgetcsv($handle, 500, $separator)) !== FALSE) {
                    if ($count <= 500) {
                        if ($data[0] != '') {
                            $today = new \Datetime();
                            $cpf = Utils::formatCpf($data[1]);
                            /** Confere se existe cadastro */
                            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
                            /** Cria cadastro */
                            if (!$user) {
                                $user = new User();
                                $user->setName($data[0])
                                    ->setCpf($data[1])
                                    ->setPassword('40833d08f2c8cec98ea7528e4d05bad3')
                                    ->setTituloEleitoralPais('Brasil')
                                    ->setTituloEleitoralPaisId($this->em->getReference(Country::class, 33))
                                    ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                                    ->setGenero($this->em->getReference(Gender::class, 3))
                                    ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                                    ->setOpcaoReligiosa(11)
                                    ->setTermoAceite0(1)
                                    ->setTermoAceite1(1)
                                    ->setTermoAceite2(1)
                                    ->setTermoAceite3(1)
                                    ->setStatus(1)
                                    ->setDataCriacao($today)
                                    ->setFiliado(0)
                                    ->setStatus(0)
                                    ->setEmailConfirmado(1)
                                    ->setObservacao('Origem: Importação de Doações')
                                    ->setUltimaAtualizacao($today);
                                $this->em->getRepository(User::class)->save($user);

                                /*-----Hubspot/Mailchimp-----*/
                                Hubspot::createContact($user);
                                $this->mailchimpCreateUser($user, null, null);

                            }
                            /** Cria transação */
                            $value = str_replace('.', '', $data[3]);
                            $value = str_replace(',', '.', $value);
                            $paid = str_replace('/', '-', $data[2]);
                            $datePaid = new \Datetime(date('Y-m-d', strtotime($paid)));
                            $transaction = new Transaction;
                            $transaction->setTbPessoaId($user->getId())
                                ->setDataCriacao($today)
                                ->setDataCriacaoOrigem($today)
                                ->setDataPago($datePaid)
                                ->setValor(floatval($value))
                                ->setFormaPagamento(3)
                                ->setOrigemTransacao(1)
                                ->setPeriodicidade(0)
                                ->setGatewayPagamento(5)
                                ->setInvoiceId($data[4])
                                ->setStatus('paid')
                                ->setNumeroRecibo($data[5])
                                ->setUltimaAtualizacao($today)
                                ->setAtualizadoPor($user->getId())
                                ->setTbDiretorioId((int)$dataParams['directory'])
                                ->setTbDiretorioOrigem((int)$dataParams['directory'])
                                ->setObs('Origem: Importação Doações');
                            $this->em->getRepository(Transaction::class)->save($transaction);
                        }
                    }
                    $count++;
                }
                fclose($handle);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Doações lançadas com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function directoryAccounts(Request $request, Response $response, int $type = 2)
    {
        $user = $this->getLogged(false, $type == 3);
        Validator::validatePermission(SystemFeatures::DIRETORIOS_CONTASDEDIRETORIOS_VISUALIZACAO, true);
        $banks = $this->em->getRepository(Bank::class)->findBy([], ['cod' => 'asc']);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $accounts = $this->em->getRepository(ChartOfAccounts::class)->findBy(['active' => 1]);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/directoryAccounts.phtml',
            'user' => $user, 'accounts' => $accounts, 'directories' => $directories, 'banks' => $banks, 'subMenu' => 'directories',
            'states' => $states, 'section' => 'directoryAccounts']);
    }

    public
    function directoryExtract(Request $request, Response $response, int $type = 2)
    {
        $user = $this->getLogged(false, $type == 3);
        Validator::validatePermission(SystemFeatures::DIRETORIOS_CONTASDEDIRETORIOS_VISUALIZACAO, true);
        $begin = $request->getQueryParam('begin');
        $end = $request->getQueryParam('end');
        $subMenu = $user->getType() == 3 ? 'records' : 'search';
        $id = $request->getAttribute('route')->getArgument('account');
        $account = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['id' => $id]);
        if (in_array($user->getLevel(), [1, 2])) {
            if ($user->getState()->getId() != $account->getDirectory()->getStateId()) $this->redirectByPermissions();
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/directoryExtract.phtml', 'user' => $user,
            'subMenu' => $subMenu, 'section' => 'directoryAccounts', 'subSectionActive' => 'register', 'account' => $account,
            'begin' => $begin, 'end' => $end
        ]);
    }

    public
    function SaveDirectoryAccount(Request $request, Response $response)
    {
        try {
            $this->getLogged();

            $data = (array)$request->getParams();
            Validator::requireValidator([
                'directoryModal' => 'Diretório',
                'nicknameModal' => 'Apelido',
                'bankModal' => 'Banco',
                'agencyModal' => 'Agencia',
                'accountModal' => 'Conta',
                'dateStart' => 'Marco Zero'
            ], $data);

            // Busca o diretório
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directoryModal']]);
            if (!$directory) throw new \Exception("Diretório inválido!");

            // Busca o banco
            $bank = $this->em->getRepository(Bank::class)->findOneBy(['name' => $data['bankModal']]);
            if (!$bank) throw new \Exception("Banco inválido!");

            // Verifica se a conta já existe para o diretório
            //$validAccount = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['directory' => $directory->getId(), 'nickname' => $data['nicknameModal']]);
            //if (!$data["accountId"] && $validAccount) throw new \Exception("Conta já cadastrada para este Diretório!");

            // Verifica se a agência possui dígito
            if (!strpos($data['agencyModal'], "-")) throw new \Exception("Insira o digito da agência.", 1);
            $data['agencyModal'] = str_replace(".", "", $data['agencyModal']);
            $data['agencyModal'] = explode("-", $data['agencyModal']);

            // Verifica se a conta possui dígito
            if (!strpos($data['accountModal'], "-")) throw new \Exception("Insira o digito da conta.", 1);
            $data['accountModal'] = str_replace(".", "", $data['accountModal']);
            $data['accountModal'] = explode("-", $data['accountModal']);

            // Cria o registro
            $register = new DirectoryAccounts();
            if ($data["accountId"]) $register = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['id' => $data["accountId"]]);
            $register->setDirectory($directory)
                ->setNickname($data['nicknameModal'])
                ->setBank($bank)
                ->setAgency($data['agencyModal'][0])
                ->setAgencyDigit($data['agencyModal'][1])
                ->setAccount($data['accountModal'][0])
                ->setAccountDigit($data['accountModal'][1])
                ->setDateStart(new \DateTime($data['dateStart']) ?: null)
                ->setValueStart(Utils::moneyToFloat($data['valueStart']) ?: null);
            if ($data['accountingAccount']) {
                $account = explode('/', $data['accountingAccount']);
                $account = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($account[0])]);
                if (!$account) throw new \Exception("Conta débito não encontrada!");
                $register->setAccountingAccount($account);
            }
            $this->em->getRepository(DirectoryAccounts::class)->save($register);

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function SaveDirectoryExtract(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'accountId' => 'Conta',
                'dateStart' => 'Data Inicio',
                'dateEnd' => 'Data Fim'
            ], $data);
            $file = $request->getUploadedFiles();
            $attachmentExtract = $file['attachmentExtract'];
            if ($attachmentExtract && $attachmentExtract->getClientFilename()) {
                $time = time();
                $folder = UPLOAD_FOLDER . 'financeiro/extratos/';
                $extension = explode('.', $attachmentExtract->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}attachmentBillet.{$extension}";
                $attachmentExtract->moveTo($target);
            } else {
                throw new \Exception("Anexo não encontrado");
            }
            $directoryExtract = new DirectoryExtract();
            $directoryExtract->setUser($user)
                ->setAccount($data['accountId'])
                ->setDateStart(new \Datetime($data['dateStart']))
                ->setDateEnd(new \Datetime($data['dateEnd']))
                ->setAttachmentExtract($target)
                ->setStatus(true);
            $this->em->getRepository(DirectoryExtract::class)->save($directoryExtract);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Extato salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function DeleteExtract(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('extract');

            $extract = $this->em->getRepository(DirectoryExtract::class)->find($id);
            $extract->setStatus(false);
            $this->em->getRepository(DirectoryExtract::class)->save($extract);

            $this->newAccessLog($user, "change extract $id - delete");

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Extrato deletado com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function getDirectoryAccounts(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']])->getId();
            $directoryAccount = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['directory' => $directory, 'nickname' => $data['type']]);
            if ($data['account'] != "") {
                $directoryAccount = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['directory' => $directory, 'nickname' => $data['type'], 'account' => $data['account']]);
            }
            $array = [];
            if ($directoryAccount) {
                $array = [
                    'id' => $directoryAccount->getDirectory()->getId(),
                    'bank' => $directoryAccount->getBank()->getName(),
                    'agency' => $directoryAccount->getAgencyString(),
                    'account' => $directoryAccount->getAccountString()
                ];
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $array,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function accountsByDirectory(Request $request, Response $response, bool $type)
    {
        try {
            $nome = $request->getQueryParam('directory');
            $origin = $request->getQueryParam('origin');
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $nome]);
            $directoryState = null;
            if ($directory->getMunicipal() === 'S') $directoryState = $this->em->getRepository(Directory::class)
                ->findOneBy(['tbEstadoId' => $directory->getStateId(), 'estadual' => 'S', 'ativo' => 'S']);
            $directoryAccounts = $this->em->getRepository(DirectoryAccounts::class)->accountsByDirectory($directory, $origin, $directoryState, $type);
            $array = [];
            foreach ($directoryAccounts as $account) {
                $array[] = [
                    'id' => $account->getId(),
                    'nickname' => $account->getNickname(),
                    'startDate' => $account->getDateStart()->add(new \DateInterval('P1D'))->format('d/m/Y'),
                    'name' => "{$account->getBank()->getName()} - {$account->getAgencyString()} - {$account->getAccountString()}/{$account->getDirectory()->getName()} - {$account->getNicknameString()}"
                ];
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $array,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function costCenter(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_ORCAMENTOS_CENTRODECUSTO_VISUALIZACAO, true);
        $users = $this->em->getRepository(UserAdmin::class)->findBy(['level' => 3, 'active' => 1, 'type' => 3], ['name' => 'asc']);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $areas = $this->em->getRepository(Area::class)->findBy([], ['name' => 'ASC']);
        $subAreas = $this->em->getRepository(SubArea::class)->findBy([], ['name' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/costCenter.phtml',
            'subSection' => 'costCenter', 'users' => $users, 'user' => $user, 'subMenu' => 'records', 'title' => 'Centro de Custo',
            'directories' => $directories, 'areas' => $areas, 'subAreas' => $subAreas, 'subSectionActive' => 'budgets']);
    }

    public
    function costCenterList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CADASTROS_ORCAMENTOS_CENTRODECUSTO_VISUALIZACAO, true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $filter = $request->getQueryParams();
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) $filter['id'] = $id;
        $costCenter = $this->em->getRepository(CostCenter::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(CostCenter::class)->listTotal($request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($costCenter);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $costCenter,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function costCenterSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $data = (array)$request->getParams();
            $msg = "Centro de Custo cadastrado com sucesso!";
            $fields = [
                'name' => 'Nome',
                'responsible' => 'Responsável',
                'directory' => 'Diretório',
            ];
            Validator::requireValidator($fields, $data);

            $costCenter = new CostCenter();
            if ($data['costCenterId'] > 0) {
                $costCenter = $this->em->getRepository(CostCenter::class)->find($data['costCenterId']);
                $msg = "Centro de Custo editado com sucesso!";
            }

            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);

            $costCenter->setResponsible($this->em->getReference(UserAdmin::class, $data['responsible']))
                ->setName($data['name'])
                ->setDirectory($directory)
                ->setStatus(1);
            $this->em->getRepository(CostCenter::class)->save($costCenter);

            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public
    function costCenterDelete(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $id = $request->getAttribute('route')->getArgument('id');
            $costCenter = $this->em->getRepository(CostCenter::class)->find($id);
            $costCenter->setStatus(0);
            $this->em->getRepository(CostCenter::class)->save($costCenter);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Centro de Custo excluído com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public
    function contracts(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_CONTRATOS_VISUALIZACAO, true);
        $costCenters = $this->em->getRepository(CostCenter::class)->findBy(['status' => 1], ['name' => 'asc']);
        $contracts = $this->em->getRepository(Contract::class)->getContracts();
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $availableNumber = $this->em->getRepository(Contract::class)->getNumberAvailable()['avaibleNumber'];
        $accounts = $this->em->getRepository(ChartOfAccounts::class)->findBy(['active' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/contracts.phtml', 'section' => 'contracts',
            'availableNumber' => $availableNumber, 'user' => $user, 'title' => 'Contratos', 'costCenters' => $costCenters,
            'contracts' => $contracts, 'directories' => $directories, 'accounts' => $accounts, 'subMenu' => 'records']);
    }

    public
    function contractsView(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_CONTRATOS_VISUALIZACAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $contract = $this->em->getRepository(Contract::class)->find($id);
        $additions = $this->em->getRepository(Contract::class)->findBy(['contract' => $contract]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/contractsView.phtml', 'section' => 'contracts',
            'user' => $user, 'subMenu' => 'records', 'title' => 'Detalhes do Contrato', 'additions' => $additions,
            'contract' => $contract]);
    }

    public
    function contractsList(Request $request, Response $response)
    {

        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CADASTROS_CONTRATOS_VISUALIZACAO, true);
        $index = $request->getQueryParam('index') ?? 0;
        $limit = $request->getQueryParam('limit') ?? 1;
        $filter = $request->getQueryParams();
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) {
            $filter['id'] = $id;
            $filter['seq'] = '';
        }
        $contracts = $this->em->getRepository(Contract::class)->list($user, $filter, $limit, $index * $limit);
        $total = $this->em->getRepository(Contract::class)->listTotal($user, $request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($contracts);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $contracts,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function detailContract(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        //if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $contract = $this->em->getRepository(Contract::class)->find($id);
        $cpf = strlen($contract->getCpf()) == 11 ? Utils::mask($contract->getCpf(), '###.###.###-##') : Utils::mask($contract->getCpf(), '##.###.###-####/##');
        $arr = [
            'name' => $contract->getName(),
            'cpf' => $cpf
        ];
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function delete(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        //if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $contract = $this->em->getRepository(Contract::class)->find($id);
        $contract->setStatus(0)
            ->setUserCancel($user)
            ->setDateCancel(new \DateTime());

        $this->em->getRepository(Contract::class)->save($contract);
        return $response->withJson([
            'status' => 'ok',
            'message' => 'Contrato excluído com sucesso!'
        ], 201)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function contractsSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            //if (!$user->isAdminNational()) $this->redirectByPermissions();
            $data = (array)$request->getParams();
            $files = $request->getUploadedFiles();
            $msg = "Contrato cadastrado com sucesso!";
            $fields = [
                'name' => 'Razão Social/Nome',
                'directory' => 'Diretório',
                'cpf' => 'CNPJ/CPF',
                'costCenter' => 'Centro de Custo',
                'category' => 'Categoria',
                'stage' => 'Status',
                'situation' => 'Situação',
                'start' => 'Data Inicial',
                'object' => 'Objeto do Contrato'
            ];
            $fields2 = [];

            if ($data['additive'] == 1) {
                $fields2 = [
                    'contract' => 'Contrato'
                ];
                $fields = array_merge($fields, $fields2);
            }

            if ($data['category'] == 1) {
                $fields2 = [
                    'monthlyValue' => 'Valor Mensal'
                ];
            } elseif ($data['category'] == 2) {
                $fields2 = [
                    'monthlyValue' => 'Valor Mensal',
                    'value' => 'Valor Global'
                ];
            } elseif ($data['category'] == 3 || $data['category'] == 4) {
                $fields2 = [
                    'value' => 'Valor Global'
                ];
            }
            $fields = array_merge($fields, $fields2);
            if ($data['situation'] == 1 || $data['situation'] == 3) {
                $fields2 = [
                    'end' => 'Data Final'
                ];
                $fields = array_merge($fields, $fields2);
            }
            Validator::requireValidator($fields, $data);
            Validator::validDate($data['start']);
            if ($data['end'] != '') Validator::validDate($data['end']);

            $contract = new Contract();
            if ($data['contractId'] > 0) {
                $contract = $this->em->getRepository(Contract::class)->find($data['contractId']);
                $msg = "Contrato editado com sucesso!";
            } else {
                $number = $data['additive'] ? $data['numberAdditive'] : $data['number'];
                $checkNumber = $this->em->getRepository(Contract::class)->findOneBy(['number' => $number]);
                if ($number != "" && $checkNumber) throw new \Exception("Parece que você inseriu um número de contrato que já está sendo usado, utilize outro número!");
            }
            $contract = $this->moveContracFile($files['contractDoc'], $contract);
            $number = match ((int)$data['additive']) {
                1 => $data['numberAdditive'],
                default => $this->randeCodeContract($data['number'], $data['contractId'])
            };
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);
            $contract->setCostCenter($this->em->getReference(CostCenter::class, $data['costCenter']))
                ->setStatus(1)
                ->setEnd(\DateTime::createFromFormat('d/m/Y', $data['end']) ? \DateTime::createFromFormat('d/m/Y', $data['end']) : null)
                ->setStart(\DateTime::createFromFormat('d/m/Y', $data['start']) ? \DateTime::createFromFormat('d/m/Y', $data['start']) : null)
                ->setValue($data['value'] != '' ? Utils::moneyToFloat($data['value']) : null)
                ->setMonthlyValue($data['monthlyValue'] != '' ? Utils::moneyToFloat($data['monthlyValue']) : null)
                ->setSituation($data['situation'])
                ->setCategory($data['category'])
                ->setName($data['name'])
                ->setSignatureDate($data['signatureDate'] != '' ? \DateTime::createFromFormat('d/m/Y', $data['signatureDate']) : null)
                ->setObject($data['object'])
                ->setCpf($data['cpf'])
                ->setNumber($number)
                ->setStage($data['stage'])
                ->setDirectory($directory);
            if ($data['account']) {
                $account = explode('/', $data['account']);
                $account = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($account[0])]);
                if (!$account) throw new \Exception("Conta débito não encontrada!");
                $contract->setAccount($account);
            }
            if ($data['additive'] == 1) {
                $contractNumber = explode(" ", $data['contract']);
                $contractId = $this->em->getRepository(Contract::class)->findOneBy(['number' => $contractNumber[0]]);
                $contract->setContract($contractId);
            }
            $this->em->getRepository(Contract::class)->save($contract);
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    private
    function randeCodeContract($number = null, $id): string
    {
        if (!$number) {
            $verify = $this->em->getRepository(Contract::class)->searchCode();
            $code = 1 . '/' . date('Y');
            if ($verify) {
                $number = explode('/', $verify[0]['number']);
                $code = intval($number[0]) + 1;
                $code = $code . '/' . date('Y');
            }
        } else {
            $arr = explode('/', $number);
            $code = intval($arr[0]) . '/' . $arr[1];
            $verify = $this->em->getRepository(Contract::class)->findOneBy(['number' => $code]);
            if ($verify && $verify->getId() != $id) throw new \Exception('Número de contrato já cadastrado!');
        }
        return str_pad($code, 9, '0', STR_PAD_LEFT);
    }

    private
    function moveContracFile($file, Contract $contract): Contract
    {
        $folder = UPLOAD_FOLDER . 'financeiro/';
        if ($file && $file->getClientFilename()) {
            $time = time();
            $extension = explode('.', $file->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}contract.{$extension}";
            $file->moveTo($target);
            $contract->setDoc($target);
        }
        return $contract;
    }

    public
    function contractsDelete(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            //if (!$user->isAdminNational()) $this->redirectByPermissions();
            $id = $request->getAttribute('route')->getArgument('id');
            $contract = $this->em->getRepository(Contract::class)->find($id);
            $contract->setStatus(0);
            $this->em->getRepository(Contract::class)->save($contract);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Centro de Custo excluído com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public
    function saveOriginating(Request $request, Response $response, UserAdmin $user, array $data)
    {
        try {
            if (!$user->isAdminNational() && !in_array($data['requestStatus'], [2, 3, 5, 7])) {
                $this->checkValidPaymentDate($user, $data['dateTransfer']);
            }

            if ($data['id'] > 0) {
                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['id']);
                $directoryOrigin = $requeriment->getDirectory();
                $directoryDestiny = $requeriment->getDirectoryTransfer();
            } else {
                Validator::requireValidator([
                    'transferDirectoryIdOrigin' => 'Diretório de Origem',
                    'transferDirectoryIdDestiny' => 'Diretório de Destino',
                    'transferAccountTypeOrigin' => 'Tipo de Conta de Origem',
                    'transferAccountTypeDestiny' => 'Tipo de Conta de Destino'
                ], $data);
                $directoryOrigin = $this->em->getRepository(Directory::class)->findOneBy(['id' => $data['transferDirectoryIdOrigin']]);
                $directoryDestiny = $this->em->getRepository(Directory::class)->findOneBy(['id' => $data['transferDirectoryIdDestiny']]);

                $requeriment = new PaymentRequirements();
                $requeriment->setUser($user);
            }
            if (!$directoryOrigin || !$directoryDestiny) throw new \Exception("Solicitação Inválida!");

            Validator::requireValidator([
                'valueTransfer' => 'Valor da Transferência',
                'dateTransfer' => 'Data da Transferência',
                'transferCostCenter' => 'Centro de Custo',
                'transferArea' => 'Area'
            ], $data);

            $provider = $this->em->getRepository(Provider::class)->findOneBy(['name' => Utils::onlyNumbers($data['transferDirectoryDestiny'])]);
            if (!$provider && $data['transferCnpjDestiny'] != "") {
                $provider = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => Utils::onlyNumbers($data['transferCnpjDestiny'])]);
            }

            if (!$provider) $provider = new Provider();
            $provider->setName($data['transferDirectoryDestiny'])
                ->setCpfCnpj($data['transferCnpjDestiny'])
                ->setType(2)
                ->setPaymentMethod(2);
            $this->em->getRepository(Provider::class)->save($provider);

            $valueTransfer = $this->discountsValue(Utils::moneyToFloat($data['valueTransfer']), $data['discounts']);
            $directoryAccount = $this->em->getRepository(DirectoryAccounts::class)
                ->findOneBy(['directory' => $directoryOrigin->getId(), 'nickname' => $data['origin']]);

            $requeriment->setOrigin($data['origin'])
                ->setType($data['type'])
                ->setDescription($data['description'])
                ->setDirectory($directoryOrigin)
                ->setDirectoryTransfer($directoryDestiny)
                ->setPaymentMethod(2)
                ->setPaymentValue($valueTransfer)
                ->setPaymentDueDate(new \Datetime($data['dateTransfer']))
                ->setPaymentDoc($data['transferCnpjDestiny'])
                ->setPaymentName($data['transferDirectoryDestiny'])
                ->setPaymentBank($this->em->getReference(Bank::class, 1))
                ->setPaymentBankAgency($data['transferAgencyDestiny'])
                ->setPaymentBankAccount($data['transferAccountDestiny'])
                ->setPaymentBankAccountType('current')
                ->setProvider($provider)
                ->setCostCenter($this->em->getReference(CostCenter::class, $data['transferCostCenter']))
                ->setArea($this->em->getReference(Area::class, $data['transferArea']))
                ->setApproved(PaymentRequirements::APPROVED_USER)
                ->setDirectoryAccount($directoryAccount);

            if ($data['requestStatus'] != "") {
                $requeriment->setRequestStatus($data['requestStatus']);
                if ($data['requestStatus'] == 1) $requeriment->setPendency($data['pendency']);
                if (in_array($data['requestStatus'], [3, 8])) {
                    if ($data['payDay'] == '' || $data['directoryAccount'] == '' || !$data['directoryAccount'])
                        throw new Exception('Preencha todos os dados necessários!');
                    $requeriment->setPayDay(\DateTime::createFromFormat('d/m/Y', $data['payDay']));

                    if ($data['requestStatus'] == 8) $requeriment->setRefundDate(new \Datetime($data['refundDate']));
                }
            }
            $files = $request->getUploadedFiles();
            $this->moveAttachmentsContractPayment($files, $requeriment);
            $this->moveOtherFilesRequirements($requeriment, $files);

            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_USER);

            if (in_array($data['requestStatus'], [3, 8])) $this->registerAccountingRecord($user, $data, $requeriment);
            if ($data['originatingTransactions'] && in_array($data['origin'], [1, 2, 3])) {
                $total = 0;
                foreach ($data['originatingTransactions'] as $originatingTransaction) {
                    $value = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $originatingTransaction])->getValorSemTaxas();
                    $splitTransaction = $this->em->getRepository(TransferSplit::class)->findOneBy(['transaction' => $originatingTransaction]);
                    if ($splitTransaction) $value = $splitTransaction->getNewValue();
                    $total += $value;
                }
                $total = number_format($total, 2, '.', '');
                $data['valueTransfer'] = str_replace('.', '', $data['valueTransfer']);
                $data['valueTransfer'] = str_replace(',', '.', $data['valueTransfer']);
                $data['valueTransfer'] = str_replace('R$ ', '', $data['valueTransfer']);
                if ((float)$total != (float)$data['valueTransfer']) throw new \Exception("Valor de transferência inválido ou diferente do valor solicitado!");
                foreach ($data['originatingTransactions'] as $originatingTransaction) {
                    $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $originatingTransaction]);
                    $originating = new OriginatingTransaction();
                    $originating->setRequirement($requeriment)
                        ->setTransaction($transaction);
                    $this->em->getRepository(OriginatingTransaction::class)->save($originating);
                    $owner = $this->em->getRepository(OriginatingOwner::class)->findOneBy(['transaction' => $transaction->getId()]);
                    if (!$owner) $owner = new OriginatingOwner();
                    $owner->setDirectory($directoryDestiny)
                        ->setTransaction($transaction);
                    $this->em->getRepository(OriginatingOwner::class)->save($owner);
                }
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function saveOriginatingCandidate(Request $request, Response $response, UserAdmin $user, array $data)
    {
        try {
            if (!$user->isAdminNational() && !in_array($data['requestStatus'], [2, 3, 5, 7])) {
                $this->checkValidPaymentDate($user, $data['dateTransferCandidate']);
            }
            if ($data['id'] > 0) {
                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['id']);
                $requeriment->setOrigin($data['origin'])
                    ->setDescription($data['description']);
            } else {
                $requeriment = new PaymentRequirements();
                $requeriment->setUser($user);
                Validator::requireValidator([
                    'directoryCandidate' => 'Diretório',
                    'cnpjCandidate' => 'CNPJ Candidato',
                    'nameCandidate' => 'Nome Candidato',
                    'bankCandidate' => 'Banco',
                    'agencyCandidate' => 'Agencia',
                    'accountCandidate' => 'Conta',
                    //'pixCandidate' => 'Pix',
                    'valueTransferCandidate' => 'Valor da transferência',
                    'dateTransferCandidate' => 'Data da transferência',
                    'transferCostCenterCandidate' => 'Centro de Custo',
                    'transferAreaCandidate' => 'Área',
                ], $data);
                $directoryCandidate = explode("/", $data['directoryCandidate']);
                $directoryCandidate = trim($directoryCandidate[1] . '/' . $directoryCandidate[2]);
                $directoryCandidate = $this->em->getRepository(Directory::class)->findOneBy(['cnpj' => $directoryCandidate]);
                if (!$directoryCandidate) throw new \Exception("Solicitação Inválida!");
                $provider = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => Utils::onlyNumbers($data['cnpjCandidate'])]);
                if (!$provider) $provider = new Provider();
                // register provider
                $provider->setName($data['nameCandidate'])
                    ->setCpfCnpj($data['cnpjCandidate'])
                    ->setType(2)
                    ->setBank($this->em->getReference(Bank::class, $data['bankCandidate'][0]))
                    ->setAgency($data['agencyCandidate'])
                    ->setAccount($data['accountCandidate'])
                    ->setPaymentMethod(2);
                $this->em->getRepository(Provider::class)->save($provider);
                //register requirement
                $valueTransferCandidate = $this->discountsValue(Utils::moneyToFloat($data['valueTransferCandidate']), $data['discounts']);
                $requeriment->setOrigin($data['origin'])
                    ->setType($data['type'])
                    ->setDescription($data['description'])
                    ->setDirectory($directoryCandidate)
                    ->setPaymentMethod(2)
                    ->setPaymentValue($valueTransferCandidate)
                    ->setPaymentDueDate(new \Datetime($data['dateTransferCandidate']))
                    ->setPaymentDoc($data['cnpjCandidate'])
                    ->setPaymentName($data['nameCandidate'])
                    ->setPaymentBank($this->em->getReference(Bank::class, $data['bankCandidate'][0]))
                    ->setPaymentBankAgency($data['agencyCandidate'])
                    ->setPaymentBankAccount($data['accountCandidate'])
                    ->setPaymentBankAccountType('current')
                    ->setPixKey($data['pixCandidate'])
                    ->setProvider($provider)
                    ->setCostCenter($this->em->getReference(CostCenter::class, $data['transferCostCenterCandidate']))
                    ->setArea($this->em->getReference(Area::class, $data['transferAreaCandidate']))
                    ->setApproved(PaymentRequirements::APPROVED_USER);
            }
            if ($data['requestStatus']) {
                $requeriment->setRequestStatus($data['requestStatus']);
                if ($data['requestStatus'] == 1) $requeriment->setPendency($data['pendency']);
                if (in_array($data['requestStatus'], [3, 8])) {
                    if ($data['payDay'] == '' || $data['directoryAccount'] == '' || !$data['directoryAccount']) throw new Exception('Preencha todos os dados necessários!');
                    $requeriment->setPayDay(\DateTime::createFromFormat('d/m/Y', $data['payDay']))
                        ->setDirectoryAccount($this->em->getReference(DirectoryAccounts::class, $data['directoryAccount']));

                    if ($data['requestStatus'] == 8) $requeriment->setRefundDate(new \Datetime($data['refundDate']));
                }
            }
            $files = $request->getUploadedFiles();
            $this->moveAttachmentsContractPayment($files, $requeriment);
            $this->moveOtherFilesRequirements($requeriment, $files);
            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_USER);

            if (in_array($data['requestStatus'], [3, 8])) $this->registerAccountingRecord($user, $data, $requeriment);
            if ($data['originatingTransactions'] && in_array($data['origin'], [1, 2, 3])) {
                $total = 0;
                foreach ($data['originatingTransactions'] as $originatingTransaction) {
                    $value = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $originatingTransaction])->getValorSemTaxas();
                    $splitTransaction = $this->em->getRepository(TransferSplit::class)->findOneBy(['transaction' => $originatingTransaction]);
                    if ($splitTransaction) $value = $splitTransaction->getNewValue();
                    $total += $value;
                }
                $total = number_format($total, 2, '.', '');
                $data['valueTransferCandidate'] = str_replace('.', '', $data['valueTransferCandidate']);
                $data['valueTransferCandidate'] = str_replace(',', '.', $data['valueTransferCandidate']);
                $data['valueTransferCandidate'] = str_replace('R$ ', '', $data['valueTransferCandidate']);
                if ((float)$total != $data['valueTransferCandidate']) throw new \Exception("Valor de transferência inválido ou diferente do valor solicitado!");
                foreach ($data['originatingTransactions'] as $originatingTransaction) {
                    $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $originatingTransaction]);
                    $originating = new OriginatingTransaction();
                    $originating->setRequirement($requeriment)
                        ->setTransaction($transaction);
                    $this->em->getRepository(OriginatingTransaction::class)->save($originating);
                }
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function originatingTransactions(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $data['value'] = Utils::moneyToFloat($data['value']);
            $beginValue = Utils::moneyToFloat($data['beginValue']);
            $endValue = Utils::moneyToFloat($data['endValue']);
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']])->getId();
            if ($data['requirement'] != 0) {
                $requirement = $this->em->getRepository(PaymentRequirements::class)->find($data['requirement']);
                if (!$requirement) throw new \Exception("Não foi possível encontrar o pagamento");
                $data['paymentDueDate'] = $requirement->getPaymentDueDate()->format('Y-m-d');
            }
            $transactions = $this->em->getRepository(Transaction::class)->getOriginatingTransactions($data, $directory, $beginValue, $endValue);
            $transactionsIncluded = $transactionsArray = [];
            if ($data['transaction']) $transactionsIncluded = $this->em->getRepository(Transaction::class)->getTransactionsIncluded($data['transaction']);
            foreach ($transactions as $transaction) {
                $value += $transaction['value'];
                array_push($transactionsArray, $transaction);
                if ($value >= $data['value']) break;
            }
            foreach ($transactionsIncluded as $transactionIncluded) {
                $value += $transactionIncluded['value'];
                array_push($transactionsArray, $transactionIncluded);
            }
            $total = 0;
            foreach ($transactionsArray as $transactionArray) {
                $total += $transactionArray['value'];
            }
            $total = floatVal(number_format($total, 2, '.', ''));
            return $response->withJson([
                'status' => 'ok',
                'total' => $total,
                'message' => $transactionsArray,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function splitTransactions(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'splitTransactionId' => 'ID Transação',
                'splitTransactionDiscount' => 'Deconto Transação'
            ], $data);
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $data['splitTransactionId']]);
            if (!$transaction) throw new \Exception("Transação não encontrada");
            $verifySplitTransaction = $this->em->getRepository(TransferSplit::class)->findOneBy(['transaction' => $data['splitTransactionId']]);
            if ($verifySplitTransaction) throw new \Exception("Transação Inválida para Split");
            $data['splitTransactionDiscount'] = str_replace('.', '', $data['splitTransactionDiscount']);
            $data['splitTransactionDiscount'] = str_replace(',', '.', $data['splitTransactionDiscount']);
            if ((float)$data['splitTransactionDiscount'] > $transaction->getValorSemTaxas()) throw new \Exception("Valor de desconto excede o valor total da transação!");
            $newValue = $transaction->getValorSemTaxas() - $data['splitTransactionDiscount'];
            $split = new TransferSplit();
            $split->setTransaction($transaction)
                ->setNewValue($newValue)
                ->setDiscount($data['splitTransactionDiscount']);
            $this->em->getRepository(TransferSplit::class)->save($split);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function management(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_ORCAMENTOS_AREA_SUB_AREA_VISUALIZACAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/management.phtml', 'subSection' => 'management',
            'subSectionActive' => 'budgets', 'user' => $user, 'subMenu' => 'records', 'directories' => $directories]);
    }

    public
    function areaSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'name' => 'Nome',
            ], $data);
            $msg = 'Área salva com sucesso!';

            $area = new Area();
            if ($data['areaId'] > 0) {
                $area = $this->em->getRepository(Area::class)->find($data['areaId']);
                $msg = 'Área editada com sucesso!';
                $this->em->getRepository(Area::class)->delDirectories($data['areaId']);
            }

            $area->setName($data['name']);

            foreach ($data['directories'] as $directory) {
                $area->addDirectory($this->em->getReference(Directory::class, $directory));
            }

            $this->em->getRepository(Area::class)->save($area);

            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function areaList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CADASTROS_ORCAMENTOS_AREA_SUB_AREA_VISUALIZACAO, true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $filter = [];
        $contracts = $this->em->getRepository(Area::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(Area::class)->listTotal($request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($contracts);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $contracts,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function areaData(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $data = $this->em->getRepository(Area::class)->data($id);
        return $response->withJson([
            'status' => 'ok',
            'message' => $data,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function subAreaSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'name' => 'Nome',
                'area' => 'Área',
            ], $data);
            $msg = 'Sub-área salva com sucesso!';

            $area = new SubArea();
            if ($data['subAreaId'] > 0) {
                $area = $this->em->getRepository(SubArea::class)->find($data['subAreaId']);
                $msg = 'Sub-área editada com sucesso!';
            }

            $area->setName($data['name'])
                ->setArea($this->em->getReference(Area::class, $data['area']));
            $this->em->getRepository(SubArea::class)->save($area);
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function subAreaList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CADASTROS_ORCAMENTOS_AREA_SUB_AREA_VISUALIZACAO, true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $filter = [];
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) $filter['id'] = $id;
        $contracts = $this->em->getRepository(SubArea::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(SubArea::class)->listTotal($request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($contracts);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $contracts,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function subAreaByArea(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $arr = [];
        $subAreas = $this->em->getRepository(SubArea::class)->findBy(['area' => $id], ['name' => 'ASC']);
        foreach ($subAreas as $subArea) {
            $arr[] = [
                'id' => $subArea->getId(),
                'name' => $subArea->getName()
            ];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public
    function getCnpjGru(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);
            $array = [];
            if ($directory) {
                $cnpjGru = $this->em->getRepository(CnpjGru::class)->findOneBy(['directory' => $directory->getId()]);
                if ($cnpjGru) {
                    $array = [
                        'cnpj' => $cnpjGru->getCnpj(),
                        'treTse' => $cnpjGru->getTreTse()
                    ];
                }
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $array,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private
    function saveGRU($request, $response, $userAdmin, $data)
    {
        try {
            $data['gruValue'] = $this->discountsValue(Utils::moneyToFloat($data['gruValue']), $data['discounts']);
            if ($data['id'] > 0) {
                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['id']);
                $requeriment->setOrigin($data['origin'])
                    ->setPaymentValue($data['gruValue'])
                    ->setNfValue($data['gruValue'])
                    ->setPaymentDueDate(new \Datetime($data['gruDate']))
                    ->setBarCode($data['gruLine'])
                    ->setRequestStatus($data['requestStatus'])
                    ->setTypeDocument((int)$data['typeDocument']);
            } else {
                $requeriment = new PaymentRequirements();
                $requeriment->setUser($userAdmin);
                Validator::requireValidator([
                    'gruDirectory' => 'Diretório',
                    'gruTseTre' => 'TSE/TRE',
                    'gruCnpjTseTre' => 'CNPJ',
                    'gruValue' => 'Valor',
                    'gruDate' => 'Data',
                    'gruLine' => 'Linha Digitável',
                    'gruCostCenter' => 'Centro de Custo',
                    'gruArea' => 'Area'
                ], $data);
                $gruDirectory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['gruDirectory']]);
                if (!$gruDirectory) throw new \Exception("Solicitação Inválida!");
                $provider = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => Utils::onlyNumbers($data['gruCnpjTseTre'])]);
                if (!$provider) $provider = new Provider();
                $provider->setName($data['gruTseTre'])
                    ->setCpfCnpj($data['gruCnpjTseTre'])
                    ->setType(2)
                    ->setPaymentMethod(2);
                $this->em->getRepository(Provider::class)->save($provider);
                $requeriment->setOrigin($data['origin'])
                    ->setType($data['type'])
                    ->setDescription($data['description'])
                    ->setDirectory($gruDirectory)
                    ->setPaymentDueDate(new \Datetime($data['gruDate']))
                    ->setPaymentValue($data['gruValue'])
                    ->setPaymentDoc($data['gruCnpjTseTre'])
                    ->setPaymentName($data['gruTseTre'])
                    ->setProvider($provider)
                    ->setDirectory($gruDirectory)
                    ->setBarCode($data['gruLine'])
                    ->setRequestStatus($data['requestStatus'])
                    ->setApproved(PaymentRequirements::APPROVED_USER);
            }
            if ($data['requestStatus'] == 1) $requeriment->setPendency($data['pendency']);
            if ($data['requestStatus'] == 2) $requeriment->setScheduledDate(\DateTime::createFromFormat('d/m/Y', $data['scheduledDate']));

            if (in_array($data['requestStatus'], [3, 8])) {
                $requeriment->setPayDay(\DateTime::createFromFormat('d/m/Y', $data['payDay']));
                if ($data['requestStatus'] == 8) $requeriment->setRefundDate(new \Datetime($data['refundDate']));
            }

            $files = $request->getUploadedFiles();
            $attachmentGRU = $files['attachmentGRU'];
            $folder = UPLOAD_FOLDER . 'financeiro/pagamentos/';
            if ($attachmentGRU && $attachmentGRU->getClientFilename()) {
                $time = time();
                $extension = explode('.', $attachmentGRU->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}attachmentGRU.{$extension}";
                $attachmentGRU->moveTo($target);
                $requeriment->setAttachmentGRU($target);
            }
            if (!$data['gruArea']) $data['gruArea'] = $data['area'];
            if (!$data['gruCostCenter']) $data['gruCostCenter'] = $data['costCenter'];

            $requeriment->setArea($this->em->getReference(Area::class, $data['gruArea']))
                ->setCostCenter($this->em->getReference(CostCenter::class, $data['gruCostCenter']))
                ->setTypeDocument(3)
                ->setNfValue($data['gruValue']);
            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_USER);

            $cnpjGru = $this->em->getRepository(CnpjGru::class)->findOneBy(['cnpj' => $data['gruCnpjTseTre']]);
            if (!$cnpjGru) {
                $cnpjGru = new CnpjGru();
                $cnpjGru->setDirectory($gruDirectory)
                    ->setCnpj($data['gruCnpjTseTre'])
                    ->setTreTse($data['gruTseTre']);
                $this->em->getRepository(CnpjGru::class)->save($cnpjGru);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function updateApproved(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'requerimentId' => 'ID do Requerimento',
                'approved' => 'Status de Aprovação'
            ];
            Validator::requireValidator($fields, $data);

            $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['requerimentId']);

            $requeriment->setApproved($data['approved']);

            if (isset($data['approved']) && $data['approved'] != '') {
                $requeriment->setRequestStatus(PaymentRequirements::REQUEST_STATUS_REQUESTED);
                $this->saveLogApproved($requeriment, PaymentRequirements::APPROVED_MANAGER);
            }

            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Aprovação alterada com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public
    function setStatusCancel(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($id);
            $requeriment->setRequestStatus(PaymentRequirements::REQUEST_STATUS_CANCEL);
            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Status alterado para Cancelado!',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }


    public
    function extract(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONCILIACAO_EXTRATOS_VISUALIZACAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/extract.phtml', 'subSection' => 'extract',
            'user' => $user, 'subSectionActive' => 'conciliation', 'subMenu' => 'financial', 'directories' => $directories]);
    }

    public
    function listExtract(Request $request, Response $response)
    {
        try {
            $this->getLogged(true);
            $filter = $request->getQueryParams();
            $account = $this->em->getRepository(DirectoryAccounts::class)->find((int)$filter['account'] ?? 0);
            if (!$account) throw new Exception('Conta inválida');
            //data inicial do filtre tem que ser no minimo um dia a mais que a data inicial do saldo
            $minDate = clone $account->getDateStart();
            $minDate = clone $minDate->add(new \DateInterval('P1D'));

            $startFilter = \DateTime::createFromFormat('d/m/Y', $filter['start']);
            $endFilter = \DateTime::createFromFormat('d/m/Y', $filter['end']);
            if (trim($filter['start'] != '') && $startFilter < $minDate) throw new Exception("Data mínima para esta conta {$minDate->format('d/m/Y')}");
            if (trim($filter['end'] != '') && $endFilter < $minDate) throw new Exception("Data mínima para esta conta {$minDate->format('d/m/Y')}");

            $initalBalance = $this->em->getRepository(OfxLines::class)->getInitialBalance($account, $account->getDateStart(), $startFilter);
            $startValue = $account->getValueStart() + $initalBalance[1] - $initalBalance[0];

            $balanceTotalByFilter = $this->em->getRepository(OfxLines::class)->getTotalBalanceByPeriod($account, $startFilter, $endFilter, $filter);
            $balanceByFilter = $this->em->getRepository(OfxLines::class)->getBalanceByPeriod($account, $startFilter, $endFilter, $filter);
            return $response->withJson([
                'status' => 'ok',
                'startBalance' => (float)$startValue,
                'totalEntries' => (float)$balanceTotalByFilter[1],
                'totalExits' => (float)$balanceTotalByFilter[0],
                'endBalance' => (float)$startValue + $balanceTotalByFilter[1] - $balanceTotalByFilter[0],
                'startDateBalance' => $startFilter->sub(new \DateInterval('P1D'))->format('d/m/Y'),
                'message' => $balanceByFilter,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function ofxPendencyConciliation(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParsedBody();
            $line = $this->em->getRepository(OfxLines::class)->find($data['pendency']);

            if ($data['type'] == 1) {
                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['requeriment']);
                $lineRequeriment = $this->em->getRepository(OfxLines::class)->findOneBy(['requirements' => $requeriment]);
                if ($lineRequeriment) throw new Exception('Esse pagamento já está vinculado, atualize a página');
                $requeriment->setOfxFiles($line->getOfxFiles())
                    ->setRequestStatus(PaymentRequirements::REQUEST_STATUS_CONCILIATION)
                    ->setStatementNumber($line->getExtractNumber());
                $this->em->getRepository(PaymentRequirements::class)->save($requeriment);
                $i = $requeriment->getApproved() + 1;
                // gerar carimbos de Conciliação
                for ($i; $i <= PaymentRequirements::APPROVED_CONCILIATION; $i++) {
                    $this->saveLogApproved($requeriment, $i);
                }
                $line->setRequirements($requeriment)
                    ->setMultiple(false);
            } elseif ($data['type'] == 2) {
                $refund = $this->em->getRepository(OfxLines::class)->find($data['requeriment']);
                $lineRefund = $this->em->getRepository(OfxLines::class)->findOneBy(['refund' => $refund]);
                if ($lineRefund) throw new Exception('Essa entrada já está vinculado, atualize a página');
                $line->setRefund($refund);
            }

            $this->em->getRepository(OfxLines::class)->save($line);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Conciliação realizada com sucesso!',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function ofxPendencyConciliationMultiple(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->getLogged(true);
            $data = (array)$request->getParsedBody();

            $line = $this->em->getRepository(OfxLines::class)->find($data['ofxLineId']);
            if ($line->getRequirements()) throw new Exception('Essa linha já foi conciliada');
            $this->em->getRepository(OfxLinePaymentRequirement::class)->dropByOfxLine($line);
            $totalRequeriments = 0;

            $transactions = str_replace(' ', '', $data['transactions']);
            $transactions = explode(',', $transactions);
            foreach ($transactions as $transaction) {
                $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($transaction);
                $lineRequeriment = $this->em->getRepository(OfxLines::class)->findOneBy(['requirements' => $requeriment]);
                $totalRequeriments += $requeriment->getPaymentValue();
                if ($lineRequeriment) throw new Exception("Pagamento {$transaction} já está vinculado a outra linha do OFX");
                $requeriment->setOfxFiles($line->getOfxFiles())
                    ->setRequestStatus(PaymentRequirements::REQUEST_STATUS_CONCILIATION)
                    ->setStatementNumber($line->getExtractNumber());
                $this->em->getRepository(PaymentRequirements::class)->save($requeriment);
                $i = $requeriment->getApproved() + 1;
                // gerar carimbos de Conciliação
                for ($i; $i <= PaymentRequirements::APPROVED_CONCILIATION; $i++) {
                    $this->saveLogApproved($requeriment, $i);
                }
                $ofxLinePaymentRequirement = new OfxLinePaymentRequirement();
                $ofxLinePaymentRequirement->setOfxLine($line)
                    ->setRequirements($requeriment);
                $this->em->getRepository(OfxLinePaymentRequirement::class)->save($ofxLinePaymentRequirement);
            }
            $totalRequeriments = Utils::formatMoney($totalRequeriments);
            $lineValue = Utils::formatMoney($line->getValue());
            if ($totalRequeriments != $lineValue) throw new Exception("O valor dos lançamentos é {$totalRequeriments} e a linha de extrato {$lineValue}");
            $line->setRequirements($requeriment)
                ->setMultiple(true);
            $this->em->getRepository(OfxLines::class)->save($line);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Conciliação realizada com sucesso!',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function ofxPendencyRemove(Request $request, Response $response)
    {
        try {

            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $data = (array)$request->getParams();

            $requeriment = $this->em->getRepository(PaymentRequirements::class)->find($data['requeriment']);
            $requeriment->removePendencies($this->em->getReference(OfxLines::class, $data['pendency']));
            $this->em->getRepository(PaymentRequirements::class)->save($requeriment);

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Conciliação excluída com sucesso!',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function getDebits(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $payments = $this->em->getRepository(PaymentRequirements::class)->getPayments();
            return $response->withJson([
                'status' => 'ok',
                'message' => $payments,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function budgets(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_ORCAMENTOS_ORCAMENTOANUAL_VISUALIZACAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        $costCenters = $this->em->getRepository(CostCenter::class)->findBy(['status' => 1], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/budgets.phtml', 'subSection' => 'budgets',
            'user' => $user, 'subMenu' => 'records', 'directories' => $directories, 'costCenters' => $costCenters,
            'subSectionActive' => 'budgets']);
    }

    public
    function budgetsDetails(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::ADMINISTRATIVO_ORCADOXREALIZADO_VISUALIZACAO, true);
        $directories = $this->em->getRepository(UserAdmin::class)->getDirectoryAcess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/budgetsDetails.phtml', 'section' => 'budgetsDetails',
            'user' => $user, 'subMenu' => 'administrative', 'directories' => $directories]);
    }

    public
    function saveBudgets(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $this->em->beginTransaction();
            $msg = 'Orçamento cadastrado com sucesso!';
            $data = (array)$request->getParams();

            $budget = new Budget();

            if ($data['budgetId'] > 0) {
                $budget = $this->em->getRepository(Budget::class)->find($data['budgetId']);
                $msg = 'Orçamento atualizado com sucesso!';
                $this->em->getRepository(BudgetValues::class)->deleteAll($budget);
            }

            $budget->setStatus(true)
                ->setTotal(Utils::moneyToFloat($data['total']))
                ->setCostCenter($this->em->getReference(CostCenter::class, $data['costCenter']))
                ->setArea($this->em->getReference(Area::class, $data['area']))
//                ->setSubArea($this->em->getReference(SubArea::class, $data['subArea']))
                ->setYear($data['year']);
            $this->em->getRepository(Budget::class)->save($budget);

            foreach ($data['value'] as $key => $item) {
                $value = new BudgetValues();
                $value->setBudget($budget)
                    ->setMonth($key)
                    ->setValue(Utils::moneyToFloat($item));
                $this->em->getRepository(BudgetValues::class)->save($value);
            }


            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function deleteBudget(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $this->em->beginTransaction();

            $id = $request->getAttribute('route')->getArgument('id');
            $budget = $this->em->getRepository(Budget::class)->find($id);
            $budget->changeStatus();

            $this->em->getRepository(Budget::class)->save($budget);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Orçamento excluído com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function launchPayments(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();

            $data = (array)$request->getParams();
            $fields = [
                'requirement' => 'Pagamento',
                'scheduleDate' => 'Data para agendamento'
            ];
            Validator::requireValidator($fields, $data);

            $payment = $this->em->getRepository(PaymentRequirements::class)->find($data['requirement']);
            if (!$payment) throw new \Exception("Pagamento inválido");

            if ($payment->getPaymentMethod() == 1) {
                Validator::requireValidator(['digitableLine' => 'Linha digitável do boleto:'], $data);
            }

            $account = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(
                ['directory' => $payment->getDirectory()->getId(), 'nickname' => $payment->getOrigin()]);
            if (!$account) throw new \Exception("Conta inválida");

            $credentials = $this->em->getRepository(CredentialsBB::class)->findOneBy(['directory' => $payment->getDirectory()->getId(), 'type' => 2]);
            if (!$credentials) throw new \Exception("Não é possível enviar este pagamento para o banco, verifique as credenciais do diretório.");

            $paymentAccount = str_replace("-", "", trim($payment->getPaymentBankAccount()));
            $creditData = [
                'agency' => (int)explode('-', $payment->getPaymentBankAgency())[0],
                'account' => substr($paymentAccount, 0, -1),
                'accountDigit' => substr($paymentAccount, -1)
            ];

            $request = substr(time(), -6);

            if ($payment->getPaymentMethod() == 1) {
                if ($data['guide']) {
                    $launch = BancoDoBrasil::bankSlipGuides($data, $account, $payment, $credentials, $request);
                } else {
                    $launch = BancoDoBrasil::bankSlip($data, $account, $payment, $credentials, $request);
                }
            } else if ($payment->getPaymentMethod() == 2) {
                $launch = BancoDoBrasil::transfersBB($data, $payment, $account, $credentials, $creditData, $request);
            } else if ($payment->getPaymentMethod() == 3) {
                $launch = BancoDoBrasil::pix($data, $account, $payment, $credentials, $request);
            }

            if (!$launch['estadoRequisicao'] && !$launch['numeroRequisicao']) throw new Exception("Erro ao tentar enviar, valide os dados e tente novamente!");

            $message = "Pagamento enviado com sucesso para o banco.";
            $jsonString = json_encode($launch);
            $extract = new ExtractBB();
            $extract->setUser($user)
                ->setRequest($request)
                ->setPayment($payment)
                ->setCreated(new \Datetime())
                ->setCnpj(Utils::onlyNumbers($payment->getPaymentDoc() ?: $payment->getProvider()->getCpfCnpj()))
                ->setValue($payment->getPaymentValue())
                ->setResponse($jsonString);
            if ($payment->getPaymentMethod() == 2) {
                $extract->setBank($payment->getPaymentBank()->getCod())
                    ->setAgency($creditData['agency'])
                    ->setAccount($creditData['account'])
                    ->setAccountDigit($creditData['accountDigit']);
            }
            $this->em->getRepository(ExtractBB::class)->save($extract);

            $scheduleDate = new \Datetime($data['scheduleDate']);
            $payment->setPaymentDueDate($scheduleDate)
                ->setScheduledDate($scheduleDate);
            $this->em->getRepository(PaymentRequirements::class)->save($payment);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function liberationPayments(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $data = (array)$request->getParams();
            if (strtotime(date('Y-m-d H:i:s')) >= strtotime(date('Y-m-d 21:00:s'))) throw new Exception("Horário inválido");
            $payment = $this->em->getRepository(PaymentRequirements::class)->find($id);
            if (!$payment) throw new \Exception("Pagamento inválido");
            $consultPayment = $this->em->getRepository(ExtractBB::class)->findOneBy(['payment' => $payment->getId(), 'statusString' => 'PENDENTE']);
            if (!$consultPayment) throw new \Exception("Pagamento não disponível para liberação.");
            $credentials = $this->em->getRepository(CredentialsBB::class)->findOneBy(['directory' => $payment->getDirectory(), 'type' => 2]);
            if (!$credentials) throw new \Exception("Não é possível liberar este pagamento no banco, verifique as credenciais do direttório");
            $liberation = BancoDoBrasil::liberation($payment, $credentials, $consultPayment);

            $jsonString = json_encode($liberation);
            $consultPayment->setStatusString('CONSISTENTE')
                ->setLiberation($jsonString)
                ->setReleasedBy($user)
                ->setReleasedIn(new \Datetime());
            $this->em->getRepository(ExtractBB::class)->save($consultPayment);
            $message = "Pagamento liberado com sucesso.";

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function cancelPayments(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $data = (array)$request->getParams();
            if (strtotime(date('Y-m-d H:i:s')) >= strtotime(date('Y-m-d 21:00:s'))) throw new Exception("Horário inválido");
            $payment = $this->em->getRepository(PaymentRequirements::class)->find($id);
            if (!$payment) throw new \Exception("Pagamento inválido");
            $consultPayment = $this->em->getRepository(ExtractBB::class)->findOneBy(['payment' => $payment->getId(), 'statusString' => 'PENDENTE']);
            if (!$consultPayment) throw new \Exception("Pagamento não disponível para liberação.");
            $credentials = $this->em->getRepository(CredentialsBB::class)->findOneBy(['directory' => $payment->getDirectory(), 'type' => 2]);
            if (!$credentials) throw new \Exception("Não é possível liberar este pagamento no banco, verifique as credenciais do direttório");
            $account = $this->em->getRepository(DirectoryAccounts::class)->findOneBy(['directory' => $payment->getDirectory(), 'nickname' => $payment->getOrigin()]);
            if (!$account) throw new \Exception("Conta inválida");
            $cancellation = BancoDoBrasil::cancelPayment($payment, $credentials, $account, $consultPayment);

            $jsonString = json_encode($cancellation);
            $consultPayment->setStatusString('CANCELADO')
                ->setCancel($jsonString)
                ->setCanceledBy($user)
                ->setCanceledIn(new \DateTime());
            $this->em->getRepository(ExtractBB::class)->save($consultPayment);
            $message = "Pagamento cancelado com sucesso.";

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function checksDoublePayment(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $payment = $this->em->getRepository(PaymentRequirements::class)->find($id);
            $dayOff = date("Y-m-d", strtotime(date("Y-m-d") . "-2 day"));
            $extract = $this->em->getRepository(ExtractBB::class)->findBy(['cnpj' => $payment->getProvider()->getCpfCnpj(), 'value' => $payment->getPaymentValue()]);
            $valid = true;
            if ($extract) {
                $message = 'Atenção, há um ou mais pagamento(s) neste mesmo valor e para este mesmo fornecedor realizado(s) a menos de dois dias,
                    antes de seguir confira na lista abaixo para que não seja gerada duplicidade de pagamento:<br><br>';
                foreach ($extract as $e) {
                    $payment = $this->em->getRepository(PaymentRequirements::class)->find($e->getPayment()->getId());
                    if ($payment->getPayDay()->format('Y-m-d') <= $dayOff) {
                        $valid = false;
                        $message .= "Pagamento: 
                        <a href='https://gestao.novo.org.br/financeiro/contas-a-pagar/{$e->getPayment()->getId()}' target='_blank'>
                            {$e->getPayment()->getId()}.</a><br>";
                    }
                }
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
                'valid' => $valid
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function checkNfNumber(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $nf = $request->getAttribute('route')->getArgument('nf');
            $providerDoc = $request->getAttribute('route')->getArgument('providerDoc');
            $provider = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => $providerDoc]);
            $checkNF = $this->em->getRepository(PaymentRequirements::class)->findOneBy(['nfNumber' => $nf, 'provider' => $provider]);
            if ($checkNF) throw new \Exception("Este número de Nota Fiscal/Recibo já foi lançado para o fornecedor {$provider->getName()}.");
            return $response->withJson([
                'status' => 'ok',
                'message' => '',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public
    function editDirectoryExtract(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'begin' => 'Inicia em',
                'end' => 'Finaliza em',
                'validation' => 'Validação',
            ];
            Validator::requireValidator($fields, $data);
            $validation = $data['validation'] == "-1" ? false : true;
            $e = $this->em->getRepository(DirectoryExtract::class)->find($data['accountId']);
            $e->setDateStart(new \Datetime($data['begin']))
                ->setDateEnd(new \Datetime($data['end']))
                ->setValidation($validation);
            $this->em->getRepository(DirectoryExtract::class)->save($e);
            $a = $this->em->getRepository(DirectoryAccounts::class)->find($e->getAccount());
            $subject = "Extrato de conta inválido";
            $msg = "Olá, o extrato importado para a conta abaixo está inválido,
                confira o comentário para normalização:<br><br>
                Conta: {$a->getNicknameString()}<br>
                Comentário: {$data['message']}";
            if ($validation == 0) Email::send($e->getUser()->getEmail(), '', $subject, $msg);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Salvo com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}
