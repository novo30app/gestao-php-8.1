<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Commons\Entities\Mission;
use App\Models\Entities\Gender;
use App\Models\Entities\SystemFeatures;
use App\Requests\MissionRequest;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class MissionController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_VISUALIZACAO, true);
        $genders = $this->em->getRepository(Gender::class)->findBy(['id' => [1, 2]]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/mission.phtml', 'subMenu' => 'affiliateSuccess',
            'subSectionActive' => 'mobilize', 'user' => $user, 'title' => 'Missões', 'genders' => $genders, 'subSection' => 'missions']);
    }

    public function list(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_VISUALIZACAO, true);
            $filter = $request->getQueryParams();
            $index = $request->getQueryParam('index') ?: 0;
            $limit = $request->getQueryParam('limit') ?: 25;
            $result = $this->em->getRepository(Mission::class)->list($filter, $limit, $index);
            $total = $this->em->getRepository(Mission::class)->listTotal($filter);
            $partial = ($index * $limit) + sizeof($result);
            $partial = $partial <= $total ? $partial : $total;
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
                'total' => (int)$total,
                'partial' => (int)$partial,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function save(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();

            $requestValidate = new MissionRequest($data);
            $requestValidate->validate();

            $result = $this->em->getRepository(Mission::class)->save($data);

            $this->em->commit();

            return $this->responseJson($result, $response, 201);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');

            $result = $this->em->getRepository(Mission::class)->changeActive($id);

            return $this->responseJson($result, $response);

        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function destroy(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');

            $result = $this->em->getRepository(Mission::class)->destroy($id);

            return $this->responseJson($result, $response);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function show(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $data = $this->em->getRepository(Mission::class)->data($id);
            return $response->withJson([
                'status' => 'ok',
                'message' => $data[0],
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function updateWeekStatus(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $data = (array)$request->getParams();

            $result = $this->em->getRepository(Mission::class)->updateWeekStatus($data);

            return $this->responseJson($result, $response);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }


}
