<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\Ombudsman;
use App\Models\Entities\OmbudsmanAnswer;
use App\Models\Entities\OmbudsmanCategory;
use App\Models\Entities\OmbudsmanResponse;
use App\Models\Entities\OmbudsmanSubject;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\UserAdmin;
use App\Services\Email;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class OmbudsmanController extends Controller
{
    public function category(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_CATEGORIAS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/category.phtml',
            'user' => $user, 'subMenu' => 'records', 'section' => 'category']);
    }

    public function frequentAnswers(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::OUVIDORIA_RESPOSTASFREQUENTES_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/frequent-answers.phtml',
            'user' => $user, 'subMenu' => 'ombudsman', 'section' => 'frequent-answers']);
    }

    public function saveCategory(Request $request, Response $response)
    {
        try {
            $em = $this->em;
            $em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'category' => 'Categoria',
                'responsible' => 'Responsável'
            ];
            Validator::requireValidator($fields, $data);
            $category = new OmbudsmanCategory();
            if ($data['categoryId'] > 0) {
                $category = $em->getRepository(OmbudsmanCategory::class)->find($data['categoryId']);
            }
            $category->setName($data['category'])
                ->setResponsible($em->getReference(UserAdmin::class, $data['responsible']))
                ->setActive(true);
            $em->getRepository(OmbudsmanCategory::class)->save($category);
            $em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Categoria cadastrada com sucesso',
            ], 201)
                ->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            $em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveAnswer(Request $request, Response $response)
    {
        try {
            $em = $this->em;
            $em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = ['answer' => 'Resposta'];
            Validator::requireValidator($fields, $data);
            $answer = new OmbudsmanAnswer();
            if ($data['answerId'] > 0) {
                $answer = $em->getRepository(OmbudsmanAnswer::class)->find($data['answerId']);
            }
            $answer->setAnswer($data['answer'])
                ->setActive(true);
            $em->getRepository(OmbudsmanAnswer::class)->save($answer);
            $em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Resposta cadastrada com sucesso'
            ], 201)
                ->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            $em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getCategories(Request $request, Response $response)
    {
        $categoryRepository = $this->em->getRepository(OmbudsmanCategory::class)->findBy(['active' => true], ['name' => 'asc']);
        $categoryArray = [];
        foreach ($categoryRepository as $category) {
            if ($category instanceof Category) ;
            $categoryArray[] = ['id' => $category->getId(), 'name' => $category->getName(),
                'responsible' => $category->getResponsible()->getName(), 'responsibleId' => $category->getResponsible()->getId()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $categoryArray,
        ], 200)->withHeader('Content-Type', 'application/json');
    }

    public function getAnswers(Request $request, Response $response)
    {
        $answersRepository = $this->em->getRepository(OmbudsmanAnswer::class)->findBy(['active' => true], ['answer' => 'asc']);
        $answerArray = [];
        foreach ($answersRepository as $answer) {
            $answerArray[] = ['id' => $answer->getId(), 'answer' => $answer->getAnswer()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $answerArray,
        ], 200)->withHeader('Content-Type', 'application/json');
    }

    public function changeCategoryStatus(Request $request, Response $response, bool $status)
    {
        try {
            $em = $this->em;
            $em->beginTransaction();
            $categoryId = $request->getAttribute('route')->getArgument('id');
            $category = $this->em->getRepository(OmbudsmanCategory::class)->find($categoryId);
            $category->setActive($status);
            $em->getRepository(OmbudsmanCategory::class)->save($category);
            $em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Categoria atualizada com sucesso'
            ], 201)->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            $em->rollback();
            return $request->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeAnswerStatus(Request $request, Response $response, bool $status)
    {
        try {
            $em = $this->em;
            $em->beginTransaction();
            $answerId = $request->getAttribute('route')->getArgument('id');
            $answer = $em->getRepository(OmbudsmanAnswer::class)->find($answerId);
            $answer->setActive($status);
            $em->getRepository(OmbudsmanAnswer::class)->save($answer);
            $em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Resposta atualizada com sucesso',
            ])->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            $em->rollback();
            return $response->withJson([
                'status' => 'ok',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function redirectOmbudsmanCategory(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $data = (array)$request->getParams();
            $ombudsman= $this->em->getRepository(Ombudsman::class)->find($id);
            $ombudsman->setDestiny($this->em->getReference(OmbudsmanCategory::class, $data['destiny']));
            $this->em->getRepository(Ombudsman::class)->save($ombudsman);
            $categoty = $this->em->getRepository(OmbudsmanCategory::class)->find($data['destiny']);
            $msg = "Prezado(a), {$categoty->getResponsible()->getName()}<br><br>
            Uma nova solicitação foi cadastrada, acesse o 
            <a href='https://gestao.novo.org.br/ouvidoria/solicitacoes/{$ombudsman->getId()}' target='_blank'>sistema</a> para responder.<br><br>
            Categoria: {$categoty->getName()} <br>
            Assunto: {$ombudsman->getOmbudsmanSubject()->getName()}";
            Email::send($categoty->getResponsible()->getEmail(), $categoty->getResponsible()->getName(),
                'Nova Solicitação - Ouvidoria', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Solicitação redirecionada com sucesso',
            ])->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'ok',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getUserAdmin(Request $request, Response $response)
    {
        $userAdminRepository = $this->em->getRepository(UserAdmin::class)->findBy(['active' => 1], ['name' => 'asc']);
        $array = [];
        foreach ($userAdminRepository as $user) {
            if ($user instanceof UserAdmin) ;
            $array[] = ['id' => $user->getId(), 'name' => $user->getName()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $array,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function requests(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::OUVIDORIA_SOLICITACOES_VISUALIZACAO, true);
        $categories = $this->em->getRepository(OmbudsmanCategory::class)->findBy(['active' => 1], ['name' => 'asc']);
        $subjects = $this->em->getRepository(OmbudsmanSubject::class)->findBy(['active' => 1], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/requests.phtml',
            'user' => $user, 'subMenu' => 'ombudsman', 'section' => 'requests',
            'categories' => $categories, 'subjects' => $subjects]);
    }

    public function requestsTable(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');$userId = $user->getId();
        $index = $request->getQueryParam('index');
        $status = $request->getQueryParam('status');
        $destiny = $request->getQueryParam('destiny');
        $ombudsmanSubject = $request->getQueryParam('subject');
        $requests = $this->em->getRepository(Ombudsman::class)->list($id, $userId, $status, $destiny, $ombudsmanSubject, 20, $index * 20);
        $total = $this->em->getRepository(Ombudsman::class)->listTotal($id, $userId, $status, $destiny, $ombudsmanSubject)['total'];
        $partial = ($index * 20) + sizeof($requests);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $requests,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function viewRequests(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::OUVIDORIA_SOLICITACOES_VISUALIZACAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $ombudsman = $this->em->getRepository(Ombudsman::class)->find($id);
        if ($ombudsman->getDestiny()->getResponsible() != $user) $this->redirectByPermissions();
        $categories = $this->em->getRepository(OmbudsmanCategory::class)->findBy(['active' => 1], ['name' => 'asc']);
        $ombudsmanResponses = $this->em->getRepository(OmbudsmanResponse::class)->findBy(['ombudsman' => $ombudsman], ['id' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/viewRequests.phtml', 'categories' => $categories,
            'user' => $user, 'subMenu' => 'ombudsman', 'section' => 'requests', 'ombudsman' => $ombudsman, 'ombudsmanResponses' => $ombudsmanResponses]);
    }

    public function saveResponse(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'answer' => 'Posicionamento frente a Solicitação'
            ];
            Validator::requireValidator($fields, $data);
            $ombudsman = $this->em->getRepository(Ombudsman::class)->find($data['idOmbudsman']);
            $ombudsman->setStatus(1);
            $this->em->getRepository(Ombudsman::class)->save($ombudsman);
            $answer = new OmbudsmanResponse();
            $answer->setOmbudsman($this->em->getReference(Ombudsman::class, $data['idOmbudsman']))
                ->setUserAdmin($user)
                ->setAnswer($data['answer']);
            $this->em->getRepository(OmbudsmanResponse::class)->save($answer);
            $msg = "Prezado(a), {$ombudsman->getRequester()->getName()}<br><br>
                    Sua solicitação foi respondida, acesse o <a href='https://espaco-novo.novo.org.br/' target='_blank'>sistema</a> para visualizar.<br><br>
                    ";
            Email::send($ombudsman->getRequester()->getEmail(), $ombudsman->getRequester()->getName(),
                'Resposta Solicitação - Ouvidoria', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Resposta regristrada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }
}