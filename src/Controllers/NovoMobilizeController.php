<?php

namespace App\Controllers;
use App\Helpers\Session;
use App\Requests\NovoMobilizeRequest;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class NovoMobilizeController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/index.phtml', 'subMenu' => 'mobilize',
            'section' => 'mobilize-dashboard', 'user' => $user, 'title' => 'Título']);
    }

//    public function list(Request $request, Response $response)
//    {
//        try {
//            $user = $this->getLogged();
//            $filter = $request->getQueryParams();
//            $index = $request->getQueryParam('index') ?: 0;
//            $limit = $request->getQueryParam('limit') ?: 25;
//            $result = $this->em->getRepository(NovoMobilize::class)->list($filter, $limit, $index);
//            $total = $this->em->getRepository(NovoMobilize::class)->listTotal($filter);
//            $partial = ($index * $limit) + sizeof($result);
//            $start = sizeof($result) > 0 ? ($index * $limit) + 1 : 0;
//            $partial = $partial <= $total ? $partial : $total;
//            return $response->withJson([
//                'status' => 'ok',
//                'message' => $result,
//                'count' => (int)$total,
//                'partial' => (int)$partial,
//                'page' => (int)$index,
//                'start' => (int)$start,
//                'limit' => (int)$limit,
//            ], 200)
//                ->withHeader('Content-type', 'application/json');
//        } catch (\Exception $e) {
//            return $response->withJson([
//                'status' => 'error',
//                'message' => $e->getMessage(),
//            ])->withStatus(500);
//        }
//    }
//
//    public function save(Request $request, Response $response)
//    {
//        try {
//           $this->em->beginTransaction();
//           $user = $this->getLogged();
//           $data = (array)$request->getParams();
//
//            $requestValidate = new NovoMobilizeRequest($data);
//            $requestValidate->validate();
//
//            $result = $this->em->getRepository(NovoMobilize::class)->save($data);
//
//            $this->em->commit();
//
//            return $this->responseJson($result, $response);
//        } catch (\Exception $e) {
//            return $response->withJson(['status' => 'error',
//                'message' => $e->getMessage(),])->withStatus(500);
//        }
//    }
//
//    public function changeActive(Request $request, Response $response)
//    {
//        try {
//            $user = $this->getLogged();
//            $id = $request->getAttribute('route')->getArgument('id');
//
//            $result = $this->em->getRepository(NovoMobilize::class)->changeActive($id);
//
//            return $this->responseJson($result, $response);
//
//        } catch (\Exception $e) {
//            return $response->withJson(['status' => 'error',
//                'message' => $e->getMessage(),])->withStatus(500);
//        }
//    }
//
//    public function destroy(Request $request, Response $response)
//    {
//        try {
//            $user = $this->getLogged();
//            $id = $request->getAttribute('route')->getArgument('id');
//
//            $result = $this->em->getRepository(NovoMobilize::class)->destroy($id);
//
//            return $this->responseJson($result, $response);
//        } catch (\Exception $e) {
//            return $response->withJson(['status' => 'error',
//                'message' => $e->getMessage(),])->withStatus(500);
//        }
//    }
//
//    public function show(Request $request, Response $response)
//    {
//        try {
//            $user = $this->getLogged();
//            $id = $request->getAttribute('route')->getArgument('id');
//            $data = $this->em->getRepository(NovoMobilize::class)->find($id);
//            return $response->withJson([
//                'status' => 'ok',
//                'message' => $data->toArray(),
//            ], 200)
//                ->withHeader('Content-type', 'application/json');
//        } catch (\Exception $e) {
//            return $response->withJson(['status' => 'error',
//                'message' => $e->getMessage(),])->withStatus(500);
//        }
//    }

}
