<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\Campaigns;
use App\Models\Entities\CampaignsUsers;
use App\Models\Entities\SystemFeatures;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CampaignsController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_CAMPANHASDONOVO_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'campaigns/index.phtml', 'section' => 'campaigns',
            'user' => $user, 'subMenu' => 'affiliateSuccess', 'title' => 'Campanhas do NOVO']);
    }

    public function affiliations(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_CAMPANHASDONOVO_VISUALIZACAO, true);
        $campaigns = match ($user->getLevel()) {
            1 => $this->em->getRepository(Campaigns::class)->findBy(['state' => $user->getState()->getId(), 'city' => $user->getCity()->getId()]),
            2 => $this->em->getRepository(Campaigns::class)->findBy(['state' => $user->getState()->getId()]),
            default => $this->em->getRepository(Campaigns::class)->findAll()
        };
        return $this->renderer->render($response, 'default.phtml', ['page' => 'campaigns/affiliations.phtml', 'section' => 'campaigns',
            'campaigns' => $campaigns, 'user' => $user, 'subMenu' => 'affiliateSuccess', 'title' => 'Filiações de Campanhas']);
    }

    public function list(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $description = $request->getQueryParam('description');
        $status = $request->getQueryParam('status');
        $campaigns = $this->em->getRepository(Campaigns::class)->list($user, $name, $description, $status, 20, $index * 20);
        $total = $this->em->getRepository(Campaigns::class)->listTotal($user, $name, $description, $status)['total'];
        $partial = ($index * $limit) + sizeof($campaigns);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $campaigns,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function changeStatus(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $campaign = $this->em->getRepository(Campaigns::class)->find($id);
            $campaign->setStatus(2);
            $this->em->getRepository(Campaigns::class)->save($campaign);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Campanha removida com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'name' => 'Nome',
                'description' => 'Descrição'
            ], $data);
            $campaign = new Campaigns();
            if ($data['campaign']) $campaign = $this->em->getRepository(Campaigns::class)->findOneBy(['id' => $data['campaign']]);
            $campaign->setName($data['name'])
                ->setUser($user)
                ->setDescription($data['description'])
                ->setStatus(1);
            if ($user->getLevel() == 1) $campaign->setCity($this->em->getReference(City::class, $user->getCity()->getId()));
            if ($user->getLevel() != 3) $campaign->setState($this->em->getReference(State::class, $user->getState()->getId()));
            $this->em->getRepository(Campaigns::class)->save($campaign);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Campanha salva com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function affiliationsList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $campaign = $request->getQueryParam('campaign');
        $status = $request->getQueryParam('status');
        $exemption = $request->getQueryParam('exemption');
        $affiliations = $this->em->getRepository(CampaignsUsers::class)->list($user, $name, $campaign, $status, $exemption, 20, $index * 20);
        $total = $this->em->getRepository(CampaignsUsers::class)->listTotal($user, $name, $campaign, $status, $exemption)['total'];
        $partial = ($index * $limit) + sizeof($affiliations);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $affiliations,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function membershipCampaigns(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_CAMPANHASDEFILIACAO_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'campaigns/membershipCampaigns.phtml',
            'section' => 'membershipCampaigns', 'user' => $user, 'subMenu' => 'affiliateSuccess', 'title' => 'Campanhas de Filiação']);
    }

    public function membershipCampaignsList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $ordernation = $request->getQueryParam('ordernation');
        $beginDate = $request->getQueryParam('beginDate');
        $finalDate = $request->getQueryParam('finalDate');
        $membershipCampaigns = $this->em->getRepository(Campaigns::class)->membershipCampaignsList($user, $name, $state, $city, $ordernation, $beginDate, $finalDate, 20, $index * 20);
        $total = $this->em->getRepository(Campaigns::class)->membershipCampaignsListTotal($user, $name, $state, $city, $beginDate, $finalDate)['total'];
        $partial = ($index * 20) + sizeof($membershipCampaigns);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $membershipCampaigns,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function membershipCampaignsListExport(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_CAMPANHASDEFILIACAO_EXPORTACAO, true);
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $ordernation = $request->getQueryParam('ordernation');
        $beginDate = $request->getQueryParam('beginDate');
        $finalDate = $request->getQueryParam('finalDate');
        $membershipCampaigns = $this->em->getRepository(Campaigns::class)->membershipCampaignsList($user, $name, $state, $city, $ordernation, $beginDate, $finalDate);
        $filename = "Campanhas De Filiação " . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Nome Indicador', 'Indicações', 'Estado', 'Cidade', 'Link', 'Indicado ID', 'Arquivo', 'WhatsApp'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($membershipCampaigns as $m) {
            fputcsv($out, array_values($m), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function membershipCampaignsResults(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_CAMPANHASDEFILIACAO_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'campaigns/membershipCampaignsResults.phtml',
            'section' => 'membershipCampaigns', 'user' => $user, 'subMenu' => 'affiliateSuccess', 'title' => 'Campanhas de Filiação Resultados']);
    }

    public function membershipCampaignsResultsList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $indicator = $request->getQueryParam('indicator');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $beginDate = $request->getQueryParam('beginDate');
        $finalDate = $request->getQueryParam('finalDate');
        $membershipCampaigns = $this->em->getRepository(Campaigns::class)->membershipCampaignsResultsList($user, $name, $indicator, $state, $city, $status, $beginDate, $finalDate, 20, $index * 20);
        $total = $this->em->getRepository(Campaigns::class)->membershipCampaignsResultsListTotal($user, $name, $indicator, $state, $city, $status, $beginDate, $finalDate)['total'];
        $partial = ($index * 20) + sizeof($membershipCampaigns);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $membershipCampaigns,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function membershipCampaignsResultsExport(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::SUCESSODOFILIADO_CAMPANHASDEFILIACAO_EXPORTACAO, true);
        $name = $request->getQueryParam('name');
        $indicator = $request->getQueryParam('indicator');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $beginDate = $request->getQueryParam('beginDate');
        $finalDate = $request->getQueryParam('finalDate');
        $membershipCampaigns = $this->em->getRepository(Campaigns::class)->membershipCampaignsResultsList($user, $name, $indicator, $state, $city, $status, $beginDate, $finalDate);
        $filename = "Campanhas De Filiação Resultados " . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Nome Indicado', 'Nome Indicador', 'Status', 'Estado', 'Cidade', 'Indicador ID', 'Indicado ID', 'Data Solicitação', 'Data Filiação'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($membershipCampaigns as $m) {
            fputcsv($out, array_values($m), ';', '"');
        }
        fclose($out);
        exit;
    }
}