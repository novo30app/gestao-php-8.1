<?php

namespace App\Controllers;

use App\Exceptions\ForbiddenException;
use App\Helpers\Session;
use App\Models\Entities\User;
use App\Models\Entities\Transaction;
use App\Models\Entities\PaymentRequirements;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class TransparencyPortalController extends Controller
{

    public function getList(Request $request, Response $response)
    {
        try {
            $header = $request->getHeader('X-Auth')[0];
            $this->getUserLoggedByToken($header ?? '', [7, 8]);
            $index = $request->getQueryParam('index');
            $filter = $request->getQueryParams();
            $payments = $this->em->getRepository(PaymentRequirements::class)->listTransparencyPortal($filter, 20, $index * 20);
            $total = $this->em->getRepository(PaymentRequirements::class)->listTransparencyPortalTotal($filter)['total'];
            $partial = ($index * 20) + sizeof($payments);
            $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
            return $response->withJson([
                'status' => 'ok',
                'message' => $payments,
                'total' => $total,
                'partial' => $partial,
            ], 200)->withHeader('Content-type', 'application/json');
        } catch (ForbiddenException $e) {
            return $response->withJson(['status' => 'error', 'message' => $e->getMessage()], 403)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error', 'message' => $e->getMessage()], 500)->withHeader('Content-type', 'application/json');
        }
    }

    public function contracts(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $verifyExemptionAffiliation = $this->em->getRepository(User::class)->verifyExemptionAffiliation($user->getId());
        $verifyCompliance = $this->em->getRepository(Transaction::class)->verifyCompliance($user->getId())['situation'];
        if (!$verifyExemptionAffiliation && $verifyCompliance != 'Adimplente') {
            Session::set('errorMsg', 'Você não poderá acessar este conteúdo até que regularize suas contribuições!');
            header("Location: {$this->baseUrl}{$url}");
            die();
        }
        $contract = $request->getAttribute('route')->getArgument('contract');
        $contractNumber = $this->em->getRepository(PaymentRequirements::class)->getContractNumber($contract);
        $contracts = $this->em->getRepository(PaymentRequirements::class)->getContracts($contract);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'transparencyPortal/contracts.phtml',
            'section' => 'transparencyPortal', 'user' => $user, 'title' => 'Contrato - ' . $contractNumber['number'], 'contracts' => $contracts]);
    }
}