<?php

namespace App\Controllers;

use App\Helpers\Session;
use App\Helpers\Validator;
use App\Models\Commons\Entities\Benefit;
use App\Models\Entities\Plan;
use App\Models\Entities\SystemFeatures;
use App\Requests\BenefitRequest;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class BenefitController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::requireValidator(SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_BENEFICIOS_VISUALIZACAO, true);
        $plans = $this->em->getRepository(Plan::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/benefit.phtml', 'subMenu' => 'affiliateSuccess',
            'subSectionActive' => 'mobilize', 'user' => $user, 'title' => 'Benefícios', 'plans' => $plans, 'subSection' => 'mobilize-benefits']);
    }

    public function requestBenefits(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/requestBenefits.phtml', 'subMenu' => 'mobilize',
            'section' => 'mobilize-benefits', 'user' => $user, 'title' => 'Benefícios']);
    }

    public function list(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $filter = $request->getQueryParams();
            $index = $request->getQueryParam('index') ?: 0;
            $limit = $request->getQueryParam('limit') ?: 25;
            $result = $this->em->getRepository(Benefit::class)->list($filter, $limit, $index);
            $total = $this->em->getRepository(Benefit::class)->listTotal($filter);
            $partial = ($index * $limit) + sizeof($result);
            $partial = $partial <= $total ? $partial : $total;
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
                'total' => (int)$total,
                'partial' => (int)$partial,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function save(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->getLogged();
            $data = (array)$request->getParams();
            $files = $request->getUploadedFiles();

            $requestValidate = new BenefitRequest($data);
            $requestValidate->validate();

            $result = $this->em->getRepository(Benefit::class)->save($data, $files);

            $this->em->commit();

            return $this->responseJson($result, $response, 201);
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');

            $result = $this->em->getRepository(Benefit::class)->changeActive($id);

            return $this->responseJson($result, $response);

        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function destroy(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');

            $result = $this->em->getRepository(Benefit::class)->destroy($id);

            return $this->responseJson($result, $response);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function show(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $data = $this->em->getRepository(Benefit::class)->data($id);

            $benefitPlan = [];
            foreach ($data['benefitPlan'] as $datum) {
                $benefitPlan[$datum['plan']['id']] = [
                    'points' => $datum['points'],
                    'minDays' => $datum['minDays'],
                    'availables' => $datum['available'],
                ];
            }

            $data['benefitPlan'] = $benefitPlan;

            return $response->withJson([
                'status' => 'ok',
                'message' => $data,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function seeder(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->em->getRepository(Benefit::class)->seeder();
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Seeders criados com sucesso!',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }
}
