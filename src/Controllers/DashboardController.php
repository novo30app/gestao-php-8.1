<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\AffiliateEvolution;
use App\Models\Entities\Ombudsman;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\User;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Directory;
use App\Models\Entities\CitiesDashboards;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class DashBoardController extends Controller
{
    public function evolution(Request $request, Response $response)
    {
        $this->getLogged(true);
        $uf = (int)$request->getQueryParam('uf') ?? 0;
        $city = (int)$request->getQueryParam('city') ?? 0;
        $end = $request->getQueryParam('end');
        $start = $request->getQueryParam('start');
        $end = new \DateTime($end);
        $start = new \DateTime($start);
        $values = [];
        $values[] = ['Month', 'Filiações', 'Desfiliações', 'Total'];
        while ($start < $end) {
            $evolution = $this->em->getRepository(AffiliateEvolution::class)->evolutionByPeriodAndLocation($start->format('m/Y'), $uf, $city);
            $values[] = [$evolution['period'], (int)$evolution['affiliations'], (int)$evolution['disaffiliation'], (int)$evolution['total']];
            $start->add(new \DateInterval('P1M'));
        }
        return $response->withJson([
            'status' => 'ok',
            'values' => $values,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function graphicAffiliatedsByStatus(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $uf = $request->getQueryParam('uf');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $situation = $request->getQueryParam('situation');
        $talents = $this->em->getRepository(User::class)->graphicAffiliatedsByStatus($user, $uf, $city, $status, $situation);
        $keys = $values = $rgb = [];
        $total = 0;
        foreach ($talents as $talent) {
            $keys[] = $talent['status'];
            $values[] = $talent['total'];
            $rgb[] = Utils::randColor();
            $total += $talent['total'];
        }
        return $response->withJson([
            'status' => 'ok',
            'keys' => $keys,
            'values' => $values,
            'rgb' => $rgb,
            'total' => $total,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function graphicAffiliatedsByState(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $uf = $request->getQueryParam('uf');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $situation = $request->getQueryParam('situation');
        $talents = $this->em->getRepository(User::class)->graphicAffiliatedsByState($user, $uf, $city, $status, $situation);
        $keys = $values = $rgb = [];
        $total = 0;
        foreach ($talents as $talent) {
            $keys[] = $talent['state'];
            $values[] = $talent['total'];
            $rgb[] = Utils::randColor();
            $total += $talent['total'];
        }
        return $response->withJson([
            'status' => 'ok',
            'keys' => $keys,
            'values' => $values,
            'rgb' => $rgb,
            'total' => $total,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function graphicAffiliatedsByCity(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $uf = $request->getQueryParam('uf');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $situation = $request->getQueryParam('situation');
        $talents = $this->em->getRepository(User::class)->graphicAffiliatedsByCity($user, $uf, $city, $status, $situation);
        $keys = $values = $rgb = [];
        $total = 0;
        foreach ($talents as $talent) {
            $keys[] = $talent['city'];
            $values[] = $talent['total'];
            $rgb[] = Utils::randColor();
            $total += $talent['total'];
        }
        return $response->withJson([
            'status' => 'ok',
            'keys' => $keys,
            'values' => $values,
            'rgb' => $rgb,
            'total' => $total,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function management(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::GESTAO_PAINELDEGESTAO_VISUALIZACAO, true);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - dashboard']);
        $dashboard = $this->em->getRepository(Directory::class)->findOneBy(['id' => 1])->getDashboard();
        if ($user->getLevel() != 3) {
            $dashboard = $this->em->getRepository(Directory::class)->findOneBy(['tbEstadoId' => $user->getState()->getId(), 'estadual' => 'S'])->getDashboard();
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dashboard/management.phtml', 'section' => 'management', 'term' => $term,
            'user' => $user, 'subMenu' => 'management', 'title' => 'Dashboard - Painel de Gestão', 'dashboard' => $dashboard]);
    }

    public function graphicOmbudsmanByStatus(Request $request, Response $response)
    {
        $this->getLogged(true);
        $dateStart = $request->getQueryParam('start');
        $dateEnd = $request->getQueryParam('end');
        $subject = $request->getQueryParam('subject');
        $category = $request->getQueryParam('category');
        $status = $request->getQueryParam('status');
        $ombudsman = $this->em->getRepository(Ombudsman::class)->graphicOmbudsmanByStatus($dateStart, $dateEnd, $subject, $category, $status);
        $keys = $values = $rgb = [];
        $total = 0;
        foreach ($ombudsman as $talent) {
            $keys[] = $talent['status'];
            $values[] = $talent['total'];
            $rgb[] = Utils::randColor();
            $total += $talent['total'];
        }
        return $response->withJson([
            'status' => 'ok',
            'keys' => $keys,
            'values' => $values,
            'rgb' => $rgb,
            'total' => $total,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function graphicOmbudsmanByDestiny(Request $request, Response $response)
    {
        $this->getLogged(true);
        $dateStart = $request->getQueryParam('start');
        $dateEnd = $request->getQueryParam('end');
        $subject = $request->getQueryParam('subject');
        $category = $request->getQueryParam('category');
        $status = $request->getQueryParam('status');
        $ombudsman = $this->em->getRepository(Ombudsman::class)->graphicOmbudsmanByDestiny($dateStart, $dateEnd, $subject, $category, $status);
        $keys = $values = $rgb = [];
        $total = 0;
        foreach ($ombudsman as $talent) {
            $keys[] = $talent['destiny'];
            $values[] = $talent['total'];
            $rgb[] = Utils::randColor();
            $total += $talent['total'];
        }
        return $response->withJson([
            'status' => 'ok',
            'keys' => $keys,
            'values' => $values,
            'rgb' => $rgb,
            'total' => $total,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function graphicOmbudsmanBySubject(Request $request, Response $response)
    {
        $this->getLogged(true);
        $dateStart = $request->getQueryParam('start');
        $dateEnd = $request->getQueryParam('end');
        $subject = $request->getQueryParam('subject');
        $category = $request->getQueryParam('category');
        $status = $request->getQueryParam('status');
        $ombudsman = $this->em->getRepository(Ombudsman::class)->graphicOmbudsmanBySubject($dateStart, $dateEnd, $subject, $category, $status);
        $keys = $values = $rgb = [];
        $total = 0;
        foreach ($ombudsman as $talent) {
            $keys[] = $talent['subject'];
            $values[] = $talent['total'];
            $rgb[] = Utils::randColor();
            $total += $talent['total'];
        }
        return $response->withJson([
            'status' => 'ok',
            'keys' => $keys,
            'values' => $values,
            'rgb' => $rgb,
            'total' => $total,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function electoralPlanning(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::GESTAO_PLANEJAMENTOELEITORAL_VISUALIZACAO, true);
        $this->newAccessLog($user, 'dashboard');
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dashboard/electoralPlanning.phtml', 'section' => 'electoralPlanning',
            'user' => $user, 'subMenu' => 'management', 'title' => 'Dashboard - Planejamento Eleitoral']);
    }
}