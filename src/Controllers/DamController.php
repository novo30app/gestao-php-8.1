<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\CandidatePost;
use App\Models\Entities\City;
use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionKeyWord;
use App\Models\Entities\InteractionMeetingParticipant;
use App\Models\Entities\InteractionMeetingSchedule;
use App\Models\Entities\InteractionMeetingSubject;
use App\Models\Entities\InteractionMeetingSuggestion;
use App\Models\Entities\InteractionType;
use App\Models\Entities\KeyWord;
use App\Models\Entities\Representatives;
use App\Models\Entities\State;
use App\Models\Entities\Candidate;
use App\Models\Entities\SystemFeatures;
use App\Services\Dam;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class DamController extends Controller
{
    public function cabinets(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_GABINETES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findBy([], ['estado' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/cabinets.phtml',
            'user' => $user, 'subMenu' => 'dam', 'section' => 'cabinets', 'states' => $states]);
    }

    public function tagsIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_TAGS_VISUALIZACAO, true);
        $cabinetsList = Dam::getCabinets();
        $cabinetsList = $cabinetsList['message'];
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/tags.phtml',
            'user' => $user, 'subMenu' => 'dam', 'section' => 'tags', 'cabinetsList' => $cabinetsList]);
    }

    public function projectsIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_PROJETOS_VISUALIZACAO, true);
        $projects = Dam::getProjects();
        $projects = $projects['message'];
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/projects.phtml',
            'user' => $user, 'subMenu' => 'dam', 'section' => 'projects', 'projects' => $projects]);
    }

    public function projectIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_PROJETOS_VISUALIZACAO, true);
        $projectId = $request->getAttribute('route')->getArgument('id');
        $result = Dam::getProjectById($projectId);
        $project = $result['project'];
        $tags = $result['tags'];
        $authors = $result['authors'];
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/project.phtml',
            'user' => $user, 'subMenu' => 'dam', 'section' => 'projects', 'project' => $project, 'tags' => $tags,
            'authors' => $authors]);
    }

    public function cabinetIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_GABINETES_VISUALIZACAO, true);
        $cabinetId = $request->getAttribute('route')->getArgument('id');
        $usersList = Dam::getUsersByCabinetId($cabinetId);
        $presencesList = Dam::getPresencesByCabinetId($cabinetId);
        $spendingsList = Dam::getSpendingsByCabinetId($cabinetId);
        $cabinet = Dam::getCabinetById($cabinetId);
        $projects = Dam::getProjectsById($cabinetId);
        $polls = Dam::getPollsByCabinetId($cabinetId);
        $reports = Dam::getReportsByCabinetId($cabinetId);
        $cabinetsList = Dam::getCabinets()['message'];
        $reports = $reports['message'];
        $polls = $polls['message'];
        $cabinet = $cabinet['message'];
        $usersList = $usersList['message'];
        $projects = $projects['message'];
        $presencesList = $presencesList['message'];
        $spendingsList = $spendingsList['message'];
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/cabinet.phtml',
            'user' => $user, 'subMenu' => 'dam', 'section' => 'cabinets', 'usersList' => $usersList,
            'spendingsList' => $spendingsList, 'presencesList' => $presencesList, 'cabinet' => $cabinetId, 'c' => $cabinet,
            'projects' => $projects, 'polls' => $polls, 'reports' => $reports, 'cabinetsList' => $cabinetsList]);
    }

    public function users(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $usersList = Dam::getUsers();
        $usersList = $usersList['message'];
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/users.phtml',
            'user' => $user, 'subMenu' => 'list', 'section' => 'users', 'usersList' => $usersList]);
    }

    public function registerUser(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'nome',
                'email' => 'email',
                'cabinet' => 'Gabinete',
                'status' => 'Status'
            ];
            Validator::requireValidator($fields, $data);
            $result = Dam::saveUser($data);
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
            ], 201)->withHeader('Content-Type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function registerTag(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'nome',
            ];
            Validator::requireValidator($fields, $data);
            $result = Dam::saveTag($data);
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
            ], 201)->withHeader('Content-Type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function registerCabinet(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'nome',
                'city' => 'cidade',
                'state' => 'estado',
            ];
            Validator::requireValidator($fields, $data);
            $result = Dam::saveCabinet($data);
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
            ], 201)->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getUsers(Request $request, Response $response)
    {
        $cabinet = $request->getAttribute('route')->getArgument('id');
        $result = Dam::getUsersByCabinetId($cabinet);

        return $response->withJson([
            'message' => $result,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getTags(Request $request, Response $response)
    {
        $result = Dam::getTags();
        return $response->withJson([
            'message' => $result,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function dossieIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_DOSSIE_VISUALIZACAO, true);
        $representatives = $this->em->getRepository(Representatives::class)->findBy([], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/dossieList.phtml',
            'user' => $user, 'subMenu' => 'dam', 'section' => 'dossie', 'representatives' => $representatives]);
    }

    public function dossie(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_DOSSIE_CRIACAO_EDICAO, true);
        $representative = false;
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) $representative = $this->em->getRepository(Representatives::class)->find($id);
        $posts = $this->em->getRepository(CandidatePost::class)->findBy([], ['name' => 'asc']);
        $states = $this->em->getRepository(State::class)->findBy([], ['estado' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/dossieRegister.phtml',
            'representative' => $representative, 'user' => $user, 'subMenu' => 'dam', 'section' => 'dossie',
            'posts' => $posts, 'states' => $states]);
    }

    public function saveDossie(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'Nome',
                'post' => 'Cargo',
                'start' => 'Inicio',
                'end' => 'Fim',
                'state' => 'Estado',
                'city' => 'Cidade',
                'votes' => 'Votos',
                'capture' => 'Capitação',
                'chiefOfStaff' => 'Chefe de Gabinete',
                'chiefOfStaffEmail' => 'Chefe de Gabinete E-mail',
                'chiefOfStaffPhone' => 'Chefe de Gabinete Whatsapp',
                //'alignment' => 'Alinhamento com o NOVO',
                //'digitalInfluence' => 'Influência Digital',
                //'negotiation' => 'Negociação',
                //'leadership' => 'Liderança',
                //'oratory' => 'Oratória',
                //'profile' => 'Perfil',
                //'politicalCareer' => 'Carreira Politica',
                //'guidelines' => 'Pautas',
                //'conquests' => 'Conquistas',
                //'productivity' => 'Produtividade',
            ];
            Validator::requireValidator($fields, $data);
            $data['capture'] = str_replace('.', '', $data['capture']);
            $data['capture'] = str_replace(',', '.', $data['capture']);
            $data['capture'] = str_replace('R$', '', $data['capture']);
            $data['capture'] = (float)trim($data['capture']);
            $representative = new Representatives();
            if ((int)$data['id'] > 0) $representative = $this->em->getRepository(Representatives::class)->find($data['id']);
            $representative->setName($data['name'])
                ->setPost($this->em->getReference(CandidatePost::class, $data['post']))
                ->setStart($data['start'])
                ->setEnd($data['end'])
                ->setState($this->em->getReference(State::class, $data['state']))
                ->setCity($this->em->getReference(City::class, $data['city']))
                ->setVotes($data['votes'])
                ->setCapture($data['capture'])
                ->setInstagram($data['instagram'])
                ->setFacebook($data['facebook'])
                ->setTwitter($data['twitter'])
                ->setSite($data['site'])
                ->setEmail($data['email'])
                ->setChiefOfStaff($data['chiefOfStaff'])
                ->setChiefOfStaffPhone($data['chiefOfStaffPhone'])
                ->setChiefOfStaffEmail($data['chiefOfStaffEmail'])
                //->setAlignment($data['alignment'])
                //->setDigitalInfluence($data['digitalInfluence'])
                //->setNegotiation($data['negotiation'])
                //->setLeadership($data['leadership'])
                //->setOratory($data['oratory'])
                ->setProfile($data['profile'])
                ->setPoliticalCareer($data['politicalCareer'])
                ->setGuidelines($data['guidelines'])
                ->setConquests($data['conquests']);
            //->setProductivity($data['productivity']);
            $this->em->getRepository(Representatives::class)->save($representative);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Cadastro realizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function interactionsIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_INTERACOES_VISUALIZACAO, true);
        $tags = $this->em->getRepository(KeyWord::class)->findBy(['type' => 1], ['name' => 'asc']);
        $types = $this->em->getRepository(InteractionType::class)->findBy([], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/interactions.phtml',
            'section' => 'interactions', 'user' => $user, 'tags' => $tags, 'subMenu' => 'dam', 'types' => $types]);
    }

    public function interactionsList(Request $request, Response $response)
    {
        $index = $request->getQueryParam('index');
        $user = $this->getLogged();
        $interactions = $this->em->getRepository(Interaction::class)->list($request->getQueryParams(), 20, $index * 20);
        $interactionsArray = [];
        foreach ($interactions as $interaction) {
            $interactionTag = [];
            $tags = $this->em->getRepository(InteractionKeyWord::class)->findBy(['interaction' => $interaction]);
            foreach ($tags as $tag) {
                $interactionTag[] = trim($tag->getKeyWord()->getName());
            }
            $interactionsArray[] = ['id' => $interaction->getId(), 'date' => $interaction->getDate()->format('Y-m-d'), 'type' => $interaction->getType()->getName(),
                'responsible' => $interaction->getResponsible(), 'interaction' => $interaction->getInteraction(),
                'tags' => implode(', ', $interactionTag)];
        }
        $total = $this->em->getRepository(Interaction::class)->listTotal($request->getQueryParams());
        $partial = ($index * 20) + sizeof($interactions);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $interactionsArray,
            'total' => (int)$total,
            'partial' => (int)$partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function interactionsView(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $interaction = $this->em->getRepository(Interaction::class)->findOneBy(['id' => $id]);
        $result = ['date' => $interaction->getDate()->format('d/m/Y'), 'type' => $interaction->getType()->getId(),
            'responsible' => $interaction->getResponsible(), 'interaction' => $interaction->getInteraction()];
        $tags = $this->em->getRepository(InteractionKeyWord::class)->findBy(['interaction' => $interaction]);
        $result['tags'] = [];
        foreach ($tags as $tag) {
            $result['tags'][] = $tag->getKeyWord()->getId();
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $result
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function interactionSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'type' => 'Tipo',
                'responsible' => 'Responsável',
                'date' => 'Data',
                'interaction' => 'Interação',
                'tags' => 'Tags',
            ];
            Validator::requireValidator($fields, $data);
            $interaction = new Interaction();
            if ($data['interactionId']) $interaction = $this->em->getRepository(Interaction::class)->findOneBy(['id' => $data['interactionId']]);
            $interaction->setDate(\DateTime::createFromFormat('d/m/Y', $data['date']))
                ->setInteraction($data['interaction'])
                ->setResponsible($data['responsible'])
                ->setType($this->em->getReference(InteractionType::class, $data['type']));
            $interaction = $this->em->getRepository(Interaction::class)->save($interaction);
            $this->em->getRepository(InteractionKeyWord::class)->dropByInteraction($interaction);
            foreach ($data['tags'] as $tag) {
                $interactionTag = new InteractionKeyWord();
                $interactionTag->setInteraction($interaction)
                    ->setKeyWord($this->em->getReference(KeyWord::class, $tag));
                $this->em->getRepository(InteractionKeyWord::class)->save($interactionTag);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Interação cadastrada com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function meetingSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'responsible' => 'Responsável',
                'date' => 'Data',
                'tags' => 'Tags',
            ];
            Validator::requireValidator($fields, $data);
            $interaction = new Interaction();
            if ($data['interactionId']) $interaction = $this->em->getRepository(Interaction::class)->findOneBy(['id' => $data['interactionId']]);
            $interaction->setDate(\DateTime::createFromFormat('d/m/Y', $data['date']))
                ->setInteraction('')
                ->setResponsible($data['responsible'])
                ->setType($this->em->getReference(InteractionType::class, InteractionType::MEETING));
            $interaction = $this->em->getRepository(Interaction::class)->save($interaction);
            $this->em->getRepository(InteractionKeyWord::class)->dropByInteraction($interaction);
            $this->em->getRepository(InteractionMeetingParticipant::class)->dropByInteraction($interaction);
            $this->em->getRepository(InteractionMeetingSchedule::class)->dropByInteraction($interaction);
            $this->em->getRepository(InteractionMeetingSubject::class)->dropByInteraction($interaction);
            $this->em->getRepository(InteractionMeetingSuggestion::class)->dropByInteraction($interaction);
            foreach ($data['tags'] as $tag) {
                $interactionTag = new InteractionKeyWord();
                $interactionTag->setInteraction($interaction)
                    ->setKeyWord($this->em->getReference(KeyWord::class, $tag));
                $this->em->getRepository(InteractionKeyWord::class)->save($interactionTag);
            }
            foreach ($data['schedule'] as $schedule) {
                $interactionMeetingSchedule = new InteractionMeetingSchedule();
                $interactionMeetingSchedule->setInteraction($interaction)
                    ->setSchedule($schedule);
                $this->em->getRepository(InteractionMeetingSchedule::class)->save($interactionMeetingSchedule);
            }
            foreach ($data['subject'] as $subject) {
                $interactionMeetingSubject = new InteractionMeetingSubject();
                $interactionMeetingSubject->setInteraction($interaction)
                    ->setSubject($subject);
                $this->em->getRepository(InteractionMeetingSubject::class)->save($interactionMeetingSubject);
            }
            foreach ($data['participantName'] as $key => $value) {
                $participant = new InteractionMeetingParticipant();
                $participant->setInteraction($interaction)
                    ->setParticipantName($value)
                    ->setParticipantRepresenting($data['participantRepresenting'][$key]);
                $this->em->getRepository(InteractionMeetingParticipant::class)->save($participant);
            }
            foreach ($data['suggestion'] as $key => $value) {
                $interactionMeetingSuggestion = new InteractionMeetingSuggestion();
                $interactionMeetingSuggestion->setInteraction($interaction)
                    ->setSuggestion($value)
                    ->setResponsible($data['suggestionResponsible'][$key]);
                $this->em->getRepository(InteractionMeetingSuggestion::class)->save($interactionMeetingSuggestion);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Reunião cadastrada com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function meetingIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DAM_INTERACOES_CRIACAO_EDICAO, true);
        $tags = $this->em->getRepository(KeyWord::class)->findBy(['type' => 1], ['name' => 'asc']);
        $id = $request->getAttribute('route')->getArgument('id');
        $interaction = null;
        $participants = $interactionTags = $schedules = $subjects = $suggestions = [];
        if ($id) {
            $interaction = $this->em->getRepository(Interaction::class)->findOneBy(['id' => $id]);
            $interactionTags = $this->em->getRepository(InteractionKeyWord::class)->findBy(['interaction' => $interaction]);
            $participants = $this->em->getRepository(InteractionMeetingParticipant::class)->findBy(['interaction' => $interaction]);
            $schedules = $this->em->getRepository(InteractionMeetingSchedule::class)->findBy(['interaction' => $interaction]);
            $subjects = $this->em->getRepository(InteractionMeetingSubject::class)->findBy(['interaction' => $interaction]);
            $suggestions = $this->em->getRepository(InteractionMeetingSuggestion::class)->findBy(['interaction' => $interaction]);
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/meeting.phtml',
            'section' => 'interactions', 'user' => $user, 'tags' => $tags, 'subMenu' => 'dam',
            'interaction' => $interaction, 'participants' => $participants, 'interactionTags' => $interactionTags,
            'schedules' => $schedules, 'subjects' => $subjects, 'suggestions' => $suggestions]);
    }
}
