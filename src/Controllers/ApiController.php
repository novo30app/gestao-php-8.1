<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\AccessLog;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\Area;
use App\Models\Entities\Bank;
use App\Models\Entities\Budget;
use App\Models\Entities\BudgetValues;
use App\Models\Entities\Candidate;
use App\Models\Entities\CandidateData;
use App\Models\Entities\City;
use App\Models\Entities\Contract;
use App\Models\Entities\CostCenter;
use App\Models\Entities\Directory;
use App\Models\Entities\DirectoryRegister;
use App\Models\Entities\Leaders;
use App\Models\Entities\OfxLinePaymentRequirement;
use App\Models\Entities\OfxLines;
use App\Models\Entities\PaymentRequerimentsApportionment;
use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\Position;
use App\Models\Entities\Provider;
use App\Models\Entities\Quiz1;
use App\Models\Entities\Result;
use App\Models\Entities\ResultadoEleicao;
use App\Models\Entities\State;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\Knowledge;
use App\Models\Entities\Communicated;
use App\Models\Entities\DirectoryAreaMeeting;
use App\Models\Entities\Events;
use App\Models\Entities\EventsRegistrations;
use App\Models\Entities\ChartOfAccounts;
use App\Models\Entities\CepMembers;
use App\Models\Entities\Indicator;
use App\Models\Entities\DirectoryAccounts;
use App\Models\Entities\DirectoryExtract;
use App\Models\Entities\Mesoregions;
use App\Models\Entities\MesoregionsCities;
use App\Models\Entities\OriginatingTransaction;
use App\Models\Entities\TrailsOfKnowledge;
use App\Models\Entities\LocalDocuments;
use App\Models\Entities\EventsCoupons;
use App\Models\Entities\EventsPress;
use App\Models\Entities\Zone;
use App\Models\Entities\Elected;
use App\Models\Entities\Sectors;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\UsersTypes;
use App\Models\Entities\PermissionUserTypeFuncionality;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiController extends Controller
{
    public function provider(Request $request, Response $response)
    {
        $this->getLogged(true);
        $cpfCnpj = $request->getAttribute('route')->getArgument('cpfCnpj');
        $directoryName = $request->getAttribute('route')->getArgument('directory');
        $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $directoryName, 'ativo' => 'S']);
        $provider = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => Utils::onlyNumbers($cpfCnpj)]);
        $contracts = $this->em->getRepository(Contract::class)->getContractsByCpf(Utils::onlyNumbers($cpfCnpj), $directory);
        $arr = [];
        foreach ($contracts as $contract) {
            $arr[] = [
                'id' => $contract['id'],
                'name' => $contract['name'],
                'number' => $contract['number'],
                'situation' => $contract['situation'],
                'stage' => $contract['stage']
            ];
        }
        return $response->withJson([
            'status' => 'ok',
            'id' => $provider ? $provider->getId() : '',
            'name' => $provider ? $provider->getName() : '',
            'type' => $provider ? $provider->getType() : '',
            'bank' => $provider ? $provider->getBank()?->getId() : '',
            'cod' => $provider ? $provider->getBank()?->getCod() : '',
            'account' => $provider ? $provider->getAccount() : '',
            'agency' => $provider ? $provider->getAgency() : '',
            'paymentMethod' => $provider ? $provider->getPaymentMethod() : '',
            'contracts' => $arr,

        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function users(Request $request, Response $response)
    {
        $user = $this->getLogged(true);

        $id = $request->getAttribute('route')->getArgument('id');
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $type = $request->getQueryParam('type');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $sector = $request->getQueryParam('sector');
        $index = $request->getQueryParam('index');
        if ($user->getType() != 3) $sector = $user->getSector()->getId();
        $users = $this->em->getRepository(UserAdmin::class)->list($user, $id, $name, $email, $type, $uf, $city, $sector, 20, $index * 20);
        $total = $this->em->getRepository(UserAdmin::class)->listTotal($user, $id, $name, $email, $type, $uf, $city, $sector)['total'];
        $cities = null;
        if ($id) {
            $cities = $this->em->getRepository(AccessAdmin::class)->listCities($id);
            $mesos = $this->em->getRepository(AccessAdmin::class)->listMesos($id);
        }
        $partial = ($index * 20) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial,
            'cities' => $cities,
            'mesos' => $mesos
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function leaders(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $name = $request->getQueryParam('name');
        $level = $request->getQueryParam('level');
        $status = $request->getQueryParam('status');
        $gender = $request->getQueryParam('gender');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $occupation = $request->getQueryParam('occupation');
        $index = $request->getQueryParam('index');
        $users = $this->em->getRepository(Leaders::class)->list($id, $name, $level, $status, $gender, $uf, $city, $occupation, 20, $index * 20);
        $total = $this->em->getRepository(Leaders::class)->listTotal($id, $name, $level, $status, $gender, $uf, $city, $occupation)['total'];
        $partial = ($index * 20) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function directories(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $level = $request->getQueryParam('level');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $users = $this->em->getRepository(DirectoryRegister::class)->list($id, $level, $uf, $city, 20, $index * 20);
        if ($id) {
            $dir = $this->em->getRepository(Directory::class)->findOneBy(['email' => $users[0]['email']]);
            if ($dir) {
                $arr = ['balanceCashFlow' => Utils::formatMoney($dir->getBalanceCashFlow())];
                $users = array_merge($users[0], $arr);
            }
        }
        $total = $this->em->getRepository(DirectoryRegister::class)->listTotal($id, $level, $uf, $city)['total'];
        $partial = ($index * 20) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function access(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $users = $this->em->getRepository(AccessLog::class)->list($name, $uf, $city, $limit, $index * $limit);
        $total = $this->em->getRepository(AccessLog::class)->listTotal($name, $uf, $city)['total'];
        $partial = ($index * $limit) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function candidates(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $name = $request->getQueryParam('name');
        $status = $request->getQueryParam('status');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $occupation = $request->getQueryParam('occupation');
        $year = $request->getQueryParam('year');
        $elected = $request->getQueryParam('elected');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $users = $this->em->getRepository(Candidate::class)->list($name, $status, $uf, $city, $occupation, $year, $elected, $limit, $index * $limit);
        $total = $this->em->getRepository(Candidate::class)->listTotal($name, $status, $uf, $city, $occupation, $year, $elected)['total'];
        $partial = ($index * $limit) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCandidates(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $status = $request->getQueryParam('status');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $occupation = $request->getQueryParam('occupation');
        $year = $request->getQueryParam('year');
        $elected = $request->getQueryParam('elected');
        $candidates = $this->em->getRepository(Candidate::class)->listExport($name, $status, $uf, $city, $occupation, $year, $elected);
        $filename = "Relatório de Candidatos - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Id', 'Nome', 'Status', 'Cargo', 'Situação', 'Desfiliação', 'Estado', 'Cidade', 'Cpf', 'E-mail', 'Votos', 'Ano', 'Telefone'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($candidates as $candidate) {
            fputcsv($out, array_values($candidate), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function defaulters(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $situation = $request->getQueryParam('situation');
        $meso = $request->getQueryParam('meso');
        $paymentMethod = $request->getQueryParam('paymentMethod');
        $email = $request->getQueryParam('email');
        $cpf = $request->getQueryParam('cpf');
        $affiliateds = $this->em->getRepository(Transaction::class)->defaulters($user, $name, $state, 
            $city, $situation, $meso, $email, $cpf, $paymentMethod, $limit, $index * $limit);
        $total = $this->em->getRepository(Transaction::class)->defaultersTotal($user, $name, $state, 
            $city, $situation, $meso, $email, $cpf, $paymentMethod)['total'];
        $partial = ($index * $limit) + sizeof($affiliateds);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $affiliateds,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function affiliateds(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $cpf = $request->getQueryParam('cpf');
        $email = $request->getQueryParam('email');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $state2 = $request->getQueryParam('state2');
        $city2 = $request->getQueryParam('city2');
        $status = $request->getQueryParam('status');
        $gender = $request->getQueryParam('gender');
        $ordination = $request->getQueryParam('ordination');
        $mesoregion = $request->getQueryParam('mesoregion');
        $agroup = $request->getQueryParam('agroup');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $typeDate = $request->getQueryParam('typeDate');
        $affiliateId = $request->getQueryParam('affiliateId');
        $irpf = $request->getQueryParam('irpf');
        $exemption = $request->getQueryParam('exemption');
        $ageBegin = $request->getQueryParam('ageBegin');
        $ageEnd = $request->getQueryParam('ageEnd');
        $situation = $request->getQueryParam('situation');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $total = $this->em->getRepository(User::class)->listTotal($user, $name, $cpf, $email, $uf, $city, $state2, $city2, $status, $gender, $ordination, $mesoregion, $ageBegin, $ageEnd, $situation, $agroup, $typeDate, $begin, $end, $affiliateId, $irpf, $exemption)['total'];
        $affiliateds = $this->em->getRepository(User::class)->list($user, $name, $cpf, $email, $uf, $city, $state2, $city2, $status, $gender, $ordination, $mesoregion, $ageBegin, $ageEnd, $situation, $agroup, $typeDate, $begin, $end, $affiliateId, $irpf, $exemption, $limit, $index * $limit);
        $partial = ($index * $limit) + sizeof($affiliateds);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $affiliateds,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function knowledge(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $total = $this->em->getRepository(Knowledge::class)->listTotal($user, $name, $state, $city)['total'];
        $knowledge = $this->em->getRepository(Knowledge::class)->list($user, $name, $state, $city, 20, $index * 20);
        $partial = ($index * 20) + sizeof($knowledge);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $knowledge,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function solicitations(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $type = $request->getQueryParam('type');
        $paymentForm = $request->getQueryParam('paymentForm');
        $plan = $request->getQueryParam('plan');
        $order = $request->getQueryParam('order');
        $orderType = $request->getQueryParam('orderType');
        $dateFilter = $request->getQueryParam('dateFilter');
        $dateFilterBegin = $request->getQueryParam('dateFilterBegin') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('dateFilterBegin')))) : "";
        $dateFilterEnd = $request->getQueryParam('dateFilterEnd') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('dateFilterEnd')))) : "";
        $total = $this->em->getRepository(User::class)->listTotalSolicitations($name, $state, $city, $status, $type, $paymentForm, $plan, $dateFilter, $dateFilterBegin, $dateFilterEnd)['total'];
        $solicitations = $this->em->getRepository(User::class)->listSolicitations($name, $state, $city, $status, $type, $paymentForm, $plan, $dateFilter, $dateFilterBegin, $dateFilterEnd, $order, $orderType, 20, $index * 20);
        $partial = ($index * 20) + sizeof($solicitations);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $solicitations,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function latestAffiliations(Request $request, Response $response)
    {
        $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $total = $this->em->getRepository(User::class)->listTotalLatestAffiliations($request->getQueryParams())['total'];
        $solicitations = $this->em->getRepository(User::class)->latestAffiliations($request->getQueryParams(), 20, $index * 20);
        $partial = ($index * 20) + sizeof($solicitations);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $solicitations,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function quiz(Request $request, Response $response)
    {
        $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $total = $this->em->getRepository(Quiz1::class)->listTotalSolicitations($request->getQueryParams())['total'];
        $solicitations = $this->em->getRepository(Quiz1::class)->listSolicitations($request->getQueryParams(), 20, $index * 20);
        $partial = ($index * 20) + sizeof($solicitations);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $solicitations,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function getCitiesByState(Request $request, Response $response)
    {
        $param = $request->getAttribute('route')->getArgument('state');
        $state = $this->em->getRepository(State::class)->find($param);
        if (!$state) {
            $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $param]);
        }
        $cities = $this->em->getRepository(City::class)->findBy(['state' => $state], ['cidade' => 'ASC']);
        $citiesArray = [];
        foreach ($cities as $city) {
            $citiesArray[] = ['id' => $city->getId(), 'name' => $city->getCidade()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $citiesArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function transactions(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $typeDate = $request->getQueryParam('typeDate');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $status = $request->getQueryParam('status');
        $origin = $request->getQueryParam('origin');
        $plan = $request->getQueryParam('plan');
        $payment = $request->getQueryParam('payment');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $directory = $request->getQueryParam('directory');
        $transactions = $this->em->getRepository(Transaction::class)->list($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, $directory, 20, $index * 20);
        $total = $this->em->getRepository(Transaction::class)->listTotal($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, $directory)['total'];
        $partial = ($index * 20) + sizeof($transactions);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $transactions,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function managementRecords(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $id = $request->getQueryParam('id');
        $affiliated_id = $request->getQueryParam('affiliated_id');
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $total = $this->em->getRepository(User::class)->listTotalRecords($id, $affiliated_id, $name, $email, $cpf, $status)['total'];
        $records = $this->em->getRepository(User::class)->listRecords($id, $affiliated_id, $name, $email, $cpf, $status, 20, $index * 20);
        $partial = ($index * 20) + sizeof($records);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $records,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function managementCommunicated(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $date = $request->getQueryParam('date') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('date')))) : "";
        $title = $request->getQueryParam('title');
        $file = $request->getQueryParam('file');
        $total = $this->em->getRepository(Communicated::class)->listTotal($date, $title, $file)['total'];
        $communicated = $this->em->getRepository(Communicated::class)->list($date, $title, $file, 20, $index * 20);
        $partial = ($index * 20) + sizeof($communicated);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais

        return $response->withJson([
            'status' => 'ok',
            'message' => $communicated,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function meetingsRecords(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $date = $request->getQueryParam('date') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('date')))) : "";
        $theme = $request->getQueryParam('theme');
        $subject = $request->getQueryParam('subject');
        $sector = $request->getQueryParam('sector');
        $total = $this->em->getRepository(DirectoryAreaMeeting::class)->listTotal($date, $theme, $subject, $sector)['total'];
        $meetings = $this->em->getRepository(DirectoryAreaMeeting::class)->list($date, $theme, $subject, $sector, $limit, $index * $limit);
        $partial = ($index * $limit) + sizeof($meetings);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais

        return $response->withJson([
            'status' => 'ok',
            'message' => $meetings,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function RegistrationsFilter(Request $request, Response $response)
    {
        $id = $request->getQueryParam('id');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $index = $request->getQueryParam('index');
        $ticket = $request->getQueryParam('ticket');
        $gender = $request->getQueryParam('gender');
        $total = $this->em->getRepository(EventsRegistrations::class)->listTotalParticipants($id, null, $name, $cpf, $status, $ticket, $gender)['total'];
        $participants = $this->em->getRepository(EventsRegistrations::class)->listRegistrationsParticipants($id, null, $name, $cpf, $status, $ticket, $gender, 25, $index * 25);
        $partial = ($index * 25) + sizeof($participants);
        $partial = $partial <= $participants ? $partial : $participants;
        return $response->withJson([
            'status' => 'ok',
            'message' => $participants,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function eventsFilter(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $name = $request->getQueryParam('name');
        $specie = $request->getQueryParam('specie');
        $directory = $request->getQueryParam('directory');
        $status = $request->getQueryParam('status');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $total = $this->em->getRepository(Events::class)->listTotal($user, $name, $specie, $directory, $status, $state, $city)['total'];
        $event = $this->em->getRepository(Events::class)->list($user, $name, $specie, $directory, $status, $state, $city, $limit, $index * $limit);
        $partial = ($index * $limit) + sizeof($event);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $event,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function CheckinFilter(Request $request, Response $response)
    {
        $id = $request->getQueryParam('id');
        $code = $request->getQueryParam('code');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $index = $request->getQueryParam('index');
        $ticket = $request->getQueryParam('ticket');
        $limit = $request->getQueryParam('limit');
        $total = $this->em->getRepository(EventsRegistrations::class)->listTotalParticipants($id, $code, $name, $cpf, $status, $ticket)['total'];
        $participants = $this->em->getRepository(EventsRegistrations::class)->listRegistrationsParticipants($id, $code, $name, $cpf, $status, $ticket, null, $limit, $index * $limit);
        $partial = ($index * $limit) + sizeof($participants);
        $partial = $partial <= $participants ? $partial : $participants;
        return $response->withJson([
            'status' => 'ok',
            'message' => $participants,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function exportCsv(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_GESTAODECADASTROS_EXPORTACAO, true);
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $state2 = $request->getQueryParam('state2');
        $city2 = $request->getQueryParam('city2');
        $status = $request->getQueryParam('status');
        $gender = $request->getQueryParam('gender');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $email = $request->getQueryParam('email');
        $mesoregion = $request->getQueryParam('mesoregion');
        $typeDate = $request->getQueryParam('typeDate');
        $affiliateId = $request->getQueryParam('affiliateId');
        $irpf = $request->getQueryParam('irpf');
        $exemption = $request->getQueryParam('exemption');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $ageBegin = $request->getQueryParam('ageBegin');
        $ageEnd = $request->getQueryParam('ageEnd');
        $situation = $request->getQueryParam('situation');
        $affiliateds = $this->em->getRepository(User::class)->listExport($user, $name, $cpf, $email, $state, $city, $state2, $city2, $status, $gender, $mesoregion,
            $ageBegin, $ageEnd, $situation, $typeDate, $begin, $end, $affiliateId, $irpf, $exemption);
        $filename = "Consultar pessoas " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Filiado ID', 'Status', 'Mensal/Semestral/Anual', 'Data do Cadastro', 'Data da Filiação', 'Data da Refiliação', 'Data da Desfiliação', 'Data da Solicitação de Filiação', 'Data da Solicitação de Desfiliação',
            'Nome', 'Gênero', 'Estado Civil', 'Telefone', 'Data Nascimento', 'Idade', 'Cpf', 'Rg', 'Pendência Título', 'Titulo', 'UF Título', 'Município Título', 'Zona Titutlo', 'Secao Título', 'Bairro', 'Endereço', 'Número', 'Complemento',
            'Cep', 'Município Endereço', 'UF Endereço', 'Motivo Desfiliação', 'Descrição Desfiliação', 'Profissao', 'Mesorregião', 'Membro'];
        if ($user->getLevel() == 3) array_push($header, 'E-mail');
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($affiliateds as $affiliated) {
            fputcsv($out, array_values($affiliated), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportCsvEventsRegistrations(Request $request, Response $response)
    {
        $this->getLogged();
        $id = $request->getQueryParam('id');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $ticket = $request->getQueryParam('ticket');
        $gender = $request->getQueryParam('gender');
        $participants = $this->em->getRepository(EventsRegistrations::class)->listRegistrationsParticipants($id, $name, $cpf, $status, $ticket, $gender);
        $filename = "Participantes evento " . $id . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID Inscrição', 'ID Pessoa', 'Código', 'Cpf', 'Nome', 'E-mail', 'Ingresso', 'Telefone', 'UF', 'Municipio', 'Valor', 'Data Inscrição',
            'Status', 'Data Checkin', 'Indicação', 'Voucher', 'Validação Voucher', 'Status Filiado', 'Forma de Pagamento', 'Cupom'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($participants as $participant) {
            fputcsv($out, array_values($participant), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportEventsTransactions(Request $request, Response $response)
    {
        $this->getLogged();
        $id = $request->getQueryParam('id');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $ticket = $request->getQueryParam('ticket');
        $gender = $request->getQueryParam('gender');
        $participants = $this->em->getRepository(EventsRegistrations::class)->exportEventsTransactions($id, $name, $cpf, $status, $ticket, $gender);
        $filename = "Transações evento " . $id . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'PessoaID', 'Nome', 'Cpf', 'Invoice ID', 'Recibo', 'Data de Criação', 'Data de Pagamento', 'UF', 'Cidade', 'Valor', 'Destino',
            'Status', 'Origem', 'Plano', 'Forma Pagamento'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($participants as $participant) {
            fputcsv($out, array_values($participant), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportCsvPress(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $eventId = $request->getQueryParam('eventId');
        $name = $request->getQueryParam('name');
        $vehicle = $request->getQueryParam('vehicle');
        $phone = $request->getQueryParam('phone');
        $type = $request->getQueryParam('type');
        $press = $this->em->getRepository(EventsPress::class)->list($name, $vehicle, $phone, $type, $eventId);
        $filename = "Inscrições Imprensa - Evento " . $eventId . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Nome Reporter', 'Veículo', 'Telefone', 'Camera', 'Fotógrafo', 'Registro'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($press as $p) {
            fputcsv($out, array_values($p), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportCsvDefaulters(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $situation = $request->getQueryParam('situation');
        $meso = $request->getQueryParam('meso');
        $paymentMethod = $request->getQueryParam('paymentMethod');
        $email = $request->getQueryParam('email');
        $cpf = $request->getQueryParam('cpf');
        $affiliateds = $this->em->getRepository(Transaction::class)->defaulters($user, $name, $state, 
            $city, $situation, $meso, $email, $cpf, $paymentMethod);
        $filename = "Relatório de adimplência " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Nome', 'Forma de Pagamento', 'UF', 'Município', 'Data Filiação', 'Plano', 'Telefone', 'Abertas', 'Pagas', 'Situação', 'genero'];
        if ($user->getLevel() == 3) array_push($header, "E-mail");
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($affiliateds as $affiliated) {
            fputcsv($out, array_values($affiliated), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportCsvTransactions(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::FINANCEIRO_RELATORIODETRANSACOES_EXPORTACAO, true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $typeDate = $request->getQueryParam('typeDate');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $status = $request->getQueryParam('status');
        $origin = $request->getQueryParam('origin');
        $plan = $request->getQueryParam('plan');
        $payment = $request->getQueryParam('payment');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $directory = $request->getQueryParam('directory');
        $transactions = $this->em->getRepository(Transaction::class)->list($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, $directory);
        $filename = "Relatório de transações " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'PessoaID', 'Nome', 'Cpf', 'Invoice ID', 'Recibo', 'Data de Criação', 'Data de Pagamento', 'UF', 'Cidade', 'Valor', 'Destino', 'Status', 'Origem', 'Plano', 'Forma Pagamento'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($transactions as $transaction) {
            fputcsv($out, array_values($transaction), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function conventions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $situation = $request->getQueryParam('situation');
        $type = $request->getQueryParam('type');
        $total = $this->em->getRepository(User::class)->listTotalConventions($user, $name, $cpf, $state, $city, $situation, $type)['total'];
        $affiliateds = $this->em->getRepository(User::class)->listConventions($user, $name, $cpf, $state, $city, $situation, $type, 20, $index * 20);
        $partial = ($index * 20) + sizeof($affiliateds);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $affiliateds,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCsvConventions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_CONVENCOES_EXPORTACAO, true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $situation = $request->getQueryParam('situation');
        $type = $request->getQueryParam('type');
        $affiliateds = $this->em->getRepository(User::class)->listConventions($user, $name, $cpf, $state, $city, $situation, $type);
        $filename = "Relatório de Votantes para convenção " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Filiado ID', 'Cpf', 'Filiação/Refiliação', 'Nome', 'Município', 'UF', 'Tipo', 'Situação', 'Meses em Aberto'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($affiliateds as $affiliated) {
            fputcsv($out, array_values($affiliated), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportContracts(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_CONTRATOS_EXPORTACAO, true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $filter = $request->getQueryParams();
        $contracts = $this->em->getRepository(Contract::class)->list($user, $filter, $limit, $index * $limit);
        $filename = "Relatório Contratos " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Número', 'Razão Social/Nome', 'CNPJ/CPF', 'Centro de Custo', 'Valor', 'Data Início', 'Data Fim', 'Status', 'Situação', 'Conta Contábil'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($contracts as $contract) {
            $cpf = strlen($contract['cpf']) == 11 ? Utils::mask($contract['cpf'], '###.###.###.-##') : Utils::mask($contract['cpf'], '##.###.###/####-##');
            $value = $contract['category'] == 1 ? 'R$ ' . number_format($contract['monthlyValue'], 2, ',', '.') : 'R$ ' . number_format($contract['value'], 2, ',', '.');

            $arr = [
                $contract['number'],
                $contract['name'],
                $cpf,
                $contract['costCenterName'],
                $value,
                $contract['dateStart'],
                $contract['dateEnd'],
                $this->em->getReference(Contract::class, $contract['id'])->getStageStr(),
                $this->em->getReference(Contract::class, $contract['id'])->getSituationStr(),
                $contract['InputAccounts']
            ];
            fputcsv($out, array_values($arr), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function disaffiliation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $status = $request->getQueryParam('status');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $typeDate = $request->getQueryParam('typeDate');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $total = $this->em->getRepository(User::class)->listTotalDisaffiliation($user, $name, $status, $state, $city, $typeDate, $begin, $end)['total'];
        $affiliates = $this->em->getRepository(User::class)->listDisaffiliation($user, $name, $status, $state, $city, $typeDate, $begin, $end, 20, $index * 20);
        $partial = ($index * 20) + sizeof($affiliates);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $affiliates,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCsvDisaffiliation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $status = $request->getQueryParam('status');
        $city = $request->getQueryParam('city');
        $typeDate = $request->getQueryParam('typeDate');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $ordination = $request->getQueryParam('ordination');
        $agroup = $request->getQueryParam('agroup');
        $affiliates = $this->em->getRepository(User::class)->listDisaffiliation($user, $name, $status, $state, $city, $typeDate, $begin, $end, $ordination, $agroup, null, null);
        $filename = "Relatório de Desfiliações - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Filiado ID', 'Nome', 'Município', 'UF', 'Data Filiação', 'Data desfiliação', 'Motivo desfiliação', 'Reclassificação desfiliação', 'Telefone',
            'Logradouro', 'Número', 'Bairro', 'Complemento', 'Cidade Endereço', 'Estado Endereço'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($affiliates as $affiliate) {
            fputcsv($out, array_values($affiliate), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function donors(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $total = $this->em->getRepository(User::class)->listTotalDonors($user, $name, $state, $city, $status)['total'];
        $donors = $this->em->getRepository(User::class)->listDonors($user, $name, $state, $city, $status, 20, $index * 20);
        $partial = ($index * 20) + sizeof($donors);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $donors,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCsvDonors(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_DOADORES_EXPORTACAO, true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $donors = $this->em->getRepository(User::class)->listDonors($user, $name, $state, $city, $status, null, null);
        $filename = "Relatório de Doadores - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Filiado ID', 'Nome', 'Data desfiliação', 'Status', 'UF', 'Cidade', 'Data filiação/Desfiliação', 'Total Qtde', 'Valor total'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($donors as $donor) {
            fputcsv($out, array_values($donor), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportCsvFinancialReport(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::FINANCEIRO_RELATORIODETRANSACOES_EXPORTACAO, true);
        $name = $request->getQueryParam('name');
        $typeDate = $request->getQueryParam('typeDate');
        $begin = $request->getQueryParam('begin') != "" ? date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $end = $request->getQueryParam('end') != "" ? date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $status = $request->getQueryParam('status');
        $origin = $request->getQueryParam('origin');
        $plan = $request->getQueryParam('plan');
        $payment = $request->getQueryParam('payment');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $directory = $request->getQueryParam('directory');
        $transactions = $this->em->getRepository(Transaction::class)->exportFinancialReport($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, $directory);
        $filename = "Relatório Financeiro - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Invoice', 'Nome', 'Cpf', 'Titulo Eleitoral', 'Transação ID', 'Data Pagamento', 'Data Depósito', 'Data Liberação', 'Data Cancelamento', 'Número Recibo', 
            'NSU', 'Observação', 'Status', 'Evento', 'Bandeira Cartão', 'Diretório ID', 'Diretório Origem', 'Estado Titulo', 'Cidade Titulo', 'Estado Endereço', 
            'Cidade Endereço', 'Valor Bruto', 'Valor Liquido', 'Taxas', 'Rateio DM', 'Rateio DE', 'Rateio DN', 'Forma Pagamento', 'Periodicidade', 'Origem Transacao', 'Gateway Pagamento'
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($transactions as $transaction) {
            fputcsv($out, array_values($transaction), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportCsvKnowledge(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_COMOCONHECEU_EXPORTACAO, true);
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $knowledge = $this->em->getRepository(Knowledge::class)->list($user, $name, $state, $city);
        $filename = "Relatório de Como Conheceu " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID Pessoa', 'Nome', 'Tipo', 'Sub-tipo', 'UF', 'Cidade', 'Evento', 'Filiado_ID', 'Descrição', 'Data'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($knowledge as $k) {
            fputcsv($out, array_values($k), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function chartOfAccounts(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $reduced = $request->getQueryParam('reduced');
        $surname = $request->getQueryParam('surname');
        $account = $request->getQueryParam('account');
        $description = $request->getQueryParam('description');
        $type = $request->getQueryParam('type');
        $category = $request->getQueryParam('category');
        $specie = $request->getQueryParam('specie');
        $accounts = $this->em->getRepository(ChartOfAccounts::class)->list($reduced, $surname, $account, $description, $type, $category, $specie, 20, $index * 20);
        $total = $this->em->getRepository(ChartOfAccounts::class)->listTotal($reduced, $surname, $account, $description, $type, $category, $specie)['total'];
        $partial = ($index * 20) + sizeof($accounts);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $accounts,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCsvAccounts(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = 0;
        $reduced = $request->getQueryParam('reduced');
        $surname = $request->getQueryParam('surname');
        $account = $request->getQueryParam('account');
        $description = $request->getQueryParam('description');
        $type = $request->getQueryParam('type');
        $category = $request->getQueryParam('category');
        $specie = $request->getQueryParam('specie');
        $accounts = $this->em->getRepository(ChartOfAccounts::class)->list($reduced, $surname, $account, $description, $type, $category, $specie);
        $filename = "Plano de Contas - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Criação', 'Ativo', 'Reduzida', 'Apelido', 'Conta', 'Descrição', 'Tipo', 'Categoria'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($accounts as $account) {
            fputcsv($out, array_values($account), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportTransactionsRescue(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $rescue = $request->getAttribute('route')->getArgument('rescue');
        $transactions = $this->em->getRepository(Transaction::class)->listTransactionsRescue($rescue);
        $filename = "Transações resgate {$rescue} - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'PessoaID', 'Nome', 'Cpf', 'Invoice ID', 'Recibo', 'Data de Pagamento', 'Data Prevista de Depósito', 'UF', 'Cidade', 'Valor Bruto', 'Valor Líquido', 'Destino', 'Status', 'Origem', 'Plano', 'Forma Pagamento', 'Resgate'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($transactions as $transaction) {
            fputcsv($out, array_values($transaction), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function membersList(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $members = $this->em->getRepository(CepMembers::class)->list($name, 20, $index * 20);
        $total = $this->em->getRepository(CepMembers::class)->listTotal($name)['total'];
        $partial = ($index * 20) + sizeof($members);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $members,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function whoIndicated(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $total = $this->em->getRepository(Indicator::class)->listTotal($user, $name, $state, $city)['total'];
        $whoIndicated = $this->em->getRepository(Indicator::class)->list($user, $name, $state, $city, 20, $index * 20);
        $partial = ($index * 20) + sizeof($whoIndicated);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $whoIndicated,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCsvWhoIndicated(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::FILIADOS_QUEMINDICOU_EXPORTACAO, true);
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $list = $this->em->getRepository(Indicator::class)->listExport($user, $name, $state, $city);
        $filename = "Planilha Quem Indicou - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID Pessoa', 'Data', 'Nome', 'ID Quem Indicou', 'Nome Quem Indicou', 'Municipio', 'UF', 'Evento', 'Origem'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportBillsToPay(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(false, $request->getAttribute('route')->getArgument('diretiva'));
        Validator::validatePermission(SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_EXPORTACAO, true);
        $id = $request->getQueryParam('id');
        $dateType = $request->getQueryParam('dateType');
        $dateBegin = $request->getQueryParam('dateBegin');
        $dateEnd = $request->getQueryParam('dateEnd');
        $accounting = $request->getQueryParam('accounting');
        $status = $request->getQueryParam('status');
        $origin = $request->getQueryParam('origin');
        $cpfCnpj = $request->getQueryParam('cpfCnpj');
        $provider = $request->getQueryParam('provider');
        $docNumber = $request->getQueryParam('docNumber');
        $type = $request->getQueryParam('type');
        $costCenter = $request->getQueryParam('costCenter');
        $user = $request->getQueryParam('user');
        $directory = $request->getQueryParam('directory');
        $directoryPaying = $request->getQueryParam('directoryPaying');
        $value = $request->getQueryParam('value');
        $paymentValue = $request->getQueryParam('paymentValue');
        $area = $request->getQueryParam('area');
        $accounted = $request->getQueryParam('accounted');
        $bbStatus = $request->getQueryParam('bbStatus');
        $list = $this->em->getRepository(PaymentRequirements::class)->listExport($userAdmin, $id, $dateType, $dateBegin, $dateEnd,
            $accounting, $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $user, $directory, $directoryPaying, $value,
            $paymentValue, $area, $accounted, $bbStatus);
        $filename = "Solicitações de pagamentos - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Vencimento', 'Usuário', 'Fornecedor', 'Cpf/Cnpj', 'Criação', 'Valor', 'Diretório', 'Status', 'Origem', 'Tipo',
            'Descrição do Documento', 'Série do Documento', 'Quantidade/ itens da nota', 'Fonte de Recurso', 'N° de documento do Pagamento', 'Conta Bancária',
            'Forma de pagamento', 'Data Emissão', 'Data de pagamento', 'Data estorno', 'Valor de pagamento', 'Nº do Extrato', 'Centro de Custo', 'Área', 'Data da Competência', 'Pendência'
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function directoryAccounts(Request $request, Response $response)
    {
        $user = $this->getLogged(true, $request->getQueryParam('userType') == 5);
        $index = $request->getQueryParam('index');
        $directory = $request->getQueryParam('directory');
        $nickname = $request->getQueryParam('nickname');
        $extract = $request->getQueryParam('extract');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $begin = $request->getQueryParam('begin');
        $end = $request->getQueryParam('end');
        $total = $this->em->getRepository(DirectoryAccounts::class)->listTotal($user, $directory, $nickname, $extract, $state, $city, $begin, $end)['total'];
        $accounts = $this->em->getRepository(DirectoryAccounts::class)->list($user, $directory, $nickname, $extract, $state, $city, $begin, $end, 20, $index * 20);
        $partial = ($index * 20) + sizeof($accounts);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $accounts,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function extractAccounts(Request $request, Response $response)
    {
        $user = $this->getLogged(true, $request->getQueryParam('userType') == 5);
        $index = $request->getQueryParam('index');
        $account = $request->getQueryParam('accountID');
        $begin = $request->getQueryParam('begin');
        $end = $request->getQueryParam('end');
        $validation = $request->getQueryParam('validation');
        $total = $this->em->getRepository(DirectoryExtract::class)->listTotal($account, $begin, $end, $validation)['total'];
        $accounts = $this->em->getRepository(DirectoryExtract::class)->list($account, $begin, $end, $validation, 20, $index * 20);
        $partial = ($index * 20) + sizeof($accounts);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $accounts,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function mesoregions(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $mesos = $this->em->getRepository(Mesoregions::class)->list($user, $name, $state, 20, $index * 20);
        $total = $this->em->getRepository(Mesoregions::class)->listTotal($user, $name, $state)['total'];
        $partial = ($index * 20) + sizeof($mesos);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $mesos,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function mesoregionsCities(Request $request, Response $response)
    {
        //$user = $this->getLogged(true);
        $id = $request->getQueryParam('id');
        $cities = $this->em->getRepository(MesoregionsCities::class)->list($id);
        return $response->withJson([
            'status' => 'ok',
            'message' => $cities,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportOriginatingTransaction(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $requirement = $request->getQueryParam('requirement');
        $list = $this->em->getRepository(OriginatingTransaction::class)->listExport($requirement);
        $filename = "Planilha de Originários - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Transação', 'Nome', 'CPF', 'Repasse', 'Recibo', 'Valor', 'Invoice'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportAccountReport(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(true);
        $dateType = $request->getQueryParam('dateType');
        $dateBegin = $request->getQueryParam('dateBegin');
        $dateEnd = $request->getQueryParam('dateEnd');
        $accounting = $request->getQueryParam('accounting');
        $status = intVal($request->getQueryParam('status'));
        $origin = $request->getQueryParam('origin');
        $cpfCnpj = $request->getQueryParam('cpfCnpj');
        $provider = $request->getQueryParam('provider');
        $docNumber = $request->getQueryParam('docNumber');
        $type = $request->getQueryParam('type');
        $costCenter = $request->getQueryParam('costCenter');
        $user = $request->getQueryParam('user');
        $directory = $request->getQueryParam('directory');
        $directoryPaying = $request->getQueryParam('directoryPaying');
        $value = $request->getQueryParam('value');
        $paymentValue = $request->getQueryParam('paymentValue');
        $list = $this->em->getRepository(PaymentRequirements::class)->exportAccountReport($userAdmin, $dateType, $dateBegin, $dateEnd, $accounting,
            $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $user, $directory, $directoryPaying, $value, $paymentValue);
        $filename = "Relatório Contábil Contas a Pagar - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['CNPJ Prestador', 'Nome/Razão Social Fornecedor', 'Esfera Partidária', 'Data Emissão/Contratação', 'Tipo Documento', 'Descrição Gasto',
            'Descrição Outros', 'Lançamento', 'Data', 'Débito', 'Crédito', 'Valor', 'Histórico Padrão', 'Complemento', 'CCDB', 'CCCR', 'CNPJ'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportFiscalReport(Request $request, Response $response)
    {
        $userAdmin = $this->getLogged(true);
        $dateType = $request->getQueryParam('dateType');
        $dateBegin = $request->getQueryParam('dateBegin');
        $dateEnd = $request->getQueryParam('dateEnd');
        $accounting = $request->getQueryParam('accounting');
        $status = intVal($request->getQueryParam('status'));
        $origin = $request->getQueryParam('origin');
        $cpfCnpj = $request->getQueryParam('cpfCnpj');
        $provider = $request->getQueryParam('provider');
        $docNumber = $request->getQueryParam('docNumber');
        $type = $request->getQueryParam('type');
        $costCenter = $request->getQueryParam('costCenter');
        $user = $request->getQueryParam('user');
        $directory = $request->getQueryParam('directory');
        $directoryPaying = $request->getQueryParam('directoryPaying');
        $value = $request->getQueryParam('value');
        $valpaymentValueue = $request->getQueryParam('paymentValue');
        $list = $this->em->getRepository(PaymentRequirements::class)->exportFiscalReport($userAdmin, $dateType, $dateBegin, $dateEnd, $accounting,
            $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $user, $directory, $directoryPaying, $value, $paymentValue);
        $filename = "Relatório Fiscal Contas a Pagar - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Tipo de Registro', 'Nº NFS-e', 'Data Hora NFE', 'Código de Verificação da NFS-e', 'Tipo de RPS', 'Série do RPS', 'Número do RPS', 'Data do Fato Gerador',
            'Inscrição Municipal do Prestador', 'Indicador de CPF/CNPJ do Prestador', 'CPF/CNPJ do Prestador', 'Razão Social do Prestador', 'Tipo do Endereço do Prestador',
            'Endereço do Prestador', 'Número do Endereço do Prestador', 'Complemento do Endereço do Prestador', 'Bairro do Prestador', 'Cidade do Prestador', 'UF do Prestador',
            'CEP do Prestador', 'Email do Prestador', 'Opção Pelo Simples', 'Situação da Nota Fiscal', 'Data de Cancelamento', 'Nº da Guia', 'Data de Quitação da Guia Vinculada a Nota Fiscal',
            'Valor dos Serviços', 'Valor das Deduções', 'Código do Serviço Prestado na Nota Fiscal', 'Alíquota', 'ISS devido', 'Valor do Crédito', 'ISS Retido',
            'Indicador de CPF/CNPJ do Tomador', 'CPF/CNPJ do Tomador', 'Inscrição Municipal do Tomador', 'Inscrição Estadual do Tomador', 'Razão Social do Tomador',
            'Tipo do Endereço do Tomador', 'Endereço do Tomador', 'Número do Endereço do Tomador', 'Complemento do Endereço do Tomador', 'Bairro do Tomador',
            'Cidade do Tomador', 'UF do Tomador', 'CEP do Tomador', 'Email do Tomador', 'Nº NFS-e Substituta', 'ISS recolhido', 'ISS a recolher', 'Indicador de CPF/CNPJ do Intermediário',
            'CPF/CNPJ do Intermediário', 'Inscrição Municipal do Intermediário', 'Razão Social do Intermediário', 'Repasse do Plano de Saúde', 'PIS/PASEP', 'COFINS',
            'INSS', 'IR', 'CSLL', 'Carga tributária: Valor', 'Carga tributária: Porcentagem', 'Carga tributária: Fonte', 'CEI', 'Matrícula da Obra',
            'Município Prestação - cód. IBGE', 'Situação do Aceite', 'Encapsulamento', 'Valor Total Recebido', 'Tipo de Consolidação', 'Nº NFS-e Consolidada', 'Campo Reservado',
            'Discriminação dos Serviços'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($list as $l) {
            fputcsv($out, array_values($l), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function trailOfKnowledge(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $keyWord = $request->getQueryParam('keyWord');
        $modules = $request->getQueryParam('modules');
        $classes = $request->getQueryParam('classes');
        $class = $this->em->getRepository(TrailsOfKnowledge::class)->list($keyWord, $modules, $classes, $limit, $index * $limit);
        $total = $this->em->getRepository(TrailsOfKnowledge::class)->listTotal($keyWord, $modules, $classes)['total'];
        $partial = ($index * $limit) + sizeof($class);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $class,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function costCenterByDirectory(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getAttribute('route')->getArgument('directory');
        $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $name]);
        $arr = [];
        if ($directory) {
            $costsCenter = $this->em->getRepository(CostCenter::class)->findBy(['directory' => $directory, 'status' => 1], ['name' => 'asc']);
            foreach ($costsCenter as $item) {
                $arr[] = [
                    'id' => $item->getId(),
                    'name' => $item->getName(),
                ];
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function tableApproved(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $class = $this->em->getRepository(PaymentRequirements::class)->listApproved($user, $request->getQueryParams(), $limit, $index * $limit);
        $total = $this->em->getRepository(PaymentRequirements::class)->listTotalApproved($user, $request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($class);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $class,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function getCabinets(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $year = $request->getQueryParam('year');
        $total = $this->em->getRepository(Candidate::class)->listTotalCabinets($name, $state, $year)['total'];
        $cabinets = $this->em->getRepository(Candidate::class)->listCabinets($name, $state, $year, 20, $index * 20);
        $partial = ($index * 20) + sizeof($cabinets);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $cabinets,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function bankByCode(Request $request, Response $response)
    {
        $this->getLogged(true);
        $cod = $request->getAttribute('route')->getArgument('cod');
        $bank = $this->em->getRepository(Bank::class)->findOneBy(['cod' => $cod]);
        if (!$bank) throw new \Exception('Código inválido!');
        return $response->withJson([
            'status' => 'ok',
            'message' => $bank->getId(),
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function localDocuments(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $keyWord = $request->getQueryParam('keyWord');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $documents = $this->em->getRepository(LocalDocuments::class)->list($user, $keyWord, $state, $city, $limit, $index * $limit);
        $total = $this->em->getRepository(LocalDocuments::class)->listTotal($user, $keyWord, $state, $city, $classes)['total'];
        $partial = ($index * $limit) + sizeof($documents);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $documents,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function reaffiliation(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $total = $this->em->getRepository(User::class)->listTotalReaffiliations($user, $request->getQueryParams())['total'];
        $solicitations = $this->em->getRepository(User::class)->listReaffiliations($user, $request->getQueryParams(), 20, $index * 20);
        $partial = ($index * 20) + sizeof($solicitations);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $solicitations,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function coupons(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $name = $request->getQueryParam('name');
        $event = $request->getQueryParam('event');
        $status = $request->getQueryParam('status');
        $eventId = $request->getQueryParam('eventId');
        $coupons = $this->em->getRepository(EventsCoupons::class)->list($name, $event, $status, $eventId, 20, $index * 20);
        $total = $this->em->getRepository(EventsCoupons::class)->listTotal($name, $event, $status, $eventId)['total'];
        $partial = ($index * 20) + sizeof($coupons);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $coupons,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function press(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $name = $request->getQueryParam('name');
        $vehicle = $request->getQueryParam('vehicle');
        $phone = $request->getQueryParam('phone');
        $type = $request->getQueryParam('type');
        $eventId = $request->getQueryParam('eventId');
        $press = $this->em->getRepository(EventsPress::class)->list($name, $vehicle, $phone, $type, $eventId, 20, $index * 20);
        $total = $this->em->getRepository(EventsPress::class)->listTotal($name, $vehicle, $phone, $type, $eventId)['total'];
        $partial = ($index * 20) + sizeof($press);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $press,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportLeaders(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CONSULTAS_DIRIGENTES_EXPORTACAO, true);
        $name = $request->getQueryParam('name');
        $level = $request->getQueryParam('level');
        $status = $request->getQueryParam('status');
        $gender = $request->getQueryParam('gender');
        $uf = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $occupation = $request->getQueryParam('occupation');
        $index = $request->getQueryParam('index');
        $leaders = $this->em->getRepository(Leaders::class)->list($name, $level, $status, $gender, $uf, $city, $occupation);
        $filename = "Relatório de Líderes - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Nome', 'CPF', 'E-mail', 'Início Mandato', 'Fim mandato', 'Estado', 'Cidade', 'Cargo', 'Status', 'Genero', 'Tipo'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($leaders as $leader) {
            fputcsv($out, array_values($leader), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function listOfxPendencies(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $pendencies = $this->em->getRepository(OfxLines::class)->list($request->getQueryParams());
        $total = $this->em->getRepository(OfxLines::class)->total($request->getQueryParams());
        $arr = [];
        $partial = ($index * $limit) + count($pendencies);
        $partial = $partial <= $total ? $partial : $total;

        foreach ($pendencies as $p) {
            $payments = [];

            if ($p->isMultiple()) {
                $requeriments = $this->em->getRepository(OfxLinePaymentRequirement::class)->findBy(['ofxLine' => $p]);
                foreach ($requeriments as $r) {
                    $r = $r->getRequirements();
                    $payments[] = [
                        'id' => $r->getId(),
                        'user' => $r->getUser() ? $r->getUser()->getName() : '',
                        'origin' => $r->getOriginStr(),
                        'type' => $r->getTypeStr(),
                        'beneficiary' => $r->getBeneficiary() ?: $r->getProvider()->getName(),
                        'attachments' => $r->getOfxAttachments(),
                        'statusStr' => $r->getRequestStatusString(),
                        'status' => $r->getRequestStatus(),
                    ];
                }
            } elseif ($p->getRequirements()) {
                $payments[] = [
                    'id' => $p->getRequirements()->getId(),
                    'user' => $p->getRequirements()->getUser() ? $p->getRequirements()->getUser()->getName() : '',
                    'origin' => $p->getRequirements()->getOriginStr(),
                    'type' => $p->getRequirements()->getTypeStr(),
                    'beneficiary' => $p->getRequirements()->getBeneficiary() ?: $p->getRequirements()->getProvider()->getName(),
                    'attachments' => $p->getRequirements()->getOfxAttachments(),
                    'statusStr' => $p->getRequirements()->getRequestStatusString(),
                    'status' => $p->getRequirements()->getRequestStatus(),
                ];
            } else {
                $requeriments = $this->em->getRepository(PaymentRequirements::class)->requerimentsByOfx($p);
                foreach ($requeriments as $r) {
                    $payments[] = [
                        'id' => $r->getId(),
                        'user' => $r->getUser() ? $r->getUser()->getName() : '',
                        'origin' => $r->getOriginStr(),
                        'type' => $r->getTypeStr(),
                        'beneficiary' => $r->getBeneficiary() ?: $r->getProvider()->getName(),
                        'attachments' => $r->getOfxAttachments(),
                        'statusStr' => $r->getRequestStatusString(),
                        'status' => $r->getRequestStatus(),
                    ];
                }
            }

            //entradas para possivel estorno
            if ($p->getRefund()) {
                $reversalsArray[] = [
                    'date' => $p->getRefund()->getDate()->format('d/m/Y'),
                    'description' => $p->getRefund()->getDescription(),
                    'id' => $p->getRefund()->getId(),
                    'value' => Utils::formatMoney($p->getRefund()->getValue()),
                    'Aaccount' => $p->getRefund()->getDirectoryAccount()->getAccountString(),
                    'uniqueIdentifier' => $p->getRefund()->getUniqueIdentifier(),
                    'extractNumber' => $p->getRefund()->getExtractNumber(),
                ];
            } else {
                $reversals = $this->em->getRepository(OfxLines::class)->reversals($p);
                $reversalsArray = [];
                foreach ($reversals as $r) {
                    $reversalsArray[] = [
                        'date' => $r->getDate()->format('d/m/Y'),
                        'description' => $r->getDescription(),
                        'id' => $r->getId(),
                        'value' => Utils::formatMoney($r->getValue()),
                        'Aaccount' => $r->getDirectoryAccount()->getAccountString(),
                        'uniqueIdentifier' => $r->getUniqueIdentifier(),
                        'extractNumber' => $r->getExtractNumber(),
                    ];
                }
            }

            $arr[] = [
                'id' => $p->getId(),
                'multiple' => $p->isMultiple(),
                'payment' => $p->getRequirements()?->getId(),
                'refund' => $p->getRefund()?->getId(),
                'date' => $p->getDate()->format('d/m/Y'),
                'type' => $p->getType(),
                'value' => $p->getValue() * -1,
                'uniqueIdentifier' => $p->getUniqueIdentifier(),
                'extractNumber' => $p->getExtractNumber(),
                'description' => $p->getDescription(),
                'ofxFiles' => $p->getOfxFiles()->getId(),
                'directoryAccount' => $p->getDirectoryAccount()->getId(),
                'directoryAccountNickname' => $p->getDirectoryAccount()->getNickname(),
                'directory' => $p->getDirectory()->getId(),
                'directoryName' => $p->getDirectory()->getName(),
                'payments' => $payments,
                'reversals' => $reversalsArray,
                'directoryAccountAgency' => $p->getOfxFiles()->getDirectoryAccount()->getAgencyString(),
                'directoryAccountAccount' => $p->getOfxFiles()->getDirectoryAccount()->getAccountString(),
            ];
        }

        return $response->withJson([
            'status' => 'ok',
            'message' => $arr,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function budgets(Request $request, Response $response)
    {
        $this->getLogged(true);
        $filter = $request->getQueryParams();
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        if ($filter['groupBy'] == 0) {
            $users = $this->em->getRepository(Budget::class)->list($filter, $limit, $index * $limit);
            $total = $this->em->getRepository(Budget::class)->listTotal($filter)['total'];
        } else if ($filter['groupBy'] == 1) {
            $users = $this->em->getRepository(Budget::class)->listByCostCenter($filter, $limit, $index * $limit);
            $total = $this->em->getRepository(Budget::class)->listTotalByCostCenter($filter)['total'];
        } else if ($filter['groupBy'] == 2) {
            $users = $this->em->getRepository(Budget::class)->listByDirectory($filter, $limit, $index * $limit);
            $total = $this->em->getRepository(Budget::class)->listTotalByDirectory($filter)['total'];
        }
        $partial = ($index * $limit) + sizeof($users);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $users,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function budgetsDetails(Request $request, Response $response)
    {
        $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = 10;
        $groupBy = $request->getQueryParam('groupBy');

        // Busca de agrupamento por área
        if ($groupBy == 0) {
            $result = $this->em->getRepository(Budget::class)->listDetails($request->getQueryParams(), $index * $limit);
            $total = $this->em->getRepository(Budget::class)->listTotal($request->getQueryParams())['total'];

            foreach ($result as $key => $item) {
                foreach ($item['values'] as $k => $value) {

                    $filter = [
                        'month' => $value['month'],
                        'year' => $item['year'],
                        'costCenter' => $item['ccId'],
                        'area' => $item['aId'],
                        'dateType' => $request->getQueryParam('dateType'),
                        'groupBy' => $groupBy,
                    ];

                    $accomplished = $this->em->getRepository(PaymentRequerimentsApportionment::class)->totalByBudgetValues($filter);
                    $result[$key]['values'][$k]['accomplished'] = $accomplished;
                }
            }

        } elseif ($groupBy == 1) {
            //Busca de agrupamento por centro de custo
            $result = $this->em->getRepository(Budget::class)->listDetailsByCostCenter($request->getQueryParams(), $index * $limit);
            $total = $this->em->getRepository(Budget::class)->listTotalDetailsByCostCenter($request->getQueryParams());
            $total = count($total);

            foreach ($result as $key => $item) {
                foreach ($item['values'] as $k => $value) {

                    $filter = [
                        'month' => $value['month'],
                        'year' => $item['year'],
                        'costCenter' => $item['ccId'],
                        'dateType' => $request->getQueryParam('dateType'),
                        'groupBy' => $groupBy,
                    ];

                    $accomplished = $this->em->getRepository(PaymentRequerimentsApportionment::class)->totalByCostCenterValues($filter);

                    $result[$key]['values'][$k]['accomplished'] = $accomplished;
                }
            }
        } elseif ($groupBy == 2) {
            //Busca de agrupamento por diretorio
            $result = $this->em->getRepository(Budget::class)->listDetailsByDirectory($request->getQueryParams(), $index * $limit);
            $total = $this->em->getRepository(Budget::class)->listTotalDetailsByDirectory($request->getQueryParams());
            $total = count($total);

            foreach ($result as $key => $item) {
                foreach ($item['values'] as $k => $value) {

                    $filter = [
                        'month' => $value['month'],
                        'year' => $item['year'],
                        'directory' => $item['dId'],
                        'dateType' => $request->getQueryParam('dateType'),
                        'groupBy' => $groupBy,
                    ];

                    $accomplished = $this->em->getRepository(PaymentRequerimentsApportionment::class)->totalByDirectoryValues($filter);

                    $result[$key]['values'][$k]['accomplished'] = $accomplished;
                }
            }
        }

        $partial = ($index * $limit) + sizeof($result);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'total' => (int)$total,
            'partial' => $partial,
            'message' => $result,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function budgetData(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $budget = $this->em->getRepository(Budget::class)->data($id);
        return $response->withJson([
            'status' => 'ok',
            'message' => $budget,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function allRegistrations(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $event = $request->getQueryParam('event');
        $name = $request->getQueryParam('name');
        $gender = $request->getQueryParam('gender');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $registrations = $this->em->getRepository(Events::class)->listGeralRegistrations($user, $event, $name, $gender, $cpf, $status, $state, $city, $limit, $index * $limit);
        $total = $this->em->getRepository(Events::class)->listTotalGeralRegistrations($user, $event, $name, $gender, $cpf, $status, $state, $city)['total'];
        $partial = ($index * $limit) + sizeof($registrations);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $registrations,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function allRegistrationsExport(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $event = $request->getQueryParam('event');
        $name = $request->getQueryParam('name');
        $gender = $request->getQueryParam('gender');
        $cpf = $request->getQueryParam('cpf');
        $status = $request->getQueryParam('status');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $registrations = $this->em->getRepository(Events::class)->listGeralRegistrations($user, $event, $name, $gender, $cpf, $status, $state, $city, $limit, $index * $limit);
        $filename = "Relatório de Líderes - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Evento ID', 'Pessoa ID', 'Nome', 'Cidade/Estado', 'CPF', 'Status Inscrição', 'Evento', 'Data Evento', 'Quem Indicou'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($registrations as $registration) {
            fputcsv($out, array_values($registration), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportDirectoryAccounts(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $directory = $request->getQueryParam('directory');
        $nickname = $request->getQueryParam('nickname');
        $extract = $request->getQueryParam('extract');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $begin = $request->getQueryParam('begin');
        $end = $request->getQueryParam('end');
        $accounts = $this->em->getRepository(DirectoryAccounts::class)->list($user, $directory, $nickname, $extract, $state, $city, $begin, $end);
        $filename = "Relatório de Contas de Diretórios - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Diretório', 'Diretório ID', 'CNPJ', 'Banco', 'Agência', 'Conta', 'Digito agência', 'Digito conta', 'Apelido', 'Agencia',
            'Conta', 'Descrição Conta', 'Tipo de Conta', 'Conta Contábil', 'Marco Zero', 'Valor Inicial', 'Extrato ID'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($accounts as $account) {
            fputcsv($out, array_values($account), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function exportExtract(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $extracts = $this->em->getRepository(DirectoryExtract::class)->export();
        $filename = "Extratos - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['envio extrato', 'diretório', 'sigla', 'cidade', 'agencia', 'conta', 'nome da conta', 'inicio extrato', 'fim extrato', 'link extrato', 'usuario'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($extracts as $extract) {
            fputcsv($out, array_values($extract), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function affiliatedsByCity(Request $request, Response $response)
    {
        $city = $request->getAttribute('route')->getArgument('city');
        $activesAffiliates = count($this->em->getRepository(User::class)->findBy(['filiado' => [7, 8], 'tituloEleitoralMunicipioId' => $city, 'exemption' => 0]));
        $exemptAffiliates = count($this->em->getRepository(User::class)->findBy(['filiado' => [7, 8], 'tituloEleitoralMunicipioId' => $city, 'exemption' => 1]));
        $arr = [
            "Ativos" => $activesAffiliates,
            "Isentos" => $exemptAffiliates
        ];
        return $response->withJson([
            'status' => 'ok',
            'msg' => $arr
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function importResults(Request $request, Response $response)
    {

        set_time_limit(0);
        ini_set("memory_limit", -1);
        ignore_user_abort(1);

        $files = ['votacao_secao_2022_PA_part_2.csv', 'votacao_secao_2022_PA_part_3.csv'];

        foreach ($files as $file) {
            $handle = fopen($file, "r");
            $row = 0;

            if ($handle !== FALSE) {

//            $city = $this->em->getRepository(City::class)->find(4500);
                while (($data = fgetcsv($handle, 1000000000, ",")) !== FALSE) {
                    $data = array_map("utf8_encode", $data);


                    $row++;
                    if ($row <= 1) continue;
                    $data = array_map("utf8_encode", $data);

                    $resultado = new ResultadoEleicao();
                    $resultado->setDTGERACAO($data[0])
                        ->setHHGERACAO($data[1])
                        ->setANOELEICAO($data[2])
                        ->setCDTIPOELEICAO($data[3])
                        ->setNMTIPOELEICAO($data[4])
                        ->setNRTURNO($data[5])
                        ->setCDELEICAO($data[6])
                        ->setDSELEICAO($data[7])
                        ->setDTELEICAO($data[8])
                        ->setTPABRANGENCIA($data[9])
                        ->setSGUF($data[10])
                        ->setSGUE($data[11])
                        ->setNMUE($data[12])
                        ->setCDMUNICIPIO($data[13])
                        ->setNMMUNICIPIO($data[14])
                        ->setNRZONA($data[15])
                        ->setNRSECAO($data[16])
                        ->setCDCARGO($data[17])
                        ->setDSCARGO($data[18])
                        ->setNRVOTAVEL($data[19])
                        ->setNMVOTAVEL($data[20])
                        ->setQTVOTOS($data[21])
                        ->setNRLOCALVOTACAO($data[22])
                        ->setSQCANDIDATO($data[23])
                        ->setNMLOCALVOTACAO($data[24])
                        ->setDSLOCALVOTACAOENDERECO($data[25]);
                    $this->em->persist($resultado);

                    if ($row % 1000 == 0) $this->em->flush();
                }
                $this->em->flush();
            }

        }
        echo "teste {$row}";
    }

    public function getCities(Request $request, Response $response)
    {
        try {
            $uf = $request->getAttribute('route')->getArgument('uf');
            $results = $this->em->getRepository(ResultadoEleicao::class)->getCities($uf);
            return $response->withJson([
                'status' => 'ok',
                'message' => $results,
            ], 201)->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getLatLong(Request $request, Response $response)
    {
        try {

            set_time_limit(0);
            ini_set("memory_limit", -1);
            ignore_user_abort(1);
            $results = $this->em->getRepository(ResultadoEleicao::class)->resultsByAddress();

            foreach ($results as $result) {
                $client = new Client([
                    'verify' => false,
                    'timeout' => 30 * 60,
                ]);

                $address = "{$result['endereco']},{$result['municipio']},{$result['uf']}";
                $address = utf8_decode($address);

                $responseClient = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
                    'query' => [
                        'address' => $address,
                        'key' => 'AIzaSyDi7z4VSBnkGMIAcHE3mwLutJoaCBBnivQ'
                    ]
                ]);

                if ($responseClient->getStatusCode() === 200) {
                    $body = $responseClient->getBody();
                    $data = json_decode($body, true);

                    if (isset($data['results'][0])) {
                        $location = $data['results'][0]['geometry']['location'];

                        $this->em->getRepository(ResultadoEleicao::class)->setLatLong($result, $location);
                    }
                }

            }

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cadastros realizados com sucesso!',
            ])->withStatus(200);

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function areaByDirectory(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getAttribute('route')->getArgument('id');
        $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $name]);
        $arr = [];
        if ($directory) {
            $arr = $this->em->getRepository(Area::class)->areaByDirectory($directory);
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function budgetByCostCenter(Request $request, Response $response)
    {
        $this->getLogged(true);
        $result = $this->em->getRepository(Budget::class)->budgetByCostCenter($request->getQueryParams());
        return $response->withJson([
            'status' => 'ok',
            'message' => $result,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function listPresenceConventions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_CONVENCOES_EXPORTACAO, true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $cpf = $request->getQueryParam('cpf');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $situation = $request->getQueryParam('situation');
        $type = $request->getQueryParam('type');
        $affiliateds = $this->em->getRepository(User::class)->listConventions($user, $name, $cpf, $state, $city, $situation, $type);
        return $this->renderer->render($response, 'search/listConventions.phtml', ['affiliateds' => $affiliateds]);
    }

    public function elected(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $cpf = $request->getQueryParam('cpf');
        $post = $request->getQueryParam('post');
        $year = $request->getQueryParam('year');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $elected = $this->em->getRepository(Elected::class)->list($name, $email, $cpf, $post, $year, $state, $city, $limit, $index * $limit);
        $total = $this->em->getRepository(Elected::class)->listTotal($name, $email, $cpf, $post, $year, $state, $city)['total'];
        $partial = ($index * $limit) + sizeof($elected);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $elected,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function electedExport(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $name = $request->getQueryParam('name');
        $email = $request->getQueryParam('email');
        $cpf = $request->getQueryParam('cpf');
        $post = $request->getQueryParam('post');
        $year = $request->getQueryParam('year');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $elected = $this->em->getRepository(Elected::class)->list($name, $email, $cpf, $post, $year, $state, $city);
        $filename = "Relatório de eleitos " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Nome', 'E-mail', 'CPF', 'Ano eleito', 'Estado', 'Cidade', 'Cargo', 'Votos'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($elected as $e) {
            fputcsv($out, array_values($e), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function sectors(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $sectors = $this->em->getRepository(Sectors::class)->list($name, $limit, $index * $limit);
        $total = $this->em->getRepository(Sectors::class)->listTotal($name)['total'];
        $partial = ($index * $limit) + sizeof($sectors);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $sectors,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function features(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $features = $this->em->getRepository(SystemFeatures::class)->list($name, $limit, $index * $limit);
        $total = $this->em->getRepository(SystemFeatures::class)->listTotal($name);
        $partial = ($index * $limit) + sizeof($features);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $features,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function usersType(Request $request, Response $response)
    {
        $this->getLogged(true);
        $name = $request->getQueryParam('name');
        $level = $request->getQueryParam('level');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $usersType = $this->em->getRepository(UsersTypes::class)->list($name, $level, $limit, $index * $limit);
        $total = $this->em->getRepository(UsersTypes::class)->listTotal($name, $level);
        $partial = ($index * $limit) + sizeof($usersType);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $usersType,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function permissions(Request $request, Response $response)
    {
        $this->getLogged(true);
        $userType = $request->getQueryParam('userType');
        $sector = $request->getQueryParam('sector');
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $permissions = $this->em->getRepository(PermissionUserTypeFuncionality::class)->list($userType, $sector, $limit, $index * $limit);
        $total = $this->em->getRepository(PermissionUserTypeFuncionality::class)->listTotal($userType, $sector);
        $partial = ($index * $limit) + sizeof($permissions);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $permissions,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportCollapse2025(Request $request, Response $response)
    {
        $this->getLogged(true);
        $registers = $this->em->getRepository(User::class)->listCollapse2025();
        $filename = "Relatório Colapso 2025 - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Id', 'Filiado ID', 'Filiado', 'Nome', 'E-mail', 'Telefone', 'Registro', 'Situação'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($registers as $register) {
            fputcsv($out, array_values($register), ';', '"');
        }
        fclose($out);
        exit;
    }
}