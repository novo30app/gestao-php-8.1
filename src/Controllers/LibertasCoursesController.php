<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Commons\Entities\LibertasCourse;
use App\Models\Entities\SystemFeatures;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class LibertasCoursesController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_CURSOS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-courses/index.phtml',
            'user' => $user, 'subMenu' => 'education', 'subSectionActive' => 'libertas', 'subSection' => 'libertasCourses']);
    }


    public function list(Request $request, Response $response)
    {
        $index = $request->getQueryParam('index');
        $filter = $request->getQueryParams();
        $limit = $request->getQueryParam('limit');
        $docs = $this->em->getRepository(LibertasCourse::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(LibertasCourse::class)->listTotal($filter);
        $partial = ($index * $limit) + sizeof($docs);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $docs,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function register(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_CURSOS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('id');
        $course = $this->em->getRepository(LibertasCourse::class)->find($id ?? 0);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-courses/register.phtml', 
            'user' => $user, 'course' => $course, 'subMenu' => 'education', 'subSectionActive' => 'libertas', 
            'subSection' => 'libertasCourses'
        ]);
    }


    public function save(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            $fields = ['title' => 'Assunto', 'img' => 'Link da Imagem', 'idMoodle' => 'ID Moodle', 'description' => 'Descrição', 'plots' => 'Parcelas', 'value' => 'Valor',];
            Validator::requireValidator($fields, $data);
            $course = new LibertasCourse();
            if ($data['id']) {
                $course = $this->em->getRepository(LibertasCourse::class)->find($data['id']);
            }
            $course->setDescription($data['description'])
                ->setImg($data['img'])
                ->setIdMoodle($data['idMoodle'])
                ->setPlots($data['plots'])
                ->setPrice($data['value'])
                ->setActive($data['active'])
                ->setUser($user)
                ->setTitle($data['title']);
            $this->em->getRepository(LibertasCourse::class)->flush($course);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Cadastrado realizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

}