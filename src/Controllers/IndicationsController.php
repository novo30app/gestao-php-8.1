<?php

namespace App\Controllers;

use App\Helpers\Email;
use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Helpers\Messages;
use App\Models\Entities\Answer;
use App\Models\Entities\ExatoDigital;
use App\Models\Entities\QuestionnaireAnswer;
use App\Models\Entities\SystemFeatures;
use App\Services\ExatoDigital as ExatoDigitalService;
use App\Services\Email as EmailService;
use App\Models\Entities\Questionnaire;
use App\Models\Entities\QuestionnaireReply;
use App\Models\Entities\User;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\IndicatedDirectories;
use App\Models\Entities\IndicatedDirectoriesMembers;
use App\Models\Entities\IndicatedDirectoriesMessages;
use App\Models\Entities\SignatureDoc;
use App\Models\Entities\Directory;
use App\Models\Entities\SignatureDocSigned;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class IndicationsController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'indications/index.phtml', 'section' => 'leadersIndications',
            'user' => $user, 'subMenu' => 'directories']);
    }

    public function indicatesDirectory(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_CRIACAO_EDICAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'indications/indicatesDirectory.phtml',
            'section' => 'leadersIndications', 'user' => $user, 'states' => $states, 'subMenu' => 'directories']);
    }

    public function listMembers(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'indications/listMembers.phtml',
            'section' => 'leadersIndications', 'user' => $user, 'subMenu' => 'directories']);
    }

    public function getIndication(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_VISUALIZACAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        if (!$id) throw new \Exception("Solicitação Inválida!");
        $states = $this->em->getRepository(State::class)->findAll();
        $indication = $this->em->getRepository(IndicatedDirectories::class)->find($id);
        $disabledIndication = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['indication' => $indication, 'active' => 0]);
        $page = $indication->getType() == 1 ? 'indications/indicatesCore.phtml' : 'indications/indicatesDirectory.phtml';
        return $this->renderer->render($response, 'default.phtml', ['page' => $page, 'section' => 'leadersIndications',
            'user' => $user, 'title' => 'Indicação De Diretórios', 'states' => $states, 'indication' => $indication,
            'subMenu' => 'directories', 'disabledIndication' => $disabledIndication]);
    }

    public function getMembers(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $indication = $this->em->getRepository(IndicatedDirectories::class)->find($id);
            $arrIndication = [];
            $arrMembers = [];
            if ($indication) {
                $arrIndication = [
                    'level' => $indication->getLevel(),
                    'state' => $indication->getState()->getId(),
                ];
                if ($indication->getLevel() == 1) $arrIndication += ['city' => $indication->getCity()->getId()];
                $members = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['indication' => $indication, 'active' => 1]);

                foreach ($members as $member) {

                    $validSignature = true;
                    $signatureDocSigned = $this->em->getRepository(SignatureDocSigned::class)->findOneBy(['signatureDoc' => 78, 'email' => $member->getEmail()]);
                    if ($signatureDocSigned && $signatureDocSigned->getSigned() == 0) $validSignature = false;

                    $answers = $this->em->getRepository(QuestionnaireAnswer::class)->findBy(['questionnaireReply' => $member->getQuestionnaire()]);
                    $points = $this->em->getRepository(QuestionnaireAnswer::class)->getPointsByQuestionnaire($member->getQuestionnaire()->getId());
                    $arrAnswer = [];
                    foreach ($answers as $item) {
                        $arrAnswer[] = [
                            'question' => $item->getQuestionOption()->getQuestion()->getText(),
                            'answer' => $item->getQuestionOption()->getNameHtmlResult(),
                        ];
                    }

                    $arrMembers[] = [
                        0 => $member->getId(),
                        1 => $member->getCpf(),
                        2 => $member->getName(),
                        3 => $member->getRg(),
                        4 => $member->getVoterTitle(),
                        5 => $member->getVoterTitleZone(),
                        6 => $member->getVoterTitleSection(),
                        7 => $member->getZipCode(),
                        8 => $member->getStreet(),
                        9 => $member->getDistrict(),
                        10 => $member->getNumber(),
                        11 => $member->getComplement(),
                        12 => $member->getStatus(),
                        13 => $member->getProofOfResidence(),
                        14 => $member->getCnh(),
                        15 => $member->getRgFile(),
                        16 => $member->getCpfFile(),
                        17 => $member->getQuestionnaire()->isDoneString(),
                        18 => $member->getQuestionnaire()->getLink(),
                        19 => $member->getEmail(),
                        20 => $member->getOffice(),
                        21 => $member->getOfficeString(),
                        22 => $user->getLevel(),
                        23 => $member->getQuestion1(),
                        24 => $member->getQuestion2(),
                        25 => $member->getQuestion3(),
                        26 => $arrAnswer,
                        27 => BASEURL . "indicacoes/responder/{$member->getId()}/{$member->getHash()}",
                        28 => $points['maxPoints'] > 0 ? round($points['points'] * 100 / $points['maxPoints'], 2) . '%' : '',
                        29 => $member->getExatoDigitalHtmlString(),
                        30 => $member->getApprovalDN(),
                        31 => $member->getApprovalDE(),
                        32 => $member->getPhone(),
                        33 => $indication->getStatus(),
                        34 => $validSignature,
                        35 => $member->getMandateBegin() ? $member->getMandateBegin()->format('Y-m-d') : '',
                        36 => $member->getMandateEnd() ? $member->getMandateEnd()->format('Y-m-d') : '',
                        37 => $member->getStatusString()
                    ];
                }
            }
            return $response->withJson([
                'status' => 'ok',
                'indication' => $arrIndication,
                'members' => $arrMembers,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getManagerData(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $cpf = $request->getAttribute('route')->getArgument('cpf');
            if (!$cpf) throw new \Exception("Solicitação Inválida!");
            $cpf = Utils::formatCpf($cpf);
            $m = $this->em->getRepository(User::class)->getManagerData($cpf);
            $manager = [
                'cpf' => Utils::onlyNumbers($m['cpf']),
                'name' => $m['name'],
                'email' => $m['email'],
                'phone' => $m['phone'],
                'rg' => $m['rg'],
                'title' => $m['title'],
                'zone' => $m['zone'],
                'section' => $m['section'],
                'zipCode' => $m['zipcode'],
                'address' => $m['address'],
                'number' => $m['number'],
                'district' => $m['district'],
                'complement' => $m['complement']
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $manager,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function removeIndication(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $indication = $this->em->getRepository(IndicatedDirectories::class)->find($id);
            $indication->setActive(false);
            $this->em->getRepository(IndicatedDirectories::class)->save($indication);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Removido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function removeMember(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->find($id);
            $member->setActive(false);
            $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($member);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Removido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function moveAttachmentsDirectory(array $files, array $data, IndicatedDirectories $indication): IndicatedDirectories
    {
        $folder = UPLOAD_FOLDER . 'indicacoes/diretorios/';
        $iptu = $files['iptu'];
        $leaseAgreement = $files['leaseAgreement'];
        $newMembersAta = $files['newMembersAta'];
        if ($iptu && $iptu->getClientFilename()) {
            $time = time();
            $extension = explode('.', $iptu->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}iptu.{$extension}";
            $iptu->moveTo($target);
            $indication->setIptu($target);
        }
        if ($leaseAgreement && $leaseAgreement->getClientFilename()) {
            $time = time();
            $extension = explode('.', $leaseAgreement->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}leaseAgreement.{$extension}";
            $leaseAgreement->moveTo($target);
            $indication->setLeaseAgreement($target);
        }
        if ($newMembersAta && $newMembersAta->getClientFilename()) {
            $field = ["newMembersAtaDate" => "Data de lançamento da Ata"];
            Validator::requireValidator($field, $data);
            if($indication->getStatus() != 5) {
                throw new \Exception("Ata de indicação de novos membros só pode ser anexada para indicações em análise DE!");
            }
            $time = time();
            $extension = explode('.', $newMembersAta->getClientFilename());
            $extension = end($extension);
            if($extension != 'pdf') {
                throw new \Exception("Ata de indicação de novos membros deve ser um arquivo PDF!");
            }
            $target = "{$folder}{$time}newMembersAta.{$extension}";
            $newMembersAta->moveTo($target);
            $indication->setNewMembersAta($target)
                ->setNewMembersAtaDate(new \DateTime($data['newMembersAtaDate']))
                ->setUserAnalysisNewMembersAta(null)
                ->setNewMembersAtaStatus(0)
                ->setRejectReason("");
        }
        return $indication;
    }

    private function moveAttachments($files, IndicatedDirectoriesMembers $member, array $memberData): IndicatedDirectoriesMembers
    {
        $folder = UPLOAD_FOLDER . 'indicacoes/membros/';
        $proofOfResidence = $memberData['proofOfResidence'];
        $cnh = $memberData['cnh'];
        $rgFile = $memberData['rgFile'];
        $cpfFile = $memberData['cpfFile'];
        if ($proofOfResidence && $proofOfResidence->getClientFilename()) {
            $time = time();
            $extension = explode('.', $proofOfResidence->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}{$memberData['cpf']}proofOfResidence.{$extension}";
            $proofOfResidence->moveTo($target);
            $member->setProofOfResidence($target);
        }
        if ($cnh && $cnh->getClientFilename()) {
            $time = time();
            $extension = explode('.', $cnh->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}{$memberData['cpf']}cnh.{$extension}";
            $cnh->moveTo($target);
            $member->setCnh($target);
        }
        if ($rgFile && $rgFile->getClientFilename()) {
            $time = time();
            $extension = explode('.', $rgFile->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}{$memberData['cpf']}rgFile.{$extension}";
            $rgFile->moveTo($target);
            $member->setRgFile($target);
        }
        if ($cpfFile && $cpfFile->getClientFilename()) {
            $time = time();
            $extension = explode('.', $cpfFile->getClientFilename());
            $extension = end($extension);
            $target = "{$folder}{$time}{$memberData['cpf']}cpfFile.{$extension}";
            $cpfFile->moveTo($target);
            $member->setCpfFile($target);
        }
        return $member;
    }

    private function getMember(array $data, array $files): array
    {
        $array = [];
        // Filtra apenas os índices que contêm officeId
        $officeKeys = preg_grep('/^officeId-\d+$/', array_keys($data));
        foreach ($officeKeys as $officeKey) {
            // Pega o número do índice (ex: 'officeId-1' => '1')
            $i = str_replace('officeId-', '', $officeKey);
            $office = [
                'id' => $data['memberId-'.$i] ?: null,
                'name' => $data['name-'.$i],
                'email' => $data['email-'.$i],
                'phone' => $data['phone-'.$i],
                'cpf' => $data['cpf-'.$i],
                'rg' => $data['rg-'.$i],
                'voterTitle' => $data['voterTitle-'.$i],
                'voterTitleZone' => $data['voterTitleZone-'.$i],
                'voterTitleSection' => $data['voterTitleSection-'.$i],
                'officeId' => $data['officeId-'.$i],
                'status' => $data['status-'.$i],
                'zipCode' => $data['zipCode-'.$i],
                'street' => $data['street-'.$i],
                'number' => $data['number-'.$i],
                'district' => $data['district-'.$i],
                'complement' => $data['complement-'.$i],
                'mandateBegin' => $data['mandateBegin-'.$i],
                'mandateEnd' => $data['mandateEnd-'.$i],
                'proofOfResidence' => $files['proofOfResidence-'.$i],
                'cnh' => $files['cnh-'.$i],
                'rgFile' => $files['rgFile-'.$i],
                'cpfFile' => $files['cpfFile-'.$i]

            ];
            array_push($array, $office); 
        }
        return $array;
    }

    private function checkAffiliated($cpf)
    {
        $register = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($cpf)]);
        if (!$register || !in_array($register->getFiliado(), [7, 8, 2, 14])) throw new \Exception("Um ou mais indicados não é filiado, verifique e tente novamente!");
    }

    public function saveIndication(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            $files = $request->getUploadedFiles();

            Validator::requireValidator([
                'state' => 'Estado',
                'status' => 'Status',
            ], $data);

            $checkExistingIndication = $this->em->getRepository(IndicatedDirectories::class)->findOneBy(['active' => 1, 'status' => 1, 'city' => $data['city']]);
            if ($data['type'] != 1 && !$data['indicationId'] && $checkExistingIndication) throw new \Exception("Já existe uma indicação aberta para esta cidade!");

            if ($data['type'] == 1) {
                $checkExistingCore = $this->em->getRepository(IndicatedDirectories::class)->findOneBy(['active' => 1, 'type' => 1, 'city' => $data['city']]);
                if (!$data['indicationId'] && $checkExistingCore) throw new \Exception("Já existe um núcleo aberto para esta cidade!");
            }

            $indication = new IndicatedDirectories();
            $indication->setUser($user);
            if ($data['indicationId']) {
                $indication = $this->em->getRepository(IndicatedDirectories::class)->findOneBy(['id' => $data['indicationId']]);
                if ((int)$data['status'] != $indication->getStatus()) $indication->setRefreshStatus(new \Datetime());
            }

            $indication->setType($data['type'])
                ->setStatus($data['status'])
                ->setState($this->em->getReference(State::class, $data['state']))
                ->setActive(true)
                ->setZipCode($data['zipCode'])
                ->setStreet($data['street'])
                ->setNumber($data['number'])
                ->setDistrict($data['district'])
                ->setComplement($data['complement']);

            if ($data['type'] != 1) {
                $indication->setTotalArea($data['totalArea'])
                    ->setTotalAreaUsed($data['totalAreaUsed']);
                $indication = $this->moveAttachmentsDirectory($files, $data, $indication);
            }

            if (!$data['stateCheckbox']) {
                $indication->setCity($this->em->getReference(City::class, $data['city']))
                    ->setLevel(1);
            } else {
                $indication->setLevel(2);
            }
            $this->em->getRepository(IndicatedDirectories::class)->save($indication);
            
            //Envia email para o departamento jurídico para avisar que uma nova ata de indicação de novos membros foi anexada
            $newMembersAta = $data['newMembersAta'];
            if($newMembersAta && $newMembersAta->getClientFilename()) {
                $msg = Messages::alertNewFileIndication($indication);
                $copyTo = ['daniel.santana@novo.org.br', 'ana.rodrigues@novo.org.br'];
                EmailService::send('legal@novo.org.br', 
                'Departamento Juridico', 
                'Nova Ata de Indicação de Novos Membros Anexada', 
                 $msg, null, null, null, $copyTo);
            }

            $membersIndication = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['indication' => $indication]);
            if ($membersIndication) {
                foreach ($membersIndication as $m) {
                    $m = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findOneBy(['id' => $m->getId()]);
                    $m->setActive(false);
                    $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($m);
                }
            }

            $membersData = $this->getMember($data, $files);
            foreach ($membersData as $memberData) {
                if($memberData['type'] != 1) {
                    $office = match ($memberData['officeId']) {
                        '1' => 'Presidente',
                        '2' => 'Vice Presidente',
                        '3' => 'Secretário Administrativo',
                        '4' => 'Secretário de Finanças',
                        '5' => 'Secretário de Relações Institucionais e Legal',
                        default => ''
                    };
                } else {
                    foreach($data['office'] as $office) {
                        $office = 'Líder ' . $office;
                    }
                }

                $fields = ['officeId' => 'Cargo do indicado a ' . $office,
                    'status' => 'Status do indicado a ' . $office,
                    'cpf' => 'CPF do indicado a ' . $office,
                    'name' => 'Nome completo do indicado a ' . $office,
                    'email' => 'E-mail do indicado a ' . $office,
                    'phone' => 'Telefone do indicado a ' . $office,
                ];

                if ($data['type'] != 1) {
                    $fields2 = [
                        'rg' => 'RG do indicado a ' . $office,
                        'voterTitle' => 'Titulo Eleitoral do indicado a ' . $office,
                        'voterTitleZone' => 'Zona do indicado a ' . $office,
                        'voterTitleSection' => 'Seção do indicado a ' . $office,
                        'zipCode' => 'CEP do indicado a ' . $office,
                        'street' => 'Logradouro do indicado a ' . $office,
                        'district' => 'Bairro do indicado a ' . $office,
                        //'mandateBegin' => 'Inicio do mandado do indicado a ' . $office,
                        //'mandateEnd' => 'Fim do mandato do indicado a ' . $office
                    ];
                    $fields = array_merge($fields, $fields2);
                    if ($memberData['number'] == "") $fields = array_merge($fields, ['number' => 'Número do endereço do indicado a ' . $office]);
                }
                Validator::requireValidator($fields, $memberData);

                $sendMail = false;
                if ($memberData['id']) {
                    $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findOneBy(['id' => $memberData['id']]);
                } else {
                    $sendMail = true;
                    $questionnaire = $this->em->getRepository(QuestionnaireReply::class)
                        ->findOneBy(['email' => trim($memberData['email']), 'questionnaire' => 1]);
                    if (!$questionnaire) {
                        $questionnaire = new QuestionnaireReply();
                        $questionnaire->setName($memberData['name'])
                            ->setEmail($memberData['email'])
                            ->setQuestionnaire($this->em->getReference(Questionnaire::class, 1));
                        $questionnaire = $this->em->getRepository(QuestionnaireReply::class)->save($questionnaire);
                    }
                    $member = new IndicatedDirectoriesMembers();
                    $member->setQuestionnaire($questionnaire);
                }

                if ($data['type'] != 1) $member = $this->moveAttachments($files, $member, $memberData);
                $member->setUser($user)
                    ->setIndication($indication)
                    ->setName($memberData['name'])
                    ->setEmail($memberData['email'])
                    ->setPhone($memberData['phone'])
                    ->setCPF($memberData['cpf'])
                    ->setOffice($memberData['officeId'])
                    ->setStatus($memberData['status'])
                    ->setActive(true);
                if ($data['type'] != 1) {
                    $member->setRG($memberData['rg'])
                        ->setVoterTitle($memberData['voterTitle'])
                        ->setVoterTitleZone($memberData['voterTitleZone'])
                        ->setVoterTitleSection($memberData['voterTitleSection'])
                        ->setState($this->em->getReference(State::class, $data['state']))
                        ->setZipCode($memberData['zipCode'])
                        ->setStreet($memberData['street'])
                        ->setNumber($memberData['number'])
                        ->setDistrict($memberData['district'])
                        ->setComplement($memberData['complement']);

                        if($memberData['mandateBegin'] != "") {
                            $member->setMandateBegin(new \Datetime($memberData['mandateBegin']));
                        }
                        if($memberData['mandateEnd'] != "") {
                            $member->setMandateEnd(new \Datetime($memberData['mandateEnd']));
                        }
                }

                if (!$data['stateCheckbox']) $member->setCity($this->em->getReference(City::class, $data['city']));

                if ($memberData['status'] == 6) {
                    $member->setApprovalDE(0)
                        ->setApprovalDN(0);
                } else if ($memberData['status'] == 7) {
                    $member->setApprovalDE(1)
                        ->setApprovalDN(0);
                } else if ($memberData['status'] == 3) {
                    //$member->setApprovalDE(1)
                    //    ->setApprovalDN(1);

                    // Verifica se o membro está afiliado
                    $this->checkAffiliated($member->getCPF());
                    //if($member->getApprovalDE() == 0) throw new \Exception("Membro não foi aprovado na análise DE");
                    //if($member->getApprovalDN() == 0) throw new \Exception("Membro não foi aprovado na análise DN");

                    // Verifica se o membro assinou o termo de compromisso
                    /*
                    $termId = 78;
                    $signatureDocSigned = $this->em->getRepository(SignatureDocSigned::class)
                        ->findOneBy([
                            'signatureDoc' => $termId, 
                            'email' => $member->getEmail()
                        ]);

                    if (!$signatureDocSigned) {
                        throw new \Exception("Termo de compromisso não encontrado para o membro");
                    }

                    if(!$signatureDocSigned->getSigned()) {
                        throw new \Exception("Membro não assinou o termo de compromisso");
                    }

                    // Verifica se o membro realizou o questionário 1
                    if(!$member->getQuestionnaire()->isDone()) {
                        throw new \Exception("Membro não realizou o questionário");
                    }*/
                }

                $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($member);
                if ($sendMail) {
                    if ($data['stateCheckbox']) {
                        Email::finishState($member, $data['type']);
                    } else {
                        Email::finish($member, $data['type']);
                    }
                }

                // envio de termo
                $doc = $this->em->getRepository(SignatureDoc::class)->find(78);
                $text = str_replace("{NOME COMPLETO}", $memberData['name'], $doc->getText());
                $docSigned = new SignatureDocSigned();
                $docSigned->setText($text)
                    ->setName($memberData['name'])
                    ->setEmail($memberData['email'])
                    ->setSignatureDoc($doc);
                $docSigned = $this->em->getRepository(SignatureDocSigned::class)->save($docSigned);

                $msgMail = "<p>Olá {$memberData['name']},</p>
                    <p>Um documento foi gerado para você assinar.</p>
                    <p>Para visualizar o mesmo <a href='{$this->baseUrl}assinatura-eletronica/documentos/assinar/{$docSigned->getId()}/{$docSigned->getHash()}' target='_blank'>clique aqui.</a></p>
                    <p>Atenção: este é um e-mail automático; por favor não responda.</p>";
                EmailService::send($memberData['email'], $memberData['name'], 'Assinatura de documento - Partido NOVO', $msgMail);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Salvo com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function resendEmail(Request $request, Response $response)
    {
        try {
            $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->find($id);
            if ($member->getStatus() == 5) throw new \Exception("Não é possível reenviar o questionário para essa pessoa.");
            $indication = $this->em->getRepository(IndicatedDirectories::class)->findOneBy(['id' => $member->getIndication()]);
            if ($indication->getCity()) {
                Email::finish($member, $indication->getType());
            } else {
                Email::finishState($member, $indication->getType());
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => "E-mail reenviado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function resendEmailOfAcceptance(Request $request, Response $response)
    {
        try {
            $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->find($id);
            if ($member->getStatus() == 5) throw new \Exception("Não é possível reenviar o termo para essa pessoa.");
            $doc = $this->em->getRepository(SignatureDoc::class)->find(78);
            $docSigned = $this->em->getRepository(SignatureDocSigned::class)
                ->findOneBy(['signatureDoc' => $doc->getId(), 'email' => $member->getEmail()]);
            if ($docSigned->getSigned() == 1) throw new \Exception("Já assinado");
            if (!$docSigned) {
                $docSigned = new SignatureDocSigned();
                $text = str_replace("{NOME COMPLETO}", $member->getName(), $doc->getText());
                $docSigned->setText($text)
                    ->setName($member->getName())
                    ->setEmail($member->getEmail())
                    ->setSignatureDoc($doc);
                $docSigned = $this->em->getRepository(SignatureDocSigned::class)->save($docSigned);
            }
            $msgMail = "<p>Olá {$member->getName()},</p>
                <p>Um documento foi gerado para você assinar.</p>
                <p>Para visualizar o mesmo <a href='{$this->baseUrl}assinatura-eletronica/documentos/assinar/{$docSigned->getId()}/{$docSigned->getHash()}' target='_blank'>clique aqui.</a></p>
                <p>Atenção: este é um e-mail automático; por favor não responda.</p>";
            EmailService::send($member->getEmail(), $member->getName(), 'Assinatura de documento - Partido NOVO', $msgMail);

            return $response->withJson([
                'status' => 'ok',
                'message' => "E-mail reenviado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveMessages(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            $indication = $this->em->getRepository(IndicatedDirectories::class)->find($data['indication']);
            if (!$indication) throw new \Exception("Solicitação inválida");
            $message = new IndicatedDirectoriesMessages();
            $message->setUser($user)
                ->setIndication($indication)
                ->setMessage(ltrim($data['description']))
                ->setVisibled(1);
            $this->em->getRepository(IndicatedDirectoriesMessages::class)->save($message);
            //$msg = Messages::newMessageIndication($indication);
            //Email::send($indication->getUser()->getEmail(), $indication->getUser()->getName(), 'Nova mensagem na Indicação de Diretórios', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Mensagem salva com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function indicationsList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $type = $request->getQueryParam('type');
        $order = $request->getQueryParam('order');
        $seq = $request->getQueryParam('seq');
        $indications = $this->em->getRepository(IndicatedDirectories::class)->list($user, $state, $city, $type, $status, $order, $seq, 20, $index * 20);
        $total = $this->em->getRepository(IndicatedDirectories::class)->listTotal($user, $state, $city, $type, $status, $order, $seq)['total'];
        $partial = ($index * 20) + sizeof($indications);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $indications,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function indicationsMembersList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $office = $request->getQueryParam('office');
        $members = $this->em->getRepository(IndicatedDirectoriesMembers::class)->list($user, $state, $city, $status, $office, 20, $index * 20);
        $total = $this->em->getRepository(IndicatedDirectoriesMembers::class)->listTotal($user, $state, $city, $status, $office)['total'];
        $partial = ($index * 20) + sizeof($members);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $members,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exatoDigital()
    {
        $searchUrl = 'br/exato/background-check/pessoa-fisica/';
        $members = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['exatoDigital' => null]);
        foreach ($members as $member) {
            $exato = $this->em->getRepository(ExatoDigital::class)->findOneBy(['doc' => $member->getCPF()]);
            if ($exato) {
                $member->setExatoDigital($exato);
                $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($member);
                continue;
            }
            $search = ExatoDigitalService::startAsync($member->getCPF(), $searchUrl);
            $exato = new ExatoDigital();
            $exato->setDoc($member->getCPF())
                ->setUid($search['UniqueIdentifier'] ?? '')
                ->setStatus($search['TransactionResultType'])
                ->setMessage($search['Message'])
                ->setSearchDescription('Exato - Relatório de Pessoa Física')
                ->setSearchUrl($searchUrl);
            $exato = $this->em->getRepository(ExatoDigital::class)->save($exato);
            if (!$search['UniqueIdentifier']) continue;
            $member->setExatoDigital($exato);
            $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($member);
        }
        $exatoDigital = $this->em->getRepository(ExatoDigital::class)->findBy(['searchUrl' => $searchUrl, 'status' => 'AsyncExecutionInProgress'], [], 50);
        foreach ($exatoDigital as $exato) {
            $search = ExatoDigitalService::getAsyncResult($exato->getUid(), $searchUrl);
            $search2 = ExatoDigitalService::getAsyncResultKeys($exato->getUid(), $searchUrl);
            if (!$search2) continue;
            $exato->setPdfUrl($search['PdfUrl'] ?? '')
                ->setStatus($search['TransactionResultType'])
                ->setMessage($search['Message'])
                ->setJsonValues(json_encode($search2));
            $this->em->getRepository(ExatoDigital::class)->save($exato);
        }
    }

    public function respond(Request $request, Response $response)
    {
        $id = $request->getAttribute('id');
        $hash = $request->getAttribute('hash');
        $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findOneBy(['id' => $id, 'hash' => $hash]);
        if (!$member) die('Link inválido');
        return $this->renderer->render($response, 'public/default.phtml', ['page' => 'questionnaireIndicatedDirectoriesMembers.phtml',
            'member' => $member,]);
    }

    public function respondSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findOneBy(['id' => $data['id'], 'hash' => $data['hash']]);
            if (!$member) throw new \Exception('Requisiço inválida!');
            Validator::requireValidator([
                'question1' => 'contexto político local',
                'question2' => 'principais desafios que o NOVO enfrentará',
                'question3' => 'como você já tem atuado para melhorar a sua comunidade',
            ], $data);
            $membersCpf = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['cpf' => $member->getCPF()]);
            foreach ($membersCpf as $member) {
                $member->setQuestion1($data['question1'])
                    ->setQuestion2($data['question2'])
                    ->setQuestion3($data['question3']);
                $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($member);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Questionário realizado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function approval(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $level = $request->getAttribute('level');
            if (empty($level) || !in_array($level, ['2', '3'])) {
                throw new \InvalidArgumentException("Nível de aprovação inválido");
            }

            $value = (int)$request->getAttribute('value');
            if ($value !== 0 && $value !== 1) {
                throw new \InvalidArgumentException("Valor de aprovação inválido");
            }

            $member = $request->getAttribute('member');
            $member = $this->em->getRepository(IndicatedDirectoriesMembers::class)->find($member);
            if (!$member) throw new \InvalidArgumentException("Membro não encontrado");

            match ($level) {
                '2' => $member->setApprovalDE($value)->setStatus(7),
                '3' => $member->setApprovalDN($value)
            };

            if ($member->getApprovalDN() == 1 && $member->getApprovalDE() == 1) {
                $member->setStatus(3);
            }

            // Verifica se o membro está afiliado
            $this->checkAffiliated($member->getCPF());

            if ($user->getType() != 3) {

                // Verifica se o membro assinou o termo de compromisso
                $termId = 78;
                $signatureDocSigned = $this->em->getRepository(SignatureDocSigned::class)
                    ->findOneBy([
                        'signatureDoc' => $termId,
                        'email' => $member->getEmail()
                    ]);

                if (!$signatureDocSigned) {
                    throw new \Exception("Termo de compromisso não encontrado para o membro");
                }

                if (!$signatureDocSigned->getSigned()) {
                    throw new \Exception("Membro não assinou o termo de compromisso");
                }

                // Verifica se o membro realizou o questionário 1
                if (!$member->getQuestionnaire()->isDone()) {
                    throw new \Exception("Membro não realizou o questionário");
                }

            }

            $member->setApprover($user);
            $this->em->getRepository(IndicatedDirectoriesMembers::class)->save($member);

            // Verifica se todos os membros da indicação foram aprovados pelo DE para alterar para DN
            $indication = $member->getIndication();
            $noApprovalDE = $this->em->getRepository(IndicatedDirectoriesMembers::class)
                ->count([
                    'indication' => $indication->getId(),
                    'approvalDE' => 0
                ]);
            if ($noApprovalDE == 0) {
                $indication->setStatus(6);
                $this->em->getRepository(IndicatedDirectories::class)->save($indication);
            }

            // Verifica se todos os membros da indicação foram aprovados pelo DN para alterar para Aprovado
            $noApprovalDN = $this->em->getRepository(IndicatedDirectoriesMembers::class)
                ->count([
                    'indication' => $indication->getId(),
                    'approvalDN' => 0
                ]);
            if ($noApprovalDN == 0) {
                $indication->setStatus(3);
                $this->em->getRepository(IndicatedDirectories::class)->save($indication);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Salvo com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function indicationsListExport(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_EXPORTACAO, true);
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $order = $request->getQueryParam('order');
        $seq = $request->getQueryParam('seq');
        $indications = $this->em->getRepository(IndicatedDirectories::class)->list($user, $state, $city, $type, $status, $order, $seq);
        $filename = "Indicação De Diretórios" . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Estado', 'Cidade', 'Solicitante', 'Data Solicitação', 'Status', 'Atualização Status	'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($indications as $i) {
            fputcsv($out, array_values($i), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function indicationsMembersListExport(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $office = $request->getQueryParam('office');
        $members = $this->em->getRepository(IndicatedDirectoriesMembers::class)->list($user, $state, $city, $status, $office);
        $filename = "Listagem De Membros" . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Estado', 'Cidade', 'Solicitante', 'CPF', 'Ativo', 'Indicação ID', 'Status', 'Questão 1', 'Questão 2', 'Questão 3', 'Questionário', 'Aprovado DN', 'Aprovado DE',
            'Cargo', 'Status'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($members as $m) {
            fputcsv($out, array_values($m), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function removeDocument(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $type = $request->getAttribute('route')->getArgument('type');
            $indication = $this->em->getRepository(IndicatedDirectories::class)->find($id);
            match ((int)$type) {
                1 => $indication->setIptu(""),
                2 => $indication->setLeaseAgreement(""),
                3 => $indication->setNewMembersAta("")
                    ->setNewMembersAtaDate(null)
                    ->setUserAnalysisNewMembersAta(null)
                    ->setNewMembersAtaStatus(0)
                    ->setRejectReason('')
            };
            $this->em->getRepository(IndicatedDirectories::class)->save($indication);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Removido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeIndication(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $status = $request->getAttribute('route')->getArgument('status');
            $status = $status == 0 ? 4 : 3;
            $indication = $this->em->getRepository(IndicatedDirectories::class)->find($id);
            $indication->setStatus($status);
            $this->em->getRepository(IndicatedDirectories::class)->save($indication);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Salvo com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function indicatesCore(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_CRIACAO_EDICAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'indications/indicatesCore.phtml',
            'section' => 'leadersIndications', 'user' => $user, 'states' => $states, 'subMenu' => 'directories']);
    }

    /**
     * Exporta os membros desativados em um arquivo CSV
     * @param Request $request
     * @param Response $response
     * @return void
     */
    public function exportMembersDisabled(Request $request, Response $response)
    {
        $indication = $request->getQueryParam('indication');

        // Constante para path base
        $basePath = "/var/www/html/";

        // Função helper para limpar paths
        $cleanPath = function($path) use ($basePath) {
            return str_replace($basePath, "", $path);
        };

        // Identifica os membros desativados
        $members = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['indication' => $indication, 'active' => false]);
        $array = [];

        // Preenche o array com os membros desativados
        foreach ($members as $member) {
            $array[] = [
                'createdAt' => $member->getCreatedAt()->format('d/m/Y H:i:s'),
                'indication' => $member->getIndication()->getId(),
                'mandateBegin' => $member->getMandateBegin() ? $member->getMandateBegin()->format('d/m/Y H:i:s') : '',
                'mandateEnd' => $member->getMandateEnd() ? $member->getMandateEnd()->format('d/m/Y H:i:s') : '',
                'name' => $member->getName(),
                'cpf' => $member->getCPF(),
                'rg' => $member->getRg(),
                'voterTitle' => $member->getVoterTitle(),
                'voterTitleZone' => $member->getVoterTitleZone(),
                'voterTitleSection' => $member->getVoterTitleSection(),
                'office' => $member->getOfficeString(),
                'status' => $member->getStatusString(),
                'uf' => $member->getState()?->getEstado() ?? '',
                'city' => $member->getCity()?->getCidade() ?? '',
                'zipCode' => $member->getZipCode(),
                'street' => $member->getStreet(),
                'number' => $member->getNumber(),
                'district' => $member->getDistrict(),
                'complement' => $member->getComplement(),
                'proofOfResidence' => $cleanPath($member->getProofOfResidence()),
                'cnh' => $cleanPath($member->getCnh()),
                'rgFile' => $cleanPath($member->getRgFile()),
                'cpfFile' => $cleanPath($member->getCpfFile()),
                'email' => $member->getEmail(),
                'phone' => $member->getPhone(),
                'approvalDN' => $member->getApprovalDN() ? 'Sim' : 'Não',
                'approvalDE' => $member->getApprovalDE() ? 'Sim' : 'Não'
            ];
        };

        // Gera o arquivo CSV
        $filename = "Membros desativados - Indicação:" . $indication . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Data de criação', 'Indicação', 'Mandato Início', 'Mandato Fim', 'Nome', 'CPF', 'RG', 'Título de eleitor', 
            'Zona', 'Seção', 'Cargo', 'Status', 'UF', 'Cidade', 'CEP', 'Rua', 'Número', 'Bairro', 'Complemento', 'Comprovante de Residência', 
            'CNH', 'RGFILE', 'CPFFILE', 'E-mail', 'Telefone', 'Aprovado DN', 'Aprovado DE'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($array as $member) {
            fputcsv($out, array_values($member), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function analysisAtaIndicationNewMembers(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $value = $request->getAttribute('route')->getArgument('value');
        $rejectReason = $request->getAttribute('route')->getArgument('rejectReason');
        $indication = $this->em->getRepository(IndicatedDirectories::class)->find($id);
        $indication->setNewMembersAtaStatus($value)
            ->setUserAnalysisNewMembersAta($user);
        if($indication->getNewMembersAtaStatus() == 2) {
            $indication->setRejectReason($rejectReason);
            $directory = match($indication->getLevel()) {
                1 => $this->em->getRepository(Directory::class)->findOneBy(['tbCidadeId' => $indication->getCity()->getId()]),
                2 => $this->em->getRepository(Directory::class)->findOneBy(['tbEstadoId' => $indication->getState()->getId()]),
                3 => $this->em->getRepository(Directory::class)->find(1)
            };
            $copyTo = $directory ? $directory->getEmail() : '';
            $msg = Messages::alertRejectIndication($indication);
            //$copyTo = [$copyTo];
            $copyTo = ['ti@novo.org.br'];
            EmailService::send($indication->getUser()->getEmail(), 
                'Partido NOVO', 
                'Ata de indicação de novos membros rejeitada', 
                $msg, null, null, null, $copyTo);
        }
        $this->em->getRepository(IndicatedDirectories::class)->save($indication);
        return $response->withJson([
            'status' => 'ok',
            'message' => "Salvo com sucesso!",
        ], 201)
            ->withHeader('Content-type', 'application/json');
    }
}