<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\User;
use App\Models\Entities\LgpdNews;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class LgpdController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::ORIENTACOES_LGPDNEWS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'lgpd/news.phtml', 'section' => 'news',
            'user' => $user, 'subMenu' => 'orientations', 'title' => 'LGPD NEWS']);
    }

    public function getNews(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $title = $request->getQueryParam('title');
        $news = $this->em->getRepository(LgpdNews::class)->list($user, $title, 20, $index * 20);
        $total = $this->em->getRepository(LgpdNews::class)->listTotal($user, $title)['total'];
        $partial = ($index * 20) + sizeof($news);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $news,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function refreshStatus(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $member = $this->em->getRepository(LgpdNews::class)->findOneBy(['id' => $id]);
            $status = $member->getStatus() == 1 ? 0 : 1;
            $member->setStatus($status);
            $this->em->getRepository(LgpdNews::class)->save($member);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registro atualizado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function register(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = ['titleModal' => 'Título'];
            Validator::requireValidator($fields, $data);
            $file = $request->getUploadedFiles()['fileModal'];
            if (!$file->getClientFilename()) {
                throw new \Exception('Atenção, é obrigatório o anexo de um Arquivo!');
            } else {
                $extension = explode('.', $file->getClientFilename());
                if ($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB');
                $target = time() . '.' . end($extension);
                $file->moveTo("uploads/lgpd/" . $target);
            }
            $news = new LgpdNews();
            $news->setCreated_at(new \Datetime())
                ->setTitle($data['titleModal'])
                ->setFile($target)
                ->setStatus(1);
            $this->em->getRepository(LgpdNews::class)->save($news);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registro salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}