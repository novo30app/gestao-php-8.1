<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Commons\Entities\LibertasMoodleRedirect;
use App\Models\Entities\Elected;
use App\Models\Entities\LibertasAutoEnrollment;
use App\Models\Entities\SystemFeatures;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class LibertasAutoEnrollmentController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_AUTOINSCRICAOMOODLE_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-auto-enrollment/index.phtml',
            'user' => $user, 'subMenu' => 'education', 'subSectionActive' => 'libertas', 'subSection' => 'autoEnrollment']);
    }

    public function list(Request $request, Response $response)
    {
        $id = $request->getAttribute('route')->getArgument('id');
        $index = $request->getQueryParam('index') ?? 0;
        $limit = $request->getQueryParam('limit') ?? 1;
        $filter = $request->getQueryParams();
        if ($id) $filter['id'] = $id;
        $docs = $this->em->getRepository(LibertasAutoEnrollment::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(LibertasAutoEnrollment::class)->listTotal($filter);
        $partial = ($index * $limit) + sizeof($docs);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $docs,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = ['email' => 'E-mail'];
            Validator::requireValidator($fields, $data);
            $libertasAutoEnrollment = new LibertasAutoEnrollment();
            if ($data['autoEnrollmentId']) {
                $libertasAutoEnrollment = $this->em->getRepository(LibertasAutoEnrollment::class)->find($data['autoEnrollmentId']);
            }
            $libertasAutoEnrollment->setEmail($data['email'])
                ->setAutoEnrollment((bool)$data['auto_enrollment'])
                ->setMandatarioExecutivo((bool)$data['mandatario_executivo'])
                ->setMandatarioLegislativo((bool)$data['mandatario_legislativo']);
            $this->em->getRepository(LibertasAutoEnrollment::class)->save($libertasAutoEnrollment);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Cadastrado realizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $libertasAutoEnrollment = $this->em->getRepository(LibertasAutoEnrollment::class)->find($id);
            $libertasAutoEnrollment->changeActive();
            $this->em->getRepository(LibertasAutoEnrollment::class)->save($libertasAutoEnrollment);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Status alterado com sucesso',
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function isAutoEnrollment(Request $request, Response $response)
    {
        $email = $request->getAttribute('route')->getArgument('email');
        $autoEnrollment = $this->em->getRepository(LibertasAutoEnrollment::class)->findOneBy(['email' => trim($email), 'active' => 1, 'auto_enrollment' => 1]);
        return $response->withJson([
            'status' => (bool)$autoEnrollment,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function isMandatarioLegislativo(Request $request, Response $response)
    {
        $email = $request->getAttribute('route')->getArgument('email');
        $cpf = $request->getAttribute('route')->getArgument('cpf');
        $cpf = Utils::onlyNumbers($cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        $autoEnrollment = $this->em->getRepository(LibertasAutoEnrollment::class)->findOneBy(['email' => trim($email), 'active' => 1, 'mandatario_legislativo' => 1]);
        $elected = $this->em->getRepository(Elected::class)->findOneBy(['cpf' => $cpf, 'status' => 1], ['year' => 'DESC']);
        return $response->withJson([
            'status' => ($autoEnrollment || $elected && $elected->getPost()->getType() == 'legislativo'),
        ])->withHeader('Content-Type', 'application/json');
    }

    public function isMandatarioExecutivo(Request $request, Response $response)
    {
        $email = $request->getAttribute('route')->getArgument('email');
        $cpf = $request->getAttribute('route')->getArgument('cpf');
        $cpf = Utils::onlyNumbers($cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        $autoEnrollment = $this->em->getRepository(LibertasAutoEnrollment::class)->findOneBy(['email' => trim($email), 'active' => 1, 'mandatario_executivo' => 1]);
        $elected = $this->em->getRepository(Elected::class)->findOneBy(['cpf' => $cpf, 'status' => 1], ['year' => 'DESC']);
        return $response->withJson([
            'status' => ($autoEnrollment || $elected && $elected->getPost()->getType() == 'executivo'),
        ])->withHeader('Content-Type', 'application/json');
    }

    public function csvImport(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->getLogged(true);
            $file = $request->getUploadedFiles();
            $file = $file['file'];

            if ($file && $file->getClientFilename()) {
                $folder = UPLOAD_FOLDER;
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                if ($extension != 'csv') throw new \Exception("Formato inválido, verifique se a planilha está em .CSV");
                $target = "{$folder}{$time}.{$extension}";
                $file->moveTo($target);
            }
            if (($handle = fopen($target, "r")) !== FALSE) {
                $separator = ',';
                $data = fgetcsv($handle, 100, ",");
                if (count($data) == 1) $separator = ';';
                while (($data = fgetcsv($handle, 100, $separator)) !== FALSE) {
                    $email = trim($data[0]);
                    $status = trim($data[1]) == 'Ativo' ? 1 : 0;
                    $auto = trim($data[2]) == 'Sim' ? 1 : 0;
                    $legislativeAgent = trim($data[3]) == 'Sim' ? 1 : 0;
                    $executiveAgent = trim($data[4]) == 'Sim' ? 1 : 0;
                    $libertasAutoEnrollment = $this->em->getRepository(LibertasAutoEnrollment::class)->findOneBy(['email' => $email]);
                    if (!$libertasAutoEnrollment) $libertasAutoEnrollment = new LibertasAutoEnrollment();
                    $libertasAutoEnrollment->setEmail($data[0])
                        ->setActive($status)
                        ->setAutoEnrollment($auto)
                        ->setMandatarioLegislativo($legislativeAgent)
                        ->setMandatarioExecutivo($executiveAgent);
                    $this->em->getRepository(LibertasAutoEnrollment::class)->save($libertasAutoEnrollment);
                }
                fclose($handle);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Planilha importada com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function moodleRedirect(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_MOODLEREDIRECTS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-auto-enrollment/moodleRedirect.phtml',
            'subMenu' => 'education', 'subSectionActive' => 'libertas', 'subSection' => 'moodleRedirect',
            'user' => $user,]);
    }

    public function moodleRedirectList(Request $request, Response $response)
    {
        $id = $request->getAttribute('route')->getArgument('id');
        $index = $request->getQueryParam('index') ?? 0;
        $limit = $request->getQueryParam('limit') ?? 1;
        $filter = $request->getQueryParams();
        if ($id) $filter['id'] = $id;
        $list = $this->em->getRepository(LibertasMoodleRedirect::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(LibertasMoodleRedirect::class)->listTotal($filter)['total'];
        $partial = ($index * $limit) + sizeof($list);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $list,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function saveMoodleRedirectList(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->getLogged(true);
            $data = (array)$request->getParams();
            $fields = [
                'shortLink' => 'Link Curto',
                'moodleLink' => 'Link Moodle',
                'status' => 'Status'
            ];
            Validator::requireValidator($fields, $data);
            $moodleRedirect = new LibertasMoodleRedirect();
            if ($data['moodleRedirectId']) {
                $moodleRedirect = $this->em->getRepository(LibertasMoodleRedirect::class)->find($data['moodleRedirectId']);
            }
            $moodleRedirect->setShortLink($data['shortLink'])
                ->setMoodleLink($data['moodleLink'])
                ->setStatus((bool)$data['status']);
            $this->em->getRepository(LibertasMoodleRedirect::class)->save($moodleRedirect);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Salvo com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}