<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Session;
use App\Helpers\Validator;
use App\Models\Entities\Address;
use App\Models\Entities\Appointment;
use App\Models\Entities\BlackList;
use App\Models\Entities\Candidate;
use App\Models\Entities\City;
use App\Models\Entities\CivilState;
use App\Models\Entities\Country;
use App\Models\Entities\Events;
use App\Models\Entities\EventsRegistrations;
use App\Models\Entities\Gender;
use App\Models\Entities\Impeachment;
use App\Models\Entities\Leaders;
use App\Models\Entities\Phone;
use App\Models\Entities\AffiliatedStatus;
use App\Models\Entities\Quiz1;
use App\Models\Entities\Schooling;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Knowledge;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\PersonSignatureInstallments;
use App\Models\Entities\ReasonDisaffection;
use App\Models\Entities\ProcessCep;
use App\Models\Entities\Comments;
use App\Models\Entities\CommentStatus;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\Historic;
use App\Models\Entities\Mesoregions;
use App\Models\Entities\IndicationOfAffiliation;
use App\Models\Entities\ResearchNovoInformative;
use App\Models\Entities\IndicatedDirectoriesMembers;
use App\Models\Entities\CogmoSchedule;
use App\Models\Entities\CogmoPhones;
use App\Services\AuthEspacoNovo;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use App\Models\Entities\State;
use App\Services\Email;

class AffiliatedController extends Controller
{

    public function quiz(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/quiz.phtml',
            'section' => 'quiz', 'subMenu' => 'affiliated', 'user' => $user,
            'states' => $states
        ]);
    }

    public function showQuiz(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $quiz = $this->em->getRepository(Quiz1::class)->find($id);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/quizView.phtml',
            'section' => 'quiz', 'subMenu' => 'affiliated', 'user' => $user, 'quiz' => $quiz
        ]);
    }


    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_GESTAODECADASTROS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $status = $this->em->getRepository(AffiliatedStatus::class)->findBy([], ['status' => 'asc']);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - affiliated']);
        $this->newAccessLog($user, 'affiliated');
        $mesoregions = $this->em->getRepository(Mesoregions::class)->getMyMesos($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/index.phtml', 'cities' => $cities, 'term' => $term,
            'section' => 'managementRecords', 'subMenu' => 'search', 'user' => $user, 'states' => $states, 'status' => $status, 'mesoregions' => $mesoregions]);
    }

    public function quizCsv(Request $request, Response $response)
    {
        $this->getLogged();
        $answers = $this->em->getRepository(Quiz1::class)->listSolicitations($request->getQueryParams(), PHP_INT_MAX, 0);
        $filename = "Questionario-" . time() . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Data', 'Nome',
            '1 - De modo geral, a população entende melhor os discursos com conteúdo mais emocional do que racional.',
            '2 - As atividades relacionadas ao trabalho dos dirigentes do Novo (em nível municipal, estadual e nacional) são plenamente compreendidas pelos filiados do partido.',
            '3 - O modelo de gestão do Novo, em que os cargos de direção são exercidos por voluntários, tem funcionado bem ao longo do tempo.',
            '4 - Diretório Nacional',
            '4 - Diretório Estadual',
            '4 - Diretório Municipal',
            '4 - CEP',
            '4 - DAM',
            '4 - DAC',
            '5 - As divergências e debates entre filiados, dirigentes e mandatários do Novo ocorrem de forma produtiva e respeitosa.',
            '6 - Pode-se dizer que os filiados do Novo têm acesso pleno às informações relevantes do partido através dos meios de comunicação existentes, a saber, e-mails, redes sociais e o conteúdo disponível no Espaço Novo.',
            '7 - O propósito do Novo, sua razão de existir, pode ser sintetizada como a de melhorar a vida dos brasileiros de forma sustentável por meio de uma transformação política baseada nos princípios da liberdade.',
            '8 - O Novo pode fazer alianças ou coligações com outros partidos, sem, necessariamente, abrir mão de seus valores e seus princípios.',
            '9 - O Novo só poderá crescer se contar com uma estrutura profissional condizente com suas aspirações como, por exemplo, dirigentes partidários remunerados.',
            '10 - Pode-se afirmar que o Novo tem como objetivo no longo prazo ser reconhecido como um partido político capaz de realizar mudanças profundas na política por meio de lideranças inspiradoras, capazes de promover a liberdade e o protagonismo do cidadão.',
            '11 - A utilização do fundo partidário para manutenção do partido deve permanecer vetada, não devendo ser pauta de discussão em nenhuma hipótese.',
            '12 - De modo geral, os filiados do Novo têm a oportunidade de serem ouvidos por seus respectivos Diretórios Municipais e Estaduais pelos meios disponíveis, como e-mail, grupos de WhatsApp, telefone e reuniões presenciais ou remotas.',
            '13 - A seleção e a formação de candidatos têm se mostrado adequada e necessária para que o Novo garanta a qualidade de seus candidatos e futuros mandatários.',
            '14 - Executivo',
            '14 - Legislativo',
            '15 - Gostaria de deixar alguma crítica ou sugestão?',
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, $header);
        foreach ($answers as $quiz) {
            $quiz = $this->em->getRepository(Quiz1::class)->find($quiz['id']);
            fputcsv($out, [
                $quiz->getId(), $quiz->getCreated()->format('d/m/Y H:i:s'), $quiz->getUser()->getName(),
                $quiz->getQuestion1(), $quiz->getQuestion2(), $quiz->getQuestion3(), $quiz->getQuestion4a(),
                $quiz->getQuestion4b(), $quiz->getQuestion4c(), $quiz->getQuestion4d(), $quiz->getQuestion4e(),
                $quiz->getQuestion4f(), $quiz->getQuestion5(), $quiz->getQuestion6(), $quiz->getQuestion7(),
                $quiz->getQuestion8(), $quiz->getQuestion9(), $quiz->getQuestion10(), $quiz->getQuestion11(),
                $quiz->getQuestion12(), $quiz->getQuestion13(), $quiz->getQuestion14a(),
                $quiz->getQuestion14b(), $quiz->getObs(),
            ]);
        }
        fclose($out);
        exit;
    }

    public function tseFile(Request $request, Response $response)
    {
        $this->getLogged();
        $affiliateds = $this->em->getRepository(User::class)->tseFile();
        $filename = "TSE-" . date('Y-m-d') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Data', 'Nome', 'Titulo'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ',', '"');
        foreach ($affiliateds as $affiliated) {
            fputcsv($out, array_values($affiliated), ',', '"');
        }
        fclose($out);
        exit;
    }

    public function submission(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        $status = $this->em->getRepository(AffiliatedStatus::class)->findBy([], ['status' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/submission.phtml',
            'section' => 'submission', 'subMenu' => 'affiliated', 'title' => 'Submissão TSE', 'user' => $user, 'states' => $states, 'status' => $status]);
    }

    public function csv(Request $request, Response $response)
    {
        $this->getLogged();
        $affiliation = $request->getAttribute('route')->getArgument('affiliation');
        $affiliation = $affiliation != "null" ? date('Y-m-d', strtotime(str_replace('/', '-', $affiliation))) : "";
        $affiliation = new \DateTime($affiliation);
        $disaffiliation = $request->getAttribute('route')->getArgument('disaffiliation');
        $disaffiliation = $disaffiliation != "null" ? date('Y-m-d', strtotime(str_replace('/', '-', $disaffiliation))) : "";
        $disaffiliation = new \DateTime($disaffiliation);
        $affiliateds = $this->em->getRepository(User::class)->submission($affiliation, $disaffiliation);
        $filename = "TSE-" . date('Y-m-d') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['Id', 'Data Filiação', 'Data Desfiliação', 'Nome', 'Titulo', 'Seção', 'Zona'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ',', '"');
        foreach ($affiliateds as $affiliated) {
            fputcsv($out, array_values($affiliated), ',', '"');
        }
        fclose($out);
        exit;
    }

    public function view(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_GESTAODECADASTROS_VISUALIZACAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $affiliated = $this->em->getRepository(User::class)->find($id);
        $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $affiliated->getId(), 'tbTipoTelefoneId' => 1]);
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $affiliated->getId(), 'tbTipoTelefoneId' => 3]);
        $genders = $this->em->getRepository(Gender::class)->findAll();
        $education = $this->em->getRepository(Schooling::class)->findAll();
        $processCep = $this->em->getRepository(ProcessCep::class)->findBy(['personId' => $affiliated->getId()]);
        $civilStatus = $this->em->getRepository(CivilState::class)->findAll();
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        $voterTitleCities = $this->em->getRepository(City::class)->findBy(['state' => $affiliated->getTituloEleitoralUfId()]);
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $affiliated->getId()]);
        $addressCities = $address ? $this->em->getRepository(City::class)->findBy(['state' => $address->getStateId()]) : null;
        $userAdmin = $this->em->getRepository(UserAdmin::class)->findAll();
        $extract = $this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $affiliated], ['dataCriacao' => 'DESC']);
        $extractOpened = $this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $affiliated, 'status' => ['pending', 'expired'], 'periodicidade' => [1, 6, 12]], ['dataCriacao' => 'DESC']);
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $affiliated, 'gatewayPagamento' => 1], ['dataCriacao' => 'DESC']);
        $signatureAffiliation = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $affiliated, 'origin' => 2]);
        $signature = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $affiliated], ['dataCriacao' => 'DESC']);

        // installments
        foreach ($signature as $si) {
            $installments = $this->em->getRepository(PersonSignatureInstallments::class)->findBy(['signature' => $si->getId()]);
            foreach ($installments as $i) {
                $installmentsArray[] = $i->getDueDate()->format('d/m/Y');
            }
        }

        $comments = $this->em->getRepository(Comments::class)->findBy(['tbPessoaId' => $affiliated->getId()], ['dataCriacao' => 'DESC']);
        $events = $this->em->getRepository(EventsRegistrations::class)->findBy(['tbPessoaId' => $affiliated->getId()], ['id' => 'DESC']);
        $reasons = $this->em->getRepository(ReasonDisaffection::class)->findAll();
        $blackList = $this->em->getRepository(BlackList::class)->findOneBy(['cpf' => $affiliated->getCpf(true)]);
        $leaders = $this->em->getRepository(IndicatedDirectoriesMembers::class)->findBy(['cpf' => $affiliated->getCpf(true)]);
        $reclassification = $this->em->getRepository(ReasonDisaffection::class)->findOneBy(['id' => $affiliated->getReclassificacaoDesfiliacao()]);
        $reasonDisaffection = null;
        if ($affiliated->getmotivoDesfiliacao()) {
            if (strpos($affiliated->getmotivoDesfiliacao(), '[') === true || strpos($affiliated->getmotivoDesfiliacao(), '{') === true) {
                $reasonDisaffection = null;
            } else {
                $reasonDisaffection = explode(",", $affiliated->getmotivoDesfiliacao());
                foreach ($reasonDisaffection as $k => $v) {
                    $reasonDisaffection[$k] = $this->em->getRepository(ReasonDisaffection::class)->findOneBy(['id' => $v]);
                }
            }
        }
        $statusComments = $this->em->getRepository(CommentStatus::class)->findBy(['visible' => 1], ['status' => 'ASC']);
        $historic = $this->em->getRepository(Historic::class)->findBy(['user' => $affiliated->getId()], ['date' => 'desc']);
        $selective = $this->em->getRepository(Candidate::class)->findBy(['email' => $affiliated->getEmail()], ['year' => 'ASC']);
        $researchNovo = $this->em->getRepository(ResearchNovoInformative::class)->findOneBy(['user' => $affiliated]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/view.phtml', 'section' => 'managementRecords', 'subMenu' => 'search', 'signatureAffiliation' => $signatureAffiliation,
            'user' => $user, 'title' => $affiliated->getName(), 'phone' => $phone, 'cellPhone' => $cellPhone, 'genders' => $genders, 'civilStatus' => $civilStatus, 'historic' => $historic, 'selective' => $selective,
            'education' => $education, 'states' => $states, 'voterTitleCities' => $voterTitleCities, 'address' => $address, 'addressCities' => $addressCities, 'events' => $events, 'extractOpened' => $extractOpened,
            'countries' => $countries, 'processCep' => $processCep, 'statusComments' => $statusComments, 'reclassification' => $reclassification, 'reasonDisaffection' => $reasonDisaffection, 'reasons' => $reasons,
            'cards' => $cards, 'signature' => $signature, 'comments' => $comments, 'affiliated' => $affiliated, 'extract' => $extract, 'userAdmin' => $userAdmin, 'blackList' => $blackList, 'leaders' => $leaders,
            'researchNovo' => $researchNovo, 'arrSignature' => $installmentsArray]);
    }

    public function addBlackList(Request $request, Response $response)
    {
        try {
            $logged = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $user = $this->em->getRepository(User::class)->find($id ?? 0);
            if (!$user) throw new Exception("Usuário não encontrado");
            $blackList = new BlackList();
            $blackList->setAffiliated($user)
                ->setCpf($user->getCpf())
                ->setUser($logged);
            $this->em->getRepository(BlackList::class)->save($blackList);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Filiado adicionado a Lista Negra',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }


    public function edit(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_GESTAODECADASTROS_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $affiliated = $this->em->getRepository(User::class)->find($id);
        $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $affiliated->getId(), 'tbTipoTelefoneId' => 1]);
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $affiliated->getId(), 'tbTipoTelefoneId' => 3]);
        $genders = $this->em->getRepository(Gender::class)->findAll();
        $education = $this->em->getRepository(Schooling::class)->findAll();
        $civilStatus = $this->em->getRepository(CivilState::class)->findAll();
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        $voterTitleCities = $this->em->getRepository(City::class)->findBy(['state' => $affiliated->getTituloEleitoralUfId()]);
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $affiliated->getId()]);
        $addressCities = $address ? $this->em->getRepository(City::class)->findBy(['state' => $address->getStateId()]) : null;
        $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['user' => $affiliated->getId()]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/edit.phtml', 'section' => 'managementRecords', 'subMenu' => 'search',
            'user' => $user, 'title' => "Filiado {$affiliated->getName()}", 'phone' => $phone, 'cellPhone' => $cellPhone, 'genders' => $genders, 'civilStatus' => $civilStatus,
            'education' => $education, 'states' => $states, 'voterTitleCities' => $voterTitleCities, 'address' => $address, 'addressCities' => $addressCities,
            'countries' => $countries, 'affiliated' => $affiliated, 'indication' => $indication]);
    }

    public function savePersonalData(request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->populateUser($data);
            if ($data['zipCode'] != '') $this->populateAddress($data);
            $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
            $cellPhone->setPais($data['cellPhoneIso2'])->setDdi($data['cellPhoneDialCode'])->setTelefone($data['cellPhone']);
            $this->em->getRepository(Phone::class)->save($cellPhone);
            $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 1]);
            if ($phone) {
                $phone->setPais($data['cellPhoneIso2'])->setDdi($data['phoneDialCode'])->setTelefone($data['phone']);
            } else {
                $phone = new Phone();
                $phone->setTbPessoaId($user->getId())->setPais($data['cellPhoneIso2'])->setDdi($data['phoneDialCode'])->setTelefone($data['phone'])->setTbTipoTelefoneId(1);
            }
            $this->em->getRepository(Phone::class)->save($phone);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Dados alterados com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function populateAddress(array $data): Address
    {
        $user = $this->em->getRepository(User::class)->find($data['id']);
        $data['addressOutside'] ??= 0;
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]) ?: new Address();
        $uf = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data['ufId']]); // vem como string
        $uf = $uf ? $uf->getId() : 1; // para funcionar o select integrado com o cep
        $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => $data['cityId'], 'state' => $uf]);
        $city = $city ? $city->getId() : 0;
        $address->setTbPessoaId($user->getId())
            ->setCountryId($data['addressCountry']) //Brasil
            ->setResideExterior($data['addressOutside'])
            ->setStateId($uf)
            ->setEstado('')
            ->setCityId($city)
            ->setCidade('')
            ->setCep($data['zipCode'])
            ->setNumero($data['number'])
            ->setEndereco($data['street'])
            ->setComplemento($data['complement'])
            ->setBairro($data['district']);
        if ($data['addressOutside']) {
            $address->setCityId(0)
                ->setCountryId($data['addressCountry'])
                ->setStateId(1)
                ->setEstado($data['uf'])
                ->setCidade($data['city']);
        }
        return $this->em->getRepository(Address::class)->save($address);
    }

    private function populateUser(array $data): User
    {
        $user = $this->em->getRepository(User::class)->find($data['id']);
        $usersRepository = $this->em->getRepository(User::class);
        $user->setEscolaridade($this->em->getReference(Schooling::class, $data['education']))
            ->setProfissao($data['profession'])
            ->setGenero($this->em->getReference(Gender::class, $data['gender']))
            ->setEstadoCivil($this->em->getReference(CivilState::class, $data['civilStatus']))
            ->setName($data['name'])
            ->setRg($data['rg'])
            ->setOrgaoExpedidor($data['issuingAgency'])
            ->setDataNascimento(\DateTime::createFromFormat('d/m/Y', $data['birth']));
        if (in_array($user->getFiliado(), array(7, 8))) {
            $fields = [
                'originVoterTitle' => 'Origem do Título de eleitor',
                'mother' => 'Nome da mãe',
                'voterTitle' => 'Título de eleitor',
                'voterTitleZone' => 'Zona do Título de eleitor',
                'voterTitleSection' => 'Seção do Título de eleitor'
            ];
            if ($data['originVoterTitle'] == '2') {
                $fields = array_merge($fields, [
                    'voterTitleUF' => 'Estado do Título de eleitor',
                    'voterTitleCity' => 'Cidade do Título de eleitor'
                ]);
            }
            Validator::requireValidator($fields, $data);
        }
        $user->setNomeMae($data['mother'])
            ->setTituloEleitoral($data['voterTitle'])
            ->setTituloEleitoralZona($data['voterTitleZone'])
            ->setTituloEleitoralSecao($data['voterTitleSection'])
            ->setTituloEleitoralMunicipio($data['voterTitleCityString'])
            ->setTituloEleitoralPaisId($this->em->getReference(Country::class, $data['voterTitleCountry']))
            ->setTituloEleitoralMunicipioId(null)
            ->setTituloEleitoralUfId(null);
        if ($data['originVoterTitle'] != 1) {
            $user->setTituloEleitoralPaisId($this->em->getReference(Country::class, 33)) //Brasil
            ->setTituloEleitoralMunicipioId($this->em->getReference(City::class, $data['voterTitleCity']))
                ->setTituloEleitoralUfId($this->em->getReference(State::class, $data['voterTitleUF']));
        }
        return $usersRepository->save($user);
    }

    public function blackList(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $blackList = $this->em->getRepository(BlackList::class)->findBy([], ['created' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/blackList.phtml',
            'section' => 'blacklist', 'subMenu' => 'affiliated', 'user' => $user,
            'blackList' => $blackList
        ]);
    }

    public function impeachment(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_PEDIDOSDEIMPUGNACAO_VISUALIZACAO, true);
        $impeachments = $this->em->getRepository(Impeachment::class)->findBy([], ['created' => 'desc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/impeachments.phtml',
            'section' => 'impeachment', 'subMenu' => 'affiliated', 'user' => $user,
            'impeachments' => $impeachments
        ]);
    }

    public function knowledge(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_COMOCONHECEU_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/knowledge.phtml',
            'section' => 'knowledge', 'subMenu' => 'affiliated', 'user' => $user,
            'states' => $states
        ]);
    }

    public function latestAffiliations(Request $request, Response $response, bool $approved = false)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_ULTIMASFILIACOES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['subMenu' => 'affiliated', 'user' => $user,
            'states' => $states, 'page' => $approved ? 'affiliated/latestAffiliations.phtml' : 'affiliated/solicitations.phtml',
            'section' => 'latestAffiliations', 'subMenu' => 'search'
        ]);
    }

    public function saveImpeachmentData(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            //$accessLog = Session::get(Session::get('accessLog'));
            $user = $this->em->getRepository(User::class)->find($data['id']);
            $user->setFiliado(6)->setStatus(0);
            $this->em->getRepository(User::class)->save($user);
            $target = $this->em->getRepository(User::class)->find($data['id']);
            $user = $this->em->getRepository(User::class)->find($user->getId());
            $impeachment = new Impeachment();
            $impeachment->setMsg($data['reason'])
                ->setUser($user)
                ->setTarget($target);
            //->setAccessLog($this->em->getReference(AccessLog::class, $accessLog))
            $this->em->getRepository(Impeachment::class)->save($impeachment);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Impugnação realizada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function savePassword(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->getLogged();
            if ($data['password'] != $data['password2']) {
                throw new \Exception("As senhas devem ser iguais");
            }

            $changePassword = $this->em->getRepository(User::class)->findOneBy(['id' => $data['id']]);
            $changePassword->setPassword(md5($data['password2']));
            $this->em->getRepository(User::class)->save($changePassword);
            AuthEspacoNovo::charge($changePassword->getName(), $changePassword->getEmail(), $changePassword->getPassword());
            Email::send($changePassword->getEmail(), $changePassword->getName(), 'Alteração de senha Espaço NOVO!', "Olá {$changePassword->getName()},<br> Sua senha provisória é <b> {$data['password2']}</b>, acesse seu Espaço NOVO e modifique!");

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Senha alterada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveEmail(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $checkEmail = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email2']]);
            if ($checkEmail) throw new \Exception("Este e-mail já está cadastrado, verifique primeiro o cadastro existente.");
            $changeEmail = $this->em->getRepository(User::class)->findOneBy(['id' => $data['id']]);
            $changeEmail->setEmail($data['email2']);
            $this->em->getRepository(User::class)->save($changeEmail);
            return $response->withJson([
                'status' => 'ok',
                'message' => "E-mail alterado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function whoIndicated(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_QUEMINDICOU_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/whoIndicated.phtml',
            'section' => 'whoIndicated', 'subMenu' => 'affiliated', 'user' => $user, 'states' => $states]);
    }

    public function reaffiliation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_PEDIDOSDEREFILIACAO_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/reaffiliation.phtml',
            'section' => 'reaffiliation', 'subMenu' => 'affiliated', 'user' => $user, 'states' => $states]);
    }

    public function scheduleMessages(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_AGENDAMENTOS_VISUALIZACAO, true);
        $collectors = $this->em->getRepository(CogmoPhones::class)->findBy(['status' => 'active']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'affiliated/schedules.phtml', 
            'section' => 'schedules', 'subMenu' => 'affiliated', 'user' => $user, 'collectors' => $collectors,
            'subMenu' => 'affiliated',
        ]);
    }

    public function scheduleMessagesRegisters(Request $request, Response $response)
    {
		$this->getLogged(true);
		$id = $request->getAttribute('id');
		$index = $request->getQueryParam('index');
		$name = $request->getQueryParam('name');
		$phone = $request->getQueryParam('phone');
		$collector = $request->getQueryParam('collector');
		$type = $request->getQueryParam('type');
		$status = $request->getQueryParam('status');
		$date = $request->getQueryParam('date');
		$limit = $request->getQueryParam('limit');
		$schedules = $this->em->getRepository(CogmoSchedule::class)->list($id, $name, $phone, $collector, $type, $status, $date, $limit, $index * $limit);
        $total = $this->em->getRepository(CogmoSchedule::class)->listTotal($id, $name, $phone, $collector, $type, $status, $date)['total'];
        $partial = ($index * $limit) + sizeof($schedules);
		$partial = $partial <= $total ? $partial : $total;
		return $response->withJson([
			'status' => 'ok',
			'message' => $schedules,
			'total' => (int)$total,
			'partial' => $partial,
		], 200)
			->withHeader('Content-type', 'application/json');    
    }
}