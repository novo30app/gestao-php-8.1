<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\AccessLog;
use App\Models\Entities\AffiliatedStatus;
use App\Models\Entities\Comments;
use App\Models\Entities\CommentStatus;
use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionType;
use App\Models\Entities\Mesoregions;
use App\Models\Entities\State;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\YouthSearch;
use App\Models\Entities\YouthSearchOption;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class YouthController extends Controller
{


    public function searchIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::JUVENTUDE_CONSULTAS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $status = $this->em->getRepository(AffiliatedStatus::class)->findBy([], ['status' => 'asc']);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - affiliated']);
        $this->newAccessLog($user, 'affiliated');
        $mesoregions = $this->em->getRepository(Mesoregions::class)->getMyMesos($user);
        $statusComments = $this->em->getRepository(CommentStatus::class)->findBy(['visible' => 1], ['status' => 'ASC']);
        $voluntaryInterestOptions = $this->em->getRepository(YouthSearchOption::class)->findBy(['question' => 'voluntaryInterest']);
        $candidateInterestOptions = $this->em->getRepository(YouthSearchOption::class)->findBy(['question' => 'candidateInterest']);
        $studentOptions = $this->em->getRepository(YouthSearchOption::class)->findBy(['question' => 'student']);
        $joinedWhatsAppGroupOptions = $this->em->getRepository(YouthSearchOption::class)->findBy(['question' => 'joinedWhatsAppGroup']);
        $municipalLeaderOptions = $this->em->getRepository(YouthSearchOption::class)->findBy(['question' => 'municipalLeader']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'youth/search.phtml', 'cities' => $cities, 'term' => $term,
            'section' => 'search', 'subMenu' => 'youth', 'user' => $user, 'states' => $states, 'status' => $status,
            'mesoregions' => $mesoregions, 'statusComments' => $statusComments, 'voluntaryInterestOptions' => $voluntaryInterestOptions,
            'candidateInterestOptions' => $candidateInterestOptions, 'studentOptions' => $studentOptions,
            'joinedWhatsAppGroupOptions' => $joinedWhatsAppGroupOptions, 'municipalLeaderOptions' => $municipalLeaderOptions]);
    }

    public function listComments(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $comments = $this->em->getRepository(Comments::class)->findBy(['tbPessoaId' => $id], ['dataCriacao' => 'DESC']);
        $arr = [];

        foreach ($comments as $comment) {
            $arr[] = [
                'date' => $comment->getDataCriacao()->format('d/m/Y'),
                'admin' => $comment->getUserAdmin()->getName(),
                'comment' => $comment->getComentario(),
                'status' => $comment->getStatus()->getStatus(),
            ];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function searchList(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $filter = $request->getQueryParams();
        $filter['ageEnd'] = 29;
        $filter['begin'] = $request->getQueryParam('begin') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('begin')))) : "";
        $filter['end'] = $request->getQueryParam('end') != "" ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('end')))) : "";
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $affiliateds = $this->em->getRepository(YouthSearch::class)->list($user, $filter, $limit, $index * $limit);
        $total = $this->em->getRepository(YouthSearch::class)->listTotal($user, $filter)['total'];
        $partial = ($index * $limit) + sizeof($affiliateds);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $affiliateds,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }


    public function searchView(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $search = $this->em->getRepository(YouthSearch::class)->findOneBy(['user' => $id]);
        $result = [
            'voluntaryInterest' => $search ? $search->getVoluntaryInterest()->getId() : '',
            'student' => $search ? $search->getStudent()->getId() : '',
            'candidateInterest' => $search ? $search->getCandidateInterest()->getId() : '',
            'joinedWhatsAppGroup' => $search ? $search->getJoinedWhatsAppGroup()->getId() : '',
            'municipalLeader' => $search ? $search->getMunicipalLeader()->getId() : '',
        ];
        return $response->withJson([
            'status' => 'ok',
            'message' => $result
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function interactionSave(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'voluntaryInterest' => 'Interesse em ser voluntário',
                'student' => 'Estudante',
                'candidateInterest' => 'Interesse em ser candidato',
                'joinedWhatsAppGroup' => 'Entrou no grupo de WhatsApp',
            ];
            Validator::requireValidator($fields, $data);
            $search = $this->em->getRepository(YouthSearch::class)->findOneBy(['user' => $data['pessoaId']]);
            if (!$search) $search = new YouthSearch();
            $search->setUpdatedAt(new \DateTime())
                ->setUser($this->em->getReference(User::class, $data['pessoaId']))
                ->setVoluntaryInterest($this->em->getReference(YouthSearchOption::class, $data['voluntaryInterest']))
                ->setStudent($this->em->getReference(YouthSearchOption::class, $data['student']))
                ->setMunicipalLeader($this->em->getReference(YouthSearchOption::class, $data['municipalLeader']))
                ->setCandidateInterest($this->em->getReference(YouthSearchOption::class, $data['candidateInterest']))
                ->setJoinedWhatsAppGroup($this->em->getReference(YouthSearchOption::class, $data['joinedWhatsAppGroup']))
                ->setUserAdmin($this->em->getReference(UserAdmin::class, $data['userAdmin']));
            $this->em->getRepository(YouthSearch::class)->save($search);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Cadastro realizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }


}
