<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\Question;
use App\Models\Entities\Questionnaire;
use App\Models\Entities\QuestionnaireAnswer;
use App\Models\Entities\QuestionnaireQuestion;
use App\Models\Entities\QuestionnaireReply;
use App\Models\Entities\QuestionOption;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class QuestionnairesController extends Controller
{
    public function questions(Request $request, Response $response)
    {
//        $pdo = new \PDO('mysql:host=bd8.novo.org.br;dbname=jornada2024', 'novo_master', 'ThhHNXBvv66HDi');
//        $pdo->exec("set names utf8");
//        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
//        $stmt = $pdo->prepare("SELECT * FROM question WHERE status = 1");
//        $stmt->execute();
//        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//        foreach ($result as $item) {
//            $question = new Question();
//            $question->setActive(1)
//                ->setResponsible($this->getLogged())
//                ->setText($item['name']);
//            $question = $this->em->getRepository(Question::class)->save($question);
//            $stmt = $pdo->prepare("SELECT * FROM questionOptions WHERE active = 1 and question = :question");
//            $stmt->execute([':question' => $item['id']]);
//            $result2 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//            foreach ($result2 as $item) {
//                $option = new QuestionOption();
//                $option->setName($item['name'])
//                    ->setPoints($item['points'])
//                    ->setQuestion($question);
//                $this->em->getRepository(QuestionOption::class)->save($option);
//            }
//        }
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $users = $this->em->getRepository(UserAdmin::class)->findBy(['level' => 3, 'active' => 1, 'type' => 3], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'questionnaires/questions.phtml', 'section' => 'questions',
            'user' => $user, 'subMenu' => 'questionnaires', 'title' => 'Questões', 'users' => $users]);
    }

    public function questionView(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) $question = $this->em->getRepository(Question::class)->find($id);
        if ($question) $options = $this->em->getRepository(QuestionOption::class)->findBy(['question' => $question, 'active' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'questionnaires/questionView.phtml', 'subMenu' => 'questionnaires',
            'user' => $user, 'title' => 'Registrar Questão', 'question' => $question, 'options' => $options, 'section' => 'questions']);
    }

    public function questionnaires(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $users = $this->em->getRepository(UserAdmin::class)->findBy(['level' => 3, 'active' => 1, 'type' => 3], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'questionnaires/questionnaires.phtml', 'section' => 'questionnaires',
            'user' => $user, 'subMenu' => 'questionnaires', 'title' => 'Questionários', 'users' => $users]);
    }

    public function questionnaireView(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $arrQuestions = [];
        if ($id) {
            $questionnaire = $this->em->getRepository(Questionnaire::class)->find($id);
            foreach ($questionnaire->getQuestions() as $item) {
                if ($item->isActive()) $arrQuestions[] = $item->getQuestion()->getId();
            }

        }
        $questions = $this->em->getRepository(Question::class)->findBy(['active' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'questionnaires/questionnaireView.phtml',
            'subMenu' => 'questionnaires', 'user' => $user, 'title' => 'Registrar Questão', 'questions' => $questions,
            'section' => 'questionnaires', 'questionnaire' => $questionnaire, 'arrQuestions' => $arrQuestions]);
    }

    public function sendQuestionnaire(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $questonnaire = $this->em->getRepository(Questionnaire::class)->find($id);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'questionnaires/sendQuestionnaire.phtml', 'section' => 'questionnaires',
            'user' => $user, 'subMenu' => 'questionnaires', 'title' => 'Enviar Questionário', 'questonnaire' => $questonnaire]);
    }

    public function sendQuestionnaireSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'Nome',
                'email' => 'E-mail',
            ];
            Validator::requireValidator($fields, $data);
            $send = new QuestionnaireReply();

            $send->setName($data['name'])
                ->setEmail($data['email'])
                ->setQuestionnaire($this->em->getReference(Questionnaire::class, $data['id']));
            $this->em->getRepository(QuestionnaireReply::class)->save($send);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Questionário enviado com sucesso",
                'id' => $send->getId(),
                'hash' => $send->getHash()
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function questionsList(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $questions = $this->em->getRepository(Question::class)->list($request->getQueryParams(), $limit, $index);
        $total = $this->em->getRepository(Question::class)->listTotal($request->getQueryParams());
        $partial = ($index * $limit) + sizeof($questions);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $questions,
            'total' => (int)$total,
            'partial' => $partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function questionnairesList(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAdminNational()) $this->redirectByPermissions();
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $questionnaires = $this->em->getRepository(Questionnaire::class)->list($request->getQueryParams(), $limit, $index);
        $total = $this->em->getRepository(Questionnaire::class)->listTotal($request->getQueryParams());
        $partial = ($index * $limit) + sizeof($questionnaires);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $questionnaires,
            'total' => (int)$total,
            'partial' => $partial
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function questionSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $data['type'] = 1;
            $fields = [
                'active' => 'Status',
                'name' => 'Pergunta',
                'options' => 'Alternativas',
                'points' => 'Valor das alternativas',
            ];
            Validator::requireValidator($fields, $data);
            $question = new Question();
            $msg = 'cadastrada';
            if ($data['questionId']) {
                $question = $this->em->getRepository(Question::class)->find($data['questionId']);
                if (!$question) throw new \Exception('Questão inválida!');
                $msg = 'atualizada';
                $options = $this->em->getRepository(QuestionOption::class)->findBy(['question' => $question, 'active' => 1]);
                foreach ($options as $option) {
                    $option->setActive(0);
                    $this->em->getRepository(QuestionOption::class)->save($option);
                }
            }
            $question->setActive($data['active'])
                ->setResponsible($user)
                ->setText($data['name']);
            $this->em->getRepository(Question::class)->save($question);
            for ($i = 0; $i < count($data['points']); $i++) {
                if ($data['options'][$i] !== '' && $data['points'][$i] !== '') {
                    $option = new QuestionOption();
                    $option->setActive(1)
                        ->setName($data['options'][$i])
                        ->setPoints($data['points'][$i])
                        ->setQuestion($question);
                    $this->em->getRepository(QuestionOption::class)->save($option);
                }
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Questão {$msg} com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function questionnaireSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            if (!$user->isAdminNational()) $this->redirectByPermissions();
            $this->em->beginTransaction();
            $data = (array)$request->getParams();

            $fields = [
                'active' => 'Status',
                'title' => 'Título',
                'start' => 'Data Início',
                'questions' => 'Questões',
            ];
            Validator::requireValidator($fields, $data);
            Validator::validDate($data['start']);
            if ($data['end'] !== '') Validator::validDate($data['end']);

            $questionnaire = new Questionnaire();
            $msg = 'cadastrado';
            if ($data['questionnaireId']) {
                $questionnaire = $this->em->getRepository(Questionnaire::class)->find($data['questionnaireId']);
                if (!$questionnaire) throw new \Exception('Questionário inválido!');
                $msg = 'atualizado';
                $questions = $this->em->getRepository(QuestionnaireQuestion::class)->findBy(['questionnaire' => $questionnaire, 'active' => 1]);
                foreach ($questions as $option) {
                    $option->setActive(0);
                    $this->em->getRepository(QuestionnaireQuestion::class)->save($option);
                }
            }
            $questionnaire->setActive($data['active'])
                ->setResponsible($user)
                ->setStart(\DateTime::createFromFormat('d/m/Y', $data['start']))
                ->setEnd(\DateTime::createFromFormat('d/m/Y', $data['end']) ?: null)
                ->setTitle($data['title']);
            $this->em->getRepository(Questionnaire::class)->save($questionnaire);

            foreach ($data['questions'] as $item) {
                $question = new QuestionnaireQuestion();
                $question->setQuestionnaire($questionnaire)
                    ->setQuestion($this->em->getReference(Question::class, $item))
                    ->setActive(true);
                $this->em->getRepository(QuestionnaireQuestion::class)->save($question);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Questionário {$msg} com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function reply(Request $request, Response $response)
    {
        $type = $request->getAttribute('type');
        $id = $request->getAttribute('id');
        $hash = $request->getAttribute('hash');
        if ($type == 1) {
            $questionnaire = $this->em->getRepository(Questionnaire::class)->findOneBy(['id' => $id, 'hash' => $hash]);
            if (!$questionnaire) die('Link inválido');
            $questionnaireReply = new QuestionnaireReply();
        } else {
            $questionnaireReply = $this->em->getRepository(QuestionnaireReply::class)->findOneBy(['id' => $id, 'hash' => $hash]);
            if (!$questionnaireReply) die('Link inválido');
            $questionnaire = $questionnaireReply->getQuestionnaire();
        }
        return $this->renderer->render($response, 'public/default.phtml', ['page' => 'questionnaire.phtml',
            'questionnaire' => $questionnaire, 'questionnaireReply' => $questionnaireReply, 'type' => $type]);
    }

    public function replySave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            if ($data['type'] == 1) {
                $questionnaire = $this->em->getRepository(Questionnaire::class)->findOneBy(['id' => $data['id'], 'hash' => $data['hash']]);
                if (!$questionnaire) throw new \Exception('Requisiço inválida!');
                $questionnaireReply = new QuestionnaireReply();
                $questionnaireReply->setName($data['name'])
                    ->setEmail($data['email']);
                $questionnaireReply = $this->em->getRepository(QuestionnaireReply::class)->save($questionnaireReply);
            } else {
                $questionnaireReply = $this->em->getRepository(QuestionnaireReply::class)->findOneBy(['id' => $data['id'], 'hash' => $data['hash']]);
                if (!$questionnaireReply) throw new \Exception('Requisiço inválida!');
            }
            $count = 1;
            for ($i = 0; $i < $data['total']; $i++) {
                $answer = new QuestionnaireAnswer();
                $answer->setQuestionnaireReply($questionnaireReply)
                    ->setQuestionOption($this->em->getReference(QuestionOption::class, (int)$data["option[{$i}]"]));
                $this->em->getRepository(QuestionnaireAnswer::class)->save($answer);
                $count++;
            }
            if ($count < $data['total']) throw new \Exception('Responda todas as questões.');
            $questionnaireReply->setDone(1)
                ->setDoneDate(new \DateTime());
            $this->em->getRepository(QuestionnaireReply::class)->save($questionnaireReply);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Questionário realizado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

}