<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Helpers\Messages;
use App\Models\Entities\CivilState;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\User;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\Country;
use App\Models\Entities\Address;
use App\Models\Entities\Phone;
use App\Models\Entities\IndicationOfAffiliation;
use App\Models\Entities\Transaction;

//use App\Models\Entities\IndicationOfAffiliationSms;
use App\Services\Email;
use App\Services\Hubspot;
use App\Services\WhatsService;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class AffiliationController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_INDICACOESDEFILIACAO_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'filiation/index.phtml', 'section' => 'affiliatedIndications',
            'subMenu' => 'affiliated', 'user' => $user, 'states' => $states
        ]);
    }

    public function newIndication(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FILIADOS_INDICACOESDEFILIACAO_CRIACAO_EDICAO, true);
        $cpm = $request->getAttribute('route')->getArgument('cpm');
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'filiation/newIndication.phtml', 'section' => 'affiliatedIndications',
            'subMenu' => 'affiliated', 'user' => $user, 'states' => $states, 'countries' => $countries, 'cpm' => $cpm
        ]);
    }

    public function getData(Request $request, Response $response)
    {
        try {
            //$userAdmin = $this->getLogged(true);
            $cpf = $request->getAttribute('route')->getArgument('cpf');
            if (!$cpf) die;
            $cpf = Utils::formatCpf($cpf);
            $m = $this->em->getRepository(User::class)->getManagerData($cpf);
            $manager = [
                'cpf' => $m['cpf'],
                'name' => $m['name'],
                'birth' => $m['birth'],
                'email' => $m['email'],
                'phone' => $m['phone'],
                'gender' => $m['gender'],
                'zipCode' => $m['zipcode'],
                'state' => $m['state'],
                'city' => $m['city'],
                'address' => $m['address'],
                'number' => $m['number'],
                'district' => $m['district'],
                'complement' => $m['complement'],
                'motherName' => $m['motherName'],
                'title' => $m['title'],
                'zone' => $m['zone'],
                'section' => $m['section'],
                'voterTitleCity' => $m['voterTitleCity'],
                'voterTitleUf' => $m['voterTitleUf']
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $manager,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function checkStatus(User $user): void
    {
        $msg = match ($user->getFiliado()) {
            6, 4 => "Atenção, a solicitação de filiação desta pessoa não pode ser concluída, entre em contato com filiacao@novo.org.br para mais detalhes!",
            7, 8 => "Atenção, os dados enviados já pertencem a um filiado!",
            1, 2, 3, 14 => "Atenção, já existe um pedido de filiação em aberto para este cadastro!",
            default => ''
        };
        if ($msg != "") throw new \Exception($msg);
    }

    private function validaRegisterData(array $data, $user = false)
    {
        $fields = [
            'name' => 'Nome Completo',
            'birth' => 'Data de Nascimento',
            'cellPhone' => 'Telefone Celular',
            'zipCode' => 'CEP',
            'street' => 'Logradouro',
            'number' => 'Número',
            'district' => 'Bairro',
            'originVoterTitle' => 'Origem título de eleitor',
            'mother' => 'Nome da Mãe',
            'voterTitleNumber' => 'Título de eleitor',
            'voterTitleZone' => 'Zona',
            'voterTitleSection' => 'Seção'
        ];
        if (!($data['addressOutside'])) {
            $fields['ufId'] = 'Estado';
            $fields['cityId'] = 'Cidade';
        } else {
            $fields['addressCountry'] = 'País';
            $fields['city'] = 'Cidade';
        }
        if ($data['originVoterTitle'] == 2) { //brasil
            $fields['voterTitleUF'] = 'UF eleitoral';
            $fields['voterTitleCity'] = 'Município Eleitoral';
        } else {
            $fields['voterTitleCountry'] = 'País eleitoral';
            $fields['voterTitleCityString'] = 'Município Eleitoral';
        }
        Validator::requireValidator($fields, $data);
        //if (!$user) Validator::validateEmail($data);
        Validator::validDate($data['birth']);
        Validator::validateCPF($data['cpf'] ?? '');
    }

    private function populateUser(array $data, $user): User
    {
        $new = false;
        if (!$user) {
            $new = true;
            $user = new User();
        }
        if ($new) $user->setCpf(Utils::formatCpf($data['cpf']))->setDataCriacao(new \DateTime())->setEmail($data['email']);
        $user->setName($data['name'])
            ->setDataNascimento(\DateTime::createFromFormat('d/m/Y', $data['birth']))
            ->setNomeMae($data['mother'])
            ->setTituloEleitoral($data['voterTitleNumber'])
            ->setTituloEleitoralZona($data['voterTitleZone'])
            ->setTituloEleitoralSecao($data['voterTitleSection'])
            ->setTituloEleitoralMunicipio($data['voterTitleCityString'])
            ->setTituloEleitoralPaisId($this->em->getReference(Country::class, $data['voterTitleCountry']))
            ->setTermoAceite0(1)
            ->setTermoAceite1(1)
            ->setTermoAceite2(1)
            ->setTermoAceite3(1)
            ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
            ->setGenero($this->em->getReference(Gender::class, $data['sex']))
            ->setEscolaridade($this->em->getReference(Schooling::class, 7))
            ->setOpcaoReligiosa(11)
            ->setIp(Utils::getAcessData()['ip'])
            ->setObservacao('Indicacao de filiacao')
            ->setIrpf($data['exemption'])
            ->setExemption($data['exemption'])
            ->setStatus(0)
            ->setUltimaAtualizacao(new \Datetime())
            ->setObservacao('Indicacao de filiacao');
        match ($user->getFiliado()) {
            9 => $user->setDataSolicitacaoRefiliacao(new \DateTime()),
            default => $user->setDataSolicitacaoFiliacao(new \DateTime())
        };
        if ($data['originVoterTitle'] != 1) {
            $user->setTituloEleitoralPaisId($this->em->getReference(Country::class, 33)) //Brasil
            ->setTituloEleitoralMunicipioId($this->em->getReference(City::class, $data['voterTitleCity']))
                ->setTituloEleitoralUfId($this->em->getReference(State::class, $data['voterTitleUF']));
        }
        $this->em->getRepository(User::class)->save($user);
        return $user;
    }

    protected function populateAddress(array $data, $user): Address
    {
        $data['addressOutside'] ??= 0;
        $address = new Address();
        $uf = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data['ufId']]);
        $uf = $uf ? $uf->getId() : null;
        $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => $data['cityId'], 'state' => $uf]);
        $city = $city ? $city->getId() : null;
        $address->setTbPessoaId($user->getId())
            ->setCountryId($data['addressCountry'])
            ->setResideExterior($data['addressOutside'])
            ->setStateId($uf)
            ->setEstado('')
            ->setCityId($city)
            ->setCidade('')
            ->setCep($data['zipCode'])
            ->setNumero($data['number'])
            ->setEndereco($data['street'])
            ->setComplemento($data['complement'])
            ->setBairro($data['district']);
        if ($data['addressOutside']) {
            $address->setCityId(0)
                ->setCountryId($data['addressCountry'])
                ->setEstado($data['uf'])
                ->setCidade($data['city']);
        }
        return $this->em->getRepository(Address::class)->save($address);
    }

    private function populatePhone(array $data, $user): Phone
    {
        $cellPhone = new Phone();
        $cellPhone->setPais($data['cellPhoneIso2'])
            ->setDdi($data['cellPhoneDialCode'])
            ->setTelefone($data['cellPhone'])
            ->setTbTipoTelefoneId(3)
            ->setTbPessoaId($user->getId());
        return $this->em->getRepository(Phone::class)->save($cellPhone);
    }

    private function insertExemptionAffliation(User $user): void
    {
        $date = new \DateTime();
        $user->setExemption(1)->setIrpf(1);
        //match ($user->getFiliado()) {
        //    9 => $user->setDataSolicitacaoReFiliacao($date)->setFiliado(14),
        //    default => $user->setDataSolicitacaoFiliacao($date)->setFiliado(2),
        //};
        $user->setStatus(0);
        $this->em->getRepository(User::class)->save($user); // atualiza cadastro
        $array = ['plan' => 1, 'value' => '0.00', 'paymentMethod' => 0, 'directory' => 1];
        $subscription = $this->createSubscription($user, $array, 0); // cria ou edita assinatura como isenta
        $transaction = $this->populateTransaction($user, '0.00', $date, $date, null, 0, 'exemption', '', 1, 0, 2, 'Filiação Isenta', '', '', '', 1, 1, '', '', $subscription->getId(), '');
        $this->em->getRepository(Transaction::class)->save($transaction); // cria transação isenta
    }

    public function SaveAffiliation(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $userAdmin = $this->getLogged();
            $data = (array)$request->getParams();
            $files = $request->getUploadedFiles();
            $data['addressOutside'] ??= 0;
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'] ?? '')]);
            if ($user) $this->checkStatus($user);
            if (!$user) {
                $checkMail = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if ($checkMail) throw new \Exception('Email já cadastrado, volte e digite um email diferente!');
            }
            $this->validaRegisterData($data, $user);
            $user = $this->populateUser($data, $user);
            $address = $this->populateAddress($data, $user);
            $phone = $this->populatePhone($data, $user);
            $hash = Utils::generateToken();
            $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['user' => $user->getId()]);
            if (!$indication) $indication = new IndicationOfAffiliation();
            $folder = UPLOAD_FOLDER . 'indicacoes/filiacao/';
            $membershipForm = $files['membershipForm'];
            if ($membershipForm && $membershipForm->getClientFilename()) {
                $time = time();
                $extension = explode('.', $membershipForm->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}membershipForm.{$extension}";
                $membershipForm->moveTo($target);
                $indication->setMembershipForm($target);
            }
            $indication->setStatus(1)
                ->setUser($user)
                ->setHash($hash)
                ->setUserAdmin($userAdmin)
                ->setUsed(0);
            $this->em->getRepository(IndicationOfAffiliation::class)->save($indication);
            if ($data['cpm'] == 'true' || $data['exemption'] == 1) {
                $this->insertExemptionAffliation($user);
            } else {
                $date = new \DateTime();
                $data = ['plan' => 1, 'dueDate' => $date->format('d'), 'value' => 42.35, 'paymentMethod' => 2, 'plan' => 1, 'directory' => 1];
                $subscription = $this->createSubscription($user, $data, 1);
                $transaction = $this->populateTransaction($user, 42.35, $date, $date, null, 2, 'pending', '', 1, 1, 2, 'Indicação de filiação', '', '', '', 1, 1, '', '', $subscription->getId(), '');
                $this->em->getRepository(Transaction::class)->save($transaction); // cria transação isenta
            }

            /*-----Hubspot/Mailchimp-----*/
            Hubspot::createContact($user);
            $this->mailchimpCreateUser($user, null, null);

            $msg = Messages::affiliationAlert($user);
            Email::send('refiliacao@novo.org.br', 'Departamento de Filiação', 'Conferência de Indicação de Filiação', $msg, '');
            $msg = "Solicitação realizada com sucesso, sua solicitação será analisada pela equipe de filiação e após aprovação ficará 3 dias em período de impugnação e o titular receberá um email de confirmação.";
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function list(Request $request, Response $response)
    {
        $user = $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $status = $request->getQueryParam('status');
        $indications = $this->em->getRepository(IndicationOfAffiliation::class)->list($user, $name, $state, $city, $status, 20, $index * 20);
        $total = $this->em->getRepository(IndicationOfAffiliation::class)->listTotal($user, $name, $state, $city, $status)['total'];
        $partial = ($index * 20) + sizeof($indications);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $indications,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function removeIndication(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $indication = $this->em->getRepository(IndicationOfAffiliation::class)->find($id);
            $indication->setStatus(3)->setUsed(1);
            $this->em->getRepository(IndicationOfAffiliation::class)->save($indication);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Removido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function resend(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $data = (array)$request->getParams();
            $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['id' => $data['modalId'], 'status' => 2, 'used' => 0]);
            if (!$indication) throw new \Exception("Verifique se a indicação já foi aprovada pelo setor de filiação ou se o titular já não efetivou o pedido!");
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $indication->getUser()->getId()]);
            if (!$data['whatsapp'] && !$data['email']) throw new \Exception("Selecione pelo menos uma opção de envio!");
            $label = "Para confirmar clique no link a seguir: https://espaco-novo.novo.org.br/confirmacao-de-filiacao/" . $indication->getHash();
            if ($data['whatsapp']) {
                $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
                if (!$phone) throw new \Exception("Telefone inválido");
                //$smsRegister = new IndicationOfAffiliationSms();
                //$smsRegister->setUser($userAdmin)
                //            ->setIndication($indication)
                //            ->setPhone($phone->getPhoneFormated());
                //$this->em->getRepository(IndicationOfAffiliationSms::class)->save($smsRegister);
                WhatsService::messageIndicationOfAffiliation($user->getfirstName(), $phone->getPhoneFormated(), $link);
            }
            if ($data['email']) {
                $msg = Messages::affiliationConfirm($user, $label);
                Email::send($user->getEmail(), $user->getName(), 'Solicitação de Filiação', $msg, '');
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => "Enviado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}