<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\Bank;
use App\Models\Entities\Provider;
use App\Models\Entities\ChartOfAccounts;
use App\Models\Entities\SystemFeatures;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class RecordsController extends Controller
{
    public function supplier(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_FORNECEDORES_VISUALIZACAO, true);
        $banks = $this->em->getRepository(Bank::class)->findAll();
        $accounts = $this->em->getRepository(ChartOfAccounts::class)->findBy(['active' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'records/provider.phtml', 'accounts' => $accounts,
            'section' => 'supplier', 'subMenu' => 'records', 'user' => $user, 'banks' => $banks
        ]);
    }

    public function list(Request $request, Response $response)
    {
        $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CADASTROS_FORNECEDORES_VISUALIZACAO, true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $suppliers = $this->em->getRepository(Provider::class)->list($request->getQueryParams(), $limit, $index * $limit);
        $total = $this->em->getRepository(Provider::class)->listTotal($request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($suppliers);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $suppliers,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function save(Request $request, Response $response)
    {
        try {
            $this->getLogged();
            $data = (array)$request->getParams();
            $this->em->beginTransaction();
            $msg = "Fornecedor cadastrado com sucesso!";
            $fields = [
                'cpfCnpj' => 'CPF/CNPJ',
                'name' => 'Razão Social',
                'paymentMethod' => 'Forma Pagamento'
            ];
            Validator::requireValidator($fields, $data);
            $verify = $this->em->getRepository(Provider::class)->findOneBy(['cpfCnpj' => Utils::onlyNumbers($data['cpfCnpj']), 'status' => 1]);
            if ($verify && $verify->getId() != $data['supplierId']) throw new \Exception('Fornecedor já cadastrado!');
            $supplier = new Provider();
            if ($data['supplierId'] > 0) {
                $supplier = $this->em->getRepository(Provider::class)->find($data['supplierId']);
                $msg = "Fornecedor editado com sucesso!";
            }
            $supplier->setCpfCnpj(Utils::onlyNumbers($data['cpfCnpj']))
                ->setName($data['name'])
                ->setNameResponsible($data['nameResponsible'])
                ->setCpfResponsible(Utils::onlyNumbers($data['cpfResponsible']))
                ->setPhone($data['phone'])
                ->setEmail($data['email'])
                ->setPaymentMethod((int)$data['paymentMethod']);
            if ($data['creditAccount']) {
                $account = explode('/', $data['creditAccount']);
                $account = $this->em->getRepository(ChartOfAccounts::class)->findOneBy(['account' => trim($account[0])]);
                if (!$account) throw new \Exception("Conta Crédito não encontrada!");
                $supplier->setAccountCredit($account);
            }
            if ($data['paymentMethod'] == 2) {
                $supplier->setBank($data['bank'] ? $this->em->getReference(Bank::class, $data['bank']) : null)
                    ->setAgency($data['agency'])
                    ->setAccount($data['account']);
            }
            $this->em->getRepository(Provider::class)->save($supplier);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response, bool $type)
    {
        try {
            $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $supplier = $this->em->getRepository(Provider::class)->find($id);
            if ($type === true) {
                $supplier->changeActive();
                $msg = "Status do Fornecedor alterado para {$supplier->activeStr()}";
            } else {
                $supplier->setStatus(false);
                $msg = "Fornecedor excluído com sucesso!";
            }
            $this->em->getRepository(Provider::class)->save($supplier);
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function data(Request $request, Response $response)
    {
        $this->getLogged(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $supplier = $this->em->getRepository(Provider::class)->data($id);
        return $response->withJson([
            'status' => 'ok',
            'message' => $supplier,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function exportProviders(Request $request, Response $response)
    {
        $this->getLogged(true);
        Validator::validatePermission(SystemFeatures::CADASTROS_FORNECEDORES_EXPORTACAO, true);
        $candidates = $this->em->getRepository(Provider::class)->list($request->getQueryParams(), $corporateName, $cpfCnpj);
        $filename = "Relatório de Fornecedores - " . date('d-m-Y') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = ['ID', 'Nome Responsável', 'Nome', 'cpfCnpj', 'Telefone', 'Ativo', 'Conta Contábil'];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($candidates as $candidate) {
            fputcsv($out, array_values($candidate), ';', '"');
        }
        fclose($out);
        exit;
    }
}