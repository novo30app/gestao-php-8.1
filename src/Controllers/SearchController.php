<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\AccessLevel;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Candidate;
use App\Models\Entities\CandidatePost;
use App\Models\Entities\CandidateStatus;
use App\Models\Entities\City;
use App\Models\Entities\Form;
use App\Models\Entities\Leaders;
use App\Models\Entities\Occupation;
use App\Models\Entities\State;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\SystemFeatures;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class SearchController extends Controller
{
    public function access(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::GESTAO_ACESSOS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/access.phtml', 'subMenu' => 'management',
            'section' => 'access', 'user' => $user, 'states' => $states
        ]);
    }

    public function candidates(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_CANDIDATOS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $occupations = $this->em->getRepository(CandidatePost::class)->findBy([], ['name' => 'asc']);
        $candidatesStatus = $this->em->getRepository(CandidateStatus::class)->findBy([], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/candidates.phtml', 'candidatesStatus' => $candidatesStatus,
            'subMenu' => 'search', 'section' => 'candidates', 'user' => $user, 'states' => $states, 'occupations' => $occupations]);
    }

    public function municipalStrategicPlanning2022(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/municipalStrategicPlanning2022.phtml',
            'subMenu' => 'search', 'section' => 'municipalStrategicPlanning2022', 'user' => $user, 'states' => $states]);
    }

    public function municipalStrategicPlanning2022List(Request $request, Response $response)
    {
        $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $email = $request->getQueryParam('email');
        $uf = $request->getQueryParam('uf');
        $city = $request->getQueryParam('city');
        $form = $this->em->getRepository(Form::class)->list($email, $uf, $city, 20, $index * 20);
        $total = $this->em->getRepository(Form::class)->listTotal($email, $uf, $city)['total'];
        $partial = ($index * 20) + sizeof($form);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $form,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function municipalStrategicPlanning2022View(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $form = $this->em->getRepository(Form::class)->find($id);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/municipalStrategicPlanning2022View.phtml',
            'section' => 'municipalStrategicPlanning2022', 'subMenu' => 'search', 'user' => $user, 'form' => $form]);
    }

    public function leaders(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_DIRIGENTES_VISUALIZACAO, true);
        $levels = $this->em->getRepository(AccessLevel::class)->findBy([], ['id' => 'asc']);
        $occupations = $this->em->getRepository(Occupation::class)->findBy([], ['name' => 'asc']);
        $states = $this->em->getRepository(State::class)->findAll();
        $directory = $request->getAttribute('route')->getArgument('directory');
        $directory = $directory ?: 0;
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/leaders.phtml',
            'subMenu' => 'directories', 'section' => 'managers', 'user' => $user, 'occupations' => $occupations,
            'levels' => $levels, 'states' => $states, 'directory' => $directory]);
    }

    private function importLeaders(): void
    {
        $row = 1;
        if (($handle = fopen("dirigentes.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row > 1) {
                    $level = trim($data[0] == 'Nacional') ? 3 : (trim($data[0] == 'Estadual') ? 2 : 1);
                    $cargo = trim($data['2']);
                    $cargo = $this->em->getRepository(Occupation::class)->findOneBy(['name' => $cargo]);
                    if ($cargo) {
                        $leader = new Leaders();
                        $leader->setName($data[3])
                            ->setCpf($data[5])
                            ->setEmail($data[4])
                            ->setOccupation($cargo)
                            ->setLevel($this->em->getReference(AccessLevel::class, $level));
                        if ($level == 2) {
                            $state = explode('-', $data[1])[1];
                            $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => trim($state)]);
                            $leader->setState($state);
                        }
                        if ($level == 1) {
                            $state = explode('-', $data[1])[1];
                            $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => trim($state)]);
                            if ($city) {
                                $leader->setCity($city);
                                $leader->setState($city->getState());
                            }
                        }
                        $this->em->getRepository(Leaders::class)->save($leader);
                    }
                }
                $row++;
            }
            fclose($handle);
        }
    }

    private function importCandidates(): void
    {
        $row = 1;
        if (($handle = fopen("candidatos.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($row > 1) {
                    $city = trim($data[3]);
                    $state = trim($data[3]);
                    $candidatePostStr = trim($data[5]);
                    $candidateStatusStr = trim($data[6]);
                    $city = trim($data[3]);
                    $cpf = $data[1];
                    $email = $data[2];
                    $year = $data[7];
                    $numberOfVotes = $data[8] ?? 0;
                    $name = Utils::firstUpper($data[0]);
                    $candidatePost = $this->em->getRepository(CandidatePost::class)->findOneBy(['name' => $candidatePostStr]);
                    if (!$candidatePost) {
                        $candidatePost = new CandidatePost();
                        $candidatePost->setName($candidatePostStr);
                        $candidatePost = $this->em->getRepository(CandidatePost::class)->save($candidatePost);
                    }
                    $candidateStatus = $this->em->getRepository(CandidateStatus::class)->findOneBy(['name' => $candidateStatusStr]);
                    if (!$candidateStatus) {
                        $candidateStatus = new CandidateStatus();
                        $candidateStatus->setName($candidateStatusStr);
                        $candidateStatus = $this->em->getRepository(CandidateStatus::class)->save($candidateStatus);
                    }
                    $candidate = new Candidate();
                    $candidate->setName($name)
                        ->setCpf($cpf)
                        ->setEmail($email)
                        ->setYear($year)
                        ->setNumberOfVotes($numberOfVotes)
                        ->setCandidatePost($candidatePost)
                        ->setCandidateStatus($candidateStatus);
                    $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => $city]);
                    $state = $this->em->getRepository(State::class)->findOneBy(['estado' => $state]);
                    if ($state) $candidate->setState($state);
                    if ($city) {
                        $candidate->setCity($city)
                            ->setState($city->getState());
                    }
                    $this->em->getRepository(Candidate::class)->save($candidate);
                }
                $row++;
            }
            fclose($handle);
        }
    }

    public function conventions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_CONVENCOES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - conventions']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/conventions.phtml', 'term' => $term,
            'subMenu' => 'directories', 'section' => 'conventions', 'user' => $user, 'states' => $states, 'cities' => $cities]);
    }

    public function disaffiliation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_DESFILIACOES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - disaffiliation']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/disaffiliation.phtml', 'term' => $term,
            'subMenu' => 'search', 'section' => 'disaffiliation', 'user' => $user, 'states' => $states, 'cities' => $cities]);
    }

    public function donors(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::FINANCEIRO_DOADORES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - donors']);
        $this->newAccessLog($user, 'donors');
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/donors.phtml', 'term' => $term,
            'subMenu' => 'financial', 'section' => 'donors', 'user' => $user, 'states' => $states, 'cities' => $cities]);
    }

    public function Mesoregions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_MESORREGIOES_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'search/mesoregions.phtml', 'subMenu' => 'records',
            'section' => 'mesoregions', 'user' => $user, 'states' => $states]);
    }
}