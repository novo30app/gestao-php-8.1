<?php

namespace App\Controllers;

use App\Exceptions\ForbiddenException;
use Doctrine\ORM\EntityManager;
use App\Helpers\Utils;
use App\Helpers\Session;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Models\Entities\Address;
use App\Models\Entities\AccessLog;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\Directory;
use App\Models\Entities\PersonRede;
use App\Models\Entities\EventsTransactions;
use App\Models\Entities\EventsRegistrations;
use App\Models\Entities\Historic;
use App\Models\Entities\Holidays;
use App\Models\Entities\IuguTokens;
use App\Models\Entities\IndicationOfAffiliation;
use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\ChartOfAccounts;
use App\Models\Entities\AccountingRecord;
use App\Models\Entities\AccountingAccounts;
use App\Models\Entities\LogApproved;
use App\Models\Entities\OfxLines;
use App\Models\Entities\PersonIugu;
use App\Services\MaxiPago;
use App\Services\Mailchimp;
use App\Services\Hubspot;
use App\Services\Email;
use App\Services\IuguService;
use Slim\Views\PhpRenderer;

abstract class Controller
{
    protected $em;
    protected $renderer;
    protected $baseUrl = BASEURL;
    protected $env = ENV;

    public function __construct(EntityManager $entityManager, PhpRenderer $renderer)
    {
        $this->em = $entityManager;
        $this->renderer = $renderer;
    }

    protected function getConfigs()
    {
        $config = parse_ini_file('configs.ini', true);
        return $config[$config['environment']];
    }

    protected function getUserLoggedByToken(string $token, array $affiliatedStatus = [])
    {
        $tokenDecode = explode(':', $token);
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => base64_decode($tokenDecode[0]),
            'token2' => base64_decode($tokenDecode[1])]);
        if (!$user) throw new ForbiddenException('403 Forbidden');
        if (!empty($affiliatedStatus) && !in_array($user->getFiliado(), $affiliatedStatus)) {
            throw new ForbiddenException('Você não tem permissão para acessar esse conteúdo');
        }
        return $user;
    }


    protected function getLogged(bool $excepetion = false, ?bool $diretive = false)
    {
        $user = Session::get('admin');
        if ($diretive) $user = Session::get('diretiva');
        if (!$user) {
            if ($excepetion) throw new \Exception("Sessão expirada");
            Session::set('redirect', $_SERVER["REQUEST_URI"]);
            Session::set('errorMsg', 'Você precisa se autenticar');
            $this->redirect('login');
            exit;
        }
        $user = $this->em->getRepository(UserAdmin::class)->find($user);
//        if (ENV === 'local') {
//            $user = $this->em->getRepository(UserAdmin::class)->find(60);
//        }
        return $user;
    }

    protected function redirect(string $url = '')
    {
        header("Location: {$this->baseUrl}{$url}");
        die();
    }

    protected function redirectByPermissions($url = '')
    {
        Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo.');
        header("Location: {$this->baseUrl}{$url}");
        die();
    }

    public function responseJson($result, $response, $code = 200)
    {
        if ($result instanceof \Exception) {
            return $response->withJson(['status' => 'error',
                'message' => $result->getMessage(),])->withStatus(500);
        }

        return $response->withJson($result)
            ->withStatus($code)
            ->withHeader('Content-type', 'application/json');
    }

    protected function iuguAddress(Address $address): array
    {
        $city = $state = null;
        if (!$address->getResideExterior()) {
            $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId())->getCidade();
            $state = $this->em->getRepository(State::class)->find($address->getStateId())->getSigla();
        }
        return [
            'resides_outside' => $address->getResideExterior(),
            'street' => $address->getEndereco(),
            'number' => $address->getNumero(),
            'district' => $address->getBairro(),
            'city' => $city ?? 'São Paulo',
            'state' => $state ?? 'SP',
            'zip_code' => Utils::onlyNumbers($address->getCep()),
        ];
    }

    protected function newAccessLog(UserAdmin $user, $menu): AccessLog
    {
        $acessData = Utils::getAcessData();
        $accessLog = new AccessLog();
        $accessLog->setIp($acessData['ip'])
            ->setDevice($acessData['name'])
            ->setSo($acessData['platform'])
            ->setUserAdmin($user)
            ->setMenu($menu);
        return $this->em->getRepository(AccessLog::class)->save($accessLog);
    }

    protected function populateTransaction(User   $user, float $value, \DateTime $creationDate, \DateTime $dueDate, ?\DateTime $paymentDate, int $paymentType,
                                           string $status, string $customerId, int $plan, int $gateway, int $origin, string $obs = '', string $tokenCreditCard, string $invoiceId,
                                           string $redeId, int $directory, int $directoryOrigin, string $url = '', string $barCode = '', $subscription = false, string $qrCode = null)
    {
        $transaction = new Transaction();
        $transaction->setTbPessoaId($user->getId())
            ->setPendenciaTitulo($user->getPendenciaTitulo() ?? 0)
            ->setValor($value)
            ->setDataCriacao($creationDate)
            ->setDataVencimento($dueDate)
            ->setDataPago($paymentDate)
            ->setStatus($status)
            ->setFormaPagamento($paymentType)
            ->setPeriodicidade($plan)
            ->setCustomerId($customerId)
            ->setGatewayPagamento($gateway)
            ->setOrigemTransacao($origin)
            ->setObs($obs)
            ->setTokenCartaoCredito($tokenCreditCard)
            ->setInvoiceId($invoiceId)
            ->setRedeId($redeId)
            ->setTbDiretorioId($directory)
            ->setTbDiretorioOrigem($directoryOrigin)
            ->setUrl($url)
            ->setCodigoBarras($barCode)
            ->setNumeroRecibo('Em Processamento')
            ->setQrCode($qrCode);
        if ($subscription) {
            $transaction->setTbAssinaturaId($subscription);
        }
        return $transaction;
    }

    protected function createSubscription(User $user, array $data, int $gateway, $redeId = false, $card = false, $origin = 2, $directory = null): PersonSignature
    {
        $subscription = false;
        if ($origin == 2) {//só pode ter uma assinatura de filiação
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2]);
        }
        if (!$subscription) $subscription = new PersonSignature();
        $today = new \DateTime();
        $nextCharge = new \DateTime();
        $nextCharge->add(new \DateInterval("P{$data['plan']}M"));
        $cycleDay = $data['dueDate'] ?: $today->format('d');
        $subscription->setStatus('created')
            ->setDataCriacao($today)
            ->setOrigemTransacao($origin)
            ->setGatewayPagamento($gateway)
            ->setGatewayStatus(1)
            ->setCustomerId($redeId)
            ->setTbPessoaId($user->getId())
            ->setValor($data['value'])
            ->setPaymentForm($data['paymentMethod'])
            ->setPeriodicidade($data['plan'])
            ->setDiaCiclo($cycleDay)
            ->setMesCiclo(date('m'))
            ->setDataGeraFatura($nextCharge)
            ->setTbDiretorioOrigem((int)$data['directory']);
        if ($card) {
            $subscription->setTokenCartaoCredito($card);
        }
        return $this->em->getRepository(PersonSignature::class)->save($subscription);
    }

    protected function createCustomer(User $user)
    {
        $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
        if (!$personRede || $this->env != 'prod') {
            $redeId = MaxiPago::createCustomer($user);
            $personRede = new PersonRede();
            $personRede->setUser($user)
                ->setCustomerId($redeId);
            $this->em->getRepository(PersonRede::class)->save($personRede);
        }
        return $personRede;
    }

    protected function createCard(User $user, $personRede, $data)
    {
        $cardToken = MaxiPago::createCard($personRede, $data['cardNumber'], $data['expiringDate'], $data['cardName']);
        $card = new PersonCreditCard();
        $card->setToken($cardToken)
            ->setUser($user)
            ->setFlag($data['flag'])
            ->setName($data['cardName'])
            ->setGatewayPagamento(2)
            ->setInternational($data['international'])
            ->setExpiration($data['expiringDate'])
            ->setCardNumber(substr($data['cardNumber'], -4));
        return $this->em->getRepository(PersonCreditCard::class)->save($card);
    }

    protected function populateEventTransaction(User $user, $event, $price, $transaction, $status, $date)
    {
        $transactionsEvent = new EventsTransactions();
        $transactionsEvent->setTbEventsId($event->getId())
            ->setTbPessoaId($user->getId())
            ->setTbTransacaoId($transaction->getId())
            ->setValue($price->getValue())
            ->setCode(strtoupper(substr(bin2hex(random_bytes(5)), 1)))
            ->setStatus($status)
            ->setData($date);
        return $this->em->getRepository(EventsTransactions::class)->save($transactionsEvent);
    }

    protected function populateEventRegistrations(User $user, $event, $price, $transactionsEvent, $status, $date)
    {
        $participants = new EventsRegistrations();
        $participants->setTbEventos($event)
            ->setTbEventosPrecosId($price->getId())
            ->setTbEventosTransacaoId($transactionsEvent->getId())
            ->setTbPessoaId($user)
            ->setNome($user->getName())
            ->setDataNascimento($user->getDataNascimento())
            ->setEmail($user->getEmail())
            ->setCpf($user->getCpf())
            ->setCode(strtoupper(substr(bin2hex(random_bytes(4)), 1)))
            ->setValue($price->getValue())
            ->setStatus($status)
            ->setData($date);
        return $this->em->getRepository(EventsRegistrations::class)->save($participants);
    }

    protected function registerHistoric($user, int $type)
    {
        $historic = new Historic();
        $historic->setUser($user)->setType($type)->setDate(new \Datetime);
        $this->em->getRepository(Historic::class)->save($historic);
    }

    public function getWorkingDay(string $date): \Datetime
    {
        /** Recebe data, converte e adiciona 31 dias */
        $date = new \Datetime(date('Y-m-d', strtotime($date)));
        $date->modify('+31 days');
        $dateFormat = $date->format('Y-m-d');
        /** Confere se é feriado, se for adiciona um dia */
        $holidays = $this->em->getRepository(Holidays::class)->findAll();
        foreach ($holidays as $holiday) {
            if ($dateFormat === date('Y-') . $holiday->getData()) {
                $date->modify('+1 days');
                $dateFormat = $date->format('Y-m-d');
            }
        }
        /** Verifica se é final de semana, se for adiciona um dia até que não seja mais */
        $weekDay = date('w', strtotime($dateFormat));
        while (in_array($weekDay, array(0, 6))) {
            $date->modify('+1 days');
            $dateFormat = $date->format('Y-m-d');
            $weekDay = date('w', strtotime($dateFormat));
        }
        /** Retorna data útil correto */
        return $date;
    }

    public function getIuguTokenDirectory(int $directory): array
    {
        $key = '473900DCC93F1CF5329439FDA1EE86A494F5B694ADF7AD70F1A8224971FF9B59';
        $deposit = 1;
        $veriryTokenIugu = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => $directory]);
        if ($veriryTokenIugu) {
            $key = $veriryTokenIugu->getToken();
            $deposit = $veriryTokenIugu->getDirectory();
        } else {
            $getDirectory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $directory]);
            if ($getDirectory->isCity()) {
                $getDirectoryStateId = $this->em->getRepository(Directory::class)->findOneBy(['tbEstadoId' => $getDirectory->getStateId(), 'estadual' => 'S']);
                if ($getDirectoryStateId) {
                    $veriryTokenIugu = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => $getDirectoryStateId->getId()]);
                    if ($veriryTokenIugu) {
                        $key = $veriryTokenIugu->getToken();
                        $deposit = $veriryTokenIugu->getDirectory();
                    }
                }
            }
        }
        $array = [
            'key' => $key,
            'deposit' => $deposit,
            'owner' => $directory
        ];
        return $array;
    }

    protected function checkIndicationOfAffiliation(User $user, int $status): void
    {
        $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['user' => $user->getId()]);
        if ($indication) {
            $indication->setUsed(1)->setStatus($status);
            $this->em->getRepository(IndicationOfAffiliation::class)->save($indication);
        }
    }

    protected function checkValidPaymentDate(UserAdmin $userAdmin, $date): void
    {
        $valid = true;
        $dateFormat = str_replace('/', '-', $date);
        $diasemana = intVal(date('w', strtotime($dateFormat)));
        // Valida se é final de semana
        if (in_array($diasemana, [6, 0])) {
            $valid = false;
        }
        // Valida se a data é válida
        if (new \Datetime(str_replace('/', '-', $date)) <= new \Datetime()) {
            $valid = false;
            if ($userAdmin->getLevel() == 3) {
                if (Validator::validatePermission(\App\Models\Entities\SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_REGISTRAR_PAGAMENTO_PARA_O_MESMO_DIA)) {
                    $valid = true;
                }
            }
        }
        // Valida se é feriado
        if ($this->em->getRepository(Holidays::class)->findOneBy(['data' => date('Y-m-d', strtotime($dateFormat))])) $valid = false;
        if (!$valid) throw new \Exception("Data inválida, escolha o próximo dia útil para efetivação do pagamento");
    }

    protected function updateStatusAffiliatedRD(User $user): void
    {
        $status = match ($user->getFiliado()) {
            0 => 'Não',
            2 => 'Em análise',
            14 => 'Em análise - Refiliação',
            1, 3 => 'Em análise - Boleto - Apagar !!!',
            4 => 'Em análise - Pedido de impugnação',
            5 => 'Cancelado - A pedido do solicitante',
            6 => 'Cancelado - Impugnado',
            default => 'Desconhecido'
        };
    }

    protected function updatePaymentStatusRD(User $user, int $type): void
    {
        $type = match ($type) {
            1 => 'Boleto - Em dia',
            2 => 'Boleto - Gerado',
            3 => 'Boleto - Vencido',
            4 => 'Cartão - Em dia',
            5 => 'Cartão - Erro',
            6 => 'Isento',
            default => 'Desconhecido'
        };
    }

    protected function validLastTransaction(User $user, Transaction $transaction, int $type): void
    {
        $valid = false;
        $lastTransaction = $this->em->getRepository(Transaction::class)->findOneBy(['tbPessoaId' => $user->getId(), 'origemTransacao' => 2], ['id' => 'DESC']);
        if ($lastTransaction) {
            if ($transaction == $lastTransaction->getId()) $valid = true;
        }
    }

    protected function registerAccountRecord(UserAdmin $userAdmin, PaymentRequirements $requirement): void
    {
        $this->em->beginTransaction();
        // se for tarifa bancária irá ser tratado com valores fixos.
        if ($requirement->getTypeDocument() == 4) {
            $comment = "Tarifa Bancária";
            $account = match ($requirement->getOrigin()) {
                2 => $this->em->getReference(ChartOfAccounts::class, 304),
                3 => $this->em->getReference(ChartOfAccounts::class, 804),
                4 => $this->em->getReference(ChartOfAccounts::class, 304),
                default => null
            };
            $creditAccount = $requirement->getDirectoryAccount()->getAccountingAccount();
        } else {
            $comment = "Prestação de Serviço - " . ucfirst(strtolower($requirement->getProvider()->getName())) . " - CNPJ {$requirement->getProvider()->getCpfCnpj()} - NF {$requirement->getNfNumber()}";
            if ($requirement->getType() == 16) {
                $comment = "Transferência para {$requirement->getDirectoryTransfer()->getName()} - {$requirement->getDirectoryTransfer()->getCnpj()}";
            }
            $account = $requirement->getContract() ? $requirement->getContract()->getAccount() : null;
            $creditAccount = $requirement->getProvider()->getAccountCredit();
            if ($requirement->getType() == 16) {
                $account = $requirement->getProvider()->getAccountCredit();
                $creditAccount = $requirement->getDirectoryAccount()->getAccountingAccount();
            }
            if (preg_match("/Vale Transporte/", $requirement->getDescription())) {
                $account = $this->em->getReference(ChartOfAccounts::class, 8179);
                $creditAccount = $requirement->getDirectoryAccount()->getAccountingAccount();
            }
            if (preg_match("/Pró-Labore/", $requirement->getDescription())) {
                $account = $this->em->getReference(ChartOfAccounts::class, 8248);
                $creditAccount = $requirement->getDirectoryAccount()->getAccountingAccount();
            }
            if (preg_match("/Salário/", $requirement->getDescription())) {
                $account = $this->em->getReference(ChartOfAccounts::class, 7751);
                $creditAccount = $requirement->getDirectoryAccount()->getAccountingAccount();
            }
        }
        /** AccountingRecord - registro contábil */
        $accountingRecord = $this->em->getRepository(AccountingRecord::class)->findOneBy(['payment' => $requirement->getId(), 'formAccounting' => 1]);
        if (!$accountingRecord) {
            $accountingRecord = new AccountingRecord();
            $accountingRecord->setUser($userAdmin)
                ->setPayment($requirement)
                ->setStatus(1)
                ->setValue($requirement->getPaymentValue())
                ->setComment($comment)
                ->setAccounting(1)
                ->setFiscal(0)
                ->setFormAccounting(1);
            if ($requirement->getType() == 16 || $requirement->getTypeDocument() == 4 || preg_match("/Vale Transporte/", $requirement->getDescription()) ||
                preg_match("/Pró-Labore/", $requirement->getDescription()) || preg_match("/Salário/", $requirement->getDescription())) {
                $dateOfx = $this->em->getRepository(OfxLines::class)->findOneBy(['requirements' => $requirement->getId()]);
                if ($dateOfx) $accountingRecord->setCompetition($dateOfx->getDate());
            }
            $this->em->getRepository(AccountingRecord::class)->save($accountingRecord);
        }
        /** AccountingAccounts - contas contábeis */
        $accountingAccounts = $this->em->getRepository(AccountingAccounts::class)->findBy(['accountingRecord' => $accountingRecord]);
        if (!$accountingAccounts && $account && $creditAccount) {
            $accountsAccounts = new AccountingAccounts();
            $accountsAccounts->setAccountingRecord($accountingRecord)
                ->setAccount($account->getId())
                ->setValue($requirement->getPaymentValue())
                ->setCreditAccount($creditAccount->getId());
            $this->em->getRepository(AccountingAccounts::class)->save($accountsAccounts);
        }
        $this->saveLogApproved($requirement, PaymentRequirements::APPROVED_ACCOUNTING);
        $this->em->commit();
    }

    protected function saveLogApproved(PaymentRequirements $requirement, int $type): void
    {
        $userAdmin = $this->getLogged(true);
        $log = new LogApproved();

        $log->setUser($userAdmin)
            ->setPaymentRequirements($requirement)
            ->setType($type);
        $this->em->getRepository(LogApproved::class)->save($log);
    }

    protected function getReceiptDirectory(Directory $directory, Transaction $transaction): string
    {
        if (!$transaction->getNumeroRecibo() || !preg_match("/{$transaction->getNumeroRecibo()}/", "P3000.")) {
            if ($directory->getReciboEmite() == 'S') {
                $mask = $this->em->getRepository(Directory::class)->find($directory)->getMaskReceipt();
                $lastReceipt = $this->em->getRepository(Transaction::class)->lastDirectoryReceipt($directory->getId())[0]['receipt'];
                $transactionsOfMonth = $this->em->getRepository(Transaction::class)->getTransactionsMonthlyByDirectory($directory);
                if ($transactionsOfMonth) { // alterar para não existe
                    $receipts = [];
                    for ($i = 1; $i <= 6; $i++) {
                        $lastReceipt++;
                        array_push($receipts, $mask . str_pad($lastReceipt, 6, "0", STR_PAD_LEFT));
                    }
                    $msg = Messages::receiptReserved($receipts, $directory);
                    $copyTo = ['deliane@novo.org.br', 'renan.almeida@novo.org.br'];
                    Email::send('luciana.pereira@novo.org.br', 'Departamento Financeiro', "Recibos reservados",
                        $msg, null, null, null, $copyTo);
                    $lastReceipt++;
                }
                return $mask . str_pad($lastReceipt, 6, "0", STR_PAD_LEFT);
            }
            return "Em processamento";
        }
        return $transaction->getNumeroRecibo();
    }

    protected function mailchimpCreateUser(User $user, $address = null, $phone = null): void
    {
        $body['email_address'] = $user->getEmail();
        $body['status'] = "subscribed";
        $body['merge_fields']['FNAME'] = $user->getFirstName();
        $body['merge_fields']['LNAME'] = $user->getSurname();
        if ($user->getDataNascimento()) $body['merge_fields']['BIRTHDAY'] = $user->getDataNascimento()->format('m/d');
        if ($user->getCpf()) $body['merge_fields']['CPF'] = $user->getCpf();
        $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE58'] = $user->getStatusString();
        if ($address) {
            $body['merge_fields']['ADDRESS']['city'] = $this->em->getRepository(City::class)->find($address->getTbCidadeId())->getCidade();
            $body['merge_fields']['ADDRESS']['state'] = $this->em->getRepository(State::class)->find($address->getStateId())->getEstado();
            $body['merge_fields']['ADDRESS']['country'] = $this->em->getRepository(Country::class)->find($address->getTbPaisId())->getNome();
        }
        if ($phone) $body['merge_fields']['PHONE'] = $phone;
        $body['merge_fields']['url'] = "novo.org.br";
        Mailchimp::createContact($body);
    }

    protected function updateStatusRegister(User $user): void
    {
        $hp = [];
        $hp['email'] = $user->getEmail();
        $hp['filiado'] = $user->getFiliadoString();
        $hp['status_de_filiado'] = $user->getFiliadoString();
        $hp['status_cadastro'] = $user->getStatusString();
        $hp['isento'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
        Hubspot::updateContact($user, $hp);
        $body = [];
        $body['email_address'] = $user->getEmail();
        $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE58'] = $user->getStatusString();
        $body['merge_fields']['ISENTO'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
        $body['merge_fields']['ADDRESS'] = '';
        Mailchimp::updateContact($body);
    }

    protected function getCustomerIdIUGU(User $user): PersonIugu
    {
        $personIugu = $this->em->getRepository(PersonIugu::class)->getCustomerId($user)['customer_id'];
        if($personIugu) {
            $personIugu = $this->em->getRepository(PersonIugu::class)->findOneBy(['customerId' => $personIugu]);
            return $personIugu;
        }
        $customer = IuguService::createCliente($user->getName(), $user->getEmail());
        $personIugu = new PersonIugu();
        $personIugu->setTbPessoaId($user->getId())->setCustomerId($customer);
        $this->em->getRepository(PersonIugu::class)->save($personIugu);
        return $personIugu;
    }
}
