<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Models\Entities\City;
use App\Models\Entities\OmbudsmanCategory;
use App\Models\Entities\OmbudsmanSubject;
use App\Models\Entities\Phone;
use App\Models\Entities\AffiliatedStatus;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\UserPermissions;
use App\Models\Entities\UsersTypes;
use App\Models\Entities\Sectors;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\PermissionUserTypeFuncionality;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\ProcessCep;
use App\Models\Entities\AccessAdmin;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Mesoregions;
use App\Models\Entities\IndicationOfAffiliation;
use App\Models\Entities\Contract;
use App\Services\WhatsService;
use App\Services\Auth;
use App\Services\Email;
use App\Services\Hubspot;
use App\Services\Mailchimp;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use App\Models\Entities\State;

class UserController extends Controller
{
    public function index(Request $request, Response $response)
    {
//        $this->em->beginTransaction();
//        if (($handle = fopen("centro-custos.csv", "r")) !== FALSE) {
//            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                if (trim($data[1]) == '') continue;
//                $payment = $this->em->getRepository(PaymentRequirements::class)->find((int)$data[0] ?? 0);
//                if (!$payment) continue;
//                $costCenter = $this->em->getRepository(CostCenter::class)->findOneBy(['name' => $data[1], 'directory' => $payment->getDirectory()]);
//                if (!$costCenter) dd($data);
//                $payment->setCostCenter($costCenter);
//                $this->em->getRepository(PaymentRequirements::class)->save($payment);
//            }
//            fclose($handle);
//        }
//        $this->em->commit();
//        die();

        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        $status = $this->em->getRepository(AffiliatedStatus::class)->findBy(['id' => [7, 8]], ['status' => 'asc']);
        $categories = $this->em->getRepository(OmbudsmanCategory::class)->findBy([], ['name' => 'asc']);
        $subjects = $this->em->getRepository(OmbudsmanSubject::class)->findBy([], ['name' => 'asc']);
        $accessAdmin = $this->em->getRepository(AccessAdmin::class)->findOneBy(['userAdmin' => $user, 'type' => 'financialReport']);
        $confidentialityAgreement = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user, 'menu' => 'confidentialityAgreement']) ? 1 : 0;
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'index.phtml', 'section' => 'home',
            'accessAdmin' => $accessAdmin, 'cities' => $cities, 'user' => $user, 'states' => $states, 'status' => $status,
            'categories' => $categories, 'subjects' => $subjects, 'confidentialityAgreement' => $confidentialityAgreement]);
    }

    public function register(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_ACESSOSADMIN_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $mesoregions = match ($user->getLevel()) {
            3 => $this->em->getRepository(Mesoregions::class)->findAll(),
            default => $this->em->getRepository(Mesoregions::class)->findBy(['state' => $user->getState()->getId()])
        };
        $usersSectors = $this->em->getRepository(Sectors::class)->findBy(['active' => true]);
        $this->newAccessLog($user, 'userAdmin');
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/index.phtml', 'cities' => $cities,
            'usersSectors' => $usersSectors, 'subMenu' => 'records', 'section' => 'users', 'user' => $user, 'states' => $states,
            'mesoregions' => $mesoregions]);
    }

    public function save(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $adminAccess = isset($data['adminAccess']) ? 1 : 0;
            $exportAccess = isset($data['exportAccess']) ? 1 : 0;
            $financialReport = isset($data['financialReport']) ? 1 : 0;
            $directoryAreaAccess = isset($data['directoryAreaAccess']) ? 1 : 0;
            $events = isset($data['events']) ? 1 : 0;
            $libertasCourse = isset($data['libertasCourses']) ? 1 : 0;
            Validator::validateAccess($adminAccess, $directoryAreaAccess, $financialReport, $events, $libertasCourse);
            $fields = [
                'level' => 'Nível',
                'type' => 'Tipo',
                'name' => 'Nome',
                'email' => 'E-mail',
            ];
            if ($data['level'] == 2) {
                $fields['state'] = 'Estado';
            } elseif ($data['level'] == 1) {
                $fields['state'] = 'Estado';
                $fields['city'] = 'Cidade Principal';
            }
            Validator::requireValidator($fields, $data);
            $oldEmail = '';
            if ($data['edit'] == 0) {
                $userCheck = $this->em->getRepository(UserAdmin::class)->findOneBy(['email' => $data['email']]);
                if ($userCheck) throw new \Exception("Usuário já cadastrado!");
                $userSave = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if (!$user->isAdminNational() && !$userSave) throw new \Exception("E-mail não encontrado, utilize o e-mail cadastrado no Espaço NOVO!");
                $userAdmin = new UserAdmin();
            } else {
                $userAdmin = $this->em->getRepository(UserAdmin::class)->findOneBy(['id' => $data['userId']]);
                $oldEmail = $userAdmin->getEmail();
            }
            $data['level'] = $data['level'] <= $user->getLevel() ? $data['level'] : $user->getLevel(); // garantir que o nivel do usuario é menor ou igual ao do admin
            $data['status'] ??= 0;
            $userAdmin->setActive($data['status'])
                ->setLevel($data['level'])
                ->setType($data['type'])
                ->setName($data['name'])
                ->setEmail($data['email'])
                ->setAdminAccess($adminAccess)
                ->setDirectoryAreaAccess($directoryAreaAccess)
                ->setExportAccess($exportAccess)
                ->setFinancialReport($financialReport)
                ->setEvents($events)
                ->setLibertasCourses($libertasCourse)
                ->setTypeView($data['typeView']);
            if ($data['city']) $userAdmin->setCity($this->em->getReference(City::class, $data['city']));
            if ($data['state']) $userAdmin->setState($this->em->getReference(State::class, $data['state']));
            if ($data['level'] == UserAdmin::LEVEL_NATIONAL) $userAdmin->setCity(null)->setState(null);
            $this->em->getRepository(UserAdmin::class)->save($userAdmin);
            if ($data['level'] == 1 && $user->getLevel() != 1) {
                $this->em->getRepository(AccessAdmin::class)->dropAccess($userAdmin->getId(), 'city');
                $this->em->getRepository(AccessAdmin::class)->dropAccess($userAdmin->getId(), 'meso');
                $accessAdminCity = new AccessAdmin();
                $accessAdminCity->setUserAdmin($userAdmin->getId())->setType('city')->setAccess($data['city']);
                $this->em->getRepository(AccessAdmin::class)->save($accessAdminCity);
                if (isset($data['cities'])) {
                    foreach ($data['cities'] as $city) {
                        $accessAdmin = new AccessAdmin();
                        $accessAdmin->setUserAdmin($userAdmin->getId())->setType('city')->setAccess($city);
                        $this->em->getRepository(AccessAdmin::class)->save($accessAdmin);
                    }
                }
                if (isset($data['mesos'])) {
                    foreach ($data['mesos'] as $meso) {
                        $accessAdmin = new AccessAdmin();
                        $accessAdmin->setUserAdmin($userAdmin->getId())->setType('meso')->setAccess($meso);
                        $this->em->getRepository(AccessAdmin::class)->save($accessAdmin);
                    }
                }
            }
            $msg = "Usuário atualizado com sucesso!";
            if ($data['edit'] == 0) $msg = "Usuário cadastrado com sucesso!";
            Auth::sincronize($data['name'], $data['email'], $oldEmail);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response, bool $status)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $userAdmin = $this->em->getRepository(UserAdmin::class)->find($id);
            $userAdmin->setActive($status);
            $newStatus = $status ? 'activated' : 'disabled';
            $this->em->getRepository(UserAdmin::class)->save($userAdmin);
            $this->newAccessLog($this->getLogged(), 'changeAcess - ' . $id . ' - ' . $newStatus);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Usuário atualizado com sucesso",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function Actions(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $button = $request->getAttribute('route')->getArgument('button');
            $action = $request->getAttribute('route')->getArgument('action');
            $column = null;
            $DataAction = $this->em->getRepository(User::class)->findOneBy(['id' => $id]);
            $DataSignature = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $id, 'origin' => 2]);
            if (in_array($button, [4, 7, 12])) {
                if (!$DataSignature) throw new \Exception("Assinatura não encontrada!");
            }
            $today = new \DateTime();
            $value = 0;
            $status = 'created';
            switch ($button) {
                case 1:
                    $message = "Cadastro desbloqueado com sucesso!";
                    if ($action == 'block') {
                        $value = 1;
                        $message = "Cadastro bloqueado com sucesso!";
                        $status = 'paused';
                    }
                    $DataAction->setBloqueado($value)
                        ->setStatus($value);
                    if ($DataSignature) {
                        $DataSignature->setStatus($status);
                        $this->em->getRepository(PersonSignature::class)->save($DataSignature);
                    }
                    break;
                case 2:
                    $message = "Suspensão retirada com sucesso!";
                    $DataAction->setSuspensao(0);
                    break;
                case 3:
                    $message = "Titulo indicado como regular, a partir de hoje todas as contribuições de filiação voltarão a ser gravadas como filiação!";
                    if ($action == 'irregular') {
                        $msg = Messages::titleIndication($DataAction);
                        Email::send($DataAction->getEmail(), $DataAction->getName(), "Aviso, Titulo Eleitoral com Pendência de Regularização!", $msg);
                        $value = 1;
                        $message = "Titulo indicado como pendente, a partir de hoje todas as contribuições de filiação serão gravadas como doação até a regularização!";
                    }
                    $DataAction->setPendenciaTitulo($value);
                    break;
                case 4:
                    $DataAction->setFiliado(6);
                    $message = "Filiado impugnado com sucesso!";
                    $DataSignature->setStatus('paused');
                    $this->em->getRepository(PersonSignature::class)->save($DataSignature);
                    $this->registerHistoric($DataAction, 7);
                    break;
                case 5:
                    $DataAction->setEmailConfirmado(1);
                    $message = "Email confirmado com sucesso!";
                    break;
                case 6:
                    $message = "Cadastro Retirado como membro de Diretório com suceso!";
                    if ($action == 'member') {
                        $message = "Cadastro indicado como membro de Diretório com suceso!";
                        $value = 1;
                    }
                    $DataAction->setMembroDiretorio($value);
                    break;
                case 7:
                    $lastTransaction = $this->em->getRepository(Transaction::class)->getDateLastTransaction($id);
                    $days = date_diff($DataAction->getDataSolicitacaoRefiliacao(), new \Datetime())->days;
                    $affiliationDate = $days > 9 ? new \Datetime($lastTransaction['data_pago']) : $DataAction->getDataSolicitacaoRefiliacao();
                    $DataAction->setFiliado(8)
                        ->setDataRefiliacao($affiliationDate)
                        ->setObservacao(null)
                        ->setUltimaAtualizacao($today);
                    if (!$DataAction->getTokenAd()) $DataAction->setTokenAd(Utils::generateToken(22) . md5(date('Y-m-d H:i:s')));
                    $message = "Refiliação realizada com sucesso!";
                    /*-----Assinatura-----*/
                    $day = date('d');
                    $period = "+{$DataSignature->getPeriodicidade()} month";
                    $dataGeraFatura = date('Y-m', strtotime(date('Y-m-d')));
                    $nextCharge = date("Y-m", strtotime($dataGeraFatura . $period)) . '-' . $day;
                    $explode = explode('-', $nextCharge);
                    $valid = checkdate($explode[1], $day, $explode[0]);
                    while ($valid == false) {
                        $day--;
                        $nextCharge = date("Y-m", strtotime($dataGeraFatura . $period)) . '-' . $day;
                        $explode = explode('-', $nextCharge);
                        $valid = checkdate($explode[1], $day, $explode[0]);
                    }
                    $DataSignature->setStatus('created')
                        ->setDataGeraFatura(new \DateTime($nextCharge))
                        ->setDiaCiclo(date('d'))
                        ->setMesCiclo(date('m'));
                    if ($DataSignature->getPeriodicidade() == 1 && $DataSignature->getValor() < MINMONTHLY) {
                        $DataSignature->setValor(MINMONTHLY);
                    } elseif ($DataSignature->getPeriodicidade() == 12 && $DataSignature->getValor() < MINYEARLY) {
                        $DataSignature->setValor(MINYEARLY);
                    } else if ($DataSignature->getPeriodicidade() == 6 && $DataSignature->getValor() < MINSEMESTER) {
                        $DataSignature->setValor(MINSEMESTER)->setPaymentForm(2)->setGatewayPagamento(1);
                    }
                    $this->em->getRepository(PersonSignature::class)->save($DataSignature);

                    /*-----Hubspot/Mailchimp-----*/
                    $hp = [];
                    $hp['email'] = $DataAction->getEmail();
                    $hp['filiado'] = $DataAction->getFiliadoString();
                    $hp['status_de_filiado'] = $DataAction->getFiliadoString();
                    $hp['status_cadastro'] = $DataAction->getStatusString();
                    $hp['isento'] = $DataAction->getExemption() == 1 ? 'SIM' : 'NÃO';
                    $hp['filiado_id'] = $DataAction->getFiliadoId();
                    $hp['data_filiado'] = $affiliationDate->format('Y-m-d');
                    $hp["state"] = $DataAction->getTituloEleitoralUfId()->getSigla();
                    $hp["city"] = $DataAction->getTituloEleitoralMunicipioId()->getCidade();
                    $hp['tokenpesquisa'] = $DataAction->getTokenAd();
                    Hubspot::updateContact($DataAction, $hp);
                    //Hubspot::insertIntoTheFlow($DataAction, 57289643); // insere no fluxo do Hubspot
                    $body = [];
                    $body['email_address'] = $DataAction->getEmail();
                    $body['merge_fields']['FILIADO'] = $DataAction->getFiliadoString();
                    $body['merge_fields']['MMERGE63'] = $DataAction->getFiliadoString();
                    $body['merge_fields']['FILIADO_ID'] = $DataAction->getFiliadoId();
                    $body['merge_fields']['FILIACAO'] = $affiliationDate->format('d/m/Y');
                    $body['merge_fields']['UF_TITULO'] = $DataAction->getTituloEleitoralUfId()->getSigla();
                    $body['merge_fields']['MUN_TITULO'] = $DataAction->getTituloEleitoralMunicipioId()->getCidade();
                    $body['merge_fields']['PESQUISA'] = $DataAction->getTokenAd();
                    $body['merge_fields']['MMERGE58'] = $DataAction->getStatusString();
                    $body['merge_fields']['ISENTO'] = $DataAction->getExemption() == 1 ? 'SIM' : 'NÃO';
                    $body['merge_fields']['ADDRESS'] = '';
                    Mailchimp::updateContact($body);

                    //dispara mensagem para refiliado
                    $msg = Messages::refiliationConfirmed($DataAction);
                    Email::send($DataAction->getEmail(), $DataAction->getName(), 'Refiliação ao NOVO confirmada', $msg);
                    //avisa setor de filiação
                    $msg = Messages::refiliationConfirmedFiliation($DataAction, date('d/m/Y', strtotime($lastTransaction['data_pago'])));
                    Email::send('refiliacao@novo.org.br', 'Departamento de Filiação', "Nova Refiliação: {$DataAction->getName()}", $msg);

                    //se houve processo, sinaliza a cep
                    $processCEP = $this->em->getRepository(ProcessCep::class)->getProcessCEPCRM($DataAction);
                    if ($processCEP) {
                        $msg = Messages::refiliationConfirmedCEP($DataAction);
                        Email::send('etica@novo.org.br', $DataAction->getName(),
                            "Nova refiliação com processo na CEP - " . $DataAction->getName(), $msg);
                    }

                    $this->registerHistoric($DataAction, 6);
                    $this->checkIndicationOfAffiliation($DataAction, 5);
                    break;
                case 8:
                    $newStatus = $DataAction->getFiliadoId() ? 14 : 2;
                    $DataAction->setFiliado($newStatus);
                    $message = "Impugnação retirada com sucesso";
                    break;
                case 9:
                    $message = "Suspensão aplicada com sucesso!";
                    $period = new \DateInterval("P1D");
                    $DataAction->setSuspensao(1);
                    $this->registerHistoric($DataAction, 8);
                    break;
                case 10:
                    $phone = $this->em->getRepository(Phone::class)->findBy(['tbPessoaId' => $DataAction->getId(), 'tbTipoTelefoneId' => 3]);
                    foreach ($phone as $p) {
                        $p->setContact($action);
                        $this->em->getRepository(Phone::class)->save($p);
                    }
                    $message = "Salvo com sucesso!";
                    break;
                case 11:
                    $message = "Isenção de filiação alterada com sucesso!";
                    $DataAction->setExemption($action)->setIrpf($action);
                    $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $DataAction, 'origin' => 2]);
                    if ($subscription) {
                        if ($action == 1) {
                            if (in_array($DataAction->getFiliado(), [1, 3])) {
                                match ($DataAction->getFiliado()) {
                                    3 => $DataAction->setDataSolicitacaoRefiliacao(new \DateTime())->setFiliado(14),
                                    default => $DataAction->setDataSolicitacaoFiliacao(new \DateTime())->setFiliado(2)
                                };
                                $lastTransaction = $this->em->getRepository(Transaction::class)->getDateLastTransaction($DataAction->getId());
                                $transaction = $this->em->getRepository(Transaction::class)->find($lastTransaction['id']);
                                $transaction->setValor(0.00)
                                    ->setFormaPagamento(0)
                                    ->setGatewayPagamento(0)
                                    ->setinvoiceId('')
                                    ->setStatus('exemption');
                                $this->em->getRepository(Transaction::class)->save($transaction);
                            }
                            $subscription->setPeriodicidade(1)
                                ->setValor(0.00)
                                ->setPaymentForm(0)
                                ->setGatewayPagamento(0)
                                ->setTbDiretorioOrigem(1);
                        } else {
                            $subscription->setPeriodicidade(1)
                                ->setValor(MINMONTHLY)
                                ->setPaymentForm(2)
                                ->setGatewayPagamento(1)
                                ->setTbDiretorioOrigem(1);
                        }
                        $this->em->getRepository(PersonSignature::class)->save($subscription);
                    }
                    break;
                case 12:
                    $days = date_diff($DataAction->getDataSolicitacaoRefiliacao(), new \Datetime())->days;
                    $affiliationDate = $days > 9 ? new \Datetime() : $DataAction->getDataSolicitacaoRefiliacao();
                    /*-----Aprova Refiliação -----*/
                    $DataAction->setFiliado(8)
                        ->setDataRefiliacao($affiliationDate)
                        ->setObservacao('Filiação Isenta')
                        ->setUltimaAtualizacao($today);
                    if (!$DataAction->getTokenAd()) $DataAction->setTokenAd(Utils::generateToken(22) . md5(date('Y-m-d H:i:s')));
                    $message = "Refiliação Isenta realizada com sucesso!";
                    /*-----Assinatura-----*/
                    $day = date('d');
                    $period = "+{$DataSignature->getPeriodicidade()} month";
                    $dataGeraFatura = date('Y-m', strtotime(date('Y-m-d')));
                    $nextCharge = date("Y-m", strtotime($dataGeraFatura . $period)) . '-' . $day;
                    $explode = explode('-', $nextCharge);
                    $valid = checkdate($explode[1], $day, $explode[0]);
                    while ($valid == false) {
                        $day--;
                        $nextCharge = date("Y-m", strtotime($dataGeraFatura . $period)) . '-' . $day;
                        $explode = explode('-', $nextCharge);
                        $valid = checkdate($explode[1], $day, $explode[0]);
                    }
                    $DataSignature->setStatus('created')
                        ->setDataGeraFatura(new \DateTime($nextCharge))
                        ->setDiaCiclo(date('d'))
                        ->setMesCiclo(date('m'))
                        ->setPeriodicidade(1)
                        ->setValor(0.00)
                        ->setPaymentForm(0)
                        ->setGatewayPagamento(0)
                        ->setTbDiretorioOrigem(1);
                    $this->em->getRepository(PersonSignature::class)->save($DataSignature);

                    /*-----Hubspot/Mailchimp-----*/
                    $hp = [];
                    $hp['email'] = $DataAction->getEmail();
                    $hp['filiado'] = $DataAction->getFiliadoString();
                    $hp['status_de_filiado'] = $DataAction->getFiliadoString();
                    $hp['status_cadastro'] = $DataAction->getStatusString();
                    $hp['isento'] = $DataAction->getExemption() == 1 ? 'SIM' : 'NÃO';
                    $hp['filiado_id'] = $DataAction->getFiliadoId();
                    $hp['data_filiado'] = $affiliationDate->format('Y-m-d');
                    $hp["state"] = $DataAction->getTituloEleitoralUfId()->getSigla();
                    $hp["city"] = $DataAction->getTituloEleitoralMunicipioId()->getCidade();
                    $hp['tokenpesquisa'] = $DataAction->getTokenAd();
                    Hubspot::updateContact($DataAction, $hp);
                    //Hubspot::insertIntoTheFlow($DataAction, 57289643); // insere no fluxo do Hubspot
                    $body = [];
                    $body['email_address'] = $DataAction->getEmail();
                    $body['merge_fields']['FILIADO'] = $DataAction->getFiliadoString();
                    $body['merge_fields']['MMERGE63'] = $DataAction->getFiliadoString();
                    $body['merge_fields']['FILIADO_ID'] = $DataAction->getFiliadoId();
                    $body['merge_fields']['FILIACAO'] = $affiliationDate->format('d/m/Y');
                    $body['merge_fields']['UF_TITULO'] = $DataAction->getTituloEleitoralUfId()->getSigla();
                    $body['merge_fields']['MUN_TITULO'] = $DataAction->getTituloEleitoralMunicipioId()->getCidade();
                    $body['merge_fields']['PESQUISA'] = $DataAction->getTokenAd();
                    $body['merge_fields']['MMERGE58'] = $DataAction->getStatusString();
                    $body['merge_fields']['ISENTO'] = $DataAction->getExemption() == 1 ? 'SIM' : 'NÃO';
                    $body['merge_fields']['ADDRESS'] = '';
                    Mailchimp::updateContact($body);

                    //dispara mensagem para refiliado
                    $msg = Messages::refiliationConfirmed($DataAction);
                    Email::send($DataAction->getEmail(), $DataAction->getName(), 'Refiliação ao NOVO confirmada', $msg);
                    //avisa setor de filiação
                    $msg = Messages::refiliationConfirmedFiliation($DataAction, date('d/m/Y'));
                    Email::send('refiliacao@novo.org.br', 'Departamento de Filiação', "Nova Refiliação: {$DataAction->getName()}", $msg);
                    $this->registerHistoric($DataAction, 6);

                    //se houve processo, sinaliza a cep
                    $processCEP = $this->em->getRepository(ProcessCep::class)->getProcessCEPCRM($DataAction);
                    if ($processCEP) {
                        $msg = Messages::refiliationConfirmedCEP($DataAction);
                        Email::send('etica@novo.org.br', $DataAction->getName(),
                            "Nova refiliação com processo na CEP - " . $DataAction->getName(), $msg);
                    }

                    break;
                case 13:
                    $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['user' => $DataAction->getId()], ['id' => 'DESC']);
                    $indication->setStatus(2);
                    $this->em->getRepository(IndicationOfAffiliation::class)->save($indication);
                    $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $DataAction->getId(), 'tbTipoTelefoneId' => 3]);
                    if ($phone) WhatsService::messageIndicationOfAffiliation($DataAction->getfirstName(), $phone->getPhoneFormated(), $indication->getHash());
                    $label = "Para confirmar clique no link a seguir: https://espaco-novo.novo.org.br/confirmacao-de-filiacao/" . $indication->getHash();
                    $msg = Messages::affiliationConfirm($DataAction, $label);
                    Email::send($DataAction->getEmail(), $DataAction->getName(), 'Confirmação de Indicação de Filiação', $msg, '');
                    $DataAction->setObservacao('Indicação de Filiação Confirmada');
                    $message = "Solicitação aprovada com sucesso!";
                    break;
                case 14:
                    /*----- Recusa Refiliação -----*/
                    $DataAction->setFiliado(5)
                        ->setDataCancelamentoPedido($today)
                        ->setObservacao('Refiliação recusada pelo Diretório')
                        ->setUltimaAtualizacao($today);
                    /*----- Pausa Assinatura -----*/
                    $DataSignature->setStatus('paused');
                    $this->em->getRepository(PersonSignature::class)->save($DataSignature);
                    $msg = Messages::refiliationReprovalFiliation($DataAction);
                    Email::send('refiliacao@novo.org.br', 'Departamento de Filiação', "Nova Refiliação Recusada: {$DataAction->getName()}", $msg);
                    $message = "Solicitação reprovada com sucesso, o setor de filiação será avisado da decisão e dará continuidade!";
                    break;
            }
            $DataAction->setStatus(0);
            $this->em->getRepository(User::class)->save($DataAction);
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            //$this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function disaffection(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            Validator::requireValidator(['reasons' => 'Tipo do Motivo'], $data);
            $data['reasons'] = implode((','), $data['reasons']);
            $today = new \DateTime();
            $DataAction = $this->em->getRepository(User::class)->findOneBy(['id' => $data['personId']]);
            $DataAction->setFiliado(9)
                ->setStatus(0)
                ->setDataDesfiliacao($today)
                ->setMotivoDesfiliacao($data['reasons'])
                ->setDescricaoDesfiliacao($data['reason']);
            $this->em->getRepository(User::class)->save($DataAction);
            $DataSignature = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $data['personId']]);
            $DataSignature->setStatus('paused');
            $this->em->getRepository(PersonSignature::class)->save($DataSignature);
            $this->em->getRepository(User::class)->save($DataAction);
            $msgMail2 = Messages::disaffiliation($DataAction);
            Email::send($DataAction->getEmail(), $DataAction->getName(), 'Desfiliação - Partido NOVO', $msgMail2);
            $this->updateStatusRegister($DataAction);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Desfiliação realizada com sucesso, o Desfiliado irá receber um e-mail de confirmação!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            //$this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function reclassification(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $DataAction = $this->em->getRepository(User::class)->findOneBy(['id' => $data['personId']]);
            $DataAction->setReclassificacaoDesfiliacao($data['reclassification']);
            $this->em->getRepository(User::class)->save($DataAction);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Reclassificação realizada com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            //$this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function cep(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $dateBegin = new \Datetime(date("Y-m-d", strtotime(str_replace("/", "-", $data['dateBegin']))));
            $dateInitial = new \Datetime(date("Y-m-d", strtotime(str_replace("/", "-", $data['dateBegin']))));
            if (isset($data['indeterminate'])) {
                $dateFinal = $dateBegin->add(new \DateInterval("P100Y"));
            } else {
                $dateFinal = new \Datetime(date("Y-m-d", strtotime(str_replace("/", "-", $data['dateFinal']))));
            }
            $file = $request->getUploadedFiles() ? $request->getUploadedFiles() : null;
            $way = null;
            if ($file['path_file']->getClientFilename() != "") {
                $file = $file['path_file'];
                $way = 'uploads/comunicados/' . $file->getClientFilename();
                $name = explode(".", $file->getClientFilename());
                $ext = array_pop($name);
                $ext = strtolower($ext);
                if (!in_array($ext, ["pdf", "doc", "docx", "zip"])) {
                    return $response->withJson([
                        'status' => 'error',
                        'message' => "Somente são aceitos arquivos em pdf, doc, docx ou zip!"
                    ])->withStatus(500);
                }

                if (!is_dir("uploads/comunicados/")) {
                    mkdir('uploads/comunicados/', 0777, true);
                }

                $folder = 'uploads/comunicados/';
                $file->moveTo($folder . $file->getClientFilename());
            }
            $DataAction = new ProcessCep();
            $DataAction->setUserAdmin($data['userAdmin'])
                ->setPersonId($data['personId'])
                ->setReason($data['reason'])
                ->setDescription($data['description'])
                ->setDateBegin($dateInitial)
                ->setDateFinal($dateFinal)
                ->setFile($way);
            $this->em->getRepository(ProcessCep::class)->save($DataAction);
            $DataActionUser = $this->em->getRepository(User::class)->findOneBy(['id' => $data['personId']]);
            switch ($data['reason']) {
                case 'Impeachment':
                    $DataActionUser->setFiliado(6);
                    break;
                case 'expulsion':
                    $DataActionUser->setFiliado(11);
                    break;
                case 'suspension':
                    $DataActionUser->setSuspensao(1);
                    break;
                case 'resolution27':
                    $DataActionUser->setSuspensao(1);
                    break;
            }
            $DataActionUser->setStatus(0);
            $this->em->getRepository(User::class)->save($DataActionUser);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Registro salvo com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            //$this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }

    }

    public function confidentialityAgreement(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $report = $request->getAttribute('route')->getArgument('report');
            $this->newAccessLog($user, 'confidentialityAgreement - ' . $report);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Registro salvo com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getName(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $cpf = $request->getAttribute('route')->getArgument('cpf');
            $name = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
            $name = $name ? $name->getName() : '';
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'name' => $name,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getContractCostCenter(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $this->getLogged();
            $contract = $request->getAttribute('route')->getArgument('contract');
            $contract = $this->em->getRepository(Contract::class)->findOneBy(['id' => $contract]);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'costCenter' => $contract ? $contract->getCostCenter()->getId() : '',
                'object' => $contract ? $contract->getObject() : '',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function newUser(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_ACESSOSADMIN_CRIACAO_EDICAO, true);
        $this->newAccessLog($user, 'userAdmin');
        $userId = $userAdmin = null;
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) {
            $userAdmin = $this->em->getRepository(UserAdmin::class)->find($id);
            $userId = $userAdmin->getId();
            //validar aqui a permissao de edição do usuario
            if (($user->getLevel() == UserAdmin::LEVEL_CITY && $user->getCity() != $userAdmin->getCity())
                ||
                ($user->getLevel() == UserAdmin::LEVEL_STATE && $user->getState() != $userAdmin->getState())
            ) {
                $this->redirectByPermissions();
            }
            //fim validacao
        }

        $usersTypes = $this->em->getRepository(UsersTypes::class)->findBy(['active' => true]);
        $usersSectors = $this->em->getRepository(Sectors::class)->findBy(['active' => true]);
        $features = $this->em->getRepository(SystemFeatures::class)->findBy(['active' => 1], ['name' => 'ASC']);

        $featuresLogged = $this->em->getRepository(UserPermissions::class)->getFeaturesByUser($user);

        $states = $this->em->getRepository(State::class)->findAll();
        $cities = $this->em->getRepository(AccessAdmin::class)->getAccess($user);
        $mesoregions = match ($user->getLevel()) {
            3 => $this->em->getRepository(Mesoregions::class)->findAll(),
            default => $this->em->getRepository(Mesoregions::class)->findBy(['state' => $user->getState()->getId()])
        };
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/newUser.phtml',
            'subMenu' => 'records', 'section' => 'users', 'user' => $user, 'usersTypes' => $usersTypes,
            'usersSectors' => $usersSectors, 'features' => $features, 'states' => $states, 'cities' => $cities,
            'mesoregions' => $mesoregions, 'userId' => $userId, 'userAdmin' => $userAdmin,
            'featuresLogged' => $featuresLogged
        ]);
    }

    public function getFeatureByUserSector(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            $features = $this->em->getRepository(PermissionUserTypeFuncionality::class)
                ->findBy(['userType' => $data['userType'], 'sector' => $data['userSector'], 'active' => true]);
            $arr = [];
            foreach ($features as $f) {
                $arr[] = ['feature' => $f->getSystemFeatures()->getId()];
            };
            return $response->withJson([
                'status' => 'ok',
                'message' => $arr,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}