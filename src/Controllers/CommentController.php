<?php 

namespace App\Controllers;
use App\Helpers\Session;
use App\Helpers\Utils;
use App\Models\Entities\Comments;
use App\Models\Entities\CommentStatus;
use App\Models\Entities\UserAdmin;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CommentController extends Controller
{
    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(); 
            $data = (array)$request->getParams(); 
            $today = new \DateTime();
            $status = $this->em->getRepository(CommentStatus::class)->findOneBy(['id' => $data['status']]);
            $DataComment = new Comments();
            $DataComment->setUserAdmin($user)
                        ->setPessoaId($data['pessoaId'])
                        ->setComentario($data['comment'])
                        ->setStatus($status)
                        ->setDataCriacao($today);
            $this->em->getRepository(Comments::class)->save($DataComment);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Comunicado salvo com sucesso",
        ], 201)
            ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}