<?php

namespace App\Controllers;

use App\Helpers\Session;
use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\AccessLog;
use App\Models\Entities\Member;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\User;
use App\Models\Entities\RecoverPassword;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\Transaction;
use App\Models\Entities\UserPermissions;
use App\Services\Auth;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class LoginController extends Controller
{
    public function login(Request $request, Response $response)
    {
        if (Session::get(Session::get('accessType'))) {
            $this->redirect();
        }
        return $this->renderer->render($response, 'login/login.phtml');
    }

    public function autentication(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            Auth::login($data['email'], $data['password']);
            $user = $this->em->getRepository(UserAdmin::class)->findOneBy(['email' => $data['email'], 'active' => 1]);
            if (!$user) throw new \Exception('Usuário ou senha inválidos.');
            $userPessoa = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
            if ($userPessoa) {
                $transactions = $this->em->getRepository(Transaction::class)->listTransactions($data['email']);
                if ($transactions['qtde'] > 0) {
                    throw new \Exception('Acesso bloqueado!<br> Para voltar a ter acesso ao admin,<br> acesse o Espaço NOVO e regularize suas contribuições!<br>Total de contribuições em aberto: ' . $transactions['qtde']);
                }
            }
            $this->newAccessLog($user, '');
            if ($user->getType() == 5) {
                Session::set('diretiva', $user->getId());
                $this->redirect('financeiro/diretiva');
            }
            Session::set('accessType', 'admin');
            Session::set(Session::get('accessType'), $user->getId());
            if ($user->getType() == 7) $this->redirect('beneficios-solicitacoes');
            $redirect = Session::get('redirect');
            $permissions = $this->em->getRepository(UserPermissions::class)->getFeaturesByUser($user);
            Session::set('permissions', $permissions);
            Session::set('userType', $user->getType());
            Session::set('userLevel', $user->getLevel());
            if ($redirect) {
                Session::forgot('redirect');
                $redirect = substr($redirect, 0, 1) == '/' ? substr($redirect, 1) : $redirect;
                $this->redirect($redirect);
                exit;
            }
        } catch (\Exception $e) {
            Session::set('errorMsg', $e->getMessage());
            header("Refresh: 0");
            exit;
        }
        $this->redirect();
    }

    public function logout(Request $request, Response $response)
    {
        Session::forgot('diretiva');
        Session::forgot('admin');
        Session::forgot('permissions');
        Session::forgot('userType');
        Session::forgot('userLevel');
        header("Location: {$this->baseUrl}/login");
        exit;
    }

}
