<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\Sectors;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\User;
use App\Models\Entities\Directory;
use App\Models\Entities\AccessLog;
use App\Models\Entities\DirectoryAreaMeeting;
use App\Models\Entities\DirectoryAreaFinancial;
use App\Models\Entities\DirectoryAreaCommunicated;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\TrailsOfKnowledge;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class DirectoriesAreaController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $permissao = $request->getAttribute('route')->getArgument('window');
        $window = match ($request->getAttribute('route')->getArgument('window')) {
            'minutas' => ['minutes', 'Minutas Contratuais', 'orientations'],
            'comunicacao' => ['communication', 'Comunicação', 'records'],
            'processos-financeiros' => ['financialProcess', 'Processos Financeiros', 'orientations'],
            'legal' => ['legal', 'Jurídico Legal', 'orientations'],
            'loja' => ['store', 'Comunicados da Loja'],
            default => $this->redirectByPermissions()
        };

        if ($permissao == 'legal') Validator::validatePermission(SystemFeatures::ORIENTACOES_JURIDICOLEGAL_VISUALIZACAO, true);
        if ($permissao == 'comunicacao') Validator::validatePermission(SystemFeatures::CADASTROS_COMUNICACAO_VISUALIZACAO, true);
        if ($permissao == 'minutas') Validator::validatePermission(SystemFeatures::ORIENTACOES_MINUTASCONTRATUAIS_VISUALIZACAO, true);
        if ($permissao == 'processos-financeiros') Validator::validatePermission(SystemFeatures::ORIENTACOES_PROCESSOSFINANCEIROS_VISUALIZACAO, true);

        $communicated = $this->em->getRepository(DirectoryAreaCommunicated::class)->list($window[0]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/communicated.phtml',
            'subMenu' => $window[2], 'communicated' => $communicated, 'section' => $window[0], 'user' => $user, 'title' => $window[1]]);
    }

    public function store(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/store.phtml',
            'subMenu' => 'directoryArea', 'section' => 'store', 'user' => $user, 'title' => "Comunicados da Loja"]);
    }

    public function financial(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_RELATORIOFINANCEIRO_VISUALIZACAO, true);
        switch ($user->getLevel()) {
            case 1:
                $details = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'detailing', 'city' => [$user->getCity(), null], 'ball' => ['Municipal', null]], ['report' => 'DESC']);
                break;
            case 2:
                $details = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'detailing', 'state' => [$user->getState(), null], 'ball' => ['Estadual', null]], ['report' => 'DESC']);
                break;
            case 3:
                $details = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'detailing'], ['report' => 'DESC']);
                break;
        }
        $financial2018 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2018']);
        $financial2019 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2019']);
        $financial2020 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2020']);
        $financial2021 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2021']);
        $financial2022 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2022']);
        $financial2023 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2023']);
        $financial2024 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2024']);
        $financial2025 = $this->em->getRepository(DirectoryAreaFinancial::class)->findBy(['tag' => 'financial-2025']);
        $term = $this->em->getRepository(AccessLog::class)->findOneBy(['userAdmin' => $user->getId(), 'menu' => 'confidentialityAgreement - financialReport']);
//		die(var_dump($term->getId()));
        $this->newAccessLog($user, 'financialReport');
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/financial.phtml', 'term' => $term,
            'details' => $details, 'financial2018' => $financial2018, 'financial2019' => $financial2019, 'financial2020' => $financial2020, 'financial2021' => $financial2021,
            'subMenu' => 'directories', 'section' => 'financialReport', 'user' => $user, 'title' => "Relatório Financeiro", 'financial2022' => $financial2022,
            'financial2023' => $financial2023, 'financial2024' => $financial2024, 'financial2025' => $financial2025]);
    }

    public function registerFinancial(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_RELATORIOFINANCEIRO_CRIACAO_EDICAO, true);
        $detail = $request->getAttribute('route')->getArgument('details');
        $directorys = $this->em->getRepository(Directory::class)->findAll();
        $financial = null;
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/registerFinancial.phtml',
            'subMenu' => 'directories', 'financial' => $financial, 'directorys' => $directorys, 'detail' => $detail,
            'section' => 'financialReport', 'user' => $user, 'title' => "Novo Relatório Financeiro"]);
    }

    public function saveFinancial(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $data = (array)$request->getParams();
            $file = $request->getUploadedFiles();
            $way = null;
            if (isset($data['detail'])) {
                $detail = $data['detail'];
            } else {
                $detail = $this->em->getRepository(DirectoryAreaFinancial::class)->findOneBy(['id' => $data['custId']]);
                $detail = $detail->getTag();
            }
            if ($file['path_file']->getClientFilename() != "") {
                $file = $file['path_file'];
                $way = $file->getClientFilename();
                $name = explode(".", $file->getClientFilename());
                $ext = array_pop($name);
                $ext = strtolower($ext);
                if (!in_array($ext, ["pptx", "pdf", "doc", "docx", "zip", "xlsx", "xls", "csv"])) {
                    return $response->withJson([
                        'status' => 'error',
                        'message' => "Somente são aceitos arquivos em pptx, pdf, doc, docx, zip, xlsx, xls ou csv!"
                    ])->withStatus(500);
                }
                $folder = 'uploads/financeiro/area-diretorio/';
                $file->moveTo($folder . $file->getClientFilename());
            }
            $date = new \Datetime;
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $data['directory']]);
            $city = $this->em->getRepository(City::class)->findOneBy(['id' => $directory->getCityId()]);
            $state = $this->em->getRepository(State::class)->findOneBy(['id' => $directory->getStateId()]);
            $financial = $data['custId'] ? $this->em->getRepository(DirectoryAreaFinancial::class)->findOneBy(['id' => $data['custId']]) : $financial = new DirectoryAreaFinancial();
            $financial->setReport($data['title'])
                ->setTag($detail)
                ->setDate($date)
                ->setBall($data['ball'])
                ->setPeriod($data['period'])
                ->setMonth($data['month'])
                ->setDirectory($directory)
                ->setFile($way)
                ->setCity($city)
                ->setState($state);
            $this->em->getRepository(DirectoryAreaFinancial::class)->save($financial);

            return $response->withJson([
                'status' => 'ok',
                'message' => "Material cadastrado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function editFinancial(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_RELATORIOFINANCEIRO_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $financial = $this->em->getRepository(DirectoryAreaFinancial::class)->findOneBy(['id' => $id]);
        $directorys = $this->em->getRepository(Directory::class)->findAll();
        $name = $financial->getReport();
        if ($financial->getReport() == null) {
            switch ($financial->getTag()) {
                case 'financial-2021':
                    $name = 'Relatório Financeiro 2021 - ' . $financial->getMonth();
                    break;
                case 'financial-2020':
                    $name = 'Relatório Financeiro 2020 - ' . $financial->getMonth();
                    break;
                case 'financial-2019':
                    $name = 'Relatório Financeiro 2019 - ' . $financial->getMonth();
                    break;
                case 'financial-2018':
                    $name = 'Relatório Financeiro 2018 - ' . $financial->getMonth();
                    break;
            }
        }

        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/editFinancial.phtml',
            'subMenu' => 'directories', 'name' => $name, 'directorys' => $directorys, 'financial' => $financial, 'id' => $id,
            'section' => 'financialReport', 'user' => $user, 'title' => "Editar Relatório"]);
    }

    public function meetings(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $permission = SystemFeatures::DIRETORIOS_REUNIOES_VISUALIZACAO;
        $section = 'meetings';
        $subMenu = 'directories';
        $sector = $request->getAttribute('route')->getArgument('sector');
        $sectorFilter = 0;
        if ($sector == 'juventude') {
            $permission = SystemFeatures::JUVENTUDE_REUNIOES_VISUALIZACAO;
            $section = 'meetingsYouth';
            $subMenu = 'youth';
            $sectorFilter = 6;
        } elseif ($sector) {
            $this->redirectByPermissions();
        }
        Validator::validatePermission($permission, true);
        $sectors = $this->em->getRepository(Sectors::class)->findBy([], ['name' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/meetings.phtml',
            'subMenu' => $subMenu, 'section' => $section, 'user' => $user, 'title' => "Reuniões gravadas",
            'sectorFilter' => $sectorFilter, 'sectors' => $sectors]);
    }

    public function playVideo(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $permission = SystemFeatures::DIRETORIOS_REUNIOES_VISUALIZACAO;
        $section = 'meetings';
        $subMenu = 'directories';
        $sector = $request->getAttribute('route')->getArgument('sector');
        $sectorFilter = 0;
        if ($sector == 'juventude') {
            $permission = SystemFeatures::JUVENTUDE_REUNIOES_VISUALIZACAO;
            $section = 'meetingsYouth';
            $subMenu = 'youth';
        } elseif ($sector) {
            $this->redirectByPermissions();
        }
        Validator::validatePermission($permission, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $meeting = $this->em->getRepository(DirectoryAreaMeeting::class)->findOneBy(['id' => $id]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/meetingsPlay.phtml',
            'subMenu' => $subMenu, 'media' => $meeting->getMedia(), 'section' => $section, 'user' => $user, 'title' => $meeting->getTheme()]);
    }

    public function communicated(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_COMUNICACAO_CRIACAO_EDICAO, true);
        $page2 = $request->getAttribute('route')->getArgument('page');
        $communicated = null;
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/register.phtml',
            'subMenu' => 'records', 'communicated' => $communicated, 'page2' => $page2,
            'section' => $page2, 'user' => $user, 'title' => "Adicionar novo Comunicado"
        ]);
    }

    public function editRegister(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_COMUNICACAO_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $communicated = $this->em->getRepository(DirectoryAreaCommunicated::class)->findOneBy(['id' => $id]);
        $page2 = $communicated->getPage();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/register.phtml',
            'subMenu' => 'records', 'communicated' => $communicated, 'redirect' => $redirect, 'page2' => $page2, 'section' => 'communication', 'user' => $user, 'title' => "Editar Comunicado"]);
    }

    public function saveMeetings(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $data = (array)$request->getParams();
            $file = $request->getUploadedFiles();
            $file = $file['path_file'];
            $way = null;
            if ($file->getClientFilename()) {
                $way = $file->getClientFilename();
                $name = explode(".", $file->getClientFilename());
                $ext = array_pop($name);
                $ext = strtolower($ext);
                if (!in_array($ext, ["pptx", "pdf", "doc", "docx", "zip", "xlsx", "xls", "csv", "rar"])) {
                    return $response->withJson([
                        'status' => 'error',
                        'message' => "Somente são aceitos arquivos em pptx, pdf, doc, docx, zip, xlsx, xls, csv ou rar!"
                    ])->withStatus(500);
                }
                $folder = 'uploads/reunioes/';
                $file->moveTo($folder . $file->getClientFilename());
            }
            $date = str_replace("/", "-", $data['dateMeeting']);
            $date = date('Y-m-d H:i:s', strtotime($date));
            $meeting = new DirectoryAreaMeeting();
            $meeting->setTheme($data['titleMeeting'])
                ->setSubject($data['resumeMeeting'])
                ->setDate($date)
                ->setSector($this->em->getReference(Sectors::class, $data['sector']))
                ->setFile($way)
                ->setMedia($data['linkMeeting'])
                ->setStatus("Ativado");
            $this->em->getRepository(DirectoryAreaMeeting::class)->save($meeting);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Reunião cadastrada com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveCommunicatedStore(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $data = (array)$request->getParams();
            $file = $request->getUploadedFiles();
            $file = $file['path_file'];
            $way = $file->getClientFilename();
            $name = explode(".", $file->getClientFilename());
            $ext = array_pop($name);
            $ext = strtolower($ext);
            if (!in_array($ext, ["pptx", "pdf", "doc", "docx", "zip", "xlsx", "xls", "csv"])) {
                return $response->withJson([
                    'status' => 'error',
                    'message' => "Somente são aceitos arquivos em pptx, pdf, doc, docx, zip, xlsx, xls!"
                ])->withStatus(500);
            }
            $folder = 'uploads/comunicados/';
            $file->moveTo($folder . $file->getClientFilename());
            $date = new \Datetime;
            $store = new DirectoryAreaCommunicated();
            $store->setDate($date->format('d/m/Y'))
                ->setUserAdmin($userAdmin->getId())
                ->setPage($data['page'])
                ->setTheme($data['title'])
                ->setContent($data['content'])
                ->setFile($way);
            $this->em->getRepository(DirectoryAreaCommunicated::class)->save($store);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Material cadastrado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveCommunicated(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $data = (array)$request->getParams();
            if ($data['id'] > 0) {
                $communicated = $this->em->getRepository(DirectoryAreaCommunicated::class)->findOneBy(['id' => $data['id']]);
            } else {
                $communicated = new DirectoryAreaCommunicated();
            }
            $file = $request->getUploadedFiles() ? $request->getUploadedFiles() : null;
            if ($file['path_file']->getClientFilename() != "") {
                $file = $request->getUploadedFiles();
                $file = $file['path_file'];
                $way = $file->getClientFilename();
                $name = explode(".", $file->getClientFilename());
                $ext = array_pop($name);
                $ext = strtolower($ext);
                if (!in_array($ext, ["pptx", "pdf", "doc", "docx", "zip", "xlsx", "xls", "csv"])) {
                    return $response->withJson([
                        'status' => 'error',
                        'message' => "Somente são aceitos arquivos em pptx, pdf, doc, docx, zip, xlsx, xls ou csv!"
                    ])->withStatus(500);
                }
                $folder = 'uploads/comunicados/';
                $file->moveTo($folder . $file->getClientFilename());
            } else {
                $way = $communicated->getFile();
            }
            $communicated->setDate($data['date'])
                ->setUserAdmin($userAdmin->getId())
                ->setPage($data['page'])
                ->setTheme($data['title'])
                ->setContent($data['content'])
                ->setFile($way)
                ->setLink($data['link']);
            $this->em->getRepository(DirectoryAreaCommunicated::class)->save($communicated);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Material cadastrado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveFile(Request $request, Response $response)
    {
        try {
            $file = $request->getUploadedFiles();
            $file = $file['path_file'];
            $way = $file->getClientFilename();
            $name = explode(".", $file->getClientFilename());
            $ext = array_pop($name);
            $ext = strtolower($ext);
            if (!in_array($ext, ["pptx", "pdf", "doc", "docx", "zip", "xlsx", "xls", "csv"])) {
                return $response->withJson([
                    'status' => 'error',
                    'message' => "Somente são aceitos arquivos em pptx, pdf, doc, docx, zip, xlsx, xls ou csv!"
                ])->withStatus(500);
            }
            $folder = 'uploads/comunicados/';
            $file->moveTo($folder . $file->getClientFilename());
            $way = "https://gestao.novo.org.br/uploads/comunicados/" . $file->getClientFilename();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Upload de arquivo realizado com sucesso,o link para compartilhamento é: " . $way,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function listStore(Request $request, Response $response)
    {
        $this->getLogged(true);
        $index = $request->getQueryParam('index');
        $limit = $request->getQueryParam('limit');
        $stores = $this->em->getRepository(DirectoryAreaCommunicated::class)->listStore($request->getQueryParams(), $index * $limit);
        $total = $this->em->getRepository(DirectoryAreaCommunicated::class)->listTotalStore($request->getQueryParams())['total'];
        $partial = ($index * $limit) + sizeof($stores);
        $partial = $partial <= $total ? $partial : $total;
        return $response->withJson([
            'status' => 'ok',
            'message' => $stores,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function trailOfKnowledge(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::DIRETORIOS_TRILHASDOCONHECIMENTO_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/trailOfKnowledge.phtml',
            'section' => 'trails', 'user' => $user, 'title' => "Trilhas do Conhecimentos", 'subMenu' => 'directories']);
    }

    public function trailOfKnowledgeClass(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $module = $request->getAttribute('route')->getArgument('module');
            $c = $this->em->getRepository(TrailsOfKnowledge::class)->findBy(['module' => $module]);
            $class = [];
            foreach ($c as $c) {
                $class[] = [
                    'id' => $c->getId(),
                    'name' => $c->getTitle()
                ];
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $class,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function localDocuments(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_DOCUMENTOSLOCAIS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'directories-area/localDocuments.phtml',
            'subMenu' => 'records', 'section' => 'localDocuments', 'user' => $user, 'title' => "Documentos Locais",
            'states' => $states]);
    }
}

?>