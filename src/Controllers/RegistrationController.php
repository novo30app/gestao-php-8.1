<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Entities\AccessLevel;
use App\Models\Entities\City;
use App\Models\Entities\DirectoryRegister;
use App\Models\Entities\Elected;
use App\Models\Entities\Leaders;
use App\Models\Entities\Occupation;
use App\Models\Entities\Post;
use App\Models\Entities\RecoverPassword;
use App\Models\Entities\State;
use App\Models\Entities\Country;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\CivilState;
use App\Models\Entities\User;
use App\Models\Entities\Phone;
use App\Models\Entities\Address;
use App\Models\Entities\Mesoregions;
use App\Models\Entities\MesoregionsCities;
use App\Models\Entities\TrailsOfKnowledge;
use App\Models\Entities\LocalDocuments;
use App\Models\Entities\CommentStatus;
use App\Models\Entities\Comments;
use App\Models\Entities\Directory;
use App\Models\Entities\Sectors;
use App\Models\Entities\SystemFeatures;
use App\Models\Entities\UsersTypes;
use App\Models\Entities\PermissionUserTypeFuncionality;
use App\Models\Entities\UserPermissions;
use App\Models\Entities\AccessAdmin;
use App\Services\Email;
use App\Services\Hubspot;
use App\Services\Auth;
use Doctrine\DBAL\Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class RegistrationController extends Controller
{

    public function leaders(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $levels = $this->em->getRepository(AccessLevel::class)->findBy([], ['name' => 'asc']);
        $occupations = $this->em->getRepository(Occupation::class)->findBy([], ['id' => 'asc']);
        $states = $this->em->getRepository(State::class)->findAll();
        $directory = $request->getAttribute('route')->getArgument('directory');
        $directory = $directory ? $directory : 0;
        return $this->renderer->render($response, 'default.phtml', ['page' => 'registration/leaders.phtml',
            'subMenu' => 'maintenance', 'section' => 'leaders', 'user' => $user, 'occupations' => $occupations,
            'levels' => $levels, 'states' => $states, 'directory' => $directory]);
    }

    public function saveLeaders(Request $request, Response $response)
    {
        try {
            $this->getLogged(true);
            $data = (array)$request->getParams();
                $data['id'] ?? 0;
            $fields = [
                'name' => 'Nome',
                'email' => 'Email',
                'occupation' => 'Cargo',
                'level' => 'Nível',
                'status' => 'Status'
            ];
            Validator::requireValidator($fields, $data);
            Validator::validateCPF($data['cpf']);
            $leader = new Leaders();
            if ($data['id'] > 0) {
                $leader = $this->em->getRepository(Leaders::class)->find($data['id']);
            }
            $leader->setName($data['name'])
                ->setCpf($data['cpf'])
                ->setEmail($data['email'])
                ->setOccupation($this->em->getReference(Occupation::class, $data['occupation']))
                ->setStartDate(Utils::convetStringToDateTime($data['startDate']))
                ->setEndDate(Utils::convetStringToDateTime($data['endDate']))
                ->setLevel($this->em->getReference(AccessLevel::class, $data['level']))
                ->setStatus((int)$data['status']);
            if ($data['city']) {
                $leader->setCity($this->em->getReference(City::class, $data['city']));
            }
            if ($data['state']) {
                $leader->setState($this->em->getReference(State::class, $data['state']));
            }
            if ($data['level'] == AccessLevel::LEVEL_NATIONAL) {
                $leader->setCity(null)
                    ->setState(null);
            }
            $this->em->getRepository(Leaders::class)->save($leader);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Dirigente cadastrado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function directories(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_DIRETORIOS_VISUALIZACAO, true);
        $levels = $this->em->getRepository(AccessLevel::class)->findBy([], ['name' => 'asc']);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'registration/directories.phtml',
            'subMenu' => 'search', 'levels' => $levels, 'section' => 'directories', 'user' => $user, 'states' => $states]);
    }

    public function saveDirectories(Request $request, Response $response)
    {
        try {
            $this->getLogged(true);
            $data = (array)$request->getParams();
                $data['directoryId'] ?? 0;
            $fields = [
                'email' => 'Email',
                'status' => 'Status',
                'state' => 'Estado',
                'level' => 'Nível'
            ];
            if ($data['level'] == 1) {
                $fields['city'] = 'Cidade';
            }
            Validator::requireValidator($fields, $data);
            $directory = new DirectoryRegister();
            if ($data['directoryId'] > 0) {
                $directory = $this->em->getRepository(DirectoryRegister::class)->find($data['directoryId']);
            }
            $directory->setEmail($data['email'])
                ->setActive($data['status'])
                ->setState($this->em->getReference(State::class, $data['state']))
                ->setLevel($this->em->getReference(AccessLevel::class, $data['level']))
                ->setCity(null);
            if ($data['level'] == 1) {
                $directory->setCity($this->em->getReference(City::class, $data['city']));
            }
            $this->em->getRepository(DirectoryRegister::class)->save($directory);
            $balanceCashFlow = Utils::moneyToFloat($data['balanceCashFlow']);
            $dir = $this->em->getRepository(Directory::class)->findOneBy(['email' => $data['email']]);
            if ($dir) {
                $dir->setBalanceCashFlow($balanceCashFlow);
                $this->em->getRepository(Directory::class)->save($dir);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Diretório cadastrado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function managementRecords(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/managementRecords.phtml', 'section' => 'managementRecords',
            'user' => $user, 'states' => $states, 'subMenu' => 'maintenance', 'title' => 'Gestão de Cadastros']);
    }

    public function managementCommunicated(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_GESTAODECOMUNICADOS_VISUALIZACAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/managementCommunicated.phtml',
            'section' => 'managementCommunicated', 'user' => $user, 'states' => $states, 'subMenu' => 'records', 'title' => 'Gestão de Comunicados']);
    }

    public function newRegister(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CONSULTAS_GESTAODECADASTROS_CRIACAO_EDICAO, true);
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        $genders = $this->em->getRepository(Gender::class)->findAll();
        $schooling = $this->em->getRepository(Schooling::class)->findAll();
        $civilStatus = $this->em->getRepository(CivilState::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'registration/newRegister.phtml', 'section' => 'managementRecords', 'countries' => $countries,
            'genders' => $genders, 'schooling' => $schooling, 'civilStatus' => $civilStatus, 'user' => $user, 'states' => $states, 'subMenu' => 'search']);
    }

    public function saveNewRegister(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'Nome',
                'email' => 'E-mail',
                'birth' => 'Data Nascimento',
                'cpf' => 'CPF',
                'cellPhone' => 'Telefone celular'
            ];
            Validator::requireValidator($fields, $data);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);
            if ($user) {
                throw new \Exception("Esta pessoa já está cadastrada na Base de dados!");
            }
            $birth = new \DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $data['birth']))));
            $civilState = $this->em->getRepository(CivilState::class)->findOneBy(['id' => 6]);
            $gender = $this->em->getRepository(Gender::class)->findOneBy(['id' => 3]);
            $education = $this->em->getRepository(Schooling::class)->findOneBy(['id' => 7]);
            $country = $this->em->getRepository(Country::class)->findOneBy(['id' => 33]);
            //salva pessoa
            $user = new User();
            $user->setName($data['name'])
                ->setEmail($data['email'])
                ->setDataNascimento($birth)
                ->setRg($data['rg'])
                ->setCpf($data['cpf'])
                ->setEstadoCivil($civilState)
                ->setGenero($gender)
                ->setEscolaridade($education)
                ->setTituloEleitoralPaisId($country)
                ->setTermoAceite0(1)
                ->setTermoAceite1(1)
                ->setTermoAceite2(1)
                ->setTermoAceite3(1)
                ->setDatacriacao(new \DateTime())
                ->setFiliado(0)
                ->setStatus(0)
                ->setEmailConfirmado(0)
                ->setPendenciaTitulo(0)
                ->setMembroDiretorio(0);
            $this->em->getRepository(User::class)->save($user)->getId();

            /*-----Hubspot/Mailchimp-----*/
            Hubspot::createContact($user);
            $this->mailchimpCreateUser($user, null, $data['cellPhone']);

            //salva telefone
            $phone = new Phone();
            $phone->setTbPessoaId($user->getId())
                ->setTbTipoTelefoneId(3)
                ->setPais('br')
                ->setDdi(55)
                ->setTelefone($data['cellPhone']);
            $this->em->getRepository(Phone::class)->save($phone);
            //salva endereco
            if ($data['zipCode']) {
                $fields = [
                    'zipCode' => 'Cep',
                    'ufId' => 'Estado',
                    'cityId' => 'Cidade',
                    'street' => 'Endereço',
                    'number' => 'Número',
                    'district' => 'Bairro',
                ];
                $complement = $data['complement'] ?: null;
                Validator::requireValidator($fields, $data);
                $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data['ufId']]);
                $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => $data['cityId']]);
                $address = new Address();
                $address->setTbPessoaId($user->getId())
                    ->setTbTipoEnderecoId(3)
                    ->setResideExterior(0)
                    ->setCountryId(33)
                    ->setStateId($state->getId())
                    ->setEstado($state->getEstado())
                    ->setCityId($city->getId())
                    ->setCidade($city->getCidade())
                    ->setCep($data['zipCode'])
                    ->setCepStatus(2)
                    ->setEndereco($data['street'])
                    ->setNumero($data['number'])
                    ->setComplemento($complement)
                    ->setBairro($data['district']);
                $this->em->getRepository(Address::class)->save($address);
            }
            $firstName = explode(" ", $data['name']);
            $msg = "Olá " . $firstName[0] . ",<br>
            Você foi cadastrado para acessar o Espaço NOVO,<br>
            para configurar sua senha <a href='http://localhost/espaco-novo/recuperar-senha'>Clique aqui!</a><br>
            Qualquer dúvida entre em contato conosco através do e-mail <a href='mailto:filiacao@novo.org.br'>filiacao@novo.org.br</a>;";
            Email::send($data['email'], $data['name'], 'Novo acesso - Espaço NOVO', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Registro salvo com sucesso, um e-mail foi enviado para o mesmo configurar a senha do Espaço NOVO!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveMesoregion(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            if ($user->getLevel() == 1) throw new \Exception("Você não tem permissão para criar Mesorregiões");
            $data = (array)$request->getParams();
            Validator::requireValidator(
                $fields = [
                    'mesoName' => 'Nome Mesorregião',
                    'cities' => 'Cidade'
                ], $data);
            $meso = $this->em->getRepository(Mesoregions::class)->findOneBy(['id' => $data['mesoId']]);
            if (!$data['cities']) throw new \Exception("Selecione pelo menos uma cidade");
            if (!$meso) $meso = new Mesoregions();
            $meso->setName($data['mesoName'])
                ->setUser($this->em->getReference(UserAdmin::class, $user->getId()))
                ->setCreatedAt(new \Datetime());
            match ($user->getLevel()) {
                2 => $meso->setState($this->em->getReference(State::class, $user->getState()->getId())),
                3 => $meso->setState($this->em->getReference(State::class, $data['state']))
            };
            $this->em->getRepository(Mesoregions::class)->save($meso);
            if ($meso) $this->em->getRepository(MesoregionsCities::class)->deleteCities($meso->getId());
            foreach ($data['cities'] as $city) {
                $check = $this->em->getRepository(MesoregionsCities::class)->findOneBy(['city' => $city]);
                if ($check) {
                    $city = $this->em->getRepository(City::class)->find($city);
                    throw new \Exception("A cidade de '{$city->getCidade()}' já faz parte da meso região de '{$check->getMesoregion()->getName()}'.");
                }
                $mesoCities = new MesoregionsCities();
                $mesoCities->setMesoregion($meso)->setCity($this->em->getReference(City::class, $city));
                $this->em->getRepository(MesoregionsCities::class)->save($mesoCities);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Mesorregião cadastrada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function getMesos(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $m = $this->em->getRepository(Mesoregions::class)->findBy(['state' => $id]);
        $mesos = [];
        foreach ($m as $meso) {
            $mesos[] = [
                'id' => $meso->getId(),
                'name' => $meso->getName()
            ];
        }
        return $response->withJson([
            'status' => 'ok',
            'mesos' => $mesos
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function saveTrailOfKnowledge(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator(
                [
                    'title' => 'Titulo',
                    'module' => 'Módulo',
                    'link' => 'Link',
                    'description' => 'Descrição'
                ], $data
            );
            $msg = 'Trilha salva com sucesso!';
            $date = new \Datetime;
            $trail = new TrailsOfKnowledge();
            if ($data['trail'] > 0) {
                $trail = $this->em->getRepository(TrailsOfKnowledge::class)->findOneBy(['id' => $data['trail']]);
                $msg = 'Trilha editada com sucesso!';
            }
            $folder = UPLOAD_FOLDER . 'trilhas/';
            $file = $request->getUploadedFiles();
            $file = $file['material'];
            if ($file && $file->getClientFilename()) {
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}trails.{$extension}";
                $file->moveTo($target);
                $trail->setMaterial($target);
            }
            $trail->setModule((int)$data['module'])
                ->setTitle($data['title'])
                ->setDescription($data['description'])
                ->setLink($data['link'])
                ->setActive(true);
            $this->em->getRepository(TrailsOfKnowledge::class)->save($trail);
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function updateTrailOfKnowledge(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $trails = $this->em->getRepository(TrailsOfKnowledge::class)->findOneBy(['id' => $id]);
            $trails->setActive(false);
            $this->em->getRepository(TrailsOfKnowledge::class)->save($trails);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Removido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getTrailOfKnowledge(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $t = $this->em->getRepository(TrailsOfKnowledge::class)->findOneBy(['id' => $id]);
            $trail = [
                'id' => $t->getId(),
                'module' => $t->getModule(),
                'class' => $t->getTitle(),
                'description' => $t->getDescription(),
                'link' => $t->getLink(),
                'material' => $t->getMaterial()
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $trail,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveLocalDocuments(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator(
                [
                    'title' => 'Titulo',
                    'state' => 'Estado',
                    'description' => 'Descrição'
                ], $data
            );
            $msg = 'Documento salvo com sucesso!';
            $date = new \Datetime;
            $document = new LocalDocuments();
            if ($data['document'] > 0) {
                $document = $this->em->getRepository(LocalDocuments::class)->findOneBy(['id' => $data['document']]);
                $msg = 'Documento editado com sucesso!';
            }
            $folder = UPLOAD_FOLDER . 'documentosLocais/';
            $file = $request->getUploadedFiles();
            $file = $file['document'];
            if ($file && $file->getClientFilename()) {
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}Documents.{$extension}";
                $file->moveTo($target);
                $document->setFile($target);
            }
            if ($data['city']) $document->setCity($data['city']);
            $document->setAuthor($user->getId())
                ->setTitle($data['title'])
                ->setDescription($data['description'])
                ->setState($data['state'])
                ->setActive(true);
            $this->em->getRepository(LocalDocuments::class)->save($document);
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getLocalDocument(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $d = $this->em->getRepository(LocalDocuments::class)->findOneBy(['id' => $id]);
            $document = [
                'id' => $d->getId(),
                'title' => $d->getTitle(),
                'state' => $d->getState(),
                'city' => $d->getCity(),
                'file' => $d->getFile(),
                'description' => $d->getDescription()
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $document,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function updateLocalDocument(Request $request, Response $response)
    {
        try {
            $userAdmin = $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            if (!$id) throw new \Exception("Solicitação Inválida!");
            $document = $this->em->getRepository(LocalDocuments::class)->findOneBy(['id' => $id]);
            $document->setActive(false);
            $this->em->getRepository(LocalDocuments::class)->save($document);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Removido com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function chatbotSave(Request $request, Response $response)
    {
        try {
            $header = $request->getHeaders();
            $token = 'znQwze9qj2n50OhznQwze9uLKiG';
            if (!isset($header['HTTP_TOKEN'][0]) || $header['HTTP_TOKEN'][0] != $token) die('token invalido');
            $data = (array)$request->getParams();
            Validator::requireValidator(['comment' => 'Comentário'], $data);
            $cpf = Utils::onlyNumbers($data['cpf'] ?? '');
            Validator::validateCPF($cpf);
            $cpf = Utils::formatCpf($cpf);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
            if (!$user) throw new \Exception('Usuário não encontrado');
            $commentStatus = $this->em->getRepository(CommentStatus::class)->findOneBy(['id' => 20]);
            $DataComment = new Comments();
            $DataComment->setUserAdmin($this->em->getReference(UserAdmin::class, 1274))
                ->setPessoaId($user->getId())
                ->setComentario($data['comment'])
                ->setStatus($commentStatus)
                ->setDataCriacao(new \Datetime());
            $this->em->getRepository(Comments::class)->save($DataComment);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Comentário cadastrado com sucesso!!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function elected(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll();
        $posts = $this->em->getRepository(Post::class)->findAll();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'registration/elected.phtml',
            'subMenu' => 'elected', 'user' => $user, "states" => $states, 'posts' => $posts]);
    }

    public function electedSave(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator(
                [
                    'name' => 'Nome',
                    'email' => 'E-mail',
                    'state' => 'Estado',
                    'cpf' => 'CPF',
                    'post' => 'Cargo',
                    'year' => 'Ano'
                ], $data
            );
            $date = new \Datetime;
            $elected = $this->em->getRepository(Elected::class)->findOneBy(['cpf' => $data['cpf']]);
            if ($elected && $data['electedId'] == "") throw new \Exception("Eleito já cadastrado");
            $elected = new Elected();
            $elected->setCreatedAt($date)
                ->setUser($user)
                ->setName($data['name'])
                ->setEmail($data['email'])
                ->setCPF($data['cpf'])
                ->setYear($data['year'])
                ->setState($this->em->getReference(State::class, $data['state']))
                ->setPost($this->em->getReference(Post::class, $data['post']))
                ->setStatus(true);
            if ($data['city']) $elected->setCity($this->em->getReference(City::class, $data['city']));
            if ($data['votes']) $elected->setVotes($data['votes']);
            $this->em->getRepository(Elected::class)->save($elected);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function electedRemove(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $e = $this->em->getRepository(Elected::class)->find($id);
            $e->setStatus(false);
            $this->em->getRepository(Elected::class)->save($e);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Removido com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function electedImport(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $this->em->beginTransaction();

            $file = $request->getUploadedFiles();
            $file = $file['file'];
            $date = new \Datetime;

            if ($file && $file->getClientFilename()) {
                $folder = BASEURL . '\uploads\eleitos/';
                $time = time();
                $extension = explode('.', $file->getClientFilename());
                $extension = end($extension);
                if ($extension != 'csv') throw new \Exception("Formato inválido, verifique se a planilha está em .CSV");
                $target = "{$folder}{$time}.{$extension}";
                $file->moveTo($target);
            }

            if (($handle = fopen($target, "r")) !== FALSE) {
                $separator = ',';
                $data = fgetcsv($handle, 100, ",");
                if (count($data) == 1) $separator = ';';
                $count = 0;
                while (($data = fgetcsv($handle, 100, $separator)) !== FALSE) {
                    if ($count <= 100) {

                        $elected = $this->em->getRepository(Elected::class)->findOneBy(['cpf' => $data[2]]);
                        if ($elected) throw new \Exception("Eleito {$elected->getName()} já está cadastrado.");

                        $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data[5]]);
                        if (!$state) throw new \Exception("Verifique se digitou todos os nomes dos estados corretamente.");

                        $city = $this->em->getRepository(City::class)->findOneBy(['state' => $state->getId(), 'cidade' => utf8_encode($data[6])]);
                        if (!$city) throw new \Exception("Verifique se digitou todos os nomes das cidades corretamente.");

                        $post = match ($data[3]) {
                            'Deputado Estadual' => 1,
                            'Deputado Federal' => 2,
                            'Governador' => 3,
                            'Prefeito' => 4,
                            'Presidente' => 5,
                            'Senador' => 6,
                            'Vereador' => 7,
                            'Suplente Senador' => 8,
                            'Vice Governador' => 9,
                            'Vice Presidente' => 10
                        };

                        $e = new Elected();
                        if ($data[0] != '') {
                            $e->setCreatedAt($date)
                                ->setUser($user)
                                ->setName($data[0])
                                ->setEmail($data[1])
                                ->setCPF($data[2])
                                ->setPost($post)
                                ->setYear($data[4])
                                ->setState($state)
                                ->setCity($city)
                                ->setStatus(1)
                                ->setVotes($data[7]);
                            $this->em->getRepository(Elected::class)->save($e);
                        }
                    } else {
                        throw new \Exception("Importe no máximo 100 registros por vêz.");
                    }
                    $count++;
                }
                fclose($handle);
            }

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Eleitos importados com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function sectors(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_SETORES_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/sectors.phtml',
            'subMenu' => 'records', 'section' => 'sectors', 'user' => $user]);
    }

    public function sectorSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator(['name' => 'Nome'], $data);
            if ($data['sectorId'] > 0) {
                $sector = $this->em->getRepository(Sectors::class)->findOneBy(['id' => $data['sectorId']]);
            } else {
                $sector = $this->em->getRepository(Sectors::class)->findOneBy(['name' => $data['name']]);
                if ($sector) throw new \Exception("Este setor já existe");
                $sector = new Sectors();
                $sector->setUser($user);
            }
            $sector->setName($data['name'])
                ->setActive(true);
            $this->em->getRepository(Sectors::class)->save($sector);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function sectorRemove(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $s = $this->em->getRepository(Sectors::class)->find($id);
            $s->setActive(false);
            $this->em->getRepository(Sectors::class)->save($s);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Removido com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function systemFeatures(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_FUNCIONALIDADESDOSISTEMA_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/systemFeatures.phtml',
            'subMenu' => 'records', 'section' => 'systemFeatures', 'user' => $user]);
    }

    public function featureSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator(['name' => 'Nome'], $data);
            if ($data['featureId'] > 0) {
                $feature = $this->em->getRepository(SystemFeatures::class)->findOneBy(['id' => $data['featureId']]);
            } else {
                $feature = $this->em->getRepository(SystemFeatures::class)->findOneBy(['name' => $data['name']]);
                if ($feature) throw new \Exception("Esta funcionalidade já existe");
                $feature = new SystemFeatures();
                $feature->setUser($user);
            }
            $feature->setUser($user)
                ->setName($data['name'])
                ->setActive(true);
            $this->em->getRepository(SystemFeatures::class)->save($feature);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function featureRemove(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $s = $this->em->getRepository(SystemFeatures::class)->find($id);
            $s->setActive(false);
            $this->em->getRepository(SystemFeatures::class)->save($s);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Removido com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function usersType(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_TIPOSDEUSUARIOS_VISUALIZACAO, true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/usersType.phtml',
            'subMenu' => 'records', 'section' => 'usersType', 'user' => $user]);
    }

    public function userTypeSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator(['name' => 'Nome',], $data);
            if ($data['userTypeId'] > 0) {
                $userType = $this->em->getRepository(UsersTypes::class)->findOneBy(['id' => $data['userTypeId']]);
            } else {
                $userType = $this->em->getRepository(UsersTypes::class)->findOneBy(['name' => $data['name']]);
                if ($userType) throw new \Exception("Este tipo de usuário já existe");
                $userType = new UsersTypes();
                $userType->setUser($user);
            }
            $userType->setUser($user)
                ->setName($data['name'])
                ->setActive(true);
            $this->em->getRepository(UsersTypes::class)->save($userType);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function userTypeRemove(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $s = $this->em->getRepository(UsersTypes::class)->find($id);
            $s->setActive(false);
            $this->em->getRepository(UsersTypes::class)->save($s);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Removido com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function permissions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_PERMISSOES_VISUALIZACAO, true);
        $usersTypes = $this->em->getRepository(UsersTypes::class)->findBy(['active' => 1]);
        $sectors = $this->em->getRepository(Sectors::class)->findBy(['active' => 1], ['name' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/permissions.phtml',
            'subMenu' => 'records', 'section' => 'permissions', 'user' => $user, 'usersTypes' => $usersTypes,
            'sectors' => $sectors]);
    }

    public function permissionsDetails(Request $request, Response $response)
    {
        $user = $this->getLogged();
        Validator::validatePermission(SystemFeatures::CADASTROS_PERMISSOES_CRIACAO_EDICAO, true);
        $id = $request->getAttribute('route')->getArgument('id');
        $arr = [];
        if ($id) {
            $permission = $this->em->getRepository(PermissionUserTypeFuncionality::class)->find($id);
            $featuresByPermission = $this->em->getRepository(PermissionUserTypeFuncionality::class)->featuresByPermission($permission);
            foreach ($featuresByPermission as $f) {
                $arr[] = $f['systemFeature'];
            }
        }
        $usersTypes = $this->em->getRepository(UsersTypes::class)->findBy(['active' => 1]);
        $sectors = $this->em->getRepository(Sectors::class)->findBy(['active' => 1], ['name' => 'ASC']);
        $features = $this->em->getRepository(SystemFeatures::class)->findBy(['active' => 1], ['name' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'maintenance/permissionsDetails.phtml', 'id' => $id,
            'subMenu' => 'records', 'section' => 'permissions', 'user' => $user, 'permission' => $permission,
            'usersTypes' => $usersTypes, 'sectors' => $sectors, 'features' => $features, 'arr' => $arr]);
    }

    public function permissionSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'userType' => 'Tipo de usuário',
                'sector' => 'Setor',
                'features' => 'Funcionalidades',
            ], $data);
            $oldRegister = $this->em->getRepository(PermissionUserTypeFuncionality::class)->findOneBy(['active' => 1, 'sector' => $data['sector'], 'userType' => $data['userType']]);
            if (!$data['permissionId'] && $oldRegister) {
                throw new \Exception('Esse tipo de usuário e setor já está cadastro, edite o cadastro atual');
            }
            $this->em->getRepository(PermissionUserTypeFuncionality::class)->inactiveByUserTypeAndSector($data['userType'], $data['sector']);
            foreach ($data['features'] as $feature) {
                $permission = new PermissionUserTypeFuncionality();
                $permission->setUser($user)
                    ->setUserType($this->em->getReference(UsersTypes::class, $data['userType']))
                    ->setSystemFeatures($this->em->getReference(SystemFeatures::class, $feature))
                    ->setSector($this->em->getReference(Sectors::class, $data['sector']))
                    ->setActive(true);
                $this->em->getRepository(PermissionUserTypeFuncionality::class)->save($permission);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Salvo com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function permissionRemove(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = $request->getAttribute('route')->getArgument('id');
            $p = $this->em->getRepository(PermissionUserTypeFuncionality::class)->find($id);
            $featuresByPermission = $this->em->getRepository(PermissionUserTypeFuncionality::class)->featuresByPermission($p);
            foreach ($featuresByPermission as $f) {
                $p = $this->em->getRepository(PermissionUserTypeFuncionality::class)->find($f['id']);
                $p->setActive(false);
                $this->em->getRepository(PermissionUserTypeFuncionality::class)->save($p);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Removido com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function usersPermissionSave(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();

            $user = $this->getLogged(true);
            $data = (array)$request->getParams();
            $adminAccess = isset($data['adminAccess']) ? 1 : 0;
            $exportAccess = isset($data['exportAccess']) ? 1 : 0;
            $financialReport = isset($data['financialReport']) ? 1 : 0;
            $directoryAreaAccess = isset($data['directoryAreaAccess']) ? 1 : 0;
            $events = isset($data['events']) ? 1 : 0;
            $libertasCourse = isset($data['libertasCourses']) ? 1 : 0;
//            Validator::validateAccess($adminAccess, $directoryAreaAccess, $financialReport, $events, $libertasCourse);
            $fields = [
                'name' => 'Nome',
                'email' => 'E-mail',
                'userType' => 'Tipo de usuário',
                'features' => 'Funcionalidades',
                'level' => 'Nível',
                'status' => 'Status',
                'typeView' => 'Visualização',
                'userSector' => 'Setor',
            ];
            if ($data['level'] == 2) {
                $fields['state'] = 'Estado';
            } elseif ($data['level'] == 1) {
                $fields['state'] = 'Estado';
                $fields['city'] = 'Cidade';
            }
            Validator::requireValidator($fields, $data);

            $oldEmail = '';
            if ($data['userId'] == "") {
                $userCheck = $this->em->getRepository(UserAdmin::class)->findOneBy(['email' => $data['email']]);
                if ($userCheck) throw new \Exception("Usuário já cadastrado!");
                $userSave = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if (!$user->isAdminNational() && !$userSave) throw new \Exception("E-mail não encontrado, utilize o e-mail cadastrado no Espaço NOVO!");
                $userAdmin = new UserAdmin();
            } else {
                $userAdmin = $this->em->getRepository(UserAdmin::class)->findOneBy(['id' => $data['userId']]);
                $oldEmail = $userAdmin->getEmail();
            }

            $data['level'] = $data['level'] <= $user->getLevel() ? $data['level'] : $user->getLevel(); // garantir que o nivel do usuario é menor ou igual ao do admin
            $data['status'] ??= 0;

            $userAdmin->setActive($data['status'])
                ->setLevel($data['level'])
                ->setType($data['userType'])
                ->setName($data['name'])
                ->setEmail($data['email'])
                ->setTypeView($data['typeView'])
                ->setAdminAccess($adminAccess)
                ->setSector($this->em->getReference(Sectors::class, $data['userSector']))
                ->setDirectoryAreaAccess($directoryAreaAccess)
                ->setExportAccess($exportAccess)
                ->setFinancialReport($financialReport)
                ->setEvents($events)
                ->setLibertasCourses($libertasCourse);
            if ($data['city']) $userAdmin->setCity($this->em->getReference(City::class, $data['city']));
            if ($data['state']) $userAdmin->setState($this->em->getReference(State::class, $data['state']));
            if ($data['level'] == UserAdmin::LEVEL_NATIONAL) $userAdmin->setCity(null)->setState(null);
            $this->em->getRepository(UserAdmin::class)->save($userAdmin);

            if ($data['level'] == 1 && $user->getLevel() != 1) {
                $this->em->getRepository(AccessAdmin::class)->dropAccess($userAdmin->getId(), 'city');
                $this->em->getRepository(AccessAdmin::class)->dropAccess($userAdmin->getId(), 'meso');
                $accessAdminCity = new AccessAdmin();
                $accessAdminCity->setUserAdmin($userAdmin->getId())->setType('city')->setAccess($data['city']);
                $this->em->getRepository(AccessAdmin::class)->save($accessAdminCity);
                if (isset($data['cities'])) {
                    foreach ($data['cities'] as $city) {
                        $accessAdmin = new AccessAdmin();
                        $accessAdmin->setUserAdmin($userAdmin->getId())->setType('city')->setAccess($city);
                        $this->em->getRepository(AccessAdmin::class)->save($accessAdmin);
                    }
                }
                if (isset($data['mesos'])) {
                    foreach ($data['mesos'] as $meso) {
                        $accessAdmin = new AccessAdmin();
                        $accessAdmin->setUserAdmin($userAdmin->getId())->setType('meso')->setAccess($meso);
                        $this->em->getRepository(AccessAdmin::class)->save($accessAdmin);
                    }
                }
            }

            $this->em->getRepository(UserPermissions::class)->deleteFeatures($userAdmin);
            foreach ($data['features'] as $f) {
                $newFeature = new UserPermissions();
                $newFeature->setAdmin($user)
                    ->setUser($userAdmin)
                    ->setSystemFeatures($this->em->getReference(SystemFeatures::class, $f));
                $this->em->getRepository(UserPermissions::class)->save($newFeature);
            }

            $msg = "Usuário atualizado com sucesso!";
            if ($data['edit'] == 0) $msg = "Usuário cadastrado com sucesso!";
            Auth::sincronize($data['name'], $data['email'], $oldEmail);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Throwable $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getUserData(Request $request, Response $response)
    {
        try {
            $this->getLogged(true);
            $id = $request->getAttribute('route')->getArgument('id');
            $user = $this->em->getRepository(UserAdmin::class)->find($id);
            $arr = [
                'name' => $user->getName(),
                'adminAccess' => $user->getAdminAccess(),
                'directoryAreaAccess' => $user->getDirectoryAreaAccess(),
                'exportAccess' => $user->getExportAccess(),
                'financialReport' => $user->getFinancialReport(),
                'events' => $user->getEvents(),
                'libertasCourses' => $user->isLibertasCourses(),
                'userSector' => $user->getSector() ? $user->getSector()->getId() : '',
                'email' => $user->getEmail(),
                'type' => $user->getType(),
                'level' => $user->getLevel(),
                'active' => $user->isActive() ? 1 : 0,
                'typeView' => $user->getTypeView() ? 1 : 0,
                'state' => $user->getState() ? $user->getState()->getId() : 0,
                'city' => $user->getCity() ? $user->getCity()->getId() : 0
            ];
            $features = $this->em->getRepository(UserPermissions::class)->findBy(['user' => $id]);
            $arr2 = [];
            foreach ($features as $f) {
                $arr2[] = ['feature' => $f->getSystemFeatures()->getId()];
            };
            $cities = $this->em->getRepository(AccessAdmin::class)->listCities($id);
            $mesos = $this->em->getRepository(AccessAdmin::class)->listMesos($id);
            return $response->withJson([
                'status' => 'ok',
                'message' => $arr,
                'features' => $arr2,
                'cities' => $cities,
                'mesos' => $mesos
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}