<?php

namespace App\Requests;

class MissionRequest extends FormRequest
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->rules = $this->rules();
        $this->attributes = $this->attributes();
    }

    public function rules(): array
    {
        return [
            'title' => ['required'],
            'description' => ['required'],
            'points' => ['required'],
            'type' => ['required'],
            'expiration' => ['date'],
            'identifier' => ['identifier']
        ];
    }

    public function attributes(): array
    {
        return [
            'title' => 'Título',
            'description' => 'Descrição',
            'points' => 'Pontuação',
            'type' => 'Tipo',
            'identifier' => 'Propriedade'
        ];
    }
}
