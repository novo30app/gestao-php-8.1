<?php

namespace App\Requests;

class NovoMobilizeRequest extends FormRequest
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->rules = $this->rules();
        $this->attributes = $this->attributes();
    }

    public function rules(): array
    {
        if (empty($this->data['id'])) {
            $rules = [
                'email' => ['required', 'email'],
                'name' => ['required'],
                'type' => ['required'],
                'password' => ['required', 'min' => [8], 'different'],
            ];
        } else {
            $rules = [
                'email' => ['required', 'email'],
                'name' => ['required'],
                'type' => ['required'],
            ];
        }
        return $rules;
    }

    public function attributes(): array
    {
        return [
            'name' => 'Nome',
            'email' => 'E-mail',
            'password' => 'Senha',
            'type' => 'Categoria'
        ];
    }
}