<?php

namespace App\Requests;

use DateTime;

class FormRequest
{
    protected $rules;
    protected $attributes;
    protected $data;

    public function validate(): void
    {
        foreach ($this->rules as $key => $rule) {
            if (in_array('required', $rule)) $this->isRequired($key);
            if (in_array('email', $rule)) $this->isEmail($key);
            if (in_array('min', $rule)) $this->isMin($key);
            if (in_array('max', $rule)) $this->isMax($key);
            if (in_array('between', $rule)) $this->isBetween($key);
            if (in_array('date', $rule)) $this->isDate($key);
        }
    }

    private function isRequired($key)
    {
        if ($this->data[$key] != 0 && empty($this->data[$key])) {
            throw new \Exception("O campo {$this->attributes[$key]} é obrigatório");
        }
    }

    private function isEmail($key)
    {
        if (!empty($this->data[$key]) && !filter_var($this->data[$key], FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("O campo {$this->attributes[$key]} não é um e-mail válido!");
        }
    }

    private function isMin($key)
    {
        $min = $this->rules[$key]['min'][0];
        if (!empty($this->data[$key]) && strlen($this->data[$key]) < $min) {
            throw new \Exception("O campo {$this->attributes[$key]} deve ter no mínimo {$min} caracteres!");
        }
    }

    private function isMax($key)
    {
        $max = $this->rules[$key]['max'][0];
        if (!empty($this->data[$key]) && strlen($this->data[$key]) > $max) {
            throw new \Exception("O campo {$this->attributes[$key]} deve ter no máximo {$max} caracteres!");
        }
    }

    private function isBetween($key)
    {
        $min = $this->rules[$key]['between'][0];
        $max = $this->rules[$key]['between'][1];
        if (!empty($this->data[$key]) && (strlen($this->data[$key]) < $min || strlen($this->data[$key]) > $max)) {
            throw new \Exception("O campo {$this->attributes[$key]} deve ter entre {$min} e {$max} caracteres!");
        }
    }

    private function isDate($key)
    {
        if (!empty($this->data[$key]) && !DateTime::createFromFormat('d/m/Y', $this->data[$key])) {
            throw new \Exception("O campo {$this->attributes[$key]} não é uma data válida!");
        }
    }
}
