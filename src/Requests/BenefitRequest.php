<?php

namespace App\Requests;

class BenefitRequest extends FormRequest
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->rules = $this->rules();
        $this->attributes = $this->attributes();
    }

    public function rules(): array
    {
        return [
            'name' => ['required'],
            'type_delivery' => ['required'],
//            'points' => ['required'],
            'available' => ['required'],
            'title' => ['required'],
            'description' => ['required'],
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => 'Benefício',
//            'points' => 'Pontuação Necessária',
            'type_delivery' => 'Entrega',
            'available' => 'Quantidade Disponível',
            'title' => 'Titulo e-mail resgate',
            'description' => 'Mensagem e-mail resgate',
        ];
    }
}
