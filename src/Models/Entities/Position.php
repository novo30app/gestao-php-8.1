<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="positions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PositionRepository")
 */
class Position
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Position
    {
        $this->name = $name;
        return $this;
    }
}
