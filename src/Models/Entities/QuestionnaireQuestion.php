<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="questionnaireQuestion")
 * @ORM @Entity(repositoryClass="App\Models\Repository\QuestionnaireQuestionRepository")
 */
class QuestionnaireQuestion
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @ManyToOne(targetEntity="Questionnaire")
	 * @JoinColumn(name="questionnaire", referencedColumnName="id")
	 */
	private Questionnaire $questionnaire;
	
	/**
	 * @ManyToOne(targetEntity="Question", inversedBy="questions")
	 * @JoinColumn(name="question", referencedColumnName="id")
	 */
	private Question $question;
	
	/**
	 * @Column(type="boolean")
	 */
	private bool $active = true;
	

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getQuestionnaire(): Questionnaire
	{
		return $this->questionnaire;
	}

	public function setQuestionnaire(Questionnaire $questionnaire): QuestionnaireQuestion
	{
		$this->questionnaire = $questionnaire;
		return $this;
	}

	public function getQuestion(): Question
	{
		return $this->question;
	}

	public function setQuestion(Question $question): QuestionnaireQuestion
	{
		$this->question = $question;
		return $this;
	}

	public function isActive(): bool
	{
		return $this->active;
	}

	public function setActive(bool $active): QuestionnaireQuestion
	{
		$this->active = $active;
		return $this;
	}
}