<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * @Table(name="eventsCoupons")
 * @Entity(repositoryClass="App\Models\Repository\EventsCouponsRepository")
 */
class EventsCoupons
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @Column(type="string")
     */
    private string $code;

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="event", referencedColumnName="id", nullable=true)
     * @var Events
     */
    private $event;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     * @var UserAdmin
     */
    private $user;

    /**
     * @Column(type="integer")
     */
    private int $status;

    /**
     * @Column(type="integer")
     */
    private int $type;

    /**
     * @Column(type="decimal")
     */
    private float $value;

    /**
     * @Column(type="integer")
     */
    private int $porcent;

    /**
     * @Column(type="integer")
     */
    private int $max;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $dateBegin;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $dateFinal;

    /**
     * @Column(type="integer")
     */
    private int $minPerPerson;

    /**
     * @Column(type="integer")
     */
    private int $maxPerPerson;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): EventsCoupons
    {
        $this->created = $created;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): EventsCoupons
    {
        $this->name = $name;
        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): EventsCoupons
    {
        $this->code = $code;
        return $this;
    }

    public function getEvent(): Events
    {
        return $this->event;
    }

    public function setEvent(Events $event): EventsCoupons
    {
        $this->event = $event;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): EventsCoupons
    {
        $this->user = $user;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusString(): string
    {
        $status = match($this->status) {
            1 => 'Ativo',
            2 => 'Cancelado',
            3 => 'Desativado',
            default => ''
        };
        return $status;
    }

    public function setStatus(int $status): EventsCoupons
    {
        $this->status = $status;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getTypeString(): string
    {
        $type = match($this->type) {
            1 => 'Porcentagem',
            2 => 'Valor',
            default => ''
        };
        return $type;
    }

    public function setType(int $type): EventsCoupons
    {
        $this->type = $type;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;   
    }

    public function getValueFormated(): ?string
    {
        $value = $this->value;  
        return number_format($value,2,',','.');   
    }

    public function setValue(float $value): EventsCoupons
    {
        $this->value = $value;
        return $this;
    }

    public function getPorcent(): ?int
    {
        return $this->porcent;   
    }

    public function setPorcent(int $porcent): EventsCoupons
    {
        $this->porcent = $porcent;
        return $this;
    }

    public function getMax(): int
    {
        return $this->max;
    }

    public function setMax(int $max): EventsCoupons
    {
        $this->max = $max;
        return $this;
    }

    public function getDateBegin(): \Datetime
    {
        return $this->dateBegin;
    }

    public function setDateBegin(\Datetime $dateBegin): EventsCoupons
    {
        $this->dateBegin = $dateBegin;
        return $this;
    }

    public function getDateFinal(): ?\Datetime
    {
        return $this->dateFinal;
    }

    public function setDateFinal(\Datetime $dateFinal): EventsCoupons
    {
        $this->dateFinal = $dateFinal;
        return $this;
    }

    public function getMinPerPerson(): int
    {
        return $this->minPerPerson;
    }

    public function setMinPerPerson(int $minPerPerson): EventsCoupons
    {
        $this->minPerPerson = $minPerPerson;
        return $this;
    }

    public function getMaxPerPerson(): int
    {
        return $this->maxPerPerson;
    }

    public function setMaxPerPerson(int $maxPerPerson): EventsCoupons
    {
        $this->maxPerPerson = $maxPerPerson;
        return $this;
    }
}