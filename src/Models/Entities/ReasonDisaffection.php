<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbMotivoDesfiliar
 *
 * @Table(name="tb_motivo_desfiliar")
 * @Entity
 */
class ReasonDisaffection
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="motivo", type="string", length=400, nullable=true)
     */
    private $motivo;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @Column(name="visivel", type="boolean", nullable=true, options={"default"="1"})
     */
    private $visivel = '1';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TbMotivoDesfiliar
     */
    public function setId(int $id): TbMotivoDesfiliar
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    /**
     * @param string|null $motivo
     * @return TbMotivoDesfiliar
     */
    public function setMotivo(?string $motivo): TbMotivoDesfiliar
    {
        $this->motivo = $motivo;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    /**
     * @param \DateTime|null $dataCriacao
     * @return TbMotivoDesfiliar
     */
    public function setDataCriacao(?\DateTime $dataCriacao): TbMotivoDesfiliar
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVisivel(): ?bool
    {
        return $this->visivel;
    }

    /**
     * @param bool|null $visivel
     * @return TbMotivoDesfiliar
     */
    public function setVisivel(?bool $visivel): TbMotivoDesfiliar
    {
        $this->visivel = $visivel;
        return $this;
    }



}
