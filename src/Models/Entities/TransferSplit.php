<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="transferSplit")
 * @ORM @Entity(repositoryClass="App\Models\Repository\TransferSplitRepository")
 */
class TransferSplit
{
    /**
     * @var int
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Transaction")
     * @JoinColumn(name="transaction", referencedColumnName="id")
     */
    private $transaction;

    /**
     * @Column(type="decimal", name="newValue", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $newValue;

    /**
     * @Column(type="decimal", name="discount", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $discount;

    /**
     * @ManyToOne(targetEntity="PaymentRequirements")
     * @JoinColumn(name="transfer", referencedColumnName="id")
     */
    private $transfer = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(Transaction $transaction): TransferSplit
    {
        $this->transaction = $transaction;
        return $this;
    }

    public function getNewValue(): float
    {
        return $this->newValue;
    }

    public function setNewValue(float $newValue): TransferSplit
    {
        $this->newValue = $newValue;
        return $this;
    }

    public function getDiscount(): float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): TransferSplit
    {
        $this->discount = $discount;
        return $this;
    }

    public function getTransfer(): ?PaymentRequirements
    {
        return $this->transfer;
    }

    public function setTransfer(?PaymentRequirements $transfer): TransferSplit
    {
        $this->transfer = $transfer;
        return $this;
    }
}