<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="signatureDocSigned")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SignatureDocSignedRepository")
 */
class SignatureDocSigned
{

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="SignatureDoc")
     * @JoinColumn(name="signatureDoc", referencedColumnName="id")
     */
    private SignatureDoc $signatureDoc;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private ?\DateTime $signedIn = null;

    /**
     * @Column(type="integer")
     */
    private int $signed = 0;

    /**
     * @Column(type="text")
     */
    private string $text = '';

    /**
     * @Column(type="string")
     */
    private string $email = '';

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private string $so = '';

    /**
     * @Column(type="string")
     */
    private string $device = '';

    /**
     * @Column(type="string")
     */
    private string $ip = '';

    /**
     * @Column(type="string")
     */
    private string $hash = '';


    public function __construct()
    {
        $this->created = new \DateTime();
        $this->hash = Utils::generateToken(25) . time();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getSignedIn(): ?\DateTime
    {
        return $this->signedIn;
    }

    public function setSignedIn(?\DateTime $signedIn): SignatureDocSigned
    {
        $this->signedIn = $signedIn;
        return $this;
    }

    public function getSigned(): int
    {
        return $this->signed;
    }

    public function getSignedString(): string
    {
        switch ($this->signed) {
            case 0:
                return 'Pendente';
            case 1:
                return 'Assinado';
            case 2:
                return 'Segunda Via';
            default:
                return 'Desconhecido';
        }
    }

    public function setSigned(int $signed): SignatureDocSigned
    {
        $this->signed = $signed;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): SignatureDocSigned
    {
        $this->name = $name;
        return $this;
    }

    public function getSignatureDoc(): SignatureDoc
    {
        return $this->signatureDoc;
    }

    public function setSignatureDoc(SignatureDoc $signatureDoc): SignatureDocSigned
    {
        $this->signatureDoc = $signatureDoc;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): SignatureDocSigned
    {
        $this->text = $text;
        return $this;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): SignatureDocSigned
    {
        $this->ip = $ip;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): SignatureDocSigned
    {
        $this->email = $email;
        return $this;
    }

    public function getSo(): string
    {
        return $this->so;
    }

    public function setSo(string $so): SignatureDocSigned
    {
        $this->so = $so;
        return $this;
    }

    public function getDevice(): string
    {
        return $this->device;
    }

    public function setDevice(string $device): SignatureDocSigned
    {
        $this->device = $device;
        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): SignatureDocSigned
    {
        $this->hash = $hash;
        return $this;
    }

}