<?php


namespace App\Models\Entities;

/**
 * @Entity @Table(name="ombudsmanCategory")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OmbudsmanCategoryRepository")
 */

class OmbudsmanCategory
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="responsible", referencedColumnName="id")
     */
    private UserAdmin $responsible;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): OmbudsmanCategory
    {
        $this->name = $name;
        return $this;
    }


    public function getResponsible(): UserAdmin
    {
        return $this->responsible;
    }

    public function setResponsible(UserAdmin $responsible): OmbudsmanCategory
    {
        $this->responsible = $responsible;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): OmbudsmanCategory
    {
        $this->active = $active;
        return $this;
    }

    public function getActiveString(): string {
        return $this->active == 1 ? 'Ativo' : 'Inativo';
    }
}