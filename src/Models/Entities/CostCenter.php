<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="costCenter")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CostCenterRepository")
 */
class CostCenter
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @ManyToOne(targetEntity="UserAdmin")
	 * @JoinColumn(name="responsible", referencedColumnName="id")
	 */
	private UserAdmin $responsible;
	
	/**
	 * @Column(type="string")
	 */
	private string $name;
	
	/**
	 * @Column(type="boolean")
	 */
	private bool $status;
	
	/**
	 * @ManyToOne(targetEntity="Directory")
	 * @JoinColumn(name="directory", referencedColumnName="id")
	 */
	private ?Directory $directory = null;
	
	/**
	 * @ManyToOne(targetEntity="Area")
	 * @JoinColumn(name="area", referencedColumnName="id", nullable=true)
	 */
	private ?Area $area = null;
	
	/**
	 * @ManyToOne(targetEntity="SubArea")
	 * @JoinColumn(name="subArea", referencedColumnName="id", nullable=true)
	 */
	private ?SubArea $subArea = null;


	public function getId(): ?int
	{
		return $this->id;
	}

	public function getResponsible(): UserAdmin
	{
		return $this->responsible;
	}

	public function setResponsible(UserAdmin $responsible): CostCenter
	{
		$this->responsible = $responsible;
		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): CostCenter
	{
		$this->name = $name;
		return $this;
	}

	public function isStatus(): bool
	{
		return $this->status;
	}

	public function setStatus(bool $status): CostCenter
	{
		$this->status = $status;
		return $this;
	}

	public function getDirectory(): ?Directory
	{
		return $this->directory;
	}

	public function setDirectory(?Directory $directory): CostCenter
	{
		$this->directory = $directory;
		return $this;
	}

	public function getArea(): ?Area
	{
		return $this->area;
	}

	public function setArea(?Area $area): CostCenter
	{
		$this->area = $area;
		return $this;
	}

	public function getSubArea(): ?SubArea
	{
		return $this->subArea;
	}

	public function setSubArea(?SubArea $subArea): CostCenter
	{
		$this->subArea = $subArea;
		return $this;
	}


}