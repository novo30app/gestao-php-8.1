<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="extractBB")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ExtractBBRepository")
 */
class ExtractBB
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private ?UserAdmin $user = null;

	/**
	 * @Column(type="integer")
	 */
	private int $request;

    /**
     * @ManyToOne(targetEntity="PaymentRequirements")
     * @JoinColumn(name="payment", referencedColumnName="id")
     */
	private PaymentRequirements $payment;

    /**
	 * @Column(type="string")
	 */
	private string $status;

    /**
	 * @Column(type="string")
	 */
	private ?string $statusString;

    /**
	 * @Column(type="string")
	 */
	private string $beneficiaryName;

	/**
	 * @Column(type="datetime")
	 */
	private \DateTime $created;

    /**
     * @Column(type="string")
     */
    private string $directory;

	/**
	 * @Column(type="integer")
	 */
	private int $cnpj;

    /**
     * @Column(type="decimal", name="value", precision=13, scale=2, nullable=true)
     * 
     * @var float
     */
    private $value;

	/**
	 * @Column(type="integer")
	 */
	private int $bank;

    /**
	 * @Column(type="string")
	 */
	private string $pix;

    /**
	 * @Column(type="integer")
	 */
	private int $agency;

    /**
	 * @Column(type="integer")
	 */
	private int $account;

    /**
	 * @Column(type="string")
	 */
	private string $accountDigit;


    /**
	 * @Column(type="integer")
	 */
	private ?int $docNumber;

    /**
     * @Column(type="string")
     */
    private string $response;

    /**
     * @Column(type="string")
     */
    private string $consult;

    /**
     * @Column(type="string")
     */
    private string $cancel;

    
    /**
     * @Column(type="string")
     */
    private string $liberation;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="released_by", referencedColumnName="id")
     */
    private ?UserAdmin $releasedBy;

    /**
     * @Column(name="released_in", type="datetime")
     */
    private ?\DateTime $releasedIn;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="canceled_by", referencedColumnName="id")
     */
    private ?UserAdmin $canceledBy;

    /**
     * @Column(name="canceled_in", type="datetime")
     */
    private ?\DateTime $canceledIn;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): ?UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): ExtractBB
    {
        $this->user = $user;
        return $this;
    }

    public function getRequest(): string
    {
        return $this->request;
    }

    public function setRequest(string $request): ExtractBB
    {
        $this->request = $request;
        return $this;
    }

    public function getPayment(): PaymentRequirements
    {
        return $this->payment;
    }

    public function setPayment(PaymentRequirements $payment): ExtractBB
    {
        $this->payment = $payment;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): ExtractBB
    {
        $this->status = $status;
        return $this;
    }

    public function getStatusString(): ?string
    {
        return $this->statusString;
    }

    public function setStatusString(string $statusString): ExtractBB
    {
        $this->statusString = $statusString;
        return $this;
    }

    public function getBeneficiaryName(): string
    {
        return $this->beneficiaryName;
    }

    public function setBeneficiaryName(string $beneficiaryName): ExtractBB
    {
        $this->beneficiaryName = $beneficiaryName;
        return $this;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): ExtractBB
    {
        $this->created = $created;
        return $this;
    }

    public function getDirectory(): string
    {
        return $this->directory;
    }

    public function setDirectory(string $directory): ExtractBB
    {
        $this->directory = $directory;
        return $this;
    }

    public function getCnpj(): int
    {
        return $this->cnpj;
    }

    public function setCnpj(int $cnpj): ExtractBB
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getValueFormated()
    {
        $value = $this->value;  
        return number_format($value,2,',','.');   
    }

    public function setValue(float $value): ExtractBB
    {
        $this->value = $value;
        return $this;
    }

    public function getBank(): int
    {
        return $this->bank;
    }

    public function setBank(int $bank): ExtractBB
    {
        $this->bank = $bank;
        return $this;
    }

    public function getPix(): string
    {
        return $this->pix;
    }

    public function setPix(string $pix): ExtractBB
    {
        $this->pix = $pix;
        return $this;
    }

    public function getAgency(): int
    {
        return $this->agency;
    }

    public function setAgency(int $agency): ExtractBB
    {
        $this->agency = $agency;
        return $this;
    }

    public function getAccount(): int
    {
        return $this->account;
    }

    public function setAccount(int $account): ExtractBB
    {
        $this->account = $account;
        return $this;
    }

    public function getAccountDigit(): string
    {
        return $this->accountDigit;
    }

    public function setAccountDigit(string $accountDigit): ExtractBB
    {
        $this->accountDigit = $accountDigit;
        return $this;
    }

    public function getDocNumber(): ?int
    {
        return $this->docNumber;
    }

    public function setDocNumber(int $docNumber): ExtractBB
    {
        $this->docNumber = $docNumber;
        return $this;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function setResponse(string $response): ExtractBB
    {
        $this->response = $response;
        return $this;
    }

    public function getConsult(): string
    {
        return $this->consult;
    }

    public function setConsult(string $consult): ExtractBB
    {
        $this->consult = $consult;
        return $this;
    }

    public function getCancel(): string
    {
        return $this->cancel;
    }

    public function setCancel(string $cancel): ExtractBB
    {
        $this->cancel = $cancel;
        return $this;
    }

    public function getLiberation(): string
    {
        return $this->liberation;
    }

    public function setLiberation(string $liberation): ExtractBB
    {
        $this->liberation = $liberation;
        return $this;
    }

    public function getReleasedBy(): ?UserAdmin
    {
        return $this->releasedBy;
    }

    public function setReleasedBy(UserAdmin $releasedBy): ExtractBB
    {
        $this->releasedBy = $releasedBy;
        return $this;
    }

    public function getReleasedIn(): ?\DateTime
    {
        return $this->releasedIn;
    }

    public function setReleasedIn(?\DateTime $releasedIn): ExtractBB
    {
        $this->releasedIn = $releasedIn;
        return $this;
    }

    public function getCanceledBy(): ?UserAdmin
    {
        return $this->canceledBy;
    }

    public function setCanceledBy(UserAdmin $canceledBy): ExtractBB
    {
        $this->canceledBy = $canceledBy;
        return $this;
    }

    public function getCanceledIn(): ?\DateTime
    {
        return $this->canceledIn;
    }

    public function setCanceledIn(?\DateTime $canceledIn): ExtractBB
    {
        $this->canceledIn = $canceledIn;
        return $this;
    }
}