<?php
namespace App\Models\Entities;
use Doctrine\Mapping as ORM;
use App\Helpers\Utils;

/**
 * TbEventosTransacoes
 *
 * @Table(name="tb_eventos_transacoes", indexes={@Index(name="idx_tb_eventos_transacoes_tb_cidade_id", columns={"tb_cidade_id"}), @Index(name="idx_tb_eventos_transacoes_tb_eventos_id", columns={"tb_eventos_id"}), @Index(name="idx_tb_eventos_transacoes_tb_transacao_id", columns={"tb_transacao_id"}), @Index(name="idx_tb_eventos_transacoes_tb_estado_id", columns={"tb_estado_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsTransactionsRepository")
 */
class EventsTransactions
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="tb_transacao_id", type="integer", nullable=true)
     */
    private $tbTransacaoId;

    /**
     * @var string|null
     *
     * @Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @Column(name="cpf", type="string", length=20, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @Column(name="telefone_ddi", type="string", length=4, nullable=true)
     */
    private $telefoneDdi;

    /**
     * @var string|null
     *
     * @Column(name="telefone_numero", type="string", length=25, nullable=true)
     */
    private $telefoneNumero;

    /**
     * @var string|null
     *
     * @Column(name="celular_ddi", type="string", length=4, nullable=true)
     */
    private $celularDdi;

    /**
     * @var string|null
     *
     * @Column(name="celular_numero", type="string", length=25, nullable=true)
     */
    private $celularNumero;

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=80, nullable=true)
     */
    private $email;

    /**
     * @var int|null
     *
     * @Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @ManyToOne(targetEntity="EventsCoupons")
     * @JoinColumn(name="coupon", referencedColumnName="id", nullable=true)
     * @var EventsCoupons
     */
    private $coupon;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=0, nullable=true, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->dataCriacao;
    } 

    public function setData(\DateTime $dataCriacao): EventsTransactions
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getEventsId(): ?int
    {
        return $this->tbEventosId;
    }

    public function setTbEventsId(?int $tbEventosId): EventsTransactions
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getTbPessoaId(): ?int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(?int $tbPessoaId): EventsTransactions
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTbTransacaoId(): ?int
    {
        return $this->tbTransacaoId;
    }

    public function setTbTransacaoId(?int $tbTransacaoId): EventsTransactions
    {
        $this->tbTransacaoId = $tbTransacaoId;
        return $this;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValue(string $valor): EventsTransactions
    {
        $this->valor = $valor;
        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setName(string $nome): EventsTransactions
    {
        $this->nome = $nome;
        return $this;
    }

    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    public function setDataNascimento(?\DateTime $dataNascimento): EventsTransactions
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }
    
    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): EventsTransactions
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): EventsTransactions
    {
        $this->email = $email;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): EventsTransactions
    {
        $this->code = $code;
        return $this;
    }

    public function getCoupon(): EventsCoupons
    {
        return $this->coupon;
    }

    public function setCoupon(EventsCoupons $coupon): EventsTransactions
    {
        $this->coupon = $coupon;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): EventsTransactions
    {
        $this->status = $status;
        return $this;
    }
}
