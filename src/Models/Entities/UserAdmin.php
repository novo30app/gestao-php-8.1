<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="usersAdmin")
 * @ORM @Entity(repositoryClass="App\Models\Repository\UserAdminRepository")
 */
class UserAdmin
{

    const TYPE_ASSISTANT = 1;
    const TYPE_LEADER = 2;
    const TYPE_ADMIN = 3;
    const TYPE_Accounting = 4;
    const LEVEL_CITY = 1;
    const LEVEL_STATE = 2;
    const LEVEL_NATIONAL = 3;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $level = 1;

    /**
     * @Column(type="integer")
     */
    private int $type = 1;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="Sectors")
     * @JoinColumn(name="sector", referencedColumnName="id", nullable=true)
     */
    private ?Sectors $sector = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private string $password = '';

    /**
     * @Column(type="string", unique=true)
     */
    private string $email = '';

    /**
     * @Column(type="boolean")
     */
    private bool $adminAccess = true;

    /**
     * @Column(type="boolean")
     */
    private bool $directoryAreaAccess = true;

    /**
     * @Column(type="boolean")
     */
    private bool $exportAccess = true;

    /**
     * @Column(type="boolean")
     */
    private bool $financialReport = true;

    /**
     * @Column(type="boolean")
     */
    private bool $events = true;

    /**
     * @Column(type="boolean")
     */
    private bool $libertasCourses = false;

    /**
     * @Column(type="boolean")
     */
    private bool $typeView = true;

    public function havePermissionCharge()
    {
        $array = array(45, 46, 39, 37, 300, 183, 195);
        return ($this->level == 3 || in_array($this->id, $array));
    }

    public function havePermissionAlertBillsToPay()
    {
        return in_array($this->id, [44, 371, 23, 21]);
    }

    public function havePermissionRequerimentPayments()
    {
        return ($this->financialReport == 1 && $this->type != 3);
    }

    public function havePermissionBillsToPay()
    {
        return (in_array($this->type, [3, 4]));
    }

    public function havePermissionbillsToReceive()
    {
        return ($this->level == 3);
    }

    public function havePermission(int $level, int $type)
    {
        return ($this->level >= $level && $this->type >= $type);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDirectory(): string
    {

        if (!$this->city && !$this->state) return "DN";
        if (!$this->city && $this->state) return "DE - {$this->getState()->getSigla()}";
        return "DM - {$this->getCity()->getCidade()}/{$this->getState()->getSigla()}";
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): UserAdmin
    {
        $this->level = $level;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): UserAdmin
    {
        $this->type = $type;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): UserAdmin
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): UserAdmin
    {
        $this->state = $state;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): UserAdmin
    {
        $this->active = $active;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): UserAdmin
    {
        $this->password = $password;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): UserAdmin
    {
        $this->email = $email;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): UserAdmin
    {
        $this->name = $name;
        return $this;
    }

    public function isAdminNational()
    {
        return $this->level == self::LEVEL_NATIONAL && $this->type == self::TYPE_ADMIN;
    }

    public function getAdminAccess(): bool
    {
        return $this->adminAccess;
    }

    public function setAdminAccess(bool $adminAccess): UserAdmin
    {
        $this->adminAccess = $adminAccess;
        return $this;
    }

    public function getDirectoryAreaAccess(): bool
    {
        return $this->directoryAreaAccess;
    }

    public function setDirectoryAreaAccess(bool $directoryAreaAccess): UserAdmin
    {
        $this->directoryAreaAccess = $directoryAreaAccess;
        return $this;
    }

    public function getExportAccess(): bool
    {
        return $this->exportAccess;
    }

    public function setExportAccess(bool $exportAccess): UserAdmin
    {
        $this->exportAccess = $exportAccess;
        return $this;
    }

    public function getFinancialReport(): bool
    {
        return $this->financialReport;
    }

    public function setFinancialReport(bool $financialReport): UserAdmin
    {
        $this->financialReport = $financialReport;
        return $this;
    }

    public function isLibertasCourses(): bool
    {
        return $this->libertasCourses;
    }

    public function setLibertasCourses(bool $libertasCourses): UserAdmin
    {
        $this->libertasCourses = $libertasCourses;
        return $this;
    }


    public function getEvents(): bool
    {
        return $this->events;
    }

    public function setEvents(bool $events): UserAdmin
    {
        $this->events = $events;
        return $this;
    }

    public function getTypeView(): bool
    {
        return $this->typeView;
    }

    public function setTypeView(bool $typeView): UserAdmin
    {
        $this->typeView = $typeView;
        return $this;
    }

    public function accountingPermission(): bool
    {
        return (in_array($this->id, [21, 23, 60, 585, 584, 586, 583, 660, 1373, 1583, 1801, 1208, 31]));
    }

    public function withoutPermissionToFinance(): bool
    {
        return (in_array($this->id, [888, 890, 915, 1599]));
    }

    public function cashFlowPermission(): bool
    {
        $allow = false;
        if ($this->level == 3) $allow = true;
        if ($this->level == 2 && in_array($this->getState()->getId(), [26, 18])) $allow = true;
        if ($this->level == 1 && $this->getCity()->getId() == 5270) $allow = true;
        return $allow;
    }

    public function getSector(): ?Sectors
    {
        return $this->sector;
    }

    public function setSector(?Sectors $sector): UserAdmin
    {
        $this->sector = $sector;
        return $this;
    }


}