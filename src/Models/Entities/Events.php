<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * TbEventos
 *
 * @Entity @Table(name="tb_eventos", indexes={@Index(name="idx_tb_eventos_data_inicio", columns={"data_inicio"}), @Index(name="idx_tb_eventos_diretorio_id", columns={"diretorio_id"}), @Index(name="idx_tb_eventos_estado_id", columns={"estado_id"}), @Index(name="idx_tb_eventos_status", columns={"status"}), @Index(name="idx_tb_eventos_nome", columns={"nome"}), @Index(name="idx_tb_eventos_cidade_id", columns={"cidade_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsRepository")
 */
class Events
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY") 
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="nucleo_id", type="integer", nullable=true)
     */
    private $nucleoId;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="estado_id", referencedColumnName="id")
     */
    private $estadoId;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="cidade_id", referencedColumnName="id")
     */
    private $cidadeId;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="usuario_responsavel_id", referencedColumnName="id")
     */
    private $usuarioResponsavelId;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=0, nullable=true, options={"default"="pendente"})
     */
    private $status;

    /**
     * @var string|null
     *
     * @Column(name="tipo", type="string", length=0, nullable=true, options={"default"="pago"})
     */
    private $tipo = null;

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @Column(name="local", type="string", length=50, nullable=true)
     */
    private $local;

    /**
     * @var string|null
     *
     * @Column(name="cep", type="string", length=100, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=100, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @Column(name="complemento", type="string", length=100, nullable=true)
     */
    private $complemento;

    /**
     * @Column(name="data_inicio", type="datetime", nullable=true)
     * @var \DateTime|null
     */
    private $dataInicio;

    /**
     *
     * @Column(name="data_termino", type="datetime", nullable=true)
     * @var \DateTime|null
     */
    private $dataTermino;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_evento", type="datetime", nullable=true)
     */
    private $dataEvento;

    /**
     * @var string|null
     *
     * @Column(name="path_logo", type="string", length=255, nullable=true)
     */
    private $pathLogo;

    /**
     * @var string|null
     *
     * @Column(name="path_banner", type="string", length=255, nullable=true)
     */
    private $pathBanner;

    /**
     * @var string|null
     *
     * @Column(name="path_informative", type="string", length=255, nullable=true)
     */
    private $pathInformative;

    /**
     * @var string|null
     *
     * @Column(name="link_video", type="string", length=255, nullable=true)
     */
    private $linkVideo;

    /**
     * @var string|null
     *
     * @Column(name="link_online", type="string", length=255, nullable=true)
     */
    private $linkOnline;

    /**
     * @var int|null
     *
     * @Column(name="limite_inscritos", type="integer", nullable=true)
     */
    private $limiteInscritos;

    /**
     * @var string|null
     *
     * @Column(name="descricao", type="text", length=0, nullable=true)
     */
    private $descricao;

    /**
     * @var bool|null
     *
     * @Column(name="apenas_filiados", type="boolean", nullable=true)
     */
    private $apenasFiliados;

    /**
     * @var string|null
     *
     * @Column(name="especie", type="text", nullable=true)
     */
    private $especie;

    /**
     * @var string|null
     *
     * @Column(name="categoria", type="text", nullable=true)
     */
    private $categoria;

    /**
     * @var string|null
     *
     * @Column(name="exibicao", type="text", nullable=true)
     */
    private $exibicao;

    /**
     * @var bool|null
     *
     * @Column(name="oficial_novo", type="boolean", nullable=true)
     */
    private $oficialNovo;

    /**
     * @var string
     *
     * @Column(name="data_criacao", type="string", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var int|null
     *
     * @Column(name="removido", type="smallint", nullable=true)
     */
    private $removido;

    /**
     * @var int|null
     *
     * @Column(name="dias_boleto", type="integer", nullable=true)
     */
    private $diasBoleto;

    /**
     * @var bool|null
     *
     * @Column(name="vaquinha", type="boolean", nullable=true)
     */
    private $vaquinha;

    /**
     * @var bool|null
     *
     * @Column(name="presencial", type="boolean", nullable=true)
     */
    private $presencial;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="diretorio_id", referencedColumnName="id")
     */
    private $Directory;

    /**
     * @Column(name="responsibleAffiliate", type="string", nullable=true)
     */
    private $responsibleAffiliate;

    public function setDirectory(Directory $Directory): Events
    {
        $this->Directory = $Directory;
        return $this;
    }

    public function getDirectory(): Directory
    {
        return $this->Directory;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->tipo;
    }

    public function setType(?string $tipo): Events
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->nome;
    }

    public function setName(?string $nome): Events
    {
        $this->nome = $nome;
        return $this;
    }

    public function getSpecie(): ?string
    {
        return $this->especie;
    }

    public function getSpecieString(): ?string
    {
        switch ($this->especie) {
            case 1:
                $especie = 'Loja';
                break;
            case 2:
                $especie = 'Mobilização';
                break;
            case 3:
                $especie = 'Online';
                break;            
            case 4:
                $especie = 'Política da Mulher';
                break;            
            case 5:
                $especie = 'Tradicional';
                break;            
            case 6:
                $especie = 'Vaquinha';
                break;       
            default:
                $especie = null;
                break;
        }
        return $especie;
    }

    public function setSpecie(?string $especie): Events
    {
        $this->especie = $especie;
        return $this;
    }

    public function getStartDate(): ?\DateTime
    {
        return $this->dataInicio;
    }

    public function setStartDate(?\DateTime $dataInicio): Events
    {
        $this->dataInicio = $dataInicio;
        return $this;
    }

    public function getFinishDate(): ?\DateTime
    {
        return $this->dataTermino;
    }

    public function setFinishDate(?\DateTime $dataTermino): Events
    {
        $this->dataTermino = $dataTermino;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): Events
    {
        $this->status = $status;
        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->categoria;
    }

    public function getCategoryString(): ?string
    {
        switch ($this->categoria) {
            case 1:
                $categoria = 'Doação';
                break;
            default:
                $categoria = 'Evento';
                break;
        }
        return $categoria;
    }

    public function setCategory(?string $categoria): Events
    {
        $this->categoria = $categoria;
        return $this;
    }

    public function getShow(): ?string
    {
        return $this->exibicao;
    }

    public function getShowString(): ?string
    {
        switch ($this->exibicao) {
            case 1:
                $exibicao = 'Sim';
                break;
            
            default:
                $exibicao = 'Não';
                break;
        }
        return $exibicao;
    }

    public function setShow(?string $exibicao): Events
    {
        $this->exibicao = $exibicao;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->descricao;
    }

    public function setDescription(?string $descricao): Events
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->endereco;
    }

    public function setAddress(?string $endereco): Events
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getLocal(): ?string
    {
        return $this->local;
    }

    public function setLocal(?string $local): Events
    {
        $this->local = $local;
        return $this;
    }

    public function getPathBanner(): ?string
    {
        return $this->pathBanner;
    }

    public function setPathBanner(?string $pathBanner): Events
    {
        $this->pathBanner = $pathBanner;
        return $this;
    }

    public function getPathLogo(): ?string
    {
        return $this->pathLogo;
    }

    public function setPathLogo(?string $pathLogo): Events
    {
        $this->pathLogo = $pathLogo;
        return $this;
    }

    public function getPathInformative(): ?string
    {
        return $this->pathInformative;
    }

    public function setPathInformative(?string $pathInformative): Events
    {
        $this->pathInformative = $pathInformative;
        return $this;
    }
    
    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(?string $cep): Events
    {
        $this->cep = $cep;
        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->bairro;
    }

    public function setNeighborhood(?string $bairro): Events
    {
        $this->bairro = $bairro;
        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complemento;
    }

    public function setComplement(?string $complemento): Events
    {
        $this->complemento = $complemento;
        return $this;
    }

    public function getBilletdays(): ?int
    {
        return $this->diasBoleto;
    }

    public function setBilletdays(?string $diasBoleto): Events
    {
        $this->diasBoleto = $diasBoleto;
        return $this;
    }

    public function getStateId(): State
    {
        return $this->estadoId;
    }

    public function setStateId(State $estadoId): Events
    {
        $this->estadoId = $estadoId;
        return $this;
    }

    public function getCity(): City
    {
        return $this->cidadeId;
    }

    public function setCity(City $cidadeId): Events
    {
        $this->cidadeId = $cidadeId;
        return $this;
    }

    public function getUserId(): UserAdmin
    {
        return $this->usuarioResponsavelId;
    }

    public function setUserId(UserAdmin $usuarioResponsavelId): Events
    {
        $this->usuarioResponsavelId = $usuarioResponsavelId;
        return $this;
    }

    public function getLinkOnline(): ?string
    {
        return $this->linkOnline;
    }

    public function setLinkOnline(?string $linkOnline): Events
    {
        $this->linkOnline = $linkOnline;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limiteInscritos;
    }

    public function setLimit(?string $limiteInscritos): Events
    {
        $this->limiteInscritos = $limiteInscritos;
        return $this;
    }

    public function getOficial(): ?string
    {
        return $this->oficialNovo;
    }

    public function getOficialString(): ?string
    {
        switch ($this->oficialNovo) {
            case 1:
                $oficialNovo = 'Evento oficial';
                break;
            default:
                $oficialNovo = 'Evento não oficial';
                break;
        }
        return $oficialNovo;
    }

    public function setOficial(?string $oficialNovo): Events
    {
        $this->oficialNovo = $oficialNovo;
        return $this;
    }

    public function setDataCreation(?string $dataCriacao): Events
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function setDataEvent(?\DateTime $dataEvento): Events
    {
        $this->dataEvento = $dataEvento;
        return $this;
    }

    public function getPublic(): ?int
    {
        return $this->apenasFiliados;
    }

    public function getPublicString(): ?string
    {
        switch ($this->apenasFiliados) {
            case 'value':
                $apenasFiliados = 'Evento exclusivo para filiados';
                break;
            default:
                $apenasFiliados = 'Evento aberto para não filiados';
                break;
        }
        return $apenasFiliados;
    }

    public function setPublic(?string $apenasFiliados): Events
    {
        $this->apenasFiliados = $apenasFiliados;
        return $this;
    }
    
    public function getResponsibleAffiliate()
    {
        return $this->responsibleAffiliate;
    }

    public function setResponsibleAffiliate($responsibleAffiliate): Events
    {
        $this->responsibleAffiliate = $responsibleAffiliate;
        return $this;
    }
}