<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="cnpjGru")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CnpjGruRepository")
 */
class CnpjGru
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=true)
     */
    private $directory;

    /**
     * @Column(type="string")
     */
    private string $cnpj;

    /**
     * @Column(type="string")
     */
    private string $treTse;

    public function getId(): int
    {
        return $this->id;
    }

    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    public function setDirectory(Directory $directory): CnpjGru
    {
        $this->directory = $directory;
        return $this;
    }

    public function getCnpj(): string
    {
        return $this->cnpj;
    }
 
    public function setCnpj(string $cnpj): CnpjGru
    {
        $this->cnpj = $cnpj;
        return $this;
    }
    
    public function getTreTse(): string
    {
        return $this->treTse;
    }

    public function setTreTse(string $treTse): CnpjGru
    {
        $this->treTse = $treTse;
        return $this;
    }
}