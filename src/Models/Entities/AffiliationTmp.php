<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="affiliationTmp")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AffiliationTmpRepository")
 * @ORM\Embedded
 * @ORM\Embedded
 */
class AffiliationTmp
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="CpfTmp")
     * @JoinColumn(name="cpfTmp", referencedColumnName="id")
     * @var CpfTmp
     */
    private $cpfTmp;

    /**
     * @Column(type="integer", nullable=true)
     * @var int
     */
    private string $addressCountry;

    /**
     * @Column(type="date", nullable=true)
     * @var \DateTime
     */
    private ?\DateTime $birth = null;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $cellPhone;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $city;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $district;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $mother;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $name;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $number;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $street;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $uf;


    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $voterTitle;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $voterTitleCity;

    /**
     * @Column(type="integer", nullable=true)
     * @var int
     */
    private string $voterTitleCountry;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $voterTitleSection;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $voterTitleUF;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $voterTitleZone;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private string $zipCode;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCpfTmp(): CpfTmp
    {
        return $this->cpfTmp;
    }

    public function setCpfTmp(CpfTmp $cpfTmp): AffiliationTmp
    {
        $this->cpfTmp = $cpfTmp;
        return $this;
    }

    public function getAddressCountry(): int
    {
        return $this->addressCountry;
    }

    public function setAddressCountry(int $addressCountry): AffiliationTmp
    {
        $this->addressCountry = $addressCountry;
        return $this;
    }

    public function getBirth(): ?\DateTime
    {
        return $this->birth;
    }

    public function setBirth(?\DateTime $birth): AffiliationTmp
    {
        $this->birth = $birth;
        return $this;
    }

    public function getCellPhone(): string
    {
        return $this->cellPhone;
    }

    public function setCellPhone(string $cellPhone): AffiliationTmp
    {
        $this->cellPhone = $cellPhone;
        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): AffiliationTmp
    {
        $this->city = $city;
        return $this;
    }

    public function getDistrict(): string
    {
        return $this->district;
    }

    public function setDistrict(string $district): AffiliationTmp
    {
        $this->district = $district;
        return $this;
    }

    public function getMother(): string
    {
        return $this->mother;
    }

    public function setMother(string $mother): AffiliationTmp
    {
        $this->mother = $mother;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): AffiliationTmp
    {
        $this->name = $name;
        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): AffiliationTmp
    {
        $this->number = $number;
        return $this;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): AffiliationTmp
    {
        $this->street = $street;
        return $this;
    }

    public function getUf(): string
    {
        return $this->uf;
    }

    public function setUf(string $uf): AffiliationTmp
    {
        $this->uf = $uf;
        return $this;
    }


    public function getVoterTitle(): string
    {
        return $this->voterTitle;
    }

    public function setVoterTitle(string $voterTitle): AffiliationTmp
    {
        $this->voterTitle = $voterTitle;
        return $this;
    }

    public function getVoterTitleCity(): string
    {
        return $this->voterTitleCity;
    }

    public function setVoterTitleCity(string $voterTitleCity): AffiliationTmp
    {
        $this->voterTitleCity = $voterTitleCity;
        return $this;
    }

    public function getVoterTitleCountry(): int
    {
        return $this->voterTitleCountry;
    }

    public function setVoterTitleCountry(int $voterTitleCountry): AffiliationTmp
    {
        $this->voterTitleCountry = $voterTitleCountry;
        return $this;
    }

    public function getVoterTitleSection(): string
    {
        return $this->voterTitleSection;
    }

    public function setVoterTitleSection(string $voterTitleSection): AffiliationTmp
    {
        $this->voterTitleSection = $voterTitleSection;
        return $this;
    }

    public function getVoterTitleUF(): string
    {
        return $this->voterTitleUF;
    }

    public function setVoterTitleUF(string $voterTitleUF): AffiliationTmp
    {
        $this->voterTitleUF = $voterTitleUF;
        return $this;
    }

    public function getVoterTitleZone(): string
    {
        return $this->voterTitleZone;
    }

    public function setVoterTitleZone(string $voterTitleZone): AffiliationTmp
    {
        $this->voterTitleZone = $voterTitleZone;
        return $this;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): AffiliationTmp
    {
        $this->zipCode = $zipCode;
        return $this;
    }

}