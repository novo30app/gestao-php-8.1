<?php


namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="providers")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ProvidersRepository")
 */
class Provider
{

    const PF = 1;
    const PJ = 2;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $type = self::PJ;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private ?string $nameResponsible = '';

    /**
     * @Column(type="string", length=20)
     */
    private string $cpfCnpj = '';

    /**
     * @Column(type="string")
     */
    private ?string $cpfResponsible = '';

    /**
     * @Column(type="string")
     */
    private ?string $phone = '';

    /**
     * @Column(type="string")
     */
    private ?string $email = '';

    /**
     * @ManyToOne(targetEntity="Bank")
     * @JoinColumn(name="bank", referencedColumnName="id", nullable=true)
     */
    private ?Bank $bank = null;

    /**
     * @Column(type="string")
     */
    private ?string $agency = '';

    /**
     * @Column(type="string")
     */
    private ?string $account = '';

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(type="boolean")
     */
    private bool $status = true;

    /**
     * @Column(type="integer")
     */
    private int $paymentMethod;

    /**
     * @ManyToOne(targetEntity="ChartOfAccounts")
     * @JoinColumn(name="accountCredit", referencedColumnName="id")
     */
    private ?ChartOfAccounts $accountCredit = null;

    /**
     * @OneToOne(targetEntity="Autonomous")
     * @JoinColumn(name="autonomous_id", referencedColumnName="id")
     */
    private ?Autonomous $autonomous = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): Provider
    {
        $this->type = $type;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Provider
    {
        $this->name = strtoupper($name);
        return $this;
    }

    public function getCpfCnpj(): string
    {
        return $this->cpfCnpj;
    }

    public function setCpfCnpj(string $cpfCnpj): Provider
    {
        $this->cpfCnpj = Utils::onlyNumbers($cpfCnpj);
        return $this;
    }

    public function getNameResponsible(): ?string
    {
        return $this->nameResponsible;
    }

    public function setNameResponsible(?string $nameResponsible): Provider
    {
        $this->nameResponsible = $nameResponsible;
        return $this;
    }

    public function getCpfResponsible(): ?string
    {
        return $this->cpfResponsible;
    }

    public function setCpfResponsible(?string $cpfResponsible): Provider
    {
        $this->cpfResponsible = $cpfResponsible;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): Provider
    {
        $this->phone = $phone;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Provider
    {
        $this->email = $email;
        return $this;
    }

    public function getBank(): ?Bank
    {
        return $this->bank;
    }

    public function setBank(?Bank $bank): Provider
    {
        $this->bank = $bank;
        return $this;
    }

    public function getAgency(): ?string
    {
        return $this->agency;
    }

    public function setAgency(?string $agency): Provider
    {
        $this->agency = $agency;
        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(?string $account): Provider
    {
        $this->account = $account;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Provider
    {
        $this->active = $active;
        return $this;
    }

    public function isStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): Provider
    {
        $this->status = $status;
        return $this;
    }

    public function getPaymentMethod(): int
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(int $paymentMethod): Provider
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    public function getAccountCredit(): ?ChartOfAccounts
    {
        return $this->accountCredit;
    }

    public function setAccountCredit(ChartOfAccounts $accountCredit): Provider
    {
        $this->accountCredit = $accountCredit;
        return $this;
    }

    public function getAutonomous(): ?Autonomous
    {
        return $this->autonomous;
    }

    public function setAutonomous(Autonomous $autonomous): Provider
    {
        $this->autonomous = $autonomous;
        return $this;
    }

}