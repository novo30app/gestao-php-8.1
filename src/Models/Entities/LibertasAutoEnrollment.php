<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="libertasAutoEnrollments")
 * @ORM @Entity(repositoryClass="App\Models\Repository\LibertasAutoEnrollmentRepository")
 */
class LibertasAutoEnrollment
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(name="title", type="string")
     */
    private string $email = '';

    /**
     * @Column(name="status", type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(name="auto_enrollment", type="boolean")
     */
    private bool $auto_enrollment = false;

    /**
     * @Column(name="mandatario_legislativo", type="boolean")
     */
    private bool $mandatario_legislativo = false;

    /**
     * @Column(name="mandatario_executivo", type="boolean")
     */
    private bool $mandatario_executivo = false;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): LibertasAutoEnrollment
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new \Exception("E-mail inválido: {$email}");
        $this->email = $email;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): LibertasAutoEnrollment
    {
        $this->active = $active;
        return $this;
    }

    public function changeActive(): LibertasAutoEnrollment
    {
        $this->active = !$this->active;
        return $this;
    }

    public function isAutoEnrollment(): bool
    {
        return $this->auto_enrollment;
    }

    public function setAutoEnrollment(bool $auto_enrollment): LibertasAutoEnrollment
    {
        $this->auto_enrollment = $auto_enrollment;
        return $this;
    }

    public function isMandatarioLegislativo(): bool
    {
        return $this->mandatario_legislativo;
    }

    public function setMandatarioLegislativo(bool $mandatario_legislativo): LibertasAutoEnrollment
    {
        $this->mandatario_legislativo = $mandatario_legislativo;
        return $this;
    }

    public function isMandatarioExecutivo(): bool
    {
        return $this->mandatario_executivo;
    }

    public function setMandatarioExecutivo(bool $mandatario_executivo): LibertasAutoEnrollment
    {
        $this->mandatario_executivo = $mandatario_executivo;
        return $this;
    }


}