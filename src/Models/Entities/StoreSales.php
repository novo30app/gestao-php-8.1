<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="storeSales")
 * @ORM @Entity(repositoryClass="App\Models\Repository\StoreSalesRepository")
 */
class StoreSales
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $register;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @Column(type="string")
     */
    private string $invoiceId;

    /**
     * @Column(type="string")
     */
    private string $status;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $paid_at;

    /**
     * @Column(type="decimal", precision=13, scale=4)
     * @var float
     */
    private $paid_value;

    /**
     * @Column(type="string")
     */
    private string $orderDimona;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRegister(): string
    {
        return $this->register;
    }

    public function setRegister(string $register): StoreSales
    {
        $this->register = $register;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): StoreSales
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getInvoiceId(): string
    {
        return $this->invoiceId;
    }

    public function setInvoiceId(string $invoiceId): StoreSales
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): StoreSales
    {
        $this->status = $status;
        return $this;
    }

    public function getPaid_at(): \DateTime
    {
        return $this->paid_at;
    }

    public function setPaid_at(\DateTime $paid_at): StoreSales
    {
        $this->paid_at = $paid_at;
        return $this;
    }

    public function getPaid_value(): float
    {
        return $this->paid_value;
    }

    public function setPaid_value(float $paid_value): StoreSales
    {
        $this->paid_value = $paid_value;
        return $this;
    }

    public function getOrderDimona(): string
    {
        return $this->orderDimona;
    }

    public function setOrderDimona(string $orderDimona): StoreSales
    {
        $this->orderDimona = $orderDimona;
        return $this;
    }
}