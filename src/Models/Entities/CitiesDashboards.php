<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="citiesDashboards")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CitiesDashboardsRepository")
 */
class CitiesDashboards
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private $city;

    /**
     * @Column(type="string")
     */
    private string $dashboard;

    public function getId(): int
    {
        return $this->id;
    }

    public function getState(): State
    {
        return $this->direstatectory;
    }

    public function setState(State $state): CitiesDashboards
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }
 
    public function setCity(City $city): CitiesDashboards
    {
        $this->city = $city;
        return $this;
    }
    
    public function getDashboard(): string
    {
        return $this->dashboard;
    }

    public function setDashboard(string $dashboard): CitiesDashboards
    {
        $this->dashboard = $dashboard;
        return $this;
    }
}