<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="iuguTokens")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IuguTokensRepository")
 */
class IuguTokens
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $directory;

    /**
     * @Column(type="string")
     */
    private string $token;

    /**
     * @Column(type="string")
     */
    private string $account;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDirectory(): int
    {
        return $this->directory;
    }

    public function setDirectory(int $directory): IuguTokens
    {
        $this->directory = $directory;
        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): IuguTokens
    {
        $this->token = $token;
        return $this;
    }

    public function getAccount(): string
    {
        return $this->account;
    }

    public function setAccount(string $account): IuguTokens
    {
        $this->account = $account;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): IuguTokens
    {
        $this->created_at = $created_at;
        return $this;
    }
}