<?php

namespace App\Models\Entities;

use App\Helpers\Utils;

/**
 * @Entity @Table(name="elected")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ElectedRepository")
 */
class Elected
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $createdAt;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @Column(type="string")
     */
    private string $email;

    /**
     * @Column(type="string")
     */
    private string $cpf;

    /**
     * @ManyToOne(targetEntity="Post")
     * @JoinColumn(name="post", referencedColumnName="id")
     */
    private Post $post;

    /**
     * @Column(type="integer")
     */
    private int $year;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     */
    private State $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     */
    private City $city;

    /**
     * @Column(type="string")
     */
    private string $votes;

    /**
     * @Column(type="boolean")
     */
    private bool $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): \Datetime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\Datetime $createdAt): Elected
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): Elected
    {
        $this->user = $user;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Elected
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Elected
    {
        $this->email = $email;
        return $this;
    }

    public function getCPF(): string
    {
        return $this->cpf;
    }

    public function setCPF(string $cpf): Elected
    {
        $cpf = Utils::onlyNumbers($cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        $this->cpf = $cpf;
        return $this;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function setPost(Post $post): Elected
    {
        $this->post = $post;
        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): Elected
    {
        $this->year = $year;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Elected
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): Elected
    {
        $this->city = $city;
        return $this;
    }

    public function getVotes(): string
    {
        return $this->votes;
    }

    public function setVotes(string $votes): Elected
    {
        $this->votes = $votes;
        return $this;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): Elected
    {
        $this->status = $status;
        return $this;
    }
}
