<?php

namespace App\Models\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity @Table(name="question")
 * @ORM @Entity(repositoryClass="App\Models\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="responsible", referencedColumnName="id")
     */
    private UserAdmin $responsible;

    /**
     * @Column(type="text")
     */
    private string $text = '';

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @OneToMany(targetEntity="QuestionOption", mappedBy="question")
     */
    private Collection $options;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResponsible(): UserAdmin
    {
        return $this->responsible;
    }

    public function setResponsible(UserAdmin $responsible): Question
    {
        $this->responsible = $responsible;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): Question
    {
        $this->text = trim($text);
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Question
    {
        $this->active = $active;
        return $this;
    }

    public function changeActive(): Question
    {
        if (!$this->active) {
            $this->active = 1;
            return $this;
        }
        $this->active = 0;
        return $this;
    }

    public function activeStr(): string
    {
        if (1 == $this->active) return "Ativo";
        return "Inativo";
    }

    public function getOptions(): Collection
    {
        return $this->options;
    }

}