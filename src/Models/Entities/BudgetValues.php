<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="budgetValues")
 * @ORM @Entity(repositoryClass="App\Models\Repository\BudgetValuesRepository")
 */

class BudgetValues
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Budget")
     * @JoinColumn(name="budget", referencedColumnName="id")
     */
    private ?Budget $budget = null;

    /**
     * @Column(type="integer")
     */
    private int $month = 0;

    /**
     * @Column(type="float")
     */
    private float $value = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(?Budget $budget): BudgetValues
    {
        $this->budget = $budget;
        return $this;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function setMonth(int $month): BudgetValues
    {
        $this->month = $month;
        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): BudgetValues
    {
        $this->value = $value;
        return $this;
    }
}
