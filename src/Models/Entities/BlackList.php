<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="BlackList")
 * @ORM @Entity(repositoryClass="App\Models\Repository\BlackListRepository")
 */
class BlackList
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $cpf;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="affiliated", referencedColumnName="id")
     */
    private User $affiliated;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @Column(type="datetime")
     */
    private ?\DateTime $created;


    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): BlackList
    {
        $this->cpf = Utils::onlyNumbers($cpf);
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser($user): BlackList
    {
        $this->user = $user;
        return $this;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    public function getAffiliated(): User
    {
        return $this->affiliated;
    }

    public function setAffiliated($affiliated): BlackList
    {
        $this->affiliated = $affiliated;
        return $this;
    }



}