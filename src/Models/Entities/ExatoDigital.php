<?php

namespace App\Models\Entities;

use App\Helpers\Utils;

/**
 * @Entity @Table(name="exatoDigital")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ExatoDigitalRepository")
 */
class ExatoDigital
{

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="string")
     */
    private string $uid = '';

    /**
     * @Column(type="string")
     */
    private string $doc = '';

    /**
     * @Column(type="string")
     */
    private string $searchDescription = '';

    /**
     * @Column(type="string")
     */
    private string $searchUrl = '';

    /**
     * @Column(type="string")
     */
    private string $status = '';


    /**
     * @Column(type="string")
     */
    private string $pdfUrl = '';

    /**
     * @Column(type="text")
     */
    private string $message = '';


    /**
     * @Column(type="text")
     */
    private string $jsonValues = '';

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function setUid(string $uid): ExatoDigital
    {
        $this->uid = $uid;
        return $this;
    }

    public function getDoc(): string
    {
        return $this->doc;
    }

    public function setDoc(string $doc): ExatoDigital
    {
        $this->doc = Utils::onlyNumbers($doc);
        return $this;
    }

    public function getSearchDescription(): string
    {
        return $this->searchDescription;
    }

    public function setSearchDescription(string $searchDescription): ExatoDigital
    {
        $this->searchDescription = $searchDescription;
        return $this;
    }

    public function getSearchUrl(): string
    {
        return $this->searchUrl;
    }

    public function setSearchUrl(string $searchUrl): ExatoDigital
    {
        $this->searchUrl = $searchUrl;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): ExatoDigital
    {
        $this->status = $status;
        return $this;
    }

    public function getPdfUrl(): string
    {
        return $this->pdfUrl;
    }

    public function setPdfUrl(string $pdfUrl): ExatoDigital
    {
        $this->pdfUrl = $pdfUrl;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): ExatoDigital
    {
        $this->message = $message;
        return $this;
    }

    public function getJsonValues(): string
    {
        return $this->jsonValues;
    }

    public function setJsonValues(string $jsonValues): ExatoDigital
    {
        $this->jsonValues = $jsonValues;
        return $this;
    }

    public function getRiskValues(): array
    {
        if ($this->jsonValues == '') return [];
        $values = json_decode($this->jsonValues, true);
        $array = [];
        foreach ($values as $value){
            foreach ($value as $key => $value){
                if ($value > 3) $array[$key] = $value;
            }
        }
        return $array;
    }

}