<?php
namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * TbEventosPrecos
 *
 * @Table(name="tb_eventos_precos", indexes={@Index(name="idx_tb_eventos_precos_descricao", columns={"descricao"}), @Index(name="fk_tb_preco_tb_eventos_idx", columns={"tb_eventos_id"})})
 * @Entity(repositoryClass="App\Models\Repository\EventsPricesRepository")
 */
class EventsPrices
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=true)
     */
    private $tbEventosId;

    /**
     * @var string
     *
     * @Column(name="descricao", type="string", length=255, nullable=false)
     */
    private $descricao;

    /**
     * @var string|null
     *
     * @Column(name="valor", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valor;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_inicio", type="datetime", nullable=true)
     */
    private $dataInicio;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_termino", type="datetime", nullable=true)
     */
    private $dataTermino;

    /**
     * @var int|null
     *
     * @Column(name="limite_inscritos", type="integer", nullable=true)
     */
    private $limiteInscritos;

    /**
     * @var int|null
     *
     * @Column(name="limite_total", type="integer", nullable=true)
     */
    private $limiteTotal;

    /**
     * @var bool|null
     *
     * @Column(name="apenas_filiados", type="boolean", nullable=true)
     */
    private $apenasFiliados;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=0, nullable=true, options={"default"="ativo"})
     */
    private $status = 'ativo';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTbEventId(?int $tbEventosId): EventsPrices
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->descricao;
    }

    public function setDescription(?string $descricao): EventsPrices
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->valor;   
    }

    public function getValueFormated(): ?string
    {
        $valor = $this->valor;  
        return number_format($valor,2,',','.');   
        
    }

    /**
     * @param string $valor
     * @return EventsPrices
     */
    public function setValue(string $valor): EventsPrices
    {
        $this->valor = $valor;
        return $this;
    }

    public function getDateBegin(): ?\DateTime
    {
        return $this->dataInicio;
    }

    public function setDateBegin(?\DateTime $dataInicio): EventsPrices
    {
        $this->dataInicio = $dataInicio;
        return $this;
    }

    public function getDateFinal(): ?\DateTime
    {
        return $this->dataTermino;
    }

    public function setDateFinal(?\DateTime $dataTermino): EventsPrices
    {
        $this->dataTermino = $dataTermino;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limiteInscritos;
    }

    public function setLimit(?string $limiteInscritos): EventsPrices
    {
        $this->limiteInscritos = $limiteInscritos;
        return $this;
    }

    public function getLimitTotal(): ?int
    {
        return $this->limiteTotal;
    }

    public function setLimitTotal(?string $limiteTotal): EventsPrices
    {
        $this->limiteTotal = $limiteTotal;
        return $this;
    }

    public function getPublicString(): ?string
    {
        if($this->apenasFiliados == 1){
            $apenasFiliados = 'Valor para filiados';
        } else {
            $apenasFiliados = 'Valor para todos';
        }
        return $apenasFiliados;
    }

    public function getPublic(): ?int
    {
        return $this->apenasFiliados;
    }

    public function setPublic(?string $apenasFiliados): EventsPrices
    {
        $this->apenasFiliados = $apenasFiliados;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): EventsPrices
    {
        $this->status = $status;
        return $this;
    }

}
