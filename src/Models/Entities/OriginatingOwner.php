<?php

namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="originatingOwner")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OriginatingOwnerRepository")
 */
class OriginatingOwner
{
    /**
     * @var int
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id")
     */
    private $directory;

    /**
     * @ManyToOne(targetEntity="Transaction")
     * @JoinColumn(name="transaction", referencedColumnName="id")
     */
    private $transaction;

    public function getId(): int
    {
        return $this->id;
    }

    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    public function setDirectory(Directory $directory): OriginatingOwner
    {
        $this->directory = $directory;
        return $this;
    }

    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(Transaction $transaction): OriginatingOwner
    {
        $this->transaction = $transaction;
        return $this;
    }
}