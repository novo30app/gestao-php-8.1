<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="trailsOfKnowledge")
 * @ORM @Entity(repositoryClass="App\Models\Repository\TrailsOfKnowledgeRepository")
 */
class TrailsOfKnowledge
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @Column(type="integer")
     */
    private int $module;

    /**
     * @Column(type="string")
     */
    private string $title;

    /**
     * @Column(type="string")
     */
    private string $description;

    /**
     * @Column(type="string")
     */
    private string $link;

    /**
     * @Column(type="string")
     */
    private string $material = '';

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): TrailsOfKnowledge
    {
        $this->created = $created;
        return $this;
    }

    public function getModule(): int
    {
        return $this->module;
    }

    public function getModuleString(): string
    {
        $module = match($this->module) {
            1 => 'Introdução',
            2 => 'Consultas',
            3 => 'Eventos',
            4 => 'Entendendo o Dashboard',
            5 => 'Financeiro',
            6 => 'Comunicação',
            7 => 'Acesso à Novos Usuários',
            8 => 'Jurídico Diretórios',
            9 => 'Prestação de Contas Diretórios',
            10 => 'Estratégias de captação de recursos',
            11 => 'Estratégias do Dia-a-dia',
            default => ''
        };
        return $module;
    }

    public function setModule(int $module): TrailsOfKnowledge
    {
        $this->module = $module;
        return $this;
    }
    
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): TrailsOfKnowledge
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): TrailsOfKnowledge
    {
        $this->description = $description;
        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): TrailsOfKnowledge
    {
        $this->link = $link;
        return $this;
    }

    public function getMaterial(): string
    {
        return $this->material;
    }

    public function setMaterial(string $material): TrailsOfKnowledge
    {
        $this->material = $material;
        return $this;
    }

    public function getActive(): string
    {
        return $this->active;
    }

    public function setActive(string $active): TrailsOfKnowledge
    {
        $this->active = $active;
        return $this;
    }
}