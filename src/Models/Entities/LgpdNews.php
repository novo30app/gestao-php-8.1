<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="lgpdNews")
 * @ORM @Entity(repositoryClass="App\Models\Repository\LgpdNewsRepository")
 */
class LgpdNews
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     * @Column(name="created_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @var string|null
     * @Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string|null
     * @Column(name="file", type="string")
     */
    private $file;

    /**
     * @var bool|null
     * @Column(name="status", type="boolean")
     */
    private $status;

    public function getId()
    {
        return $this->id;
    }

    public function getCreated_at(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreated_at(?\DateTime $created_at): LgpdNews
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): LgpdNews
    {
        $this->title = $title;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): LgpdNews
    {
        $this->file = $file;
        return $this;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): LgpdNews
    {
        $this->status = $status;
        return $this;
    }
}