<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="interactionTypes")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionTypeRepository")
 */
class InteractionType
{

    const MEETING = 6;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): InteractionType
    {
        $this->name = $name;
        return $this;
    }

}