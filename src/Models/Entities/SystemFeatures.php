<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="systemFeatures")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SystemFeaturesRepository")
 */
class SystemFeatures
{

    const DIRETORIOS_EVENTOS_VISUALIZACAO = 643;
    const DIRETORIOS_EVENTOS_CRIACAO_EDICAO = 644;
    const DIRETORIOS_EVENTOS_REMOCAO = 645;
    const DIRETORIOS_EVENTOS_EXPORTACAO = 646;
    const DIRETORIOS_TRILHASDOCONHECIMENTO_VISUALIZACAO = 647;
    const DIRETORIOS_TRILHASDOCONHECIMENTO_CRIACAO_EDICAO = 648;
    const DIRETORIOS_TRILHASDOCONHECIMENTO_REMOCAO = 649;
    const DIRETORIOS_TRILHASDOCONHECIMENTO_EXPORTACAO = 650;
    const DIRETORIOS_REUNIOES_VISUALIZACAO = 651;
    const DIRETORIOS_REUNIOES_CRIACAO_EDICAO = 652;
    const DIRETORIOS_REUNIOES_REMOCAO = 653;
    const DIRETORIOS_REUNIOES_EXPORTACAO = 654;
    const DIRETORIOS_INDICACAODEDIRETORIOS_VISUALIZACAO = 655;
    const DIRETORIOS_INDICACAODEDIRETORIOS_CRIACAO_EDICAO = 656;
    const DIRETORIOS_INDICACAODEDIRETORIOS_REMOCAO = 657;
    const DIRETORIOS_INDICACAODEDIRETORIOS_EXPORTACAO = 658;
    const DIRETORIOS_RELATORIOFINANCEIRO_VISUALIZACAO = 659;
    const DIRETORIOS_RELATORIOFINANCEIRO_CRIACAO_EDICAO = 660;
    const DIRETORIOS_RELATORIOFINANCEIRO_REMOCAO = 661;
    const DIRETORIOS_RELATORIOFINANCEIRO_EXPORTACAO = 662;
    const DIRETORIOS_CONVENCOES_VISUALIZACAO = 663;
    const DIRETORIOS_CONVENCOES_CRIACAO_EDICAO = 664;
    const DIRETORIOS_CONVENCOES_REMOCAO = 665;
    const DIRETORIOS_CONVENCOES_EXPORTACAO = 666;
    const CONSULTAS_ADIMP_INADIMP_VISUALIZACAO = 667;
    const CONSULTAS_ADIMP_INADIMP_CRIACAO_EDICAO = 668;
    const CONSULTAS_ADIMP_INADIMP_REMOCAO = 669;
    const CONSULTAS_ADIMP_INADIMP_EXPORTACAO = 670;
    const CONSULTAS_GESTAODECADASTROS_VISUALIZACAO = 671;
    const CONSULTAS_GESTAODECADASTROS_CRIACAO_EDICAO = 672;
    const CONSULTAS_GESTAODECADASTROS_REMOCAO = 673;
    const CONSULTAS_GESTAODECADASTROS_EXPORTACAO = 674;
    const CONSULTAS_ULTIMASFILIACOES_VISUALIZACAO = 675;
    const CONSULTAS_ULTIMASFILIACOES_CRIACAO_EDICAO = 676;
    const CONSULTAS_ULTIMASFILIACOES_REMOCAO = 677;
    const CONSULTAS_ULTIMASFILIACOES_EXPORTACAO = 678;
    const CONSULTAS_FILIADOSEMPERIODODEIMPUGNACAO_VISUALIZACAO = 679;
    const CONSULTAS_FILIADOSEMPERIODODEIMPUGNACAO_CRIACAO_EDICAO = 680;
    const CONSULTAS_FILIADOSEMPERIODODEIMPUGNACAO_REMOCAO = 681;
    const CONSULTAS_FILIADOSEMPERIODODEIMPUGNACAO_EXPORTACAO = 682;
    const CONSULTAS_DESFILIACOES_VISUALIZACAO = 683;
    const CONSULTAS_DESFILIACOES_CRIACAO_EDICAO = 684;
    const CONSULTAS_DESFILIACOES_REMOCAO = 685;
    const CONSULTAS_DESFILIACOES_EXPORTACAO = 686;
    const CONSULTAS_DIRIGENTES_VISUALIZACAO = 687;
    const CONSULTAS_DIRIGENTES_CRIACAO_EDICAO = 688;
    const CONSULTAS_DIRIGENTES_REMOCAO = 689;
    const CONSULTAS_DIRIGENTES_EXPORTACAO = 690;
    const CONSULTAS_DIRETORIOS_VISUALIZACAO = 691;
    const CONSULTAS_DIRETORIOS_CRIACAO_EDICAO = 692;
    const CONSULTAS_DIRETORIOS_REMOCAO = 693;
    const CONSULTAS_DIRETORIOS_EXPORTACAO = 694;
    const CONSULTAS_CANDIDATOS_VISUALIZACAO = 695;
    const CONSULTAS_CANDIDATOS_CRIACAO_EDICAO = 696;
    const CONSULTAS_CANDIDATOS_REMOCAO = 697;
    const CONSULTAS_CANDIDATOS_EXPORTACAO = 698;
    const CADASTROS_ACESSOSADMIN_VISUALIZACAO = 699;
    const CADASTROS_ACESSOSADMIN_CRIACAO_EDICAO = 700;
    const CADASTROS_ACESSOSADMIN_REMOCAO = 701;
    const CADASTROS_ACESSOSADMIN_EXPORTACAO = 702;
    const CADASTROS_FORNECEDORES_VISUALIZACAO = 703;
    const CADASTROS_FORNECEDORES_CRIACAO_EDICAO = 704;
    const CADASTROS_FORNECEDORES_REMOCAO = 705;
    const CADASTROS_FORNECEDORES_EXPORTACAO = 706;
    const CADASTROS_CONTRATOS_VISUALIZACAO = 707;
    const CADASTROS_CONTRATOS_CRIACAO_EDICAO = 708;
    const CADASTROS_CONTRATOS_REMOCAO = 709;
    const CADASTROS_CONTRATOS_EXPORTACAO = 710;
    const DIRETORIOS_CONTASDEDIRETORIOS_VISUALIZACAO = 711;
    const DIRETORIOS_CONTASDEDIRETORIOS_CRIACAO_EDICAO = 712;
    const DIRETORIOS_CONTASDEDIRETORIOS_REMOCAO = 713;
    const DIRETORIOS_CONTASDEDIRETORIOS_EXPORTACAO = 714;
    const CADASTROS_PLANODECONTAS_VISUALIZACAO = 715;
    const CADASTROS_PLANODECONTAS_CRIACAO_EDICAO = 716;
    const CADASTROS_PLANODECONTAS_REMOCAO = 717;
    const CADASTROS_PLANODECONTAS_EXPORTACAO = 718;
    const CADASTROS_ORCAMENTOS_AREA_SUB_AREA_VISUALIZACAO = 719;
    const CADASTROS_ORCAMENTOS_AREA_SUB_AREA_CRIACAO_EDICAO = 720;
    const CADASTROS_ORCAMENTOS_AREA_SUB_AREA_REMOCAO = 721;
    const CADASTROS_ORCAMENTOS_AREA_SUB_AREA_EXPORTACAO = 722;
    const CADASTROS_ORCAMENTOS_CENTRODECUSTO_VISUALIZACAO = 723;
    const CADASTROS_ORCAMENTOS_CENTRODECUSTO_CRIACAO_EDICAO = 724;
    const CADASTROS_ORCAMENTOS_CENTRODECUSTO_REMOCAO = 725;
    const CADASTROS_ORCAMENTOS_CENTRODECUSTO_EXPORTACAO = 726;
    const CADASTROS_ORCAMENTOS_ORCAMENTOANUAL_VISUALIZACAO = 727;
    const CADASTROS_ORCAMENTOS_ORCAMENTOANUAL_CRIACAO_EDICAO = 728;
    const CADASTROS_ORCAMENTOS_ORCAMENTOANUAL_REMOCAO = 729;
    const CADASTROS_ORCAMENTOS_ORCAMENTOANUAL_EXPORTACAO = 730;
    const CADASTROS_COMUNICACAO_VISUALIZACAO = 731;
    const CADASTROS_COMUNICACAO_CRIACAO_EDICAO = 732;
    const CADASTROS_COMUNICACAO_REMOCAO = 733;
    const CADASTROS_COMUNICACAO_EXPORTACAO = 734;
    const CADASTROS_GESTAODEDIRIGENTES_VISUALIZACAO = 735;
    const CADASTROS_GESTAODEDIRIGENTES_CRIACAO_EDICAO = 736;
    const CADASTROS_GESTAODEDIRIGENTES_REMOCAO = 737;
    const CADASTROS_GESTAODEDIRIGENTES_EXPORTACAO = 738;
    const CADASTROS_DOCUMENTOSLOCAIS_VISUALIZACAO = 739;
    const CADASTROS_DOCUMENTOSLOCAIS_CRIACAO_EDICAO = 740;
    const CADASTROS_DOCUMENTOSLOCAIS_REMOCAO = 741;
    const CADASTROS_DOCUMENTOSLOCAIS_EXPORTACAO = 742;
    const CADASTROS_GESTAODECOMUNICADOS_VISUALIZACAO = 743;
    const CADASTROS_GESTAODECOMUNICADOS_CRIACAO_EDICAO = 744;
    const CADASTROS_GESTAODECOMUNICADOS_REMOCAO = 745;
    const CADASTROS_GESTAODECOMUNICADOS_EXPORTACAO = 746;
    const CADASTROS_MESORREGIOES_VISUALIZACAO = 747;
    const CADASTROS_MESORREGIOES_CRIACAO_EDICAO = 748;
    const CADASTROS_MESORREGIOES_REMOCAO = 749;
    const CADASTROS_MESORREGIOES_EXPORTACAO = 750;
    const CADASTROS_CATEGORIAS_VISUALIZACAO = 751;
    const CADASTROS_CATEGORIAS_CRIACAO_EDICAO = 752;
    const CADASTROS_CATEGORIAS_REMOCAO = 753;
    const CADASTROS_CATEGORIAS_EXPORTACAO = 754;
    const FILIADOS_INDICACOESDEFILIACAO_VISUALIZACAO = 755;
    const FILIADOS_INDICACOESDEFILIACAO_CRIACAO_EDICAO = 756;
    const FILIADOS_INDICACOESDEFILIACAO_REMOCAO = 757;
    const FILIADOS_INDICACOESDEFILIACAO_EXPORTACAO = 758;
    const FILIADOS_PEDIDOSDEREFILIACAO_VISUALIZACAO = 759;
    const FILIADOS_PEDIDOSDEREFILIACAO_CRIACAO_EDICAO = 760;
    const FILIADOS_PEDIDOSDEREFILIACAO_REMOCAO = 761;
    const FILIADOS_PEDIDOSDEREFILIACAO_EXPORTACAO = 762;
    const FILIADOS_PEDIDOSDEIMPUGNACAO_VISUALIZACAO = 763;
    const FILIADOS_PEDIDOSDEIMPUGNACAO_CRIACAO_EDICAO = 764;
    const FILIADOS_PEDIDOSDEIMPUGNACAO_REMOCAO = 765;
    const FILIADOS_PEDIDOSDEIMPUGNACAO_EXPORTACAO = 766;
    const FILIADOS_COMOCONHECEU_VISUALIZACAO = 767;
    const FILIADOS_COMOCONHECEU_CRIACAO_EDICAO = 768;
    const FILIADOS_COMOCONHECEU_REMOCAO = 769;
    const FILIADOS_COMOCONHECEU_EXPORTACAO = 770;
    const FILIADOS_QUEMINDICOU_VISUALIZACAO = 771;
    const FILIADOS_QUEMINDICOU_CRIACAO_EDICAO = 772;
    const FILIADOS_QUEMINDICOU_REMOCAO = 773;
    const FILIADOS_QUEMINDICOU_EXPORTACAO = 774;
    const FILIADOS_AGENDAMENTOS_VISUALIZACAO = 936;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_VISUALIZACAO = 775;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_CRIACAO_EDICAO = 776;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_REMOCAO = 777;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_EXPORTACAO = 778;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_ENVIAR_PARA_BANCO = 937;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_LIBERAR_PAGAMENTO = 938;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_CANCELAR_PAGAMENTO = 939;
    const FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_REGISTRAR_PAGAMENTO_PARA_O_MESMO_DIA = 940;
    const FINANCEIRO_EXTRATOS_VISUALIZACAO = 779;
    const FINANCEIRO_EXTRATOS_CRIACAO_EDICAO = 780;
    const FINANCEIRO_EXTRATOS_REMOCAO = 781;
    const FINANCEIRO_EXTRATOS_EXPORTACAO = 782;
    const FINANCEIRO_APROVACOESPENDENTES_VISUALIZACAO = 783;
    const FINANCEIRO_APROVACOESPENDENTES_CRIACAO_EDICAO = 784;
    const FINANCEIRO_APROVACOESPENDENTES_REMOCAO = 785;
    const FINANCEIRO_APROVACOESPENDENTES_EXPORTACAO = 786;
    const FINANCEIRO_CONCILIACAO_IMPORTACAO_VISUALIZACAO = 787;
    const FINANCEIRO_CONCILIACAO_IMPORTACAO_CRIACAO_EDICAO = 788;
    const FINANCEIRO_CONCILIACAO_IMPORTACAO_REMOCAO = 789;
    const FINANCEIRO_CONCILIACAO_IMPORTACAO_EXPORTACAO = 790;
    const FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO = 791;
    const FINANCEIRO_CONCILIACAO_PENDENTES_CRIACAO_EDICAO = 792;
    const FINANCEIRO_CONCILIACAO_PENDENTES_REMOCAO = 793;
    const FINANCEIRO_CONCILIACAO_PENDENTES_EXPORTACAO = 794;
    const FINANCEIRO_CONCILIACAO_EXTRATOS_VISUALIZACAO = 795;
    const FINANCEIRO_CONCILIACAO_EXTRATOS_CRIACAO_EDICAO = 796;
    const FINANCEIRO_CONCILIACAO_EXTRATOS_REMOCAO = 797;
    const FINANCEIRO_CONCILIACAO_EXTRATOS_EXPORTACAO = 798;
    const FINANCEIRO_CONCILIACAO_IUGU_VISUALIZACAO = 799;
    const FINANCEIRO_CONCILIACAO_IUGU_CRIACAO_EDICAO = 800;
    const FINANCEIRO_CONCILIACAO_IUGU_REMOCAO = 801;
    const FINANCEIRO_CONCILIACAO_IUGU_EXPORTACAO = 802;
    const FINANCEIRO_CONCILIACAO_REDE_VISUALIZACAO = 803;
    const FINANCEIRO_CONCILIACAO_REDE_CRIACAO_EDICAO = 804;
    const FINANCEIRO_CONCILIACAO_REDE_REMOCAO = 805;
    const FINANCEIRO_CONCILIACAO_REDE_EXPORTACAO = 806;
    const FINANCEIRO_RELATORIODETRANSACOES_VISUALIZACAO = 807;
    const FINANCEIRO_RELATORIODETRANSACOES_CRIACAO_EDICAO = 808;
    const FINANCEIRO_RELATORIODETRANSACOES_REMOCAO = 809;
    const FINANCEIRO_RELATORIODETRANSACOES_EXPORTACAO = 810;
    const FINANCEIRO_DOADORES_VISUALIZACAO = 811;
    const FINANCEIRO_DOADORES_CRIACAO_EDICAO = 812;
    const FINANCEIRO_DOADORES_REMOCAO = 813;
    const FINANCEIRO_DOADORES_EXPORTACAO = 814;
    const FINANCEIRO_RECIBOS_VISUALIZACAO = 815;
    const FINANCEIRO_RECIBOS_CRIACAO_EDICAO = 816;
    const FINANCEIRO_RECIBOS_REMOCAO = 817;
    const FINANCEIRO_RECIBOS_EXPORTACAO = 818;
    const ORIENTACOES_PROCESSOSFINANCEIROS_VISUALIZACAO = 819;
    const ORIENTACOES_PROCESSOSFINANCEIROS_CRIACAO_EDICAO = 820;
    const ORIENTACOES_PROCESSOSFINANCEIROS_REMOCAO = 821;
    const ORIENTACOES_PROCESSOSFINANCEIROS_EXPORTACAO = 822;
    const ORIENTACOES_JURIDICOLEGAL_VISUALIZACAO = 823;
    const ORIENTACOES_JURIDICOLEGAL_CRIACAO_EDICAO = 824;
    const ORIENTACOES_JURIDICOLEGAL_REMOCAO = 825;
    const ORIENTACOES_JURIDICOLEGAL_EXPORTACAO = 826;
    const ORIENTACOES_MINUTASCONTRATUAIS_VISUALIZACAO = 827;
    const ORIENTACOES_MINUTASCONTRATUAIS_CRIACAO_EDICAO = 828;
    const ORIENTACOES_MINUTASCONTRATUAIS_REMOCAO = 829;
    const ORIENTACOES_MINUTASCONTRATUAIS_EXPORTACAO = 830;
    const ORIENTACOES_LGPDNEWS_VISUALIZACAO = 831;
    const ORIENTACOES_LGPDNEWS_CRIACAO_EDICAO = 832;
    const ORIENTACOES_LGPDNEWS_REMOCAO = 833;
    const ORIENTACOES_LGPDNEWS_EXPORTACAO = 834;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_VISUALIZACAO = 835;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_CRIACAO_EDICAO = 836;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_REMOCAO = 837;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_EXPORTACAO = 838;
    const SUCESSODOFILIADO_NOVOMOBILIZA_BENEFICIOS_VISUALIZACAO = 839;
    const SUCESSODOFILIADO_NOVOMOBILIZA_BENEFICIOS_CRIACAO_EDICAO = 840;
    const SUCESSODOFILIADO_NOVOMOBILIZA_BENEFICIOS_REMOCAO = 841;
    const SUCESSODOFILIADO_NOVOMOBILIZA_BENEFICIOS_EXPORTACAO = 842;
    const SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_VISUALIZACAO = 843;
    const SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_CRIACAO_EDICAO = 844;
    const SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_REMOCAO = 845;
    const SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_EXPORTACAO = 846;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_VISUALIZACAO = 847;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_CRIACAO_EDICAO = 848;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_REMOCAO = 849;
    const SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_EXPORTACAO = 850;
    const SUCESSODOFILIADO_CAMPANHASDEFILIACAO_VISUALIZACAO = 851;
    const SUCESSODOFILIADO_CAMPANHASDEFILIACAO_CRIACAO_EDICAO = 852;
    const SUCESSODOFILIADO_CAMPANHASDEFILIACAO_REMOCAO = 853;
    const SUCESSODOFILIADO_CAMPANHASDEFILIACAO_EXPORTACAO = 854;
    const SUCESSODOFILIADO_CAMPANHASDONOVO_VISUALIZACAO = 855;
    const SUCESSODOFILIADO_CAMPANHASDONOVO_CRIACAO_EDICAO = 856;
    const SUCESSODOFILIADO_CAMPANHASDONOVO_REMOCAO = 857;
    const SUCESSODOFILIADO_CAMPANHASDONOVO_EXPORTACAO = 858;
    const EDUCACAO_INSTITUTOLIBERTAS_AUTOINSCRICAOMOODLE_VISUALIZACAO = 859;
    const EDUCACAO_INSTITUTOLIBERTAS_AUTOINSCRICAOMOODLE_CRIACAO_EDICAO = 860;
    const EDUCACAO_INSTITUTOLIBERTAS_AUTOINSCRICAOMOODLE_REMOCAO = 861;
    const EDUCACAO_INSTITUTOLIBERTAS_AUTOINSCRICAOMOODLE_EXPORTACAO = 862;
    const EDUCACAO_INSTITUTOLIBERTAS_CURSOS_VISUALIZACAO = 863;
    const EDUCACAO_INSTITUTOLIBERTAS_CURSOS_CRIACAO_EDICAO = 864;
    const EDUCACAO_INSTITUTOLIBERTAS_CURSOS_REMOCAO = 865;
    const EDUCACAO_INSTITUTOLIBERTAS_CURSOS_EXPORTACAO = 866;
    const EDUCACAO_INSTITUTOLIBERTAS_CUPONS_VISUALIZACAO = 867;
    const EDUCACAO_INSTITUTOLIBERTAS_CUPONS_CRIACAO_EDICAO = 868;
    const EDUCACAO_INSTITUTOLIBERTAS_CUPONS_REMOCAO = 869;
    const EDUCACAO_INSTITUTOLIBERTAS_CUPONS_EXPORTACAO = 870;
    const DAM_GABINETES_VISUALIZACAO = 871;
    const DAM_GABINETES_CRIACAO_EDICAO = 872;
    const DAM_GABINETES_REMOCAO = 873;
    const DAM_GABINETES_EXPORTACAO = 874;
    const DAM_PROJETOS_VISUALIZACAO = 875;
    const DAM_PROJETOS_CRIACAO_EDICAO = 876;
    const DAM_PROJETOS_REMOCAO = 877;
    const DAM_PROJETOS_EXPORTACAO = 878;
    const DAM_TAGS_VISUALIZACAO = 879;
    const DAM_TAGS_CRIACAO_EDICAO = 880;
    const DAM_TAGS_REMOCAO = 881;
    const DAM_TAGS_EXPORTACAO = 882;
    const DAM_DOSSIE_VISUALIZACAO = 883;
    const DAM_DOSSIE_CRIACAO_EDICAO = 884;
    const DAM_DOSSIE_REMOCAO = 885;
    const DAM_DOSSIE_EXPORTACAO = 886;
    const DAM_INTERACOES_VISUALIZACAO = 887;
    const DAM_INTERACOES_CRIACAO_EDICAO = 888;
    const DAM_INTERACOES_REMOCAO = 889;
    const DAM_INTERACOES_EXPORTACAO = 890;
    const ADMINISTRATIVO_ORCADOXREALIZADO_VISUALIZACAO = 891;
    const ADMINISTRATIVO_ORCADOXREALIZADO_CRIACAO_EDICAO = 892;
    const ADMINISTRATIVO_ORCADOXREALIZADO_REMOCAO = 893;
    const ADMINISTRATIVO_ORCADOXREALIZADO_EXPORTACAO = 894;
    const GESTAO_ACESSOS_VISUALIZACAO = 895;
    const GESTAO_ACESSOS_CRIACAO_EDICAO = 896;
    const GESTAO_ACESSOS_REMOCAO = 897;
    const GESTAO_ACESSOS_EXPORTACAO = 898;
    const GESTAO_PAINELDEGESTAO_VISUALIZACAO = 899;
    const GESTAO_PAINELDEGESTAO_CRIACAO_EDICAO = 900;
    const GESTAO_PAINELDEGESTAO_REMOCAO = 901;
    const GESTAO_PAINELDEGESTAO_EXPORTACAO = 902;
    const GESTAO_PLANEJAMENTOELEITORAL_VISUALIZACAO = 903;
    const GESTAO_PLANEJAMENTOELEITORAL_CRIACAO_EDICAO = 904;
    const GESTAO_PLANEJAMENTOELEITORAL_REMOCAO = 905;
    const GESTAO_PLANEJAMENTOELEITORAL_EXPORTACAO = 906;
    const OUVIDORIA_SOLICITACOES_VISUALIZACAO = 907;
    const OUVIDORIA_SOLICITACOES_CRIACAO_EDICAO = 908;
    const OUVIDORIA_SOLICITACOES_REMOCAO = 909;
    const OUVIDORIA_SOLICITACOES_EXPORTACAO = 910;
    const OUVIDORIA_RESPOSTASFREQUENTES_VISUALIZACAO = 911;
    const OUVIDORIA_RESPOSTASFREQUENTES_CRIACAO_EDICAO = 912;
    const OUVIDORIA_RESPOSTASFREQUENTES_REMOCAO = 913;
    const OUVIDORIA_RESPOSTASFREQUENTES_EXPORTACAO = 914;
    const CADASTROS_PERMISSOES_VISUALIZACAO = 915;
    const CADASTROS_PERMISSOES_CRIACAO_EDICAO = 916;
    const CADASTROS_PERMISSOES_REMOCAO = 917;
    const CADASTROS_PERMISSOES_EXPORTACAO = 918;
    const CADASTROS_SETORES_VISUALIZACAO = 919;
    const CADASTROS_SETORES_CRIACAO_EDICAO = 920;
    const CADASTROS_SETORES_REMOCAO = 921;
    const CADASTROS_SETORES_EXPORTACAO = 922;
    const CADASTROS_TIPOSDEUSUARIOS_VISUALIZACAO = 923;
    const CADASTROS_TIPOSDEUSUARIOS_CRIACAO_EDICAO = 924;
    const CADASTROS_TIPOSDEUSUARIOS_REMOCAO = 925;
    const CADASTROS_TIPOSDEUSUARIOS_EXPORTACAO = 926;
    const CADASTROS_FUNCIONALIDADESDOSISTEMA_VISUALIZACAO = 927;
    const CADASTROS_FUNCIONALIDADESDOSISTEMA_CRIACAO_EDICAO = 928;
    const CADASTROS_FUNCIONALIDADESDOSISTEMA_REMOCAO = 929;
    const CADASTROS_FUNCIONALIDADESDOSISTEMA_EXPORTACAO = 930;
    const INICIO = 931;

    const EDUCACAO_INSTITUTOLIBERTAS_MOODLEREDIRECTS_VISUALIZACAO = 932;
    const EDUCACAO_INSTITUTOLIBERTAS_MOODLEREDIRECTS_CRIACAO_EDICAO = 933;
    const EDUCACAO_INSTITUTOLIBERTAS_MOODLEREDIRECTS_REMOCAO = 934;
    const EDUCACAO_INSTITUTOLIBERTAS_MOODLEREDIRECTS_EXPORTACAO = 935;

    const JUVENTUDE_CONSULTAS_VISUALIZACAO = 941;
    const JUVENTUDE_CONSULTAS_CRIACAO_EDICAO = 942;
    const JUVENTUDE_CONSULTAS_REMOCAO = 943;
    const JUVENTUDE_CONSULTAS_EXPORTACAO = 944;

    const JUVENTUDE_REUNIOES_VISUALIZACAO = 945;
    const JUVENTUDE_REUNIOES_CRIACAO_EDICAO = 946;
    const JUVENTUDE_REUNIOES_REMOCAO = 947;
    const JUVENTUDE_REUNIOES_EXPORTACAO = 948;


    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): SystemFeatures
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): SystemFeatures
    {
        $this->user = $user;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): SystemFeatures
    {
        $this->name = $name;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): SystemFeatures
    {
        $this->active = $active;
        return $this;
    }
}