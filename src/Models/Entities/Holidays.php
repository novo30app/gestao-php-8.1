<?php

namespace App\Models\Entities;

/**
 * @Entity(repositoryClass="App\Models\Repository\HolidaysRepository")
 * @Table(name="tb_feriados")
 * @ORM 
 */

 class Holidays
 {
   /**
    * @Id
    * @GeneratedValue 
    * @Column(type="integer")
    */
    private $id;

    /**
     * @Column(type="string")
     */
    private $data;

    /**
     * @Column(type="string")
     */
    private $nome;

    /**
     * @Column(type="string")
     */
    private $nível;

    public function getId(): int
    {
        return $this->id;
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function setData(string $data): Holidays
    {
        $this->data = $data;
        return $this;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): Holidays
    {
        $this->nome = $nome;
        return $this;
    }

    public function getNível(): string
    {
        return $this->nível;
    }

    public function setNível(string $nível): Holidays
    {
        $this->nível = $nível;
        return $this;
    }
 }