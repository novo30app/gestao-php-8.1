<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="paymentRequirementsTravelPackage")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PaymentRequirementsTravelPackageRepository")
 */
class PaymentRequerimentsTravelPackage
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @ManyToOne(targetEntity="PaymentRequirements", inversedBy="travels")
	 * @JoinColumn(name="paymentRequirements", referencedColumnName="id")
	 */
	private PaymentRequirements $paymentRequirements;
	
	/**
	 * @Column(type="string")
	 */
	private string $name = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $cpf = '';
	
	/**
	 * @Column(type="integer")
	 */
	private int $bond = 0;
	
	/**
	 * @Column(type="string")
	 */
	private string $reason = '';
	
	/**
	 * @Column(type="float")
	 */
	private float $value = 0;
	
	/**
	 * @Column(type="string")
	 */
	private string $file = '';
	
	/**
	 * @Column(type="integer", options={"default"="1"})
	 */
	private int $type = 0;
	
	/**
	 * @Column(type="boolean", options={"default"="1"})
	 */
	private ?bool $active = null;
	
	/**
     * @ManyToOne(targetEntity="CostCenter")
     * @JoinColumn(name="costCenter", referencedColumnName="id")
     */
    private ?CostCenter $costCenter;

	/**
	 * @Column(type="string")
	 */
	private string $airlineCompany = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $ticketLocator = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $flightRoute = '';

	/**
	 * @Column(type="date")
	 */
	private ?\DateTime $flightDate;
	
	/**
	 * @Column(type="string")
	 */
	private string $hotelCompany = '';

	/**
	 * @Column(type="string")
	 */
	private string $invoiceNumber = '';	

	/**
	 * @Column(type="date")
	 */
	private ?\DateTime $startDate;

	/**
	 * @Column(type="date")
	 */
	private ?\DateTime $endDate;

	public function getId(): ?int
	{
		return $this->id;
	}
	
	public function getPaymentRequirements(): PaymentRequirements
	{
		return $this->paymentRequirements;
	}
	
	public function setPaymentRequirements(PaymentRequirements $paymentRequirements): PaymentRequerimentsTravelPackage
	{
		$this->paymentRequirements = $paymentRequirements;
		return $this;
	}
	
	public function getName(): string
	{
		return $this->name;
	}
	
	public function setName(string $name): PaymentRequerimentsTravelPackage
	{
		$this->name = $name;
		return $this;
	}
	
	public function getCpf(): string
	{
		return $this->cpf;
	}
	
	public function setCpf(string $cpf): PaymentRequerimentsTravelPackage
	{
		$this->cpf = $cpf;
		return $this;
	}
	
	public function getBond(): int
	{
		return $this->bond;
	}
	
	public function setBond(int $bond): PaymentRequerimentsTravelPackage
	{
		$this->bond = $bond;
		return $this;
	}
	
	public function getReason(): string
	{
		return $this->reason;
	}
	
	public function setReason(string $reason): PaymentRequerimentsTravelPackage
	{
		$this->reason = $reason;
		return $this;
	}
	
	public function getValue(): float|int
	{
		return $this->value;
	}
	
	public function setValue(float|int $value): PaymentRequerimentsTravelPackage
	{
		$this->value = $value;
		return $this;
	}
	
	public function getFile(): string
	{
		return Utils::formatAttachment($this->file);
	}
	
	public function setFile(string $file): PaymentRequerimentsTravelPackage
	{
		$this->file = $file;
		return $this;
	}

	public function isActive(): bool
	{
		return $this->active;
	}

	public function setActive(bool $active): PaymentRequerimentsTravelPackage
	{
		$this->active = $active;
		return $this;
	}

	public function getType(): int
	{
		return $this->type;
	}

	public function setType(int $type): PaymentRequerimentsTravelPackage
	{
		$this->type = $type;
		return $this;
	}

	public function getCostCenter(): ?CostCenter
    {
        return $this->costCenter;
    }

    public function setCostCenter(CostCenter $costCenter): PaymentRequerimentsTravelPackage
    {
        $this->costCenter = $costCenter;
        return $this;
    }

	public function getAirlineCompany(): string
	{
		return $this->airlineCompany;
	}

	public function setAirlineCompany(string $airlineCompany): PaymentRequerimentsTravelPackage
	{
		$this->airlineCompany = $airlineCompany;
		return $this;
	}	

	public function getTicketLocator(): string
	{
		return $this->ticketLocator;
	}

	public function setTicketLocator(string $ticketLocator): PaymentRequerimentsTravelPackage
	{
		$this->ticketLocator = $ticketLocator;
		return $this;
	}

	public function getFlightRoute(): string
	{
		return $this->flightRoute;
	}

	public function setFlightRoute(string $flightRoute): PaymentRequerimentsTravelPackage
	{
		$this->flightRoute = $flightRoute;
		return $this;
	}

	public function getFlightDate(): ?\DateTime
	{
		return $this->flightDate;
	}

	public function setFlightDate(\DateTime $flightDate): PaymentRequerimentsTravelPackage
	{
		$this->flightDate = $flightDate;
		return $this;
	}

	public function getHotelCompany(): string
	{
		return $this->hotelCompany;
	}

	public function setHotelCompany(string $hotelCompany): PaymentRequerimentsTravelPackage
	{
		$this->hotelCompany = $hotelCompany;
		return $this;
	}

	public function getInvoiceNumber(): string
	{
		return $this->invoiceNumber;
	}

	public function setInvoiceNumber(string $invoiceNumber): PaymentRequerimentsTravelPackage
	{
		$this->invoiceNumber = $invoiceNumber;
		return $this;
	}

	public function getStartDate(): ?\DateTime
	{
		return $this->startDate;
	}

	public function setStartDate(\DateTime $startDate): PaymentRequerimentsTravelPackage
	{
		$this->startDate = $startDate;
		return $this;
	}

	public function getEndDate(): ?\DateTime
	{
		return $this->endDate;
	}

	public function setEndDate(\DateTime $endDate): PaymentRequerimentsTravelPackage
	{
		$this->endDate = $endDate;
		return $this;
	}
}