<?php


namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * DirectoryAreaCommunicated
 *
 * @Entity @Table(name="directoryAreaCommunicated")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryAreaCommunicatedRepository")
 */

class DirectoryAreaCommunicated
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="string", name="date", nullable=true)
     * @var string
     */
    private $date;

    /**
     * @var int|null
     *
     * @Column(name="userAdmin", type="integer", nullable=false)
     */
    private $userAdmin;

    /**
     * @var string|null
     *
     * @Column(name="page", type="string", length=25, nullable=false)
     */
    private $page;

    /**
     * @var string|null
     *
     * @Column(name="theme", type="string", length=150, nullable=false)
     */
    private $theme;

    /**
     * @Column(name="content", type="text")
     */
    private string $content;

    /**
     * @var string|null
     *
     * @Column(name="file", type="string", length=250, nullable=true)
     */
    private $file;

    /**
     * @var string|null
     *
     * @Column(name="link", type="string", length=100, nullable=true)
     */
    private ?string $link = '';
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): DirectoryAreaCommunicated
    {
        $this->date = $date;
        return $this;
    }   

    public function getUserAdmin(): ?string
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(?string $userAdmin): DirectoryAreaCommunicated
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getPage(): ?string
    {
        return $this->page;
    }

    public function setPage(?string $page): DirectoryAreaCommunicated
    {
        $this->page = $page;
        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): DirectoryAreaCommunicated
    {
        $this->theme = $theme;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): DirectoryAreaCommunicated
    {
        $this->content = $content;
        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): DirectoryAreaCommunicated
    {
        $this->file = $file;
        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): DirectoryAreaCommunicated
    {
        $this->link = $link;
        return $this;
    }


}