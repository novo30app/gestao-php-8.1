<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
  * @Table(name="tb_pessoa_status_filiado")
 * @Entity
 */
class AffiliatedStatus
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(type="string", length=60)
     */
    private ?string $status = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $statusId = '';

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return AffiliatedStatus
     */
    public function setStatus(?string $status): AffiliatedStatus
    {
        $this->status = $status;
        return $this;
    }

    public function getStatusId(): ?string
    {
        return $this->statusId;
    }

    public function setStatusId(?string $statusId): AffiliatedStatus
    {
        $this->statusId = $statusId;
        return $this;
    }

}
