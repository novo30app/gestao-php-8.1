<?php

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * CivilState
 *
 * @Table(name="tb_estadocivil")
 * @Entity
 */
class CivilState
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="estadocivil", type="string", length=20, nullable=true)
     */
    private $estadocivil;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CivilState
     */
    public function setId(int $id): CivilState
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEstadocivil(): ?string
    {
        return $this->estadocivil;
    }

    /**
     * @param string|null $estadocivil
     * @return CivilState
     */
    public function setEstadocivil(?string $estadocivil): CivilState
    {
        $this->estadocivil = $estadocivil;
        return $this;
    }



}
