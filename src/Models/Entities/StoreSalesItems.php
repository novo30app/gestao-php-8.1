<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="storeSalesItems")
 * @ORM @Entity(repositoryClass="App\Models\Repository\StoreSalesItemsRepository")
 */
class StoreSalesItems
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="StoreSales")
     * @JoinColumn(name="sale", referencedColumnName="id")
     */
    private StoreSales $sale;

    /**
     * @Column(type="string")
     */
    private string $description;

    /**
     * @Column(type="integer")
     */
    private int $quantity;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @Column(type="decimal", precision=13, scale=4)
     * @var float
     */
    private $price;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSale(): StoreSales
    {
        return $this->sale;
    }

    public function setSale(StoreSales $sale): StoreSalesItems
    {
        $this->sale = $sale;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): StoreSalesItems
    {
        $this->description = $description;
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): StoreSalesItems
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): StoreSalesItems
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): StoreSalesItems
    {
        $this->price = $price;
        return $this;
    }
}