<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="accountingAccounts")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AccountingAccountsRepository")
 */
class AccountingAccounts
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="AccountingRecord")
     * @JoinColumn(name="accountingRecord", referencedColumnName="id")
     */
    private AccountingRecord $accountingRecord;
   
    /**
     * @Column(type="integer", name="account")
     * @var int
     */
    private $account;

    /**
     * @Column(type="decimal", name="value", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $value;

    /**
     * @Column(type="integer", name="creditAccount")
     * @var int
     */
    private $creditAccount;

    /**
     * @Column(type="decimal", name="creditValue", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $creditValue;

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccountingRecord(): AccountingRecord
    {
        return $this->accountingRecord;
    }

    public function setAccountingRecord(AccountingRecord $accountingRecord): AccountingAccounts
    {
        $this->accountingRecord = $accountingRecord;
        return $this;
    }

    public function getAccount(): int
    {
        return $this->account;
    }

    public function setAccount(int $account): AccountingAccounts
    {
        $this->account = $account;
        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getValueFormated()
    {
        return number_format($this->value, 2, ',', '.');   
    }

    public function setValue(float $value): AccountingAccounts
    {
        $this->value = $value;
        return $this;
    }

    public function getCreditAccount(): int
    {
        return $this->creditAccount;
    }

    public function setCreditAccount(int $creditAccount): AccountingAccounts
    {
        $this->creditAccount = $creditAccount;
        return $this;
    }

    public function getCreditValue(): float
    {
        return $this->creditValue;
    }

    public function getCreditValueFormated()
    {
        return number_format($this->creditValue, 2, ',', '.');   
    }

    public function setCreditValue(float $creditValue): AccountingAccounts
    {
        $this->creditValue = $creditValue;
        return $this;
    }
}