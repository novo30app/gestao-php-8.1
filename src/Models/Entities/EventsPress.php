<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;


/**
 * @Entity @Table(name="eventsPress")
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsPressRepository")
 */
class EventsPress
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="eventsId", referencedColumnName="id", nullable=true)
     */
    private Events $eventsId;

    /**
     * @Column(type="string")
     */
    private string $reporter = '';

    /**
     * @Column(type="string")
     */
    private string $vehicle = '';

    /**
     * @Column(type="string")
     */
    private string $phone = '';

    /**
     * @Column(type="boolean")
     */
    private bool $status = true;

    /**
     * @Column(type="boolean")
     */
    private bool $camera = true;

    /**
     * @Column(type="boolean")
     */
    private bool $photographer = true;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): TrailsOfKnowledge
    {
        $this->created = $created;
        return $this;
    }

    public function getEventsId(): Events
    {
        return $this->eventsId;
    }

    public function setEventsId(Events $eventsId): EventsPress
    {
        $this->eventsId = $eventsId;
        return $this;
    }

    public function getReporter(): string
    {
        return $this->reporter;
    }

    public function setReporter(string $reporter): EventsPress
    {
        $this->reporter = $reporter;
        return $this;
    }

    public function getVehicle(): string
    {
        return $this->vehicle;
    }

    public function setVehicle(string $vehicle): EventsPress
    {
        $this->vehicle = $vehicle;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): EventsPress
    {
        $this->phone = $phone;
        return $this;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): EventsPress
    {
        $this->status = $status;
        return $this;
    }

    public function getCamera(): bool
    {
        return $this->camera;
    }

    public function setCamera(bool $camera): EventsPress
    {
        $this->camera = $camera;
        return $this;
    }

    public function getPhotographer(): bool
    {
        return $this->photographer;
    }

    public function setPhotographer(bool $photographer): EventsPress
    {
        $this->photographer = $photographer;
        return $this;
    }
}