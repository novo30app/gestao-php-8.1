<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="zones")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ZoneRepository")
 */
class Zone
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @Column(type="string")
     */
    private string $address = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $neighborhood = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $zipCode = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $latitude = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $longitude = '';

    /**
     * @Column(type="integer")
     */
    private int $number = 0;

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $location = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): Zone
    {
        $this->city = $city;
        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): Zone
    {
        $this->address = $address;
        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): Zone
    {
        $this->neighborhood = $neighborhood;
        return $this;
    }

    public function getLat(): ?string
    {
        return $this->latitude;
    }

    public function setLat(?string $latitude): Zone
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function getLong(): ?string
    {
        return $this->longitude;
    }

    public function setLong(?string $longitude): Zone
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): Zone
    {
        $this->number = $number;
        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): Zone
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): Zone
    {
        $this->location = $location;
        return $this;
    }
}
