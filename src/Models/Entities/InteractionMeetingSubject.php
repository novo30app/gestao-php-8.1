<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="interactionMeetingSubjects")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionMeetingSubjectRepository")
 */
class InteractionMeetingSubject
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Interaction")
     * @JoinColumn(name="interaction", referencedColumnName="id")
     */
    private Interaction $interaction;

    /**
     * @Column(type="string")
     */
    private string $subject = '';


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInteraction(): Interaction
    {
        return $this->interaction;
    }

    public function setInteraction(Interaction $interaction): InteractionMeetingSubject
    {
        $this->interaction = $interaction;
        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): InteractionMeetingSubject
    {
        $this->subject = $subject;
        return $this;
    }


}