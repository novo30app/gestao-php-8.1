<?php


namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * DirectoryAreaMeeting
 *
 * @Entity @Table(name="directoryAreaMeetings")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryAreaMeetingRepository")
 */

class DirectoryAreaMeeting
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="theme", type="string", length=250, nullable=true)
     */
    private $theme;
    
    /**
     * @var string|null
     *
     * @Column(name="subject", type="text")
     */
    private $subject;

    /**
     * @var string|null
     *
     * @Column(name="date", type="string", length=50, nullable=true)
     */
    private $date;

    /**
     * @var string|null
     *
     * @Column(name="file", type="string", length=150, nullable=true)
     */
    private $file;

    /**
     * @var string|null
     *
     * @Column(name="media", type="string", length=150, nullable=true)
     */
    private $media;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=15, nullable=true)
     */
    private $status;

    /**
     * @ManyToOne(targetEntity="Sectors")
     * @JoinColumn(name="sector", referencedColumnName="id", nullable=true)
     */
    private ?Sectors $sector = null;


    public function getTheme(): ?string
    {
        return $this->theme;
    }

    public function setTheme(?string $theme): DirectoryAreaMeeting
    {
        $this->theme = $theme;
        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): DirectoryAreaMeeting
    {
        $this->subject = $subject;
        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): DirectoryAreaMeeting
    {
        $this->date = $date;
        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): DirectoryAreaMeeting
    {
        $this->file = $file;
        return $this;
    }

    public function getMedia(): ?string
    {
        return $this->media;
    }

    public function setMedia(?string $media): DirectoryAreaMeeting
    {
        $this->media = $media;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): DirectoryAreaMeeting
    {
        $this->status = $status;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): DirectoryAreaMeeting
    {
        $this->id = $id;
        return $this;
    }

    public function getSector(): ?Sectors
    {
        return $this->sector;
    }

    public function setSector(?Sectors $sector): DirectoryAreaMeeting
    {
        $this->sector = $sector;
        return $this;
    }

}