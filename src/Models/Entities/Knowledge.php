<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="tb_como_conheceu")
 * @ORM @Entity(repositoryClass="App\Models\Repository\KnowledgeRepository")
 */
class Knowledge
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;
    
    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @Column(name="subtype", type="string", length=100, nullable=true)
     */
    private $subtype;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=100, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=100, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @Column(name="evento", type="string", length=100, nullable=true)
     */
    private $evento;

    /**
     * @var int|null
     *
     * @Column(name="filiado_id", type="integer", nullable=true)
     */
    private $filiadoId;

    /**
     * @Column(name="content", type="text")
     */
    private string $content;

    /**
     * @Column(name="created_at", type="datetime")
     */
    private \DateTime $created;

    public function getType(): ?string
    {
        return $this->type;
    }
}