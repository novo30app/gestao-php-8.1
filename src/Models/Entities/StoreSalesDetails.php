<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="storeSalesDetails")
 * @ORM @Entity(repositoryClass="App\Models\Repository\StoreSalesDetailsRepository")
 */
class StoreSalesDetails
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ManyToOne(targetEntity="StoreSales")
     * @JoinColumn(name="sale", referencedColumnName="id")
     */
    private StoreSales $sale;

    /**
     * @Column(type="string")
     */
    private string $invoiceId;

    /**
     * @Column(type="decimal", precision=13, scale=4)
     * @var float
     */
    private $total;

    /**
     * @Column(type="string")
     */
    private string $status;

    /**
     * @Column(type="date")
     * @var \DateTime
     */
    private $paid_at;

    /**
     * @Column(type="string")
     */
    private string $payable_with;

    /**
     * @Column(type="string")
     */
    private string $payer_name;

    /**
     * @Column(type="string")
     */
    private string $payer_email;

    /**
     * @Column(type="string")
     */
    private string $payer_cpf_cnpj;

    /**
     * @Column(type="string")
     */
    private string $payer_phone;

    /**
     * @Column(type="string")
     */
    private string $payer_phone_prefix;

    /**
     * @Column(type="string")
     */
    private string $payer_address_zip_code;

    /**
     * @Column(type="string")
     */
    private string $payer_address_street;

    /**
     * @Column(type="string")
     */
    private string $payer_address_district;

    /**
     * @Column(type="string")
     */
    private string $payer_address_city;

    /**
     * @Column(type="string")
     */
    private string $payer_address_state;

    /**
     * @Column(type="string")
     */
    private string $payer_address_number;

    /**
     * @Column(type="string")
     */
    private ?string $payer_address_complement;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): StoreSalesDetails
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getSale(): StoreSales
    {
        return $this->sale;
    }

    public function setSale(StoreSales $sale): StoreSalesDetails
    {
        $this->sale = $sale;
        return $this;
    }

    public function getInvoiceId(): string
    {
        return $this->invoiceId;
    }

    public function setInvoiceId(string $invoiceId): StoreSalesDetails
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): StoreSalesDetails
    {
        $this->total = $total;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): StoreSalesDetails
    {
        $this->status = $status;
        return $this;
    }

    public function getPaid_at(): \DateTime
    {
        return $this->paid_at;
    }

    public function setPaid_at(\DateTime $paid_at): StoreSalesDetails
    {
        $this->paid_at = $paid_at;
        return $this;
    }

    public function getPayable_with(): string
    {
        return $this->payable_with;
    }

    public function setPayable_with(string $payable_with): StoreSalesDetails
    {
        $this->payable_with = $payable_with;
        return $this;
    }

    public function getPayer_name(): string
    {
        return $this->payer_name;
    }

    public function setPayer_name(string $payer_name): StoreSalesDetails
    {
        $this->payer_name = $payer_name;
        return $this;
    }

    public function getPayer_email(): string
    {
        return $this->payer_email;
    }

    public function setPayer_email(string $payer_email): StoreSalesDetails
    {
        $this->payer_email = $payer_email;
        return $this;
    }

    public function getPayer_cpf_cnpj(): string
    {
        return $this->payer_cpf_cnpj;
    }

    public function setPayer_cpf_cnpj(string $payer_cpf_cnpj): StoreSalesDetails
    {
        $this->payer_cpf_cnpj = $payer_cpf_cnpj;
        return $this;
    }

    public function getPayer_phone(): string
    {
        return $this->payer_phone;
    }

    public function setPayer_phone(string $payer_phone): StoreSalesDetails
    {
        $this->payer_phone = $payer_phone;
        return $this;
    }

    public function getPayer_phone_prefix(): string
    {
        return $this->payer_phone_prefix;
    }

    public function setPayer_phone_prefix(string $payer_phone_prefix): StoreSalesDetails
    {
        $this->payer_phone_prefix = $payer_phone_prefix;
        return $this;
    }

    public function getPayer_address_zip_code(): string
    {
        return $this->payer_address_zip_code;
    }

    public function setPayer_address_zip_code(string $payer_address_zip_code): StoreSalesDetails
    {
        $this->payer_address_zip_code = $payer_address_zip_code;
        return $this;
    }

    public function getPayer_address_street(): string
    {
        return $this->payer_address_street;
    }

    public function setPayer_address_street(string $payer_address_street): StoreSalesDetails
    {
        $this->payer_address_street = $payer_address_street;
        return $this;
    }

    public function getPayer_address_district(): string
    {
        return $this->payer_address_district;
    }

    public function setPayer_address_district(string $payer_address_district): StoreSalesDetails
    {
        $this->payer_address_district = $payer_address_district;
        return $this;
    }

    public function getPayer_address_city(): string
    {
        return $this->payer_address_city;
    }

    public function setPayer_address_city(string $payer_address_city): StoreSalesDetails
    {
        $this->payer_address_city = $payer_address_city;
        return $this;
    }

    public function getPayer_address_state(): string
    {
        return $this->payer_address_state;
    }

    public function setPayer_address_state(string $payer_address_state): StoreSalesDetails
    {
        $this->payer_address_state = $payer_address_state;
        return $this;
    }

    public function getPayer_address_number(): string
    {
        return $this->payer_address_number;
    }

    public function setPayer_address_number(string $payer_address_number): StoreSalesDetails
    {
        $this->payer_address_number = $payer_address_number;
        return $this;
    }

    public function getPayer_address_complement(): ?string
    {
        return $this->payer_address_complement;
    }

    public function setPayer_address_complement(?string $payer_address_complement): StoreSalesDetails
    {
        $this->payer_address_complement = $payer_address_complement;
        return $this;
    }
}