<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="originatingTransaction")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OriginatingTransactionRepository")
 */
class OriginatingTransaction
{
    /**
     * @var int
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="PaymentRequirements")
     * @JoinColumn(name="requirement", referencedColumnName="id")
     */
    private $requirement;

    /**
     * @ManyToOne(targetEntity="Transaction")
     * @JoinColumn(name="transaction", referencedColumnName="id")
     */
    private $transaction;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRequirement(): PaymentRequirements
    {
        return $this->requirement;
    }

    public function setRequirement(PaymentRequirements $requirement): OriginatingTransaction
    {
        $this->requirement = $requirement;
        return $this;
    }

    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(Transaction $transaction): OriginatingTransaction
    {
        $this->transaction = $transaction;
        return $this;
    }
}