<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="representatives")
 * @ORM @Entity(repositoryClass="App\Models\Repository\RepresentativesRepository")
 */
class Representatives
{

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="CandidatePost")
     * @JoinColumn(name="post", referencedColumnName="id")
     */
    private CandidatePost $post;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     */
    private City $city;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     */
    private State $state;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(type="string")
     */
    private string $picture = '';

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private string $chiefOfStaff = '';

    /**
     * @Column(type="string")
     */
    private string $chiefOfStaffEmail = '';

    /**
     * @Column(type="string")
     */
    private string $chiefOfStaffPhone = '';

    /**
     * @Column(type="string")
     */
    private string $email = '';

    /**
     * @Column(type="string")
     */
    private string $instagram = '';

    /**
     * @Column(type="string")
     */
    private string $site = '';

    /**
     * @Column(type="string")
     */
    private string $facebook = '';

    /**
     * @Column(type="string")
     */
    private string $twitter = '';

    /**
     * @Column(type="integer")
     */
    private int $start = 0;

    /**
     * @Column(type="integer")
     */
    private int $end = 0;

    /**
     * @Column(type="integer")
     */
    private int $votes = 0;

    /**
     * @Column(type="integer")
     */
    private int $oratory = 0;

    /**
     * @Column(type="integer")
     */
    private int $leadership = 0;

    /**
     * @Column(type="integer")
     */
    private int $negotiation = 0;

    /**
     * @Column(type="integer")
     */
    private int $digitalInfluence = 0;

    /**
     * @Column(type="integer")
     */
    private int $alignment = 0;

    /**
     * @Column(type="float")
     */
    private float $capture = 0;

    /**
     * @Column(type="text")
     */
    private string $politicalCareer = '';

    /**
     * @Column(type="text")
     */
    private string $guidelines = '';

    /**
     * @Column(type="text")
     */
    private string $conquests = '';

    /**
     * @Column(type="text")
     */
    private string $productivity = '';

    /**
     * @Column(type="text")
     */
    private string $profile = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function getPost(): CandidatePost
    {
        return $this->post;
    }

    public function setPost(CandidatePost $post): Representatives
    {
        $this->post = $post;
        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): Representatives
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Representatives
    {
        $this->state = $state;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Representatives
    {
        $this->active = $active;
        return $this;
    }

    public function getPicture(): string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): Representatives
    {
        $this->picture = $picture;
        return $this;
    }

    public function getProductivity(): string
    {
        return $this->productivity;
    }

    public function setProductivity(string $productivity): Representatives
    {
        $this->productivity = $productivity;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Representatives
    {
        $this->name = $name;
        return $this;
    }

    public function getChiefOfStaff(): string
    {
        return $this->chiefOfStaff;
    }

    public function setChiefOfStaff(string $chiefOfStaff): Representatives
    {
        $this->chiefOfStaff = $chiefOfStaff;
        return $this;
    }

    public function getChiefOfStaffEmail(): string
    {
        return $this->chiefOfStaffEmail;
    }

    public function setChiefOfStaffEmail(string $chiefOfStaffEmail): Representatives
    {
        $this->chiefOfStaffEmail = $chiefOfStaffEmail;
        return $this;
    }

    public function getChiefOfStaffPhone(): string
    {
        return $this->chiefOfStaffPhone;
    }

    public function setChiefOfStaffPhone(string $chiefOfStaffPhone): Representatives
    {
        $this->chiefOfStaffPhone = Utils::onlyNumbers($chiefOfStaffPhone);
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Representatives
    {
        $this->email = $email;
        return $this;
    }

    public function getInstagram(): string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): Representatives
    {
        $this->instagram = $instagram;
        return $this;
    }

    public function getSite(): string
    {
        return $this->site;
    }

    public function setSite(string $site): Representatives
    {
        $this->site = $site;
        return $this;
    }

    public function getFacebook(): string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): Representatives
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function getTwitter(): string
    {
        return $this->twitter;
    }

    public function setTwitter(string $twitter): Representatives
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function setStart(int $start): Representatives
    {
        $this->start = $start;
        return $this;
    }

    public function getEnd(): int
    {
        return $this->end;
    }

    public function setEnd(int $end): Representatives
    {
        $this->end = $end;
        return $this;
    }

    public function getVotes(): int
    {
        return $this->votes;
    }

    public function setVotes(int $votes): Representatives
    {
        $this->votes = $votes;
        return $this;
    }

    public function getCapture(): float
    {
        return $this->capture;
    }

    public function setCapture(float $capture): Representatives
    {
        $this->capture = $capture;
        return $this;
    }

    public function getPoliticalCareer(): string
    {
        return $this->politicalCareer;
    }

    public function setPoliticalCareer(string $politicalCareer): Representatives
    {
        $this->politicalCareer = $politicalCareer;
        return $this;
    }

    public function getGuidelines(): string
    {
        return $this->guidelines;
    }

    public function setGuidelines(string $guidelines): Representatives
    {
        $this->guidelines = $guidelines;
        return $this;
    }

    public function getConquests(): string
    {
        return $this->conquests;
    }

    public function setConquests(string $conquests): Representatives
    {
        $this->conquests = $conquests;
        return $this;
    }

    public function getProfile(): string
    {
        return $this->profile;
    }

    public function setProfile(string $profile): Representatives
    {
        $this->profile = $profile;
        return $this;
    }

    public function getOratory(): int
    {
        return $this->oratory;
    }

    public function setOratory(int $oratory): Representatives
    {
        $this->oratory = $oratory;
        return $this;
    }

    public function getLeadership(): int
    {
        return $this->leadership;
    }

    public function setLeadership(int $leadership): Representatives
    {
        $this->leadership = $leadership;
        return $this;
    }

    public function getNegotiation(): int
    {
        return $this->negotiation;
    }

    public function setNegotiation(int $negotiation): Representatives
    {
        $this->negotiation = $negotiation;
        return $this;
    }

    public function getDigitalInfluence(): int
    {
        return $this->digitalInfluence;
    }

    public function setDigitalInfluence(int $digitalInfluence): Representatives
    {
        $this->digitalInfluence = $digitalInfluence;
        return $this;
    }

    public function getAlignment(): int
    {
        return $this->alignment;
    }

    public function setAlignment(int $alignment): Representatives
    {
        $this->alignment = $alignment;
        return $this;
    }

}