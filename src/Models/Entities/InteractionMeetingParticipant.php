<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="interactionMeetingParticipants")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionMeetingParticipantsRepository")
 */
class InteractionMeetingParticipant
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Interaction")
     * @JoinColumn(name="interaction", referencedColumnName="id")
     */
    private Interaction $interaction;

    /**
     * @Column(type="string")
     */
    private string $participantName = '';

    /**
     * @Column(type="string")
     */
    private string $participantRepresenting = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInteraction(): Interaction
    {
        return $this->interaction;
    }

    public function setInteraction(Interaction $interaction): InteractionMeetingParticipant
    {
        $this->interaction = $interaction;
        return $this;
    }

    public function getParticipantName(): string
    {
        return $this->participantName;
    }

    public function setParticipantName(string $participantName): InteractionMeetingParticipant
    {
        $this->participantName = $participantName;
        return $this;
    }

    public function getParticipantRepresenting(): string
    {
        return $this->participantRepresenting;
    }

    public function setParticipantRepresenting(string $participantRepresenting): InteractionMeetingParticipant
    {
        $this->participantRepresenting = $participantRepresenting;
        return $this;
    }


}