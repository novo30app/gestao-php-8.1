<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="accessLog")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AccessLogRepository")
 */
class AccessLog
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;


    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private ?User $user = null;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userAdmin", referencedColumnName="id", nullable=true)
     */
    private ?UserAdmin $userAdmin = null;

    /**
     * @Column(type="string")
     */
    private string $ip = '';

    /**
     * @Column(type="string")
     */
    private string $device = '';

    /**
     * @Column(type="string")
     */
    private string $so = '';

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Column(type="string")
     */
    private string $menu = '';

    public function __construct()
    {
        $this->created = new \DateTime();
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): AccessLog
    {
        $this->user = $user;
        return $this;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): AccessLog
    {
        $this->ip = $ip;
        return $this;
    }

    public function getDevice(): string
    {
        return $this->device;
    }

    public function setDevice(string $device): AccessLog
    {
        $this->device = $device;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }


    public function getSo(): string
    {
        return $this->so;
    }

    public function setSo(string $so): AccessLog
    {
        $this->so = $so;
        return $this;
    }

    public function getUserAdmin(): UserAdmin
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(UserAdmin $userAdmin): AccessLog
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getMenu(): string
    {
        return $this->menu;
    }

    public function setMenu(string $menu): AccessLog
    {
        $this->menu = $menu;
        return $this;
    }

}