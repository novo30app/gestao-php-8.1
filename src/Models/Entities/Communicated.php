<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * Communicated
 *
 * @Entity @Table(name="tb_conteudo")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CommunicatedRepository")
 */
class Communicated
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var string|null
     *
     * @Column(name="processo", type="string", length=15, nullable=true)
     */
    private $processo;

    /**
     * @var string|null
     *
     * @Column(name="titulo", type="string", length=500, nullable=true)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @Column(name="conteudo", type="text", length=65535, nullable=true)
     */
    private $conteudo;

    /**
     * @var string|null
     *
     * @Column(name="arquivo", type="string", length=200, nullable=true)
     */
    private $arquivo;

    /**
     * @var string|null
     *
     * @Column(name="link_comunicado", type="string", length=200, nullable=true)
     */
    private $linkComunicado;

    /**
     * @var bool|null
     *
     * @Column(name="restrito", type="boolean", nullable=true)
     */
    private $restrito;

    /**
     * @var bool|null
     *
     * @Column(name="exibir", type="boolean", nullable=true)
     */
    private $exibir;

    /**
     * @var bool|null
     *
     * @Column(name="comite", type="boolean", nullable=true)
     */
    private $comite;

    /**
     * @var bool|null
     *
     * @Column(name="comite_consultas", type="boolean", nullable=true)
     */
    private $comiteConsultas;

    /**
     * @var bool|null
     *
     * @Column(name="resolucao", type="boolean", nullable=true)
     */
    private $resolucao;

    /**
     * @var string|null
     *
     * @Column(name="tipo", type="string", length=200, nullable=true)
     */
    private $tipo;

    /**
     * @var bool|null
     *
     * @Column(name="link", type="boolean", nullable=true)
     */
    private $link;

    /**
     * @var string|null
     *
     * @Column(name="diretorio", type="string", length=25, nullable=true)
     */
    private $diretorio;

    /**
     * @var bool|null
     *
     * @Column(name="ativo", type="boolean", nullable=true)
     */
    private $ativo;

    public function getTitle()
    {
        return $this->titulo;
    }

    public function setTitle(?string $titulo): Communicated
    {
        $this->titulo = $titulo;
        return $this;
    }

    public function getDate()
    {
        return $this->data; 
    }

    public function setDate(?\DateTime $data): Communicated
    {
        $this->data = $data;
        return $this;
    }

    public function getProcess()
    {
        return $this->processo; 
    }

    public function setProcess(?string $processo): Communicated
    {
        $this->processo = $processo;
        return $this;
    }

    public function getLinkContent()
    {
        return $this->linkComunicado;
    }

    public function setLinkContent(?string $linkComunicado): Communicated
    {
        $this->linkComunicado = $linkComunicado;
        return $this;
    }

    public function getArchive()
    {
        return $this->arquivo;
    }

    public function setArchive(?string $arquivo): Communicated
    {
        $this->arquivo = $arquivo;
        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink(?bool $link): Communicated
    {
        $this->link = $link;
        return $this;
    }

    public function getType()
    {
        return $this->tipo;
    }

    public function setType(?string $tipo): Communicated
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function getRestricted()
    {
        return $this->restrito;
    }

    public function setRestricted(?bool $restrito): Communicated
    {
        $this->restrito = $restrito;
        return $this;
    }

    public function getShow()
    {
        return $this->exibir;
    }

    public function setShow(?bool $exibir): Communicated
    {
        $this->exibir = $exibir;
        return $this;
    }

    public function getDecisions()
    {
        return $this->comite;
    }

    public function setDecisions(?bool $comite): Communicated
    {
        $this->comite = $comite;
        return $this;
    }

    public function getConsultations()
    {
        return $this->comiteConsultas;
    }

    public function setConsultation(?bool $comiteConsultas): Communicated
    {
        $this->comiteConsultas = $comiteConsultas;
        return $this;
    }

    public function getResolutions()
    {
        return $this->resolucao;
    }

    public function setResolutions(?bool $resolucao): Communicated
    {
        $this->resolucao = $resolucao;
        return $this;
    }

    public function getDirectory()
    {
        return $this->diretorio;
    }

    public function setDirectory(?string $diretorio): Communicated
    {
        $this->diretorio = $diretorio;
        return $this;
    }   

    public function setActive(?bool $ativo): Communicated
    {
        $this->ativo = $ativo;
        return $this;
    }  

}
