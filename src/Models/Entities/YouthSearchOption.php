<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Entity @Table(name="youthSearchOptions")
 */
class YouthSearchOption
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;


    /**
     * @Column(type="string")
     */
    private string $name = '';


    /**
     * @Column(type="string")
     */
    private string $question = '';


    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): YouthSearchOption
    {
        $this->name = $name;
        return $this;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function setQuestion(string $question): YouthSearchOption
    {
        $this->question = $question;
        return $this;
    }

}