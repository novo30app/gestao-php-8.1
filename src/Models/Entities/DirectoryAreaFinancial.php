<?php


namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * DirectoryAreaFinancial
 *
 * @Entity @Table(name="directoryAreaFinancial")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryAreaFinancialRepository")
 */

class DirectoryAreaFinancial
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="report", type="string", length=150, nullable=false)
     */
    private $report;

    /**
     * @var string|null
     *
     * @Column(name="tag", type="string", length=25, nullable=false)
     */
    private $tag;

    /**
     * @Column(type="datetime", name="date", nullable=true)
     * @var string
     */
    private $date;

    /**
     * @var string|null
     *
     * @Column(name="ball", type="string", length=25, nullable=true)
     */
    private $ball;

    /**
     * @var string|null
     *
     * @Column(name="period", type="string", length=25, nullable=false)
     */
    private $period;

    /**
     * @var string|null
     *
     * @Column(name="month", type="string", length=25, nullable=false)
     */
    private $month;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=true)
     */
    private ?Directory $directory;

    /**
     * @var string|null
     *
     * @Column(name="file", type="string", length=150, nullable=true)
     */
    private $file;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;
    
    public function getId(): ?string
    {
        return $this->id;
    }

    public function getReport(): ?string
    {
        return $this->report;
    }

    public function setReport(?string $report): DirectoryAreaFinancial
    {
        $this->report = $report;
        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(?string $tag): DirectoryAreaFinancial
    {
        $this->tag = $tag;
        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): DirectoryAreaFinancial
    {
        $this->date = $date;
        return $this;
    }

    public function getBall(): ?string
    {
        return $this->ball;
    }

    public function setBall(?string $ball): DirectoryAreaFinancial
    {
        $this->ball = $ball;
        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(?string $period): DirectoryAreaFinancial
    {
        $this->period = $period;
        return $this;
    }

    public function getMonth(): ?string
    {
        return $this->month;
    }

    public function setMonth(?string $month): DirectoryAreaFinancial
    {
        $this->month = $month;
        return $this;
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): DirectoryAreaFinancial
    {
        $this->directory = $directory;
        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): DirectoryAreaFinancial
    {
        $this->file = $file;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): DirectoryAreaFinancial
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): DirectoryAreaFinancial
    {
        $this->state = $state;
        return $this;
    }

}