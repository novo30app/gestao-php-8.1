<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbFoiFiliado
 *
 * @Table(name="tb_foi_filiado")
 * @Entity
 */
class WasAffiliated
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="foi_filiado", type="string", length=45, nullable=true)
     */
    private $foiFiliado;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @Column(name="visivel", type="boolean", nullable=true)
     */
    private $visivel;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFoiFiliado(): ?string
    {
        return $this->foiFiliado;
    }

    public function setFoiFiliado(?string $foiFiliado): WasAffiliated
    {
        $this->foiFiliado = $foiFiliado;
        return $this;
    }

    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDataCriacao(?\DateTime $dataCriacao): WasAffiliated
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getVisivel(): ?bool
    {
        return $this->visivel;
    }

    public function setVisivel(?bool $visivel): WasAffiliated
    {
        $this->visivel = $visivel;
        return $this;
    }


}
