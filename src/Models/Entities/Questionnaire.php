<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity @Table(name="questionnaire")
 * @ORM @Entity(repositoryClass="App\Models\Repository\QuestionnaireRepository")
 */
class Questionnaire
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $hash = '';

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="responsible", referencedColumnName="id")
     */
    private UserAdmin $responsible;

    /**
     * @Column(type="text")
     */
    private string $title = '';

    /**
     * @Column(type="datetime")
     */
    private ?\DateTime $start = null;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private ?\DateTime $end = null;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @OneToMany(targetEntity="QuestionnaireQuestion", mappedBy="questionnaire")
     */
    private Collection $questions;


    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->hash = Utils::generateToken(10);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getResponsible(): UserAdmin
    {
        return $this->responsible;
    }

    public function setResponsible(UserAdmin $responsible): Questionnaire
    {
        $this->responsible = $responsible;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Questionnaire
    {
        $this->title = $title;
        return $this;
    }

    public function getStart(): ?\DateTime
    {
        return $this->start;
    }

    public function setStart(?\DateTime $start): Questionnaire
    {
        $this->start = $start;
        return $this;
    }

    public function getEnd(): ?\DateTime
    {
        return $this->end;
    }

    public function setEnd(?\DateTime $end): Questionnaire
    {
        $this->end = $end;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Questionnaire
    {
        $this->active = $active;
        return $this;
    }

    public function getQuestions(): ArrayCollection|Collection
    {
        return $this->questions;
    }
}