<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="chartOfAccounts")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ChartOfAccountsRepository")
 */
class ChartOfAccounts
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created_at;

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    /**
     * @Column(type="integer")
     */
    private ?int $reduced;

    /**
     * @Column(type="string")
     */
    private ?string $surname;

    /**
     * @Column(type="string")
     */
    private string $account;

    /**
     * @Column(type="string")
     */
    private string $description;

    /**
     * @Column(type="integer")
     */
    private int $type;

    /**
     * @Column(type="integer")
     */
    private int $category;

    /**
     * @Column(type="string")
     */
    private string $descriptionSPCA;

    /**
     * @Column(type="integer")
     */ 
    private int $typeDocument;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated_at(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreated_at(\DateTime $created_at): ChartOfAccounts
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): ChartOfAccounts
    {
        $this->active = $active;
        return $this;
    }

    public function getReduced(): ?int
    {
        return $this->reduced;
    }

    public function setReduced(?int $reduced): ChartOfAccounts
    {
        $this->reduced = $reduced;
        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }
 
    public function setSurname(?string $surname): ChartOfAccounts
    {
        $this->surname = $surname;
        return $this;
    }

    public function getAccountDescription(): string
    {
        $description = $this->account . ' / ' . $this->description;
        return $description;
    }
    
    public function getAccount(): string
    {
        return $this->account;
    }

    public function setAccount(string $account): ChartOfAccounts
    {
        $this->account = $account;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): ChartOfAccounts
    {
        $this->description = $description;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): ChartOfAccounts
    {
        $this->type = $type;
        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): ChartOfAccounts
    {
        $this->category = $category;
        return $this;
    }

    public function getDescriptionSPCA(): string
    {
        return $this->descriptionSPCA;
    }

    public function setDescriptionSPCA(string $descriptionSPCA): ChartOfAccounts
    {
        $this->descriptionSPCA = $descriptionSPCA;
        return $this;
    }

    public function getTypeDocument(): int
    {
        return $this->typeDocument;
    }

    public function getTypeDocumentString(): int
    {
        $typeDocument = match ($this->typeDocument) {
            1 => 'Fatura',
            2 => 'Nota',
            3 => 'Outros',
            4 => 'Recibo',
            5 => 'Transf',
            default => ''
        };
        return $typeDocument;
    }


    public function setTypeDocument(int $typeDocument): ChartOfAccounts
    {
        $this->typeDocument = $typeDocument;
        return $this;
    }
}