<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="php81Entity")
 * @ORM @Entity(repositoryClass="App\Models\Repository\Php81EntityRepository")
 */
class Php81Entity
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="text")
     */
    private string $desciption = '';


    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getDesciption(): string
    {
        return $this->desciption;
    }

    public function setDesciption(string $desciption): Php81Entity
    {
        $this->desciption = $desciption;
        return $this;
    }


}