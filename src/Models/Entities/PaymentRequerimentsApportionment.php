<?php

namespace App\Models\Entities;

use App\Helpers\Utils;

/**
 * @Entity @Table(name="paymentRequerimentsApportionment")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PaymentRequirementsApportionmentRepository")
 */
class PaymentRequerimentsApportionment
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="PaymentRequirements", inversedBy="apportionments")
     * @JoinColumn(name="paymentRequirements", referencedColumnName="id")
     */
    private PaymentRequirements $paymentRequirements;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=true)
     */
    private ?Directory $directory = null;

    /**
     * @ManyToOne(targetEntity="CostCenter")
     * @JoinColumn(name="costCenter", referencedColumnName="id", nullable=true)
     */
    private ?CostCenter $costCenter = null;

    /**
     * @Column(type="float", nullable=true)
     */
    private ?float $percent = null;

    /**
     * @Column(type="float", nullable=true)
     */
    private ?float $value = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaymentRequirements(): PaymentRequirements
    {
        return $this->paymentRequirements;
    }

    public function setPaymentRequirements(PaymentRequirements $paymentRequirements): PaymentRequerimentsApportionment
    {
        $this->paymentRequirements = $paymentRequirements;
        return $this;
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): PaymentRequerimentsApportionment
    {
        $this->directory = $directory;
        return $this;
    }

    public function getCostCenter(): ?CostCenter
    {
        return $this->costCenter;
    }

    public function setCostCenter(?CostCenter $costCenter): PaymentRequerimentsApportionment
    {
        $this->costCenter = $costCenter;
        return $this;
    }

    public function getPercent(): ?float
    {
        return $this->percent;
    }

    public function setPercent(?float $percent): PaymentRequerimentsApportionment
    {
        $this->percent = $percent;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): PaymentRequerimentsApportionment
    {
        $this->value = $value;
        return $this;
    }
}
