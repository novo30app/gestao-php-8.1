<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="interactions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionRepository")
 */
class Interaction
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="InteractionType")
     * @JoinColumn(name="type", referencedColumnName="id")
     */
    private InteractionType $type;

    /**
     * @Column(type="text")
     */
    private string $interaction = '';

    /**
     * @Column(type="date")
     */
    private \DateTime $date;

    /**
     * @Column(type="string")
     */
    private string $responsible = '';


    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): InteractionType
    {
        return $this->type;
    }

    public function setType(InteractionType $type): Interaction
    {
        $this->type = $type;
        return $this;
    }

    public function getInteraction(): string
    {
        return $this->interaction;
    }

    public function setInteraction(string $interaction): Interaction
    {
        $this->interaction = $interaction;
        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): Interaction
    {
        $this->date = $date;
        return $this;
    }

    public function getResponsible(): string
    {
        return $this->responsible;
    }

    public function setResponsible(string $responsible): Interaction
    {
        $this->responsible = $responsible;
        return $this;
    }

}