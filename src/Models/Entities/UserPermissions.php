<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="userPermissions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\UserPermissionsRepository")
 */
class UserPermissions
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="admin", referencedColumnName="id")
     */
    private UserAdmin $admin;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @ManyToOne(targetEntity="SystemFeatures")
     * @JoinColumn(name="systemFeature", referencedColumnName="id")
     */
    private SystemFeatures $systemFeature;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): UserPermissions
    {
        $this->created = $created;
        return $this;
    }

    public function getAdmin(): UserAdmin
    {
        return $this->admin;
    }

    public function setAdmin(UserAdmin $admin): UserPermissions
    {
        $this->admin = $admin;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): UserPermissions
    {
        $this->user = $user;
        return $this;
    }

    public function getSystemFeatures(): SystemFeatures
    {
        return $this->systemFeature;
    }

    public function setSystemFeatures(SystemFeatures $systemFeature): UserPermissions
    {
        $this->systemFeature = $systemFeature;
        return $this;
    }
}