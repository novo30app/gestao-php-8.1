<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * AccessAdmin
 * 
 * @Entity @Table(name="accessAdmin")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AccessAdminRepository")
 */
class AccessAdmin
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var int|null
     * @Column(name="userAdmin", type="integer", nullable=true)
     */
    private $userAdmin;

    /**
     * @var bool|null
     * @Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var int|null
     * @Column(name="access", type="integer", nullable=true)
     */
    private $access;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserAdmin(): ?int
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(?int $userAdmin): AccessAdmin
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): AccessAdmin
    {
        $this->type = $type;
        return $this;
    }

    public function getAccess(): ?int
    {
        return $this->access;
    }

    public function setAccess(?int $access): AccessAdmin
    {
        $this->access = $access;
        return $this;
    }
}