<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity @Table(name="questionnaireReply")
 * @ORM @Entity(repositoryClass="App\Models\Repository\QuestionnaireReplyRepository")
 */
class QuestionnaireReply
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $hash = '';

    /**
     * @ManyToOne(targetEntity="Questionnaire")
     * @JoinColumn(name="questionnaire", referencedColumnName="id")
     */
    private Questionnaire $questionnaire;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private string $email = '';

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private ?\DateTime $doneDate = null;

    /**
     * @Column(type="boolean")
     */
    private bool $done = false;


    public function __construct()
    {
        $this->hash = Utils::generateToken(10);
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getQuestionnaire(): Questionnaire
    {
        return $this->questionnaire;
    }

    public function setQuestionnaire(Questionnaire $questionnaire): QuestionnaireReply
    {
        $this->questionnaire = $questionnaire;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): QuestionnaireReply
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): QuestionnaireReply
    {
        $this->email = trim($email);
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getDoneDate(): ?\DateTime
    {
        return $this->doneDate;
    }

    public function setDoneDate(?\DateTime $doneDate): QuestionnaireReply
    {
        $this->doneDate = $doneDate;
        return $this;
    }

    public function isDone(): bool
    {
        return $this->done;
    }

    public function setDone(bool $done): QuestionnaireReply
    {
        $this->done = $done;
        return $this;
    }

    public function getLink(): string
    {
        return BASEURL . "questionarios/responder/2/{$this->id}/{$this->hash}";
    }

    public function isDoneString(): string
    {
        return $this->done ? '1' : '0';
    }


}