<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="appointments")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AppointmentRepository")
 */

class Appointment
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $status = '';

    /**
     * @Column(type="string")
     */
    private string $link = '';

    /**
     * @Column(type="datetime")
     */
    private \DateTime $appointment;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="affiliated", referencedColumnName="id")
     */
    private User $affiliated;

    /**
     * @Column(type="text")
     */
    private string $obs = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Appointment
    {
        $this->status = $status;
        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): Appointment
    {
        $this->link = $link;
        return $this;
    }

    public function getAppointment(): \DateTime
    {
        return $this->appointment;
    }

    public function setAppointment(\DateTime $appointment): Appointment
    {
        $this->appointment = $appointment;
        return $this;
    }

    public function getAffiliated(): User
    {
        return $this->affiliated;
    }

    public function setAffiliated(User $affiliated): Appointment
    {
        $this->affiliated = $affiliated;
        return $this;
    }

    public function getObs(): string
    {
        return $this->obs;
    }

    public function setObs(string $obs): Appointment
    {
        $this->obs = $obs;
        return $this;
    }
}