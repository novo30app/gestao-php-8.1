<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="interactionMeetingSchedules")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionMeetingScheduleRepository")
 */
class InteractionMeetingSchedule
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Interaction")
     * @JoinColumn(name="interaction", referencedColumnName="id")
     */
    private Interaction $interaction;

    /**
     * @Column(type="string")
     */
    private string $schedule = '';


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInteraction(): Interaction
    {
        return $this->interaction;
    }

    public function setInteraction(Interaction $interaction): InteractionMeetingSchedule
    {
        $this->interaction = $interaction;
        return $this;
    }

    public function getSchedule(): string
    {
        return $this->schedule;
    }

    public function setSchedule(string $schedule): InteractionMeetingSchedule
    {
        $this->schedule = $schedule;
        return $this;
    }

}