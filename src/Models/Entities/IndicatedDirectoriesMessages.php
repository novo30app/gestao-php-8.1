<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicatedDirectoriesMessages")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicatedDirectoriesMessagesRepository")
 */
class IndicatedDirectoriesMessages
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private UserAdmin $user;

    /**
     * @ManyToOne(targetEntity="IndicatedDirectories")
     * @JoinColumn(name="indication", referencedColumnName="id", nullable=true)
     */
    private $indication;

    /**
     * @Column(type="string")
     * @var string
     */
    private $message;

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $visibled;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): IndicatedDirectoriesMessages
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): IndicatedDirectoriesMessages
    {
        $this->user = $user;
        return $this;
    }

    public function getIndication(): IndicatedDirectories
    {
        return $this->indication;
    }

    public function setIndication(IndicatedDirectories $indication): IndicatedDirectoriesMessages
    {
        $this->indication = $indication;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): IndicatedDirectoriesMessages
    {
        $this->message = $message;
        return $this;
    }

    public function getVisibled(): bool
    {
        return $this->visibled;
    }

    public function setVisibled(bool $visibled): IndicatedDirectoriesMessages
    {
        $this->visibled = $visibled;
        return $this;
    }
}