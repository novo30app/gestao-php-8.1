<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * CommentStatus
 *
 * @Table(name="tb_status_comentarios")
 * @Entity
 */

class CommentStatus
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(name="status", type="text")
     */
    private string $status;

    /**
     * @var bool|null
     *
     * @Column(name="visivel", type="boolean", nullable=true)
     */
    private $visible = '0';

    public function getId()
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }
}

?>