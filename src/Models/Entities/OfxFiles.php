<?php

namespace App\Models\Entities;

use App\Helpers\Utils;

/**
 * @Entity @Table(name="ofxFiles")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OfxFilesRepository")
 */
class OfxFiles
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @Column(type="datetime")
	 */
	private ?\DateTime $created = null;
	
	/**
	 * @ManyToOne(targetEntity="UserAdmin")
	 * @JoinColumn(name="user", referencedColumnName="id")
	 */
	private UserAdmin $user;
	
	/**
	 * @ManyToOne(targetEntity="DirectoryAccounts")
	 * @JoinColumn(name="directoryAccount", referencedColumnName="id")
	 */
	private DirectoryAccounts $directoryAccount;
	
	/**
	 * @Column(type="date", nullable=true)
	 */
	private ?\DateTime $start = null;
	
	/**
	 * @Column(type="date", nullable=true)
	 */
	private ?\DateTime $end = null;
	
	/**
	 * @Column(type="string")
	 */
	private ?string $file = null;
	
	
	public function __construct()
	{
		$this->created = new \DateTime();
	}
	
	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCreated(): ?\DateTime
	{
		return $this->created;
	}

	public function getUser(): ?UserAdmin
	{
		return $this->user;
	}

	public function setUser(?UserAdmin $user): OfxFiles
	{
		$this->user = $user;
		return $this;
	}

	public function getDirectoryAccount(): ?DirectoryAccounts
	{
		return $this->directoryAccount;
	}

	public function setDirectoryAccount(?DirectoryAccounts $directoryAccount): OfxFiles
	{
		$this->directoryAccount = $directoryAccount;
		return $this;
	}

	public function getStart(): ?\DateTime
	{
		return $this->start;
	}

	public function setStart(?\DateTime $start): OfxFiles
	{
		$this->start = $start;
		return $this;
	}

	public function getEnd(): ?\DateTime
	{
		return $this->end;
	}

	public function setEnd(?\DateTime $end): OfxFiles
	{
		$this->end = $end;
		return $this;
	}

	public function getFile(): ?string
	{
		$file = $this->file;
		if ($file) $file = Utils::formatAttachment($this->file);
		return $file;
	}
	
	public function getFileOfx(): ?string
	{
		return $this->file;
	}

	public function setFile(?string $file): OfxFiles
	{
		$this->file = $file;
		return $this;
	}
}