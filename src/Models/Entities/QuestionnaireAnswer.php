<?php

namespace App\Models\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity @Table(name="questionnaireAnswers")
 * @ORM @Entity(repositoryClass="App\Models\Repository\QuestionnaireAnswerRepository")
 */
class QuestionnaireAnswer
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="QuestionnaireReply")
     * @JoinColumn(name="questionnaireReply", referencedColumnName="id")
     */
    private QuestionnaireReply $questionnaireReply;

    /**
     * @ManyToOne(targetEntity="QuestionOption")
     * @JoinColumn(name="questionOption", referencedColumnName="id")
     */
    private QuestionOption $questionOption;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestionnaireReply(): QuestionnaireReply
    {
        return $this->questionnaireReply;
    }

    public function setQuestionnaireReply(QuestionnaireReply $questionnaireReply): QuestionnaireAnswer
    {
        $this->questionnaireReply = $questionnaireReply;
        return $this;
    }

    public function getQuestionOption(): QuestionOption
    {
        return $this->questionOption;
    }

    public function setQuestionOption(QuestionOption $questionOption): QuestionnaireAnswer
    {
        $this->questionOption = $questionOption;
        return $this;
    }


}