<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="paymentRequirementsFiles")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PaymentRequirementsFilesRepository")
 */
class PaymentRequirementsFiles
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="integer")
     */
    private int $requirement;

    /**
     * @Column(type="string")
     */
    private string $file;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getRequirement(): int
    {
        return $this->requirement;
    }

    public function setRequirement(int $requirement): PaymentRequirementsFiles
    {
        $this->requirement = $requirement;
        return $this;
    }

    public function getFile(): string
    {
        return Utils::formatAttachment($this->file);
    }

    public function setFile(string $file): PaymentRequirementsFiles
    {
        $this->file = $file;
        return $this;
    }
}