<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="directoryAccounts")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryAccountsRepository")
 */
class DirectoryAccounts
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var int
     * @Column(name="directory", type="integer", nullable=false)
     */

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=false)
     * @var Directory
     */
    private $directory;

    /**
     * @var string
     * @Column(name="nickname", type="integer", nullable=false)
     */
    private $nickname;

    /**
     * @ManyToOne(targetEntity="Bank")
     * @JoinColumn(name="bank", referencedColumnName="id", nullable=false)
     * @var Bank
     */
    private $bank;

    /**
     * @var int
     * @Column(name="agency", type="integer", nullable=false)
     */
    private $agency;

    /**
     * @var string
     * @Column(name="agencyDigit", type="string", nullable=false)
     */
    private $agencyDigit;

    /**
     * @var int
     * @Column(name="account", type="integer", nullable=false)
     */
    private $account;

    /**
     * @var string
     * @Column(name="accountDigit", type="string", nullable=false)
     */
    private $accountDigit;

    /**
     * @ManyToOne(targetEntity="ChartOfAccounts")
     * @JoinColumn(name="accountingAccount", referencedColumnName="id")
     */
    private ?ChartOfAccounts $accountingAccount;

    /**
     * @Column(name="dateStart", type="date", nullable=false)
     */
    private ?\DateTime $dateStart = null;

    /**
     * @Column(name="valueStart", type="float", nullable=false)
     */
    private ?float $valueStart = null;

    /**
     * @Column(name="apiExtract", type="boolean", nullable=true)
     */
    private $apiExtract = 0;

    /**
     * @var string
     * @Column(name="applicationkey", type="string", nullable=false)
     */
    private $applicationkey;

    public function getId(): int
    {
        return $this->id;
    }

    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    public function setDirectory(Directory $directory): DirectoryAccounts
    {
        $this->directory = $directory;
        return $this;
    }

    public function getNickname(): int
    {
        return $this->nickname;
    }

    public function getNicknameString(): ?string
    {
        $nickname = match($this->nickname) {
            1 => 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS',
            2 => 'FP - POLÍTICA DA MULHER',
            3 => 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS',
            4 => 'FP - FUNDO PARTIDÁRIO',
            5 => 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL',
            6 => 'FEFC GERAL',
            7 => 'FEFC MULHER',
            8 => 'FEFC MULHER NEGRA',
            9 => 'FEFC HOMEM NEGRO',
            10 => 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL',
            11 => 'FEFC INDÍGENA',
            // INVESTIMENTO
            12 => 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS - INVESTIMENTO',
            13 => 'FP - FUNDO PARTIDÁRIO INVESTIMENTO',
            14 => 'FP - POLÍTICA DA MULHER INVESTIMENTO',
            15 => 'FEFC GERAL - INVESTIMENTO',
            16 => 'FEFC MULHER - INVESTIMENTO',
            17 => 'FEFC MULHER NEGRA - INVESTIMENTO',
            18 => 'FEFC HOMEM NEGRO - INVESTIMENTO',
            19 => 'FEFC INDÍGENA - INVESTIMENTO',
	        default => 'Desconhecido'
        };
        return $nickname;
    }

    public function setNickname(int $nickname): DirectoryAccounts
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function getBank(): Bank
    {
        return $this->bank;
    }

    public function setBank(Bank $bank): DirectoryAccounts
    {
        $this->bank = $bank;
        return $this;
    }

    public function getAgencyString(): string
    {
        $agency = $this->agency . '-' . $this->agencyDigit;
        return $agency;
    }

    public function getAgency(): int
    {
        return $this->agency;
    }

    public function setAgency(int $agency): DirectoryAccounts
    {
        $this->agency = $agency;
        return $this;
    }

    public function getAgencyDigit(): string
    {
        return $this->agencyDigit;
    }

    public function setAgencyDigit(string $agencyDigit): DirectoryAccounts
    {
        $this->agencyDigit = $agencyDigit;
        return $this;
    }

    public function getAccountString(): string
    {
        $account = $this->account . '-' . $this->accountDigit;
        return $account;
    }

    public function getAccount(): int
    {
        return $this->account;
    }

    public function setAccount(int $account): DirectoryAccounts
    {
        $this->account = $account;
        return $this;
    }

    public function getAccountDigit(): string
    {
        return $this->accountDigit;
    }

    public function setAccountDigit(string $accountDigit): DirectoryAccounts
    {
        $this->accountDigit = $accountDigit;
        return $this;
    }

    public function getAccountingAccount(): ?ChartOfAccounts
    {
        return $this->accountingAccount;
    }

    public function setAccountingAccount(ChartOfAccounts $accountingAccount): DirectoryAccounts
    {
        $this->accountingAccount = $accountingAccount;
        return $this;
    }

    public function getDateStart(): ?\DateTime
    {
        return $this->dateStart;
    }

    public function setDateStart(?\DateTime $dateStart): DirectoryAccounts
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    public function getValueStart(): ?float
    {
        return $this->valueStart;
    }

    public function setValueStart(?float $valueStart): DirectoryAccounts
    {
        $this->valueStart = $valueStart;
        return $this;
    }

    public function getApiExtract(): bool
    {
        return $this->apiExtract;
    }

    public function setApiExtract(bool $apiExtract): DirectoryAccounts
    {
        $this->apiExtract = $apiExtract;
        return $this;
    }

    public function getApplicationkey(): ?string
    {
        return $this->applicationkey;
    }

    public function setApplicationkey(?string $applicationkey): DirectoryAccounts
    {
        $this->applicationkey = $applicationkey;
        return $this;
    }
}
