<?php

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonCreditCard
 *
 * @Entity @Table(name="tb_pessoa_cartao_credito", indexes={@Index(name="idx_id", columns={"id"}),@Index(name="idx_tb_pessoa_cartao_credito_data_criacao", columns={"data_criacao"}), @Index(name="idx_tb_pessoa_cartao_credito_legenda", columns={"legenda"}), @Index(name="idx_tb_pessoa_cartao_credito_nome", columns={"nome"}), @Index(name="idx_tb_pessoa_cartao_credito_principal", columns={"principal"}), @Index(name="idx_tb_pessoa_cartao_credito_gateway_pagamento", columns={"gateway_pagamento"}), @Index(name="idx_tb_pessoa_cartao_credito_bandeira", columns={"bandeira"}), @Index(name="idx_tb_pessoa_cartao_credito_visivel", columns={"visivel"}), @Index(name="idx_tb_pessoa_cartao_credito_tb_pessoa_id", columns={"tb_pessoa_id"}), @Index(name="idx_tb_pessoa_cartao_credito_token_cartao_credito", columns={"token_cartao_credito"})})
 * @Entity(repositoryClass="App\Models\Repository\PersonCreditCardRepository")
 */
class PersonCreditCard
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", length=50, nullable=true)
     */
    private $user;

    /**
     * @var string|null
     *
     * @Column(name="token_cartao_credito", type="string", length=50, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var int|null
     *
     * @Column(name="gateway_pagamento", type="integer", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @Column(name="legenda", type="string", length=45, nullable=true)
     */
    private $legenda;

    /**
     * @var string|null
     *
     * @Column(name="bandeira", type="string", length=40, nullable=true)
     */
    private $flag;

    /**
     * @var bool|null
     *
     * @Column(name="principal", type="boolean", nullable=true, options={"default"="1"})
     */
    private $principal = '1';

    /**
     * @var bool|null
     *
     * @Column(name="visivel", type="boolean", nullable=true, options={"default"="1"})
     */
    private $visivel = '1';

    /**
     * @var bool|null
     *
     * @Column(name="international", type="boolean", nullable=true, options={"default"="0"})
     */
    private $international = '0';

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @Column(name="expiration", type="string", length=40, nullable=true)
     */
    private $expiration;

    public function __construct()
    {
        $this->dataCriacao = new \DateTime();
    }

    public function toArray():array {
        $array['id'] = $this->getId();
        $array['flag'] = $this->getFlag();
        $array['number'] = $this->getLegend();
        $array['name'] = $this->getName();
        $array['status'] = $this->getStatus();
        $array['statusStr'] = $this->getStatusStr();
        return $array;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTokenCartaoCredito(): ?string
    {
        return $this->tokenCartaoCredito;
    }

    /**
     * @param string|null $tokenCartaoCredito
     * @return PersonCreditCard
     */
    public function setTokenCartaoCredito(?string $tokenCartaoCredito): PersonCreditCard
    {
        $this->tokenCartaoCredito = $tokenCartaoCredito;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLegenda(): ?string
    {
        return $this->legenda;
    }

    /**
     * @param string|null $legenda
     * @return PersonCreditCard
     */
    public function setLegenda(?string $legenda): PersonCreditCard
    {
        $this->legenda = $legenda;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVisivel(): ?bool
    {
        return $this->visivel;
    }

    /**
     * @param bool|null $visivel
     * @return PersonCreditCard
     */
    public function setVisivel(?bool $visivel): PersonCreditCard
    {
        $this->visivel = $visivel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string|null $nome
     * @return PersonCreditCard
     */
    public function setNome(?string $nome): PersonCreditCard
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExpiration(): ?string
    {
        return $this->expiration;
    }

    /**
     * @param string|null $expiration
     * @return PersonCreditCard
     */
    public function setExpiration(?string $expiration): PersonCreditCard
    {
        $this->expiration = $expiration;
        return $this;
    }


    public function getLegend($flag = false)
    {
        if ($flag && $this->getFlag()){
            $numbers = substr($this->legenda, -4);
            return "**** **** **** {$numbers} - " . ucfirst($this->getFlag());
        }
        $numbers = substr($this->legenda, -4);
        return "**** **** **** {$numbers}";
    }

    public function getFlag()
    {
        if (!$this->flag){
            return '';
        }

        return $this->flag;
    }

    public function getName()
    {
        return $this->nome;
    }

    public function getCardNumber()
    {
        return $this->legenda;
    }

    /**
     * @return int|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param int|null $tbPessoaId
     * @return PersonCreditCard
     */
    public function setUser(?User $user): PersonCreditCard
    {
        $this->user = $user->getId();
        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->tokenCartaoCredito;
    }

    /**
     * @param string|null $tokenCartaoCredito
     * @return PersonCreditCard
     */
    public function setToken(?string $tokenCartaoCredito): PersonCreditCard
    {
        $this->tokenCartaoCredito = $tokenCartaoCredito;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGatewayPagamento(): ?int
    {
        return $this->gatewayPagamento;
    }

    /**
     * @param int|null $gatewayPagamento
     * @return PersonCreditCard
     */
    public function setGatewayPagamento(?int $gatewayPagamento): PersonCreditCard
    {
        $this->gatewayPagamento = $gatewayPagamento;
        return $this;
    }

    /**
     * @param string|null $legenda
     * @return PersonCreditCard
     */
    public function setCardNumber(?string $legenda): PersonCreditCard
    {
        $this->legenda = $legenda;
        return $this;
    }

    /**
     * @param string|null $bandeira
     * @return PersonCreditCard
     */
    public function setFlag(?string $bandeira): PersonCreditCard
    {
        $this->flag = $bandeira;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPrincipal(): ?bool
    {
        return $this->principal;
    }

    /**
     * @param bool|null $principal
     * @return PersonCreditCard
     */
    public function setPrincipal(?bool $principal): PersonCreditCard
    {
        $this->principal = $principal;
        return $this;
    }

    public function getStatusStr(): string
    {
        if ($this->visivel){
            return "Habilitado";
        }
        return "Desabilitado";
    }

    public function getStatus(): ?bool
    {
        return $this->visivel;
    }

    public function setStatus(?bool $visivel): PersonCreditCard
    {
        $this->visivel = $visivel;
        return $this;
    }

    public function getInternational(): ?bool
    {
        return $this->international;
    }

    public function setInternational(?bool $international): PersonCreditCard
    {
        $this->international = $international;
        return $this;
    }

    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDataCriacao(?\DateTime $dataCriacao): PersonCreditCard
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function setName(?string $nome): PersonCreditCard
    {
        $this->nome = $nome;
        return $this;
    }



}
