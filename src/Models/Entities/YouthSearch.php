<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="youthSearchs")
 * @ORM @Entity(repositoryClass="App\Models\Repository\YouthSearchRepository")
 */
class YouthSearch
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;


    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userAdmin", referencedColumnName="id")
     */
    private UserAdmin $userAdmin;


    /**
     * @Column(type="datetime")
     */
    private \DateTime $updatedAt;

    /**
     * @ManyToOne(targetEntity="YouthSearchOption")
     * @JoinColumn(name="municipalLeader", referencedColumnName="id")
     */
    private YouthSearchOption $municipalLeader;

    /**
     * @ManyToOne(targetEntity="YouthSearchOption")
     * @JoinColumn(name="voluntaryInterest", referencedColumnName="id")
     */
    private YouthSearchOption $voluntaryInterest;

    /**
     * @ManyToOne(targetEntity="YouthSearchOption")
     * @JoinColumn(name="student", referencedColumnName="id")
     */
    private YouthSearchOption $student;

    /**
     * @ManyToOne(targetEntity="YouthSearchOption")
     * @JoinColumn(name="candidateInterest", referencedColumnName="id")
     */
    private YouthSearchOption $candidateInterest;


    /**
     * @ManyToOne(targetEntity="YouthSearchOption")
     * @JoinColumn(name="joinedWhatsAppGroup", referencedColumnName="id")
     */
    private YouthSearchOption $joinedWhatsAppGroup;


    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): YouthSearch
    {
        $this->user = $user;
        return $this;
    }

    public function getMunicipalLeader(): ?YouthSearchOption
    {
        return $this->municipalLeader;
    }

    public function setMunicipalLeader(?YouthSearchOption $municipalLeader): YouthSearch
    {
        $this->municipalLeader = $municipalLeader;
        return $this;
    }

    public function getUserAdmin(): UserAdmin
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(UserAdmin $userAdmin): YouthSearch
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): YouthSearch
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getVoluntaryInterest(): YouthSearchOption
    {
        return $this->voluntaryInterest;
    }

    public function setVoluntaryInterest(YouthSearchOption $voluntaryInterest): YouthSearch
    {
        $this->voluntaryInterest = $voluntaryInterest;
        return $this;
    }

    public function getStudent(): YouthSearchOption
    {
        return $this->student;
    }

    public function setStudent(YouthSearchOption $student): YouthSearch
    {
        $this->student = $student;
        return $this;
    }

    public function getCandidateInterest(): YouthSearchOption
    {
        return $this->candidateInterest;
    }

    public function setCandidateInterest(YouthSearchOption $candidateInterest): YouthSearch
    {
        $this->candidateInterest = $candidateInterest;
        return $this;
    }

    public function getJoinedWhatsAppGroup(): YouthSearchOption
    {
        return $this->joinedWhatsAppGroup;
    }

    public function setJoinedWhatsAppGroup(YouthSearchOption $joinedWhatsAppGroup): YouthSearch
    {
        $this->joinedWhatsAppGroup = $joinedWhatsAppGroup;
        return $this;
    }


}