<?php

namespace App\Models\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="bbCredentials")
 */
class CredentialsBB
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime", nullable=false)
     */
    private ?\DateTime $created = null;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=false)
     * @var Directory
     */
    private Directory $directory;

    /**
     * @Column(type="integer")
     */
    private int $type = 1;


    /**
     * @Column(type="string")
     */
    private string $developer_application_key = '';

    /**
     * @Column(type="string")
     */
    private string $client_id = '';

    /**
     * @Column(type="string")
     */
    private string $client_secret = '';

    /**
     * @Column(type="text")
     */
    private string $basic = '';

    /**
     * @Column(type="string")
     */
    private string $responsible = '';

    /**
     * @Column(type="string")
     */
    private string $certificatePem = '';

    /**
     * @Column(type="string")
     */
    private string $certificateKey = '';

    /**
     * @Column(type="string")
     */
    private string $certificatePassword = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    public function setCreated(?\DateTime $created): CredentialsBB
    {
        $this->created = $created;
        return $this;
    }

    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    public function setDirectory(Directory $directory): CredentialsBB
    {
        $this->directory = $directory;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getTypeStr(): string
    {
        switch ($this->type) {
            case 1:
                return 'Extratos';
            case 2:
                return 'Pagamentos';
            default:
                return 'Desconhecido';
        }
    }

    public function setType(int $type): CredentialsBB
    {
        $this->type = $type;
        return $this;
    }

    public function getDeveloperApplicationKey(): string
    {
        return $this->developer_application_key;
    }

    public function setDeveloperApplicationKey(string $developer_application_key): CredentialsBB
    {
        $this->developer_application_key = $developer_application_key;
        return $this;
    }

    public function getClientId(): string
    {
        return $this->client_id;
    }

    public function setClientId(string $client_id): CredentialsBB
    {
        $this->client_id = $client_id;
        return $this;
    }

    public function getClientSecret(): string
    {
        return $this->client_secret;
    }

    public function setClientSecret(string $client_secret): CredentialsBB
    {
        $this->client_secret = $client_secret;
        return $this;
    }

    public function getBasic(): string
    {
        return $this->basic;
    }

    public function setBasic(string $basic): CredentialsBB
    {
        $this->basic = $basic;
        return $this;
    }

    public function getResponsible(): string
    {
        return $this->responsible;
    }

    public function setResponsible(string $responsible): CredentialsBB
    {
        $this->responsible = $responsible;
        return $this;
    }

    public function getCertificatePem(bool $fullPath = true): string
    {
        if (!$fullPath) return end(explode('certificates/', $this->certificatePem));
        return $this->certificatePem;
    }

    public function setCertificatePem(string $certificatePem): CredentialsBB
    {
        $this->certificatePem = $certificatePem;
        return $this;
    }

    public function getCertificateKey(bool $fullPath = true): string
    {
        if (!$fullPath) return end(explode('certificates/', $this->certificateKey));
        return $this->certificateKey;
    }

    public function setCertificateKey(string $certificateKey): CredentialsBB
    {
        $this->certificateKey = $certificateKey;
        return $this;
    }

    public function getCertificatePassword(): string
    {
        return $this->certificatePassword;
    }

    public function setCertificatePassword(string $certificatePassword): CredentialsBB
    {
        $this->certificatePassword = $certificatePassword;
        return $this;
    }
}