<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaIugu
 *
 * @Table(name="tb_pessoa_iugu", indexes={@Index(name="idx_pessoa", columns={"tb_pessoa_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\PersonIuguRepository")
 */
class PersonIugu
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private ?int $user;

    /**
     * @Column(name="customer_id", type="string", length=45, nullable=true)
     */
    private ?string $customerId;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao;

    public function __construct()
    {
        $this->dataCriacao = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbPessoaId(): ?int
    {
        return $this->user;
    }

    public function setTbPessoaId(?int $tbPessoaId): PersonIugu
    {
        $this->user = $tbPessoaId;
        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): PersonIugu
    {
        $this->customerId = $customerId;
        return $this;
    }

}
