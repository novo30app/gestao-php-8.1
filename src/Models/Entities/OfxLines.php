<?php

namespace App\Models\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @Entity @Table(name="ofxLines",
 *  indexes={
 *      @Index(name="ofxLines_date", columns={"date"})
 *         }
 *  )
 * @ORM @Entity(repositoryClass="App\Models\Repository\OfxLinesRepository")
 */
class OfxLines
{

    const EXPENSE = false; // despesa
    const REVENUE = true; // receita

	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;

	/**
	 * @Column(type="datetime")
	 */
	private \DateTime $created;

	/**
	 * @ManyToOne(targetEntity="UserAdmin")
	 * @JoinColumn(name="user", referencedColumnName="id")
	 */
	private UserAdmin $user;

	/**
	 * @ManyToOne(targetEntity="OfxFiles")
	 * @JoinColumn(name="ofxFiles", referencedColumnName="id")
	 */
	private OfxFiles $ofxFiles;

	/**
	 * @Column(type="date")
	 */
	private ?\DateTime $date = null;

	/**
	 * @Column(type="string")
	 */
	private string $type = '';

	/**
	 * @Column(type="float")
	 */
	private float $value = 0;

	/**
	 * @Column(type="string")
	 */
	private string $uniqueIdentifier = '';

	/**
	 * @Column(type="string")
	 */
	private string $extractNumber = '';

	/**
	 * @Column(type="string")
	 */
	private string $description = '';

	/**
	 * @ManyToOne(targetEntity="Directory")
	 * @JoinColumn(name="directory", referencedColumnName="id")
	 */
	private Directory $directory;

	/**
	 * @ManyToOne(targetEntity="DirectoryAccounts")
	 * @JoinColumn(name="directoryAccount", referencedColumnName="id")
	 */
	private DirectoryAccounts $directoryAccount;

    /**
     * @Column(type="boolean", options={"default"=0})
     */
    private bool $category = false;

    /**
     * @Column(type="boolean", options={"default"=0})
     */
    private bool $multiple = false;

    /**
     * @ManyToOne(targetEntity="PaymentRequirements")
     * @JoinColumn(name="requirements", referencedColumnName="id", nullable=true)
     */
    private ?PaymentRequirements $requirements = null;

    /**
     * @ManyToOne(targetEntity="OfxFiles")
     * @JoinColumn(name="ofx", referencedColumnName="id", nullable=true)
     */
    private ?OfxFiles $ofx = null;

    /**
     * @ManyToOne(targetEntity="OfxLines")
     * @JoinColumn(name="refund", referencedColumnName="id", nullable=true)
     */
    private ?OfxLines $refund = null;


    public function __construct()
	{
		$this->created = new \DateTime();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function getUser(): UserAdmin
	{
		return $this->user;
	}

	public function setUser(UserAdmin $user): OfxLines
	{
		$this->user = $user;
		return $this;
	}

	public function getOfxFiles(): OfxFiles
	{
		return $this->ofxFiles;
	}

	public function setOfxFiles(OfxFiles $ofxFiles): OfxLines
	{
		$this->ofxFiles = $ofxFiles;
		return $this;
	}

	public function getDate(): ?\DateTime
	{
		return $this->date;
	}

	public function setDate(?\DateTime $date): OfxLines
	{
		$this->date = $date;
		return $this;
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function setType(string $type): OfxLines
	{
		$this->type = $type;
		return $this;
	}

	public function getValue(): float
	{
		return $this->value;
	}

	public function setValue(float $value): OfxLines
	{
		$this->value = $value;
		return $this;
	}

	public function getUniqueIdentifier(): string
	{
		return $this->uniqueIdentifier;
	}

	public function setUniqueIdentifier(string $uniqueIdentifier): OfxLines
	{
		$this->uniqueIdentifier = $uniqueIdentifier;
		return $this;
	}

	public function getExtractNumber(): string
	{
		return $this->extractNumber;
	}

	public function setExtractNumber(string $extractNumber): OfxLines
	{
		$this->extractNumber = $extractNumber;
		return $this;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): OfxLines
	{
		$this->description = $description;
		return $this;
	}

	public function getDirectory(): Directory
	{
		return $this->directory;
	}

	public function setDirectory(Directory $directory): OfxLines
	{
		$this->directory = $directory;
		return $this;
	}

	public function getDirectoryAccount(): DirectoryAccounts
	{
		return $this->directoryAccount;
	}

	public function setDirectoryAccount(DirectoryAccounts $directoryAccount): OfxLines
	{
		$this->directoryAccount = $directoryAccount;
		return $this;
	}

    public function getRequirements(): ?PaymentRequirements
    {
        return $this->requirements;
    }

    public function setRequirements(?PaymentRequirements $requirements): OfxLines
    {
        $this->requirements = $requirements;
        return $this;
    }

    public function isCategory(): bool
    {
        return $this->category;
    }

    public function setCategory(bool $category): OfxLines
    {
        $this->category = $category;
        return $this;
    }

    public function getOfx(): ?OfxFiles
    {
        return $this->ofx;
    }

    public function setOfx(?OfxFiles $ofx): OfxLines
    {
        $this->ofx = $ofx;
        return $this;
    }

    public function getRefund(): ?OfxLines
    {
        return $this->refund;
    }

    public function setRefund(?OfxLines $refund): OfxLines
    {
        $this->refund = $refund;
        return $this;
    }

    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    public function setMultiple(bool $multiple): OfxLines
    {
        $this->multiple = $multiple;
        return $this;
    }

}
