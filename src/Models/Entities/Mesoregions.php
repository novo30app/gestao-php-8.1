<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="mesoregions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\MesoregionsRepository")
 */
class Mesoregions
{
    /**
     * @var int
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     * @Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     */
    private $state;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \DateTime|null
     * @Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Mesoregions
    {
        $this->name = $name;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Mesoregions
    {
        $this->state = $state;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): Mesoregions
    {
        $this->user = $user;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): Mesoregions
    {
        $this->created_at = $created_at;
        return $this;
    }
}