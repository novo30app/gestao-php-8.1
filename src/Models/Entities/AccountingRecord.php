<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="accountingRecord")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AccountingRecordRepository")
 */
class AccountingRecord
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @ManyToOne(targetEntity="PaymentRequirements")
     * @JoinColumn(name="payment", referencedColumnName="id")
     */
    private PaymentRequirements $payment;

    /**
     * @Column(type="integer")
     */
    private int $status;

    /**
     * @Column(type="decimal", name="value", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $value;

    /**
     * @Column(type="date")
     */
    private $competition;

    /**
     * @Column(type="string")
     */
    private string $comment;

    /**
     * @Column(type="boolean")
     */
    private bool $accounting;

    /**
     * @Column(type="boolean")
     */
    private bool $fiscal;

    /**
     * @Column(type="integer")
     */
    private int $formAccounting;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): AccountingRecord
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): AccountingRecord
    {
        $this->user = $user;
        return $this;
    }

    public function getPayment(): PaymentRequirements
    {
        return $this->payment;
    }

    public function setPayment(PaymentRequirements $payment): AccountingRecord
    {
        $this->payment = $payment;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): AccountingRecord
    {
        $this->status = $status;
        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getValueFormat()
    {
        $value = $this->value;  
        return number_format($value,2,',','.');   
    }

    public function setValue(float $value): AccountingRecord
    {
        $this->value = $value;
        return $this;
    }

    public function getCompetition(): ?\DateTime
    {
        return $this->competition;
    }

    public function setCompetition(\DateTime $competition): AccountingRecord
    {
        $this->competition = $competition;
        return $this;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function setComment(string $comment): AccountingRecord
    {
        $this->comment = $comment;
        return $this;
    }

    public function getAccounting(): bool
    {
        return $this->accounting;
    }

    public function setAccounting(bool $accounting): AccountingRecord
    {
        $this->accounting = $accounting;
        return $this;
    }

    public function getFiscal(): bool
    {
        return $this->fiscal;
    }

    public function setFiscal(bool $fiscal): AccountingRecord
    {
        $this->fiscal = $fiscal;
        return $this;
    }

    public function getFormAccounting(): int
    {
        return $this->formAccounting;
    }

    public function setFormAccounting(int $formAccounting): AccountingRecord
    {
        $this->formAccounting = $formAccounting;
        return $this;
    }
}