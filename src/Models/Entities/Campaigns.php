<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * @Table(name="campaigns")
 * @Entity(repositoryClass="App\Models\Repository\CampaignsRepository")
 */
class Campaigns
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $dateBegin;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $dateFinal;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     * @var UserAdmin
     */
    private $user;

    /**
     * @Column(type="string")
     */
    private string $description;

    /**
     * @Column(type="integer")
     */
    private int $status;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     * @var State
     */
    private $state;
    
    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     * @var City
     */
    private $city;

    /**
     * @Column(type="string")
     */
    private string $hash;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->hash = Utils::generateToken();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): Campaigns
    {
        $this->created = $created;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Campaigns
    {
        $this->name = $name;
        return $this;
    }

        public function getDateBegin(): \Datetime
    {
        return $this->dateBegin;
    }

    public function setDateBegin(\Datetime $dateBegin): Campaigns
    {
        $this->dateBegin = $dateBegin;
        return $this;
    }

    public function getDateFinal(): ?\Datetime
    {
        return $this->dateFinal;
    }

    public function setDateFinal(\Datetime $dateFinal): Campaigns
    {
        $this->dateFinal = $dateFinal;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): Campaigns
    {
        $this->user = $user;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Campaigns
    {
        $this->description = $description;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;   
    }

    public function getStatusString(): string
    {
        $status = match ($this->status) {
            '1' => 'Ativa',
            '2' => 'Inativa',
            default => ''
        };
        return $status;   
    }

    public function setStatus(int $status): Campaigns
    {
        $this->status = $status;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Campaigns
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): Campaigns
    {
        $this->city = $city;
        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): Campaigns
    {
        $this->hash = $hash;
        return $this;
    }
}