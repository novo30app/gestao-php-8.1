<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="directoryExtract")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryExtractRepository")
 */
class DirectoryExtract
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;


    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @Column(type="integer")
     */
    private int $account;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $dateStart;


    /**
     * @Column(type="datetime")
     */
    private \Datetime $dateEnd;

    /**
     * @Column(type="boolean")
     */
    private bool $validation;

    /**
     * @Column(type="text")
     */
    private string $attachmentExtract;

    /**
     * @Column(type="boolean")
     */
    private bool $status;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): DirectoryExtract
    {
        $this->user = $user;
        return $this;
    }

    public function getAccount(): int
    {
        return $this->account;
    }

    public function setAccount(int $account): DirectoryExtract
    {
        $this->account = $account;
        return $this;
    }

    public function getDateStart(): \DateTime
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTime $dateStart): DirectoryExtract
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    public function getDateEnd(): \DateTime
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTime $dateEnd): DirectoryExtract
    {
        $this->dateEnd = $dateEnd;
        return $this;
    }
    
    public function getValidation(): bool
    {
        return $this->validation;
    }

    public function setValidation(bool $validation): DirectoryExtract
    {
        $this->validation = $validation;
        return $this;
    }

    public function getAttachmentExtract(): string
    {
        return $this->attachmentExtract;
    }

    public function setAttachmentExtract(string $attachmentExtract): DirectoryExtract
    {
        $this->attachmentExtract = $attachmentExtract;
        return $this;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): DirectoryExtract
    {
        $this->status = $status;
        return $this;
    }
}