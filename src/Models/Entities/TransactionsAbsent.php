<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="transactionsAbsent")
 * @ORM @Entity(repositoryClass="App\Models\Repository\TransactionsAbsentRepository")
 */
class TransactionsAbsent
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $invoiceId;

    /**
     * @Column(type="string")
     */
    private string $payment_method;

    /**
     * @Column(type="string")
     */
    private string $status;

    /**
     * @Column(type="decimal", precision=13, scale=4)
     * @var float
     */
    private $paid_value;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $occurrence_date;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $due_date;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $paid_at;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getInvoiceId(): string
    {
        return $this->invoiceId;
    }

    public function setInvoiceId(string $invoiceId): TransactionsAbsent
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    public function getPayment_method(): string
    {
        return $this->payment_method;
    }

    public function setPayment_method(string $payment_method): TransactionsAbsent
    {
        $this->payment_method = $payment_method;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): TransactionsAbsent
    {
        $this->status = $status;
        return $this;
    }

    public function getPaid_value(): float
    {
        return $this->paid_value;
    }

    public function setPaid_value(float $paid_value): TransactionsAbsent
    {
        $this->paid_value = $paid_value;
        return $this;
    }

    public function getOccurrence_date(): \DateTime
    {
        return $this->occurrence_date;
    }

    public function setOccurrence_date(\DateTime $occurrence_date): TransactionsAbsent
    {
        $this->occurrence_date = $occurrence_date;
        return $this;
    }

    public function getDue_date(): \DateTime
    {
        return $this->due_date;
    }

    public function setDue_date(\DateTime $due_date): TransactionsAbsent
    {
        $this->due_date = $due_date;
        return $this;
    }

    public function getPaid_at(): \DateTime
    {
        return $this->paid_at;
    }

    public function setPaid_at(\DateTime $paid_at): TransactionsAbsent
    {
        $this->paid_at = $paid_at;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): TransactionsAbsent
    {
        $this->created_at = $created_at;
        return $this;
    }
}