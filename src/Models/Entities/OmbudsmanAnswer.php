<?php


namespace App\Models\Entities;


/**
 * @Entity @Table(name="ombudsmanAnswers")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OmbudsmanAnswerRepository")
 */

class OmbudsmanAnswer
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $answer = '';

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): OmbudsmanAnswer
    {
        $this->answer = $answer;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): OmbudsmanAnswer
    {
        $this->active = $active;
        return $this;
    }

    public function getActiveString(): string {
        return $this->active == 1 ? 'Ativo' : 'Inativo';
    }

}