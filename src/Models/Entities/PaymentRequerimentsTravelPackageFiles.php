<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="paymentRequirementsTravelPackageFiles")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PaymentRequirementsTravelPackageFilesRepository")
 */
class PaymentRequerimentsTravelPackageFiles
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;

	/**
	 * @ManyToOne(targetEntity="PaymentRequerimentsTravelPackage", inversedBy="files")
	 * @JoinColumn(name="package", referencedColumnName="id")
	 */
	private PaymentRequerimentsTravelPackage $package;

	/**
	 * @Column(type="string")
	 */
	private string $file = '';

	/**
	 * @Column(type="boolean")
	 */
	private bool $active;


	public function getId(): ?int
	{
		return $this->id;
	}

	public function getPackage(): PaymentRequerimentsTravelPackage
	{
		return $this->package;
	}

	public function setPackage(PaymentRequerimentsTravelPackage $package): PaymentRequerimentsTravelPackageFiles
	{
		$this->package = $package;
		return $this;
	}

	public function getFile(): string
	{
		return Utils::formatAttachment($this->file);
	}
	public function setFile(string $file): PaymentRequerimentsTravelPackageFiles
	{
		$this->file = $file;
		return $this;
	}

	public function getActive(): ?bool
	{
		return $this->active;
	}

	public function setActive(?bool $active): PaymentRequerimentsTravelPackageFiles
	{
		$this->active = $active;
		return $this;
	}
}