<?php
namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * @Table(name="eventsDocuments")
 * @Entity(repositoryClass="App\Models\Repository\EventsDocumentsRepository")
 */
class EventsDocuments
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     * @Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     * @var UserAdmin
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="event", referencedColumnName="id", nullable=true)
     * @var Events
     */
    private $event;

    /**
     * @var int|null
     * @Column(name="type", type="integer", length=0, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     * @Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     * @Column(name="name", type="string", length=0, nullable=true)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function seCreated(\DateTime $created): EventsDocuments
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): EventsDocuments
    {
        $this->user = $user;
        return $this;
    }

    public function getEvent(): Events
    {
        return $this->event;
    }

    public function setEvent(Events $event): EventsDocuments
    {
        $this->event = $event;
        return $this;
    }

    public function getTypeString()
    {
        switch ($this->type) {
            case 1:
                $type = 'Autorização';
                break;
            case 2:
                $type = 'Comunicado';
                break;
            default:
                $type = 'Outros';
                break;
        }
        return $type;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): EventsDocuments
    {
        $this->type = $type;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): EventsDocuments
    {
        $this->description = $description;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): EventsDocuments
    {
        $this->name = $name;
        return $this;
    }
}