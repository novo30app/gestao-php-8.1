<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="supplier")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SupplierRepository")
 */
class Supplier
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @Column(type="string")
	 */
	private string $cpfCnpj = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $corporateName = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $nameResponsible = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $cpfResponsible = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $phone = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $email = '';
	
	/**
	 * @ManyToOne(targetEntity="Bank")
	 * @JoinColumn(name="bank", referencedColumnName="id")
	 */
	private ?Bank $bank = null;
	
	/**
	 * @Column(type="string")
	 */
	private string $agency = '';
	
	/**
	 * @Column(type="string")
	 */
	private string $account = '';
	
	/**
	 * @Column(type="boolean")
	 */
	private bool $active = true;
	
	/**
	 * @Column(type="boolean")
	 */
	private bool $status = true;
	
	
	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCpfCnpj(): string
	{
		return $this->cpfCnpj;
	}

	public function setCpfCnpj(string $cpfCnpj): Supplier
	{
		$this->cpfCnpj = $cpfCnpj;
		return $this;
	}

	public function getCorporateName(): string
	{
		return $this->corporateName;
	}

	public function setCorporateName(string $corporateName): Supplier
	{
		$this->corporateName = $corporateName;
		return $this;
	}

	public function getNameResponsible(): string
	{
		return $this->nameResponsible;
	}

	public function setNameResponsible(string $nameResponsible): Supplier
	{
		$this->nameResponsible = $nameResponsible;
		return $this;
	}

	public function getCpfResponsible(): string
	{
		return $this->cpfResponsible;
	}

	public function setCpfResponsible(string $cpfResponsible): Supplier
	{
		$this->cpfResponsible = $cpfResponsible;
		return $this;
	}

	public function getPhone(): string
	{
		return $this->phone;
	}

	public function setPhone(string $phone): Supplier
	{
		$this->phone = $phone;
		return $this;
	}

	public function getEmail(): string
	{
		return $this->email;
	}

	public function setEmail(string $email): Supplier
	{
		$this->email = $email;
		return $this;
	}

	public function getBank(): ?Bank
	{
		return $this->bank;
	}

	public function setBank(?Bank $bank): Supplier
	{
		$this->bank = $bank;
		return $this;
	}

	public function getAgency(): string
	{
		return $this->agency;
	}

	public function setAgency(string $agency): Supplier
	{
		$this->agency = $agency;
		return $this;
	}

	public function getAccount(): string
	{
		return $this->account;
	}

	public function setAccount(string $account): Supplier
	{
		$this->account = $account;
		return $this;
	}

	public function isActive(): bool
	{
		return $this->active;
	}

	public function setActive(bool $active): Supplier
	{
		$this->active = $active;
		return $this;
	}
	
	public function changeActive(): Supplier
	{
		if (!$this->active) {
			$this->active = 1;
			return $this;
		}
		$this->active = 0;
		return $this;
	}
	
	public function activeStr(): string
	{
		if (1 == $this->active) return "Ativo";
		return "Inativo";
	}
	
	public function isStatus(): bool
	{
		return $this->status;
	}

	public function setStatus(bool $status): Supplier
	{
		$this->status = $status;
		return $this;
	}
}