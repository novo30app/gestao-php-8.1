<?php

namespace App\Models\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @Entity @Table(name="ofxLinesPaymentRequirements")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OfxLinePaymentRequirementRepository")
 */
class OfxLinePaymentRequirement
{

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="OfxLines")
     * @JoinColumn(name="ofxLine", referencedColumnName="id")
     */
    private OfxLines $ofxLine;

    /**
     * @ManyToOne(targetEntity="PaymentRequirements")
     * @JoinColumn(name="requirements", referencedColumnName="id")
     */
    private PaymentRequirements $requirements;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getOfxLine(): OfxLines
    {
        return $this->ofxLine;
    }

    public function setOfxLine(OfxLines $ofxLine): OfxLinePaymentRequirement
    {
        $this->ofxLine = $ofxLine;
        return $this;
    }

    public function getRequirements(): PaymentRequirements
    {
        return $this->requirements;
    }

    public function setRequirements(PaymentRequirements $requirements): OfxLinePaymentRequirement
    {
        $this->requirements = $requirements;
        return $this;
    }

}
