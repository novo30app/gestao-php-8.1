<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="resultadoEleicao")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ResultadoEleicaoRepository")
 */
class ResultadoEleicao
{

    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $DT_GERACAO = '';

    /**
     * @Column(type="string")
     */
    private string $HH_GERACAO = '';

    /**
     * @Column(type="string")
     */
    private string $ANO_ELEICAO = '';

    /**
     * @Column(type="string")
     */
    private string $CD_TIPO_ELEICAO = '';

    /**
     * @Column(type="string")
     */
    private string $NM_TIPO_ELEICAO = '';

    /**
     * @Column(type="string")
     */
    private string $NR_TURNO = '';

    /**
     * @Column(type="string")
     */
    private string $CD_ELEICAO = '';

    /**
     * @Column(type="string")
     */
    private string $DS_ELEICAO = '';

    /**
     * @Column(type="string")
     */
    private string $DT_ELEICAO = '';

    /**
     * @Column(type="string")
     */
    private string $TP_ABRANGENCIA = '';

    /**
     * @Column(type="string")
     */
    private string $SG_UF = '';

    /**
     * @Column(type="string")
     */
    private string $SG_UE = '';

    /**
     * @Column(type="string")
     */
    private string $NM_UE = '';

    /**
     * @Column(type="string")
     */
    private string $CD_MUNICIPIO = '';

    /**
     * @Column(type="string")
     */
    private string $NM_MUNICIPIO = '';

    /**
     * @Column(type="string")
     */
    private string $NR_ZONA = '';

    /**
     * @Column(type="string")
     */
    private string $NR_SECAO = '';

    /**
     * @Column(type="string")
     */
    private string $CD_CARGO = '';

    /**
     * @Column(type="string")
     */
    private string $DS_CARGO = '';

    /**
     * @Column(type="string")
     */
    private string $NR_VOTAVEL = '';

    /**
     * @Column(type="string")
     */
    private string $NM_VOTAVEL = '';

    /**
     * @Column(type="string")
     */
    private string $QT_VOTOS = '';

    /**
     * @Column(type="string")
     */
    private string $NR_LOCAL_VOTACAO = '';

    /**
     * @Column(type="string")
     */
    private string $SQ_CANDIDATO = '';

    /**
     * @Column(type="string")
     */
    private string $NM_LOCAL_VOTACAO = '';

    /**
     * @Column(type="string")
     */
    private string $DS_LOCAL_VOTACAO_ENDERECO = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private string $latitude = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private string $longitude = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDTGERACAO(): string
    {
        return $this->DT_GERACAO;
    }

    public function setDTGERACAO(string $DT_GERACAO): ResultadoEleicao
    {
        $this->DT_GERACAO = $DT_GERACAO;
        return $this;
    }

    public function getHHGERACAO(): string
    {
        return $this->HH_GERACAO;
    }

    public function setHHGERACAO(string $HH_GERACAO): ResultadoEleicao
    {
        $this->HH_GERACAO = $HH_GERACAO;
        return $this;
    }

    public function getANOELEICAO(): string
    {
        return $this->ANO_ELEICAO;
    }

    public function setANOELEICAO(string $ANO_ELEICAO): ResultadoEleicao
    {
        $this->ANO_ELEICAO = $ANO_ELEICAO;
        return $this;
    }

    public function getCDTIPOELEICAO(): string
    {
        return $this->CD_TIPO_ELEICAO;
    }

    public function setCDTIPOELEICAO(string $CD_TIPO_ELEICAO): ResultadoEleicao
    {
        $this->CD_TIPO_ELEICAO = $CD_TIPO_ELEICAO;
        return $this;
    }

    public function getNMTIPOELEICAO(): string
    {
        return $this->NM_TIPO_ELEICAO;
    }

    public function setNMTIPOELEICAO(string $NM_TIPO_ELEICAO): ResultadoEleicao
    {
        $this->NM_TIPO_ELEICAO = $NM_TIPO_ELEICAO;
        return $this;
    }

    public function getNRTURNO(): string
    {
        return $this->NR_TURNO;
    }

    public function setNRTURNO(string $NR_TURNO): ResultadoEleicao
    {
        $this->NR_TURNO = $NR_TURNO;
        return $this;
    }

    public function getCDELEICAO(): string
    {
        return $this->CD_ELEICAO;
    }

    public function setCDELEICAO(string $CD_ELEICAO): ResultadoEleicao
    {
        $this->CD_ELEICAO = $CD_ELEICAO;
        return $this;
    }

    public function getDSELEICAO(): string
    {
        return $this->DS_ELEICAO;
    }

    public function setDSELEICAO(string $DS_ELEICAO): ResultadoEleicao
    {
        $this->DS_ELEICAO = $DS_ELEICAO;
        return $this;
    }

    public function getDTELEICAO(): string
    {
        return $this->DT_ELEICAO;
    }

    public function setDTELEICAO(string $DT_ELEICAO): ResultadoEleicao
    {
        $this->DT_ELEICAO = $DT_ELEICAO;
        return $this;
    }

    public function getTPABRANGENCIA(): string
    {
        return $this->TP_ABRANGENCIA;
    }

    public function setTPABRANGENCIA(string $TP_ABRANGENCIA): ResultadoEleicao
    {
        $this->TP_ABRANGENCIA = $TP_ABRANGENCIA;
        return $this;
    }

    public function getSGUF(): string
    {
        return $this->SG_UF;
    }

    public function setSGUF(string $SG_UF): ResultadoEleicao
    {
        $this->SG_UF = $SG_UF;
        return $this;
    }

    public function getSGUE(): string
    {
        return $this->SG_UE;
    }

    public function setSGUE(string $SG_UE): ResultadoEleicao
    {
        $this->SG_UE = $SG_UE;
        return $this;
    }

    public function getNMUE(): string
    {
        return $this->NM_UE;
    }

    public function setNMUE(string $NM_UE): ResultadoEleicao
    {
        $this->NM_UE = $NM_UE;
        return $this;
    }

    public function getCDMUNICIPIO(): string
    {
        return $this->CD_MUNICIPIO;
    }

    public function setCDMUNICIPIO(string $CD_MUNICIPIO): ResultadoEleicao
    {
        $this->CD_MUNICIPIO = $CD_MUNICIPIO;
        return $this;
    }

    public function getNMMUNICIPIO(): string
    {
        return $this->NM_MUNICIPIO;
    }

    public function setNMMUNICIPIO(string $NM_MUNICIPIO): ResultadoEleicao
    {
        $this->NM_MUNICIPIO = $NM_MUNICIPIO;
        return $this;
    }

    public function getNRZONA(): string
    {
        return $this->NR_ZONA;
    }

    public function setNRZONA(string $NR_ZONA): ResultadoEleicao
    {
        $this->NR_ZONA = $NR_ZONA;
        return $this;
    }

    public function getNRSECAO(): string
    {
        return $this->NR_SECAO;
    }

    public function setNRSECAO(string $NR_SECAO): ResultadoEleicao
    {
        $this->NR_SECAO = $NR_SECAO;
        return $this;
    }

    public function getCDCARGO(): string
    {
        return $this->CD_CARGO;
    }

    public function setCDCARGO(string $CD_CARGO): ResultadoEleicao
    {
        $this->CD_CARGO = $CD_CARGO;
        return $this;
    }

    public function getDSCARGO(): string
    {
        return $this->DS_CARGO;
    }

    public function setDSCARGO(string $DS_CARGO): ResultadoEleicao
    {
        $this->DS_CARGO = $DS_CARGO;
        return $this;
    }

    public function getNRVOTAVEL(): string
    {
        return $this->NR_VOTAVEL;
    }

    public function setNRVOTAVEL(string $NR_VOTAVEL): ResultadoEleicao
    {
        $this->NR_VOTAVEL = $NR_VOTAVEL;
        return $this;
    }

    public function getNMVOTAVEL(): string
    {
        return $this->NM_VOTAVEL;
    }

    public function setNMVOTAVEL(string $NM_VOTAVEL): ResultadoEleicao
    {
        $this->NM_VOTAVEL = $NM_VOTAVEL;
        return $this;
    }

    public function getQTVOTOS(): string
    {
        return $this->QT_VOTOS;
    }

    public function setQTVOTOS(string $QT_VOTOS): ResultadoEleicao
    {
        $this->QT_VOTOS = $QT_VOTOS;
        return $this;
    }

    public function getNRLOCALVOTACAO(): string
    {
        return $this->NR_LOCAL_VOTACAO;
    }

    public function setNRLOCALVOTACAO(string $NR_LOCAL_VOTACAO): ResultadoEleicao
    {
        $this->NR_LOCAL_VOTACAO = $NR_LOCAL_VOTACAO;
        return $this;
    }

    public function getSQCANDIDATO(): string
    {
        return $this->SQ_CANDIDATO;
    }

    public function setSQCANDIDATO(string $SQ_CANDIDATO): ResultadoEleicao
    {
        $this->SQ_CANDIDATO = $SQ_CANDIDATO;
        return $this;
    }

    public function getNMLOCALVOTACAO(): string
    {
        return $this->NM_LOCAL_VOTACAO;
    }

    public function setNMLOCALVOTACAO(string $NM_LOCAL_VOTACAO): ResultadoEleicao
    {
        $this->NM_LOCAL_VOTACAO = $NM_LOCAL_VOTACAO;
        return $this;
    }

    public function getDSLOCALVOTACAOENDERECO(): string
    {
        return $this->DS_LOCAL_VOTACAO_ENDERECO;
    }

    public function setDSLOCALVOTACAOENDERECO(string $DS_LOCAL_VOTACAO_ENDERECO): ResultadoEleicao
    {
        $this->DS_LOCAL_VOTACAO_ENDERECO = $DS_LOCAL_VOTACAO_ENDERECO;
        return $this;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): ResultadoEleicao
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): ResultadoEleicao
    {
        $this->longitude = $longitude;
        return $this;
    }
}
