<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="logApproved")
 * @ORM @Entity(repositoryClass="App\Models\Repository\LogApprovedRepository")
 */
class LogApproved
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @Column(type="datetime")
	 */
	private \DateTime $created;
	
	/**
	 * @ManyToOne(targetEntity="UserAdmin")
	 * @JoinColumn(name="user", referencedColumnName="id")
	 */
	private UserAdmin $user;
	
	/**
	 * @ManyToOne(targetEntity="PaymentRequirements")
	 * @JoinColumn(name="paymentRequirements", referencedColumnName="id")
	 */
	private PaymentRequirements $paymentRequirements;
	
	/**
	 * @Column(type="integer")
	 */
	private int $type;
	
	public function __construct()
	{
		$this->created = new \DateTime();
	}
	

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function getUser(): UserAdmin
	{
		return $this->user;
	}

	public function setUser(UserAdmin $user): LogApproved
	{
		$this->user = $user;
		return $this;
	}

	public function getPaymentRequirements(): PaymentRequirements
	{
		return $this->paymentRequirements;
	}

	public function setPaymentRequirements(PaymentRequirements $paymentRequirements): LogApproved
	{
		$this->paymentRequirements = $paymentRequirements;
		return $this;
	}

	public function getType(): int
	{
		return $this->type;
	}

	public function setType(int $type): LogApproved
	{
		$this->type = $type;
		return $this;
	}
}