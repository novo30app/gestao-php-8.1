<?php
namespace App\Models\Entities;
use Doctrine\Mapping as ORM;
use App\Helpers\Utils;

/**
 * TbEventosParticipantes 
 *
 * @Table(name="tb_eventos_participantes", indexes={@Index(name="idx_tb_eventos_participantes_tb_eventos_id", columns={"tb_eventos_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsRegistrationsRepository")
 */
class EventsRegistrations
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="tb_eventos_id", referencedColumnName="id", nullable=false)
     * @var Events
     */
    private $tbEventos;

    /**
     * @var int
     *
     * @Column(name="tb_eventos_precos_id", type="integer", nullable=false)
     */
    private $tbEventosPrecosId;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_transacao_id", type="integer", nullable=true)
     */
    private $tbEventosTransacaoId;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="tb_pessoa_id", referencedColumnName="id", nullable=false)
     * @var User
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var int|null
     *
     * @Column(name="telefone_numero", type="string", nullable=true)
     */
    private $telefoneNumero;

    /**
     * @var int|null
     *
     * @Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var int|null
     *
     * @Column(name="cep", type="integer", nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var int|null
     *
     * @Column(name="numero", type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var string
     *
     * @Column(name="status", type="string", length=0, nullable=false, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var string|null
     *
     * @Column(name="checkin_hash", type="string", length=80, nullable=true)
     */
    private $checkinHash;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_checkin", type="datetime", nullable=true)
     */
    private $dataCheckin;

    /**
     * @var string|null
     *
     * @Column(name="recommendation", type="string", length=155, nullable=true)
     */
    private $recommendation;


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): EventsRegistrations
    {
        $this->nome = $nome;
        return $this;
    }

    public function getData()
    {
        return $this->dataCriacao;
    }

    public function setData(\DateTime $dataCriacao): EventsRegistrations
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getCpf()
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): EventsRegistrations
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getPessoaId(): ?User
    {
        return $this->tb_pessoa_id;
    }

    public function getPessoa(): ?User
    {
        return $this->tbPessoa;
    }

    public function getMunicipio(): ?string
    {
        return $this->cidade;
    }
    
    public function geTelefoneNumero(): ?string
    {
        return $this->telefoneNumero;
    }

    public function setTelefoneNumero(string $telefoneNumero): EventsRegistrations
    {
        $this->telefoneNumero = $telefoneNumero;
        return $this;
    }

    public function getValorFormated(): ?string
    {
        $valor = $this->valor;  
        return "R$ " . number_format($valor,2,',','.');   
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValue(string $valor): EventsRegistrations
    {
        $this->valor = $valor;
        return $this;
    }

    public function getTbEventos(): Events
    {
        return $this->tbEventos;
    }

    public function setTbEventos(Events $tbEventos): EventsRegistrations
    {
        $this->tbEventos = $tbEventos;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): EventsRegistrations
    {
        $this->status = $status;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): EventsRegistrations
    {
        $this->code = $code;
        return $this;
    }

    public function getDataCheckin(): ?\DateTime
    {
        return $this->dataCheckin;
    }

    public function setDataCheckin(?\DateTime $dataCheckin): EventsRegistrations
    {
        $this->dataCheckin = $dataCheckin;
        return $this;
    }
    
    public function getTbEventosPrecosId(): ?int
    {
        return $this->tbEventosPrecosId;
    }

    public function setTbEventosPrecosId(?int $tbEventosPrecosId): EventsRegistrations
    {
        $this->tbEventosPrecosId = $tbEventosPrecosId;
        return $this;
    }

    public function getTbEventosTransacaoId(): ?int
    {
        return $this->tbEventosTransacaoId;
    }

    public function setTbEventosTransacaoId(?int $tbEventosTransacaoId): EventsRegistrations
    {
        $this->tbEventosTransacaoId = $tbEventosTransacaoId;
        return $this;
    }

    public function getTbPessoaId(): User
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(User $tbPessoaId): EventsRegistrations
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    public function setDataNascimento(?\DateTime $dataNascimento): EventsRegistrations
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): EventsRegistrations
    {
        $this->email = $email;
        return $this;
    }

    public function getRecommendation(): ?string
    {
        return $this->recommendation;
    }

    public function setRecommendation(?string $recommendation): EventsParticipants
    {
        $this->recommendation = $recommendation;
        return $this;
    }
}
