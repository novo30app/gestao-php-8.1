<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="cepMembers")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CepMembersRepository")
 */
class CepMembers
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     * @Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var datetime|null
     * @Column(name="dateElected", type="date", nullable=false)
     */
    private $dateElected;

    /**
     * @var string|null
     * @Column(name="file", type="string", nullable=false)
     */
    private $file;

    /**
     * @var bool|null
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;
    
    /**
     * @var string|null
     * @Column(name="lastUpdate", type="string", nullable=true)
     */
    private $lastUpdate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CepMembers
    {
        $this->name = $name;
        return $this;
    }

    public function getDateElected(): \Datetime
    {
        return $this->dateElected;
    }

    public function setDateElected(\Datetime $dateElected): CepMembers
    {
        $this->dateElected = $dateElected;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): CepMembers
    {
        $this->file = $file;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): CepMembers
    {
        $this->active = $active;
        return $this;
    }

    public function getLastUpdate(): ?string
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(string $lastUpdate): CepMembers
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }
}