<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="keyWords")
 * @ORM @Entity(repositoryClass="App\Models\Repository\KeyWordRepository")
 */
class KeyWord
{

    const DAM_INTERACTIONS = 1;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $type = 1;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): KeyWord
    {
        $this->type = $type;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): KeyWord
    {
        $this->name = $name;
        return $this;
    }


}