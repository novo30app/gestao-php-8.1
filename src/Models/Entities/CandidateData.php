<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="candidatesData")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CandidateDataRepository")
 */
class CandidateData
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private string $number = '';

    /**
     * @Column(type="integer")
     */
    private int $year = 0;

    /**
     * @ManyToOne(targetEntity="Position")
     * @JoinColumn(name="position", referencedColumnName="id")
     */
    private Position $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CandidateData
    {
        $this->name = $name;
        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): CandidateData
    {
        $this->year = $year;
        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): CandidateData
    {
        $this->number = $number;
        return $this;
    }

    public function getPosition(): Position
    {
        return $this->position;
    }

    public function setPosition(Position $position): CandidateData
    {
        $this->position = $position;
        return $this;
    }
}
