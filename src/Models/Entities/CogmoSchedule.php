<?php

namespace App\Models\Entities;
use Doctrine\Mapping as ORM;

/**
 * 
 * @Entity @Table(name="cogmo_schedule")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CogmoScheduleRepository")
 */
class CogmoSchedule
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private User $contact_id;

    /**
     * @Column(name="contact_number", type="string")
     */
    private string $contact_number;

    /**
     * @ManyToOne(targetEntity="Transaction")
     * @JoinColumn(name="transaction", referencedColumnName="id")
     */
    private Transaction $transaction;

    /**
     * @ManyToOne(targetEntity="CogmoPhones")
     * @JoinColumn(name="phone_number", referencedColumnName="id")
     */
    private CogmoPhones $phone_number;

    /**
     * @Column(name="message", type="text", nullable=true)
     */
    private string $message;

    /**
     * @Column(name="scheduled_time", type="datetime")
     */
    private \DateTime $scheduled_time;

    /**
     * @Column(name="sent_time", type="datetime", nullable=true)
     */
    private ?\DateTime $sent_time;

    /**
     * @Column(name="status", type="string", length=20)
     */
    private string $status = 'pending';

    /**
     * @Column(name="error_message", type="text", nullable=true)
     */
    private ?string $error_message;

    /**
     * @Column(name="created_at", type="datetime")
     */
    private \DateTime $created_at;

    /**
     * @Column(name="updated_at", type="datetime")
     */
    private \DateTime $updated_at;

    // Getters
    public function getId(): int 
    { 
        return $this->id; 
    }

    public function getContactId(): User
    { 
        return $this->contact_id; 
    }

    public function getContactNumber(): string
    { 
        return $this->contact_number; 
    }

    public function getTransaction(): Transaction
    { 
        return $this->transaction; 
    }   

    public function getPhoneNumber(): CogmoPhones
    { 
        return $this->phone_number; 
    }

    public function getMessage(): string 
    { 
        return $this->message; 
    }

    public function getScheduledTime(): \DateTime 
    { 
        return $this->scheduled_time; 
    }

    public function getSentTime(): \DateTime 
    { 
        return $this->sent_time; 
    }

    public function getStatus(): string 
    {  
        return $this->status; 
    }

    public function getErrorMessage(): string 
    { 
        return $this->error_message; 
    }

    public function getCreatedAt(): \DateTime 
    { 
        return $this->created_at; 
    }
    public function getUpdatedAt(): \DateTime 
    { 
        return $this->updated_at; 
    }

    // Setters
    public function setContactId(User $contact_id): CogmoSchedule
    { 
        $this->contact_id = $contact_id; 
        return $this; 
    }

    public function setContactNumber(string $contact_number): CogmoSchedule
    { 
        $this->contact_number = $contact_number; 
        return $this; 
    }

    public function setTransaction(Transaction $transaction): CogmoSchedule
    { 
        $this->transaction = $transaction; 
        return $this; 
    }

    public function setPhoneNumber(CogmoPhones $phone_number): CogmoSchedule
    { 
        $this->phone_number = $phone_number; 
        return $this; 
    }

    public function setMessage(string $message): CogmoSchedule
    { 
        $this->message = $message; 
        return $this; 
    }

    public function setScheduledTime(\DateTime $scheduled_time): CogmoSchedule
    { 
        $this->scheduled_time = $scheduled_time; 
        return $this; 
    }
    public function setSentTime(\DateTime $sent_time): CogmoSchedule
    { 
        $this->sent_time = $sent_time; 
        return $this; 
    }

    public function setStatus(string $status): CogmoSchedule
    { 
        $this->status = $status; 
        return $this; 
    }
    public function setErrorMessage(string $error_message): CogmoSchedule
    { 
        $this->error_message = $error_message; 
        return $this; 
    }

    public function setCreatedAt(\DateTime $created_at): CogmoSchedule
    { 
        $this->created_at = $created_at; 
        return $this; 
    }

    public function setUpdatedAt(): CogmoSchedule
    { 
        $this->updated_at = new \DateTime(); 
        return $this; 
    }
}