<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="permissionUserTypeFuncionality")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PermissionUserTypeFuncionalityRepository")
 */
class PermissionUserTypeFuncionality
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @ManyToOne(targetEntity="UsersTypes")
     * @JoinColumn(name="userType", referencedColumnName="id")
     */
    private UsersTypes $userType;

    /**
     * @ManyToOne(targetEntity="SystemFeatures")
     * @JoinColumn(name="systemFeature", referencedColumnName="id")
     */
    private SystemFeatures $systemFeature;

    /**
     * @ManyToOne(targetEntity="Sectors")
     * @JoinColumn(name="sector", referencedColumnName="id")
     */
    private Sectors $sector;

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): PermissionUserTypeFuncionality
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): PermissionUserTypeFuncionality
    {
        $this->user = $user;
        return $this;
    }

    public function getUserType(): UsersTypes
    {
        return $this->userType;
    }

    public function setUserType(UsersTypes $userType): PermissionUserTypeFuncionality
    {
        $this->userType = $userType;
        return $this;
    }

    public function getSystemFeatures(): SystemFeatures
    {
        return $this->systemFeature;
    }

    public function setSystemFeatures(SystemFeatures $systemFeature): PermissionUserTypeFuncionality
    {
        $this->systemFeature = $systemFeature;
        return $this;
    }

    public function getSector(): Sectors
    {
        return $this->sector;
    }

    public function setSector(Sectors $sector): PermissionUserTypeFuncionality
    {
        $this->sector = $sector;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): PermissionUserTypeFuncionality
    {
        $this->active = $active;
        return $this;
    }
}