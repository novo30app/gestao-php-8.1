<?php

namespace App\Models\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity @Table(name="area")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AreaRepository")
 */
class Area
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;

	/**
	 * @Column(type="string")
	 */
	private string $name = '';

    /**
     * @ManyToMany(targetEntity="Directory", inversedBy="areas")
     * @JoinTable(name="area_directories")
     */
    private Collection $directories;

    public function __construct()
    {
        $this->directories = new ArrayCollection();
    }


	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): Area
	{
		$this->name = $name;
		return $this;
	}

    public function getDirectories(): Collection
    {
        return $this->directories;
    }

    public function addDirectory(Directory $directory): void
    {
        if ($this->directories->contains($directory)) {
            return;
        }
        $this->directories->add($directory);
        $directory->addArea($this);
    }
}
