<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * @Table(name="campaignsUsers")
 * @Entity(repositoryClass="App\Models\Repository\CampaignsUsersRepository")
 */
class CampaignsUsers
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     * @var User
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="Campaigns")
     * @JoinColumn(name="campaign", referencedColumnName="id", nullable=true)
     * @var Campaigns
     */
    private $campaign;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): CampaignsUsers
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): CampaignsUsers
    {
        $this->user = $user;
        return $this;
    }

    public function getCampaign(): Campaigns
    {
        return $this->campaign;
    }

    public function setCampaign(Campaigns $campaign): CampaignsUsers
    {
        $this->campaign = $campaign;
        return $this;
    }
}