<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="interactionKeyWords")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionKeyWordRepository")
 */
class InteractionKeyWord
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="KeyWord")
     * @JoinColumn(name="keyWord", referencedColumnName="id")
     */
    private KeyWord $keyWord;

    /**
     * @ManyToOne(targetEntity="Interaction")
     * @JoinColumn(name="interaction", referencedColumnName="id")
     */
    private Interaction $interaction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyWord(): KeyWord
    {
        return $this->keyWord;
    }

    public function setKeyWord(KeyWord $keyWord): InteractionKeyWord
    {
        $this->keyWord = $keyWord;
        return $this;
    }

    public function getInteraction(): Interaction
    {
        return $this->interaction;
    }

    public function setInteraction(Interaction $interaction): InteractionKeyWord
    {
        $this->interaction = $interaction;
        return $this;
    }

}