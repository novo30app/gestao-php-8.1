<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicatedDirectoriesMembers")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicatedDirectoriesMembersRepository")
 */
class IndicatedDirectoriesMembers
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @Column(type="string")
     * @var string
     */
    private $hash;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private UserAdmin $user;

    /**
     * @ManyToOne(targetEntity="QuestionnaireReply")
     * @JoinColumn(name="questionnaire", referencedColumnName="id")
     */
    private QuestionnaireReply $questionnaire;

    /**
     * @ManyToOne(targetEntity="ExatoDigital")
     * @JoinColumn(name="exatoDigital", referencedColumnName="id", nullable=true)
     */
    private ?ExatoDigital $exatoDigital = null;

    /**
     * @ManyToOne(targetEntity="IndicatedDirectories")
     * @JoinColumn(name="indication", referencedColumnName="id", nullable=true)
     */
    private $indication;

    /**
     * @Column(type="date")
     * @var \Datetime
     */
    private $mandateBegin;

    /**
     * @Column(type="date")
     * @var \Datetime
     */
    private $mandateEnd;

    /**
     * @Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phone = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $cpf;

    /**
     * @Column(type="string")
     */
    private $rg = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $voterTitle;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $voterTitleZone;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $voterTitleSection;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $office;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $status;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private $city;

    /**
     * @Column(type="string")
     * @var string
     */
    private $zipCode = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $street;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $number = 0;

    /**
     * @Column(type="string")
     * @var string
     */
    private $district;

    /**
     * @Column(type="string")
     * @var string
     */
    private $complement;

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $active;

    /**
     * @Column(type="string")
     * @var string
     */
    private $proofOfResidence = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $cnh = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $rgFile = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $cpfFile = '';

    /**
     * @Column(type="text")
     * @var string
     */
    private $question1 = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $question2 = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $question3 = '';

    /**
     * @Column(type="integer")
     * @var int
     */
    private $approvalDN = 0;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $approvalDE = 0;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="approver", referencedColumnName="id", nullable=true)
     */
    private $approver;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->hash = Utils::generateToken(10);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): IndicatedDirectoriesMembers
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): IndicatedDirectoriesMembers
    {
        $this->user = $user;
        return $this;
    }

    public function getIndication(): IndicatedDirectories
    {
        return $this->indication;
    }

    public function setIndication(IndicatedDirectories $indication): IndicatedDirectoriesMembers
    {
        $this->indication = $indication;
        return $this;
    }

    public function getMandateBegin(): ?\Datetime
    {
        return $this->mandateBegin;
    }

    public function setMandateBegin(\Datetime $mandateBegin): IndicatedDirectoriesMembers
    {
        $this->mandateBegin = $mandateBegin;
        return $this;
    }

    public function getMandateEnd(): ?\Datetime
    {
        return $this->mandateEnd;
    }

    public function setMandateEnd(\Datetime $mandateEnd): IndicatedDirectoriesMembers
    {
        $this->mandateEnd = $mandateEnd;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): IndicatedDirectoriesMembers
    {
        $this->name = $name;
        return $this;
    }

    public function getCPF(): string
    {
        return $this->cpf;
    }

    public function setCPF(string $cpf): IndicatedDirectoriesMembers
    {
        $this->cpf = Utils::onlyNumbers($cpf);
        return $this;
    }

    public function getRG(): string
    {
        return $this->rg;
    }

    public function setRG(string $rg): IndicatedDirectoriesMembers
    {
        $this->rg = $rg;
        return $this;
    }

    public function getVoterTitle(): ?int
    {
        return $this->voterTitle;
    }

    public function setVoterTitle(string $voterTitle): IndicatedDirectoriesMembers
    {
        $this->voterTitle = $voterTitle;
        return $this;
    }

    public function getVoterTitleZone(): ?string
    {
        return $this->voterTitleZone;
    }

    public function setVoterTitleZone(int $voterTitleZone): IndicatedDirectoriesMembers
    {
        $this->voterTitleZone = $voterTitleZone;
        return $this;
    }

    public function getVoterTitleSection(): ?int
    {
        return $this->voterTitleSection;
    }

    public function setVoterTitleSection(int $voterTitleSection): IndicatedDirectoriesMembers
    {
        $this->voterTitleSection = $voterTitleSection;
        return $this;
    }

    public function getOffice(): int
    {
        return $this->office;
    }

    public function getOfficeString(): string
    {
        if($this->indication->getType() == 1) {
            $office = 'Líder ' . $this->office;
        } else {
            $office = match ($this->office) {
                1 => 'Presidente',
                2 => 'Vice Presidente',
                3 => 'Secretário Administrativo',
                4 => 'Secretário de Finanças',
                5 => 'Secretário de Relações Institucionais e Legal',
                default => ''
            };
        }
        return $office;
    }

    public function setOffice(int $office): IndicatedDirectoriesMembers
    {
        $this->office = $office;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusString(): string
    {
        $status = match ($this->status) {
            1 => 'Indicação',
            2 => 'Análise',
            3 => 'Aprovado',
            4 => 'Formalização',
            5 => 'Inativo',
            6 => 'Análise DE',
            7 => 'Análise DN',
            default => ''
        };
        return $status;
    }

    public function setStatus(int $status): IndicatedDirectoriesMembers
    {
        $this->status = $status;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(State $state): IndicatedDirectoriesMembers
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(City $city): IndicatedDirectoriesMembers
    {
        $this->city = $city;
        return $this;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): IndicatedDirectoriesMembers
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): IndicatedDirectoriesMembers
    {
        $this->street = $street;
        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): IndicatedDirectoriesMembers
    {
        $this->number = $number;
        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(string $district): IndicatedDirectoriesMembers
    {
        $this->district = $district;
        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(string $complement): IndicatedDirectoriesMembers
    {
        $this->complement = $complement;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): IndicatedDirectoriesMembers
    {
        $this->active = $active;
        return $this;
    }

    public function getProofOfResidence(): ?string
    {
        return $this->proofOfResidence;
    }

    public function setProofOfResidence(?string $proofOfResidence): IndicatedDirectoriesMembers
    {
        $this->proofOfResidence = $proofOfResidence;
        return $this;
    }

    public function getCnh(): ?string
    {
        return $this->cnh;
    }

    public function setCnh(?string $cnh): IndicatedDirectoriesMembers
    {
        $this->cnh = $cnh;
        return $this;
    }

    public function getRgFile(): ?string
    {
        return $this->rgFile;
    }

    public function setRgFile(?string $rgFile): IndicatedDirectoriesMembers
    {
        $this->rgFile = $rgFile;
        return $this;
    }

    public function getCpfFile(): ?string
    {
        return $this->cpfFile;
    }

    public function setCpfFile(?string $cpfFile): IndicatedDirectoriesMembers
    {
        $this->cpfFile = $cpfFile;
        return $this;
    }

    public function getQuestionnaire(): QuestionnaireReply
    {
        return $this->questionnaire;
    }

    public function setQuestionnaire(QuestionnaireReply $questionnaire): IndicatedDirectoriesMembers
    {
        $this->questionnaire = $questionnaire;
        return $this;
    }

    public function getExatoDigitalHtmlString(): string
    {
        if (!$this->exatoDigital) return 'Não realizado';
        $msg = $this->exatoDigital->getMessage();
        foreach ($this->exatoDigital->getRiskValues() as $key => $value) {
            $color = $value > 4 ? 'danger' : 'warning';
            $msg .= "<span class='badge badge-{$color}'>{$key}</span>";
        }
        if ($this->exatoDigital->getPdfUrl()) {
            $msg .= "<a class='badge badge-info' href='{$this->exatoDigital->getPdfUrl()}' target='_blank'><i class='fa fa-file-pdf'></i>Visualizar</a>";
        }
        return $msg;
    }

    public function getExatoDigital(): ExatoDigital
    {
        return $this->exatoDigital;
    }

    public function setExatoDigital(ExatoDigital $exatoDigital): IndicatedDirectoriesMembers
    {
        $this->exatoDigital = $exatoDigital;
        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): IndicatedDirectoriesMembers
    {
        $this->hash = $hash;
        return $this;
    }

    public function getQuestion1(): string
    {
        return trim($this->question1);
    }

    public function setQuestion1(string $question1): IndicatedDirectoriesMembers
    {
        $this->question1 = $question1;
        return $this;
    }

    public function getQuestion2(): string
    {
        return trim($this->question2);
    }

    public function setQuestion2(string $question2): IndicatedDirectoriesMembers
    {
        $this->question2 = $question2;
        return $this;
    }

    public function getQuestion3(): string
    {
        return trim($this->question3);
    }

    public function setQuestion3(string $question3): IndicatedDirectoriesMembers
    {
        $this->question3 = $question3;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): IndicatedDirectoriesMembers
    {
        $this->email = trim($email);
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): IndicatedDirectoriesMembers
    {
        $this->phone = $phone;
        return $this;
    }

    public function getApprovalDN(): ?int
    {
        return $this->approvalDN;
    }

    public function getApprovalDNString(): string
    {
        $approvalDN = match ($this->approvalDN) {
            1 => 'Aprovado',
            2 => 'Reprovado',
            default => ''
        };
        return $approvalDN;
    }

    public function setApprovalDN(int $approvalDN): IndicatedDirectoriesMembers
    {
        $this->approvalDN = $approvalDN;
        return $this;
    }

    public function getApprovalDE(): ?int
    {
        return $this->approvalDE;
    }

    public function getApprovalDEString(): string
    {
        $approvalDE = match ($this->approvalDE) {
            1 => 'Aprovado',
            2 => 'Reprovado',
            default => ''
        };
        return $approvalDE;
    }

    public function setApprovalDE(int $approvalDE): IndicatedDirectoriesMembers
    {
        $this->approvalDE = $approvalDE;
        return $this;
    }

    public function getApprover(): UserAdmin
    {
        return $this->approver;
    }

    public function setApprover(UserAdmin $approver): IndicatedDirectoriesMembers
    {
        $this->approver = $approver;
        return $this;
    }
}