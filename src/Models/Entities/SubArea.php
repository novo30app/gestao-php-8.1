<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="subArea")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SubAreaRepository")
 */
class SubArea
{
	/**
	 * @Id @GeneratedValue @Column(type="integer")
	 */
	private ?int $id = null;
	
	/**
	 * @ManyToOne(targetEntity="Area")
	 * @JoinColumn(name="area", referencedColumnName="id")
	 */
	private ?Area $area = null;
	
	/**
	 * @Column(type="string")
	 */
	private string $name = '';
	

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getArea(): ?Area
	{
		return $this->area;
	}

	public function setArea(?Area $area): SubArea
	{
		$this->area = $area;
		return $this;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): SubArea
	{
		$this->name = $name;
		return $this;
	}
}