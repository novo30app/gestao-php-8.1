<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicatedDirectories")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicatedDirectoriesRepository")
 */
class IndicatedDirectories
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $level;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private UserAdmin $user;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $type;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $status;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $refreshStatus;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private $city;

    /**
     * @Column(type="string")
     * @var string
     */
    private $zipCode = 0;

    /**
     * @Column(type="string")
     * @var string
     */
    private $street = '';

    /**
     * @Column(type="string")
     * @var int
     */
    private $number = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $district = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $complement = '';

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $active = true;

    /**
     * @Column(type="string")
     * @var string
     */
    private $totalArea = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $totalAreaUsed = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $iptu = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $leaseAgreement = '';

    /**
     * @Column(type="string")
     * @var string
     */
    private $newMembersAta = '';

    /**
     * @Column(type="datetime")

     * @var \DateTime
     */
    private $newMembersAtaDate;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userAnalysisNewMembersAta", referencedColumnName="id", nullable=true)
     */
    private ?UserAdmin $userAnalysisNewMembersAta = null;

    /**
     * @Column(type="string")
     * @var string
     */
    private $newMembersAtaStatus = '';

    /**
     * @Column(type="string")

     * @var string
     */
    private $rejectReason = '';

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getLevelString(): string
    {
        $level = match ($this->level) {
            1 => 'Municipal',
            2 => 'Estadual',
            default => ''
        };
        return $level;
    }

    public function setLevel(int $level): IndicatedDirectories
    {
        $this->level = $level;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): IndicatedDirectories
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): IndicatedDirectories
    {
        $this->user = $user;
        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function getTypeString(): string
    {
        $type = match ($this->type) {
            1 => 'Núcleo',
            2 => 'Comissão Provisória',
            3 => 'Diretório',
            default => '-'
        };
        return $type;
    }

    public function setType(int $type): IndicatedDirectories
    {
        $this->type = $type;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusString(): string
    {
        $status = match ($this->status) {
            1 => 'Indicação',
            2 => 'Formalização',
            3 => 'Ativo',
            4 => 'Encerrado',
            5 => 'Análise DE',
            6 => 'Análise DN',
            default => ''
        };
        return $status;
    }

    public function setStatus(int $status): IndicatedDirectories
    {
        $this->status = $status;
        return $this;
    }

    public function getRefreshStatus(): \DateTime
    {
        return $this->refreshStatus;
    }

    public function setRefreshStatus(\DateTime $refreshStatus): IndicatedDirectories
    {
        $this->refreshStatus = $refreshStatus;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): IndicatedDirectories
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): IndicatedDirectories
    {
        $this->city = $city;
        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): IndicatedDirectories
    {
        $this->zipCode = Utils::onlyNumbers($zipCode);
        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): IndicatedDirectories
    {
        $this->street = $street;
        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): IndicatedDirectories
    {
        $this->number = $number;
        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(?string $district): IndicatedDirectories
    {
        $this->district = $district;
        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): IndicatedDirectories
    {
        $this->complement = $complement;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): IndicatedDirectories
    {
        $this->active = $active;
        return $this;
    }

    public function getTotalArea(): ?string
    {
        return $this->totalArea;
    }

    public function setTotalArea(?string $totalArea): IndicatedDirectories
    {
        $this->totalArea = $totalArea;
        return $this;
    }

    public function getTotalAreaUsed(): ?string
    {
        return $this->totalAreaUsed;
    }

    public function setTotalAreaUsed(?string $totalAreaUsed): IndicatedDirectories
    {
        $this->totalAreaUsed = $totalAreaUsed;
        return $this;
    }

    public function getIptu(): ?string
    {
        $iptu = $this->iptu;
        if($iptu != "") $iptu = Utils::formatAttachment($this->iptu);
        return $iptu;
    }

    public function setIptu(?string $iptu): IndicatedDirectories
    {
        $this->iptu = $iptu;
        return $this;
    }

    public function getLeaseAgreement(): ?string
    {
        $leaseAgreement = $this->leaseAgreement;
        if($leaseAgreement != "") $leaseAgreement = Utils::formatAttachment($this->leaseAgreement);
        return $leaseAgreement;
    }

    public function setLeaseAgreement(?string $leaseAgreement): IndicatedDirectories
    {
        $this->leaseAgreement = $leaseAgreement;
        return $this;
    }

    public function getNewMembersAta(): ?string
    {
        $newMembersAta = $this->newMembersAta;
        if($newMembersAta != "") $newMembersAta = Utils::formatAttachment($this->newMembersAta);
        return $newMembersAta;
    }

    public function setNewMembersAta(?string $newMembersAta ): IndicatedDirectories
    {
        $this->newMembersAta = $newMembersAta;
        return $this;
    }

    public function getNewMembersAtaDate(): ?\DateTime
    {
        return $this->newMembersAtaDate;
    }

    public function setNewMembersAtaDate(?\DateTime $newMembersAtaDate): IndicatedDirectories      
    {
        $this->newMembersAtaDate = $newMembersAtaDate;
        return $this;
    }

    public function getUserAnalysisNewMembersAta(): ?UserAdmin
    {
       return $this->userAnalysisNewMembersAta;
    }

    public function setUserAnalysisNewMembersAta(?UserAdmin $userAnalysisNewMembersAta): IndicatedDirectories
    {
        $this->userAnalysisNewMembersAta = $userAnalysisNewMembersAta;
        return $this;
    }

    public function getNewMembersAtaStatus(): ?string
    {
        return $this->newMembersAtaStatus;
    }

    public function setNewMembersAtaStatus(?string $newMembersAtaStatus): IndicatedDirectories
    {
        $this->newMembersAtaStatus = $newMembersAtaStatus;
        return $this;
    }

    public function getRejectReason(): ?string
    {
        return $this->rejectReason;
    }

    public function setRejectReason(?string $rejectReason): IndicatedDirectories
    {
        $this->rejectReason = $rejectReason;
        return $this;
    }
}