<?php


namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @Entity @Table(name="leaders",
 *  indexes={@Index(name="leaders_cpf", columns={"cpf"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\LeadersRepository")
 */
class Leaders
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="date", nullable=true)
     */
    private ?\DateTime $startDate;

    /**
     * @Column(type="date", nullable=true)
     */
    private ?\DateTime $endDate;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @Column(type="string")
     */
    private string $email;

    /**
     * @Column(type="string", length=11, options={"fixed"=true})
     */
    private string $cpf;

    /**
     * @ManyToOne(targetEntity="Occupation")
     * @JoinColumn(name="occupation", referencedColumnName="id")
     */
    private Occupation $occupation;

    /**
     * @ManyToOne(targetEntity="AccessLevel")
     * @JoinColumn(name="level", referencedColumnName="id")
     */
    private AccessLevel $level;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $status;

    public function getId(): int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    public function setStartDate($startDate): Leaders
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    public function setEndDate($endDate): Leaders
    {
        $this->endDate = $endDate;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): Leaders
    {
        $this->name = $name;
        return $this;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf($cpf): Leaders
    {
        $this->cpf = Utils::onlyNumbers($cpf);
        return $this;
    }

    public function getOccupation(): Occupation
    {
        return $this->occupation;
    }

    public function setOccupation($occupation): Leaders
    {
        $this->occupation = $occupation;
        return $this;
    }

    public function getLevel(): AccessLevel
    {
        return $this->level;
    }

    public function setLevel($level): Leaders
    {
        $this->level = $level;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): Leaders
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): Leaders
    {
        $this->state = $state;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Leaders
    {
        $this->email = $email;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusString(): string
    {
        $status = match ($this->status) {
            1 => 'Permanente',
            2 => 'Provisório',
            default => ''
        };
        return $status;
    }

    public function setStatus(int $status): Leaders
    {
        $this->status = $status;
        return $this;
    }
}