<?php

namespace App\Models\Entities;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="autonomous")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AutonomousRepository")
 */
class Autonomous
{
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Provider")
     * @JoinColumn(name="provider_id", referencedColumnName="id")
     */
    private Provider $provider;

    /**
     * @Column(name="pis_pasep", type="string", length=20, nullable=true)
     */
    private ?string $pisPasep = null;

    /**
     * @ManyToOne(targetEntity="CivilState")
     * @JoinColumn(name="civil_status", referencedColumnName="id")
     */
    private ?CivilState $civilStatus = null;

    /**
     * @ManyToOne(targetEntity="Schooling")
     * @JoinColumn(name="instruction_level", referencedColumnName="id")
     */
    private ?Schooling $instructionLevel = null;

    /**
     * @Column(name="occupation", type="string", length=100, nullable=true)
     */
    private ?string $occupation = null;

    /**
     * @Column(name="attachment_residence_autonomous", type="string", length=255, nullable=true)
     */
    private ?string $attachmentResidenceAutonomous = null;

    /**
     * @Column(name="attachment_rg_autonomous", type="string", length=255, nullable=true)
     */
    private ?string $attachmentRgCnhAutonomous = null;

    /**
     * @Column(name="created_at", type="datetime")
     */
    private DateTime $createdAt;

    /**
     * @Column(name="updated_at", type="datetime", nullable=true)
     */
    private ?DateTime $updatedAt = null;

    public function __construct()
    {
        $this->updatedAt = new DateTime();
    }

    private function isValidAttachment(string $attachment): bool
    {
        $allowedExtensions = ['pdf', 'jpg', 'jpeg', 'png'];
        $extension = strtolower(pathinfo($attachment, PATHINFO_EXTENSION));
        return in_array($extension, $allowedExtensions);
    }

    // Getters
    public function getId(): int
    {
        return $this->id;
    }

    public function getProvider(): Provider
    {
        return $this->provider;
    }

    public function getPisPasep(): ?string
    {
        return $this->pisPasep;
    }

    public function getCivilStatus(): ?CivilState
    {
        return $this->civilStatus;
    }

    public function getInstructionLevel(): ?Schooling
    {
        return $this->instructionLevel;
    }

    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    public function getAttachmentResidenceAutonomous(): ?string
    {
        return $this->attachmentResidenceAutonomous;
    }

    public function getAttachmentRgCnhAutonomous(): ?string
    {
        return $this->attachmentRgCnhAutonomous;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getupdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    // Setters

    public function setProvider(Provider $provider): Autonomous
    {
        $this->provider = $provider;
        return $this;
    }

    public function setPisPasep(?string $pisPasep): Autonomous
    {
        $this->pisPasep = $pisPasep;
        return $this;
    }

    public function setCivilStatus(?CivilState $civilStatus): Autonomous
    {
        $this->civilStatus = $civilStatus;
        return $this;
    }

    public function setInstructionLevel(?Schooling $instructionLevel): Autonomous
    {
        $this->instructionLevel = $instructionLevel;
        return $this;
    }

    public function setOccupation(?string $occupation): Autonomous
    {
        $this->occupation = $occupation;
        return $this;
    }
    public function setAttachmentResidenceAutonomous(?string $attachmentResidenceAutonomous): Autonomous
    {
        if($attachmentResidenceAutonomous !== null && !$this->isValidAttachment($attachmentResidenceAutonomous)) {
            throw new \InvalidArgumentException("Invalid attachment format. Only PDF, JPG, JPEG, and PNG are allowed.");
        }
        $this->attachmentResidenceAutonomous = $attachmentResidenceAutonomous;
        return $this;
    }

    public function setAttachmentRgCnhAutonomous(?string $attachmentRgCnhAutonomous): Autonomous
    {
        if($attachmentRgCnhAutonomous !== null && !$this->isValidAttachment($attachmentRgCnhAutonomous)) {
            throw new \InvalidArgumentException("Invalid attachment format. Only PDF, JPG, JPEG, and PNG are allowed.");
        }
        $this->attachmentRgCnhAutonomous = $attachmentRgCnhAutonomous;
        return $this;
    }

    public function setCreatedAt(): Autonomous
    {
        $this->createdAt = new DateTime();
        return $this;
    }

    public function setUpdatedAt(): Autonomous
    {
        $this->updatedAt = new DateTime();
        return $this;
    }
}