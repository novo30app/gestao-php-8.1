<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="affiliateEvolution",
 *  indexes={@Index(name="affiliateEvolution_period", columns={"period"}),
 *          @Index(name="affiliateEvolution_state", columns={"state"}),
 *          @Index(name="affiliateEvolution_city", columns={"city"})
 *  }
 * )
 * @ORM @Entity(repositoryClass="App\Models\Repository\AffiliateEvolutionRepository")
 */
class AffiliateEvolution
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $period = '';

    /**
     * @Column(type="integer")
     */
    private int $state = 0;

    /**
     * @Column(type="integer")
     */
    private int $city = 0;

    /**
     * @Column(type="integer")
     */
    private int $total = 0;

    /**
     * @Column(type="integer")
     */
    private int $affiliations = 0;

    /**
     * @Column(type="integer")
     */
    private int $disaffiliation = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriod(): string
    {
        return $this->period;
    }

    public function setPeriod(string $period): AffiliateEvolution
    {
        $this->period = $period;
        return $this;
    }

    public function getState(): int
    {
        return $this->state;
    }

    public function setState(int $state): AffiliateEvolution
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): int
    {
        return $this->city;
    }

    public function setCity(int $city): AffiliateEvolution
    {
        $this->city = $city;
        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): AffiliateEvolution
    {
        $this->total = $total;
        return $this;
    }

    public function getAffiliations(): int
    {
        return $this->affiliations;
    }

    public function setAffiliations(int $affiliations): AffiliateEvolution
    {
        $this->affiliations = $affiliations;
        return $this;
    }

    public function getDisaffiliation(): int
    {
        return $this->disaffiliation;
    }

    public function setDisaffiliation(int $disaffiliation): AffiliateEvolution
    {
        $this->disaffiliation = $disaffiliation;
        return $this;
    }

}