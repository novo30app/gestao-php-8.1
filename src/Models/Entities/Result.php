<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="results")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ResultRepository")
 */
class Result
{
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="CandidateData")
     * @JoinColumn(name="candidate", referencedColumnName="id")
     */
    private CandidateData $candidate;

    /**
     * @ManyToOne(targetEntity="Zone")
     * @JoinColumn(name="zone", referencedColumnName="id")
     */
    private Zone $zone;

    /**
     * @Column(type="integer")
     */
    private int $votes = 0;

    /**
     * @Column(type="integer")
     */
    private int $turn = 0;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): CandidateData
    {
        return $this->candidate;
    }

    public function setCandidate(CandidateData $candidate): Result
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getZone(): Zone
    {
        return $this->zone;
    }

    public function setZone(Zone $zone): Result
    {
        $this->zone = $zone;
        return $this;
    }

    public function getVotes(): int
    {
        return $this->votes;
    }

    public function setVotes(int $votes): Result
    {
        $this->votes = $votes;
        return $this;
    }

    public function getTurn(): int
    {
        return $this->turn;
    }

    public function setTurn(int $turn): Result
    {
        $this->turn = $turn;
        return $this;
    }
}
