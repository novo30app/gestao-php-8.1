<?php

namespace App\Models\Entities;

use App\Helpers\Utils;

/**
 * @Entity @Table(name="contract")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ContractRepository")
 */
class Contract
{

    public const SITUATION_VIGENTE_PRAZO_DETERMINADO = 1;
    public const SITUATION_VIGENTE_PRAZO_INDETERMINADO = 2;
    public const SITUATION_EXPIRADO = 3;


    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $cpf = '';

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @ManyToOne(targetEntity="Contract")
     * @JoinColumn(name="contract", referencedColumnName="id", nullable=true)
     */
    private ?Contract $contract = null;

    /**
     * @ManyToOne(targetEntity="CostCenter")
     * @JoinColumn(name="costCenter", referencedColumnName="id")
     */
    private CostCenter $costCenter;

    /**
     * @Column(type="float", nullable=true)
     */
    private ?float $value = null;

    /**
     * @Column(type="float", nullable=true)
     */
    private ?float $monthlyValue = null;

    /**
     * @Column(type="text")
     */
    private string $doc = '';

    /**
     * @Column(type="integer")
     */
    private int $stage;

    /**
     * @Column(type="integer", options={"default"="3"})
     */
    private int $category;

    /**
     * @Column(type="integer")
     */
    private int $situation;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $start;

    /**
     * @Column(type="datetime", nullable = true)
     */
    private ?\DateTime $end = null;

    /**
     * @Column(type="boolean")
     */
    private bool $status;

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $number = '';

    /**
     * @Column(type="text")
     */
    private string $object = '';

    /**
     * @Column(type="datetime", nullable = true)
     */
    private ?\DateTime $dateCancel = null;

    /**
     * @Column(type="datetime", nullable = true)
     */
    private ?\DateTime $signatureDate = null;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userCancel", referencedColumnName="id", nullable=true)
     */
    private ?UserAdmin $userCancel = null;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=true)
     */
    private Directory $directory;

    /**
     * @ManyToOne(targetEntity="ChartOfAccounts")
     * @JoinColumn(name="account", referencedColumnName="id")
     */
    private ?ChartOfAccounts $account;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): Contract
    {
        $this->cpf = Utils::onlyNumbers($cpf);
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Contract
    {
        $this->name = strtoupper($name);
        return $this;
    }

    public function getCostCenter(): CostCenter
    {
        return $this->costCenter;
    }

    public function setCostCenter(CostCenter $costCenter): Contract
    {
        $this->costCenter = $costCenter;
        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): Contract
    {
        $this->value = $value;
        return $this;
    }

    public function getDoc(): string
    {
        $doc = $this->doc;
        if ($doc) $doc = Utils::formatAttachment($this->doc);
        return $doc;
    }

    public function setDoc(string $doc): Contract
    {
        $this->doc = $doc;
        return $this;
    }

    public function getStage(): int
    {
        return $this->stage;
    }

    public function getStageStr(): string
    {
        switch ($this->stage) {
            case 1:
                return 'Criado';
            case 2:
                return 'Colhendo Assinatura';
            case 3:
                return 'Assinado';
            default:
                return 'Desconhecido';
        }
    }

    public function setStage(int $stage): Contract
    {
        $this->stage = $stage;
        return $this;
    }

    public function getSituation(): int
    {
        return $this->situation;
    }

    public function getSituationStr(): string
    {
        switch ($this->situation) {
            case 1:
                return 'Vigente - Prazo Determinado';
            case 2:
                return 'Vigente - Prazo Indeterminado';
            case 3:
                return 'Expirado';
            default:
                return 'Desconhecido';
        }
    }

    public function setSituation(int $situation): Contract
    {
        $this->situation = $situation;
        return $this;
    }

    public function getStart(): \DateTime
    {
        return $this->start;
    }

    public function setStart(\DateTime $start): Contract
    {
        $this->start = $start;
        return $this;
    }

    public function getEnd(): ?\DateTime
    {
        return $this->end;
    }

    public function setEnd(?\DateTime $end): Contract
    {
        $this->end = $end;
        return $this;
    }

    public function isStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): Contract
    {
        $this->status = $status;
        return $this;
    }

    public function getMonthlyValue(): ?float
    {
        return $this->monthlyValue;
    }

    public function setMonthlyValue(?float $monthlyValue): Contract
    {
        $this->monthlyValue = $monthlyValue;
        return $this;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function getCategoryStr(): string
    {
        switch ($this->category) {
            case 1:
                return 'Recorrente - Prazo indeterminado';
            case 2:
                return 'Recorrente - Prazo determinado';
            case 3:
                return 'Pontual - Prazo indeterminado';
            case 4:
                return 'Pontual - Prazo determinado';
            default:
                return 'Desconhecido';
        }
    }

    public function setCategory(int $category): Contract
    {
        $this->category = $category;
        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): Contract
    {
        $this->number = $number;
        return $this;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function setObject(string $object): Contract
    {
        $this->object = $object;
        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): Contract
    {
        $this->contract = $contract;
        return $this;
    }

    public function getDateCancel(): ?\DateTime
    {
        return $this->dateCancel;
    }

    public function setDateCancel(?\DateTime $dateCancel): Contract
    {
        $this->dateCancel = $dateCancel;
        return $this;
    }

    public function getUserCancel(): ?UserAdmin
    {
        return $this->userCancel;
    }

    public function setUserCancel(?UserAdmin $userCancel): Contract
    {
        $this->userCancel = $userCancel;
        return $this;
    }

    public function getSignatureDate(): ?\DateTime
    {
        return $this->signatureDate;
    }

    public function setSignatureDate(?\DateTime $signatureDate): Contract
    {
        $this->signatureDate = $signatureDate;
        return $this;
    }

    public function getDirectory(): Directory
    {
        return $this->directory;
    }

    public function setDirectory(Directory $directory): Contract
    {
        $this->directory = $directory;
        return $this;
    }

    public function getAccount(): ?ChartOfAccounts
    {
        return $this->account;
    }

    public function setAccount(ChartOfAccounts $account): Contract
    {
        $this->account = $account;
        return $this;
    }
}