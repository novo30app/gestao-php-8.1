<?php
namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * TbEventosPerguntas
 *
 * @Table(name="tb_eventos_perguntas")
 * @Entity(repositoryClass="App\Models\Repository\EventsQuestionsRepository")
 */
class EventsQuestions
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var string|null
     *
     * @Column(name="tipo", type="string", length=0, nullable=true, options={"default"="texto"})
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @Column(name="descricao", type="string", length=255, nullable=true)
     */
    private $descricao;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=0, nullable=true, options={"default"="ativo"})
     */
    private $status = 'ativo';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTbEventId(): ?string
    {
        return $this->tbEventosId;
    }

    public function setTbEventId(?string $tbEventosId): EventsQuestions
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->tipo;
    }

    public function setType(?string $tipo): EventsQuestions
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->descricao;
    }

    public function setDescription(?string $descricao): EventsQuestions
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): EventsQuestions
    {
        $this->status = $status;
        return $this;
    }


}
