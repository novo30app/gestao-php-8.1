<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicationOfAffiliationSms")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicationOfAffiliationSmsRepository")
 */
class IndicationOfAffiliationSms
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @ManyToOne(targetEntity="IndicationOfAffiliation")
     * @JoinColumn(name="indication", referencedColumnName="id", nullable=true)
     */
    private $indication;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phone;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): indicationOfAffiliationSms
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): indicationOfAffiliationSms
    {
        $this->user = $user;
        return $this;
    }


    public function getIndication(): indicationOfAffiliation
    {
        return $this->indication;
    }

    public function setIndication(IndicationOfAffiliation $indication): indicationOfAffiliationSms
    {
        $this->indication = $indication;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): indicationOfAffiliationSms
    {
        $this->phone = $phone;
        return $this;
    }
}