<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="sectors")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SectorsRepository")
 */
class Sectors
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): Sectors
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): Sectors
    {
        $this->user = $user;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Sectors
    {
        $this->name = $name;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Sectors
    {
        $this->active = $active;
        return $this;
    }
}