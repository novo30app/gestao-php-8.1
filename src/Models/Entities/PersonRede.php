<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaRede
 *
 * @Table(name="tb_pessoa_rede", indexes={@Index(name="idx_pessoa", columns={"tb_pessoa_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\PersonRedeRepository")
 */
class PersonRede
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $user;

    /**
     * @var string|null
     *
     * @Column(name="customer_id", type="string", length=45, nullable=true)
     */
    private $customerId;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao;

    public function __construct()
    {
        $this->dataCriacao = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user;
    }

    public function setUser(User $user): PersonRede
    {
        $this->user = $user->getId();
        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): PersonRede
    {
        $this->customerId = $customerId;
        return $this;
    }




}
