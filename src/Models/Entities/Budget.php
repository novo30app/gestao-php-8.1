<?php

namespace App\Models\Entities;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity @Table(name="budgets")
 * @ORM @Entity(repositoryClass="App\Models\Repository\BudgetRepository")
 */
class Budget
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="CostCenter")
     * @JoinColumn(name="costCenter", referencedColumnName="id", nullable=true)
     */
    private ?CostCenter $costCenter = null;

    /**
     * @Column(type="integer", nullable=true)
     */
    private int $year = 0;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private bool $status = true;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private ?\DateTime $created = null;

    /**
     * @Column(type="float", nullable=true)
     */
    private ?float $total = 0;

    /**
     * @OneToMany(targetEntity="BudgetValues", mappedBy="budget")
     */
    private Collection $values;

    /**
     * @ManyToOne(targetEntity="Area")
     * @JoinColumn(name="area", referencedColumnName="id", nullable=true)
     */
    private ?Area $area = null;

    /**
     * @ManyToOne(targetEntity="SubArea")
     * @JoinColumn(name="subArea", referencedColumnName="id", nullable=true)
     */
    private ?SubArea $subArea = null;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->values = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCostCenter(): ?CostCenter
    {
        return $this->costCenter;
    }

    public function setCostCenter(?CostCenter $costCenter): Budget
    {
        $this->costCenter = $costCenter;
        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): Budget
    {
        $this->year = $year;
        return $this;
    }

    public function isStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): Budget
    {
        $this->status = $status;
        return $this;
    }

    public function changeStatus(): Budget
    {
        $new = !$this->status;
        $this->status = $new;
        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): Budget
    {
        $this->total = $total;
        return $this;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    public function getValues(): Collection
    {
        return $this->values;
    }

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): Budget
    {
        $this->area = $area;
        return $this;
    }

    public function getSubArea(): ?SubArea
    {
        return $this->subArea;
    }

    public function setSubArea(?SubArea $subArea): Budget
    {
        $this->subArea = $subArea;
        return $this;
    }
}
