<?php
namespace App\Models\Entities;

use App\Helpers\Session;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * TbAdminAcesso
 *
 * @Entity @Table(name="tb_admin_acesso")
 * @ORM @Entity(repositoryClass="App\Models\Repository\AdminAccessRepository")
 */
class AdminAccess
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="item_id", type="integer", nullable=true)
     */
    private $itemId;

    /**
     * @var string|null
     *
     * @Column(name="tipo_relacao", type="string", length=0, nullable=true)
     */
    private $tipoRelacao;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="item_id", referencedColumnName="id", nullable=false)
     * @var Directory
     */
    private $DirectoryName;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="item_id", referencedColumnName="id", nullable=false)
     * @var State
     */
    private $StateName;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="item_id", referencedColumnName="id", nullable=false)
     * @var City
     */
    private $CityName;

    public function getDirectory(): ?Directory
    {
        return $this->DirectoryName;
    }

    public function getState(): ?State
    {
        return $this->StateName;
    }

    public function getCity(): ?City
    {
        return $this->CityName;
    }

    public function getItemId(): ?int
    {
        return $this->itemId;
    }

}
