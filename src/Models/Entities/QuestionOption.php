<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="questionOptions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\QuestionOptionsRepository")
 */
class QuestionOption
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @ManyToOne(targetEntity="Question", inversedBy="options")
     * @JoinColumn(name="question", referencedColumnName="id")
     */
    private Question $question;

    /**
     * @Column(type="float")
     */
    private float $points;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNameHtmlResult(): string
    {
        $color = 'danger';
        if ($this->points == 2) $color = 'success';
        elseif ($this->points == 1) $color = 'warning';
        return "<span class='text-{$color}'>{$this->name}</span>";
    }


    public function setName(string $name): QuestionOption
    {
        $this->name = trim($name);
        return $this;
    }

    public function getQuestion(): Question
    {
        return $this->question;
    }

    public function setQuestion(Question $question): QuestionOption
    {
        $this->question = $question;
        return $this;
    }

    public function getPoints(): float
    {
        return $this->points;
    }

    public function setPoints(float $points): QuestionOption
    {
        $this->points = $points;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): QuestionOption
    {
        $this->active = $active;
        return $this;
    }
}