<?php

namespace App\Models\Entities;
/**
 * @Entity @Table(name="interactionMeetingSuggestions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\InteractionMeetingSuggestionRepository")
 */
class InteractionMeetingSuggestion
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="Interaction")
     * @JoinColumn(name="interaction", referencedColumnName="id")
     */
    private Interaction $interaction;

    /**
     * @Column(type="string")
     */
    private string $suggestion = '';

    /**
     * @Column(type="string")
     */
    private string $responsible = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInteraction(): Interaction
    {
        return $this->interaction;
    }

    public function setInteraction(Interaction $interaction): InteractionMeetingSuggestion
    {
        $this->interaction = $interaction;
        return $this;
    }

    public function getSuggestion(): string
    {
        return $this->suggestion;
    }

    public function setSuggestion(string $suggestion): InteractionMeetingSuggestion
    {
        $this->suggestion = $suggestion;
        return $this;
    }

    public function getResponsible(): string
    {
        return $this->responsible;
    }

    public function setResponsible(string $responsible): InteractionMeetingSuggestion
    {
        $this->responsible = $responsible;
        return $this;
    }

}