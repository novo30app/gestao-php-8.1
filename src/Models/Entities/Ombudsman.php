<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="ombudsmans")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OmbudsmanRepository")
 */
class Ombudsman
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="requester", referencedColumnName="id")
     */
    private User $requester;


    /**
     * @ManyToOne(targetEntity="OmbudsmanSubject")
     * @JoinColumn(name="ombudsmanSubject", referencedColumnName="id")
     */
    private OmbudsmanSubject $ombudsmanSubject;


    /**
     * @ManyToOne(targetEntity="OmbudsmanCategory")
     * @JoinColumn(name="destiny", referencedColumnName="id")
     */
    private OmbudsmanCategory $destiny;


    /**
     * @Column(type="integer")
     */
    private int $status = 0;

    /**
     * @Column(type="text")
     */
    private string $description = '';

    /**
     * @Column(type="string")
     */
    private string $attachment = '';

    /**
     * @Column(type="boolean")
     */
    private bool $anonymous = false;


    public function __construct()
    {
        $this->created = new \DateTime();
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getRequester(): User
    {
        return $this->requester;
    }

    public function setRequester(User $requester): Ombudsman
    {
        $this->requester = $requester;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): Ombudsman
    {
        $this->status = $status;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Ombudsman
    {
        $this->description = $description;
        return $this;
    }

    public function isAnonymous(): bool
    {
        return $this->anonymous;
    }

    public function setAnonymous(bool $anonymous): Ombudsman
    {
        $this->anonymous = $anonymous;
        return $this;
    }

    public function getOmbudsmanSubject(): OmbudsmanSubject
    {
        return $this->ombudsmanSubject;
    }

    public function setOmbudsmanSubject(OmbudsmanSubject $ombudsmanSubject): Ombudsman
    {
        $this->ombudsmanSubject = $ombudsmanSubject;
        return $this;
    }

    public function getAttachment(): string
    {
        if ($this->attachment) {
            $attachment = explode("espaco-novo.novo.org.br/", $this->attachment);
            return "https://espaco-novo.novo.org.br/{$attachment[1]}";
        }
        return $this->attachment;
    }

    public function setAttachment(string $attachment): Ombudsman
    {
        $this->attachment = $attachment;
        return $this;
    }

    public function getStatusString(): string
    {
        return $this->status == 1 ? 'Respondido' : 'Em análise';
    }

    public function getDestiny(): OmbudsmanCategory
    {
        return $this->destiny;
    }

    public function setDestiny(OmbudsmanCategory $destiny): Ombudsman
    {
        $this->destiny = $destiny;
        return $this;
    }

}