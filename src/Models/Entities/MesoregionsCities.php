<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="mesoregionsCities")
 * @ORM @Entity(repositoryClass="App\Models\Repository\MesoregionsCitiesRepository")
 */
class MesoregionsCities
{
    /**
     * @var int
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Mesoregions")
     * @JoinColumn(name="mesoregion", referencedColumnName="id")
     */
    private $mesoregion;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     */
    private $city;

    public function getId(): int
    {
        return $this->id;
    }

    public function getMesoregion(): Mesoregions
    {
        return $this->mesoregion;
    }

    public function setMesoregion(Mesoregions $mesoregion): MesoregionsCities
    {
        $this->mesoregion = $mesoregion;
        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): MesoregionsCities
    {
        $this->city = $city;
        return $this;
    }
}