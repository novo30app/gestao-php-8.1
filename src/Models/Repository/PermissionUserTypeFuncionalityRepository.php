<?php

namespace App\Models\Repository;

use App\Models\Entities\PermissionUserTypeFuncionality;
use Doctrine\ORM\EntityRepository;

class PermissionUserTypeFuncionalityRepository extends EntityRepository
{
    public function save(PermissionUserTypeFuncionality $entity): PermissionUserTypeFuncionality
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($userType = null, $sector = null, &$params): string
    {
        $where = '';
        if ($userType) {
            $params[':userType'] = "$userType";
            $where .= " AND p.userType = :userType";
        }
        if ($sector) {
            $params[':sector'] = "$sector";
            $where .= " AND p.sector = :sector";
        }
        return $where;
    }

    public function list($userType = null, $sector = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($userType, $sector, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, DATE_FORMAT(p.created, '%d/%m/%Y %H:%m:%s') AS created, p.user, p.userType, u.name AS userTypeName,
                    p.sector, s.name AS sectorName, p.active
                FROM permissionUserTypeFuncionality p
                LEFT JOIN usersTypes u ON u.id = p.userType
                LEFT JOIN sectors s ON s.id = p.sector
                WHERE p.active = 1 {$where}
                GROUP BY p.userType, p.sector {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($userType = null, $sector = null): int
    {
        $params = [];
        $where = $this->generateWhere($userType, $sector, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS total
                FROM (
                    SELECT DISTINCT p.userType, p.sector
                    FROM permissionUserTypeFuncionality p
                    WHERE p.active = 1 {$where}
                ) AS unique_combinations";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative()['total'] ?? 0;
    }

    public function featuresByPermission(PermissionUserTypeFuncionality $permission): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT id, systemFeature
                FROM permissionUserTypeFuncionality
                WHERE active = 1 AND userType = :userType AND sector = :sector";
        $rows = $pdo->prepare($sql)->execute(
            [':userType' => $permission->getUserType()->getId(), ':sector' => $permission->getSector()->getId()]);
        return $rows->fetchAllAssociative();
    }

    public function inactiveByUserTypeAndSector(int $userType, string $sector): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "UPDATE permissionUserTypeFuncionality SET active = 0 WHERE userType = :userType AND sector = :sector";
        $sth = $pdo->prepare($sql);
        $sth->execute([':userType' => $userType, ':sector' => $sector]);
    }


}