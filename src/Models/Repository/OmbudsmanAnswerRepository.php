<?php

namespace App\Models\Repository;

use App\Models\Entities\OmbudsmanAnswer;
use Doctrine\ORM\EntityRepository;

class OmbudsmanAnswerRepository extends EntityRepository
{
    public function save(OmbudsmanAnswer $entity) {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}