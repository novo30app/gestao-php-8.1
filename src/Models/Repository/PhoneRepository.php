<?php

namespace App\Models\Repository;

use App\Models\Entities\Phone;
use Doctrine\ORM\EntityRepository;

class PhoneRepository extends EntityRepository
{
    public function save(Phone $entity): Phone
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}