<?php

namespace App\Models\Repository;

use App\Models\Entities\CostCenter;
use App\Models\Entities\Directory;
use Doctrine\ORM\EntityRepository;

class CostCenterRepository extends EntityRepository
{
	public function save(CostCenter $entity):CostCenter
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}

	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}

	private function generateWhere(array $filter, &$params): string
	{
		$where = '';
		if ($filter['id']) {
			$params[':id'] = $filter['id'];
			$where .= " AND costCenter.id = :id";
		}
		if ($filter['name']) {
			$name = $filter['name'];
			$params[':name'] = "%$name%";
			$where .= " AND costCenter.name LIKE :name";
		}
		if ($filter['responsible']) {
			$params[':responsible'] = $filter['responsible'];
			$where .= " AND costCenter.responsible = :responsible";
		}
        if ($filter['directory']) {
            $directory = $this->getEntityManager()->getRepository(Directory::class)->findOneBy(['nome' => $filter['directory']]);
            $params[':directory'] = $directory->getId();
            $where .= " AND costCenter.directory = :directory";
        }
		return $where;
	}

	public function list(array $filter, $limit = null, $offset = null): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT costCenter.id, costCenter.name, costCenter.responsible, usersAdmin.name AS userName, costCenter.value,
       			costCenter.directory, costCenter.area, costCenter.subArea, area.name AS nameArea, tb_diretorio.nome AS directoryName
                FROM costCenter
                LEFT JOIN  usersAdmin ON usersAdmin.id = costCenter.responsible
                LEFT JOIN  tb_diretorio ON tb_diretorio.id = costCenter.directory
                LEFT JOIN  area ON area.id = costCenter.area
                WHERE costCenter.status = 1 {$where}
				ORDER BY name ASC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}

	public function listTotal(array $filter): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(costCenter.id) AS total
                FROM costCenter
                WHERE costCenter.status = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}

}
