<?php

namespace App\Models\Repository;

use App\Models\Entities\Representatives;
use Doctrine\ORM\EntityRepository;

class RepresentativesRepository extends EntityRepository
{
    public function save(Representatives $entity): Representatives
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}