<?php

namespace App\Models\Repository;

use App\Models\Entities\Question;
use Doctrine\ORM\EntityRepository;


class QuestionRepository extends EntityRepository
{
    public function save(Question $entity): Question
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateWhere(array $filter, &$params)
    {
        $where = '';
        if ($filter['responsible']) {
            $params[':responsible'] = $filter['responsible'];
            $where .= " AND q.responsible = :responsible";
        }
        if ($filter['active'] || $filter['active'] === '0') {
            $params[':active'] = $filter['active'];
            $where .= " AND q.active = :active";
        }
        return $where;
    }

    public function list(array $filter, $limit, $offset)
    {
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT q.text, q.active, q.id, u.name AS userName
				FROM  App\Models\Entities\Question as q				
				JOIN q.responsible AS u
                WHERE 1 = 1 {$where}
                ORDER BY q.id DESC
                ")
            ->setMaxResults($limit)
            ->setFirstResult($offset * $limit)
            ->execute($params);
    }

    public function listTotal(array $filter)
    {
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(q) AS total
				FROM  App\Models\Entities\Question as q
                WHERE 1 = 1 {$where}
                ")
            ->execute($params)[0]['total'];
    }


    public function form()
    {
        return $this->getEntityManager()->createQuery(
            "SELECT q 
				FROM  App\Models\Entities\Question as q
                WHERE 1 = 1 AND q.active = 1 ORDER BY rand()")
            ->execute();
    }


}