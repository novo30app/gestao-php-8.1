<?php

namespace App\Models\Repository;

use App\Models\Entities\CandidatePost;
use Doctrine\ORM\EntityRepository;

class CandidatePostRepository extends EntityRepository
{
    public function save(CandidatePost $entity):CandidatePost
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}