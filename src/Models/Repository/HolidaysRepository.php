<?php

namespace App\Models\Repository;

use App\Models\Entities\Holidays;
use Doctrine\ORM\EntityRepository;

class HolidaysRepository extends EntityRepository
{
    public function save(Holidays $entity):Holidays
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}