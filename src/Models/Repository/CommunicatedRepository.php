<?php

namespace App\Models\Repository;

use App\Models\Entities\Communicated;
use Doctrine\ORM\EntityRepository;

class CommunicatedRepository extends EntityRepository
{
    public function save(Communicated $entity):Communicated
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere( $date = null, $title = null, $file = null, &$params)
    {
        $where = '';
        if($date) {
            $params[':date'] = "%$date%";
            $where .= " AND tc.data LIKE :date";
        }
        if($title) {
            $params[':title'] = "%$title%";
            $where .= " AND tc.titulo LIKE :title";
        }
        if($file) {
            $params[':file'] = "%$file%";
            $where .= " AND tc.arquivo LIKE :file";
        }
        return $where;
    }

    public function list( $date = null, $title = null, $file = null, $limit = null, $offset = null ): array
    {
        $params = [];
        $where = $this->generateWhere( $date, $title, $file, $params );
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tc.id, DATE_FORMAT(tc.data, '%d/%m/%Y') as date, tc.processo as process, tc.titulo as title, if(tc.link = 1, tc.link_comunicado, tc.arquivo) as file, 
                tc.restrito as restricted, tc.exibir as communicated, tc.comite as decision, tc.comite_consultas as consultation, tc.resolucao as resolution,
                tc.diretorio as directory, tc.link, tc.link_comunicado as linkCommunicated
                FROM tb_conteudo AS tc
                WHERE tc.ativo = 1 AND tc.titulo IS NOT NULL {$where}
                GROUP BY tc.id
                ORDER BY tc.data DESC {$limitSql};";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal( $date = null, $title = null, $file = null ): array
    {
        $params = [];
        $where = $this->generateWhere( $date, $title, $file, $params );
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tc.id)) AS total 
                FROM tb_conteudo AS tc
                WHERE tc.ativo = 1 AND tc.titulo IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}