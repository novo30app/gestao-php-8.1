<?php

namespace App\Models\Repository;

use App\Models\Entities\QuestionnaireReply;
use Doctrine\ORM\EntityRepository;

class QuestionnaireReplyRepository extends EntityRepository
{
    public function save(QuestionnaireReply $entity): QuestionnaireReply
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}