<?php

namespace App\Models\Repository;

use App\Models\Entities\TrailsOfKnowledge;
use Doctrine\ORM\EntityRepository;

class TrailsOfKnowledgeRepository extends EntityRepository
{
    public function save(TrailsOfKnowledge $entity):TrailsOfKnowledge
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($keyWord = null, $modules = null, $classes = null, &$params): string
    {
        $where = '';
        if ($keyWord) {
            $params[':keyWord'] = "%$keyWord%";
            $where .= " AND t.title LIKE :keyWord";
        }
        if ($modules) {
            $params[':modules'] = "$modules";
            $where .= " AND t.module = :modules";
        }
        if ($classes) {
            $params[':classes'] = "$classes";
            $where .= " AND t.id LIKE :classes";
        }
        $params[':active'] = "1";
        $where .= " AND t.active = :active";
        return $where;
    }

    public function list($keyWord = null, $modules = null, $classes = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($keyWord, $modules, $classes, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT t.id, t.title, t.link, t.description, t.material,
                (CASE t.module
                    WHEN 1 THEN 'Introdução'
                    WHEN 2 THEN 'Consultas'
                    WHEN 3 THEN 'Eventos'
                    WHEN 4 THEN 'Entendendo o Dashboard'
                    WHEN 5 THEN 'Financeiro'
                    WHEN 6 THEN 'Comunicação'
                    WHEN 7 THEN 'Acesso à Novos Usuários'
                    WHEN 8 THEN 'Jurídico Diretórios'
                    WHEN 9 THEN 'Prestação de Contas Diretórios'
                    WHEN 10 THEN 'Estratégias de captação de recursos'
                    WHEN 11 THEN 'Estratégias do Dia-a-dia'
                END) AS module
                FROM trailsOfKnowledge t
                WHERE 1 = 1 {$where} GROUP BY t.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($keyWord = null, $modules = null, $classes = null): array
    {
        $params = [];
        $where = $this->generateWhere($keyWord, $modules, $classes, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(t.id)) AS total 
                FROM trailsOfKnowledge t
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}