<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\YouthSearch;
use Doctrine\ORM\EntityRepository;

class YouthSearchRepository extends EntityRepository
{
    public function save(YouthSearch $entity): YouthSearch
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateAccess(UserAdmin $user)
    {
        $condition = '';
        switch ($user->getLevel()) {
            case 1:
                $condition = " AND titulo_eleitoral_municipio_id IN 
                                (SELECT access FROM accessAdmin WHERE userAdmin = {$user->getId()} and type = 'city'
                                    UNION ALL
                                SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = {$user->getId()}))";
                break;
            case 2:
                $condition = " AND titulo_eleitoral_uf_id IN (SELECT state FROM usersAdmin WHERE id = {$user->getId()})";
                break;
        }
        return $condition;
    }

    public function appointmentAffiliated()
    {
        $date = new \DateTime();
        $dateLimit = clone $date;
        $date = $date->format('Y-m-d');
        $dateLimit->sub(new \DateInterval('P1M'));
        $dateLimit = $dateLimit->format('Y-m-d');
        $params = [
            ':date1' => $dateLimit,
            ':date2' => $date,
        ];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tb_pessoa.id, tb_pessoa.nome, tb_pessoa.data_filiacao 
                FROM tb_pessoa
	            WHERE tb_pessoa.filiado = 7 AND tb_pessoa.data_filiacao BETWEEN :date1 AND :date2 AND 
                        (SELECT count(appointments.ID) from appointments WHERE tb_pessoa.ID=appointments.AFFILIATED AND appointments.STATUS IN (1, 2)) = 0 
                ORDER BY nome ASC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhere(UserAdmin $user, array $filter, &$params, &$join)
    {
        $where = '';
        $join = '';
        $groupedStatus = ['7' => '7,8', '1' => '1,3', '9' => '9,10,11,12', '2' => '2,14', '0' => '0,5'];
        if ($filter['irpf']) {
            if ($filter['irpf'] == '-1') $filter['irpf'] = 0;
            $params[':irpf'] = $filter['irpf'];
            $where .= " AND tp.irpf = :irpf";
        }
        if ($filter['exemption']) {
            if ($filter['exemption'] == '-1') $filter['exemption'] = 0;
            $params[':exemption'] = $filter['exemption'];
            $where .= " AND tp.exemption = :exemption";
        }
        if ($filter['name']) {
            $params[':name'] = "%{$filter['name']}%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($filter['cpf']) {
            $params[':cpf'] = "{$filter['cpf']}";
            $where .= " AND tp.cpf = :cpf";
        }
        if ($filter['email']) {
            $params[':email'] = "%{$filter['email']}%";
            $where .= " AND tp.email LIKE :email";
        }
        if ($filter['state']) {
            $params[':uf'] = $filter['state'];
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf";
        }
        if ($filter['voluntaryInterest']) {
            $params[':voluntaryInterest'] = $filter['voluntaryInterest'];
            //nao contactado
            if ($filter['voluntaryInterest'] == 3) $where .= " AND (youthSearchs.voluntaryInterest = :voluntaryInterest OR youthSearchs.user IS NULL)";
            else $where .= " AND youthSearchs.voluntaryInterest = :voluntaryInterest";
        }
        if ($filter['student']) {
            $params[':student'] = $filter['student'];
            //nao contactado
            if ($filter['student'] == 14) $where .= " AND (youthSearchs.student = :student OR youthSearchs.user IS NULL)";
            else $where .= " AND youthSearchs.student = :student";
        }
        if ($filter['municipalLeader']) {
            $params[':municipalLeader'] = $filter['municipalLeader'];
            //nao contactado
            if ($filter['municipalLeader'] == 21) $where .= " AND (youthSearchs.municipalLeader = :municipalLeader OR youthSearchs.user IS NULL)";
            else $where .= " AND youthSearchs.municipalLeader = :municipalLeader";
        }
        if ($filter['joinedWhatsAppGroup']) {
            $params[':joinedWhatsAppGroup'] = $filter['joinedWhatsAppGroup'];
            //nao contactado
            if ($filter['joinedWhatsAppGroup'] == 18) $where .= " AND (youthSearchs.joinedWhatsAppGroup = :joinedWhatsAppGroup OR youthSearchs.user IS NULL)";
            else $where .= " AND youthSearchs.joinedWhatsAppGroup = :joinedWhatsAppGroup";
        }
        if ($filter['candidateInterest']) {
            $params[':candidateInterest'] = $filter['candidateInterest'];
            //nao contactado
            if ($filter['candidateInterest'] == 8) $where .= " AND (youthSearchs.candidateInterest = :candidateInterest OR youthSearchs.user IS NULL)";
            else $where .= " AND youthSearchs.candidateInterest = :candidateInterest";
        }
        if ($filter['city']) {
            $params[':city'] = $filter['city'];
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($filter['state2']) {
            $params[':state2'] = $filter['state2'];
            $join .= ' LEFT JOIN tb_estado AS state2 ON state2.id = en.tb_estado_id ';
            $where .= " AND en.tb_estado_id = :state2";
        }
        if ($filter['city2']) {
            $params[':city2'] = $filter['city2'];
            $join .= ' LEFT JOIN tb_cidade AS city2 ON city2.id = en.tb_cidade_id ';
            $where .= " AND en.tb_cidade_id = :city2";
        }
        if ($filter['status'] != '') {
            if (array_key_exists($filter['status'], $groupedStatus)) {
                $where .= " AND tp.filiado IN ({$groupedStatus[$filter['status']]})";
            } else {
                $params[':status'] = $filter['status'];
                $where .= " AND tp.filiado = :status";
            }
        }
        if ($filter['gender']) {
            $params[':gender'] = $filter['gender'];
            $where .= ' AND tp.genero = :gender';
        }
        if ($filter['begin']) {
            $params[':begin'] = "{$filter['begin']}";
            if ($filter['typeDate'] == 'data_filiacao_refiliacao') {
                $where .= " AND (tp.data_filiacao >= :begin || tp.data_refiliacao >= :begin)";
            } else {
                $where .= " AND tp.{$filter['typeDate']} >= :begin";
            }
        }
        if ($filter['end']) {
            $params[':end'] = $filter['end'];
            if ($filter['typeDate'] == 'data_filiacao_refiliacao') {
                $where .= " AND (tp.data_filiacao <= :end || tp.data_refiliacao <= :end)";
            } else {
                $where .= " AND tp.{$filter['typeDate']} <= :end";
            }
        }
        if ($filter['affiliateId']) {
            $params[':affiliateId'] = $filter['affiliateId'];
            $where .= " AND tp.filiado_id = :affiliateId";
        }
        if ($filter['mesoregion']) {
            $join .= ' LEFT JOIN mesoregions me ON me.state = state.id
                       LEFT JOIN mesoregionsCities mec ON mec.mesoregion = me.id ';
            $params[':meso'] = $filter['mesoregion'];
            $where .= " AND tp.titulo_eleitoral_municipio_id IN (SELECT city FROM mesoregionsCities WHERE mesoregion = :meso)";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if (!$filter['city']) {
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (tp.titulo_eleitoral_municipio_id = :city || 
                    tp.titulo_eleitoral_municipio_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        }
        if ($filter['ageBegin']) {
            $params[':ageBegin'] = (new \DateTime())->sub(new \DateInterval("P{$filter['ageBegin']}Y"))->format('Y-m-d');
            $where .= " AND tp.data_nascimento <= :ageBegin";
        }
        if ($filter['ageEnd']) {
            $params[':ageEnd'] = (new \DateTime())->sub(new \DateInterval("P{$filter['ageEnd']}Y"))->format('Y-m-d');
            $where .= " AND tp.data_nascimento >= :ageEnd";
        }
        if ($filter['situation']) {
            if ($filter['situation'] == 'Isento') {
                $where .= " AND tp.exemption = 1";
            } else {
                $params[':situation'] = $filter['situation'];
                $where .= " AND tp.exemption = 0 && IFNULL(
                            (SELECT IF(tbt.status = 'paid' , 'Adimplente', 'Inadimplente') AS 'Situacao'
                                FROM  tb_transacoes tbt
                                WHERE tbt.tb_pessoa_id = tp.id 
                                    AND tbt.status NOT IN ('accumulated' , 'canceled','reversed') 
                                    AND IF(tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                                    AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                                    ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                                ), 
                            'Inadimplente') = :situation ";
            }
        }
        return $where;
    }

    public function list(UserAdmin $user, array $filter, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $filter, $params, $join);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $orderBy = ' ORDER BY tp.id DESC ';
        if ($filter['orderBy']) {
            $orderBy = explode('-', $filter['orderBy']);
            if ($orderBy[0] == 'data_filiacao_refiliacao') {
                $orderBy =
                    "ORDER BY
                        CASE 
                            WHEN tp.data_refiliacao IS NOT NULL THEN tp.data_refiliacao
                            ELSE tp.data_filiacao
                        END 
                    {$orderBy[1]}";
            } elseif ($orderBy[0] == 'data_solicitacao') {
                $orderBy =
                    "ORDER BY
                        CASE 
                            WHEN tp.data_solicitacao_refiliacao IS NOT NULL THEN tp.data_solicitacao_refiliacao
                            ELSE tp.data_solicitacao_filiacao
                        END 
                    {$orderBy[1]}";
            } else {
                $orderBy = " ORDER BY {$orderBy[0]} {$orderBy[1]} ";
            }
        }
        if ($filter['phone']){
            $params[':phone'] = Utils::onlyNumbers($filter['phone']);
            $params[':phone'] = $filter['phone'];
            $where .= " AND (SELECT COUNT(tbtel.id) FROM tb_telefone AS tbtel 
                        WHERE tbtel.tb_pessoa_id = tp.id AND tbtel.tb_tipo_telefone_id = 3 AND tbtel.telefone = :phone) > 0 ";
        }

        $sql = "SELECT tp.id, IF(tp.filiado IN (7,8), tp.filiado_id, '-') AS affiliatedId, tp.nome AS name,
                        IFNULL(state.sigla, '-') AS state, IFNULL(city.cidade, '-') AS city, 
                        CONCAT(TIMESTAMPDIFF(YEAR, tp.data_nascimento, CURDATE()), ' anos') AS age,                       
                        ts.status AS 'status',
                        DATE_FORMAT(IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao), '%d/%m/%Y') AS filiacao,
                        (SELECT 
                            CONCAT(tbtel.ddi, tbtel.telefone)
                            FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3
                        LIMIT 1) AS phone, 
                        youthSearchs.updatedAt, usersAdmin.name AS usersAdmin, o1.name AS voluntaryInterest,
                        o2.name AS student, o3.name AS candidateInterest, o4.name AS joinedWhatsAppGroup,
                        o5.name AS municipalLeader
                    FROM tb_pessoa AS tp
                    LEFT JOIN youthSearchs ON youthSearchs.user = tp.id
                    LEFT JOIN usersAdmin ON usersAdmin.id = youthSearchs.userAdmin
                    LEFT JOIN youthSearchOptions AS o1 ON o1.id = youthSearchs.voluntaryInterest
                    LEFT JOIN youthSearchOptions AS o2 ON o2.id = youthSearchs.student
                    LEFT JOIN youthSearchOptions AS o3 ON o3.id = youthSearchs.candidateInterest
                    LEFT JOIN youthSearchOptions AS o4 ON o4.id = youthSearchs.joinedWhatsAppGroup
                    LEFT JOIN youthSearchOptions AS o5 ON o5.id = youthSearchs.municipalLeader
                    LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_endereco AS en ON en.tb_pessoa_id = tp.id                    
                    LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                    {$join}
                WHERE 1 = 1 {$where} 
                GROUP BY tp.id {$orderBy} {$limitSql}";
//        die($sql);
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($user, $filter, $params, $join);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total
                FROM tb_pessoa AS tp
                LEFT JOIN youthSearchs ON youthSearchs.user = tp.id
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_endereco AS en ON en.tb_pessoa_id = tp.id
                LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                {$join}
                WHERE 1= 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

}
