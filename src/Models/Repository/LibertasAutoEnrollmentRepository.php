<?php

namespace App\Models\Repository;

use App\Models\Entities\LibertasAutoEnrollment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class LibertasAutoEnrollmentRepository extends EntityRepository
{
    public function save(LibertasAutoEnrollment $entity): LibertasAutoEnrollment
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateWhere($filter, &$params): string
    {
        $where = '';
        if ($filter['id']) {
            $params[':id'] = $filter['id'];
            $where .= " AND c.id = :id";
        }
        if ($filter['email']) {
            $name = $filter['email'];
            $params[':email'] = "%{$name}%";
            $where .= " AND c.email LIKE :email";
        }
        if ($filter['auto']) {
            $params[':auto'] = $filter['auto'] == "-1" ? "0" : "1";
            $where .= " AND c.auto_enrollment = :auto";
        }
        if ($filter['legislativeAgent']) {
            $params[':legislativeAgent'] = $filter['legislativeAgent'] == "-1" ? "0" : "1";
            $where .= " AND c.mandatario_legislativo = :legislativeAgent";
        }
        if ($filter['executiveAgent']) {
            $params[':executiveAgent'] = $filter['executiveAgent'] == "-1" ? "0" : "1";
            $where .= " AND c.mandatario_executivo = :executiveAgent";
        }
        if ($filter['status']) {
            $params[':status'] = $filter['status'] == "-1" ? "0" : "1";
            $where .= " AND c.active = :status";
        }
        return $where;
    }

    public function list(array $filter, int $limit, int $offset): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT c.id, c.email, c.active, c.auto_enrollment, c.mandatario_legislativo, c.mandatario_executivo
				FROM  App\Models\Entities\LibertasAutoEnrollment as c				
                WHERE 1 = 1 {$where}
                ORDER BY c.id DESC")
            ->setMaxResults($limit)
            ->setFirstResult($offset * $limit)
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function listTotal($filter): int
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(c) AS total FROM App\Models\Entities\LibertasAutoEnrollment AS c WHERE 1 = 1 {$where}")
            ->execute($params)[0]['total'];
    }


}
