<?php

namespace App\Models\Repository;

use App\Models\Entities\Zone;
use Doctrine\ORM\EntityRepository;

class ZoneRepository extends EntityRepository
{
    public function save(Zone $entity): Zone
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getCity($uf, $city)
    {
        $params[':uf'] = $uf;
        $params[':city'] = $city;
        return $this->getEntityManager()->createQuery(
            "SELECT c
                FROM App\Models\Entities\City as c
                JOIN c.state AS s
                WHERE s.sigla = :uf AND c.cidade = :city"
        )
            ->execute($params);
    }
}
