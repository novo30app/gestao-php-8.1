<?php

namespace App\Models\Repository;

use App\Models\Entities\ExtractBB;
use Doctrine\ORM\EntityRepository;

class ExtractBBRepository extends EntityRepository
{
    public function save(ExtractBB $entity):ExtractBB
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getPayments(): array
    {

        $obs = "REMESSA 7 - consolidado";

        $date = date('Y-m-d');
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT *
                    FROM paymentRequirements 
                    WHERE type = :type AND created LIKE :created AND user = :user AND requestStatus = 2 AND id NOT IN (SELECT request FROM extractBB) AND obs = :obs";
        $rows = $pdo->prepare($sql)->execute([':type' => 22, ':created' => "%$date%", ':user' => 60, ':obs' => $obs]);
        return $rows->fetchAllAssociative();
    }

    public function getBanks($id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT *
                FROM banks WHERE id = :id";
        $rows = $pdo->prepare($sql)->execute([':id' => $id]);
        return $rows->fetchAssociative();
    }

    public function consultPayments(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT * 
                    FROM extractBB 
                    WHERE created >= DATE(NOW() - INTERVAL 15 DAY)
                    ORDER BY id DESC";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}