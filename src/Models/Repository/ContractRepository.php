<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\Contract;
use App\Models\Entities\Directory;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class ContractRepository extends EntityRepository
{
    public function save(Contract $entity): Contract
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere(UserAdmin $user, array $filter, &$params): string
    {
        $where = '';
        if ($filter['id']) {
            $params[':id'] = $filter['id'];
            $where .= " AND contract.id = :id";
        }
        if ($filter['costCenter']) {
            $params[':costCenter'] = $filter['costCenter'];
            $where .= " AND contract.costCenter = :costCenter";
        }
        if ($filter['stage']) {
            $params[':stage'] = $filter['stage'];
            $where .= " AND contract.stage = :stage";
        }
        if ($filter['situation']) {
            $params[':situation'] = $filter['situation'];
            $where .= " AND contract.situation = :situation";
        }
	    if ($filter['number']) {
			$number = $filter['number'];
		    $params[':number'] = "%$number%";
		    $where .= " AND contract.number LIKE :number";
	    }
	    if ($filter['name']) {
			$name = $filter['name'];
		    $params[':name'] = "%$name%";
		    $where .= " AND contract.name LIKE :name";
	    }
	    if ($filter['value']) {
		    $params[':value'] = Utils::moneyToFloat($filter['value']);
		    $where .= " AND (contract.value = :value OR contract.monthlyValue = :value)";
	    }	
		if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':uf'] = $user->getState()->getId();
			$params[':city'] = $user->getCity()->getId();
            $where .= " AND contract.directory IN (SELECT id FROM tb_diretorio WHERE tb_estado_id = :uf AND tb_cidade_id = :city)";
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND contract.directory IN (SELECT id FROM tb_diretorio WHERE tb_estado_id = :uf)";
        }
		if ($filter['cnpj']) {
			$cnpj = str_replace('-', '', str_replace('/', '', str_replace('.', '', $filter['cnpj'])));
            $params[':cnpj'] = "%$cnpj%";
            $where .= " AND contract.cpf LIKE :cnpj";
        }
        return $where;
    }

    public function list(UserAdmin $user, array $filter, $limit = null, $offset = null): array
    {
        $params = [];
		if ($filter['order'] == 'costCenter') {
			$order = 'costCenter.name';
		} else {
			$order = 'contract.' . $filter['order'];
		}
        if ($filter['id']) $order = 'contract.id ';
        $where = $this->generateWhere($user, $filter, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT contract.id, contract.name, contract.cpf, contract.value, contract.stage, dir.nome AS directory,
       				costCenter.name AS costCenterName, contract.situation, contract.object,
       				contract.costCenter, DATE_FORMAT(contract.start, '%d/%m/%Y') AS dateStart, contract.doc,
       				contract.number, contract.category, contract.monthlyValue, contract.contract,
       				DATE_FORMAT(contract.end, '%d/%m/%Y') AS dateEnd, contract.value, contract.monthlyValue,
       				DATE_FORMAT(contract.signatureDate, '%d/%m/%Y') AS signature, 
					CONCAT(ch.account, ' / ', ch.description) AS InputAccounts,
					(SELECT CONCAT(c.number, ' - ', c.name) FROM contract c WHERE c.id = contract.contract) AS descriptionContract
                FROM contract
                LEFT JOIN  costCenter ON costCenter.id = contract.costCenter
				LEFT JOIN tb_diretorio dir ON dir.id = contract.directory
				LEFT JOIN chartOfAccounts ch ON ch.id = contract.account
                WHERE contract.status = 1 {$where}
				ORDER BY {$order} {$filter['seq']} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($user, $filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(contract.id) AS total
                FROM contract
                WHERE contract.status = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
	
	public function searchCode()
	{
		$params = [];
		$year = date('Y');
		$params[':year'] = "%/$year%";
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT contract.number
                FROM contract
                WHERE contract.status = 1 AND contract.number LIKE :year
                ORDER BY contract.number DESC LIMIT 1";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function getContracts(): array
	{
		$params = [];
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT contract.id, contract.name,contract.number
                FROM contract
                LEFT JOIN  costCenter ON costCenter.id = contract.costCenter
                WHERE contract.status = 1 AND contract.contract IS NULL
				ORDER BY contract.id DESC";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function getContractsByCpf(string $cpf, ?Directory $directory): array
	{
		$params = [];
		$params[':cpf'] = $cpf;
		$where = '';
		if ($directory) {
			$params[':directory'] = $directory->getId();
			$where = " AND contract.directory = :directory";
		}
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT contract.id, contract.name, contract.number, contract.situation, contract.stage, contract.costCenter
                FROM contract
                LEFT JOIN  costCenter ON costCenter.id = contract.costCenter
                WHERE contract.status = 1 AND contract.contract IS NULL AND contract.cpf = :cpf 
					AND contract.dateCancel IS NULL {$where}
				ORDER BY contract.id DESC";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function searchSituationContract(): array
	{
		$params = [];
		$date = new \DateTime();
		$params[':date'] = $date->format('Y-m-d');
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT contract.id
                FROM contract
                WHERE contract.situation < 3 AND contract.end < :date AND contract.contract IS NULL
				ORDER BY contract.id DESC";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function verifyAdditive($id): array
	{
		$params = [];
		$date = new \DateTime();
		$params[':date'] = $date->format('Y-m-d');
		$params[':contract'] = $id;
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT contract.id
                FROM contract
                WHERE (contract.end >= :date OR contract.end IS NULL) AND contract.contract = :contract
				ORDER BY contract.id DESC";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}

	public function getNumberAvailable(): array
	{
		$params = [];
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT LPAD(CONCAT(SUBSTRING_INDEX(number, '/', 1) + 1, '/', YEAR(CURDATE())), 9, '0') AS avaibleNumber 
				FROM contract ORDER BY number DESC LIMIT 1";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}
}