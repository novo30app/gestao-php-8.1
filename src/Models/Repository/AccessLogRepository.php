<?php

namespace App\Models\Repository;

use App\Models\Entities\AccessLog;
use Doctrine\ORM\EntityRepository;

class AccessLogRepository extends EntityRepository
{
    public function save(AccessLog $entity):AccessLog
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, $state = null, $city = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($state) {
            $params[':uf'] = $state;
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        return $where;
    }

    public function list($name = null, $uf = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $uf, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT log.id, tp.nome AS name, DATE_FORMAT(log.created, '%d/%m/%Y %H:%i:%s') AS created, log.device, log.so,
                        IF(city.cidade IS NOT NULL, city.cidade, '') AS cityStr, 
                        IF(state.sigla IS NOT NULL, state.sigla, '') AS ufStr                       
                FROM accessLog AS log
                JOIN tb_pessoa AS tp ON tp.id = log.user
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                WHERE 1 = 1 {$where}
                ORDER BY id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null, $uf = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $uf, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(log.id) AS total                  
                FROM accessLog AS log
                JOIN tb_pessoa AS tp ON tp.id = log.user
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}