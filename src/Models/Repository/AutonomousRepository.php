<?php

namespace App\Models\Repository;

use Doctrine\ORM\EntityRepository;
use App\Models\Entities\Autonomous;

class AutonomousRepository extends EntityRepository
{
    public function save(Autonomous $entity): Autonomous
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}