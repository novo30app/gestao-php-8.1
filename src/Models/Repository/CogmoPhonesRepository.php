<?php

namespace App\Models\Repository;

use App\Models\Entities\CogmoPhones;
use Doctrine\ORM\EntityRepository;

class CogmoPhonesRepository extends EntityRepository
{
    public function save(CogmoPhones $entity):CogmoPhones   
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}