<?php

namespace App\Models\Repository;

use App\Models\Entities\SignatureDoc;
use Doctrine\ORM\EntityRepository;

class SignatureDocRepository extends EntityRepository
{
    public function save(SignatureDoc $entity): SignatureDoc
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($title, &$params): string
    {
        $where = '';
        if ($title) {
            $params[':title'] = "%$title%";
            $where .= " AND signatureDoc.title LIKE :title";
        }
        return $where;
    }

    public function list($title, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($title, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT  signatureDoc.id, signatureDoc.created, signatureDoc.title, signatureDoc.active, usersAdmin.name
                FROM signatureDoc
                JOIN usersAdmin ON usersAdmin.id = signatureDoc.user
                WHERE 1 = 1  {$where} ORDER BY signatureDoc.id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($title): array
    {
        $params = [];
        $where = $this->generateWhere($title, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(signatureDoc.id) AS total  
                FROM signatureDoc
                JOIN usersAdmin ON usersAdmin.id = signatureDoc.user
                WHERE 1 = 1  {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}