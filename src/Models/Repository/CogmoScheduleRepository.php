<?php

namespace App\Models\Repository;

use App\Models\Entities\CogmoSchedule;
use Doctrine\ORM\EntityRepository;
use App\Helpers\Utils;

class CogmoScheduleRepository extends EntityRepository
{
    public function save(CogmoSchedule $entity): CogmoSchedule
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    public function generateWhere($id, $name, $phone, $collector, $type, $status, $date, &$params): string
    {
        $where = '';
        if ($id) {
            $where .= " AND c.id = :id";
            $params[':id'] = $id;
        }
        if ($name) {
            $where .= " AND p.nome LIKE :name";
            $params[':name'] = "%$name%";
        }
        if ($phone) {
            $where .= " AND c.contact_number LIKE :phone";
            $params[':phone'] = "55" . Utils::onlyNumbers($phone);
        }
        if ($collector) {
            $where .= " AND cp.id = :collector";
            $params[':collector'] = $collector;
        }
        if ($type) {
            $type = $type == 1 ? 'Cobrar' : 'Lembrar';
            $where .= " AND c.message LIKE :type";
            $params[':type'] = "%$type%";
        }
        if ($status) {
            $where .= " AND c.status = :status";
            $params[':status'] = $status;
        }
        if ($date) {
            $where .= " AND c.scheduled_time LIKE :date";
            $params[':date'] = "%$date%";
        }
        return $where;
    }

    public function list($id, $name, $phone, $collector, $type, $status, $date, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($id, $name, $phone, $collector, $type, $status, $date, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT 
                    c.id, 
                    p.nome AS name,
                    c.transaction,
                    c.contact_id,
                    c.contact_number, 
                    cp.name AS collector,
                    IF(c.message LIKE '%Cobrar%', 'Cobrança', 'Lembrete') AS message, 
                    (CASE c.status
                        WHEN 'scheduled' THEN 'Agendado' 
                        WHEN 'sent' THEN 'Enviado' 
                        WHEN 'error' THEN 'Erro' 
                        WHEN 'canceled' THEN 'Cancelado' 
                        ELSE 'Desconhecido' 
                    END) AS statusLabel, 
                    c.status,
                    DATE_FORMAT(c.scheduled_time, '%d/%m/%Y %H:%i') AS scheduled_time
                FROM cogmo_schedule c
                LEFT JOIN tb_pessoa p ON p.id = c.contact_id
                LEFT JOIN cogmo_phones cp ON cp.id = c.phone_number
                WHERE 1 = 1 {$where}
                ORDER BY c.status, c.scheduled_time ASC
                {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($id, $name, $phone, $collector, $type, $status, $date): array
    {
        $params = [];
        $where = $this->generateWhere($id, $name, $phone, $collector, $type, $status, $date, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS total 
                FROM cogmo_schedule c
                LEFT JOIN tb_pessoa p ON p.id = c.contact_id
                LEFT JOIN cogmo_phones cp ON cp.id = c.phone_number 
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

}