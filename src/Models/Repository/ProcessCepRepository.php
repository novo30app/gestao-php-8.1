<?php

namespace App\Models\Repository;

use App\Models\Entities\ProcessCep;
use Doctrine\ORM\EntityRepository;

class ProcessCepRepository extends EntityRepository
{
    public function save(ProcessCep $entity):ProcessCep
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getProcessCEPCRM($user): array
	{
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT *
                FROM cep.denounced
                WHERE cpf = :cpf || denouncedEmail = :email";
		$rows = $pdo->prepare($sql)->execute([':cpf' => $user->getCpf(), ':email' => $user->getEmail()]);

		return $rows->fetchAllAssociative();
	}
}