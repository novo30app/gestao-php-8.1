<?php

namespace App\Models\Repository;

use App\Models\Entities\Events;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class EventsRepository extends EntityRepository
{
    public function save(Events $entity):Events
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql; 
    }

    private function generateAccess(UserAdmin $user) {
        $id = $user->getId();
        $condition = '';
        switch ($user->getLevel()) {
            case 1:
                $condition = " AND e.cidade_id IN (SELECT access FROM accessAdmin WHERE userAdmin = $id and type = 'city')";
                break;
            case 2:
                $condition = " AND e.estado_id IN (SELECT state FROM usersAdmin WHERE id = $id)";
                break;
        }
        return $condition;
    }

    private function generateWhere($user = null, $name = null, $specie = null, $directory = null, $status = null, $state = null, $city = null, &$params)
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND e.nome LIKE :name";
        } 
        if ($directory) {
            $params[':directory'] = "$directory";
            $where .= " AND e.diretorio_id = :directory";
        } 
        if ($specie) {
            $params[':specie'] = "$specie";
            $where .= " AND e.especie = :specie";
        } 
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND e.status = :status";
        } 
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND e.estado_id = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND e.cidade_id = :city";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if(!$city){
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (e.cidade_id = :city || 
                    e.cidade_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND e.estado_id = :uf";
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $specie = null, $directory = null, $status = null, $state = null, $city = null, $limit = null, $offset = null): array
    {       
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $specie, $directory, $status, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT e.id, e.nome AS nome, DATE_FORMAT(e.data_inicio, '%d/%m/%Y %H:%i:%s') as dataInicio, td.nome AS diretorio, e.status AS status, e.tipo AS tipo, IFNULL(tbc.cidade,'-') AS city, IFNULL(tbe.sigla,'-') AS state,
                (CASE e.especie
                    WHEN 1 THEN 'Loja'
                    WHEN 2 THEN 'Mobilização'
                    WHEN 3 THEN 'Online'
                    WHEN 4 THEN 'Política da Mulher'
                    WHEN 5 THEN 'Tradicional'
                    WHEN 6 THEN 'Vaquinha'
                    ELSE '-'
                END) AS 'especie'
                FROM tb_eventos AS e 
                LEFT JOIN tb_diretorio td ON td.id = e.diretorio_id
                LEFT JOIN tb_cidade tbc ON tbc.id = e.cidade_id
                LEFT JOIN tb_estado tbe ON tbe.id = e.estado_id
                WHERE 1 = 1 {$where} ORDER BY e.id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($user, $name = null, $specie = null, $directory = null, $status = null, $state = null, $city = null): array
    {   
        $params = [];
        $limitSql = $this->generateAccess($user);
        $where = $this->generateWhere($user, $name, $specie, $directory, $status, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(e.id)) AS total                 
                    FROM tb_eventos AS e 
                    LEFT JOIN tb_diretorio td on td.id = e.diretorio_id
                    WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhereGeralRegistrations($user = null, $event = null, $name = null, $gender = null, $cpf = null, $status = null, $state = null, $city = null, &$params)
    {
        $where = '';
        if ($event) {
            $params[':event'] = "%$event%";
            $where .= " AND ev.nome LIKE :event";
        }
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tep.nome LIKE :name";
        }
        if ($gender) {
            if($gender == 4) {
                $where .= " AND tp.genero IS NULL";
            } else {
                $params[':gender'] = "$gender";
                $where .= " AND tp.genero = :gender";
            }
        } 
        if ($cpf) {
            $params[':cpf'] = "%$cpf%";
            $where .= " AND tep.cpf LIKE :cpf";
        } 
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND tep.status = :status";
        } 
        if ($state) {
            $params[':state'] = "$state";
            $where .= " AND ev.estado_id = :state";
        }         
        if ($city) {
            $params[':city'] = "$city";
            $where .= " AND ev.cidade_id = :city";
        } 
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if(!$city){
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (ev.cidade_id = :city || 
                    ev.cidade_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND ev.estado_id = :uf";
        }
        return $where;
    }

    public function listGeralRegistrations(UserAdmin $user, $event = null, $name = null, $gender = null, $cpf = null, $status = null, $state = null, $city = null, $limit = null, $offset = null): array
    {       
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereGeralRegistrations($user, $event, $name, $gender, $cpf, $status, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ev.id, IFNULL(tp.id, tep.tb_pessoa_id) AS personId, IFNULL(tp.nome, tep.nome) AS name, CONCAT(ci.cidade, '/', es.sigla) AS location, 
                (CASE tp.genero
                    WHEN 1 THEN 'Masculino'
                    WHEN 2 THEN 'Feminino'
                    WHEN 3 THEN 'Não quero informar'
                    ELSE 'Desconhecido'
                    END
                ) AS gender,
                IFNULL(IFNULL(tp.cpf, tep.cpf), '-') AS cpf, tep.status, ev.nome AS event, 
                DATE_FORMAT(IFNULL(ev.data_evento, ev.data_inicio), '%d/%m/%Y %H:%i:%s') AS eventDate,
                    IFNULL(tep.recommendation, '-') AS recommendation
                FROM tb_eventos_participantes tep
                LEFT JOIN tb_eventos ev ON ev.id = tep.tb_eventos_id
                LEFT JOIN tb_pessoa tp ON tp.id = tep.tb_pessoa_id
                LEFT JOIN tb_estado es ON es.id = ev.estado_id
                LEFT JOIN tb_cidade ci ON ci.id = ev.cidade_id
                WHERE 1 = 1 {$where}
                GROUP BY tep.id
                ORDER BY tep.data_criacao DESC
                {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalGeralRegistrations(UserAdmin $user, $event = null, $name = null, $gender = null, $cpf = null, $status = null, $state = null, $city = null): array
    {   
        $params = [];
        $limitSql = $this->generateAccess($user);
        $where = $this->generateWhereGeralRegistrations($user, $event, $name, $gender, $cpf, $status, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tep.id)) AS total FROM tb_eventos_participantes tep
                LEFT JOIN tb_eventos ev ON ev.id = tep.tb_eventos_id
                LEFT JOIN tb_pessoa tp ON tp.id = tep.tb_pessoa_id
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}