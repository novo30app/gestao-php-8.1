<?php

namespace App\Models\Repository;

use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionKeyWord;
use Doctrine\ORM\EntityRepository;

class InteractionKeyWordRepository extends EntityRepository
{
    public function save(InteractionKeyWord $entity): InteractionKeyWord
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function dropByInteraction(Interaction $interaction)
    {
        $sql = "DELETE FROM interactionKeyWords WHERE interaction = :interaction";
        $params = [':interaction' => $interaction->getId()];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }
}