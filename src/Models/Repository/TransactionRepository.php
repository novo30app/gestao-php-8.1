<?php

namespace App\Models\Repository;

use App\Models\Entities\Directory;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    public function save(Transaction $entity): Transaction
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getConciliationRede(\DateTime $start, \DateTime $end, int $directory): array
    {
        return $this->getEntityManager()
            ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t                                  
                              WHERE t.status = 'paid' AND t.gatewayPagamento = 2 AND ( t.tbDiretorioId = :directory )
                              AND t.dataPago BETWEEN :start AND :end")
            ->setParameters([':start' => $start->format('Y-m-d'), ':end' => "{$end->format('Y-m-d')} 23:59:59",
                ':directory' => $directory])
            ->getResult() ?? [];
    }

    public function getByUser(User $user): array
    {
        return $this->getEntityManager()
            ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t                                  
                              WHERE t.tbPessoaId = :user AND t.status <> 'canceled' ORDER BY t.dataCriacao DESC")
            ->setParameters([':user' => $user->getId()])
            ->getResult() ?? [];
    }


    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhereDefaulters(UserAdmin $user, $name = null, $state = null, $city = null, $situation = null, 
        $meso = null, $email = null, $cpf = null, $paymentMethod = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tbp.nome LIKE :name";
        }
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND tbp.email LIKE :email";
        }
        if ($cpf) {
            $params[':cpf'] = "%$cpf%";
            $where .= " AND tbp.cpf LIKE :cpf";
        }
        if ($state == "-1") {
            $params[':outside'] = 1;
            $where .= " AND te.reside_exterior = :outside";
        } else {
            if ($state) {
                $params[':state'] = $state;
                $where .= " AND tbp.titulo_eleitoral_uf_id = :state";
            }
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND tbp.titulo_eleitoral_municipio_id = :city";
            }
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if (!$city) {


                $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
                $sql = "SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                        UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)";
                $rows = $pdo->prepare($sql)->execute([':userId' => $user->getId()]);
                $cities = $rows->fetchAllAssociative();
                $citiesArray = [];
                foreach ($cities as $city) $citiesArray[] = $city['access'];
                if ($citiesArray) {
                    $cities = implode(',', $citiesArray);
                    $where .= " AND (tbp.titulo_eleitoral_municipio_id = :city OR tbp.titulo_eleitoral_municipio_id IN ({$cities}))";
                } else {
                    $where .= " AND tbp.titulo_eleitoral_municipio_id = :city ";
                }
                $params[':city'] = $user->getCity()->getId();

            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND tbp.titulo_eleitoral_uf_id = :uf";
        }
        if ($situation) {
            if ($situation == 'Isento') {
                $where .= " AND tbp.exemption = 1";
            } else {
                $params[':situation'] = $situation;
                $where .= " AND IFNULL((SELECT IF(tbp.exemption = 1, 'Isento', IF(tbt.valor = '0.00' || tbt.status = 'paid' , 'Adimplente', 'Inadimplente'))
                                        FROM  tb_transacoes tbt
                                        WHERE tbt.tb_pessoa_id = tbp.id 
                                            AND tbt.status NOT IN ('accumulated' , 'canceled','reversed') 
                                            AND IF(tbp.exemption = 0 AND tbp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                                            ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                                        ), 'Inadimplente') = :situation ";
            }
        }
        if ($meso) {
            $params[':meso'] = $meso;
            $where .= " AND tbp.titulo_eleitoral_municipio_id IN (SELECT city FROM mesoregionsCities WHERE mesoregion = :meso)";
        }
        if ($paymentMethod) {
            $params[':paymentMethod'] = $paymentMethod;
            $where .= " AND tpa.forma_pagamento = :paymentMethod";
        }
        return $where;
    }

    public function defaulters(UserAdmin $user, $name = null, $state = null, $city = null, $situation = null, $meso = null, 
        $email = null, $cpf = null, $paymentMethod = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereDefaulters($user, $name, $state, $city, $situation, 
            $meso, $email, $cpf, $paymentMethod, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $email = $user->getLevel() == 3 ? ', tbp.email' : '';
        $sql = "SELECT tbp.id, tbp.nome, IF(tpa.forma_pagamento = 2, 'Boleto', 'Cartão de Crédito') AS paymentMethod,
                    IFNULL(IF(te.reside_exterior = 1, CONCAT(tp.nome, '/', te.estado), tbe.estado), '-') AS estado,
                    IFNULL(IF(te.reside_exterior = 1, te.cidade, tbc.cidade), '-') AS cidade,
                    DATE_FORMAT(IF(tbp.filiado = 7, tbp.data_filiacao, tbp.data_refiliacao), '%d/%m/%Y') AS filiacao,
                    (SELECT IF(periodicidade = 12, 'Anual', 'Mensal') FROM tb_pessoa_assinatura ta WHERE ta.tb_pessoa_id = tbp.id AND ta.origem_transacao = 2) AS 'plan',
                    (SELECT 
                        CONCAT(tbtel.ddi,
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                        FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tbp.id AND tb_tipo_telefone_id = 3
                    LIMIT 1) AS phone,
                    IF(tbp.pendencia_titulo = 1,
                        (SELECT COUNT(*) FROM tb_transacoes tbtt WHERE tbtt.tb_pessoa_id = tbp.id AND (tbtt.origem_transacao = 2 ||  tbtt.origem_transacao = 1 AND tbtt.obs = 'F') AND status IN ('pending' , 'expired')),
                        (SELECT COUNT(*) FROM tb_transacoes tbtt WHERE tbtt.tb_pessoa_id = tbp.id AND tbtt.origem_transacao = 2 AND status IN ('pending' , 'expired'))) AS opened,
                    IF(tbp.pendencia_titulo = 1,
                        (SELECT COUNT(*) FROM tb_transacoes tbtt WHERE tbtt.tb_pessoa_id = tbp.id AND (tbtt.origem_transacao = 2 ||  tbtt.origem_transacao = 1 AND tbtt.obs = 'F') AND status IN ('paid')),  
                    (SELECT COUNT(*) FROM tb_transacoes tbtt WHERE tbtt.tb_pessoa_id = tbp.id AND origem_transacao = 2 AND status IN ('paid'))) AS paid,
                    IFNULL((SELECT IF(tbp.exemption = 1, 'Isento', IF(tbt.valor = '0.00' || tbt.status = 'paid' , 'Adimplente', 'Inadimplente'))
                        FROM  tb_transacoes tbt
                        WHERE tbt.tb_pessoa_id = tbp.id 
                            AND tbt.status NOT IN ('accumulated' , 'canceled', 'reversed') 
                            AND IF(tbp.exemption = 0 AND tbp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                        ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                    ), 'Inadimplente') AS situation, 
                    g.genero {$email}
                FROM tb_pessoa tbp
                LEFT JOIN tb_transacoes tbt ON tbt.tb_pessoa_id = tbp.id
                LEFT JOIN tb_cidade tbc ON tbc.id = tbp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_estado tbe ON tbe.id = tbp.titulo_eleitoral_uf_id
                LEFT JOIN tb_genero g ON g.id = tbp.genero
                LEFT JOIN tb_endereco te ON te.tb_pessoa_id = tbp.id
                LEFT JOIN tb_pais tp ON tp.id = te.tb_pais_id
                LEFT JOIN tb_pessoa_assinatura tpa ON tpa.tb_pessoa_id = tbp.id AND tpa.origem_transacao = 2
                WHERE filiado IN (7,8) {$where} GROUP BY tbp.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function defaultersTotal(UserAdmin $user, $name = null, $state = null, $city = null, $situation = null, $meso = null, 
        $email = null, $cpf = null, $paymentMethod = null): array
    {
        $params = [];
        $where = $this->generateWhereDefaulters($user, $name, $state, $city, $situation, 
            $meso, $email, $cpf, $paymentMethod, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tbp.id)) AS total 
                 FROM tb_pessoa tbp
                 LEFT JOIN tb_transacoes tbt ON tbt.tb_pessoa_id = tbp.id
                 LEFT JOIN tb_cidade tbc ON tbc.id = tbp.titulo_eleitoral_municipio_id
                 LEFT JOIN tb_estado tbe ON tbe.id = tbp.titulo_eleitoral_uf_id
                 LEFT JOIN tb_genero g ON g.id = tbp.genero
                 LEFT JOIN tb_endereco te ON te.tb_pessoa_id = tbp.id
                 LEFT JOIN tb_pais tp ON tp.id = te.tb_pais_id
                 LEFT JOIN tb_pessoa_assinatura tpa ON tpa.tb_pessoa_id = tbp.id AND tpa.origem_transacao = 2
                 WHERE filiado IN (7,8) {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhere(UserAdmin $user, $name = null, $typeDate = null, $begin = null, $end = null, $status = null, $origin = null, $plan = null, $payment = null, $state = null, $city = null, $directory = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND tt.status = :status";
        }
        if ($origin) {
            $params[':origin'] = "$origin";
            $where .= " AND tt.origem_transacao = :origin";
        }
        if ($plan) {
            if ($plan == "-1") $plan = 0;
            $params[':plan'] = "$plan";
            $where .= " AND tt.periodicidade = :plan";
        }
        if ($payment) {
            $params[':payment'] = "$payment";
            $where .= " AND tt.forma_pagamento = :payment";
        }
        if ($begin) {
            $params[':begin'] = "$begin";
            $where .= " AND tt.$typeDate >= :begin";
        }
        if ($end) {
            $params[':end'] = "$end";
            $where .= " AND tt.$typeDate <= :end";
        }
        if ($directory) {
            $params[':directory'] = "$directory";
            $where .= " AND d.nome = :directory";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':city'] = $user->getCity()->getId();
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state AND tp.titulo_eleitoral_municipio_id = :city";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf";
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        } else {
            if ($state) {
                $params[':state'] = $state;
                $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            }
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $typeDate = null, $begin = null, $end = null, $status = null, $origin = null, $plan = null, $payment = null, $state = null, $city = null, $directory = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, $directory, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tt.id, tt.tb_pessoa_id, tp.nome as name, tp.cpf, tt.invoice_id,  tt.numero_recibo as receipt, DATE_FORMAT(tt.data_criacao, '%d/%m/%Y') as created, IFNULL(DATE_FORMAT(tt.data_pago, '%d/%m/%Y'),'-') as payed,
                    IFNULL(te.estado, '-') AS state,  IFNULL(tc.cidade, '-') as city, REPLACE(REPLACE(REPLACE(FORMAT(tt.valor, 2), '.', '@'), ',', '.'), '@', ',') as 'value', d.nome AS directory,
                    (CASE `tt`.`status`
                        WHEN 'paid' THEN 'Pago'
                        WHEN 'expired' THEN 'Expirado'
                        WHEN 'pending' THEN 'Pendente'
                        WHEN 'canceled' THEN 'Cancelada'
                        WHEN 'Chargeback' THEN 'Chargeback'
                        WHEN 'refunded' THEN 'Reembolsado'
                        WHEN 'partially paid' THEN 'Pago Parcial'
                        WHEN 'accumulated' THEN 'Acumulado'
                        WHEN 'reversed' THEN 'Estornado'
                    END) as `status`,
                    (CASE `tt`.`origem_transacao`
                        WHEN 1 THEN 'Doação'
                        WHEN 2 THEN 'Filiação'
                        WHEN 3 THEN 'Doação Campanha'
                        WHEN 4 THEN 'Evento'
                        WHEN 5 THEN 'Complemento de Filiação'
                        WHEN 6 THEN 'Doação Maquininha'
                        WHEN 8 THEN 'Plano de Contribuição'
                        WHEN 15 THEN 'Filiação antecipada'
                    END) AS `origin`,
                    (CASE `tt`.`periodicidade`
                        WHEN 0 THEN 'Avulsa'
                        WHEN 1 THEN 'Mensal'
                        WHEN 6 THEN 'Semestral'
                        WHEN 12 THEN 'Anual'
                        WHEN 24 THEN 'Bienal'
                    END) AS `plan`,
                    (CASE `tt`.`forma_pagamento`
                        WHEN 1 THEN 'Cartão'
                        WHEN 2 THEN 'Boleto'
                        WHEN 3 THEN 'TED'
                        WHEN 4 THEN 'DOC'
                        WHEN 5 THEN 'Transf Online'
                        WHEN 6 THEN 'Deposit Online'
                        WHEN 7 THEN 'Deposito'
                        WHEN 8 THEN 'Cheque'
                        WHEN 9 THEN 'Transferência'
                        WHEN 10 THEN 'Cartão de Débito'
                        WHEN 11 THEN 'PIX'
                    END) AS `payment`
                FROM tb_transacoes as tt 
                    LEFT JOIN tb_pessoa tp ON tt.tb_pessoa_id = tp.id
                    LEFT JOIN tb_endereco tben ON tben.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_diretorio d ON d.id = tt.tb_diretorio_id
                WHERE 1 = 1 {$where}
                GROUP BY tt.id
                ORDER BY tt.id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $typeDate = null, $begin = null, $end = null, $status = null, $origin = null, $plan = null, $payment = null, $state = null, $city = null, $directory = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, $directory, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tt.id)) AS total 
                FROM tb_transacoes AS tt
                LEFT JOIN tb_pessoa tp ON tt.tb_pessoa_id = tp.id
                LEFT JOIN tb_endereco tben ON tben.tb_pessoa_id = tp.id
				LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
				LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_diretorio d ON d.id = tt.tb_diretorio_id
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listTransactions($email = null): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS qtde FROM tb_transacoes tt
                    JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                    WHERE tp.email = '{$email}' AND tt.status IN ('pending','expired') AND tt.origem_transacao = 2
                    AND 
                        IF (tp.membro_diretorio != 1, 
                            tt.data_criacao < CURDATE() - INTERVAL 10 DAY AND tt.data_criacao > '2019-01-01',
                            tt.data_criacao < CURDATE() - INTERVAL 10 DAY
                        )";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function exportFinancialReport(UserAdmin $user, $name = null, $typeDate = null, $begin = null, $end = null, $status = null, 
        $origin = null, $plan = null, $payment = null, $state = null, $city = null, $directory = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $typeDate, $begin, $end, $status, $origin, $plan, $payment, $state, $city, 
            $directory, $params);

        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT 
                    tt.invoice_id,
                    tp.nome,
                    tp.cpf,
                    IFNULL(tp.titulo_eleitoral, '') AS titulo_eleitoral,
                    
                    tt.id AS transacao_id,
                    tt.data_pago,
                    IFNULL(tt.data_deposito, '') AS data_deposito,
                    IFNULL(tt.data_liberacao, '') AS data_liberacao,
                    IFNULL(tt.data_cancelamento, '') AS data_cancelamento,
                    tt.numero_recibo,
                    tt.invoice_id,
                    IFNULL(tt.nsu, '') AS nsu,
                    IFNULL(tt.obs, '') AS obs,
                    tt.status,
                    IFNULL(te.nome, '') AS evento,
                    IFNULL(tt.creditCardFlag, '') AS bandeira_do_cartao,
                    
                    IFNULL(d.nome, '') AS diretorio_id,
                    IFNULL(dir2.nome, '') AS diretorio_origem,
                    
                    IFNULL(es1.estado, '') AS estado_titulo,
                    IFNULL(ci1.cidade, '') AS cidade_titulo,
                    
                    es2.estado AS estado_endereco,
                    ci2.cidade AS cidade_endereco,
                    
                    REPLACE(REPLACE(REPLACE(FORMAT(tt.valor, 2), '.', '@'), ',', '.'), '@', ',') AS valor_bruto,
                    IFNULL(REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ','), '') AS valor_liquido,
                    IFNULL(REPLACE(ROUND((tt.valor - tt.valor_s_taxas),2),'.',','), '') AS taxas,
                    
                    /* BEGIN: RATEIO DM */
                    IF(tt.origem_transacao IN(2, 8),
                        /* Filiação ou plano de contribuição */
                        IFNULL(
                            (CASE 
                                WHEN tp.titulo_eleitoral_uf_id NOT IN (7,2) 
                                AND tp.titulo_eleitoral_municipio_id 
                                    IN(SELECT tb_cidade_id 
                                        FROM tb_diretorio 
                                        WHERE tb_diretorio.nome LIKE '%Diretório Municipal%' 
                                        OR tb_diretorio.nome LIKE '%Núcleo Municipal%') 
                                THEN REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas * 0.60, 2), '.', '@'), ',', '.'), '@', ',')
                                    
                                WHEN tp.titulo_eleitoral_uf_id NOT IN (7,2)
                                AND tp.titulo_eleitoral_municipio_id 
                                    NOT IN(SELECT tb_cidade_id 
                                            FROM tb_diretorio 
                                            WHERE tb_diretorio.nome LIKE '%Diretório Municipal%' 
                                            OR tb_diretorio.nome LIKE '%Núcleo Municipal%')
                                THEN ''
                                
                                ELSE ''
                            END), ''),
                        
                        /* Doação ou evento */
                        IF(tt.tb_diretorio_origem IN (SELECT id 
                                                        FROM tb_diretorio 
                                                        WHERE tb_diretorio.nome LIKE '%Diretório Municipal%' 
                                                        OR tb_diretorio.nome LIKE '%Núcleo Municipal%'),                    
                        IFNULL(REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ','), ''), '')
                            
                    ) AS rateio_dm, 
                    /* END: RATEIO DM */
                    
                    /* BEGIN: RATEIO DE */
                    IF(tt.origem_transacao IN(2, 8),
                        /* Filiação ou plano de contribuição */
                        IFNULL(
                            (CASE 
                                WHEN tp.titulo_eleitoral_uf_id NOT IN (7,2)
                                AND tp.titulo_eleitoral_municipio_id 
                                    IN(SELECT tb_cidade_id 
                                        FROM tb_diretorio 
                                        WHERE tb_diretorio.nome like '%Diretório Municipal%' 
                                        OR tb_diretorio.nome LIKE '%Núcleo Municipal%') 
                                THEN REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas * 0.30, 2), '.', '@'), ',', '.'), '@', ',')
                                    
                                WHEN tp.titulo_eleitoral_uf_id IN (7,2) 
                                || tp.titulo_eleitoral_municipio_id NOT IN(SELECT tb_cidade_id 
                                                                                FROM tb_diretorio 
                                                                                WHERE tb_diretorio.nome LIKE '%Diretório Municipal%' 
                                                                                OR tb_diretorio.nome LIKE '%Núcleo Municipal%')
                                THEN REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas * 0.90, 2), '.', '@'), ',', '.'), '@', ',')
                                
                                ELSE ''
                            END), ''),
                            
                        /* Doação ou evento */
                        IF(tt.tb_diretorio_origem IN (SELECT id 
                                                        FROM tb_diretorio 
                                                        WHERE tb_diretorio.nome LIKE '%Diretório Estadual%' 
                                                        OR tb_diretorio.nome LIKE '%Núcleo Estadual%' 
                                                        OR tb_diretorio.nome LIKE '%Diretório Regional%'),                    
                        IFNULL(REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ','), ''), '')
                                
                    ) AS rateio_de,
                    /* END: RATEIO DE */
                    
                    /* BEGIN: RATEIO DN */     
                    IF(tt.origem_transacao IN(2, 8),
                        /* Filiação ou plano de contribuição */
                        IFNULL(
                            (CASE 
                                WHEN tp.titulo_eleitoral_municipio_id 
                                    IN(SELECT tb_cidade_id 
                                        FROM tb_diretorio 
                                        WHERE tb_diretorio.nome LIKE '%Diretório Municipal%' 
                                        OR tb_diretorio.nome LIKE '%Núcleo Municipal%') 
                                THEN REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas * 0.10, 2), '.', '@'), ',', '.'), '@', ',')
                                    
                                WHEN tp.titulo_eleitoral_municipio_id 
                                    NOT IN(SELECT tb_cidade_id 
                                            FROM tb_diretorio 
                                            WHERE tb_diretorio.nome LIKE '%Diretório Municipal%' 
                                            OR tb_diretorio.nome LIKE '%Núcleo Municipal%')
                                THEN REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas * 0.10, 2), '.', '@'), ',', '.'), '@', ',')
                                
                                ELSE REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ',')
                            END), ''),
                            
                        /* Doação ou evento */
                        IF(tt.tb_diretorio_origem IS NULL 
                            || tt.tb_diretorio_origem IN (0, 1) 
                            || tt.tb_diretorio_origem IN (SELECT id FROM tb_diretorio WHERE tb_diretorio.nome like '%Comissão%'), 
                            IFNULL(REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ','), ''), '')
                        
                    ) AS rateio_dn,
                    /* BEGIN: RATEIO DN */
                    
                    (CASE tt.forma_pagamento
                        WHEN 1 THEN 'Cartão de Crédito'
                        WHEN 2 THEN 'Boleto'
                        WHEN 3 THEN 'TED'
                        WHEN 4 THEN 'DOC'
                        WHEN 5 THEN 'Transferência Online'
                        WHEN 6 THEN 'Depósito Oline'
                        WHEN 7 THEN 'Depósito'
                        WHEN 8 THEN 'Cheque'
                        WHEN 9 THEN 'Tranferência'
                        WHEN 10 THEN 'Cartão de Débito'
                        WHEN 11 THEN 'PIX'
                        ELSE ''
                    END) AS pagamento,
                    
                    (CASE tt.periodicidade
                        WHEN 1 THEN 'Mensal'
                        WHEN 12 THEN 'Anual'
                        WHEN 24 THEN 'Bienal'
                        ELSE ''
                    END) AS plano,
                    
                    (CASE tt.origem_transacao
                        WHEN 1 THEN 'Doação'
                        WHEN 2 THEN 'Filiação'
                        WHEN 3 THEN 'Doação Campanha'
                        WHEN 4 THEN 'Evento'
                        WHEN 5 THEN 'Complemento de Filiação'
                        WHEN 6 THEN 'Doação Maquininha'
                        WHEN 7 THEN 'Processo Seletivo'
                        WHEN 8 THEN 'Plano de contribuição'
                        ELSE ''
                    END) AS origem,
                    
                    (CASE tt.gateway_pagamento
                        WHEN 1 THEN 'Iugu'
                        WHEN 2 THEN 'Rede'
                        WHEN 3 THEN 'Cielo'
                        WHEN 4 THEN 'Braspag'
                        WHEN 5 THEN 'Banco do Brasil'
                        ELSE ''
                    END) AS gateway

                FROM tb_transacoes tt

                LEFT JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                LEFT JOIN tb_estado es1 ON es1.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci1 ON ci1.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id
                LEFT JOIN tb_estado es2 ON es2.id = en.tb_estado_id
                LEFT JOIN tb_cidade ci2 ON ci2.id = en.tb_cidade_id
                LEFT JOIN tb_diretorio d ON d.id = tt.tb_diretorio_id
                LEFT JOIN tb_diretorio dir2 ON dir2.id = tt.tb_diretorio_origem
                LEFT JOIN tb_eventos_transacoes tet ON tet.tb_transacao_id = tt.id
                LEFT JOIN tb_eventos te ON te.id = tet.tb_eventos_id

                WHERE tt.status = 'paid' {$where}
                GROUP BY tt.invoice_id
                ORDER BY tt.data_pago ASC";

        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
    public function getDateLastTransaction($id)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes WHERE tb_pessoa_id = :id AND origem_transacao = :origin AND periodicidade != :plan ORDER BY data_criacao DESC LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':origin' => 2, ':plan' => 0]);
        return $rows->fetchAssociative();
    }

    public function sumValueDaily($day)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT SUM(t.valor) AS value FROM tb_transacoes t WHERE DATE_FORMAT(t.data_pago, '%Y-%m-%d') = :day";
        $rows = $pdo->prepare($sql)->execute([':day' => $day]);
        return $rows->fetchAssociative();
    }

    public function listTransactionsRescue($rescue): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tt.id, tt.tb_pessoa_id, tp.nome as name, tp.cpf, tt.invoice_id,  tt.numero_recibo as receipt, DATE_FORMAT(tt.data_pago, '%d/%m/%Y') as paid_at, IFNULL(DATE_FORMAT(tt.data_deposito, '%d/%m/%Y'),'-') as deposit,
                    te.estado as state,  tc.cidade as city, REPLACE(REPLACE(REPLACE(FORMAT(tt.valor, 2), '.', '@'), ',', '.'), '@', ',') as 'valor_bruto', REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ',') as 'valor_s_taxas', d.nome AS directory,
                    (CASE `tt`.`status`
                        WHEN 'paid' THEN 'Pago'
                        WHEN 'expired' THEN 'Expirado'
                        WHEN 'pending' THEN 'Pendente'
                        WHEN 'canceled' THEN 'Cancelada'
                        WHEN 'Chargeback' THEN 'Chargeback'
                        WHEN 'refunded' THEN 'Reembolsado'
                        WHEN 'partially paid' THEN 'Pago Parcial'
                        WHEN 'accumulated' THEN 'Acumulado'
                        WHEN 'reversed' THEN 'Estornado'
                    END) as `status`,
                    (CASE `tt`.`origem_transacao`
                        WHEN 1 THEN 'Doação'
                        WHEN 2 THEN 'Filiação'
                        WHEN 3 THEN 'Doação Campanha'
                        WHEN 4 THEN 'Evento'
                        WHEN 5 THEN 'Complemento de Filiação'
                        WHEN 6 THEN 'Doação Maquininha'
                        WHEN 8 THEN 'Plano de Contribuição'
                        WHEN 15 THEN 'Filiação antecipada'
                    END) AS `origin`,
                    (CASE `tt`.`periodicidade`
                        WHEN 0 THEN 'Avulsa'
                        WHEN 1 THEN 'Mensal'
                        WHEN 6 THEN 'Semestral'
                        WHEN 12 THEN 'Anual'
                        WHEN 24 THEN 'Bienal'
                    END) AS `plan`,
                    (CASE `tt`.`forma_pagamento`
                        WHEN 1 THEN 'Cartão'
                        WHEN 2 THEN 'Boleto'
                        WHEN 3 THEN 'TED'
                        WHEN 4 THEN 'DOC'
                        WHEN 5 THEN 'Transf Online'
                        WHEN 6 THEN 'Deposit Online'
                        WHEN 7 THEN 'Deposito'
                        WHEN 8 THEN 'Cheque'
                        WHEN 9 THEN 'Transferência'
                        WHEN 10 THEN 'Cartão de Débito'
                        WHEN 11 THEN 'PIX'
                    END) AS `payment`, tt.rescue
                FROM tb_transacoes as tt 
                    LEFT JOIN tb_pessoa tp ON tt.tb_pessoa_id = tp.id
                    LEFT JOIN tb_endereco tben ON tben.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_diretorio d ON d.id = tt.tb_diretorio_origem
                WHERE tt.rescue = :rescue
                GROUP BY tt.id";
        $rows = $pdo->prepare($sql)->execute([':rescue' => $rescue]);
        return $rows->fetchAllAssociative();
    }

    public function findReceiptByNumber($data, $receipt)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT t.id, t.tb_pessoa_id, t.valor, DATE_FORMAT(t.data_pago, '%d/%m/%Y') AS data_pago, t.invoice_id, t.forma_pagamento, 
                td.nome_presidente, td.cpf_presidente, td.cnpj, td.nome AS nomeDiretorio, t.numero_recibo, tp.nome, tp.cpf,
                (CASE `t`.`forma_pagamento`
                        WHEN 1 THEN 'Cartão'
                        WHEN 2 THEN 'Boleto'
                        WHEN 3 THEN 'TED'
                        WHEN 4 THEN 'DOC'
                        WHEN 5 THEN 'Transf Online'
                        WHEN 6 THEN 'Deposit Online'
                        WHEN 7 THEN 'Deposito'
                        WHEN 8 THEN 'Cheque'
                        WHEN 9 THEN 'Transferência'
                        WHEN 10 THEN 'Cartão de Débito'
                        WHEN 11 THEN 'PIX'
                    END) AS `paymentString`
                FROM tb_transacoes t
                LEFT JOIN tb_diretorio td ON t.tb_diretorio_id = td.id
                LEFT JOIN tb_pessoa tp ON tp.id = t.tb_pessoa_id
                WHERE t.status = 'paid' AND t.numero_recibo != 'Em processamento' AND t.tb_diretorio_id = :directory AND RIGHT(numero_recibo, 6) = :receipt";
        $rows = $pdo->prepare($sql)->execute([':directory' => $data['directory'], ':receipt' => $receipt]);
        return $rows->fetchAllAssociative();
    }

    public function findReceiptByCPF($data)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT t.id, t.tb_pessoa_id, t.valor, DATE_FORMAT(t.data_pago, '%d/%m/%Y') AS data_pago, t.invoice_id, t.forma_pagamento, 
                td.nome_presidente, td.cpf_presidente, td.cnpj, td.nome AS nomeDiretorio, t.numero_recibo, RIGHT(numero_recibo, 6) AS reciboCurto, tp.nome, tp.cpf,
                (CASE `t`.`forma_pagamento`
                        WHEN 1 THEN 'Cartão'
                        WHEN 2 THEN 'Boleto'
                        WHEN 3 THEN 'TED'
                        WHEN 4 THEN 'DOC'
                        WHEN 5 THEN 'Transf Online'
                        WHEN 6 THEN 'Deposit Online'
                        WHEN 7 THEN 'Deposito'
                        WHEN 8 THEN 'Cheque'
                        WHEN 9 THEN 'Transferência'
                        WHEN 10 THEN 'Cartão de Débito'
                        WHEN 11 THEN 'PIX'
                    END) AS `paymentString`
                FROM tb_transacoes t
                LEFT JOIN tb_diretorio td ON t.tb_diretorio_id = td.id
                LEFT JOIN tb_pessoa tp ON tp.id = t.tb_pessoa_id
                WHERE tp.cpf = :cpf AND t.status = 'paid' AND t.numero_recibo != 'Em processamento' AND t.tb_diretorio_id = :directory AND YEAR(t.data_pago) = :year";
        $rows = $pdo->prepare($sql)->execute([':directory' => $data['directory'], ':cpf' => $data['cpf'], ':year' => $data['year']]);
        return $rows->fetchAllAssociative();
    }

    public function listConciliationReceivables(string $start, int $directory): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT SUM(valor_s_taxas) AS value
                FROM tb_transacoes 
                WHERE data_deposito LIKE :start AND gateway_pagamento = :gateway AND status = :status AND tb_diretorio_id = :directory";
        $rows = $pdo->prepare($sql)->execute([':start' => "%$start%", ':gateway' => 2, ':status' => 'paid', ':directory' => $directory]);
        return $rows->fetchAllAssociative();
    }

    public function getOriginatingTransactions(array $data, string $directory, float $beginValue = null, $endValue = null): array
    {
        $params = [];
        $where = $join = '';

        if ($beginValue) {
            $params[':beginValue'] = "$beginValue";
            $where .= " AND tt.valor_s_taxas >= :beginValue";
        }
        if ($endValue) {
            $params[':endValue'] = "$endValue";
            $where .= " AND tt.valor_s_taxas <= :endValue";
        }
        if ($directory != 1) {
            $join = "JOIN originatingOwner oo ON oo.transaction = ot.transaction";
            $params[':directory'] = $directory;
            $where .= " AND oo.directory = :directory";
        } else {
            $where .= " AND tt.id NOT IN (select transaction from originatingTransaction)";
        }

        $params[':dateTransfer'] = $data['dateTransfer'];
        $where .= " AND DATE_FORMAT(tt.data_pago, '%Y-%m-%d') <= :dateTransfer";

        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id AS person, DATE_FORMAT(tt.data_pago, '%d/%m/%Y') AS payDay, tp.nome AS name, tt.id AS transaction, tt.numero_recibo AS receipt, 
                    if(ts.newValue, ts.newValue, tt.valor_s_taxas) AS value, if(ts.newValue, 1, 0) AS validSplit, 
                    if(ts.newValue, CONCAT('Sim ( Valor Original R$ ', REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ','), ' )' ), 'Não') AS split,
                    CONCAT('R$ ' ,REPLACE(REPLACE(REPLACE(FORMAT(if(ts.newValue, ts.newValue, tt.valor_s_taxas), 2), '.', '@'), ',', '.'), '@', ',')) AS valueFormated, 
                    dir.nome AS directory
                FROM tb_pessoa tp
                LEFT JOIN tb_transacoes tt ON tp.id = tt.tb_pessoa_id
                LEFT JOIN transferSplit ts ON ts.transaction = tt.id
                LEFT JOIN tb_diretorio dir ON dir.id = tt.tb_diretorio_id
                LEFT JOIN originatingTransaction ot ON ot.transaction = tt.id
                {$join}
                WHERE numero_recibo != 'Em processamento' 
                    AND YEAR(data_pago) >= 2021 
                    AND tt.valor_s_taxas IS NOT NULL 
                    AND ts.transfer IS NULL 
                    AND tt.tb_diretorio_id IN (1, $directory) 
                    AND tp.permissionario = 0 AND observacao != 'Falecido' {$where}
                GROUP BY tt.id 
                ORDER BY FIELD(tt.tb_diretorio_id, '$directory', '1'), ts.newValue ASC, tt.data_pago ASC
                LIMIT 50000";

        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function getTransactionsIncluded(string $transaction): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id AS person, tp.nome AS name, tt.id AS transaction, tt.numero_recibo AS receipt, if(tt.valor_s_taxas, tt.valor_s_taxas, tt.valor) AS value, DATE_FORMAT(tt.data_pago, '%d/%m/%Y') AS payDay,
                if(ts.newValue, CONCAT('Sim ( Valor Original R$ ', REPLACE(REPLACE(REPLACE(FORMAT(tt.valor_s_taxas, 2), '.', '@'), ',', '.'), '@', ','), ' )' ), 'Não') AS split,
                CONCAT('R$ ' ,REPLACE(REPLACE(REPLACE(FORMAT(if(ts.newValue, ts.newValue, tt.valor_s_taxas), 2), '.', '@'), ',', '.'), '@', ',')) AS valueFormated
                FROM tb_pessoa  tp
                LEFT JOIN tb_transacoes tt ON tp.id = tt.tb_pessoa_id
                LEFT JOIN transferSplit ts ON ts.transaction = tt.id
                WHERE tt.id IN($transaction)
                GROUP BY tt.id";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function lastDirectoryReceipt($directory)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT LPAD(SUBSTRING(numero_recibo, -6), 6, 0) AS receipt 
                FROM tb_transacoes 
                WHERE numero_recibo LIKE 'P%' AND status = 'paid' AND numero_recibo IS NOT NULL AND tb_diretorio_id = :directory
                ORDER BY id DESC
                LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':directory' => $directory]);
        return $rows->fetchAllAssociative();
    }

    public function getTransactionsMonthlyByDirectory(Directory $directory): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT *
                FROM tb_transacoes tt
                WHERE tt.tb_diretorio_id = :directory AND DATE_FORMAT(tt.data_pago, '%Y-%m') = :date";
        $rows = $pdo->prepare($sql)->execute([':directory' => $directory->getId(), ':date' => date('Y-m')]);
        return $rows->fetchAllAssociative();
    }
}