<?php

namespace App\Models\Repository;

use App\Models\Entities\Directory;
use Doctrine\ORM\EntityRepository;

class DirectoryRepository extends EntityRepository
{
	public function save(Directory $entity): Directory
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}
}