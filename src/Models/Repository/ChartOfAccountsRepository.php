<?php

namespace App\Models\Repository;
use App\Helpers\Utils;
use App\Models\Entities\ChartOfAccounts;
use Doctrine\ORM\EntityRepository;

class ChartOfAccountsRepository extends EntityRepository
{
    public function save(ChartOfAccounts $entity):ChartOfAccounts
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($reduced = null, $surname = null, $account = null, $description = null, $type = null, $category = null, $specie = null, &$params): string
    {
        $where = '';
        if ($reduced) {
            $params[':reduced'] = "%$reduced%";
            $where .= " AND c.reduced LIKE :reduced";
        }
        if ($surname) {
            $params[':surname'] = "%$surname%";
            $where .= " AND c.surname LIKE :surname";
        }
        if ($account) {
            $params[':account'] = "%$account%";
            $where .= " AND c.account LIKE :account";
        }
        if ($description) {
            $params[':description'] = "%$description%";
            $where .= " AND c.description LIKE :description";
        }
        if ($type) {
            $params[':type'] = "$type";
            $where .= " AND c.type = :type";
        }
        if ($category) {
            $params[':category'] = "$category";
            $where .= " AND c.category = :category";
        }
        if ($specie) {
            $params[':specie'] = "$specie%";
            $where .= " AND c.description LIKE :specie";
        }
        return $where;
    }

    public function list($reduced = null, $surname = null, $account = null, $description = null, $type = null, $category = null, $specie = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($reduced, $surname, $account, $description, $type, $category, $specie, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, IFNULL(c.reduced, '-') AS reduced, IFNULL(c.surname, '-') AS surname, c.account, c.description, DATE_FORMAT(c.created_at, '%d/%m/%Y') AS created_at, c.active,
                (CASE c.type
                    WHEN 1 THEN 'Débito'
                    WHEN 2 THEN 'Crédito'
                END) AS 'type',
                (CASE c.category
                    WHEN 1 THEN 'Receita'
                    WHEN 2 THEN 'Despesa'
                END) AS 'category'
                FROM chartOfAccounts c
                WHERE 1 = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($reduced = null, $surname = null, $account = null, $description = null, $type = null, $category = null, $specie = null): array
    {
        $params = [];
        $where = $this->generateWhere($reduced, $surname, $account, $description, $type, $category, $specie, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(c.id)) AS total FROM chartOfAccounts c
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listExport($reduced = null, $surname = null, $account = null, $description = null, $type = null, $category = null, $specie = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($reduced, $surname, $account, $description, $type, $category, $specie, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, IFNULL(c.reduced, '-') AS reduced, IFNULL(c.surname, '-') AS surname, c.account, c.description, 
                DATE_FORMAT(c.created_at, '%d/%m/%Y') AS created_at, c.active,
                (CASE c.type
                    WHEN 1 THEN 'Débito'
                    WHEN 2 THEN 'Crédito'
                END) AS 'type',
                (CASE c.category
                    WHEN 1 THEN 'Receita'
                    WHEN 2 THEN 'Despesa'
                END) AS 'category'
                FROM chartOfAccounts c
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}