<?php

namespace App\Models\Repository;

use App\Models\Entities\AccountingRecord;
use Doctrine\ORM\EntityRepository;

class AccountingRecordRepository extends EntityRepository
{
    public function save(AccountingRecord $entity): AccountingRecord
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getAccounts(int $payment): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT IFNULL(CONCAT(c.account, ' / ', c.description), 'M') AS 'debit', IFNULL(CONCAT(cc.account, ' / ', cc.description), 'M') AS 'credit',
                    (CASE 
                        WHEN aa.value IS NULL THEN REPLACE(REPLACE(REPLACE(FORMAT(aa.creditValue, 2), '.', '@'), ',', '.'), '@', ',')
                        WHEN aa.creditValue IS NULL THEN REPLACE(REPLACE(REPLACE(FORMAT(aa.value, 2), '.', '@'), ',', '.'), '@', ',')
                        ELSE REPLACE(REPLACE(REPLACE(FORMAT(LEAST(aa.value, aa.creditValue), 2), '.', '@'), ',', '.'), '@', ',')
                    END) AS 'value', ar.formAccounting
                FROM accountingRecord ar
                LEFT JOIN accountingAccounts aa ON aa.accountingRecord = ar.id
                LEFT JOIN chartOfAccounts c ON c.id = aa.account
                LEFT JOIN chartOfAccounts cc ON cc.id = aa.creditAccount
                WHERE payment = :payment AND formAccounting != :formAccounting";
        $rows = $pdo->prepare($sql)->execute([':payment' => $payment, ':formAccounting' => 0]);
        return $rows->fetchAllAssociative();
    }
}