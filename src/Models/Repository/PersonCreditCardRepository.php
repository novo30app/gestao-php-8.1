<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonCreditCard;
use Doctrine\ORM\EntityRepository;

class PersonCreditCardRepository extends EntityRepository
{
    public function save(PersonCreditCard $entity): PersonCreditCard
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}