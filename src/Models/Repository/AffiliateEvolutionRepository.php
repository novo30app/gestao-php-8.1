<?php

namespace App\Models\Repository;

use App\Models\Entities\AffiliateEvolution;
use Doctrine\ORM\EntityRepository;

class AffiliateEvolutionRepository extends EntityRepository
{
    public function save(AffiliateEvolution $entity): AffiliateEvolution
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function deleteByPeriod(string $period): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM affiliateEvolution WHERE period = :period ";
        $sth = $pdo->prepare($sql);
        $sth->execute([':period' => $period]);
    }

    public function evolutionByPeriodAndLocation(string $period, int $uf, int $city): array
    {
        $params = [];
        $where = $this->generateWhere($params, $period, $uf, $city);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM affiliateEvolution 
                WHERE 1 = 1 {$where} LIMIT 1";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhere(&$params, $period, $uf = null, $city = null)
    {
        $where = '';
        $params[':uf'] = $uf;
        $where .= " AND state = :uf";
        $params[':city'] = $city;
        $where .= " AND city = :city";
        $params[':period'] = $period;
        $where .= " AND period = :period";
        return $where;
    }
}