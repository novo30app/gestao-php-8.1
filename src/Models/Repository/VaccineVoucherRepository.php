<?php

namespace App\Models\Repository;

use App\Models\Entities\User;
use App\Models\Entities\Events;
use App\Models\Entities\vaccineVoucher;
use Doctrine\ORM\EntityRepository;

class VaccineVoucherRepository extends EntityRepository
{
    public function save(vaccineVoucher $entity):vaccineVoucher
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}