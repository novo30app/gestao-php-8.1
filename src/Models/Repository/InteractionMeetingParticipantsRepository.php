<?php

namespace App\Models\Repository;

use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionMeetingParticipant;
use Doctrine\ORM\EntityRepository;

class InteractionMeetingParticipantsRepository extends EntityRepository
{
    public function save(InteractionMeetingParticipant $entity): InteractionMeetingParticipant
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function dropByInteraction(Interaction $interaction)
    {
        $sql = "DELETE FROM interactionMeetingParticipants WHERE interaction = :interaction";
        $params = [':interaction' => $interaction->getId()];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }
}