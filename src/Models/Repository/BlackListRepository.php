<?php

namespace App\Models\Repository;

use App\Models\Entities\BlackList;
use Doctrine\ORM\EntityRepository;

class BlackListRepository extends EntityRepository
{
    public function save(BlackList $entity):BlackList
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}