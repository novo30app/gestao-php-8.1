<?php

namespace App\Models\Repository;

use App\Models\Entities\OmbudsmanCategory;
use Doctrine\ORM\EntityRepository;

class OmbudsmanCategoryRepository extends EntityRepository
{
    public function save(OmbudsmanCategory $entity) {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}