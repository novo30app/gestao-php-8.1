<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\AdminAccess;
use Doctrine\ORM\EntityRepository;

class AdminAccessRepository extends EntityRepository
{
    public function save(Events $entity):AdminAccess
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}