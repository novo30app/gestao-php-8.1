<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsPress;
use Doctrine\ORM\EntityRepository;

class EventsPressRepository extends EntityRepository
{
    public function save(EventsPress $entity):EventsPress
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, $vehicle = null, $phone = null, $type = null, $eventId = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND ep.reporter LIKE :name";
        }
        if ($vehicle) {
            $params[':vehicle'] = "%$vehicle%";
            $where .= " AND ep.vehicle LIKE :vehicle";
        }
        if ($phone) {
            $params[':phone'] = "%$phone%";
            $where .= " AND ep.phone LIKE :phone";
        }
        if ($type == 1) {
            $params[':type'] = 1;
            $where .= " AND ep.camera = :type";
        } else if($type == 2) {
            $params[':type'] = 1;
            $where .= " AND ep.photographer = :type"; 
        }
        if ($eventId) {
            $params[':eventId'] = "$eventId";
            $where .= " AND ep.eventsId = :eventId";
        }
        return $where;
    }

    public function list($name = null, $vehicle = null, $phone = null, $type = null, $eventId = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $vehicle, $phone, $type, $eventId, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ep.reporter, ep.vehicle, ep.phone, ep.camera, ep.photographer, DATE_FORMAT(ep.created, '%d/%m/%Y %H:%i:%s') AS created
                FROM eventsPress ep
                WHERE 1 = 1 {$where}
                GROUP BY ep.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null, $vehicle = null, $phone = null, $type = null, $eventId = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $vehicle, $phone, $type, $eventId, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(ep.id)) AS total 
                FROM eventsPress ep
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}