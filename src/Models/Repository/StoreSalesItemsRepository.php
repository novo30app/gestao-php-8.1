<?php

namespace App\Models\Repository;

use App\Models\Entities\StoreSalesItems;
use Doctrine\ORM\EntityRepository;

class StoreSalesItemsRepository extends EntityRepository
{
    public function save(StoreSalesItems $entity):StoreSalesItems
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}