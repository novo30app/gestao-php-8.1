<?php

namespace App\Models\Repository;

use App\Models\Entities\Knowledge;
use Doctrine\ORM\EntityRepository;

class KnowledgeRepository extends EntityRepository
{
    public function save(Knowledge $entity):Knowledge
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, $state = null, $city = null)
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE '%$name%'";
        }
        if($user->getLevel() == 2){
            $state = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = $state";
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id = $city";
            } 
        } else if ($user->getLevel() == 1){
            $state = $user->getState()->getId();
            $city = $user->getCity()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = $state AND tp.titulo_eleitoral_municipio_id = $city";
        } else {
            if ($state) {
                $params[':state'] = "$state";
                $where .= " AND tp.titulo_eleitoral_uf_id LIKE $state";
            }
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id LIKE $city";
            }
        }
        return $where;
    }

    public function list($user, $name = null, $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.nome as name,     
                    CASE tc.type
                        WHEN 1 THEN 'Fui indicado por um filiado/candidato/mandatário'
                        WHEN 2 THEN 'Conheci pelas midias sociais'
                        WHEN 3 THEN 'Participei de um evento do NOVO'
                        WHEN 4 THEN 'Conheci pelo site do novo.org.br'
                        WHEN 5 THEN 'Li matéria do NOVO em jornais e revistas'
                        WHEN 6 THEN 'Outros'
                        ELSE tc.type 
                    END as type,       
                    if(tc.subtype is null,'',tc.subtype) as subtype, te.sigla as uf, tci.cidade as city, if(tc.evento is null, '',tc.evento)  as event, 
                    if(tc.filiado_id is null,'', tc.filiado_id) as filiated_id, if(tc.content is null,'', tc.content) as content, DATE_FORMAT(tc.created_at, '%d/%m/%Y') as created_at
                FROM tb_como_conheceu AS tc
                LEFT JOIN tb_pessoa tp ON tp.id = tc.tb_pessoa_id
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tci ON tci.id = tp.titulo_eleitoral_municipio_id 
                WHERE 1 = 1 AND tp.nome IS NOT NULL {$where}
                GROUP BY tc.id ORDER BY tc.created_at DESC {$limitSql};";         
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($user, $name = null, $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tc.id)) AS total 
                FROM tb_como_conheceu AS tc
                LEFT JOIN tb_pessoa tp ON tp.id = tc.tb_pessoa_id
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tci ON tci.id = tp.titulo_eleitoral_municipio_id 
                WHERE tc.tb_pessoa_id IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}