<?php

namespace App\Models\Repository;

use Doctrine\ORM\EntityRepository;
use App\Models\Entities\DirectoryAreaCommunicated;

class DirectoryAreaCommunicatedRepository extends EntityRepository
{
    public function save(DirectoryAreaCommunicated $entity): DirectoryAreaCommunicated
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function list($page): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM directoryAreaCommunicated WHERE page = :page ORDER BY theme + 0 ASC";
        $rows = $pdo->prepare($sql)->execute([':page' => $page]);
        return $rows->fetchAllAssociative();
    }
	
	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}
	
	private function generateWhere(array $filter, &$params): string
	{
		$where = '';
		if ($filter['title']) {
			$title = $filter['title'];
			$params[':title'] = "%$title%";
			$where .= " AND directoryAreaCommunicated.theme LIKE :title";
		}
		return $where;
	}
	
	public function listStore(array $filter, $limit = null, $offset = null): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT *
				FROM directoryAreaCommunicated
				WHERE directoryAreaCommunicated.page = 'store' {$where}
			    {$limitSql} ORDER BY id DESC";
		
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function listTotalStore(array $filter): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(directoryAreaCommunicated.id) AS total
                FROM directoryAreaCommunicated
                WHERE directoryAreaCommunicated.page = 'store' {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}
}