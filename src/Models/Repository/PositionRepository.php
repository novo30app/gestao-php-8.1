<?php

namespace App\Models\Repository;

use App\Models\Entities\Position;
use Doctrine\ORM\EntityRepository;

class PositionRepository extends EntityRepository
{
    public function save(Position $entity): Position
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}
