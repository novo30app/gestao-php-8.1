<?php

namespace App\Models\Repository;

use App\Models\Entities\TransferSplit;
use Doctrine\ORM\EntityRepository;

class TransferSplitRepository extends EntityRepository
{
    public function save(TransferSplit $entity): TransferSplit
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}