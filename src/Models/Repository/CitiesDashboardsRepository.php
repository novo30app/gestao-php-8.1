<?php

namespace App\Models\Repository;

use App\Models\Entities\CitiesDashboards;
use Doctrine\ORM\EntityRepository;

class CitiesDashboardsRepository extends EntityRepository
{
    public function save(CitiesDashboards $entity):CitiesDashboards
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}