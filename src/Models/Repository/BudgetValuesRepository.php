<?php

namespace App\Models\Repository;

use App\Models\Entities\Budget;
use App\Models\Entities\BudgetValues;
use Doctrine\ORM\EntityRepository;

class BudgetValuesRepository extends EntityRepository
{
    public function save(BudgetValues $entity): BudgetValues
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function deleteAll(Budget $budget): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM budgetValues
                WHERE budget = :id";
        $rows = $pdo->prepare($sql)->execute([':id' => $budget->getId()]);
    }
}
