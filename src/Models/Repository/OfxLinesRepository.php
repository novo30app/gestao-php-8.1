<?php

namespace App\Models\Repository;

use App\Models\Entities\DirectoryAccounts;
use App\Models\Entities\OfxLines;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class OfxLinesRepository extends EntityRepository
{
    public function save(OfxLines $entity): OfxLines
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function generateWhere(array $filter, &$params): string
    {
        $where = '';

        if ($filter['directory']) {
            $params[':directory'] = $filter['directory'];
            $where .= ' AND d.nome = :directory';
        }

        if ($filter['directoryAccount']) {
            $params[':directoryAccount'] = $filter['directoryAccount'];
            $where .= ' AND file.directoryAccount = :directoryAccount';
        }
        $start = \DateTime::createFromFormat('d/m/Y', $filter['start']);
        if ($start) {
            $params[':start'] = $start->format('Y-m-d');
            $where .= ' AND o.date >= :start';
        }
        $end = \DateTime::createFromFormat('d/m/Y', $filter['end']);
        if ($end) {
            $params[':end'] = $end->format('Y-m-d');
            $where .= ' AND o.date <= :end';
        }
        if ($filter['status'] == 1) {
            $where .= ' AND (o.requirements IS NULL AND o.refund IS NULL)';
        } elseif ($filter['status'] == 2) {
            $where .= ' AND (o.requirements IS NOT NULL OR o.refund IS NOT NULL)';
        }
        if ($filter['ofx']) {
            $params[':ofx'] = $filter['ofx'];
            $where .= ' AND o.ofx = :ofx';
        }

        return $where;
    }

    public function list($filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT o
                FROM  App\Models\Entities\OfxLines as o
                LEFT JOIN o.directory AS d
                LEFT JOIN o.ofx AS file
                WHERE o.category = 0 {$where}
                ORDER BY o.date DESC, o.value DESC
               ")
            ->setMaxResults($filter['limit'])
            ->setFirstResult($filter['index'] * $filter['limit'])
            ->execute($params);
    }

    public function total($filter): int
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(DISTINCT(o.id))
                FROM  App\Models\Entities\OfxLines as o
                LEFT JOIN o.directory AS d
                LEFT JOIN o.ofx AS file
                WHERE o.category = 0 {$where}
                ORDER BY o.id DESC
               ")
            ->execute($params)[0][1];
    }

    public function getInitialBalance(DirectoryAccounts $account, \DateTime $start, \DateTime $end): array
    {
        $values = $this->getEntityManager()->createQuery(
            "SELECT SUM(o.value) AS value, o.category 
                 FROM  App\Models\Entities\OfxLines AS o               
                 WHERE o.directoryAccount = :account AND o.date > :start AND o.date < :end
                 GROUP BY o.category")
            ->execute([':account' => $account, ':start' => $start->format('Y-m-d'), ':end' => $end->format('Y-m-d')]);
        $result = [];
        foreach ($values as $value) {
            $result[(int)$value['category']] = $value['value'];
        }
        return $result;
    }

    public function getTotalBalanceByPeriod(DirectoryAccounts $account, \DateTime $start, bool|\DateTime $end, array $filter): array
    {
        $params = [':account' => $account, ':start' => $start->format('Y-m-d')];
        $where = $this->generateWhereExtract($end, $filter, $params);
        $values = $this->getEntityManager()->createQuery(
            "SELECT SUM(o.value) AS value, o.category 
                 FROM  App\Models\Entities\OfxLines AS o               
                 WHERE o.directoryAccount = :account AND o.date >= :start {$where} 
                 GROUP BY o.category")
            ->execute($params);
        $result = [];
        foreach ($values as $value) {
            $result[(int)$value['category']] = $value['value'];
        }
        return $result;
    }

    public function getBalanceByPeriod(DirectoryAccounts $account, \DateTime $start, bool|\DateTime $end, array $filter = []): array
    {
        $params = [':account' => $account, ':start' => $start->format('Y-m-d')];
        $where = $this->generateWhereExtract($end, $filter, $params);

        return $this->getEntityManager()->createQuery(
            "SELECT o.id, o.date, o.value, o.category, o.description, o.extractNumber, o.uniqueIdentifier, o.type, 
                    IDENTITY(o.requirements) as payment, IDENTITY(o.ofxFiles) as ofx,
                    (SELECT MAX(a.id) FROM  App\Models\Entities\AccountingRecord AS a WHERE a.payment = o.requirements AND a.formAccounting = 1 AND a.competition IS NOT NULL) AS ar
                 FROM  App\Models\Entities\OfxLines AS o               
                 WHERE o.directoryAccount = :account AND o.date >= :start {$where} 
                 ORDER BY o.date ASC")
            ->execute($params);
    }


    private function generateWhereExtract(bool|\DateTime $end, array $filter = [], &$params = []): string
    {
        $where = '';
        if ($end) {
            $params[':end'] = $end->format('Y-m-d');
            $where .= ' AND o.date <= :end ';
        }
        if ($filter['conciliate'] != '' && $filter['conciliate'] == 0) {
            $where .= ' AND o.requirements IS NULL ';
        }
        if ($filter['conciliate'] != '' && $filter['conciliate'] == 1) {
            $where .= ' AND o.requirements IS NOT NULL ';
        }
        if ($filter['accounted'] != '' && $filter['accounted'] == 0) {
            $where .= ' AND (SELECT COUNT(a1.id) FROM  App\Models\Entities\AccountingRecord AS a1 WHERE a1.payment = o.requirements 
                AND a1.formAccounting = 1 AND a1.competition IS NOT NULL) = 0 ';
        }
        if ($filter['accounted'] != '' && $filter['accounted'] == 1) {
            $where .= ' AND (SELECT COUNT(a1.id) FROM  App\Models\Entities\AccountingRecord AS a1 WHERE a1.payment = o.requirements 
                AND a1.formAccounting = 1 AND a1.competition IS NOT NULL) > 0 ';
        }

        return $where;
    }

    public function reversals(OfxLines $ofx)
    {
        $date2 = clone $ofx->getDate();
        $params = [
            ':date1' => $ofx->getDate()->format('Y-m-d'),
            ':date2' => $date2->add(new \DateInterval('P1D'))->format('Y-m-d'),
            ':value' => $ofx->getValue(),
            ':directoryAccount' => $ofx->getDirectoryAccount()->getId(),
            ':category' => 1,
        ];
        return $this->getEntityManager()->createQuery(
            "SELECT o
                 FROM  App\Models\Entities\OfxLines AS o               
                 WHERE o.directoryAccount = :directoryAccount AND o.date BETWEEN :date1 AND :date2 AND o.category = :category AND o.value = :value 
                 AND (o.description LIKE '%DEVOLVIDA%' OR o.description LIKE '%REJEITADO%')
                 AND (SELECT COUNT(o1.id) FROM App\Models\Entities\OfxLines AS o1 WHERE o1.refund = o.id) = 0")
            ->execute($params);

    }


}
