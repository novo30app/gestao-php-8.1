<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\Address;
use App\Models\Entities\User;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class UsersRepository extends EntityRepository
{
    public function save(User $entity): User
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateAccess(UserAdmin $user)
    {
        $condition = '';
        switch ($user->getLevel()) {
            case 1:
                $condition = " AND titulo_eleitoral_municipio_id IN 
                                (SELECT access FROM accessAdmin WHERE userAdmin = {$user->getId()} and type = 'city'
                                    UNION ALL
                                SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = {$user->getId()}))";
                break;
            case 2:
                $condition = " AND titulo_eleitoral_uf_id IN (SELECT state FROM usersAdmin WHERE id = {$user->getId()})";
                break;
        }
        return $condition;
    }

    public function appointmentAffiliated()
    {
        $date = new \DateTime();
        $dateLimit = clone $date;
        $date = $date->format('Y-m-d');
        $dateLimit->sub(new \DateInterval('P1M'));
        $dateLimit = $dateLimit->format('Y-m-d');
        $params = [
            ':date1' => $dateLimit,
            ':date2' => $date,
        ];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tb_pessoa.id, tb_pessoa.nome, tb_pessoa.data_filiacao 
                FROM tb_pessoa
	            WHERE tb_pessoa.filiado = 7 AND tb_pessoa.data_filiacao BETWEEN :date1 AND :date2 AND 
                        (SELECT count(appointments.ID) from appointments WHERE tb_pessoa.ID=appointments.AFFILIATED AND appointments.STATUS IN (1, 2)) = 0 
                ORDER BY nome ASC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhere(UserAdmin $user, $name = null, $cpf = null, $email = null, $state = null, $city = null, $state2 = null, $city2 = null, $status = null,
                                             $gender = null, $typeDate = null, $mesoregion = null, $ageBegin = null, $ageEnd = null, $situation = null, $begin = null, $end = null, $affiliateId = null,
                                             $irpf = null, $exemption = null, &$params)
    {
        $where = '';
        $groupedStatus = ['7' => '7,8', '1' => '1,3', '9' => '9,10,11,12', '2' => '2,14', '0' => '0,5'];
        if ($irpf) {
            if ($irpf == '-1') $irpf = 0;
            $params[':irpf'] = $irpf;
            $where .= " AND tp.irpf = :irpf";
        }
        if ($exemption) {
            if ($exemption == '-1') $exemption = 0;
            $params[':exemption'] = $exemption;
            $where .= " AND tp.exemption = :exemption";
        }
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($cpf) {
            $params[':cpf'] = "$cpf";
            $where .= " AND tp.cpf = :cpf";
        }
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND tp.email LIKE :email";
        }
        if ($state) {
            $params[':uf'] = $state;
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($state2) {
            $params[':state2'] = $state2;
            $where .= " AND en.tb_estado_id = :state2";
        }
        if ($city2) {
            $params[':city2'] = $city2;
            $where .= " AND en.tb_cidade_id = :city2";
        }
        if ($status != '') {
            if (array_key_exists($status, $groupedStatus)) {
                $where .= " AND tp.filiado IN ({$groupedStatus[$status]})";
            } else {
                $params[':status'] = $status;
                $where .= " AND tp.filiado = :status";
            }
        }
        if ($gender) {
            $params[':gender'] = $gender;
            $where .= ' AND tp.genero = :gender';
        }
        if ($begin) {
            $params[':begin'] = "$begin";
            if ($typeDate == 'data_filiacao_refiliacao') {
                $where .= " AND (tp.data_filiacao >= :begin || tp.data_refiliacao >= :begin)";
            } else {
                $where .= " AND tp.$typeDate >= :begin";
            }
        }
        if ($end) {
            $params[':end'] = "$end";
            if ($typeDate == 'data_filiacao_refiliacao') {
                $where .= " AND (tp.data_filiacao <= :end || tp.data_refiliacao <= :end)";
            } else {
                $where .= " AND tp.$typeDate <= :end";
            }
        }
        if ($affiliateId) {
            $params[':affiliateId'] = $affiliateId;
            $where .= " AND tp.filiado_id = :affiliateId";
        }
        if ($mesoregion) {
            $params[':meso'] = $mesoregion;
            $where .= " AND tp.titulo_eleitoral_municipio_id IN (SELECT city FROM mesoregionsCities WHERE mesoregion = :meso)";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if (!$city) {
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (tp.titulo_eleitoral_municipio_id = :city || 
                    tp.titulo_eleitoral_municipio_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        }
        if ($ageBegin) {
            $params[':ageBegin'] = "$ageBegin";
            $where .= " AND TIMESTAMPDIFF(YEAR, tp.data_nascimento, CURDATE()) >= :ageBegin";
        }
        if ($ageEnd) {
            $params[':ageEnd'] = "$ageEnd";
            $where .= " AND TIMESTAMPDIFF(YEAR, tp.data_nascimento, CURDATE()) <= :ageEnd";
        }
        if ($situation) {
            if ($situation == 'Isento') {
                $where .= " AND tp.exemption = 1";
            } else {
                $params[':situation'] = $situation;
                $where .= " AND tp.exemption = 0 && IFNULL(
                            (SELECT IF(tbt.status = 'paid' , 'Adimplente', 'Inadimplente') AS 'Situacao'
                                FROM  tb_transacoes tbt
                                WHERE tbt.tb_pessoa_id = tp.id 
                                    AND tbt.status NOT IN ('accumulated' , 'canceled','reversed') 
                                    AND IF(tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                                    AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                                    ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                                ), 
                            'Inadimplente') = :situation ";
            }
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $cpf = null, $email = null, $state = null, $city = null, $state2 = null, $city2 = null, $status = null, $gender = null,
                                   $ordination = null, $mesoregion = null, $ageBegin = null, $ageEnd = null, $situation = null, $agroup = null, $typeDate = null, $begin = null, $end = null,
                                   $affiliateId = null, $irpf = null, $exemption = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $cpf, $email, $state, $city, $state2, $city2, $status, $gender, $typeDate, $mesoregion, $ageBegin, $ageEnd, $situation,
            $begin, $end, $affiliateId, $irpf, $exemption, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, IF(tp.filiado IN (7,8), tp.filiado_id, '-') AS affiliatedId, tp.nome AS name, IFNULL(state.sigla, '-') AS state, IFNULL(city.cidade, '-') AS city, 
                    IFNULL(DATE_FORMAT(tp.data_desfiliacao, '%d/%m/%Y'), '-') AS disaffiliation, 
                        CONCAT(TIMESTAMPDIFF(YEAR, tp.data_nascimento, CURDATE()), ' anos') AS age,
                        IF(tp.exemption = 1, 'Isento', 'Normal') AS exemption,
                        (CASE
                            WHEN tp.filiado = 7 THEN DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y')
                            WHEN tp.filiado = 8 THEN DATE_FORMAT(tp.data_refiliacao, '%d/%m/%Y')
                            WHEN tp.filiado = 9 AND tp.data_filiacao IS NOT NULL THEN DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y')
                            WHEN tp.filiado = 9 AND tp.data_filiacao IS NULL THEN DATE_FORMAT(tp.data_refiliacao, '%d/%m/%Y')
                            ELSE '-'
                        END) AS filiation,
                        (CASE
                            WHEN tp.filiado = 7 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                            WHEN tp.filiado = 8 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                            WHEN tp.filiado = 9 AND tp.data_refiliacao IS NOT NULL THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                            WHEN tp.filiado = 9 AND tp.data_refiliacao IS NULL THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                            ELSE '-'
                        END) AS filiationSolicitation,
                        ts.status AS 'status',
                        (SELECT 
                            CONCAT(tbtel.ddi,
                            SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                            SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                            SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                            FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3
                        LIMIT 1) AS phone
                FROM tb_pessoa AS tp
                    LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_endereco AS en ON en.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado AS state2 ON state2.id = en.tb_estado_id
                    LEFT JOIN tb_cidade AS city2 ON city2.id = en.tb_cidade_id
                    LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                    LEFT JOIN mesoregions me ON me.state = state.id
                    LEFT JOIN mesoregionsCities mec ON mec.mesoregion = me.id
                WHERE 1 = 1 {$where} 
                GROUP BY tp.id ORDER BY tp.id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $cpf = null, $email = null, $state = null, $city = null, $state2 = null, $city2 = null, $status = null, $gender = null,
                                        $ordination = null, $mesoregion = null, $ageBegin = null, $ageEnd = null, $situation = null, $agroup = null, $typeDate = null, $begin = null, $end = null, $affiliateId = null,
                                        $irpf = null, $exemption = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $cpf, $email, $state, $city, $state2, $city2, $status, $gender, $typeDate, $mesoregion, $ageBegin, $ageEnd, $situation,
            $begin, $end, $affiliateId, $irpf, $exemption, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total
                FROM tb_pessoa AS tp
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_endereco AS en ON en.tb_pessoa_id = tp.id
                LEFT JOIN tb_estado AS state2 ON state2.id = en.tb_estado_id
                LEFT JOIN tb_cidade AS city2 ON city2.id = en.tb_cidade_id
                LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                LEFT JOIN mesoregions me ON me.state = state.id
                LEFT JOIN mesoregionsCities mec ON mec.mesoregion = me.id
                WHERE 1= 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listExport(UserAdmin $user, $name = null, $cpf = null, $email = null, $state = null, $city = null, $state2 = null, $city2 = null, $status = null, $gender = null,
                                         $mesoregion = null, $ageBegin = null, $ageEnd = null, $situation = null, $typeDate = null, $begin = null, $end = null, $affiliateId = null, $irpf = null, $exemption = null,
                                         $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $cpf, $email, $state, $city, $state2, $city2, $status, $gender, $typeDate, $mesoregion, $ageBegin, $ageEnd, $situation, $begin, $end,
            $affiliateId, $irpf, $exemption, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $email = $user->getLevel() == 3 ? ', tp.email' : '';
        $sql = "SELECT tp.id, tp.filiado_id, tbd.status, 
                        (CASE tp.filiado
                            WHEN 7 THEN 'Filiado'
                            WHEN 8 THEN 'Refiliado'
                            WHEN 9 THEN 'Desfiliado'
                            ELSE 'Não Filiado'
                        END) AS 'status',
                    (CASE tbpa.periodicidade
                        WHEN 1 THEN 'Mensal'
                        WHEN 6 THEN 'Semestral'
                        WHEN 12 THEN 'Anual'
                    END) as plan, 
                    tp.data_criacao, tp.data_filiacao, tp.data_refiliacao, tp.data_desfiliacao, tp.data_solicitacao_filiacao, tp.data_solicitacao_refiliacao,
                    tp.nome as name, tbg.genero, tbesc.estadocivil,
                    (SELECT  concat(tbtel.ddi , concat('(',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,1,2),')',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,3,5),'',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ',''),8)))
                        FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3 LIMIT 1) AS phone, 
                    tp.data_nascimento, CONCAT(TIMESTAMPDIFF(YEAR, tp.data_nascimento, CURDATE()), ' anos') AS age, tp.cpf, tp.rg, IF(tp.pendencia_titulo = 1, 'Sim', 'Não') as pendencia_titulo, tp.titulo_eleitoral, 
                    state.sigla AS state, city.cidade AS city, tp.titulo_eleitoral_zona, tp.titulo_eleitoral_secao,
                    en.bairro, en.endereco, en.numero, en.complemento, en.cep, city2.cidade AS city2, state2.sigla AS state2, 
                    tp.motivo_desfiliacao, tp.descricao_desfiliacao, tp.profissao, m.name AS mesorregião, IF(tp.membro_diretorio = 1, 'Sim', 'Não')  {$email}
                FROM tb_pessoa AS tp
                    LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                    LEFT JOIN tb_endereco AS en ON en.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado AS state2 ON state2.id = en.tb_estado_id
                    LEFT JOIN tb_cidade AS city2 ON city2.id = en.tb_cidade_id
                    LEFT JOIN tb_pessoa_status_doador AS tbd ON tbd.id = tp.doador
                    LEFT JOIN tb_genero AS tbg ON tbg.id = tp.genero
                    LEFT JOIN tb_estadocivil as tbesc ON tbesc.id = tp.estado_civil
                    LEFT JOIN tb_pessoa_assinatura tbpa ON tbpa.tb_pessoa_id = tp.id AND tbpa.origem_transacao = 2
                    LEFT JOIN mesoregionsCities me ON me.city = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN mesoregions m ON m.id = me.mesoregion
                WHERE 1 = 1 {$where} GROUP BY tp.id";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhereGraphics(UserAdmin $user, &$params, $uf = null, $city = null, $status = null, $situation = null)
    {
        $where = '';
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':city2'] = $user->getCity()->getId();
            $where .= " AND tp.titulo_eleitoral_municipio_id IN ((SELECT access FROM accessAdmin WHERE userAdmin = {$user->getId()} AND type = 'city'), :city2)";
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        }
        if ($status) {
            $params[':status'] = $status;
            $where .= " AND tp.filiado = :status";
        }
        return $where;
    }

    public function graphicAffiliatedsByStatus(UserAdmin $user, $uf = null, $city = null, $status = null, $situation = null): array
    {
        $params = [];
        $where = $this->generateWhereGraphics($user, $params, $uf, $city, $status, $situation);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(tp.id) AS total, if(tp.filiado = 7, 'Filiado','Refiliado') as status
                FROM tb_pessoa AS tp
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN compliance AS cc ON cc.id = tp.id
                WHERE tp.filiado in (7,8) {$where}
                GROUP BY tp.filiado
                ORDER BY total DESC;";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function graphicAffiliatedsByState(UserAdmin $user, $uf = null, $city = null, $status = null, $situation = null): array
    {
        $params = [];
        $where = $this->generateWhereGraphics($user, $params, $uf, $city, $status, $situation);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(tp.id) AS total, state.sigla AS state
                FROM tb_pessoa AS tp
                JOIN tb_pessoa_status_filiado AS status ON status.id = tp.filiado
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN compliance AS cc ON cc.id = tp.id
                WHERE tp.filiado in (7,8) {$where}  
                GROUP BY tp.titulo_eleitoral_uf_id
                ORDER BY total DESC LIMIT 5;";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function graphicAffiliatedsByCity(UserAdmin $user, $uf = null, $city = null, $status = null, $situation = null): array
    {
        $params = [];
        $where = $this->generateWhereGraphics($user, $params, $uf, $city, $status, $situation);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(tp.id) AS total, city.cidade AS city
                FROM tb_pessoa AS tp
                JOIN tb_pessoa_status_filiado AS status ON status.id = tp.filiado
                LEFT JOIN tb_estado AS state ON state.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade AS city ON city.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN compliance AS cc ON cc.id = tp.id
                WHERE tp.filiado in (7,8) {$where}                
                GROUP BY tp.titulo_eleitoral_municipio_id                
                ORDER BY total DESC LIMIT 5 ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function tseFile(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT IF(filiado = 7, data_filiacao, data_refiliacao) AS data, nome, titulo_eleitoral
                FROM tb_pessoa 
                WHERE (filiado = 7 AND data_filiacao >= DATE_SUB(CURDATE(), INTERVAL 6 MONTH)) OR (filiado = 8 AND data_refiliacao >= DATE_SUB(CURDATE(), INTERVAL 6 MONTH))                
                ORDER BY data DESC";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    private function generateWhereSubmission(&$params, $affiliation = null, $disaffiliation = null)
    {
        $where = '';
        if ($affiliation) {
            $params[':affiliation'] = $affiliation->format('Y-m-d');
            $where .= " AND IF(filiado = 7, data_filiacao, data_refiliacao) >= :affiliation";
        }
        if ($disaffiliation) {
            $params[':disaffiliation'] = $disaffiliation->format('Y-m-d');
            $where .= " AND data_desfiliacao <= :disaffiliation";
        }
        return $where;
    }

    public function submission($affiliation = null, $disaffiliation = null): array
    {
        $params = [];
        $where = $this->generateWhereSubmission($params, $affiliation, $disaffiliation);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT id, IF(filiado = 7, data_filiacao, data_refiliacao) AS data, data_desfiliacao, nome, titulo_eleitoral, titulo_eleitoral_secao, titulo_eleitoral_zona
                FROM tb_pessoa 
                WHERE 1 = 1 {$where}               
                ORDER BY data DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function evolutionPlus(\DateTime $start, \DateTime $end, $uf = null, $city = null): int
    {
        $where = '';
        $params[':start'] = $start->format('Y-m-d');
        $params[':end'] = $end->format('Y-m-d');
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND titulo_eleitoral_uf_id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND titulo_eleitoral_municipio_id = :city";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ID) AS total FROM tb_pessoa 
                WHERE (filiado = 7 AND data_filiacao BETWEEN :start AND :end) OR 
                      (filiado = 8 AND data_refiliacao BETWEEN :start AND :end) {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return (int)$rows->fetchAssociative()['total'];
    }

    public function evolutionMinus(\DateTime $start, \DateTime $end, $uf = null, $city = null): int
    {
        $where = '';
        $params[':start'] = $start->format('Y-m-d');
        $params[':end'] = $end->format('Y-m-d');
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND titulo_eleitoral_uf_id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND titulo_eleitoral_municipio_id = :city";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ID) AS total FROM tb_pessoa 
                WHERE filiado in (9,10,11,12) AND data_desfiliacao between :start AND :end {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return (int)$rows->fetchAssociative()['total'];
    }

    public function evolutionTotal(\DateTime $start, \DateTime $end, $uf = null, $city = null): int
    {
        $where = '';
        $params[':start'] = $start->format('Y-m-d');
        $params[':end'] = $end->format('Y-m-d');
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND titulo_eleitoral_uf_id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND titulo_eleitoral_municipio_id = :city";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ID) AS total FROM tb_pessoa 
                WHERE (filiado in (9,10,11,12) AND data_desfiliacao between :start AND :end)
                    OR ((filiado = 7 AND data_filiacao < :start) OR 
                        (filiado = 8 AND data_refiliacao < :start)) {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return (int)$rows->fetchAssociative()['total'];
    }

    private function generateWhereSolicitations($name = null, $state = null, $city = null, $status = null, $type = null, $paymentForm = null, $plan = null,
                                                $dateFilter = null, $dateFilterBegin = null, $dateFilterEnd = null, &$params)
    {
        $where = '';
        if ($name) {
            $params[':name'] = $name;
            $where .= " AND tp.nome = :name";
        }
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($status) {
            $params[':filiado'] = $status;
            $where .= " AND tp.filiado = :filiado";
        }
        if ($type) {
            $type = $type == 2 ? 0 : $type;
            $params[':type'] = $type;
            $where .= " AND tp.exemption = :type";
        }
        if ($paymentForm) {
            $params[':paymentForm'] = $paymentForm;
            $where .= " AND ta.forma_pagamento = :paymentForm";
        }
        if ($plan) {
            $params[':plan'] = $plan;
            $where .= " AND ta.periodicidade = :plan";
        }
        if ($dateFilter && $dateFilterBegin && $dateFilterEnd) {
            $params[':dateFilterBegin'] = $dateFilterBegin;
            $params[':dateFilterEnd'] = $dateFilterEnd;
            if ($dateFilter == 1) {
                $where .= " AND (tp.data_solicitacao_filiacao BETWEEN :dateFilterBegin AND :dateFilterEnd ||
                                tp.data_solicitacao_refiliacao BETWEEN :dateFilterBegin AND :dateFilterEnd)";
            } else {
                $where .= " AND (tp.data_filiacao BETWEEN :dateFilterBegin AND :dateFilterEnd ||
                                tp.data_refiliacao BETWEEN :dateFilterBegin AND :dateFilterEnd)";
            }
        }
        return $where;
    }

    public function listSolicitations($name = null, $state = null, $city = null, $status = null, $type = null, $paymentForm = null, $plan = null,
                                      $dateFilter = null, $dateFilterBegin = null, $dateFilterEnd = null, $order = null, $orderType = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereSolicitations($name, $state, $city, $status, $type, $paymentForm, $plan, $dateFilter, $dateFilterBegin, $dateFilterEnd, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.nome AS name, tp.titulo_eleitoral AS title, te.estado AS uf, tc.cidade AS city, 
                    IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS exemption,
                    (CASE tp.filiado
                        WHEN 1 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 2 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 3 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                        WHEN 4 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 7 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 8 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                        WHEN 14 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                    END) AS solicitation,
                    (CASE tp.filiado
                        WHEN 7 THEN DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y')
                        WHEN 8 THEN DATE_FORMAT(tp.data_refiliacao, '%d/%m/%Y')
                        ELSE '-'
                    END) AS filiation,
                    (CASE tp.filiado 
                        WHEN 2 THEN 'Período Impugnação Filiação'
                        WHEN 4 THEN 'Impugnação solicitada'
                        WHEN 7 THEN 'Filiado'
                        WHEN 8 THEN 'Refiliado'
                        WHEN 14 THEN 'Período Impugnação Refiliação'      
                        WHEN 1 THEN 'Aguardando Pagamento Filiação'
                        WHEN 3 THEN 'Aguardando Pagamento Refiliação'          
                    END) as status,
                    (CASE ta.forma_pagamento
                        WHEN 2 THEN 'Boleto'
                        WHEN 1 THEN 'Cartão'
                        ELSE '-'
                    END) AS payment,
                    (CASE ta.periodicidade
                        WHEN 1 THEN 'Mensal'
                        WHEN 6 THEN 'Semestral'
                        WHEN 12 THEN 'Anual'
                        ELSE '-'
                    END) AS plan
                FROM tb_pessoa AS tp
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id AND ta.origem_transacao = 2
                WHERE tp.filiado in(1, 2, 3, 4, 7, 8, 14) {$where}
                GROUP BY tp.id ORDER BY {$order} {$orderType} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalSolicitations($name = null, $state = null, $city = null, $status = null, $type = null, $paymentForm = null,
                                           $plan = null, $dateFilter = null, $dateFilterBegin = null, $dateFilterEnd = null): array
    {
        $params = [];
        $where = $this->generateWhereSolicitations($name, $state, $city, $status, $type, $paymentForm, $plan, $dateFilter, $dateFilterBegin, $dateFilterEnd, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total 
                FROM tb_pessoa AS tp
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id AND ta.origem_transacao = 2
                WHERE tp.filiado IN(1, 2, 3, 4, 7, 8, 14) {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhereRecords($id = null, $affiliated_id = null, $name = null, $email = null, $cpf = null, $status = null, &$params)
    {
        $where = '';
        $groupedStatus = ['7' => '7,8', '8' => '8', '1' => '1,3', '9' => '9,10,11,12', '2' => '2,14', '0' => '0,5'];

        if ($id) {
            $params[':id'] = "%$id%";
            $where .= " AND tp.id LIKE :id";
        }
        if ($affiliated_id) {
            $params[':affiliated_id'] = "%$affiliated_id%";
            $where .= " AND tp.filiado_id LIKE :affiliated_id";
        }
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($email) {
            $params[':email'] = $email;
            $where .= " AND tp.email LIKE :email";
        }
        if ($cpf) {
            $params[':cpf'] = $cpf;
            $where .= " AND tp.cpf LIKE :cpf";
        }
        if ($status != '') {
            if (array_key_exists($status, $groupedStatus)) {
                $where .= " AND tp.filiado IN ({$groupedStatus[$status]})";
            } else {
                $params[':status'] = $status;
                $where .= " AND tp.filiado = :status";
            }
        }
        return $where;
    }

    public function listRecords($id = null, $affiliated_id = null, $name = null, $email = null, $cpf = null, $status = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereRecords($id, $affiliated_id, $name, $email, $cpf, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, IFNULL(tp.filiado_id,'-') as affiliated_id, tp.nome as name, tp.email, tp.cpf, IFNULL(DATE_FORMAT(tp.data_desfiliacao, '%d/%m/%Y'),'-') as date_disaffection,
                    (CASE tp.filiado 
                        WHEN 7 THEN 'Filiado'
                        WHEN 8 THEN 'Refiliado'
                        WHEN null THEN 'Cadastrado'
                        ELSE tpsf.status
                    END) as status,
                    (CASE tp.filiado
                        WHEN 8 THEN DATE_FORMAT(tp.data_refiliacao, '%d/%m/%Y')
                        WHEN 7 THEN DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y')
                        WHEN 9 THEN DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y')
                        ELSE '-'
                    END) as date_affiliated
                FROM tb_pessoa AS tp
                LEFT JOIN tb_pessoa_status_filiado tpsf on tpsf.id = tp.filiado 
                WHERE tp.nome IS NOT NULL {$where}
                GROUP BY tp.id ORDER BY tp.data_criacao DESC {$limitSql};";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalRecords($id = null, $affiliated_id = null, $name = null, $email = null, $cpf = null, $status = null): array
    {
        $params = [];
        $where = $this->generateWhereRecords($id, $affiliated_id, $name, $email, $cpf, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total 
                FROM tb_pessoa AS tp
                WHERE tp.nome IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listConventions(UserAdmin $user, $name = null, $cpf = null, $state = null, $city = null, $situation = null, $type = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereConventions($user, $name, $cpf, $state, $city, $situation, $type, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM (
                    /* ISENTOS */
                    SELECT  tp.id, tp.filiado_id, tp.cpf, IF(tp.filiado = 7, 'Filiado','Refiliado') as 'status', tp.nome AS 'name',
                        tbc.cidade AS 'city', tbe.sigla AS 'state', IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS type,
                        IF(tp.exemption = 1  OR (
                            (SELECT COUNT(0) 
                                FROM tb_transacoes tbtt 
                                WHERE tbtt.tb_pessoa_id = tp.id 
                                    AND tbtt.origem_transacao = 2 
                                    AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                    AND tbtt.status IN ('pending' , 'expired')
                                    AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                            ) = 0
                        ) AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante') AS 'situation',
                        IF(tp.exemption = 1, '0',
                            (SELECT COUNT(0) 
                                FROM tb_transacoes tbtt 
                                WHERE tbtt.tb_pessoa_id = tp.id 
                                AND tbtt.origem_transacao = 2
                                AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                AND tbtt.status IN ('pending' , 'expired')
                                AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                            )
                        ) AS 'diff'
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 1 {$where}
                    GROUP BY tp.id
                    /* ISENTOS */
                    UNION ALL
                    /* MENSAIS PAGANTES */
                    SELECT 
                        tp.id, 
                        tp.filiado_id, 
                        tp.cpf, 
                        IF(tp.filiado = 7, 'Filiado','Refiliado') as 'status', 
                        tp.nome AS 'name',
                        tbc.cidade AS 'city', 
                        tbe.sigla AS 'state', 
                        IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS type,
                        IF(tp.exemption = 1  OR (
                                                    (SELECT COUNT(0) 
                                                        FROM tb_transacoes tbtt 
                                                        WHERE tbtt.tb_pessoa_id = tp.id 
                                                        AND tbtt.origem_transacao IN (2, 8)
                                                        AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                                        AND tbtt.status IN ('pending' , 'expired')
                                                        AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                                                    ) = 0
                                            ) AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante') AS 'situation',
                        IF(tp.exemption = 1, '0',
                                                (SELECT COUNT(0) 
                                                    FROM tb_transacoes tbtt 
                                                    WHERE tbtt.tb_pessoa_id = tp.id 
                                                    AND tbtt.origem_transacao IN (2, 8)
                                                    AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                                    AND tbtt.status IN ('pending' , 'expired')
                                                    AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                                                )
                                        ) AS 'diff'
                                    
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND (tp.pendencia_titulo = 0 || tp.pendencia_titulo IS NULL) 
                    AND ta.periodicidade = 1 {$where}
                    GROUP BY tp.id
                    /* MENSAIS PAGANTES */
                    UNION ALL
                    /* ANUAIS PAGANTES */
                    SELECT 
                        tp.id, 
                        tp.filiado_id, 
                        tp.cpf, 
                        IF(tp.filiado = 7, 'Filiado','Refiliado') as 'status', 
                        tp.nome AS 'name',
                        tbc.cidade AS 'city', 
                        tbe.sigla AS 'state', 
                        IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS type,
                        IF(tp.exemption = 1  OR (
                                                    (SELECT status
														FROM tb_transacoes
														WHERE tb_pessoa_id = tp.id
														AND origem_transacao IN (2, 8)
														ORDER BY data_criacao DESC
														LIMIT 1
                                                    ) = 'paid'
                                            ) AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante') AS 'situation',
                        IF(tp.exemption = 1, '0',
                                                (SELECT COUNT(0) 
                                                    FROM tb_transacoes tbtt 
                                                    WHERE tbtt.tb_pessoa_id = tp.id 
                                                    AND tbtt.origem_transacao IN (2, 8)
                                                    AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                                    AND tbtt.status IN ('pending' , 'expired')
                                                    AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 12 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                                                )
                                        ) AS 'diff'
                                    
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND (tp.pendencia_titulo = 0 || tp.pendencia_titulo IS NULL) 
                    AND ta.periodicidade = 12 {$where}
                    GROUP BY tp.id
                    /* ANUAIS PAGANTES */
                    UNION ALL
                    /* MENSAIS PAGANTES COM PENDENCIA */
                    SELECT 
                    tp.id, 
                    tp.filiado_id, 
                    tp.cpf, 
                    IF(tp.filiado = 7, 'Filiado','Refiliado') as 'status', 
                    tp.nome AS 'name',
                    tbc.cidade AS 'city', 
                    tbe.sigla AS 'state', 
                    IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS type,
                    IF(tp.exemption = 1 OR (
                                                (SELECT COUNT(0) 
                                                    FROM tb_transacoes tbtt 
                                                    WHERE tbtt.tb_pessoa_id = tp.id 
                                                    AND tbtt.origem_transacao = 1 AND tbtt.pendencia_titulo IS NOT NULL 
                                                    AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                                    AND tbtt.status IN ('pending' , 'expired')
                                                    AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                                                ) = 0
                                            ) AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante') AS 'situation',

                        IF(tp.exemption = 1, '0',
                            (SELECT COUNT(0) 
                                FROM tb_transacoes tbtt 
                                WHERE tbtt.tb_pessoa_id = tp.id 
                                AND tbtt.origem_transacao = 1 AND tbtt.pendencia_titulo IS NOT NULL
                                AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                AND tbtt.status IN ('pending' , 'expired')
                                AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                                )
                            ) AS 'diff'
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND tp.pendencia_titulo = 1
                    AND ta.periodicidade = 1 {$where}
                    GROUP BY tp.id
                    /* MENSAIS PAGANTES COM PENDENCIA */
                    UNION ALL
                    /* ANUAIS PAGANTES COM PENDENCIA */
                    SELECT 
                    tp.id, 
                    tp.filiado_id, 
                    tp.cpf, 
                    IF(tp.filiado = 7, 'Filiado','Refiliado') as 'status', 
                    tp.nome AS 'name',
                    tbc.cidade AS 'city', 
                    tbe.sigla AS 'state', 
                    IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS type,
                    IF(tp.exemption = 1 OR (
                                                (SELECT status
													FROM tb_transacoes
													WHERE tb_pessoa_id = tp.id
													AND origem_transacao = 1
                                                    AND pendencia_titulo IS NOT NULL
													ORDER BY data_criacao DESC
													LIMIT 1
                                                ) = 'paid'
                                            ) AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante') AS 'situation',

                        IF(tp.exemption = 1, '0',
                            (SELECT COUNT(0) 
                                FROM tb_transacoes tbtt 
                                WHERE tbtt.tb_pessoa_id = tp.id 
                                AND tbtt.origem_transacao = 1 AND tbtt.pendencia_titulo IS NOT NULL
                                AND tbtt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) 
                                AND tbtt.status IN ('pending' , 'expired')
                                AND tbtt.data_criacao BETWEEN (CURDATE() - INTERVAL 12 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                                )
                            ) AS 'diff'
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND tp.pendencia_titulo = 1
                    AND ta.periodicidade = 12 {$where}
                    GROUP BY tp.id 
                    /* ANUAIS PAGANTES COM PENDENCIA */
                ) lista ORDER BY name {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalConventions(UserAdmin $user, $name = null, $cpf = null, $state = null, $city = null, $situation = null, $type = null): array
    {
        $params = [];
        $where = $this->generateWhereConventions($user, $name, $cpf, $state, $city, $situation, $type, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS 'total' FROM (
                    /* ISENTOS */
                    SELECT  DISTINCT(tp.id)
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 1 {$where}
                    GROUP BY tp.id
                    /* ISENTOS */
                    UNION ALL
                    /* MENSAIS PAGANTES */
                    SELECT DISTINCT(tp.id)					
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND (tp.pendencia_titulo = 0 || tp.pendencia_titulo IS NULL) 
                    AND ta.periodicidade = 1 {$where}
                    GROUP BY tp.id
                    /* MENSAIS PAGANTES */
                    UNION ALL
                    /* ANUAIS PAGANTES */
                    SELECT DISTINCT(tp.id)					
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND (tp.pendencia_titulo = 0 || tp.pendencia_titulo IS NULL) 
                    AND ta.periodicidade = 12 {$where}
                    GROUP BY tp.id
                    /* ANUAIS PAGANTES */
                    UNION ALL
                    /* MENSAIS PAGANTES COM PENDENCIA */
                    SELECT DISTINCT(tp.id)
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND tp.pendencia_titulo = 1
                    AND ta.periodicidade = 1 {$where}
                    GROUP BY tp.id
                    /* MENSAIS PAGANTES COM PENDENCIA */
                    UNION ALL
                    /* ANUAIS PAGANTES COM PENDENCIA */
                    SELECT DISTINCT(tp.id)
                    FROM tb_pessoa tp
                    LEFT JOIN tb_diretorio dir ON tp.titulo_eleitoral_municipio_id = dir.tb_cidade_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                    WHERE tp.filiado IN(7,8) 
                    AND IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao) <= (CURDATE() - INTERVAL IF(dir.nome LIKE '%Diretório%', 180, 90) DAY)
                    AND tp.exemption = 0
                    AND tp.pendencia_titulo = 1
                    AND ta.periodicidade = 12 {$where}
                    GROUP BY tp.id
                    /* ANUAIS PAGANTES COM PENDENCIA */
                ) total";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhereConventions(UserAdmin $user, $name = null, $cpf = null, $state = null, $city = null, $situation = null, $type = null, &$params)
    {
        $where = '';
        $sql = "IF(ta.periodicidade = 1, 
                    IF(tp.exemption = 1 OR (
                        (SELECT COUNT(0) 
                            FROM tb_transacoes tbt 
                            WHERE tbt.tb_pessoa_id = tp.id 
                                AND IF(tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao IN (2, 8))
                                AND tbt.data_criacao >= IF(tp.filiado = 7, tp.data_filiacao, tp.data_refiliacao)
                                AND tbt.status IN ('pending' , 'expired')
                                AND tbt.data_criacao BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND (CURDATE() - INTERVAL 45 DAY)
                    ) = 0) 
                    AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante'),

                    IF(tp.exemption = 1 OR (
                        (SELECT status
						    FROM tb_transacoes tbt
						    WHERE tb_pessoa_id = tp.id
                                AND IF(tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao IN (2, 8))
                                AND pendencia_titulo IS NOT NULL
							    ORDER BY tbt.data_criacao DESC
							    LIMIT 1
                    ) = 'paid') 
                    AND (tp.suspensao = 0 || tp.suspensao IS NULL), 'Votante', 'Não Votante')
                )";
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($cpf) {
            $params[':cpf'] = "%$cpf%";
            $where .= " AND tp.cpf LIKE :cpf";
        }
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($situation) {
            $params[':situation'] = $situation;
            $where .= " AND $sql = :situation";
        }
        if (in_array($type, [0, 1])) {
            $params[':type'] = $type;
            $where .= " AND tp.exemption = :type";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':city'] = $user->getCity()->getId();
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state AND tp.titulo_eleitoral_municipio_id = :city";
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        } else {
            if ($state) {
                $params[':state'] = $state;
                $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            }
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        }
        return $where;
    }

    public function listDisaffiliation(UserAdmin $user, $name = null, $status = null, $state = null, $city = null,
                                                 $typeDate = null, $begin = null, $end = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereDisaffiliation($user, $name, $status, $state, $city, $typeDate, $begin, $end, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tbp.id, tbp.filiado_id, tbp.nome AS 'name', tbc.cidade AS 'city', tbe.sigla AS 'state',
                    (CASE 
                        WHEN tbp.data_refiliacao IS NOT NULL THEN DATE_FORMAT(tbp.data_refiliacao, '%d/%m/%Y')
                        ELSE DATE_FORMAT(tbp.data_filiacao, '%d/%m/%Y')
                    END) AS filiation,
                    DATE_FORMAT(tbp.data_desfiliacao, '%d/%m/%Y') as 'disaffiliation',
                    IFNULL(tbmd2.motivo, tbmd.motivo) as reason, 
                    IFNULL(tbp.descricao_desfiliacao, '-') as 'description',
                    (SELECT  concat(tbtel.ddi , concat('(',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,1,2),')',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,3,5),'',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ',''),8)))
                        FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tbp.id AND tb_tipo_telefone_id = 3 LIMIT 1) AS phone,
                    en.endereco, en.numero, en.bairro, en.complemento, ci.cidade, es.estado
                FROM tb_pessoa tbp
                    LEFT JOIN tb_transacoes tbt ON tbt.tb_pessoa_id = tbp.id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tbp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tbp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_motivo_desfiliar tbmd ON tbmd.id = tbp.motivo_desfiliacao
                    LEFT JOIN tb_motivo_desfiliar tbmd2 ON tbmd2.id = tbp.reclassificacao_desfiliacao
                    LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tbp.id
                    LEFT JOIN tb_cidade ci ON ci.id = en.tb_cidade_id
                    LEFT JOIN tb_estado es ON es.id = en.tb_estado_id
                    WHERE tbp.filiado in (9) {$where} 
                    GROUP BY tbp.id 
                    ORDER BY tbp.data_desfiliacao DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalDisaffiliation(UserAdmin $user, $name = null, $status = null, $state = null, $city = null,
                                                      $typeDate = null, $begin = null, $end = null): array
    {
        $params = [];
        $where = $this->generateWhereDisaffiliation($user, $name, $status, $state, $city, $typeDate, $begin, $end, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tbp.id)) AS total
               FROM tb_pessoa tbp
                    LEFT JOIN tb_cidade tbc ON tbc.id = tbp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tbp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_motivo_desfiliar tbmd ON tbmd.id = tbp.motivo_desfiliacao
                    LEFT JOIN tb_motivo_desfiliar tbmd2 ON tbmd.id = tbp.reclassificacao_desfiliacao
                    LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tbp.id
                    LEFT JOIN tb_cidade ci ON ci.id = en.tb_cidade_id
                    LEFT JOIN tb_estado es ON es.id = en.tb_estado_id
                    WHERE tbp.filiado in (9, 10,11,12) {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhereDisaffiliation(UserAdmin $user, $name = null, $status = null, $state = null, $city = null, $typeDate = null, $begin = null, $end = null, &$params)
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tbp.nome LIKE :name";
        }
        if ($status) {
            if ($status == 'Isento') {
                $where .= " AND tbp.exemption = 1";
            } else {
                $params[':status'] = $status;
                $where .= "";
                $where .= " AND tbp.exemption = 0 && IFNULL(
                    (SELECT IF(tbt.status = 'paid' , 'Adimplente', 'Inadimplente') AS 'Situacao'
                        FROM  tb_transacoes tbt
                        WHERE tbt.tb_pessoa_id = tbp.id 
                            AND tbt.status NOT IN ('accumulated' , 'canceled','reversed') 
                            AND IF(tbp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                            ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                        ), 
                    'Inadimplente') = :status ";
            }
        }
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND tbp.titulo_eleitoral_uf_id = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND tbp.titulo_eleitoral_municipio_id = :city";
        }
        if ($begin) {
            $params[':begin'] = "$begin";
            $where .= " AND tbp.$typeDate >= :begin";
        }
        if ($end) {
            $params[':end'] = "$end";
            $where .= " AND tbp.$typeDate <= :end";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if (!$city) {
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (tbp.titulo_eleitoral_municipio_id = :city || 
                    tbp.titulo_eleitoral_municipio_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tbp.titulo_eleitoral_uf_id = :uf2";
        }
        return $where;
    }

    public function listDonors(UserAdmin $user, $name = null, $state = null, $city = null, $status = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereDonors($user, $name, $state, $city, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, IFNULL(tp.filiado_id, '-') as affiliated, tp.nome as name, IFNULL(DATE_FORMAT(tp.data_desfiliacao, '%d/%m/%Y'),'-') AS 'disaffiliation', tf.status, 
                        IF(tp.filiado IN (7,8), tet.sigla, tea.sigla) as state, 
                        IF(tp.filiado IN (7,8), tct.cidade, tca.cidade) as city,
                    (CASE 
                        WHEN tp.filiado = 7 THEN IFNULL(DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y'), '-')
                        WHEN tp.filiado = 8 THEN IFNULL(DATE_FORMAT(tp.data_refiliacao, '%d/%m/%Y'), '-')
                        ELSE '-'
                    END) AS filiation,
                    (SELECT COUNT(*) FROM tb_transacoes tt WHERE tt.tb_pessoa_id = ta.tb_pessoa_id AND tt.origem_transacao = 1 AND tt.status = 'paid') as count,
                    (SELECT REPLACE(REPLACE(REPLACE(FORMAT(SUM(tt.valor), 2), '.', '@'), ',', '.'), '@', ',') FROM tb_transacoes tt WHERE tt.tb_pessoa_id = ta.tb_pessoa_id AND tt.origem_transacao = 1 AND tt.status = 'paid') as sum
                FROM tb_pessoa_assinatura ta
                    LEFT JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                    LEFT JOIN tb_pessoa_status_filiado tf ON tf.id = tp.filiado
                    LEFT JOIN tb_estado tet ON tet.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade tct ON tct.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_endereco te ON te.tb_pessoa_id = ta.tb_pessoa_id
                    LEFT JOIN tb_estado tea ON tea.id = te.tb_estado_id
                    LEFT JOIN tb_cidade tca ON tca.id = te.tb_cidade_id
                WHERE ta.origem_transacao = 1 AND ta.status = 'created' AND tp.id IS NOT NULL {$where} 
                GROUP BY ta.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalDonors(UserAdmin $user, $name = null, $state = null, $city = null, $status = null): array
    {
        $params = [];
        $where = $this->generateWhereDonors($user, $name, $state, $city, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(ta.id)) AS total
                FROM tb_pessoa_assinatura ta
                    LEFT JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                    LEFT JOIN tb_pessoa_status_filiado tf ON tf.id = tp.filiado
                    LEFT JOIN tb_endereco te ON te.tb_pessoa_id = ta.tb_pessoa_id
                    LEFT JOIN tb_estado tes ON tes.id = te.tb_estado_id
                    LEFT JOIN tb_cidade tc ON tc.id = te.tb_cidade_id
                WHERE ta.origem_transacao = 1 AND ta.status = 'created' AND tp.id IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function generateWhereDonors(UserAdmin $user, $name = null, $state = null, $city = null, $status = null, &$params)
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($status) {
            $params[':status'] = $status;
            $where .= " AND tp.filiado = :status";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if (!$city) {
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (tp.titulo_eleitoral_municipio_id = :city || 
                    tp.titulo_eleitoral_municipio_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        }
        return $where;
    }

    private function generateWhereLatestAffiliations(array $filter, &$params)
    {
        $where = '';
        $startDate = \DateTime::createFromFormat('d/m/Y', $filter['startDate'])->format('Y-m-d');
        $params[':startDate'] = $startDate;
        $where .= " AND ( (tp.filiado = 7 AND tp.data_filiacao >= :startDate) OR  (tp.filiado = 8 AND tp.data_refiliacao >= :startDate))";
        if ($filter['name']) {
            $params[':name'] = $filter['name'];
            $where .= " AND tp.nome = :name";
        }
        if ($filter['state']) {
            $params[':state'] = $filter['state'];
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
        }
        if ($filter['city']) {
            $params[':city'] = $filter['city'];
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($filter['irpf'] > -1) {
            $params[':irpf'] = $filter['irpf'];
            $where .= " AND tp.exemption = :irpf";
        }
        return $where;
    }

    public function latestAffiliations(array $filter, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereLatestAffiliations($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.nome AS name, tp.titulo_eleitoral AS title, te.estado AS uf, tc.cidade AS city, 
                    (SELECT 
                        CONCAT(tbtel.ddi,
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                        FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3
                    LIMIT 1) AS phone,        
                    IF(tp.exemption = 1, 'Filiação Isenta', 'Filiação Normal') AS exemption,
                    (CASE tp.filiado
                        WHEN 7 THEN tp.data_filiacao
                        WHEN 8 THEN tp.data_refiliacao
                    END) AS filiation,
                    (CASE tp.filiado                        
                        WHEN 7 THEN 'Filiado'
                        WHEN 8 THEN 'Refiliado'       
                    END) as status,
                    (CASE ta.forma_pagamento
                        WHEN 2 THEN 'Boleto'
                        WHEN 1 THEN 'Cartão'
                        ELSE '-'
                    END) AS payment,
                    (CASE ta.periodicidade
                        WHEN 1 THEN 'Mensal'
                        WHEN 6 THEN 'Semestral'
                        WHEN 12 THEN 'Anual'
                        ELSE '-'
                    END) AS plan
                FROM tb_pessoa AS tp
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id AND ta.origem_transacao = 2
                WHERE tp.filiado in(7, 8) {$where} 
                GROUP BY tp.id ORDER BY filiation DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalLatestAffiliations(array $filter): array
    {
        $params = [];
        $where = $this->generateWhereLatestAffiliations($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total 
                FROM tb_pessoa AS tp
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id AND ta.origem_transacao = 2
                WHERE tp.filiado IN(7, 8) {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function getManagerData(string $cpf): array|bool
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.cpf, tp.nome AS name, tp.rg, tp.titulo_eleitoral AS title, tp.titulo_eleitoral_zona AS zone, tp.titulo_eleitoral_secao AS section, tp.genero AS gender,
                    DATE_FORMAT(tp.data_nascimento, '%d/%m/%Y') AS birth, tp.email, te.cep AS zipcode, te.endereco AS address, te.numero AS number, te.bairro AS district, te.complemento AS complement,
                    es.sigla AS state, ci.cidade AS city, tp.nome_mae AS motherName, tp.titulo_eleitoral_municipio_id AS voterTitleCity, tp.titulo_eleitoral_uf_id AS voterTitleUf,
                    (SELECT 
                        CONCAT(SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                                SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                                SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                        FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3
                    LIMIT 1) AS phone
                FROM tb_pessoa tp
                LEFT JOIN tb_endereco te ON te.tb_pessoa_id = tp.id
                LEFT JOIN tb_estado es ON es.id = te.tb_estado_id
                LEFT JOIN tb_cidade ci ON ci.id = te.tb_cidade_id
                WHERE cpf = :cpf
                GROUP BY tp.id";
        $rows = $pdo->prepare($sql)->execute([':cpf' => $cpf]);
        return $rows->fetchAssociative();
    }

    private function generateWhereReaffiliations($user, array $filter, &$params)
    {
        $where = '';
        if ($filter['name']) {
            $params[':name'] = "%{$filter['name']}%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_NATIONAL) {
            if ($filter['state']) {
                $state = $filter['state'];
                $params[':state'] = "$state";
                $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            }
            if ($filter['city']) {
                $city = $filter['city'];
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        } elseif ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            if ($filter['city']) {
                $city = $filter['city'];
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        } else {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        return $where;
    }

    public function listReaffiliations(UserAdmin $user, array $filter, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereReaffiliations($user, $filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.nome AS name, IF(tp.exemption = 1, 12, 7) AS exemption,
                    DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y') as date, tpsf.status AS status,
                    IFNULL(tbe.estado, en.estado) AS uf, IFNULL(tbc.cidade, en.cidade) AS city
                FROM tb_pessoa tp
                LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_pessoa_status_filiado tpsf ON tpsf.id = tp.filiado
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id
                WHERE tp.filiado = 14 
                    AND tp.data_solicitacao_refiliacao <= FROM_DAYS((TO_DAYS(CURDATE()) - 3)) 
                    AND tp.observacao = 'aguardando aprovacao refiliacao' {$where}
                GROUP BY tp.id
                ORDER BY tp.data_solicitacao_refiliacao";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalReaffiliations(UserAdmin $user, array $filter): array
    {
        $params = [];
        $where = $this->generateWhereReaffiliations($user, $filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total
                FROM tb_pessoa tp
                LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_pessoa_status_filiado tpsf ON tpsf.id = tp.filiado
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id
                WHERE tp.filiado = 14 
                    AND tp.data_solicitacao_refiliacao <= FROM_DAYS((TO_DAYS(CURDATE()) - 3)) 
                    AND tp.observacao = 'aguardando aprovacao refiliacao' {$where}
                ORDER BY tp.data_solicitacao_refiliacao";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listChatBot(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.nome AS name, tp.cpf, 
                    IF(tt.codigo_barras != '', tt.codigo_barras, '') AS digitableLine, 
                    IF(tt.forma_pagamento = 2, IF(tt.url != '', tt.url, CONCAT('https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction=', TO_BASE64(tt.id))), '') AS url, 
                    IF(tt.forma_pagamento = 1, 'Cartão de Crédito', 'Boleto') AS paymentMethod, 
                    IF(tt.forma_pagamento = 1, IFNULL(tc.expiration, ''), '') AS dueDate, 
                    DATEDIFF(CURDATE(), date_format(tt.data_criacao, '%Y-%m-%d')) AS compliance
                FROM tb_transacoes tt
                    JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                    JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                    LEFT JOIN tb_pessoa_cartao_credito tc ON tc.token_cartao_credito = ta.token_cartao_credito
                WHERE tp.filiado IN (7,8) AND tp.exemption = 0 AND tt.status IN ('expired' , 'pending') AND tt.origem_transacao = 2 
                    AND NOT CAST(tt.ultima_atualizacao AS DATE) <=> CURDATE() AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) 
                    AND (tt.obs != 'não-reprocessa' || tt.obs IS NULL) AND TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_criacao AS DATE)) IN (1, 2, 3, 4)
                GROUP BY tt.id";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listCollapse2025()
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT bellowSigned.id, IFNULL(tp.filiado_id, '-') AS filiado_id, IFNULL(fi.status, '-') AS filiado, bellowSigned.name, bellowSigned.email, 
                    phone, DATE_FORMAT(created, '%d/%m/%Y %H:%i:%s') AS created,
                    IFNULL((SELECT IF(tp.exemption = 1, 'Isento', IF(tbt.valor = '0.00' || tbt.status = 'paid' , 'Adimplente', 'Inadimplente'))
                        FROM  tb_transacoes tbt
                        WHERE tbt.tb_pessoa_id = tp.id 
                            AND tbt.status NOT IN ('accumulated' , 'canceled', 'reversed') 
                            AND IF(tp.exemption = 0 AND tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                        ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                    ), 'Inadimplente') AS situation
                FROM site2022.bellowSigned
                LEFT JOIN tb_pessoa tp ON tp.email = bellowSigned.email
                LEFT JOIN tb_pessoa_status_filiado fi ON fi.id = tp.filiado
                WHERE theme = 'colapso2024'";
        $sth = $pdo->prepare($sql);
        return $sth->execute()->fetchAllAssociative();
    }
}