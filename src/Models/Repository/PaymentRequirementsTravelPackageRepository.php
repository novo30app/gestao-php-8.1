<?php

namespace App\Models\Repository;

use App\Models\Entities\PaymentRequerimentsTravelPackage;
use Doctrine\ORM\EntityRepository;

class PaymentRequirementsTravelPackageRepository extends EntityRepository
{
	public function save(PaymentRequerimentsTravelPackage $entity): PaymentRequerimentsTravelPackage
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}
}