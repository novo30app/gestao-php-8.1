<?php

namespace App\Models\Repository;

use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionMeetingSubject;
use Doctrine\ORM\EntityRepository;

class InteractionMeetingSubjectRepository extends EntityRepository
{
    public function save(InteractionMeetingSubject $entity): InteractionMeetingSubject
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function dropByInteraction(Interaction $interaction)
    {
        $sql = "DELETE FROM interactionMeetingSubjects WHERE interaction = :interaction";
        $params = [':interaction' => $interaction->getId()];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }
}