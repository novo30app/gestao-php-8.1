<?php

namespace App\Models\Repository;

use App\Models\Entities\PaymentDetail;
use Doctrine\ORM\EntityRepository;

class PaymentDetailRepository extends EntityRepository
{
    public function save(PaymentDetail $entity): PaymentDetail
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}