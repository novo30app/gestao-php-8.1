<?php

namespace App\Models\Repository;

use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionMeetingSuggestion;
use Doctrine\ORM\EntityRepository;

class InteractionMeetingSuggestionRepository extends EntityRepository
{
    public function save(InteractionMeetingSuggestion $entity): InteractionMeetingSuggestion
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function dropByInteraction(Interaction $interaction)
    {
        $sql = "DELETE FROM interactionMeetingSuggestions WHERE interaction = :interaction";
        $params = [':interaction' => $interaction->getId()];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }
}