<?php

namespace App\Models\Repository;

use App\Models\Entities\EvaluationStatus;
use App\Models\Entities\ExatoDigital;
use Doctrine\ORM\EntityRepository;

class ExatoDigitalRepository extends EntityRepository
{
    public function save(ExatoDigital $entity): ExatoDigital
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

}