<?php

namespace App\Models\Repository;

use App\Models\Entities\SelectiveProcess2022Module1;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class SelectiveProcess2022Module1Repository extends EntityRepository
{
    public function save(SelectiveProcess2022Module1 $entity):SelectiveProcess2022Module1
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function findByState(UserAdmin $userAdmin): array
    {
        return $query = $this->getEntityManager()
            ->createQuery("SELECT s FROM App\Models\Entities\SelectiveProcess2022Module1 AS s
                                  JOIN s.candidate AS c
                              WHERE c.tituloEleitoralUfId = :state")
            ->setParameters([':state' => $userAdmin->getState()->getId()])
            ->getResult();
    }
}