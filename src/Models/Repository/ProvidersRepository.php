<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\Provider;
use Doctrine\ORM\EntityRepository;

class ProvidersRepository extends EntityRepository
{
    public function save(Provider $entity):Provider
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
	
	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}
	
	private function generateWhere(array $filter, &$params): string
	{
		$where = '';
		if ($filter['cpfCnpj']) {
			$params[':cpfCnpj'] = Utils::onlyNumbers($filter['cpfCnpj']);
			$params[':cpfCnpj']  = "%{$params[':cpfCnpj']}%";
			$where .= " AND providers.cpfCnpj LIKE :cpfCnpj";
		}
		if ($filter['corporateName']) {
			$name = $filter['corporateName'];
			$params[':corporateName'] = "%{$name}%";
			$where .= " AND providers.name LIKE :corporateName";
		}
		if ($filter['nameResponsible']) {
			$nameResponsible = $filter['nameResponsible'];
			$params[':nameResponsible'] = "%{$nameResponsible}%";
			$where .= " AND providers.nameResponsible LIKE :nameResponsible";
		}
		return $where;
	}
	
	public function list(array $filter, $limit = null, $offset = null): array
	{
		
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT providers.id, providers.nameResponsible, providers.name, providers.cpfCnpj, providers.phone, providers.active, c.account
                FROM providers
				LEFT JOIN chartOfAccounts c ON c.id = providers.accountCredit
                WHERE providers.status = 1 {$where}
				ORDER BY id DESC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function listTotal(array $filter): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(providers.id) AS total
                FROM providers
                WHERE providers.status = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}
	
	public function data(int $id): array
	{
		$params = [];
		$params[':id'] = $id;
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT *, (SELECT CONCAT(account, ' / ', description) FROM chartOfAccounts WHERE id = providers.accountCredit) AS inputAccounts
                FROM providers
                WHERE providers.id =:id AND providers.status = 1";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
}