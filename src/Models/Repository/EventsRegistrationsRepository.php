<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsRegistrations;
use Doctrine\ORM\EntityRepository;

class EventsRegistrationsRepository extends EntityRepository
{
    public function save(EventsRegistrations $entity):EventsRegistrations
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($id = null, $code = null, $name = null, $cpf = null, $status = null, $ticket = null, $gender = null, &$params)
    {
        $where = '';
        if ($id) {
            $params[':id'] = "$id";
            $where .= " AND p.tb_eventos_id = :id";
        }
        if ($code) {
            $params[':code'] = "%$code%";
            $where .= " AND p.code LIKE :code";
        } 
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND p.nome LIKE :name";
        } 
        if ($cpf) {
            $params[':cpf'] = "%$cpf%";
            $where .= " AND tp.cpf LIKE :cpf";
        } 
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND p.status = :status";
        } 
        if ($ticket) {
            $params[':ticket'] = "$ticket";
            $where .= " AND evp.descricao = :ticket";
        } 
        if ($gender) {
            $params[':gender'] = "$gender)";
            $where .= " AND tp.genero = :gender";
        } 
        return $where;
    }

    public function listRegistrationsParticipants($id = null, $code = null, $name = null, $cpf = null, $status = null, $ticket = null, $gender = null, $limit = null, $offset = null): array
    {       
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($id, $code, $name, $cpf, $status, $ticket, $gender, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, tp.id AS pessoaId, IFNULL(p.code, '-') AS code, IFNULL(p.cpf, '-') AS cpf, p.nome, p.email, evp.descricao AS ticket,
                        IF(p.telefone_numero IS NOT NULL, p.telefone_numero,
                            (SELECT  concat(tbtel.ddi , concat('(',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                            ,1,2),')',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                            ,3,5),'',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ',''),8)))
                            FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3 LIMIT 1)) AS phone,
                        IFNULL(IFNULL(IFNULL(ti_es.estado, en_es.estado), p.estado), '') AS 'estado', IFNULL(IFNULL(IFNULL(ti_ci.cidade, en_ci.cidade), p.cidade), '') AS 'cidade',
                        IF(p.valor = '0.00', 'gratuito', REPLACE(REPLACE(REPLACE(FORMAT(IF(tet.coupon IS NOT NULL, tet.valor, p.valor), 2),'.','@'),',','.'),'@',',')) AS 'valor',
                        DATE_FORMAT(p.data_criacao, '%d/%m/%Y %H:%i:%s') as data_criacao, p.status,
                        IF(p.data_checkin IS NULL, 'n/r', concat('Realizado em ',  DATE_FORMAT(p.data_checkin, '%d/%m/%Y %H:%i:%s'))) AS data_checkin, IFNULL(p.recommendation, '-') AS recommendation, v.file AS voucher, v.validation,
                        (CASE tp.filiado 
                            WHEN 0 THEN 'Não Filiado'
                            WHEN 1 THEN 'Pendente de Pgto Filiação'
                            WHEN 2 THEN 'Período Impugnação Filiação'
                            WHEN 3 THEN 'Pendente de Pgto Refiliação'
                            WHEN 4 THEN 'Impugnação solicitada'
                            WHEN 5 THEN 'Solicitação Cancelada'
                            WHEN 6 THEN 'Impugnado'
                            WHEN 7 THEN 'Filiado'
                            WHEN 8 THEN 'Refiliado'
                            WHEN 9 THEN 'Desfiliado'
                            WHEN 14 THEN 'Período Impugnação Refiliação'   
                            ELSE '-'   
                        END) AS situation,
                        IFNULL((SELECT
                            (CASE tt.forma_pagamento
                                WHEN 1 THEN 'Cartão de Crédito'
                                WHEN 2 THEN 'Boleto'
                                WHEN 11 THEN 'PIX'
                                ELSE '---'
                            END) 
                        FROM tb_eventos_transacoes et 
                        LEFT JOIN tb_transacoes tt ON tt.id = et.tb_transacao_id
                        WHERE et.id = p.tb_eventos_transacao_id), '---') AS paymentMethod, ec.name AS coupon
                FROM tb_eventos_participantes AS p 
                LEFT JOIN tb_pessoa tp ON p.tb_pessoa_id = tp.id
                LEFT JOIN tb_estado ti_es ON ti_es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ti_ci ON ti_ci.id = tp.titulo_eleitoral_municipio_id
				LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id
				LEFT JOIN tb_estado en_es ON en_es.id = en.tb_estado_id
                LEFT JOIN tb_cidade en_ci ON en_ci.id = en.tb_cidade_id
                LEFT JOIN tb_eventos_precos evp ON evp.id = p.tb_eventos_precos_id
                LEFT JOIN vaccineVoucher v ON v.user = tp.id
                LEFT JOIN tb_eventos_transacoes tet ON tet.id = p.tb_eventos_transacao_id
                LEFT JOIN eventsCoupons ec ON ec.id = tet.coupon
                WHERE 1 = 1 AND p.nome IS NOT NULL {$where} 
                GROUP BY p.id 
                ORDER BY p.nome ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalParticipants($id = null, $code = null, $name = null, $cpf = null, $status = null, $ticket = null, $gender = null): array
    {   
        $params = [];
        $where = $this->generateWhere($id, $code, $name, $cpf, $status, $ticket, $gender, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(p.id)) AS total 
                FROM tb_eventos_participantes AS p
                LEFT JOIN tb_pessoa tp ON p.tb_pessoa_id = tp.id
                LEFT JOIN tb_estado es ON es.id = p.tb_estado_id
                LEFT JOIN tb_cidade ci ON ci.id = p.tb_cidade_id
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id
                LEFT JOIN tb_eventos_precos evp ON evp.id = p.tb_eventos_precos_id
                LEFT JOIN vaccineVoucher v ON v.user = tp.id
                LEFT JOIN tb_eventos_transacoes tet ON tet.id = p.tb_eventos_transacao_id
                LEFT JOIN eventsCoupons ec ON ec.id = tet.coupon
                WHERE 1 = 1 AND p.nome IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function getvalues($id = null, $status = null)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT REPLACE(REPLACE(REPLACE(FORMAT(SUM(et.valor), 2),'.','@'),',','.'),'@',',') as value 
                FROM tb_eventos_transacoes et 
                WHERE et.tb_eventos_id = :id AND et.status = :status"; 
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':status' => $status]);
        return $rows->fetchAssociative();   
    }

    public function exportEventsTransactions($id = null, $name = null, $cpf = null, $status = null, $ticket = null,
        $gender = null): array
    {     
        $params = [];
        $where = $this->generateWhere($id, null, $name, $cpf, $status, $ticket, $gender, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tt.id, tt.tb_pessoa_id, tp.nome as name, tp.cpf, tt.invoice_id,  tt.numero_recibo as receipt, 
                    DATE_FORMAT(tt.data_criacao, '%d/%m/%Y') as created, IFNULL(DATE_FORMAT(tt.data_pago, '%d/%m/%Y'),'-') as payed,
                    IFNULL(te.estado, '-') AS state,  IFNULL(tc.cidade, '-') as city, 
                    REPLACE(REPLACE(REPLACE(FORMAT(tt.valor, 2), '.', '@'), ',', '.'), '@', ',') as 'value', d.nome AS directory,
                    (CASE `tt`.`status`
                        WHEN 'paid' THEN 'Pago'
                        WHEN 'expired' THEN 'Expirado'
                        WHEN 'pending' THEN 'Pendente'
                        WHEN 'canceled' THEN 'Cancelada'
                        WHEN 'Chargeback' THEN 'Chargeback'
                        WHEN 'refunded' THEN 'Reembolsado'
                        WHEN 'partially paid' THEN 'Pago Parcial'
                        WHEN 'accumulated' THEN 'Acumulado'
                        WHEN 'reversed' THEN 'Estornado'
                    END) as `status`,
                    (CASE `tt`.`origem_transacao`
                        WHEN 1 THEN 'Doação'
                        WHEN 2 THEN 'Filiação'
                        WHEN 3 THEN 'Doação Campanha'
                        WHEN 4 THEN 'Evento'
                        WHEN 5 THEN 'Complemento de Filiação'
                        WHEN 6 THEN 'Doação Maquininha'
                        WHEN 15 THEN 'Filiação antecipada'
                    END) AS `origin`,
                    (CASE `tt`.`periodicidade`
                        WHEN 0 THEN 'Avulsa'
                        WHEN 1 THEN 'Mensal'
                        WHEN 6 THEN 'Semestral'
                        WHEN 12 THEN 'Anual'
                    END) AS `plan`,
                    (CASE `tt`.`forma_pagamento`
                        WHEN 1 THEN 'Cartão'
                        WHEN 2 THEN 'Boleto'
                        WHEN 3 THEN 'TED'
                        WHEN 4 THEN 'DOC'
                        WHEN 5 THEN 'Transf Online'
                        WHEN 6 THEN 'Deposit Online'
                        WHEN 7 THEN 'Deposito'
                        WHEN 8 THEN 'Cheque'
                        WHEN 9 THEN 'Transferência'
                        WHEN 10 THEN 'Cartão de Débito'
                        WHEN 11 THEN 'PIX'
                    END) AS `payment`
                FROM tb_transacoes as tt 
                    LEFT JOIN tb_pessoa tp ON tt.tb_pessoa_id = tp.id
                    LEFT JOIN tb_endereco tben ON tben.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_diretorio d ON d.id = tt.tb_diretorio_id
                    LEFT JOIN tb_eventos_transacoes evt ON evt.tb_transacao_id = tt.id
                    LEFT JOIN tb_eventos_participantes p ON p.tb_eventos_transacao_id = evt.id
					LEFT JOIN tb_eventos_precos evp ON evp.id = p.tb_eventos_precos_id
                WHERE 1 = 1 {$where}
                GROUP BY tt.id
                ORDER BY tt.id DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}