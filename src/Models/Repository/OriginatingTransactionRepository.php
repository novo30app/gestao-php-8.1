<?php

namespace App\Models\Repository;

use App\Models\Entities\OriginatingTransaction;
use Doctrine\ORM\EntityRepository;

class OriginatingTransactionRepository extends EntityRepository
{
    public function save(OriginatingTransaction $entity): OriginatingTransaction
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
    
    public function listExport($requirement): array
	{
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT o.transaction, tp.nome, tp.cpf, o.requirement, tt.numero_recibo,
                CONCAT('R$ ' ,REPLACE(REPLACE(REPLACE(FORMAT(IFNULL(ts.newValue, tt.valor_s_taxas), 2), '.', '@'), ',', '.'), '@', ',')) AS value, tt.invoice_id
                FROM originatingTransaction o
                LEFT JOIN transferSplit ts ON o.transaction = ts.transaction
                LEFT JOIN tb_transacoes tt ON tt.id = o.transaction
                LEFT JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                WHERE o.requirement = :requirement";
		$rows = $pdo->prepare($sql)->execute([':requirement' => $requirement]);
		return $rows->fetchAllAssociative();
	}
}