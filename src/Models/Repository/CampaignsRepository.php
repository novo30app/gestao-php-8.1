<?php

namespace App\Models\Repository;

use App\Models\Entities\UserAdmin;
use App\Models\Entities\Campaigns;
use Doctrine\ORM\EntityRepository;

class CampaignsRepository extends EntityRepository
{
    public function save(Campaigns $entity):Campaigns
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere(UserAdmin $user, $name = null, $description = null, $status = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND c.name LIKE :name";
        }
        if ($description) {
            $params[':description'] = "%$description%";
            $where .= " AND c.description LIKE :description";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND c.status LIKE :status";
        }
        if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND c.state = :state";
        } else if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_CITY) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND c.state = :state";
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND c.city = :city";
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $description = null, $status = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $description, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, c.name, DATE_FORMAT(c.created, '%d/%m/%Y') AS created, u.name AS user, c.description, c.hash, c.status,
                (SELECT IFNULL(CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(sum(ta.valor), 2), '.', '@'), ',', '.'), '@', ',')), '-') AS 'valor'
                    FROM tb_pessoa_assinatura ta
                    LEFT JOIN tb_pessoa p ON p.id = ta.tb_pessoa_id
                    LEFT JOIN campaignsUsers ca ON ca.user = p.id
                    WHERE c.id = ca.campaign AND p.filiado IN (7,8) AND ta.origem_transacao = 2 AND ta.status = 'created') AS 'collection'
                FROM campaigns c
                LEFT JOIN usersAdmin u ON u.id = c.user
                WHERE c.status = 1 {$where} 
                GROUP BY c.id ORDER BY c.created ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $description = null, $status = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $description, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(c.id) AS total                  
                FROM campaigns c
                LEFT JOIN usersAdmin u ON u.id = c.user
                WHERE c.status = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function membershipCampaignsList(UserAdmin $user, $name = null, $state = null, $city = null, $ordernation = null, $beginDate = null, $finalDate = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->membershipCampaignsGenerateWhere($user, $name, $state, $city, $beginDate, $finalDate, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.nome AS name, count(*) AS result, es.sigla AS uf, ci.cidade AS city, CONCAT('https://novo.org.br/filiacao/?codigo=', tp.id) AS link, i.indicator, IFNULL(ifiles.file, '---') as file,
                    (SELECT 
                        CONCAT(tbtel.ddi,
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                        FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3
                    LIMIT 1) AS phone  
                FROM indicator i
                LEFT JOIN tb_pessoa tp ON tp.id = i.indicator
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN indicatorFiles ifiles ON ifiles.user = tp.id
                WHERE tp.nome IS NOT NULL {$where}
                GROUP BY i.indicator
                ORDER BY count(*) {$ordernation} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function membershipCampaignsListTotal(UserAdmin $user, $name = null, $state = null, $city = null, $beginDate = null, $finalDate = null): array
    {
        $params = [];
        $where = $this->membershipCampaignsGenerateWhere($user, $name, $state, $city, $beginDate, $finalDate, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(i.indicator)) AS total
                FROM indicator i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN indicatorFiles ifiles ON ifiles.user = tp.id
                WHERE tp.nome IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function membershipCampaignsGenerateWhere($user,  $name = null, $state = null, $city = null, $beginDate = null, $finalDate = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($state) {
            $params[':state'] = "$state";
            $where .= " AND es.id = :state";
        }
        if ($city) {
            $params[':city'] = "$city";
            $where .= " AND ci.id = :city";
        }
        if ($beginDate) {
            $params[':beginDate'] = "$beginDate";
            $where .= " AND IF(tp.filiado IN (9, 3, 14, 8), tp.data_solicitacao_refiliacao, tp.data_solicitacao_filiacao) >= :beginDate ";
        }
        if ($finalDate) {
            $params[':finalDate'] = "$finalDate";
            $where .= " AND IF(tp.filiado IN (9, 3, 14, 8), tp.data_solicitacao_refiliacao, tp.data_solicitacao_filiacao) <= :finalDate ";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if(!$city){
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (tp.titulo_eleitoral_municipio_id = :city || 
                    tp.titulo_eleitoral_municipio_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        }
        return $where;
    }

    public function membershipCampaignsResultsList(UserAdmin $user, $name = null, $indicator = null, $state = null, $city = null, $status = null, $beginDate = null, $finalDate = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->membershipCampaignsResultsGenerateWhere($user, $name, $indicator, $state, $city, $status, $beginDate, $finalDate, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.nome AS name, tp2.nome AS indicator, tf.status, es.sigla AS uf, ci.cidade AS city, i.user, i.indicator AS indicatorId,    
                IFNULL(IF(tp.filiado IN (0, 1, 2, 7), DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y'), DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')), '---') AS solicitationDate,
                IFNULL(IF(tp.filiado = 7, DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y'), DATE_FORMAT(tp.data_refiliacao, '%d/%m/%Y')), '---') AS affiliationDate
                FROM indicator i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_pessoa tp2 ON tp2.id = i.indicator
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_pessoa_status_filiado tf ON tf.id = tp.filiado
                WHERE tp.nome IS NOT NULL {$where}
                GROUP BY i.user
                ORDER BY count(*) DESC {$limitSql}";
                $rows = $pdo->prepare($sql)->execute($params);
                return $rows->fetchAllAssociative();
    }

    public function membershipCampaignsResultsListTotal(UserAdmin $user, $name = null, $indicator = null, $state = null, $city = null, $status = null, $beginDate = null, $finalDate = null): array
    {
        $params = [];
        $where = $this->membershipCampaignsResultsGenerateWhere($user, $name, $indicator, $state, $city, $status, $beginDate, $finalDate, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(i.user)) AS total
                FROM indicator i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_pessoa tp2 ON tp2.id = i.indicator
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN tb_pessoa_status_filiado tf ON tf.id = tp.filiado
                WHERE tp.nome IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function membershipCampaignsResultsGenerateWhere($user,  $name = null, $indicator = null, $state = null, $city = null, $status = null, $beginDate = null, $finalDate = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($indicator) {
            $params[':indicator'] = "%$indicator%";
            $where .= " AND tp2.nome LIKE :indicator";
        }
        if ($state) {
            $params[':state'] = "$state";
            $where .= " AND es.id = :state";
        }
        if ($city) {
            $params[':city'] = "$city";
            $where .= " AND ci.id = :city";
        }
        if ($status != '') {
            $groupedStatus = ['7' => '7,8', '1' => '1,3', '9' => '9,10,11,12', '2' => '2,14', '0' => '0,5'];
            if (array_key_exists($status, $groupedStatus)) {
                $where .= " AND tp.filiado IN ({$groupedStatus[$status]})";
            } else {
                $params[':status'] = $status;
                $where .= " AND tp.filiado = :status";
            }
        }
        if ($beginDate) {
            $params[':beginDate'] = "$beginDate";
            $where .= " AND IF(tp.filiado IN (9, 3, 14, 8), tp.data_solicitacao_refiliacao, tp.data_solicitacao_filiacao) >= :beginDate ";
        }
        if ($finalDate) {
            $params[':finalDate'] = "$finalDate";
            $where .= " AND IF(tp.filiado IN (9, 3, 14, 8), tp.data_solicitacao_refiliacao, tp.data_solicitacao_filiacao) <= :finalDate ";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            if(!$city){
                $params[':city'] = $user->getCity()->getId();
                $params[':userId'] = $user->getId();
                $where .= " AND (tp.titulo_eleitoral_municipio_id = :city || 
                    tp.titulo_eleitoral_municipio_id IN 
                        (SELECT access FROM accessAdmin WHERE userAdmin = :userId and type = 'city'
                            UNION ALL
                        SELECT city FROM mesoregionsCities WHERE mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' AND userAdmin = :userId)))";
            }
        } else if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :uf2";
        }
        return $where;
    }
}