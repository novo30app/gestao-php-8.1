<?php

namespace App\Models\Repository;

use App\Models\Entities\Directory;
use App\Models\Entities\UserAdmin;
use App\Models\Entities\DirectoryAccounts;
use Doctrine\ORM\EntityRepository;

class DirectoryAccountsRepository extends EntityRepository
{
	public function save(DirectoryAccounts $entity): DirectoryAccounts
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}

	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}

	private function generateWhere($user, $directory = null, $nickname = null, $extract = null, $state = null, $city = null, $begin = null, $end = null, &$params)
	{
		$where = '';
		if ($directory) {
			$params[':directory'] = "%$directory%";
			$where .= " AND d.nome LIKE :directory";
		}
		if ($nickname) {
			$params[':nickname'] = "$nickname";
			$where .= " AND da.nickname = :nickname";
		}
		if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':city'] = $user->getCity()->getId();
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND d.municipal = 'S'
						AND d.tb_estado_id = :uf 
						AND d.tb_cidade_id = :city";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND d.tb_estado_id = :uf";
        }
		if ($extract) {
			if($extract == 1) {
				$where .= " AND ex.id IS NOT NULL";
			} else {
				$where .= " AND ex.id IS NULL";
			}
		}
		if($state) {
			$params[':state'] = "$state";
			$where .= " AND d.tb_estado_id = :state";
		}
		if($city) {
			$params[':city'] = "$city";
			$where .= " AND d.tb_cidade_id = :city AND d.municipal = 'S'";
		}
		if($begin) {
			$params[':begin'] = "$begin";
			$where .= "  AND ex.dateStart >= :begin";
		}
		if($end) {
			$params[':end'] = "$end";
			$where .= "  AND ex.dateEnd <= :end";
		}
		return $where;
	}

	public function listTotal(UserAdmin $user, $directory = null, $nickname = null, $extract = null, $state = null, $city = null, $begin = null, $end = null): array
	{
		$params = [];
		$where = $this->generateWhere($user, $directory, $nickname, $extract, $state, $city, $begin, $end, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(DISTINCT(da.id)) AS total
                FROM directoryAccounts da
                LEFT JOIN banks ON banks.id = da.bank
                LEFT JOIN tb_diretorio d ON d.id = da.directory
				LEFT JOIN directoryExtract ex ON ex.account = da.id AND ex.status = 1
                WHERE 1 = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}

	public function list(UserAdmin $user, $directory = null, $nickname = null, $extract = null, $state = null, $city = null, $begin = null, $end = null, $limit = null, $offset = null): array
	{
		$params = [];
		$limitSql = $this->generateLimit($limit, $offset);
		$where = $this->generateWhere($user, $directory, $nickname, $extract, $state, $city, $begin, $end, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT da.id, d.nome AS directory, d.id AS directoryId, d.cnpj AS cnpj, banks.name AS bank, CONCAT(da.agency, '-', da.agencyDigit) AS agency2,
                    CONCAT(da.account, '-', da.accountDigit) AS account2, agencyDigit, accountDigit, da.nickname, da.agency, da.account, IFNULL(ch.description, '') AS accountingAccount,
                    (CASE da.nickname
                        WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
                        WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
                        WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
                        WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
                        WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
                        WHEN 6 THEN 'FEFC GERAL'
                        WHEN 7 THEN 'FEFC MULHER'
                        WHEN 8 THEN 'FEFC MULHER NEGRA'
                        WHEN 9 THEN 'FEFC HOMEM NEGRO'
                        WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
                        WHEN 11 THEN 'FEFC INDÍGENA'
						WHEN 12 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS - INVESTIMENTO'
						WHEN 13 THEN 'FP - FUNDO PARTIDÁRIO INVESTIMENTO'
						WHEN 14 THEN 'FP - POLÍTICA DA MULHER INVESTIMENTO'
						WHEN 15 THEN 'FEFC GERAL - INVESTIMENTO'
						WHEN 16 THEN 'FEFC MULHER - INVESTIMENTO'
						WHEN 17 THEN 'FEFC MULHER NEGRA - INVESTIMENTO'
						WHEN 18 THEN 'FEFC HOMEM NEGRO - INVESTIMENTO'
						WHEN 19 THEN 'FEFC INDÍGENA - INVESTIMENTO'
                    END) AS nicknameString, ch.account AS accountNumber, da.dateStart, da.valueStart, ex.id AS extract
                FROM directoryAccounts da
                LEFT JOIN banks ON banks.id = da.bank
                LEFT JOIN tb_diretorio d ON d.id = da.directory
				LEFT JOIN chartOfAccounts ch ON ch.id = da.accountingAccount
				LEFT JOIN directoryExtract ex ON ex.account = da.id AND ex.status = 1
                WHERE 1 = 1 {$where}
                GROUP BY da.id, da.nickname {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}

	public function accountsByDirectory(Directory $directory, $origin = null, ?Directory $directoryState = null, bool $type)
	{
		$params = [];
		$where = $whereDirectory = '';
		if ($origin) {
			$params[':origin'] = $origin;
			$where .= 'd.nickname = :origin AND ';
		};
		if (strpos($directory->getName(), 'Núcleo') !== false) {
			if ($directory->getEstadual() === 'S') {
				$whereDirectory .= 'd.directory = :directory';
				//$params[':directory'] = 1;
				$params[':directory'] = $directory->getId();
			} else if ($directory->getMunicipal() === 'S') {
				$whereDirectory .= '(d.directory = :directory OR d.directory = :directoryState)';
				$params[':directory'] = $directory->getId();
				$params[':directoryState'] = $directoryState->getId();
			} else {
				$whereDirectory .= 'd.directory = :directory';
				$params[':directory'] = $directory->getId();
			}
		} else {
			$whereDirectory .= 'd.directory = :directory';
			$params[':directory'] = $directory->getId();
		}
		return $this->getEntityManager()->createQuery(
			"SELECT d
                FROM  App\Models\Entities\DirectoryAccounts as d
                WHERE {$where} {$whereDirectory}
                ORDER BY d.directory DESC
               ")
			->execute($params);
	}
}
