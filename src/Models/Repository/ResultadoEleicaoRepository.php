<?php

namespace App\Models\Repository;

use App\Models\Entities\ResultadoEleicao;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ResultadoEleicaoRepository extends EntityRepository
{
    public function save(ResultadoEleicao $entity): ResultadoEleicao
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function resultsByAddress()
    {
        return $this->getEntityManager()->createQuery(
            "SELECT r.NM_LOCAL_VOTACAO AS local, r.DS_LOCAL_VOTACAO_ENDERECO AS endereco, r.NM_MUNICIPIO AS municipio, r.SG_UE AS uf
                 FROM App\Models\Entities\ResultadoEleicao AS r
                 WHERE r.latitude IS NULL
                 GROUP BY r.NM_LOCAL_VOTACAO
                              ")
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function setLatLong(array $data, array $location): void
    {
        $params = [
            ':local' => $data['local'],
            ':endereco' => $data['endereco'],
            ':municipio' => $data['municipio'],
            ':uf' => $data['uf'],
            'latitude' => $location['lat'],
            'longitude' => $location['lng']
        ];
        $this->getEntityManager()->createQuery(
            'UPDATE App\Models\Entities\ResultadoEleicao AS r
                    SET r.latitude = :latitude, r.longitude = :longitude 
                    WHERE r.NM_LOCAL_VOTACAO = :local AND r.DS_LOCAL_VOTACAO_ENDERECO = :endereco AND r.NM_MUNICIPIO = :municipio AND r.SG_UE = :uf'
        )
            ->setParameters($params)
            ->execute();
    }

    public function getLatLong($city)
    {
        $params = [':city' => $city,];
        return $this->getEntityManager()->createQuery(
            "SELECT MAX(r.latitude) AS max_latitude, MIN(r.latitude) AS min_latitude, MAX(r.longitude) AS max_longitude, MIN(r.longitude) AS min_longitude
				FROM  App\Models\Entities\ResultadoEleicao AS r
                WHERE r.latitude IS NOT NULL AND r.longitude IS NOT NULL AND r.NM_MUNICIPIO = :city
                GROUP BY r.latitude, r.longitude
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function getCities($uf)
    {
        $params = [
            ':uf' => $uf,
        ];
        return $this->getEntityManager()->createQuery(
            "SELECT r.NM_MUNICIPIO AS city
				FROM  App\Models\Entities\ResultadoEleicao AS r
                WHERE r.SG_UF = :uf
                GROUP BY r.NM_MUNICIPIO
                ORDER BY r.NM_MUNICIPIO ASC"
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function listTotal($data)
    {
        $params = [
            ':position' => $data['position'],
            ':year' => $data['year'],
            ':turn' => $data['turn'],
            ':city' => $data['city'],
        ];
        return $this->getEntityManager()->createQuery(
            "SELECT CASE WHEN r.NM_VOTAVEL IS NOT NULL THEN 'TOTAL' ELSE 'UNDEFINED' END AS candidate, SUM(r.QT_VOTOS) AS total, r.latitude AS lat, r.longitude AS long
				FROM  App\Models\Entities\ResultadoEleicao AS r
                WHERE r.latitude IS NOT NULL AND r.longitude IS NOT NULL AND r.DS_CARGO = :position AND r.NM_MUNICIPIO = :city AND r.ANO_ELEICAO = :year AND r.NR_TURNO = :turn
                GROUP BY r.NM_VOTAVEL, r.NM_LOCAL_VOTACAO
                ORDER BY r.NM_VOTAVEL ASC"
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function listResults($data)
    {
        $params = [
            ':position' => $data['position'],
            ':year' => $data['year'],
            ':turn' => $data['turn'],
            ':city' => $data['city'],
        ];
        return $this->getEntityManager()->createQuery(
            "SELECT r.NM_VOTAVEL AS candidate, SUM(r.QT_VOTOS) AS total, r.latitude AS lat, r.longitude AS long
				FROM  App\Models\Entities\ResultadoEleicao AS r
                WHERE r.latitude IS NOT NULL AND r.longitude IS NOT NULL AND r.DS_CARGO = :position AND r.NM_MUNICIPIO = :city AND r.ANO_ELEICAO = :year AND r.NR_TURNO = :turn
                GROUP BY r.NM_VOTAVEL, r.NM_LOCAL_VOTACAO
                ORDER BY r.NM_VOTAVEL ASC"
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
