<?php

namespace App\Models\Repository;

use App\Models\Entities\QuestionOption;
use Doctrine\ORM\EntityRepository;

class QuestionOptionsRepository extends EntityRepository
{
    public function save(QuestionOption $entity): QuestionOption
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}