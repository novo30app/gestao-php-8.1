<?php

namespace App\Models\Repository;

use App\Models\Entities\UserAdmin;
use App\Models\Entities\IndicationOfAffiliation;
use Doctrine\ORM\EntityRepository;

class IndicationOfAffiliationRepository extends EntityRepository
{
    public function save(IndicationOfAffiliation $entity):IndicationOfAffiliation
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, $state = null, $city = null, $status = null, &$params): string
    {   
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_NATIONAL) {
            if ($state) {
                $params[':state'] = "$state";
                $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            }
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        } elseif ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
            }
        } else {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND i.status = :status";
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $state = null, $city = null, $status = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $state, $city, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT i.id, i.user, tp.nome AS name, DATE_FORMAT(i.createdAt, '%d/%m/%Y') AS createdAt, es.estado AS state, ci.cidade AS city, 
                    ua.name AS userAdmin, '' AS status, IFNULL(i.membershipForm, '-') AS membershipForm, i.status,
                    (CASE i.status
                        WHEN 1 THEN 'Aguardando aprovação de filiação'
                        WHEN 2 THEN 'Aguardando aprovação do filiado'
                        WHEN 3 THEN 'Cancelado'
                        WHEN 4 THEN 'Período de impugnação'
                        WHEN 5 THEN 'Filiado'
                        WHEN 6 THEN 'Aguardando Pagamento'
                        ELSE 'Desconhecido'
                    END) AS statusString
                FROM indicationOfAffiliation i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN usersAdmin ua ON ua.id = i.userAdmin
                WHERE i.status != 3 {$where}
                GROUP BY i.id 
                ORDER BY i.createdAt DESC {$limitSql}";       
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $state = null, $city = null, $status = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $state, $city, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(i.id)) AS total
                FROM indicationOfAffiliation i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                LEFT JOIN usersAdmin ua ON ua.id = i.userAdmin
                WHERE i.status != 3 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}