<?php

namespace App\Models\Repository;

use App\Models\Entities\IndicatedDirectoriesMembers;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class IndicatedDirectoriesMembersRepository extends EntityRepository
{
    public function save(IndicatedDirectoriesMembers $entity): IndicatedDirectoriesMembers
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $state = null, $city = null, $status = null, $office = null, &$params): string
    {   
        $where = '';
        if ($state) {
            $params[':state'] = "$state";
            $where .= " AND m.state = :state";
        }
        if ($city) {
            $params[':city'] = "$city";
            $where .= " AND m.city = :city";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND m.status = :status";
        }
        if ($office) {
            $params[':office'] = "$office";
            $where .= " AND m.office = :office";
        }
        return $where;
    }

    public function list(UserAdmin $user, $state = null, $city = null, $status = null, $office = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $state, $city, $status, $office, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT m.id, es.estado AS state, IFNULL(ci.cidade, '---') AS city, m.name, m.cpf, m.active, m.indication, m.status,
                    m.question1, m.question2, question3, q.done AS questionnaire, m.approvalDN, m.approvalDE,
                    IF(i.type != 1,
                        (CASE m.office
                            WHEN 1 THEN 'Presidente'
                            WHEN 2 THEN 'Vice Presidente'
                            WHEN 3 THEN 'Secretário Administrativo'
                            WHEN 4 THEN 'Secretário de Finanças'
                            WHEN 5 THEN 'Secretário de Relações Institucionais e Legal'
                        END),
                        (CASE m.office
                            WHEN 1 THEN 'Líder 1'
                            WHEN 2 THEN 'Líder 2'
                            WHEN 3 THEN 'Líder 3'
                            WHEN 4 THEN 'Líder 4'
                            WHEN 5 THEN 'Líder 5'
                        END)) AS office,
                    (CASE m.status
                        WHEN 1 THEN 'Indicação'
                        WHEN 2 THEN 'Análise'
                        WHEN 3 THEN 'Aprovado'
                        WHEN 5 THEN 'Inativo'
                        WHEN 6 THEN 'Análise DE'
                        WHEN 7 THEN 'Análise DN'
                    END) AS statusString
                FROM indicatedDirectoriesMembers m
                LEFT JOIN indicatedDirectories i ON i.id = m.indication
                LEFT JOIN tb_estado es ON es.id = m.state
                LEFT JOIN tb_cidade ci ON ci.id = m.city
                LEFT JOIN questionnaireReply q ON q.id = m.questionnaire
                WHERE m.status = 3 AND i.active = 1 {$where} {$limitSql}";         
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $state = null, $city = null, $status = null, $office = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $state, $city, $status, $office, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(m.id)) AS total
                FROM indicatedDirectoriesMembers m
                LEFT JOIN indicatedDirectories i ON i.id = m.indication
                LEFT JOIN tb_estado es ON es.id = m.state
                LEFT JOIN tb_cidade ci ON ci.id = m.city
                LEFT JOIN questionnaireReply q ON q.id = m.questionnaire
                WHERE m.status = 3 AND i.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}