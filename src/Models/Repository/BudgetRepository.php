<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\Budget;
use App\Models\Entities\Directory;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class BudgetRepository extends EntityRepository
{
    public function save(Budget $entity): Budget
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere(array $filter, &$params): string
    {
        $where = '';
        if ($filter['id']) {
            $params[':id'] = $filter['id'];
            $where .= " AND budgets.id = :id";
        }
        if ($filter['costCenter']) {
            $params[':costCenter'] = $filter['costCenter'];
            $where .= " AND budgets.costCenter = :costCenter";
        }
        if ($filter['area']) {
            $params[':area'] = $filter['area'];
            $where .= " AND budgets.area = :area";
        }
        if ($filter['directory']) {
            $directory = $this->_em->getRepository(Directory::class)->findOneBy(['nome' => $filter['directory']]);
            $params[':directory'] = $directory->getId();
            $where .= " AND costCenter.directory = :directory";
        }
        if ($filter['year']) {
            $params[':year'] = $filter['year'];
            $where .= " AND budgets.year = :year";
        }
        if ($filter['value']) {
            $params[':value'] = Utils::moneyToFloat($filter['value']);
            $where .= " AND budgets.value = :value";
        }
        return $where;
    }

    public function list(array $filter, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT budgets.id, budgets.year, budgets.total, budgets.costCenter, costCenter.name AS costCenterName, 
                DATE_FORMAT(budgets.created, '%d/%m/%Y') AS date, tb_diretorio.nome AS directoryName, area.name AS area,
                area.id AS areaId, costCenter.id AS costCenterId, tb_diretorio.codigo As directoryId,
                    (SELECT 
                        SUM(paymentRequirements.paymentValue)
                    FROM paymentRequirements
                    WHERE paymentRequirements.costCenter = budgets.costCenter AND paymentRequirements.area = budgets.area
                    AND paymentRequirements.requestStatus IN (3, 7)
                        AND YEAR(payDay) = budgets.year
                    ) AS realized,
                    (SELECT 
                        SUM(paymentRequirements.paymentValue)
                    FROM paymentRequirements
                    WHERE paymentRequirements.costCenter = budgets.costCenter AND paymentRequirements.area = budgets.area
                    AND paymentRequirements.requestStatus NOT IN(3, 5, 7)
                    AND YEAR(paymentDueDate) = budgets.year
                    ) AS provisioned
                FROM budgets
                JOIN costCenter ON costCenter.id = budgets.costCenter
                JOIN area ON area.id = budgets.area
                JOIN tb_diretorio ON tb_diretorio.id = costCenter.directory
                WHERE budgets.status = 1 {$where}
				ORDER BY {$filter['order']} {$filter['seq']} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(budgets.id) AS total
                FROM budgets
                LEFT JOIN costCenter ON costCenter.id = budgets.costCenter
                WHERE budgets.status = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function data($id)
    {
        $params = [':id' => $id];
        return $this->getEntityManager()->createQuery(
            "SELECT b AS data, v AS values, d.nome AS directory, c.id AS costCenter, IDENTITY(b.area) AS area, IDENTITY(b.subArea) AS subArea
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.id = :id
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function listDetails($filter, $offset = null)
    {
        $params = [];
        $where = $this->generateWhereDetails($filter, $params);
        $budgets = $this->getEntityManager()->createQuery(
            "SELECT b
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				LEFT JOIN b.area AS a
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.status = 1 {$where}
                GROUP BY b.id
                "
        )
            ->setMaxResults(10)
            ->setFirstResult($offset)
            ->setParameters($params)
            ->getResult();

        $budgetsArr = [];

        foreach ($budgets as $budget) {
            $values = [];

            foreach ($budget->getValues() as $value) {
                $values[] = [
                    'id' => $value->getId(),
                    'month' => $value->getMonth(),
                    'value' => $value->getValue(),
                ];
            }

            $budgetsArr[] = [
                'id' => $budget->getId(),
                'year' => $budget->getYear(),
                'total' => $budget->getTotal(),
                'values' => $values,
                'directory' => $budget->getCostCenter()->getDirectory()->getName(),
                'costCenter' => $budget->getCostCenter()->getName(),
                'ccId' => $budget->getCostCenter()->getId(),
                'area' => $budget->getArea()->getName(),
                'aId' => $budget->getArea()->getId(),
            ];
        }

        return $budgetsArr;
    }

    public function listTotalDetails($filter)
    {
        $params = [];
        $where = $this->generateWhereDetails($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(b.id) AS total
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				LEFT JOIN b.area AS a
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.status = 1 {$where}
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function listDetailsByCostCenter($filter, $offset = null)
    {
        $params = [];
        $where = $this->generateWhereDetails($filter, $params);
        $budgets = $this->getEntityManager()->createQuery(
            "SELECT b
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.status = 1 {$where}
                GROUP BY b.costCenter
                "
        )
            ->setMaxResults(10)
            ->setFirstResult($offset)
            ->setParameters($params)
            ->getResult();

        $budgetsArr = [];

        foreach ($budgets as $budget) {
            $params[':constCenter'] = $budget->getCostCenter()->getId();
            $values = $this->getEntityManager()->createQuery(
                "SELECT SUM(v.value) AS value, v.month
				FROM  App\Models\Entities\BudgetValues AS v
				LEFT JOIN v.budget AS b
				INNER JOIN b.costCenter AS c
                WHERE b.status = 1 AND b.costCenter = :constCenter {$where}
                GROUP BY v.month
                "
            )
                ->setParameters($params)
                ->getResult(Query::HYDRATE_ARRAY);

            $budgetsArr[] = [
                'year' => $budget->getYear(),
                'total' => $budget->getTotal(),
                'values' => $values,
                'directory' => $budget->getCostCenter()->getDirectory()->getName(),
                'costCenter' => $budget->getCostCenter()->getName(),
                'ccId' => $budget->getCostCenter()->getId(),
            ];
        }

        return $budgetsArr;
    }

    public function listTotalDetailsByCostCenter($filter)
    {
        $params = [];
        $where = $this->generateWhereDetails($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(b.id) AS total
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.status = 1 {$where}
                GROUP BY b.costCenter
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function listDetailsByDirectory($filter, $offset = null)
    {
        $params = [];
        $where = $this->generateWhereDetails($filter, $params);
        $budgets = $this->getEntityManager()->createQuery(
            "SELECT b
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.status = 1 {$where}
                GROUP BY c.directory
                "
        )
            ->setMaxResults(10)
            ->setFirstResult($offset)
            ->setParameters($params)
            ->getResult();

        $budgetsArr = [];

        foreach ($budgets as $budget) {
            $params[':directory'] = $budget->getCostCenter()->getDirectory()->getId();
            $values = $this->getEntityManager()->createQuery(
                "SELECT SUM(v.value) AS value, v.month
				FROM App\Models\Entities\BudgetValues AS v
				LEFT JOIN v.budget AS b
				LEFT JOIN b.costCenter AS c
                WHERE b.status = 1 AND c.directory = :directory {$where}
                GROUP BY v.month
                "
            )
                ->setParameters($params)
                ->getResult(Query::HYDRATE_ARRAY);

            $budgetsArr[] = [
                'year' => $budget->getYear(),
                'total' => $budget->getTotal(),
                'values' => $values,
                'directory' => $budget->getCostCenter()->getDirectory()->getName(),
                'dId' => $budget->getCostCenter()->getDirectory()->getId(),
            ];
        }

        return $budgetsArr;
    }

    public function listTotalDetailsByDirectory($filter)
    {
        $params = [];
        $where = $this->generateWhereDetails($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(b.id) AS total
				FROM  App\Models\Entities\Budget AS b
				LEFT JOIN b.values AS v
				INNER JOIN b.costCenter AS c
				INNER JOIN c.directory AS d
                WHERE b.status = 1 {$where}
                GROUP BY c.directory
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    private function generateWhereDetails(array $filter, &$params): string
    {
        $where = '';
        if ($filter['costCenter']) {
            $params[':costCenter'] = $filter['costCenter'];
            $where .= " AND c.name = :costCenter";
        }
        if ($filter['area']) {
            $params[':area'] = $filter['area'];
            $where .= " AND b.area = :area";
        }
        if ($filter['directory']) {
            $directory = $this->_em->getRepository(Directory::class)->findOneBy(['nome' => $filter['directory']]);
            $params[':directory'] = $directory->getId();
            $where .= " AND c.directory = :directory";
        }
        if ($filter['year']) {
            $params[':year'] = $filter['year'];
            $where .= " AND b.year = :year";
        }
        return $where;
    }

    public function listByCostCenter(array $filter, $limit = null, $offset = null)
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT budgets.year, SUM(budgets.total) AS total, costCenter.name AS costCenterName, 
                DATE_FORMAT(budgets.created, '%d/%m/%Y') AS date, tb_diretorio.nome AS directoryName,
                    (SELECT 
                        SUM(paymentRequirements.paymentValue)
                    FROM paymentRequirements
                    WHERE paymentRequirements.costCenter = budgets.costCenter
                    AND paymentRequirements.requestStatus IN (3, 7)
                        AND YEAR(payDay) = budgets.year
                        GROUP BY paymentRequirements.costCenter
                    ) AS realized,
                    (SELECT 
                        SUM(paymentRequirements.paymentValue)
                    FROM paymentRequirements
                    WHERE paymentRequirements.costCenter = budgets.costCenter
                    AND paymentRequirements.requestStatus NOT IN(3, 5, 7)
                    AND YEAR(paymentDueDate) = budgets.year
                    GROUP BY paymentRequirements.costCenter
                    ) AS provisioned
                FROM budgets
                JOIN costCenter ON costCenter.id = budgets.costCenter
                JOIN tb_diretorio ON tb_diretorio.id = costCenter.directory
                WHERE budgets.status = 1 {$where}
				GROUP BY budgets.costCenter
                ORDER BY costCenter.name ASC {$limitSql}
                ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalByCostCenter(array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS total
                    FROM (
                        SELECT COUNT(budgets.costCenter) AS total_costCenter
                        FROM budgets
                        LEFT JOIN costCenter ON costCenter.id = budgets.costCenter
                        WHERE budgets.status = 1 {$where}
                        GROUP BY budgets.costCenter
                    ) AS grouped_results";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listByDirectory(array $filter, $limit = null, $offset = null)
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $wherePR = $this->generateWhereByDirectory($filter, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT budgets.year, SUM(budgets.total) AS total, tb_diretorio.nome AS directoryName,
                    (SELECT 
                        SUM(paymentRequirements.paymentValue)
                    FROM paymentRequirements
                    WHERE paymentRequirements.directory = costCenter.directory {$wherePR}
                    AND paymentRequirements.requestStatus IN (3, 7)
                        AND YEAR(payDay) = budgets.year
                        GROUP BY paymentRequirements.directory
                    ) AS realized,
                    (SELECT 
                        SUM(paymentRequirements.paymentValue)
                    FROM paymentRequirements
                    WHERE paymentRequirements.directory = costCenter.directory {$wherePR}
                    AND paymentRequirements.requestStatus NOT IN(3, 5, 7)
                    AND YEAR(paymentDueDate) = budgets.year
                    GROUP BY paymentRequirements.directory
                    ) AS provisioned
                FROM budgets
                JOIN costCenter ON costCenter.id = budgets.costCenter
                JOIN tb_diretorio ON tb_diretorio.id = costCenter.directory
                WHERE budgets.status = 1 {$where}
				GROUP BY costCenter.directory
                ORDER BY tb_diretorio.nome ASC {$limitSql}
                ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalByDirectory(array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS total
                    FROM (
                        SELECT COUNT(budgets.costCenter) AS total_costCenter
                        FROM budgets
                        LEFT JOIN costCenter ON costCenter.id = budgets.costCenter
                        WHERE budgets.status = 1 {$where}
                        GROUP BY costCenter.directory
                    ) AS grouped_results";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function generateWhereByDirectory(array $filter, &$params): string
    {
        $where = '';
        if ($filter['costCenter']) {
            $params[':costCenter'] = $filter['costCenter'];
            $where .= " AND paymentRequirements.costCenter = :costCenter";
        }
        if ($filter['area']) {
            $params[':area'] = $filter['area'];
            $where .= " AND paymentRequirements.area = :area";
        }
        if ($filter['directory']) {
            $directory = $this->_em->getRepository(Directory::class)->findOneBy(['nome' => $filter['directory']]);
            $params[':directory'] = $directory->getId();
            $where .= " AND paymentRequirements.directory = :directory";
        }
        return $where;
    }
}
