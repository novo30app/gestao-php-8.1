<?php

namespace App\Models\Repository;

use App\Models\Entities\Candidate;
use Doctrine\ORM\EntityRepository;

class CandidateRepository extends EntityRepository
{
    public function save(Candidate $entity):Candidate
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, $status = null, $uf = null, $city = null, $occupation = null, $year = null, $elected = null,  &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND candidates.name LIKE :name";
        }
        if ($status) {
            $params[':status'] = $status;
            $where .= " AND candidates.candidateStatus = :status";
        }
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND candidates.state = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND candidates.city = :city";
        }
        if ($occupation) {
            $params[':occupation'] = $occupation;
            $where .= " AND candidates.candidatePost = :occupation";
        }
        if ($year) {
            $params[':year'] = $year;
            $where .= " AND candidates.year = :year";
        }
        if ($elected) {
            $params[':elected'] = $elected;
            $where .= " AND candidates.candidateResult = :elected";
        }
        return $where;
    }

    public function list($name = null, $status = null, $uf = null, $city = null, $occupation = null, $year = null, $elected = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $status, $uf, $city, $occupation, $year, $elected, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT candidates.id, candidates.name, candidatesStatus.name AS statusStr, candidatesPosts.name AS occupationStr, ts.status,
                    IF(tp.filiado IN(9, 11, 10, 12), DATE_FORMAT(tp.data_desfiliacao, '%d/%m/%Y'), '') AS disaffiliation,
                    IF(city.cidade IS NOT NULL, city.cidade, '-') AS cityStr, candidates.cpf, candidates.email,
                    IF(state.sigla IS NOT NULL, state.sigla, '-') AS ufStr, candidates.numberOfVotes, candidates.year,
                    IFNULL((SELECT  concat(tbtel.ddi , concat(' (',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,1,2),') ',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,3,5),'',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ',''),8)))
                    FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3 LIMIT 1), '-') AS phone
                FROM candidates
                JOIN  candidatesPosts ON candidatesPosts.id = candidates.candidatePost
                JOIN  candidatesStatus ON candidatesStatus.id = candidates.candidateStatus
                LEFT JOIN  tb_estado as state ON state.id = candidates.state
                LEFT JOIN  tb_cidade as city ON city.id = candidates.city
                LEFT JOIN tb_pessoa tp ON tp.email = candidates.email
                LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                WHERE 1 = 1 {$where}
                ORDER BY name ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null, $status = null, $uf = null, $city = null, $occupation = null, $year = null, $elected = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $status, $uf, $city, $occupation, $year, $elected, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(candidates.id) AS total                  
                FROM candidates
                JOIN  candidatesPosts ON candidatesPosts.id = candidates.candidatePost
                JOIN  candidatesStatus ON candidatesStatus.id = candidates.candidateStatus
                LEFT JOIN  tb_estado as state ON state.id = candidates.state
                LEFT JOIN  tb_cidade as city ON city.id = candidates.city
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listExport($name = null, $status = null, $uf = null, $city = null, $occupation = null, $year = null, $elected = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $status, $uf, $city, $occupation, $year, $elected, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT candidates.id, candidates.name, candidatesStatus.name AS statusStr, candidatesPosts.name AS occupationStr, ts.status,
                    IF(tp.filiado IN(9, 11, 10, 12), DATE_FORMAT(tp.data_desfiliacao, '%d/%m/%Y'), '') AS disaffiliation,
                    IF(state.sigla IS NOT NULL, state.sigla, '-') AS ufStr, IF(city.cidade IS NOT NULL, city.cidade, '-') AS cityStr, candidates.cpf, 
                    candidates.email, candidates.numberOfVotes, candidates.year,
                    IFNULL((SELECT  concat(tbtel.ddi , concat(' (',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,1,2),') ',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ','') 
                        ,3,5),'',substr(replace(replace(replace(replace(trim(tbtel.telefone),'(',''),')',''),'-',''),' ',''),8)))
                    FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3 LIMIT 1), '-') AS phone
                FROM candidates
                JOIN  candidatesPosts ON candidatesPosts.id = candidates.candidatePost
                JOIN  candidatesStatus ON candidatesStatus.id = candidates.candidateStatus
                LEFT JOIN  tb_estado as state ON state.id = candidates.state
                LEFT JOIN  tb_cidade as city ON city.id = candidates.city
                LEFT JOIN tb_pessoa tp ON tp.email = candidates.email
                LEFT JOIN tb_pessoa_status_filiado AS ts ON ts.id = tp.filiado
                WHERE 1 = 1 {$where}
                ORDER BY name ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhereCabinets($name = null, $state = null, $year = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND c.name LIKE :name";
        }
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND c.state = :state";
        }
        if ($year) {
            $params[':year'] = $year;
            $where .= " AND c.elected = :year";
        }
        return $where;
    }

    public function listCabinets($name = null, $state = null, $year = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereCabinets($name, $state, $year, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, c.name, c.state, IFNULL(c.city, '---') AS city, c.elected 
                FROM dam.cabinets c
                WHERE c.active = 1 {$where}
                ORDER BY c.name ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalCabinets($name = null, $state = null, $year = null): array
    {
        $params = [];
        $where = $this->generateWhereCabinets($name, $state, $year, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(c.id)) AS total                  
                FROM dam.cabinets c
                WHERE c.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function getCandidateJourney2024(string $id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ca.name, ca.email, ca.birthDate, ca.cpf, ca.whats, pe.zipCode, st.id AS state, ci.id AS city, 
                    pe.address, pe.number, pe.neighborhood, IFNULL(pe.complement, '') AS complement
                FROM jornada2024.candidate ca
                LEFT JOIN jornada2024.personalHistory pe ON pe.candidate = ca.id
                LEFT JOIN jornada2024.states st ON st.uf = pe.state
                LEFT JOIN jornada2024.cities ci ON ci.name = pe.city
                LEFT JOIN jornada2024.transactions tr ON tr.candidate = ca.id
                WHERE tr.invoiceId = :invoice";
        $rows = $pdo->prepare($sql)->execute([':invoice' => $id]);
        return $rows->fetchAllAssociative();    
    }
}