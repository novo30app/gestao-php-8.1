<?php

namespace App\Models\Repository;

use App\Models\Entities\QuestionnaireAnswer;
use Doctrine\ORM\EntityRepository;

class QuestionnaireAnswerRepository extends EntityRepository
{
    public function save(QuestionnaireAnswer $entity): QuestionnaireAnswer
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getPointsByQuestionnaire(int $questionnaireReply)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT count(a.id) * 2 AS maxPoints, SUM(questionOptions.points) AS points
                FROM questionnaireAnswers AS a
                JOIN questionnaireReply ON questionnaireReply.id = a.questionnaireReply               
                JOIN questionOptions ON questionOptions.id = a.questionOption
                WHERE questionnaireReply.id = :questionnaireReply";
        $sth = $pdo->prepare($sql);
        return $sth->execute([':questionnaireReply' => $questionnaireReply])->fetchAssociative();
    }

}