<?php

namespace App\Models\Repository;

use App\Models\Entities\ResearchNovoInformative;
use Doctrine\ORM\EntityRepository;

class ResearchNovoInformativeRepository extends EntityRepository
{
    public function save(ResearchNovoInformative $entity): ResearchNovoInformative
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}