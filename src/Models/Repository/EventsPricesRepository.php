<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsPrices;
use Doctrine\ORM\EntityRepository;

class EventsPricesRepository extends EntityRepository
{
    public function save(EventsPrices $entity):EventsPrices
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function disabledPrices($eventId = null)
    {       
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "UPDATE tb_eventos_precos SET status = 'finalizado' WHERE tb_eventos_id = :eventId";
        $sth = $pdo->prepare($sql);
        $sth->execute([':eventId' => $eventId]);
    }

    public function getTickets($id)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_eventos_precos WHERE tb_eventos_id = :id GROUP BY descricao ORDER BY valor";       
        $rows = $pdo->prepare($sql)->execute([':id' => $id]);
        return $rows->fetchAllAssociative();
    }
}