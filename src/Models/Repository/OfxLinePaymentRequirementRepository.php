<?php

namespace App\Models\Repository;

use App\Models\Entities\OfxLinePaymentRequirement;
use App\Models\Entities\OfxLines;
use Doctrine\ORM\EntityRepository;

class OfxLinePaymentRequirementRepository extends EntityRepository
{
    public function save(OfxLinePaymentRequirement $entity): OfxLinePaymentRequirement
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function dropByOfxLine(OfxLines $line)
    {
        $sql = "DELETE FROM ofxLinesPaymentRequirements WHERE ofxLine = :ofxLine";
        $params = [':ofxLine' => $line->getId()];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }
}