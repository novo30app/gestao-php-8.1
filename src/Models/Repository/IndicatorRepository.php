<?php

namespace App\Models\Repository;

use App\Models\Entities\Indicator;
use Doctrine\ORM\EntityRepository;

class IndicatorRepository extends EntityRepository
{
    public function save(Indicator $entity):Indicator
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, $state = null, $city = null)
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE '%$name%'";
        }
        if($user->getLevel() == 2){
            $state = $user->getState()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = $state";
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id = $city";
            } 
        } else if ($user->getLevel() == 1){
            $state = $user->getState()->getId();
            $city = $user->getCity()->getId();
            $where .= " AND tp.titulo_eleitoral_uf_id = $state AND tp.titulo_eleitoral_municipio_id = $city";
        } else {
            if ($state) {
                $params[':state'] = "$state";
                $where .= " AND tp.titulo_eleitoral_uf_id LIKE $state";
            }
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND tp.titulo_eleitoral_municipio_id LIKE $city";
            }
        }
        return $where;
    }

    public function list($user, $name = null, $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, DATE_FORMAT(i.created_at, '%d/%m/%Y %H:%i:%s') AS created_at, tp.nome AS name, tp2.id AS indicatorId, 
                    tp2.nome AS indicator, te.sigla AS uf, tci.cidade AS city, IFNULL(e.nome, '-') AS event,
                    (CASE i.origin
                        WHEN 1 THEN 'Tela de Doação'
                        WHEN 2 THEN 'Tela de Filiação'
                        WHEN 4 THEN 'Tela de Evento'
                    END) AS origin
                FROM indicator AS i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_pessoa tp2 ON tp2.id = i.indicator
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tci ON tci.id = tp.titulo_eleitoral_municipio_id 
                LEFT JOIN tb_eventos e ON e.id = i.eventId
                WHERE 1 = 1 AND tp.nome IS NOT NULL {$where}
                GROUP BY i.id ORDER BY i.created_at DESC {$limitSql};";         
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($user, $name = null, $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(i.id)) AS total 
                FROM indicator AS i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tci ON tci.id = tp.titulo_eleitoral_municipio_id 
                LEFT JOIN tb_eventos e ON e.id = i.eventId
                WHERE 1 = 1 AND tp.nome IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listExport($user, $name = null, $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, DATE_FORMAT(i.created_at, '%d/%m/%Y %H:%i:%s') AS created_at, tp.nome AS name, tp2.id AS indicatorId, 
                    tp2.nome AS indicator, te.sigla AS uf, tci.cidade AS city, e.nome AS event,
                    (CASE i.origin
                        WHEN 1 THEN 'Tela de Doação'
                        WHEN 2 THEN 'Tela de Filiação'
                        WHEN 4 THEN 'Tela de Evento'
                    END) AS origin
                FROM indicator AS i
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_pessoa tp2 ON tp2.id = i.indicator
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tci ON tci.id = tp.titulo_eleitoral_municipio_id 
                LEFT JOIN tb_eventos e ON e.id = i.eventId
                WHERE 1 = 1 AND tp.nome IS NOT NULL {$where}
                GROUP BY i.id ORDER BY i.created_at DESC";         
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}