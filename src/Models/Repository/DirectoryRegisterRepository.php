<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\DirectoryRegister;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class DirectoryRegisterRepository extends EntityRepository
{
    public function save(DirectoryRegister $entity): DirectoryRegister
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($id = 0, $level = null, $uf = null, $city = null, &$params): string
    {
        $where = '';
        if ($id) {
            $params[':id'] = $id;
            $where .= " AND directory.id = :id";
        }
        if ($level) {
            $params[':level'] = $level;
            $where .= " AND directory.level = :level";
        }
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND directory.state = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND directory.city = :city";
        }
        return $where;
    }

    public function list($id = 0, $level = null, $uf = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($id, $level, $uf, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT directory.id, directory.state, directory.city,
                (CASE level.name
                    WHEN 'Nacional' THEN 'Nacional'
                    WHEN 'Estadual' THEN 'Diretório Estadual' 
                    WHEN 'Municipal' THEN 'Diretório Municipal'
                    WHEN 'Núcleo Estadual' THEN 'Núcleo Estadual'
                    WHEN 'Núcleo Municipal' THEN 'Núcleo Municipal'
                END) AS levelStr,                 
                directory.level, directory.active, 
                        IF(city.cidade IS NOT NULL, city.cidade, '-') AS cityStr, directory.email,
                        IF(state.sigla IS NOT NULL, state.sigla, '') AS ufStr                     
                FROM directoryRegister as directory
                JOIN  tb_estado as state ON state.id = directory.state
                LEFT JOIN  tb_cidade as city ON city.id = directory.city
                JOIN  accessLevel as level ON level.id = directory.level
                WHERE 1 = 1 {$where}
                ORDER BY level DESC, state ASC  {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($id = 0, $level = null, $uf = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($id, $level, $uf, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(directory.id) AS total                  
                FROM directoryRegister as directory
                LEFT JOIN  tb_estado as state ON state.id = directory.state
                LEFT JOIN  tb_cidade as city ON city.id = directory.city
                JOIN  accessLevel as level ON level.id = directory.level
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}