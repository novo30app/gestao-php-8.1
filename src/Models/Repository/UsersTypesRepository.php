<?php

namespace App\Models\Repository;

use App\Models\Entities\UsersTypes;
use Doctrine\ORM\EntityRepository;

class UsersTypesRepository extends EntityRepository
{
    public function save(UsersTypes $entity):UsersTypes
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND u.name LIKE :name";
        }
        return $where;
    }

    public function list($name = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT u.id, DATE_FORMAT(u.created, '%d/%m/%Y %H:%m:%s') AS created, u.user, u.name, u.active
                FROM usersTypes u
                WHERE u.active = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(u.id)) AS total
                FROM usersTypes u
                WHERE u.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}