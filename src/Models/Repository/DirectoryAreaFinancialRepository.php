<?php

namespace App\Models\Repository;

use Doctrine\ORM\EntityRepository;
use App\Models\Entities\DirectoryAreaFinancial;

class DirectoryAreaFinancialRepository extends EntityRepository
{
    public function save(DirectoryAreaFinancial $entity): DirectoryAreaFinancial
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}