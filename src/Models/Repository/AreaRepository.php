<?php

namespace App\Models\Repository;

use App\Models\Entities\Area;
use App\Models\Entities\Directory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class AreaRepository extends EntityRepository
{
	public function save(Area $entity):Area
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}

	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}

	private function generateWhere(array $filter, &$params): string
	{
		$where = '';
		if ($filter['id']) {
			$params[':id'] = $filter['id'];
			$where .= " AND area.id = :id";
		}
		return $where;
	}

	public function list(array $filter, $limit = null, $offset = null): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT area.id, area.name
                FROM area
                WHERE 1 = 1 {$where}
				ORDER BY name ASC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}

	public function listTotal(array $filter): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(area.id) AS total
                FROM area
                WHERE 1 = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}

    public function delDirectories($id)
    {
        $sql = "DELETE FROM area_directories WHERE area_id = :id";
        $params = [':id' => $id];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }

    public function data($id)
    {
        $params = [':id' => $id];
        return $this->getEntityManager()->createQuery(
            "SELECT a, d
				FROM  App\Models\Entities\Area AS a
				LEFT JOIN a.directories AS d
                WHERE a.id = :id
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function areaByDirectory(Directory $directory)
    {
        $params = [':id' => $directory->getId()];
        return $this->getEntityManager()->createQuery(
            "SELECT a
				FROM  App\Models\Entities\Area AS a
				LEFT JOIN a.directories AS d
                WHERE d.id = :id
                "
        )
            ->setParameters($params)
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
