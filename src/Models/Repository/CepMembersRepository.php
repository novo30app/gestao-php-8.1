<?php

namespace App\Models\Repository;

use App\Models\Entities\CepMembers;
use Doctrine\ORM\EntityRepository;

class CepMembersRepository extends EntityRepository
{
    public function save(CepMembers $entity): CepMembers
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND m.name LIKE :name";
        }
        return $where;
    }

    public function list($name = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT m.id, m.name, DATE_FORMAT(m.dateElected, '%d/%m/%Y') AS dateElected, m.file, m.active
                FROM cepMembers m
                WHERE 1 = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(m.id))
                FROM cepMembers m
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}