<?php

namespace App\Models\Repository;

use App\Models\Entities\Mesoregions;
use App\Models\Entities\UserAdmin;

use Doctrine\ORM\EntityRepository;

class MesoregionsRepository extends EntityRepository
{
    public function save(Mesoregions $entity):Mesoregions
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user = null, $name = null, $state = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND m.name LIKE :name";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND m.state = :state";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_NATIONAL){
            if ($state) {
                $params[':state'] = $state;
                $where .= " AND m.state = :state";
            }
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $state = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $state, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT m.id, m.name, e.estado AS state, DATE_FORMAT(m.created_at, '%d/%m/%Y') AS created_at FROM mesoregions m
                LEFT JOIN tb_estado e ON e.id = m.state
                WHERE 1 = 1 {$where} GROUP BY m.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $state = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $state, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(m.id)) AS total 
                FROM mesoregions m
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function getMesos(UserAdmin $user): array
    {
        $where = '';
        if($user->getLevel() == 2) {
            $where .= " AND m.state = {$user->getState()->getId()}";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM mesoregions m
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function getMyMesos(UserAdmin $user): array
    {
        $where = '';
        if($user->getLevel() == 1) {
            $where .= " AND id IN (SELECT access FROM accessAdmin WHERE userAdmin = {$user->getId()} AND type = 'meso')";
        } else if ($user->getLevel() == 2) {
            $where .= " AND state = {$user->getState()->getId()}";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM mesoregions 
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}