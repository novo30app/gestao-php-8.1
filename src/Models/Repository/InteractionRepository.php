<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\Cabinet;
use App\Models\Entities\Interaction;
use Doctrine\ORM\EntityRepository;

class InteractionRepository extends EntityRepository
{
    public function save(Interaction $entity): Interaction
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateWhere(array $filter, &$params)
    {
        $where = '';
        if ($filter['responsible']) {
            $params[':responsible'] = "%{$filter['responsible']}%";
            $where .= " AND i.responsible LIKE :responsible";
        }
        if ($filter['type']) {
            $params[':type'] = $filter['type'];
            $where .= " AND i.type = :type";
        }
        if ($filter['start']) {
            $params[':start'] = Utils::formatDateForDB($filter['start']);
            $where .= " AND i.date >= :start";
        }
        if ($filter['end']) {
            $params[':end'] = Utils::formatDateForDB($filter['end']);
            $where .= " AND i.date <= :end";
        }
        if ($filter['tags']) {
            $params[':tag'] = $filter['tags'];
            $where .= " AND (SELECT COUNT(it.id) FROM App\Models\Entities\InteractionKeyWord as it 
                            WHERE it.interaction = i.id AND it.keyWord = :tag) > 0";
        }
        return $where;
    }

    public function list(array $filter, $limit, $offset)
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT i FROM  App\Models\Entities\Interaction as i
                WHERE 1 = 1 {$where}
                ORDER BY i.id DESC")
            ->setMaxResults($limit)
            ->setFirstResult($offset * $limit)
            ->execute($params);
    }

    public function listTotal(array $filter)
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        return $this->getEntityManager()->createQuery(
            "SELECT COUNT(i) AS total FROM  App\Models\Entities\Interaction as i             
               WHERE 1 = 1 {$where}")
            ->execute($params)[0]['total'];
    }
}