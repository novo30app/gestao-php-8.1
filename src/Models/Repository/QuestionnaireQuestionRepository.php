<?php

namespace App\Models\Repository;

use App\Models\Entities\QuestionnaireQuestion;
use Doctrine\ORM\EntityRepository;

class QuestionnaireQuestionRepository extends EntityRepository
{
	public function save(QuestionnaireQuestion $entity): QuestionnaireQuestion
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}
}