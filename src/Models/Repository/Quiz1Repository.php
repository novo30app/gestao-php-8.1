<?php

namespace App\Models\Repository;

use App\Models\Entities\Quiz1;
use Doctrine\ORM\EntityRepository;

class Quiz1Repository extends EntityRepository
{

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhereSolicitations(array $filter, &$params)
    {
        $where = '';
        if ($filter['name']) {
            $params[':name'] = "%{$filter['name']}%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($filter['state']) {
            $params[':state'] = $filter['state'];
            $where .= " AND tp.titulo_eleitoral_uf_id = :state";
        }
        if ($filter['city']) {
            $params[':city'] = $filter['city'];
            $where .= " AND tp.titulo_eleitoral_municipio_id = :city";
        }
        return $where;
    }


    public function listSolicitations(array $filer, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereSolicitations($filer, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT quiz1.id, tp.nome as name, te.estado as uf, tc.cidade as city
                FROM quiz1 
                JOIN tb_pessoa AS tp ON tp.id = quiz1.user
                LEFT JOIN tb_estado te ON te.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade tc ON tc.id = tp.titulo_eleitoral_municipio_id
                WHERE 1 = 1 {$where}
                ORDER BY quiz1.id DESC {$limitSql};";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalSolicitations(array $filer): array
    {
        $params = [];
        $where = $this->generateWhereSolicitations($filer, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(tp.id)) AS total 
                FROM quiz1 
                JOIN tb_pessoa AS tp ON tp.id = quiz1.user
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

}