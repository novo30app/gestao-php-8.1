<?php

namespace App\Models\Repository;

use App\Models\Entities\OfxLines;
use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class PaymentRequirementsRepository extends EntityRepository
{
    public function save(PaymentRequirements $entity): PaymentRequirements
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    public function billsToPayList(UserAdmin $userAdmin, ?string $id, $status = 0, ?int $origin = 0, ?string $cpfCnpj = '', ?string $provider = null, ?string $docNumber = null, ?string $type,
                                   ?string   $costCenter = '', ?string $directory = '', ?string $directoryPaying, ?string $dateType = '', ?string $dateBegin = '', ?string $dateEnd = '',
                                   ?string   $accounting = null, ?string $user, ?string $value, ?string $paymentValue, $area = null, $accounted = null, $bbStatus = null,
                                             $limit = null, $offset = null, $conciliation = null): array
    {
        $params = [];

        //join diferente para listagem 'Orçado x Realizado' e 'Contas a Pagar'
        $join = "LEFT JOIN costCenter cc ON cc.id = p.costCenter";
        if ($status == 'ef_or_conc') {
            $join = "LEFT JOIN costCenter cc ON cc.id = p.costCenter
                     LEFT JOIN budgets b ON b.costCenter = cc.id";
        }
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereBillsToPayList($params, $userAdmin, $id, $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $directory, $directoryPaying,
            $dateType, $dateBegin, $dateEnd, $accounting, $user, $value, $paymentValue, $conciliation, $area, $accounted, $bbStatus);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, p.nfValue, DATE_FORMAT(p.paymentDueDate, '%d/%m/%Y') AS dueDate, p.origin, p.attachmentNf, p.attachmentContract, IFNULL(cc.name, '-') AS costCenter,
                    p.attachmentBillet, p.attachmentPayment, p.requestStatus, u.name AS user,
                    pr.cpfCnpj, pr.name AS prName, DATE_FORMAT(p.created, '%d/%m/%Y %H:%i:%s') AS created, p.type, c.doc AS contract, p.beneficiary, p.ofxFiles,
                    REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') as 'paymentValue', IFNULL(dir.nome, '-') AS directory,
                    p.description, (SELECT statusString FROM extractBB WHERE payment = p.id ORDER BY created DESC LIMIT 1) AS statusString
                FROM paymentRequirements AS p    
                JOIN usersAdmin AS u ON u.id = p.user   
                LEFT JOIN tb_diretorio AS dir ON dir.id = p.directory 
                LEFT JOIN tb_estado AS state ON state.id = dir.tb_estado_id
                LEFT JOIN tb_cidade AS city ON city.id = dir.tb_cidade_id
                LEFT JOIN providers AS pr ON pr.id = p.provider
                LEFT JOIN contract AS c ON c.id = p.contract
                LEFT JOIN accountingRecord ar ON ar.payment = p.id
				{$join}
				LEFT JOIN  directoryAccounts dirA ON dirA.id = p.directoryAccount
				LEFT JOIN tb_diretorio AS dir2 ON dir2.id = dirA.directory
                WHERE 1 = 1 {$where}
				GROUP BY p.id
                ORDER BY p.id DESC {$limitSql}";

//		echo $sql . "<br><br>";
//
//		dd($params);

        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function billsToPayListTotal(UserAdmin $userAdmin, ?string $id, $status = 0, ?int $origin = 0, ?string $cpfCnpj = '', ?string $provider = null, ?string $docNumber = null, ?string $type,
                                        ?string   $costCenter = '', ?string $directory = '', ?string $directoryPaying, ?string $dateType, ?string $dateBegin, ?string $dateEnd, ?string $accounting = null,
                                        ?string   $user, ?string $value, ?string $paymentValue, $conciliation = null, $area = null, $accounted = null, $bbStatus = null): array
    {
        $params = [];
        $where = $this->generateWhereBillsToPayList($params, $userAdmin, $id, $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $directory, $directoryPaying,
            $dateType, $dateBegin, $dateEnd, $accounting, $user, $value, $paymentValue, $conciliation, $area, $accounted, $bbStatus);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();

        //join diferente para listagem 'Orçado x Realizado' e 'Contas a Pagar'
        $join = "LEFT JOIN costCenter cc ON cc.id = p.costCenter";
        if ($status == 'ef_or_conc') {
            $join = "LEFT JOIN costCenter cc ON cc.id = p.costCenter
                     LEFT JOIN budgets b ON b.costCenter = cc.id";
        }
        $sql = "SELECT COUNT(DISTINCT (p.id)) AS total
                FROM paymentRequirements AS p    
                JOIN usersAdmin AS u ON u.id = p.user   
                LEFT JOIN tb_diretorio AS dir ON dir.id = p.directory 
                LEFT JOIN tb_estado AS state ON state.id = dir.tb_estado_id
                LEFT JOIN tb_cidade AS city ON city.id = dir.tb_cidade_id
                LEFT JOIN providers AS pr ON pr.id = p.provider
                LEFT JOIN contract AS c ON c.id = p.contract
                LEFT JOIN accountingRecord ar ON ar.payment = p.id
				{$join}
				LEFT JOIN  directoryAccounts dirA ON dirA.id = p.directoryAccount
				LEFT JOIN tb_diretorio AS dir2 ON dir2.id = dirA.directory
				WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhereBillsToPayList(&$params, UserAdmin $user, $id, $status, $origin, $cpfCnpj, $provider, $docNumber = null, $type, $costCenter, $directory, $directoryPaying,
                                                 $dateType, $dateBegin, $dateEnd, $accounting, $use, $value, $paymentValue, $conciliation = null, $area = null, $accounted = null, $bbStatus = null): string
    {
        $where = '';

        if ($status == 'ef_or_conc') {
            $where .= " AND (p.requestStatus = 3 OR p.requestStatus = 7)";
        } elseif ((int)$status >= 0) {
            $params[':status'] = $status;
            $where .= " AND p.requestStatus = :status";
        }
        if ($origin > 0) {
            $params[':origin'] = $origin;
            $where .= " AND p.origin = :origin";
        }
        if ($cpfCnpj) {
            $cpfCnpj = preg_replace("/[^0-9]/", "", $cpfCnpj);
            $params[':cpfCnpj'] = $cpfCnpj;
            $where .= " AND pr.cpfCnpj = :cpfCnpj";
        }
        if ($provider) {
            $params[':provider'] = "%$provider%";
            $where .= " AND pr.name LIKE :provider";
        }
        if ($type) {
            $params[':type'] = $type;
            $where .= " AND p.type = :type";
        }
        if ($docNumber) {
            $params[':docNumber'] = "%$docNumber%";
            $where .= " AND p.nfNumber LIKE :docNumber";
        }
        if ($costCenter) {
            $params[':costCenter'] = $costCenter;
            $where .= " AND cc.name = :costCenter";
        }
        if ($directory) {
            $params[':directory'] = $directory;
            $where .= " AND dir.nome = :directory";
        }
        if ($directoryPaying) {
            $params[':directoryPaying'] = $directoryPaying;
            $where .= " AND dir2.nome = :directoryPaying";
        }
        $dateType = match ($dateType) {
            '2' => 'p.payday',
            '3' => 'ar.competition',
            '4' => 'p.refundDate',
            default => 'p.paymentDueDate'
        };
        if ($dateBegin) {
            $params[':dateBegin'] = $dateBegin;
            $where .= " AND $dateType >= :dateBegin";
        }
        if ($dateEnd) {
            $params[':dateEnd'] = $dateEnd;
            $where .= " AND $dateType <= :dateEnd";
        }
        if ($area) {
            $params[':area'] = $area;
            $where .= " AND p.area = :area";
        }
        if ($use) {
            $params[':user'] = "%$use%";
            $where .= " AND u.name LIKE :user";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':city2'] = $user->getCity()->getId();
            $params[':uf2'] = $user->getState()->getId();
            $where .= " AND (dir.tb_cidade_id = :city2 AND dir.estadual = 'N' AND dir.id NOT IN (108, 102) AND dir.tb_estado_id = :uf2
                            || 
                            p.costCenter = 760)";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf2'] = $user->getState()->getId();
            $params[':user'] = $user->getId();

            $accessEssentJus = "";
            if ($user->getState()->getId() == 18) {
                $accessEssentJus = " || (p.type = 16 AND p.directoryTransfer IN (SELECT id FROM tb_diretorio WHERE tb_estado_id = :uf2))
				            || (p.type = 22 AND p.directory IN (SELECT id FROM tb_diretorio WHERE tb_estado_id = :uf2))";
            }

            $where .= " AND (dir.id NOT IN (108, 102) AND (dir.tb_estado_id = :uf2 ||
                            dir.estadual = 'N' AND dir.tb_cidade_id IN (select access from accessAdmin where type = 'city' and userAdmin = :user)) || 
                            p.costCenter = 760
                            $accessEssentJus)";

        }
        if ($value) {
            $data['value'] = str_replace('.', '', $value);
            $data['value'] = str_replace(',', '.', $data['value']);
            $data['value'] = str_replace('R$', '', $data['value']);
            $data['value'] = (float)trim($data['value']);
            $params[':value'] = $data['value'];
            $where .= match ($type) {
                '16' => " AND p.paymentValue = :value",
                default => " AND p.nfValue = :value",
            };
        }
        if ($paymentValue) {
            $data['paymentValue'] = str_replace('.', '', $paymentValue);
            $data['paymentValue'] = str_replace(',', '.', $data['paymentValue']);
            $data['paymentValue'] = str_replace('R$', '', $data['paymentValue']);
            $data['paymentValue'] = (float)trim($data['paymentValue']);
            $params[':paymentValue'] = $data['paymentValue'];
            $where .= " AND (paymentValue + penalty + fees - discounts) = :paymentValue";
        }
        if ($accounting) {
            $params[':accounting'] = $accounting;
            $where .= " AND ar.status = :accounting";
        }
        if ($accounted != '' && $accounted == 0) {
            $where .= ' AND (SELECT COUNT(a1.id) FROM accountingRecord AS a1 WHERE a1.payment = p.id) = 0 ';
        }
        if ($accounted != '' && $accounted == 1) {
            $where .= ' AND (SELECT COUNT(a1.id) FROM  accountingRecord AS a1 WHERE a1.payment = p.id) > 0 ';
        }
        if ($bbStatus) {
            $bbStatus = explode(",", $bbStatus);
            $bbStatus = array_map(function ($v) {
                return "'" . $v . "'";
            }, $bbStatus);
            $bbStatus = implode(",", $bbStatus);
            $condition = "";
            if (!strpos($bbStatus, 'PAGO') && !strpos($bbStatus, 'AGENDADO')) $condition = "p.requestStatus NOT IN(2, 3) AND";
            $where .= " AND {$condition} (SELECT statusString FROM extractBB WHERE payment = p.id ORDER BY created DESC LIMIT 1) IN ($bbStatus)";
        }
        if ($id) {
            $params = [];
            $where = '';
            $params[':id'] = $id;
            $where .= " AND p.id = :id";
        }
        return $where;
    }

    public function getUsers(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT u.id, u.name
                FROM paymentRequirements AS p    
                JOIN usersAdmin AS u ON u.id = p.user
                GROUP BY p.user ORDER BY u.name ASC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listExport(UserAdmin $userAdmin, ?string $id, ?string $dateType, ?string $dateBegin, string $dateEnd, ?string $accounting, $status = 0, ?int $origin = 0,
                               ?string   $cpfCnpj = '', ?string $provider = '', ?string $docNumber = null, ?string $type, ?string $costCenter, ?string $user, ?string $directory = '', ?string $directoryPaying,
                               ?string   $value, ?string $paymentValue, $area, $accounted = null, $bbStatus = null): array
    {
        $params = [];
        $where = $this->generateWhereBillsToPayList($params, $userAdmin, $id, $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $directory, $directoryPaying, $dateType, $dateBegin, $dateEnd,
            $accounting, $user, $value, $paymentValue, $conciliation = null, $area, $accounted, $bbStatus);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();

        //join diferente para listagem 'Orçado x Realizado' e 'Contas a Pagar'
        $join = "LEFT JOIN costCenter cc ON cc.id = p.costCenter";
        if ($status == 'ef_or_conc') {
            $join = "LEFT JOIN costCenter cc ON cc.id = p.costCenter
                    LEFT JOIN budgets b ON b.costCenter = cc.id
            ";
        }

        $sql = "SELECT p.id, DATE_FORMAT(p.paymentDueDate, '%d/%m/%Y') AS dueDate, u.name AS user, pr.name, pr.cpfCnpj, DATE_FORMAT(p.created, '%d/%m/%Y %H:%i:%s') AS created,
                    REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') as 'paymentValue', IFNULL(dir.nome, '-') AS directory,
                    (CASE p.requestStatus
                            WHEN 0 THEN 'Pagamento Solicitado'
                            WHEN 1 THEN 'Pendência'
                            WHEN 2 THEN 'Pagamento Agendado'
                            WHEN 3 THEN 'Pagamento Efetuado'
                            WHEN 4 THEN 'Aguardando Aprovação'
                            WHEN 5 THEN 'Cancelado'
                            WHEN 6 THEN 'Pagamento Validado'
                            WHEN 7 THEN 'Conciliado'
                            WHEN 8 THEN 'Estornado'
                            WHEN 9 THEN 'Estimável'
                            ELSE 'Desconhecido'
                    END) AS requestStatus,
                    (CASE p.origin
                        WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
                        WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
                        WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
                        WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
                        WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
                        WHEN 6 THEN 'FEFC GERAL'
                        WHEN 7 THEN 'FEFC MULHER'
                        WHEN 8 THEN 'FEFC MULHER NEGRA'
                        WHEN 9 THEN 'FEFC HOMEM NEGRO'
                        WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
                        WHEN 11 THEN 'FEFC INDÍGENA'
                        ELSE 'Desconhecido'
                    END) AS origin,
                    (CASE p.type
                        WHEN 1 THEN 'Hospedagem/Viagens'
                        WHEN 2 THEN 'Taxi'
                        WHEN 3 THEN 'Material Impresso'
                        WHEN 4 THEN 'Combustíveis'
                        WHEN 5 THEN 'Evento'
                        WHEN 6 THEN 'Compra de Ativos'
                        WHEN 7 THEN 'Mão de Obra/Prestação de Serviços'
                        WHEN 8 THEN 'Despesa de Escritório'
                        WHEN 9 THEN 'Contas de Consumo'
                        WHEN 10 THEN 'Gastos com Alimentação'
                        WHEN 11 THEN 'Outros'
                        WHEN 12 THEN 'Locação'
                        WHEN 13 THEN 'Gastos com Impulsionamentos'
                        WHEN 14 THEN 'Serviços Jurídicos'
                        WHEN 15 THEN 'Serviços Contábeis'
                        WHEN 16 THEN 'Repasse/Transferência Diretórios'
                        WHEN 22 THEN 'Repasse/Transferência Candidatos'
                        WHEN 17 THEN 'Viagens e Hospedagem'
                        WHEN 18 THEN 'GRU´s'
                        WHEN 19 THEN 'Cessão'
                        WHEN 20 THEN 'Taxa/Tarifa'
                    END) AS type,
					p.description AS 'Descrição do Documento',
					'U' AS 'Série do Documento',
					'-' AS 'Quantidade/ itens da nota',
					( CASE p.origin
                        WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
                        WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
                        WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
                        WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
                        WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
                        WHEN 6 THEN 'FEFC GERAL'
                        WHEN 7 THEN 'FEFC MULHER'
                        WHEN 8 THEN 'FEFC MULHER NEGRA'
                        WHEN 9 THEN 'FEFC HOMEM NEGRO'
                        WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
                        WHEN 11 THEN 'FEFC INDÍGENA'
						ELSE '-'
					END) AS 'Fonte de Recurso',
					IFNULL(p.statementNumber, '-') AS 'N° de documento do Pagamento',
					IFNULL(CONCAT(dirA.agency, '-', dirA.agencyDigit, '/', dirA.account, '-', dirA.accountDigit), '-') AS 'Conta Bancária',
					(CASE p.paymentMethod
						WHEN 1 THEN 'Boleto'
						WHEN 2 THEN 'Depósito Bancário'
						WHEN 3 THEN 'PIX'
						ELSE '-'
					END) AS 'Forma de pagamento',
                    IFNULL(DATE_FORMAT(p.nfDate, '%d/%m/%Y'), '-') AS 'Data de emissão',
					IFNULL(DATE_FORMAT(p.payDay, '%d/%m/%Y'), '-') AS 'Data de pagamento',
                    IFNULL(DATE_FORMAT(p.refundDate, '%d/%m/%Y'), '-') AS 'Data de estorno',
					REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') AS 'Valor de pagamento',
                    p.nfNumber AS 'Nº do Extrato', cc.name AS costCenter, area.name AS area,
                    IFNULL(DATE_FORMAT(ar.competition, '%d/%m/%Y'), '-'), p.pendency
                FROM paymentRequirements AS p    
                JOIN usersAdmin AS u ON u.id = p.user   
                LEFT JOIN tb_diretorio AS dir ON dir.id = p.directory 
                LEFT JOIN tb_estado AS state ON state.id = dir.tb_estado_id
                LEFT JOIN tb_cidade AS city ON city.id = dir.tb_cidade_id
                LEFT JOIN providers AS pr ON pr.id = p.provider
                LEFT JOIN area ON area.id = p.area
                LEFT JOIN contract AS c ON c.id = p.contract
                LEFT JOIN accountingRecord ar ON ar.payment = p.id
				{$join}
				LEFT JOIN  directoryAccounts dirA ON dirA.id = p.directoryAccount
				LEFT JOIN tb_diretorio AS dir2 ON dir2.id = dirA.directory
                WHERE 1 = 1 {$where} 
				GROUP BY p.id
                ORDER BY p.id DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function exportAccountReport(UserAdmin $userAdmin, string $id, string $dateType, string $dateBegin, string $dateEnd, string $accounting, int $status = 0, int $origin = 0,
                                        string    $cpfCnpj = '', string $provider = '', string $docNumber = null, string $type, string $costCenter, string $user, string $directory = '', string $directoryPaying,
                                        string    $value, string $paymentValue): array
    {
        $params = [];
        $where = $this->generateWhereBillsToPayList($params, $userAdmin, $id, $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $directory,
            $directoryPaying, $dateType, $dateBegin, $dateEnd, $accounting, $user, $value, $paymentValue);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT
                    pr.cpfCnpj AS 'CNPJ Prestador',
                    pr.name AS 'Nome/Razão Social Fornecedor',
                    (CASE 
                        WHEN dir.estadual = 'S' THEN 'Estadual'
                        WHEN dir.municipal = 'S' THEN 'Municipal'
                        ELSE 'Nacional'
                    END) AS 'Esfera Partidária',
                    DATE_FORMAT(p.payDay, '%d/%m/%Y') AS 'Data Emissão/Contratação',
					(CASE p.typeDocument
						WHEN 1 THEN 'Nota Fiscal'
						WHEN 2 THEN 'Recibo'
						ELSE ''
					END),
                    CONCAT(If(ar.formAccounting = 2, 'PAGAMENTO: ', ''), ' NF: ', p.nfNumber, ' FORNECEDOR: ', pr.name, ' CNPJ: ', pr.cpfCnpj) AS 'Descrição Gasto',
                    '' AS 'Descrição Outros',
                    '' AS 'Lançamento',
                    DATE_FORMAT(ar.competition, '%d/%m/%Y %H:%i:%s') AS 'Data',
                    IFNULL(c.account, 'M') AS 'Débito',
                    IFNULL(cc.account, 'M') AS 'Crédito',
                    (CASE 
						WHEN aa.value IS NULL THEN REPLACE(REPLACE(REPLACE(FORMAT(aa.creditValue, 2), '.', '@'), ',', '.'), '@', ',')
                        WHEN aa.creditValue IS NULL THEN REPLACE(REPLACE(REPLACE(FORMAT(aa.value, 2), '.', '@'), ',', '.'), '@', ',')
                        ELSE REPLACE(REPLACE(REPLACE(FORMAT(aa.value, 2), '.', '@'), ',', '.'), '@', ',')
					END) AS 'Valor',
					'' AS 'Histórico Padrão',
                    ar.comment AS 'Complemento',
                    '' AS 'CCDB',
                    '' AS 'CCCR',
                    '' AS 'CNPJ',
					IF(ar.status = 1, 'Aprovado', 'Pendência') AS accountingStatus,
                    p.nfNumber,
					REPLACE(REPLACE(REPLACE(FORMAT(p.penalty, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.fees, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.discounts, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.inss, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.irrf, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.csll, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.cofins, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.pispasep, 2), '.', '@'), ',', '.'), '@', ','),
					c.description, c.reduced,
					ar.comment, 'U', '1',
					( CASE p.origin
                        WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
                        WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
                        WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
                        WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
                        WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
                        WHEN 6 THEN 'FEFC GERAL'
                        WHEN 7 THEN 'FEFC MULHER'
                        WHEN 8 THEN 'FEFC MULHER NEGRA'
                        WHEN 9 THEN 'FEFC HOMEM NEGRO'
                        WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
                        WHEN 11 THEN 'FEFC INDÍGENA'
						ELSE '-'
					END), 
					'', 
					CONCAT((CASE dia.nickName
							WHEN 3 THEN 'OR-Ordinário-'
							WHEN 4 THEN 'FP-Ordinário-'
						END),'Bc:001 Ag:', dia.agency, '-', dia.agencyDigit, ' Ct:', dia.account, '-', dia.accountDigit),
					'Transf. eletrônica / Débito / Boleto / Pix',
					(CASE p.paymentMethod
						WHEN 11 THEN 'Pix'
						ELSE 'Transferência Eletrônica de Valores'
					END),
					IFNULL(DATE_FORMAT(p.payDay, '%d/%m/%Y'), '-') AS 'Data de pagamento',
					REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') AS 'Valor de pagamento'
                FROM accountingRecord ar
                LEFT JOIN accountingAccounts aa ON aa.accountingRecord = ar.id
                LEFT JOIN chartOfAccounts c ON c.id = aa.account
				LEFT JOIN chartOfAccounts cc ON cc.id = aa.creditAccount
                LEFT JOIN paymentRequirements p ON p.id = ar.payment
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
				LEFT JOIN directoryAccounts dia ON dia.id = p.directoryAccount 		
                LEFT JOIN directoryAccounts dirB ON dirB.id = p.directoryAccount
				LEFT JOIN tb_diretorio AS dir2 ON dir2.id = dirB.directory 		
                WHERE ar.formAccounting != 0 {$where}
				UNION ALL
				SELECT
                    pr.cpfCnpj AS 'CNPJ Prestador',
                    pr.name AS 'Nome/Razão Social Fornecedor',
                    (CASE 
                        WHEN dir.estadual = 'S' THEN 'Estadual'
                        WHEN dir.municipal = 'S' THEN 'Municipal'
                        ELSE 'Nacional'
                    END) AS 'Esfera Partidária',
                    DATE_FORMAT(p.payDay, '%d/%m/%Y') AS 'Data Emissão/Contratação',
					(CASE p.typeDocument
						WHEN 1 THEN 'Nota Fiscal'
						WHEN 2 THEN 'Recibo'
						ELSE ''
					END),
					CONCAT('NF: ', p.nfNumber, ' FORNECEDOR: ', pr.name, ' CNPJ: ', pr.cpfCnpj) AS 'Descrição Gasto',
                    '' AS 'Descrição Outros',
                    '' AS 'Lançamento',
                    DATE_FORMAT(p.payday, '%d/%m/%Y %H:%i:%s') AS 'Data',
                    (SELECT account FROM chartOfAccounts WHERE id = con.account) AS 'Débito',
					(SELECT account FROM chartOfAccounts WHERE id = pr.accountCredit) AS 'Crédito',
                    p.nfValue AS 'Valor',
					'' AS 'Histórico Padrão',
                    '' AS 'Complemento',
                    '' AS 'CCDB',
                    '' AS 'CCCR',
                    '' AS 'CNPJ',
					'' AS accountingStatus,
                    p.nfNumber,
					REPLACE(REPLACE(REPLACE(FORMAT(p.penalty, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.fees, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.discounts, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.inss, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.irrf, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.csll, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.cofins, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.pispasep, 2), '.', '@'), ',', '.'), '@', ','),
					(SELECT description FROM chartOfAccounts WHERE id = con.account) AS description,
                    (SELECT reduced FROM chartOfAccounts WHERE id = con.account) AS reduced,
					'', 'U', '1',
					( CASE p.origin
                        WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
                        WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
                        WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
                        WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
                        WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
                        WHEN 6 THEN 'FEFC GERAL'
                        WHEN 7 THEN 'FEFC MULHER'
                        WHEN 8 THEN 'FEFC MULHER NEGRA'
                        WHEN 9 THEN 'FEFC HOMEM NEGRO'
                        WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
                        WHEN 11 THEN 'FEFC INDÍGENA'
						ELSE '-'
					END),
					'',
					CONCAT((CASE dia.nickName
							WHEN 3 THEN 'OR-Ordinário-'
							WHEN 4 THEN 'FP-Ordinário-'
						END),'Bc:001 Ag:', dia.agency, '-', dia.agencyDigit, ' Ct:', dia.account, '-', dia.accountDigit),
					'Transf. eletrônica / Débito / Boleto / Pix',
					(CASE p.paymentMethod
						WHEN 11 THEN 'Pix'
						ELSE 'Transferência Eletrônica de Valores'
					END),
					IFNULL(DATE_FORMAT(p.payDay, '%d/%m/%Y'), '-') AS 'Data de pagamento',
					REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') AS 'Valor de pagamento'
                FROM paymentRequirements p
				LEFT JOIN tb_diretorio dir ON dir.id = p.directory
				LEFT JOIN directoryAccounts dia ON dia.id = p.directoryAccount 
                JOIN providers pr ON pr.id = p.provider
                JOIN contract con ON con.id = p.contract
                LEFT JOIN directoryAccounts dirB ON dirB.id = p.directoryAccount
				LEFT JOIN tb_diretorio AS dir2 ON dir2.id = dirB.directory 
                WHERE con.account IS NOT NULL AND pr.account IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function exportFiscalReport(UserAdmin $userAdmin, string $id, string $dateType, string $dateBegin, string $dateEnd, string $accounting, int $status = 0, int $origin = 0,
                                       string    $cpfCnpj = '', string $provider = '', string $docNumber = null, string $type, string $costCenter, string $user, string $directory = '', string $directoryPaying, string $value, string $paymentValue): array
    {
        $params = [];
        $where = $this->generateWhereBillsToPayList($params, $userAdmin, $id, $status, $origin, $cpfCnpj, $provider, $docNumber, $type, $costCenter, $directory, $directoryPaying, $dateType, $dateBegin,
            $dateEnd, $accounting, $user, $value, $paymentValue);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT
                    '' AS 'Tipo de Registro',
                    '' AS 'Nº NFS-e',
                    '' AS 'Data Hora NFE',
                    '' AS 'Código de Verificação da NFS-e',
                    '' AS 'Tipo de RPS',
                    '' AS 'Série do RPS',
                    '' AS 'Número do RPS',
                    '' AS 'Data do Fato Gerador',
                    '' AS 'Inscrição Municipal do Prestador',
                    '' AS 'Indicador de CPF/CNPJ do Prestador',
                    pr.cpfCnpj AS 'CPF/CNPJ do Prestador',
                    pr.name AS 'Razão Social do Prestador',
                    '' AS 'Tipo do Endereço do Prestador',
                    '' AS 'Endereço do Prestador',
                    '' AS 'Número do Endereço do Prestador',
                    '' AS 'Complemento do Endereço do Prestador',
                    '' AS 'Bairro do Prestador',
                    '' AS 'Cidade do Prestador',
                    '' AS 'UF do Prestador',
                    '' AS 'CEP do Prestador',
                    '' AS 'Email do Prestador',
                    '' AS 'Opção Pelo Simples',
                    '' AS 'Situação da Nota Fiscal',
                    '' AS 'Data de Cancelamento',
                    '' AS 'Nº da Guia',
                    '' AS 'Data de Quitação da Guia Vinculada a Nota Fiscal',
                    REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') AS 'Valor dos Serviços',
                    '' AS 'Valor das Deduções',
                    '' AS 'Código do Serviço Prestado na Nota Fiscal',
                    '' AS 'Alíquota',
                    '' AS 'ISS devido',
                    '' AS 'Valor do Crédito',
                    '' AS 'ISS Retido',
                    '' AS 'Indicador de CPF/CNPJ do Tomador',
                    '' AS 'CPF/CNPJ do Tomador',
                    '' AS 'Inscrição Municipal do Tomador',
                    '' AS 'Inscrição Estadual do Tomador',
                    '' AS 'Razão Social do Tomador',
                    '' AS 'Tipo do Endereço do Tomador',
                    '' AS 'Endereço do Tomador',
                    '' AS 'Número do Endereço do Tomador',
                    '' AS 'Complemento do Endereço do Tomador',
                    '' AS 'Bairro do Tomador',
                    '' AS 'Cidade do Tomador',
                    '' AS 'UF do Tomador',
                    '' AS 'CEP do Tomado',
                    '' AS 'Email do Tomador',
                    '' AS 'Nº NFS-e Substituta',
                    '' AS 'ISS recolhido',
                    '' AS 'ISS a recolher',
                    '' AS 'Indicador de CPF/CNPJ do Intermediário',
                    '' AS 'CPF/CNPJ do Intermediário',
                    '' AS 'Inscrição Municipal do Intermediário',
                    '' AS 'Razão Social do Intermediário',
                    '' AS 'Repasse do Plano de Saúde',
                    REPLACE(REPLACE(REPLACE(FORMAT(p.pispasep, 2), '.', '@'), ',', '.'), '@', ',') AS 'PIS/PASEP',
                    REPLACE(REPLACE(REPLACE(FORMAT(p.cofins, 2), '.', '@'), ',', '.'), '@', ',') AS 'COFINS',
                    REPLACE(REPLACE(REPLACE(FORMAT(p.inss, 2), '.', '@'), ',', '.'), '@', ',') AS 'INSS',
                    '' AS 'IR',
                    REPLACE(REPLACE(REPLACE(FORMAT(p.csll, 2), '.', '@'), ',', '.'), '@', ',') AS 'CSLL',
                    '' AS 'Carga tributária: Valor',
                    '' AS 'Carga tributária: Porcentagem',
                    '' AS 'Carga tributária: Fonte',
                    '' AS 'CEI',
                    '' AS 'Matrícula da Obra',
                    '' AS 'Município Prestação - cód. IBGE',
                    '' AS 'Situação do Aceite',
                    '' AS 'Encapsulamento',
                    '' AS 'Valor Total Recebido',
                    '' AS 'Tipo de Consolidação',
                    '' AS 'Nº NFS-e Consolidada',
                    '' AS 'Campo Reservado',
                    p.description AS 'Discriminação dos Serviços',
					REPLACE(REPLACE(REPLACE(FORMAT(p.penalty, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.fees, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.discounts, 2), '.', '@'), ',', '.'), '@', ','),
					REPLACE(REPLACE(REPLACE(FORMAT(p.irrf, 2), '.', '@'), ',', '.'), '@', ',')
                FROM accountingRecord ar
                LEFT JOIN accountingAccounts aa ON aa.accountingRecord = ar.id
                LEFT JOIN chartOfAccounts c ON c.id = aa.account
                LEFT JOIN paymentRequirements p ON p.id = ar.payment
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
                LEFT JOIN directoryAccounts dirB ON dirB.id = p.directoryAccount
				LEFT JOIN tb_diretorio AS dir2 ON dir2.id = dirB.directory 
                WHERE ar.formAccounting != 0 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhereApproved(&$params, $filter): string
    {
        $where = '';
        if ($filter['cpfCnpj']) {
            $cpfCnpj = preg_replace("/[^0-9]/", "", $filter['cpfCnpj']);
            $params[':cpfCnpj'] = $cpfCnpj;
            $where .= " AND pr.cpfCnpj = :cpfCnpj";
        }
        if ($filter['provider']) {
            $provider = $filter['provider'];
            $params[':provider'] = "%$provider%";
            $where .= " AND pr.name LIKE :provider";
        }
        if ($filter['costCenter']) {
            $params[':costCenter'] = $filter['costCenter'];
            $where .= " AND p.costCenter = :costCenter";
        }
        if ($filter['directory']) {
            $params[':directory'] = $filter['directory'];
            $where .= " AND dir.nome = :directory";
        }
        if ($filter['user']) {
            $params[':user'] = $filter['user'];
            $where .= " AND u.name = :user";
        }
        return $where;
    }

    public function listApproved(UserAdmin $userAdmin, array $filter, $limit = null, $offset = null): array
    {
        $params = [];
        $params[':responsible'] = $userAdmin->getId();
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereApproved($params, $filter);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, p.nfValue, DATE_FORMAT(p.paymentDueDate, '%d/%m/%Y') AS dueDate, p.origin, u.name AS user, p.approved,
                    pr.cpfCnpj, pr.name AS prName, DATE_FORMAT(p.created, '%d/%m/%Y %H:%i:%s') AS created, p.type, p.cpf,
                    IFNULL(dir.nome, '-') AS directory, REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',') as 'paymentValue',
                    costCenter.name AS ccName, p.attachmentPayment, p.attachmentContract, p.attachmentBillet, p.attachmentNf, a.name AS area
                FROM paymentRequirements AS p
                JOIN usersAdmin AS u ON u.id = p.user
				LEFT JOIN contract ON contract.id = p.contract
                LEFT JOIN costCenter AS cc ON cc.id = contract.costCenter
				LEFT JOIN costCenter ON costCenter.id = p.costCenter
				LEFT JOIN tb_diretorio AS dir ON dir.id = p.directory
                LEFT JOIN tb_estado AS state ON state.id = dir.tb_estado_id
                LEFT JOIN tb_cidade AS city ON city.id = dir.tb_cidade_id
                LEFT JOIN providers AS pr ON pr.id = p.provider
                LEFT JOIN area AS a ON a.id = p.area
                WHERE costCenter.responsible = :responsible AND p.requestStatus = 4 {$where}
                ORDER BY p.id DESC {$limitSql}";

        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotalApproved(UserAdmin $userAdmin, array $filter): array
    {
        $params = [];
        $params[':responsible'] = $userAdmin->getId();
        $where = $this->generateWhereApproved($params, $filter);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(p.id) AS total
                FROM paymentRequirements AS p
			    JOIN usersAdmin AS u ON u.id = p.user
				LEFT JOIN contract ON contract.id = p.contract
                LEFT JOIN costCenter AS cc ON cc.id = contract.costCenter
				LEFT JOIN costCenter ON costCenter.id = p.costCenter
				LEFT JOIN tb_diretorio AS dir ON dir.id = p.directory
                LEFT JOIN tb_estado AS state ON state.id = dir.tb_estado_id
                LEFT JOIN tb_cidade AS city ON city.id = dir.tb_cidade_id
                LEFT JOIN providers AS pr ON pr.id = p.provider
                LEFT JOIN area AS a ON a.id = p.area
                WHERE costCenter.responsible = :responsible AND p.requestStatus = 4 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }


    public function list(array $filter, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT directory.nome AS directory_name, paymentRequirements.id, paymentRequirements.description,
				paymentRequirements.directory, paymentRequirements.directoryAccount, paymentRequirements.paymentValue,
				paymentRequirements.paymentDueDate, paymentRequirements.category
                FROM paymentRequirements
                LEFT JOIN tb_diretorio AS directory On directory.id = paymentRequirements.directory
                WHERE 1 = 1 {$where}
                ORDER BY paymentDueDate desc {$limitSql}
               ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(paymentRequirements.id) AS total
                FROM paymentRequirements
                LEFT JOIN tb_diretorio AS directory On directory.id = paymentRequirements.directory
                WHERE 1 = 1  {$where}
               ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function balanceLogIn(array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT  SUM(paymentRequirements.paymentValue) AS logIn
                FROM paymentRequirements
                LEFT JOIN tb_diretorio AS directory On directory.id = paymentRequirements.directory
                WHERE 1 = 1 AND paymentRequirements.category = 1  {$where}
               ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function balanceLogOut(array $filter): array
    {
        $params = [];
        $where = $this->generateWhere($filter, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT SUM(paymentRequirements.paymentValue) AS logOut
                FROM paymentRequirements
                LEFT JOIN tb_diretorio AS directory On directory.id = paymentRequirements.directory
                WHERE 1 = 1 AND paymentRequirements.category = 0  {$where}
               ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhere(array $filter, &$params): string
    {
        $where = '';
        if ($filter['account']) {
            if (strpos($filter['account'], ',') === false) {
                $params[':directoryAccount'] = $filter['account'];
                $where .= " AND paymentRequirements.directoryAccount = :directoryAccount";
            } else {
                $where .= " AND (";
                $arr = explode(',', $filter['account']);
                $i = 1;
                foreach ($arr as $item) {
                    if ($i > 1) $where .= " OR ";
                    $params[":directoryAccount{$i}"] = $item;
                    $where .= " paymentRequirements.directoryAccount = :directoryAccount{$i}";
                    $i++;
                }
                $where .= ")";
            }
        }
        if ($filter['directory']) {
            $params[':directory'] = $filter['directory'];
            $where .= " AND directory.nome = :directory";
        }
        if ($filter['start']) {
            $start = \DateTime::createFromFormat('d/m/Y', $filter['start']);
            $params[':start'] = $start->format('Y-m-d 00:00');
            $where .= " AND paymentRequirements.paymentDueDate >= :start";
        }
        if ($filter['end']) {
            $end = \DateTime::createFromFormat('d/m/Y', $filter['end']);
            $params[':end'] = $end->format('Y-m-d 23:59');
            $where .= " AND paymentRequirements.paymentDueDate <= :end";
        }
        if ($filter['status']) {
            $params[':status'] = $filter['status'];
            $where .= match ($filter['status']) {
                6 => " AND paymentRequirements.ofxFiles IS NOT NULL",
                default => " AND paymentRequirements.requestStatus = :status",
            };
        }
        return $where;
    }

    public function getPayments(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, DATE_FORMAT(p.payDay, '%d/%m/%Y') AS payDay, 
                    CONCAT('R$ ',REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',')) as paymentValue,
                    p.description, dir.nome AS directory, pr.cpfCnpj AS providerCpfCnpj, pr.name AS providerName, con.id AS contract, 
                    p.attachmentNf AS attachmentNf, p.attachmentContract AS attachmentContract, p.attachmentBillet AS attachmentBillet,
                    p.attachmentPayment AS attachmentPayment,
                    (CASE p.origin
                        WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
                        WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
                        WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
                        WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
                        WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
                        WHEN 6 THEN 'FEFC GERAL'
                        WHEN 7 THEN 'FEFC MULHER'
                        WHEN 8 THEN 'FEFC MULHER NEGRA'
                        WHEN 9 THEN 'FEFC HOMEM NEGRO'
                        WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
                        WHEN 11 THEN 'FEFC INDÍGENA'
                        ELSE null
                    END) AS origin,
                    IF(p.id = 1122, 'Boleto', 
                    (CASE p.typeDocument
                        WHEN 1 THEN 'Nota Fiscal'
                        WHEN 2 THEN 'Recibo'
                        WHEN 3 THEN 'Guias/impostos'
                        WHEN 4 THEN 'Tarifas'
                        WHEN 5 THEN 'Transf'
                        WHEN 6 THEN 'Boleto'
                        WHEN 7 THEN 'Contrato'
                        WHEN 8 THEN 'Fatura'
                        WHEN 9 THEN 'Outros'
                        ELSE ''
                    END)) AS typeDocument,
                    p.statementNumber AS extract, DATE_FORMAT(ar.competition, '%d/%m/%Y') AS createdAt, p.nfNumber AS docNumber, 
                    ch.descriptionSPCA AS descriptionSPCA
                FROM paymentRequirements p
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
                LEFT JOIN contract con ON con.id = p.contract
				LEFT JOIN accountingRecord ar ON ar.payment = p.id
				LEFT JOIN accountingAccounts aa ON aa.accountingRecord = ar.id
                LEFT JOIN chartOfAccounts ch ON ch.id =  aa.account
                LEFT JOIN tempImportDebitSPCA temp ON temp.payment = p.id
                WHERE p.requestStatus IN(7) 
                    AND p.directory = 1 
                    AND p.payDay > '2024-01-01' 
                    AND p.payDay <= '2024-12-31' 
                    AND p.origin = 4 
                    AND ar.formAccounting = 1
                GROUP BY p.id";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function requerimentsByOfx(OfxLines $ofx)
    {
        $params = [
            ':payDay' => $ofx->getDate(),
            ':paymentValue' => $ofx->getValue(),
            ':category' => 0,
            ':requestStatus' => [0, 2, 3, 4, 6]
        ];
        if ($ofx->getDate()->format('Y-m-d') < '2024-01-01') {
            $params[':directory'] = $ofx->getDirectory()->getId();
            $where = ' AND p.directory = :directory';
        } else {
            $params[':directoryAccount'] = $ofx->getOfx()->getDirectoryAccount()->getId();
            $where = ' AND p.directoryAccount = :directoryAccount';
        }
        if ($ofx->getUniqueIdentifier() == 'API') {
            $where .= ' AND provider.cpfCnpj = :cpf';
            $params[':cpf'] = $ofx->getExtractNumber();
        }
        //verificar aqui, acho que o paymentValue já está sem o valor do desconto na hora de salvar
        return $this->getEntityManager()->createQuery(
            "SELECT p
                FROM App\Models\Entities\PaymentRequirements as p
                LEFT JOIN p.provider AS provider
                WHERE p.payDay = :payDay AND ((p.paymentValue + p.penalty + p.fees - p.discounts) = :paymentValue 
                                                OR p.paymentValue = :paymentValue) 
                AND p.category = :category AND p.requestStatus IN (:requestStatus) {$where}"
        )
            ->execute($params);

    }


    public function listTransparencyPortal(array $filter, int $limit = 20, int $offset = 0): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereTransparencyPortal($params, $filter);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DATE_FORMAT(p.payDay, '%d/%m/%Y') AS payDay, 
                    CONCAT('R$ ',REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',')) as paymentValue,
                    p.description, dir.nome AS directory, pr.cpfCnpj AS providerCpfCnpj, pr.name AS providerName, 
                    IFNULL(IFNULL(con.id, attachmentContract), '') AS contract,  
                    IFNULL(p.attachmentNf, '') AS attachmentNf, IFNULL(p.attachmentBillet, '') AS attachmentBillet,
                    (CASE p.origin
                        WHEN 1 THEN 'Campanha Eleitoral'
                        WHEN 2 THEN 'Política da Mulher'
                        WHEN 3 THEN 'Outros Recursos'
                        WHEN 4 THEN 'Fundo Partidário'
                        ELSE ''
                    END) AS origin
                FROM paymentRequirements p
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
                LEFT JOIN contract con ON con.id = p.contract
                WHERE p.requestStatus = 3 AND p.payDay BETWEEN '2023-04-01' AND '2024-11-30' AND dir.nome NOT LIKE '%Núcleo%' AND dir.ativo = 'S'  {$where}
                ORDER BY p.payDay DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTransparencyPortalTotal(array $filter): array
    {
        $params = [];
        $where = $this->generateWhereTransparencyPortal($params, $filter);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(p.id)) AS total 
                FROM paymentRequirements p
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
                LEFT JOIN contract con ON con.id = p.contract
                WHERE p.requestStatus = 3 AND p.payDay BETWEEN '2023-04-01' AND '2024-11-30' AND dir.nome NOT LIKE '%Núcleo%' AND dir.ativo = 'S' {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }


    private function generateWhereTransparencyPortal(&$params, array $filter): string
    {
        $where = '';
        if ($filter['payDayBegin']) {
            $params[':payDayBegin'] = $filter['payDayBegin'];
            $where .= " AND p.payDay >= :payDayBegin";
        }
        if ($filter['payDayEnd']) {
            $params[':payDayEnd'] = $filter['payDayEnd'];
            $where .= " AND p.payDay <= :payDayEnd";
        }
        if ($filter['paymentValue']) {
            $data['paymentValue'] = str_replace('.', '', $filter['paymentValue']);
            $data['paymentValue'] = str_replace(',', '.', $data['paymentValue']);
            $data['paymentValue'] = str_replace('R$', '', $data['paymentValue']);
            $data['paymentValue'] = (float)trim($data['paymentValue']);
            $params[':paymentValue'] = $data['paymentValue'];
            $where .= " AND p.paymentValue = :paymentValue";
        }
        if ($filter['origin']) {
            $params[':origin'] = $filter['origin'];
            $where .= " AND p.origin = :origin";
        }
        if ($filter['directory']) {
            $params[':directory'] = $filter['directory'];
            $where .= " AND p.directory = :directory";
        }
        if ($filter['description']) {
            $params[':description'] = "%{$filter['description']}%";
            $where .= " AND p.description LIKE :description";
        }
        if ($filter['providerCnpj']) {
            $providerCnpj = preg_replace("/[^0-9]/", "", $filter['providerCnpj']);
            $params[':cpfCnpj'] = $providerCnpj;
            $where .= " AND pr.cpfCnpj = :cpfCnpj";
        }
        if ($filter['providerName']) {
            $params[':name'] = "%{$filter['providerName']}%";
            $where .= " AND pr.name LIKE :name";
        }
        return $where;
    }


}
