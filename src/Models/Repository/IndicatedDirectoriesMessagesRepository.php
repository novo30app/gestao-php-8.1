<?php

namespace App\Models\Repository;

use App\Models\Entities\IndicatedDirectoriesMessages;
use Doctrine\ORM\EntityRepository;

class IndicatedDirectoriesMessagesRepository extends EntityRepository
{
    public function save(IndicatedDirectoriesMessages $entity): IndicatedDirectoriesMessages
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}