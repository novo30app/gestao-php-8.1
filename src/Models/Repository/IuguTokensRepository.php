<?php

namespace App\Models\Repository;

use App\Models\Entities\IuguTokens;
use Doctrine\ORM\EntityRepository;

class IuguTokensRepository extends EntityRepository
{
    public function save(IuguTokens $entity) {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}