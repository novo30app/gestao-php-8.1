<?php

namespace App\Models\Repository;

use App\Models\Entities\Form;
use App\Models\Entities\User;
use Doctrine\ORM\EntityRepository;

class FormRepository extends EntityRepository
{
    public function save(Form $entity): Form
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($email, $uf, $city, &$params): string
    {
        $where = '';
        $where = '';
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND form.email LIKE :email";
        }
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND form.state = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND form.city = :city";
        }
        return $where;
    }


    public function list($email, $uf, $city, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($email, $uf, $city, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT form.email, tb_estado.estado AS state, tb_cidade.cidade AS city, form.id               
                FROM form 
                JOIN tb_estado ON tb_estado.id = form.state
                JOIN tb_cidade ON tb_cidade.id = form.city
                WHERE 1 = 1 {$where}
                ORDER BY form.id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($email, $uf, $city): array
    {
        $params = [];
        $where = $this->generateWhere($email, $uf, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(form.id) AS total                  
                FROM form
                JOIN tb_estado ON tb_estado.id = form.state
                JOIN tb_cidade ON tb_cidade.id = form.city
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}