<?php

namespace App\Models\Repository;

use App\Models\Entities\SubArea;
use Doctrine\ORM\EntityRepository;

class SubAreaRepository extends EntityRepository
{
	public function save(SubArea $entity):SubArea
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}
	
	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}
	
	private function generateWhere(array $filter, &$params): string
	{
		$where = '';
		if ($filter['id']) {
			$params[':id'] = $filter['id'];
			$where .= " AND subArea.id = :id";
		}
		return $where;
	}
	
	public function list(array $filter, $limit = null, $offset = null): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT subArea.id, subArea.name, subArea.area, area.name AS nameArea
                FROM subArea
				LEFT JOIN area ON area.id = subArea.area
                WHERE 1 = 1 {$where}
				ORDER BY name ASC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function listTotal(array $filter): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(subArea.id) AS total
                FROM subArea
				LEFT JOIN area ON area.id = subArea.area
                WHERE 1 = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}
}