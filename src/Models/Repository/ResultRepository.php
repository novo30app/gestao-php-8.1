<?php

namespace App\Models\Repository;

use App\Models\Entities\Result;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ResultRepository extends EntityRepository
{
    public function save(Result $entity): Result
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }


}
