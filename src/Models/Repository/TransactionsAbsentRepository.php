<?php

namespace App\Models\Repository;

use App\Models\Entities\TransactionsAbsent;
use Doctrine\ORM\EntityRepository;

class TransactionsAbsentRepository extends EntityRepository
{
    public function save(TransactionsAbsent $entity): TransactionsAbsent
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getTransactionsAbsent(): array
	{
		$params = [];
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT * FROM transactionsAbsent
                WHERE DATE_FORMAT(paid_at, '%Y/%m/%d') >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
}