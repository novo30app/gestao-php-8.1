<?php

namespace App\Models\Repository;

use App\Models\Entities\SignatureDocSigned;
use Doctrine\ORM\EntityRepository;

class SignatureDocSignedRepository extends EntityRepository
{
    public function save(SignatureDocSigned $entity): SignatureDocSigned
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
	
	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}
	
	private function generateWhere($name, $email, &$params): string
	{
		$where = '';
		if ($name) {
			$params[':name'] = "%$name%";
			$where .= " AND signatureDocSigned.name LIKE :name";
		}
		if ($email) {
			$params[':email'] = "%$email%";
			$where .= " AND signatureDocSigned.email LIKE :email";
		}
		return $where;
	}
	
	public function list($id, $name, $email, $limit = null, $offset = null): array
	{
		$params = [];
		$params[':doc'] = $id;
		$where = $this->generateWhere($name, $email, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT  signatureDocSigned.name, signatureDocSigned.email, DATE_FORMAT(signatureDocSigned.created, '%d/%m/%Y %H:%i:%s') as created,
       			signatureDocSigned.signed, DATE_FORMAT(signatureDocSigned.signedIn, '%d/%m/%Y %H:%i:%s') as date, signatureDocSigned.id, signatureDocSigned.hash
                FROM signatureDocSigned
                WHERE signatureDocSigned.signatureDoc = :doc  {$where} ORDER BY signatureDocSigned.id DESC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function listTotal($id, $name, $email): array
	{
		$params = [];
		$params[':doc'] = $id;
		$where = $this->generateWhere($name, $email, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(signatureDocSigned.id) AS total
                FROM signatureDocSigned
                WHERE signatureDocSigned.signatureDoc = :doc  {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}
}