<?php

namespace App\Models\Repository;

use App\Models\Entities\KeyWord;
use Doctrine\ORM\EntityRepository;

class KeyWordRepository extends EntityRepository
{
    public function save(KeyWord $entity): KeyWord
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}