<?php

namespace App\Models\Repository;

use App\Models\Entities\MesoregionsCities;
use Doctrine\ORM\EntityRepository;

class MesoregionsCitiesRepository extends EntityRepository
{
    public function save(MesoregionsCities $entity):MesoregionsCities
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function list(string $id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ci.id, ci.cidade FROM mesoregionsCities m
                LEFT JOIN tb_cidade ci ON ci.id = m.city
                WHERE m.mesoregion = :id";
        $rows = $pdo->prepare($sql)->execute([':id' => $id]);
        return $rows->fetchAllAssociative();
    }

    public function deleteCities(string $mesoId): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM mesoregionsCities
                WHERE mesoregion = :meso";
        $rows = $pdo->prepare($sql)->execute([':meso' => $mesoId]);
    }
}