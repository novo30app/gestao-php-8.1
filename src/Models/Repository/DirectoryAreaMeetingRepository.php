<?php

namespace App\Models\Repository;

use Doctrine\ORM\EntityRepository;
use App\Models\Entities\DirectoryAreaMeeting;

class DirectoryAreaMeetingRepository extends EntityRepository
{
    public function save(DirectoryAreaMeeting $entity): DirectoryAreaMeeting
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($date = null, $theme = null, $subject = null, $sector = null, &$params)
    {
        $where = '';
        if ($date) {
            $params[':date'] = "%$date%";
            $where .= " AND m.date LIKE :date";
        }
        if ($theme) {
            $params[':theme'] = "%$theme%";
            $where .= " AND m.theme LIKE :theme";
        }
        if ($subject) {
            $params[':subject'] = "%$subject%";
            $where .= " AND m.subject LIKE :subject";
        }
        if ($sector) {
            $params[':sector'] = $sector;
            $where .= " AND m.sector = :sector";
        }
        return $where;
    }

    public function list($date = null, $theme = null, $subject = null, $sector = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($date, $theme, $subject, $sector, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT m.id, m.theme, ifnull(m.subject, '') as subject, DATE_FORMAT(m.date, '%d/%m/%Y') as date, m.file, m.media, m.status,
                sectors.name AS sector
                FROM directoryAreaMeetings as m
                LEFT JOIN sectors ON sectors.id = m.sector
                WHERE 1 = 1 {$where}
                GROUP BY m.id ORDER BY m.date desc  {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($date = null, $theme = null, $subject = null, $sector = null): array
    {
        $params = [];
        $where = $this->generateWhere($date, $theme, $subject, $sector, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(m.id)) AS total 
                FROM directoryAreaMeetings as m
                LEFT JOIN sectors ON sectors.id = m.sector
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}
