<?php

namespace App\Models\Repository;

use App\Models\Entities\Ombudsman;
use Doctrine\ORM\EntityRepository;

class OmbudsmanRepository extends EntityRepository
{
    public function save(Ombudsman $entity): Ombudsman
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($id = 0, $userId = 0, $status = 0, $destiny = 0, $ombudsmanSubject = 0, &$params): string
    {
        $where = '';
        if ($id) {
            $params[':id'] = $id;
            $where .= " AND ombudsmans.id = :id";
        }
        if ($status > -1) {
            $params[':status'] = $status;
            $where .= " AND ombudsmans.status = :status";
        }
        if ($destiny) {
            $params[':destiny'] = $destiny;
            $where .= " AND ombudsmans.destiny = :destiny";
        }
        if ($ombudsmanSubject) {
            $params[':ombudsmanSubject'] = $ombudsmanSubject;
            $where .= " AND ombudsmans.ombudsmanSubject = :ombudsmanSubject";
        }
        if ($userId) {
            $params[':userId'] = $userId;
            $where .= " AND ombudsmanCategory.responsible = :userId";
        }
        return $where;
    }

    public function list($id = 0, $userId = 0, $status = 0, $destiny = 0, $ombudsmanSubject = 0, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($id, $userId, $status, $destiny, $ombudsmanSubject, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ombudsmans.id, DATE_FORMAT(ombudsmans.created, '%d/%m/%Y') AS date, ombudsmanCategory.name AS destiny, 
                ombudsmanSubjects.name AS ombudsmanSubject, tb_pessoa.nome AS name, ombudsmans.anonymous, ombudsmans.status               
                FROM ombudsmans
                JOIN ombudsmanCategory ON ombudsmanCategory.id = ombudsmans.destiny
                JOIN ombudsmanSubjects ON ombudsmanSubjects.id = ombudsmans.ombudsmanSubject
                JOIN tb_pessoa ON tb_pessoa.id = ombudsmans.requester
                WHERE 1 = 1 {$where}
                ORDER BY ombudsmans.id DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($id = 0, $userId, $status = 0, $destiny = 0, $ombudsmanSubject = 0): array
    {
        $params = [];
        $where = $this->generateWhere($id, $userId, $status, $destiny, $ombudsmanSubject, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ombudsmans.id) AS total                  
                FROM ombudsmans
                JOIN ombudsmanCategory ON ombudsmanCategory.id = ombudsmans.destiny
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhereGraphics($dateStart = 0, $dateEnd = 0, $subject = 0, $category = 0, $status = 0, &$params): string
    {
        $where = '';
        $dateStart = \DateTime::createFromFormat('d/m/Y', $dateStart);
        $dateEnd = \DateTime::createFromFormat('d/m/Y', $dateEnd);
        if ($dateStart) {
            $params[':dateStart'] = $dateStart->format('Y-m-d');
            $where .= " AND ombudsmans.created >= :dateStart";
        }
        if ($dateEnd) {
            $params[':dateEnd'] = $dateEnd->format('Y-m-d');
            $where .= " AND ombudsmans.created <= :dateEnd";
        }
        if ($status > -1) {
            $params[':status'] = $status;
            $where .= " AND ombudsmans.status = :status";
        }
        if ($subject) {
            $params[':subject'] = $subject;
            $where .= " AND ombudsmans.ombudsmanSubject = :subject";
        }
        if ($category) {
            $params[':category'] = $category;
            $where .= " AND ombudsmans.destiny = :category";
        }
        return $where;
    }

    public function graphicOmbudsmanByStatus($dateStart = 0, $dateEnd = 0, $subject = 0, $category = 0, $status = 0): array
    {
        $params = [];
        $where = $this->generateWhereGraphics($dateStart, $dateEnd, $subject, $category, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ombudsmans.id) AS total, CASE ombudsmans.status WHEN 0 THEN 'Em Análise' WHEN 1 THEN 'Respondido' END AS status
                FROM ombudsmans 
                WHERE 1 = 1 {$where}                
                GROUP BY ombudsmans.status ORDER BY total DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function graphicOmbudsmanByDestiny($dateStart = 0, $dateEnd = 0, $subject = 0, $category = 0, $status = 0): array
    {
        $params = [];
        $where = $this->generateWhereGraphics($dateStart, $dateEnd, $subject, $category, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ombudsmans.id) AS total, ombudsmanCategory.name AS destiny
                FROM ombudsmans
                JOIN ombudsmanCategory ON ombudsmanCategory.id = ombudsmans.destiny
                WHERE 1 = 1 {$where}                
                GROUP BY ombudsmans.destiny ORDER BY total DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function graphicOmbudsmanBySubject($dateStart = 0, $dateEnd = 0, $subject = 0, $category = 0, $status = 0): array
    {
        $params = [];
        $where = $this->generateWhereGraphics($dateStart, $dateEnd, $subject, $category, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(ombudsmans.id) AS total, ombudsmanSubjects.name AS subject
                FROM ombudsmans
                JOIN ombudsmanSubjects ON ombudsmanSubjects.id = ombudsmans.ombudsmanSubject
                WHERE 1 = 1 {$where}                
                GROUP BY ombudsmans.ombudsmanSubject ORDER BY total DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}