<?php

namespace App\Models\Repository;

use App\Models\Entities\CandidateData;
use Doctrine\ORM\EntityRepository;

class CandidateDataRepository extends EntityRepository
{
    public function save(CandidateData $entity): CandidateData
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}
