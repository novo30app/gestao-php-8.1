<?php


namespace App\Models\Repository;


use App\Models\Entities\Php81Entity;
use Doctrine\ORM\EntityRepository;

class Php81EntityRepository extends EntityRepository
{
    public function save(Php81Entity $entity): Php81Entity
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}