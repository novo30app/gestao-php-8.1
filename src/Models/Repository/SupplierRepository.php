<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\Supplier;
use Doctrine\ORM\EntityRepository;

class SupplierRepository extends EntityRepository
{
	public function save(Supplier $entity):Supplier
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}
	
	private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}
	
	private function generateWhere(array $filter, &$params): string
	{
		$where = '';
		if ($filter['cpfCnpj']) {
			$params[':cpfCnpj'] = Utils::onlyNumbers($filter['cpfCnpj']);
            $params[':cpfCnpj']  = "%{$params[':cpfCnpj']}%";
			$where .= " AND supplier.cpfCnpj LIKE :cpfCnpj";
		}
		if ($filter['corporateName']) {
			$corporateName = $filter['corporateName'];
			$params[':corporateName'] = "%{$corporateName}%";
			$where .= " AND supplier.corporateName LIKE :corporateName";
		}
		if ($filter['nameResponsible']) {
			$nameResponsible = $filter['nameResponsible'];
			$params[':nameResponsible'] = "%{$nameResponsible}%";
			$where .= " AND supplier.nameResponsible LIKE :nameResponsible";
		}
		return $where;
	}
	
	public function list(array $filter, $limit = null, $offset = null): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$limitSql = $this->generateLimit($limit, $offset);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT supplier.id, supplier.nameResponsible, supplier.corporateName, supplier.cpfCnpj, supplier.phone, supplier.active
                FROM supplier
                WHERE supplier.status = 1 {$where}
				ORDER BY id DESC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
	
	public function listTotal(array $filter): array
	{
		$params = [];
		$where = $this->generateWhere($filter, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(supplier.id) AS total
                FROM supplier
                WHERE supplier.status = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}
	
	public function data(int $id): array
	{
		$params = [];
		$params[':id'] = $id;
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT *
                FROM supplier
                WHERE supplier.id =:id AND supplier.status = 1";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
}