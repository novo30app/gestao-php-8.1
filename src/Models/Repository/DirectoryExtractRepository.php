<?php

namespace App\Models\Repository;

use App\Models\Entities\DirectoryExtract;
use Doctrine\ORM\EntityRepository;

class DirectoryExtractRepository extends EntityRepository
{
    public function save(DirectoryExtract $entity): DirectoryExtract
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
	{
		$limitSql = '';
		if ($limit) {
			$limit = (int)$limit;
			$offset = (int)$offset;
			$limitSql = " LIMIT {$limit} OFFSET {$offset}";
		}
		return $limitSql;
	}

	private function generateWhere($account = null, $begin = null, $end = null, $validation = null, &$params)
	{
		$where = '';
		if ($begin) {
			$params[':begin'] = "$begin";
			$where .= " AND de.dateStart >= :begin";
		}
        if ($end) {
			$params[':end'] = "$end";
			$where .= " AND de.dateEnd <= :end";
		}
		if ($validation) {
			if($validation != 2) {
				if($validation == -1) $validation = false;
				$params[':validation'] = "$validation";
				$where .= " AND de.validation = :validation";
			} else {
				$where .= " AND de.validation IS NULL";
			}
		}
		$where .=  " AND de.account = $account";
		return $where;
	}

	public function listTotal($account = null, $begin = null, $end = null, $validation = null): array
	{
		$params = [];
		$where = $this->generateWhere($account, $begin, $end, $validation, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT COUNT(DISTINCT(de.id)) AS total
                FROM directoryExtract de
                WHERE 1 = 1 {$where}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAssociative();
	}

	public function list($account = null, $begin = null, $end = null, $validation = null, $limit = null, $offset = null): array
	{
		$params = [];
		$limitSql = $this->generateLimit($limit, $offset);
		$where = $this->generateWhere($account, $begin, $end, $validation, $params);
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT de.id, DATE_FORMAT(de.dateStart, '%d/%m/%Y') AS dateStart, DATE_FORMAT(dateEnd, '%d/%m/%Y') AS dateEnd, 
					de.attachmentExtract, ua.name, DATE_FORMAT(de.created, '%d/%m/%Y %H:%i:%s') AS created, de.validation
                FROM directoryExtract de
				LEFT JOIN usersAdmin ua ON ua.id = de.user 
                WHERE de.status = 1 {$where}
                GROUP BY de.id
				ORDER BY de.created DESC {$limitSql}";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}

	public function export(): array
	{
		$params = [];
		$pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
		$sql = "SELECT 	IFNULL(DATE_FORMAT(de.created, '%d/%m/%Y'), 'sem extrato') AS 'envio extrato', dir.nome, es.sigla, ci.cidade, 
					IFNULL(concat(da.agency, '-', da.agencyDigit), '-') AS agencia, IFNULL(concat(da.account, '-', da.accountDigit), '-') AS conta,
					(CASE da.nickname
						WHEN 1 THEN 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS'
						WHEN 2 THEN 'FP - POLÍTICA DA MULHER'
						WHEN 3 THEN 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS'
						WHEN 4 THEN 'FP - FUNDO PARTIDÁRIO'
						WHEN 5 THEN 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
						WHEN 6 THEN 'FEFC GERAL'
						WHEN 7 THEN 'FEFC MULHER'
						WHEN 8 THEN 'FEFC MULHER NEGRA'
						WHEN 9 THEN 'FEFC HOMEM NEGRO'
						WHEN 10 THEN 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
						WHEN 11 THEN 'FEFC INDÍGENA'
						ELSE 'Sem contas cadastradas'
					END) AS 'nome da conta',
					IFNULL(DATE_FORMAT(de.dateStart, '%d/%m/%Y'), '-') AS 'inicio extrato',
					IFNULL(DATE_FORMAT(de.dateEnd, '%d/%m/%Y'), '-') AS 'fim extrato',
					IFNULL(REPLACE(de.attachmentExtract, '/var/www/html/', 'https://'), '-') AS 'link extrato',
					IFNULL(us.name, '-') AS usuario
				
			FROM tb_diretorio dir
				LEFT JOIN directoryAccounts da ON da.directory = dir.id
				LEFT JOIN directoryExtract de ON de.account = da.id
				LEFT JOIN usersAdmin us ON us.id = de.user
				LEFT JOIN tb_estado es ON es.id = dir.tb_estado_id
				LEFT JOIN tb_cidade ci ON ci.id = dir.tb_cidade_id
				WHERE dir.nome LIKE '%municipal%' AND de.status = 1
			ORDER BY de.created DESC";
		$rows = $pdo->prepare($sql)->execute($params);
		return $rows->fetchAllAssociative();
	}
}