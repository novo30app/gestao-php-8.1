<?php

namespace App\Models\Repository;

use App\Models\Entities\UserAdmin;
use App\Models\Entities\LocalDocuments;
use Doctrine\ORM\EntityRepository;

class LocalDocumentsRepository extends EntityRepository
{
	public function save(LocalDocuments $entity): LocalDocuments
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $keyWord = null, $state = null, $city = null, &$params): string
    {
        $where = '';
        if ($keyWord) {
            $params[':keyWord'] = "%$keyWord%";
            $where .= " AND ld.title LIKE :keyWord";
        }
        if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_NATIONAL) {
            if ($state) {
                $params[':state'] = "$state";
                $where .= " AND ld.state = :state";
            }
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND ld.city = :city";
            }
        } elseif ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND ld.state = :state";
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND ld.city = :city";
            }
        } else {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND ld.state = :state";
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND ld.city = :city";
        }
        $params[':active'] = "1";
        $where .= " AND ld.active = :active";
        return $where;
    }

    public function list(UserAdmin $user, $keyWord = null, $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $keyWord, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ld.id, DATE_FORMAT(ld.created, '%d/%m/%Y %H:%i:%s') AS created, u.name as author, ld.title, ld.description, ld.file,
                    es.sigla AS state, IFNULL(ci.cidade, '---') AS city, ld.active
                FROM localDocuments ld
                LEFT JOIN usersAdmin u ON u.id = ld.author
                LEFT JOIN tb_estado es ON es.id = ld.state
                LEFT JOIN tb_cidade ci ON ci.id = ld.city
                WHERE 1 = 1 {$where} GROUP BY ld.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $keyWord = null, $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $keyWord, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(ld.id)) AS total 
                FROM localDocuments ld
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}