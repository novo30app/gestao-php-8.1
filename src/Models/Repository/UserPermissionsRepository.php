<?php

namespace App\Models\Repository;

use App\Models\Entities\UserPermissions;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class UserPermissionsRepository extends EntityRepository
{
    public function save(UserPermissions $entity): UserPermissions
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function deleteFeatures(UserAdmin $user): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM userPermissions
                WHERE user = :user";
        $pdo->prepare($sql)->execute([':user' => $user->getId()]);
    }

    public function getFeaturesByUser(UserAdmin $user): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT systemFeature FROM userPermissions WHERE user = :user";
        $permissions = $pdo->prepare($sql)->execute([':user' => $user->getId()])->fetchAllAssociative();
        $result = [];
        foreach ($permissions as $permission) $result[] = $permission['systemFeature'];
        return $result;
    }


}