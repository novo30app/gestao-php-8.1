<?php

namespace App\Models\Repository;

use App\Models\Entities\OriginatingOwner;
use Doctrine\ORM\EntityRepository;

class OriginatingOwnerRepository extends EntityRepository
{
    public function save(OriginatingOwner $entity): OriginatingOwner
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}