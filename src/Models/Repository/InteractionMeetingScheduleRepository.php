<?php

namespace App\Models\Repository;

use App\Models\Entities\Interaction;
use App\Models\Entities\InteractionMeetingSchedule;
use Doctrine\ORM\EntityRepository;

class InteractionMeetingScheduleRepository extends EntityRepository
{
    public function save(InteractionMeetingSchedule $entity): InteractionMeetingSchedule
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function dropByInteraction(Interaction $interaction)
    {
        $sql = "DELETE FROM interactionMeetingSchedules WHERE interaction = :interaction";
        $params = [':interaction' => $interaction->getId()];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $stm = $pdo->prepare($sql);
        $stm->execute($params);
    }
}