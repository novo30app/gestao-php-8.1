<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonSignature;
use Doctrine\ORM\EntityRepository;

class PersonSignatureRepository extends EntityRepository
{
    public function save(PersonSignature $entity): PersonSignature
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}