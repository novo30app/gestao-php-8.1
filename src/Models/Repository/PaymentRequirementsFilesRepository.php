<?php

namespace App\Models\Repository;

use App\Models\Entities\PaymentRequirementsFiles;
use Doctrine\ORM\EntityRepository;

class PaymentRequirementsFilesRepository extends EntityRepository
{
    public function save(PaymentRequirementsFiles $entity): PaymentRequirementsFiles
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}