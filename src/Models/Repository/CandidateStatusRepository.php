<?php

namespace App\Models\Repository;

use App\Models\Entities\CandidateStatus;
use Doctrine\ORM\EntityRepository;

class CandidateStatusRepository extends EntityRepository
{
    public function save(CandidateStatus $entity):CandidateStatus
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}