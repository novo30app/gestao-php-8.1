<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class UserAdminRepository extends EntityRepository
{
    public function save(UserAdmin $entity): UserAdmin
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere(UserAdmin $user, $id = 0, $name = null, $email = null, $type = null, $uf = null, $city = null, $sector = null, &$params): string
    {
        $where = '';
        if ($id) {
            $params[':id'] = $id;
            $where .= " AND admin.id = :id";
        }
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND admin.name LIKE :name";
        }
        if ($email) {
            $params[':email'] = "%$email%";
            $where .= " AND admin.email LIKE :email";
        }
        if ($type) {
            $params[':type'] = $type;
            $where .= " AND admin.type = :type";
        }
        if ($sector) {
            $params[':sector'] = $sector;
            $where .= " AND admin.sector = :sector";
        }
        if ($user->getLevel() == UserAdmin::LEVEL_CITY) { // municipal
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND admin.city = :city";
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND admin.state = :uf";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_STATE) { //estadual
            $params[':uf'] = $user->getState()->getId();
            $where .= " AND admin.state = :uf";
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND admin.city = :city";
            }
        } elseif ($user->getLevel() == UserAdmin::LEVEL_NATIONAL) { //nacional
            if ($city) {
                $params[':city'] = $city;
                $where .= " AND admin.city = :city";
            }
            if ($uf) {
                $params[':uf'] = $uf;
                $where .= " AND admin.state = :uf";
            }
        }

        $params[':typeLogged'] = $user->getType();
        $where .= " AND admin.type <= :typeLogged";
        return $where;
    }

    public function list(UserAdmin $user, $id = 0, $name = null, $email = null, $type = null, $uf = null, $city = null, $sector = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $id, $name, $email, $type, $uf, $city, $sector, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT admin.id, admin.name, admin.email, admin.active, admin.type, admin.libertasCourses, admin.level, admin.state, admin.city, city.cidade AS cityName, admin.adminAccess, 
                        admin.directoryAreaAccess, admin.exportAccess, admin.financialReport, admin.typeView, admin.events,
                        IF(admin.level > 1, '-', city.cidade) AS cityStr, 
                        IF(state.sigla IS NOT NULL, state.sigla, '-') AS ufStr, 
                        (CASE 
                            WHEN admin.type = 1 THEN \"Assistente\"
                            WHEN admin.type = 2 THEN \"Líder\"
                            WHEN admin.type = 3 THEN \"Administrador\"
                            WHEN admin.type = 4 THEN \"Contábil\"
                            WHEN admin.type = 5 THEN \"Diretiva\"
                            ELSE \"Desconhecido\" 
                        END) AS typeStr,
                        (CASE 
                            WHEN admin.level = 1 THEN \"Municipal\"
                            WHEN admin.level = 2 THEN \"Estadual\"
                            ELSE \"Nacional\" 
                        END) AS levelStr, sector.name AS sector
                FROM usersAdmin AS admin
                LEFT JOIN  tb_estado as state ON state.id = admin.state
                LEFT JOIN  tb_cidade as city ON city.id = admin.city
                LEFT JOIN  sectors as sector ON sector.id = admin.sector
                WHERE 1 = 1 {$where} GROUP BY admin.id ORDER BY admin.name ASC, admin.type DESC, state ASC, city ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $id = 0, $name = null, $email = null, $type = null, $uf = null, $city = null, $sector = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $id, $name, $email, $type, $uf, $city, $sector, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(admin.id) AS total  
                FROM usersAdmin AS admin
                LEFT JOIN  tb_estado as state ON state.id = admin.state
                LEFT JOIN  tb_cidade as city ON city.id = admin.city
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function login(string $email, string $password)
    {
        $user = $this->findOneBy(['email' => $email, 'active' => 1]);
        if (!$user || (!password_verify($password, $user->getPassword()))) {
            throw new \Exception('Usuário ou senha inválidos.');
        }
        return $user;
    }

    public function getDirectoryAcess(UserAdmin $user): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();

        $where = 1 == 1;
        $params = [];

        if ($user->getSector()->getId() != 4) {
            $where = 'dir.ativo = :ativo AND dir.exibir = :exibir';
            $params['ativo'] = 'S';
            $params['exibir'] = 1;
        }

        $sql = "SELECT dir.id, dir.nome, dir.cnpj FROM tb_diretorio dir WHERE {$where}";

        if ($user->getLevel() == UserAdmin::LEVEL_STATE) {
            $sql .= " AND dir.id NOT IN (1, 108, 102) AND (dir.tb_estado_id IN (SELECT state FROM usersAdmin WHERE id = {$user->getId()}) ||
                dir.estadual = 'N' AND dir.tb_cidade_id IN (SELECT access FROM accessAdmin a where a.type = 'city' AND a.userAdmin = {$user->getId()}))";
        } elseif ($user->getLevel() == UserAdmin::LEVEL_CITY) {
            $sql .= " AND dir.id NOT IN (1, 108, 102) AND dir.estadual = 'N' AND dir.tb_cidade_id IN (SELECT city FROM usersAdmin WHERE id = {$user->getId()})";
        } elseif ($user->getSector()->getId() == 4) {
            $sql .= " AND dir.id = :specialId";
            $params['specialId'] = 102;
        } else {
            $sql .= " OR dir.id IN (108, 102)";
        }

        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function permissoes(int $offset): array
    {
        $limit = 10;
        $offset = $limit * $offset;
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT id  FROM usersAdmin AS admin               
                WHERE (
                    SELECT COUNT(userPermissions.id) FROM userPermissions
                WHERE userPermissions.user = admin.id
                ) = 0 
                LIMIT {$limit} OFFSET {$offset}";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }


}