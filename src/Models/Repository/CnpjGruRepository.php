<?php

namespace App\Models\Repository;

use App\Models\Entities\CnpjGru;
use Doctrine\ORM\EntityRepository;

class CnpjGruRepository extends EntityRepository
{
    public function save(CnpjGru $entity):CnpjGru
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}