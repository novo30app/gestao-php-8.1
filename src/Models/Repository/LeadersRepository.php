<?php

namespace App\Models\Repository;

use App\Models\Entities\Doubt;
use App\Models\Entities\Leaders;
use Doctrine\ORM\EntityRepository;

class LeadersRepository extends EntityRepository
{
    public function save(Leaders $entity): Leaders
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($id = null, $name = null, $level = null, $status = null, $gender = null, 
        $uf = null, $city = null, $occupation = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND im.name LIKE :name";
        }
        if ($occupation) {
            $params[':occupation'] = $occupation;
            $where .= " AND im.office = :occupation";
        }
        if ($level) {
            $params[':level'] = $level;
            $where .= " AND i.type = :level";
        }
        if ($status) {
            $params[':status'] = $status;
            $where .= " AND im.status = :status";
        }
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND es.id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND ci.id = :city";
        }
        return $where;
    }

    public function list($id = null, $name = null, $level = null, $status = null, $gender = null, $uf = null, 
        $city = null, $occupation = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($id, $name, $level, $status, $gender, $uf, $city, $occupation, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT im.name, im.cpf, im.email,
                (SELECT REGEXP_REPLACE(t.telefone, '[^0-9]', '') FROM tb_telefone t WHERE t.tb_pessoa_id = p.id AND t.tb_tipo_telefone_id = 3 LIMIT 1) AS phone,
                IFNULL(DATE_FORMAT(im.mandateBegin, '%d/%m/%Y'), '-') AS mandateBegin,
                IFNULL(DATE_FORMAT(im.mandateEnd, '%d/%m/%Y'), '-') AS mandateEnd,
                es.estado AS state, IFNULL(ci.cidade, '-') AS city,
                IF(i.type != 1,
                    (CASE im.office
                        WHEN 1 THEN 'Presidente'
                        WHEN 2 THEN 'Vice Presidente'
                        WHEN 3 THEN 'Secretário Administrativo'
                        WHEN 4 THEN 'Secretário de Finanças'
                        WHEN 5 THEN 'Secretário de Relações Institucionais e Legal'
                    END),
                    (CASE im.office
                        WHEN 1 THEN 'Líder 1'
                        WHEN 2 THEN 'Líder 2'
                        WHEN 3 THEN 'Líder 3'
                        WHEN 4 THEN 'Líder 4'
                        WHEN 5 THEN 'Líder 5'
                    END)
                ) AS office,
                (CASE im.status
                    WHEN 1 THEN 'Indicação'
                    WHEN 2 THEN 'Análise'
                    WHEN 3 THEN 'Aprovado'
                    WHEN 5 THEN 'Inativo'
                    WHEN 6 THEN 'Análise DE'
                    WHEN 7 THEN 'Análise DN'
                END) AS status,
                (CASE p.genero
                    WHEN 1 THEN 'Homem'
                    WHEN 2 THEN 'Mulher'
                    ELSE 'Não informado'
                END) AS genderString,
                (CASE i.type
                    WHEN 1 THEN 'Núcleo'
                    WHEN 2 THEN 'Comissão Provisória'
                    WHEN 3 THEN 'Diretório'
                    ELSE '-'
                END) AS type
            FROM indicatedDirectoriesMembers im
            LEFT JOIN indicatedDirectories i ON i.id = im.indication
            LEFT JOIN tb_estado es ON es.id = i.state
            LEFT JOIN tb_cidade ci ON ci.id = i.city
            LEFT JOIN tb_pessoa p ON replace(replace(p.cpf, '.', ''), '-', '') = im.cpf
            WHERE im.active = 1 AND im.status = 3 AND i.active = 1 AND i.status = 3 {$where}
            ORDER BY FIELD(i.type, 1, 3, 2), im.name ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($id = null, $name = null, $level = null, $status = null, $gender = null, $uf = null, 
        $city = null, $occupation = null): array
    {
        $params = [];
        $where = $this->generateWhere($id, $name, $level, $status, $gender, $uf, $city, $occupation, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT im.id) AS total                  
                FROM indicatedDirectoriesMembers im
                LEFT JOIN indicatedDirectories i ON i.id = im.indication
                LEFT JOIN tb_estado es ON es.id = i.state
                LEFT JOIN tb_cidade ci ON ci.id = i.city
                LEFT JOIN tb_pessoa p ON replace(replace(p.cpf, '.', ''), '-', '') = im.cpf
                WHERE im.active = 1 AND im.status = 3 AND i.active = 1 AND i.status = 3 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    public function listExport($id = null, $name = null, $level = null, $status = null, $gender = null, $uf = null, $city = null, 
        $occupation = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($id, $name, $level, $status, $gender, $uf, $city, $occupation, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "???????";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}