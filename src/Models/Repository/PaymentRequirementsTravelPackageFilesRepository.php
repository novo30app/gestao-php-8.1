<?php

namespace App\Models\Repository;

use App\Models\Entities\PaymentRequerimentsTravelPackageFiles;
use Doctrine\ORM\EntityRepository;

class PaymentRequirementsTravelPackageFilesRepository extends EntityRepository
{
	public function save(PaymentRequerimentsTravelPackageFiles $entity): PaymentRequerimentsTravelPackageFiles
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}
}