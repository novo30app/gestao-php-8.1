<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsQuestions;
use Doctrine\ORM\EntityRepository;

class EventsQuestionsRepository extends EntityRepository
{
    public function save(EventsQuestions $entity):EventsQuestions
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}