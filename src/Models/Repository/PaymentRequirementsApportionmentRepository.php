<?php

namespace App\Models\Repository;

use App\Models\Entities\BudgetValues;
use App\Models\Entities\PaymentRequerimentsApportionment;
use App\Models\Entities\PaymentRequirements;
use Doctrine\ORM\EntityRepository;

class PaymentRequirementsApportionmentRepository extends EntityRepository
{
    public function save(PaymentRequerimentsApportionment $entity): PaymentRequerimentsApportionment
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function delete(PaymentRequirements $requirement): void
    {
        $params = [];
        $params[':id'] = $requirement->getId();
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM paymentRequerimentsApportionment
                WHERE paymentRequirements = :id";
        $pdo->prepare($sql)->execute($params);
    }

    public function totalByBudgetValues(array $filter): float
    {
        $whereDate = 'p.payDay';
        if ($filter['dateType'] == 3) $whereDate = 'accountingRecord.competition';
        $start = new \DateTime("01-{$filter['month']}-{$filter['year']}");
        $end = clone $start;
        $end->add(new \DateInterval('P1M'));
        $end->sub(new \DateInterval('P1D'));
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT SUM(CASE WHEN p.type = :repasse THEN p.paymentValue ELSE a.value END) AS total FROM paymentRequerimentsApportionment AS a
                JOIN paymentRequirements AS p ON p.id = a.paymentRequirements 
                LEFT JOIN accountingRecord ON accountingRecord.payment = p.id AND accountingRecord.formAccounting = 1
                WHERE (a.costCenter = :costCenter 
                       OR (a.costCenter IS NULL AND a.directory IS NULL AND p.costCenter = :costCenter) 
                    ) AND p.area = :area
                  AND {$whereDate} BETWEEN :start AND :end";
        $rows = $pdo->prepare($sql)->execute([
            ':costCenter' => $filter['costCenter'],
            ':start' => $start->format('Y-m-d'),
            ':end' => $end->format('Y-m-d'),
            ':area' => $filter['area'],
            ':repasse' => 16,
        ]);
        return $rows->fetchAssociative()['total'] ?? 0;
    }

    public function totalByCostCenterValues(array $filter): float
    {
        // setando o campo de busca
        $whereDate = 'p.payDay';
        if ($filter['dateType'] == 3) $whereDate = 'accountingRecord.competition';

        // setando as datas
        $start = new \DateTime("01-{$filter['month']}-{$filter['year']}");
        $end = clone $start;
        $end->add(new \DateInterval('P1M'));
        $end->sub(new \DateInterval('P1D'));

        // setando os parâmetros
        $params = [
            ':costCenter' => $filter['costCenter'],
            ':start' => $start->format('Y-m-d'),
            ':end' => $end->format('Y-m-d'),
            ':repasse' => 16,
        ];

        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT SUM(CASE WHEN p.type = :repasse THEN p.paymentValue ELSE a.value END) AS total 
                FROM paymentRequerimentsApportionment AS a
                JOIN paymentRequirements AS p ON p.id = a.paymentRequirements 
                LEFT JOIN accountingRecord ON accountingRecord.payment = p.id AND accountingRecord.formAccounting = 1
                WHERE (a.costCenter = :costCenter 
                       OR (a.costCenter IS NULL AND a.directory IS NULL AND p.costCenter = :costCenter) 
                    ) 
                  AND {$whereDate} BETWEEN :start AND :end
                  ";
        $rows = $pdo->prepare($sql)->execute($params);

        return $rows->fetchAssociative()['total'] ?? 0;
    }

    public function totalByDirectoryValues(array $filter): float
    {
        // setando o campo de busca
        $whereDate = 'p.payDay';
        if ($filter['dateType'] == 3) $whereDate = 'accountingRecord.competition';

        // setando as datas
        $start = new \DateTime("01-{$filter['month']}-{$filter['year']}");
        $end = clone $start;
        $end->add(new \DateInterval('P1M'));
        $end->sub(new \DateInterval('P1D'));

        // setando os parâmetros
        $params = [
            ':directory' => $filter['directory'],
            ':start' => $start->format('Y-m-d'),
            ':end' => $end->format('Y-m-d'),
            ':repasse' => 16,
        ];

        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT SUM(CASE WHEN p.type = :repasse THEN p.paymentValue ELSE a.value END) AS total 
                FROM paymentRequerimentsApportionment AS a
                LEFT JOIN accountingRecord ON accountingRecord.payment = p.id AND accountingRecord.formAccounting = 1
                JOIN paymentRequirements AS p ON p.id = a.paymentRequirements 
                JOIN costCenter AS cc ON cc.id = p.costCenter 
                WHERE (a.directory = :directory 
                       OR (a.directory IS NULL AND cc.directory = :directory) 
                    ) 
                  AND {$whereDate} BETWEEN :start AND :end
                  ";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative()['total'] ?? 0;
    }

    public function getPendents(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM paymentRequirements 
                where requestStatus in (3,7) and payDay >= '2024-01-01'
                and (SELECT count(id) FROM paymentRequerimentsApportionment
                    where paymentRequirements = paymentRequirements.id) = 0;";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }


}
