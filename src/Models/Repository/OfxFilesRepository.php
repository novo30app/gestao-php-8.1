<?php

namespace App\Models\Repository;

use App\Models\Entities\DirectoryAccounts;
use App\Models\Entities\OfxFiles;
use Doctrine\ORM\EntityRepository;

class OfxFilesRepository extends EntityRepository
{
    public function save(OfxFiles $entity): OfxFiles
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function verifyDate(DirectoryAccounts $directoryAccounts, \DateTime $start, \DateTime $end)
    {
        $params = [];
        $params[':directoryAccount'] = $directoryAccounts->getId();
        $params[':start'] = $start->format('Y-m-d');
        $params[':end'] = $end->format('Y-m-d');

        return $this->getEntityManager()->createQuery(
            "SELECT o
                FROM  App\Models\Entities\OfxFiles as o
                WHERE (o.start BETWEEN :start AND :end OR o.end BETWEEN :start AND :end 
                        OR :start BETWEEN o.start AND o.end OR :end BETWEEN o.start AND o.end) 
                AND o.directoryAccount = :directoryAccount
                ORDER BY o.id DESC
               ")
            ->setMaxResults(1)
            ->execute($params)[0];
    }
}