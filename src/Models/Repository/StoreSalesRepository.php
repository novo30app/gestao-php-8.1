<?php

namespace App\Models\Repository;

use App\Models\Entities\StoreSales;
use Doctrine\ORM\EntityRepository;

class StoreSalesRepository extends EntityRepository
{
    public function save(StoreSales $entity):StoreSales
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}