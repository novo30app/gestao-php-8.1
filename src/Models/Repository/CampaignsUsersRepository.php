<?php

namespace App\Models\Repository;

use App\Models\Entities\UserAdmin;
use App\Models\Entities\CampaignsUsers;
use Doctrine\ORM\EntityRepository;

class CampaignsUsersRepository extends EntityRepository
{
    public function save(Campaigns $entity):Campaigns
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, $campaign = null, $status = null, $exemption = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND p.nome LIKE :name";
        }
        if ($campaign) {
            $params[':campaign'] = "%$campaign%";
            $where .= " AND ca.name LIKE :campaign";
        }
        if ($status) {
            if ($status == '-1') $status = 0;
            $params[':status'] = "$status";
            $where .= " AND p.filiado = :status";
        }
        if ($exemption) {
            if($exemption == 2) $exemption = 0;
            $params[':exemption'] = "$exemption";
            $where .= " AND p.exemption = :exemption";
        }
        if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND p.titulo_eleitoral_uf_id = :state";
        } else if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_CITY) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND p.titulo_eleitoral_uf_id = :state";
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND p.titulo_eleitoral_municipio_id = :city";
        }
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $campaign = null, $status = null, $exemption = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $campaign, $status, $exemption, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, p.nome AS name, sf.status AS status, ca.name AS campaign, DATE_FORMAT(c.created, '%d/%m/%Y %H:%i:%s') AS created, IF(p.exemption = 1, 'Isento', 'Não Isento') AS exemption
                FROM campaignsUsers c
                LEFT JOIN tb_pessoa p ON p.id = c.user
                LEFT JOIN campaigns ca ON ca.id = c.campaign
                LEFT JOIN tb_pessoa_status_filiado sf ON sf.id = p.filiado
                WHERE 1 = 1 {$where} 
                GROUP BY c.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $campaign = null, $status = null, $exemption = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $campaign, $status, $exemption, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(c.id) AS total                  
                FROM campaignsUsers c
                LEFT JOIN tb_pessoa p ON p.id = c.user
                LEFT JOIN campaigns ca ON ca.id = c.campaign
                LEFT JOIN tb_pessoa_status_filiado sf ON sf.id = p.filiado
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}