<?php

namespace App\Models\Repository;

use App\Models\Entities\UserAdmin;
use App\Models\Entities\IndicationOfAffiliationSms;
use Doctrine\ORM\EntityRepository;

class IndicationOfAffiliationSmsRepository extends EntityRepository
{
    public function save(IndicationOfAffiliationSms $entity):IndicationOfAffiliationSms
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}