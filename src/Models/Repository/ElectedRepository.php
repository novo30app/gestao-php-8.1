<?php

namespace App\Models\Repository;

use App\Models\Entities\Elected;
use Doctrine\ORM\EntityRepository;

class ElectedRepository extends EntityRepository
{
    public function save(Elected $entity):Elected
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, $email = null, $cpf = null, $post = null, $year = null, $state = null, $city = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND e.name LIKE :name";
        }
        if ($email) {
            $params[':email'] = $email;
            $where .= " AND e.email = :email";
        }
        if ($cpf) {
            $params[':cpf'] = $cpf;
            $where .= " AND e.cpf = :cpf";
        }
        if ($post) {
            $params[':post'] = $post;
            $where .= " AND e.post = :post";
        }        
        if ($year) {
            $params[':year'] = $year;
            $where .= " AND e.year = :year";
        }
        if ($state) {
            $params[':state'] = $state;
            $where .= " AND e.state = :state";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND e.city = :city";
        }
        return $where;
    }

    public function list($name = null, $email = null, $cpf = null, $post = null, $year = null, $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $email, $cpf, $post, $year, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT e.id, e.name, e.email, e.cpf, e.year, es.estado AS state, IFNULL(ci.cidade, '-') AS city, 
                p.name AS post, IFNULL(e.votes, '-') AS votes, e.state AS stateId, e.city AS cityId, e.post AS postId
                FROM elected e
                JOIN posts p ON p.id = e.post
                LEFT JOIN tb_estado es ON es.id = e.state
                LEFT JOIN tb_cidade ci ON ci.id = e.city
                WHERE e.status = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null, $email = null, $cpf = null, $post = null, $year = null, $state = null, $city = null,): array
    {
        $params = [];
        $where = $this->generateWhere($name, $email, $cpf, $post, $year, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(e.id)) AS total
                FROM elected e
                LEFT JOIN tb_estado es ON es.id = e.state
                LEFT JOIN tb_cidade ci ON ci.id = e.city
                WHERE e.status = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}