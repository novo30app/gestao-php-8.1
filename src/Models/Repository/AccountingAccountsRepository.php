<?php

namespace App\Models\Repository;

use App\Models\Entities\AccountingRecord;
use App\Models\Entities\AccountingAccounts;
use Doctrine\ORM\EntityRepository;

class AccountingAccountsRepository extends EntityRepository
{
    public function save(AccountingAccounts $entity): AccountingAccounts
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function deleteAccounts(AccountingRecord $accountingRecordId): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM accountingAccounts
                WHERE accountingRecord = :accountingRecordId";
        $rows = $pdo->prepare($sql)->execute([':accountingRecordId' => $accountingRecordId->getId()]);
    }
}