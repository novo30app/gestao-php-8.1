<?php

namespace App\Models\Repository;

use App\Models\Entities\SystemFeatures;
use Doctrine\ORM\EntityRepository;

class SystemFeaturesRepository extends EntityRepository
{
    public function save(SystemFeatures $entity):SystemFeatures
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND s.name LIKE :name";
        }
        return $where;
    }

    public function list($name = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT s.id, DATE_FORMAT(s.created, '%d/%m/%Y %H:%m:%s') AS created, s.user, s.name, s.active
                FROM systemFeatures s
                WHERE s.active = 1 {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null): int
    {
        $params = [];
        $where = $this->generateWhere($name, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(s.id)) AS total
                FROM systemFeatures s
                WHERE s.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative()['total'];
    }
}