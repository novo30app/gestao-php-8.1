<?php

namespace App\Models\Repository;

use App\Models\Entities\AccessAdmin;
use Doctrine\ORM\EntityRepository;

class AccessAdminRepository extends EntityRepository
{
    public function save(AccessAdmin $entity):AccessAdmin
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getAccess($user): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, c.cidade 
                FROM accessAdmin a 
                LEFT JOIN tb_cidade c ON a.access = c.id 
                WHERE a.type = 'city' AND a.userAdmin = {$user->getId()}
                UNION ALL
                SELECT m.city, ci.cidade FROM mesoregionsCities m
                LEFT JOIN tb_cidade ci ON ci.id = m.city 
                WHERE m.mesoregion IN (SELECT access FROM accessAdmin WHERE type = 'meso' and userAdmin = {$user->getId()})";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function dropAccess(string $id, string $type): void
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM accessAdmin WHERE userAdmin = :id AND type = :type";
        $sth = $pdo->prepare($sql);
        $sth->execute([':id' => $id, ':type' => $type]);
    }

    public function listCities(string $id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT access as id, (SELECT cidade FROM tb_cidade c WHERE c.id = a.access) as nome_cidade FROM accessAdmin a WHERE a.userAdmin = :id AND a.type = :type";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':type' => 'city']);
        return $rows->fetchAllAssociative();
    }

    public function listMesos(string $id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT access as id, (SELECT name FROM mesoregions m WHERE m.id = a.access) as nome_meso FROM accessAdmin a WHERE a.userAdmin = :id AND a.type = :type";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':type' => 'meso']);
        return $rows->fetchAllAssociative();
    }
}