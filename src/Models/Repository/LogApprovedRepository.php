<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\LogApproved;
use App\Models\Entities\PaymentRequirements;
use Doctrine\ORM\EntityRepository;

class LogApprovedRepository extends EntityRepository
{
    public function save(LogApproved $entity): LogApproved
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function deleteByPaymentAndType(PaymentRequirements $requirement, int $type): void
    {
        $params[':paymentRequirements'] = $requirement->getId();
        $params[':type'] = $type;
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "DELETE FROM logApproved
                WHERE paymentRequirements = :paymentRequirements AND type = :type";
        $pdo->prepare($sql)->execute($params);
    }

    public function showActiveLogsBypayment(PaymentRequirements $requirement): array
    {
        $last = $this->getEntityManager()->createQuery(
            "SELECT l
				FROM  App\Models\Entities\LogApproved AS l
                WHERE l.paymentRequirements = :paymentRequirements ORDER BY l.type DESC")
            ->setMaxResults(1)
            ->execute([':paymentRequirements' => $requirement->getId()]);
        if (!$last) return [];
        $result['last'] = $last[0]->getType();
        $params = [
            ':paymentRequirements' => $requirement->getId(),
            ':last' => $result['last']
        ];
        $where = ')';
        if ($last[0]->getType() != 6) {
            $where = ' OR l.type = 6)';
        }
        $logs = $this->getEntityManager()->createQuery(
            "SELECT l
				FROM  App\Models\Entities\LogApproved AS l
                WHERE l.paymentRequirements = :paymentRequirements AND (l.type <= :last {$where}")
            ->execute($params);
        foreach ($logs as $log) {
            $result[$log->getType()] = [
                'name' => $log->getUser()->getName(),
                'created' => Utils::formatDateExt($log->getCreated())
            ];
        }

        if (isset($result[6])) $result['last'] = 6;

        return $result;
    }


}
