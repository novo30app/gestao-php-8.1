<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsDocuments;
use Doctrine\ORM\EntityRepository;

class EventsDocumentsRepository extends EntityRepository
{
    public function save(EventsDocuments $entity):EventsDocuments
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}