<?php

namespace App\Models\Repository;

use App\Models\Entities\IndicatedDirectories;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class IndicatedDirectoriesRepository extends EntityRepository
{
    public function save(IndicatedDirectories $entity): IndicatedDirectories
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $state = null, $city = null, $type = null, $status = null, &$params): string
    {   
        $where = '';
        if ($state) {
            $params[':state'] = "$state";
            $where .= " AND i.state = :state";
        }
        if ($city) {
            $params[':city'] = "$city";
            $where .= " AND i.city = :city";
        }
        if ($type) {
            $params[':type'] = "$type";
            $where .= " AND i.type = :type";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND i.status = :status";
        }
        return $where;
    }

    public function list(UserAdmin $user, $state = null, $city = null, $type = null, $status = null, $order = null, 
        $seq = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $state, $city, $type, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT i.id, es.estado AS state, IFNULL(ci.cidade, '---') AS city, u.name, 
                    IFNULL(DATE_FORMAT(i.refreshStatus, '%d/%m/%Y %H:%i'), '-') AS refreshStatus,
                    DATE_FORMAT(i.createdAt, '%d/%m/%Y %H:%i') AS createdAt,
                        (CASE i.status
                            WHEN 1 THEN 'Indicação'
                            WHEN 2 THEN 'Formalização'
                            WHEN 3 THEN 'Ativo'
                            WHEN 4 THEN 'Encerrado'
                            WHEN 5 THEN 'Análise DE'
                            WHEN 6 THEN 'Análise DN'
                        END) AS status,
                        (CASE i.type
                            WHEN 1 THEN 'Núcleo'
                            WHEN 2 THEN 'Comissão Provisória'
                            WHEN 3 THEN 'Diretório'
                            ELSE '-'
                        END) AS type 
                FROM indicatedDirectories i
                LEFT JOIN tb_estado es ON es.id = i.state
                LEFT JOIN tb_cidade ci ON ci.id = i.city
                LEFT JOIN usersAdmin u ON u.id = i.user
                WHERE i.active = 1 {$where}
                ORDER BY i.{$order} $seq {$limitSql}";       
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $state = null, $city = null, $type = null, $status = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $state, $city, $type, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(i.id)) AS total
                FROM indicatedDirectories i
                LEFT JOIN tb_estado es ON es.id = i.state
                LEFT JOIN tb_cidade ci ON ci.id = i.city
                LEFT JOIN usersAdmin u ON u.id = i.user
                WHERE i.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}