<?php

namespace App\Models\Repository;

use App\Models\Entities\StoreSalesDetails;
use Doctrine\ORM\EntityRepository;

class StoreSalesDetailsRepository extends EntityRepository
{
    public function save(StoreSalesDetails $entity):StoreSalesDetails
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}