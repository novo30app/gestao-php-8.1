<?php

namespace App\Models\Repository;

use App\Models\Entities\SelectiveProcess2022Module1RateLog;
use Doctrine\ORM\EntityRepository;

class SelectiveProcess2022Module1RateLogRepository extends EntityRepository
{
    public function save(SelectiveProcess2022Module1RateLog $entity): SelectiveProcess2022Module1RateLog
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}