<?php

namespace App\Models\Repository;

use App\Models\Entities\InteractionType;
use Doctrine\ORM\EntityRepository;

class InteractionTypeRepository extends EntityRepository
{
    public function save(InteractionType $entity): InteractionType
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}