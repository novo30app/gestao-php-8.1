<?php

namespace App\Models\Repository;

use App\Models\Entities\Appointment;
use Doctrine\ORM\EntityRepository;

class AppointmentRepository extends EntityRepository
{
    public function save(Appointment $entity) {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function list($id, $name = null, $status = null) {
        $where = '';
        $params = [];

        if($id) {
            $where .= 'AND appointments.ID = :id ';
            $params[':id'] = $id;
        }

        if($name) {
            $where .= 'AND tb_pessoa.NOME LIKE :name ';
            $params[':name'] = '%'.$name.'%';
        }

        if($status != '' && $status != null) {
            $where .= 'AND appointments.STATUS = :status ';
            $params[':status'] = $status;
        }

        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT appointments.ID, appointments.APPOINTMENT, appointments.STATUS, 
                appointments.LINK, appointments.OBS, tb_pessoa.NOME as USER, tb_pessoa.ID as USERID,
                CASE
                    WHEN appointments.STATUS = 0 THEN 'Não Encontrado' 
                    WHEN appointments.STATUS = 1 THEN 'Agendado'
                    WHEN appointments.STATUS = 2 THEN 'Realizado'
                    WHEN appointments.STATUS = 3 THEN 'Cancelado'
                    ELSE 'Não Encontrado'
                END as AUX
                FROM appointments
                JOIN tb_pessoa ON tb_pessoa.ID=appointments.AFFILIATED
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}