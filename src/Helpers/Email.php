<?php

namespace App\Helpers;

use App\Models\Commons\Entities\UserPoint;
use App\Models\Entities\IndicatedDirectoriesMembers;

class Email
{
    public static function finish(IndicatedDirectoriesMembers $member, $type = null)
    {
        $link = BASEURL . "indicacoes/responder/{$member->getId()}/{$member->getHash()}";
        $city = "{$member->getIndication()->getCity()->getCidade()}/{$member->getIndication()->getCity()->getState()->getSigla()}";
        $msg = "<p>Olá {$member->getName()},</p>";
        $msg .= "<p>Você foi indicado ao processo de seleção de Dirigente do Partido NOVO na cidade de {$city}. </p>";
        $msg .= "<p>Para dar continuidade ao seu processo, você passará por algumas etapas que serão esclarecidas logo abaixo:</p>";
        $msg .= "<p>Questionário 1. Neste teste queremos saber algumas das suas posições para assuntos que são relevantes para o Novo. 
                Separe de 5-10 minutos para responder este teste. Para preencher o questionário 1, 
                <a href='{$member->getQuestionnaire()->getLink()}' target='_blank'>clique aqui</a>. </p>";

        if($type != 1) {
            $msg .= "<p>2 - Contexto Político. Neste tópico, queremos conhecer um pouco do contexto político do seu município e como você enxerga 
                o que o Novo poderá fazer para melhorar a realidade local. Separe de 10-15 minutos para preencher esta parte. Para preencher o questionário 2, 
                <a href='{$link}' target='_blank'>clique aqui</a>.</p>";
        }

        $msg .= "<p>Agradecemos a sua participação e aguardamos as suas respostas.</p>";
        $msg .= "<p>Atenciosamente</p>";
        \App\Services\Email::send($member->getEmail(), $member->getName(), 'Indicação para dirigente', $msg);
    }

    public static function finishState(IndicatedDirectoriesMembers $member, $type = null)
    {
        $link = BASEURL . "indicacoes/responder/{$member->getId()}/{$member->getHash()}";
        $state = "{$member->getIndication()->getState()->getEstado()}";
        $msg = "<p>Olá {$member->getName()},</p>";
        $msg .= "<p>Você foi indicado ao processo de seleção de Dirigente do Partido NOVO no estado de {$state}. </p>";
        $msg .= "<p>Para dar continuidade ao seu processo, você passará por algumas etapas que serão esclarecidas logo abaixo:</p>";
        $msg .= "<p>Questionário 1. Neste teste queremos saber algumas das suas posições para assuntos que são relevantes para o Novo. 
                Separe de 5-10 minutos para responder este teste. Para preencher o questionário 1, 
                <a href='{$member->getQuestionnaire()->getLink()}' target='_blank'>clique aqui</a>. </p>";
        
        if($type != 1) {
            $msg .= "<p>2 - Contexto Político. Neste tópico, queremos conhecer um pouco do contexto político do seu município e como você enxerga 
                o que o Novo poderá fazer para melhorar a realidade local. Separe de 10-15 minutos para preencher esta parte. Para preencher o questionário 2, 
                <a href='{$link}' target='_blank'>clique aqui</a>.</p>";
        }

        $msg .= "<p>Agradecemos a sua participação e aguardamos as suas respostas.</p>";
        $msg .= "<p>Atenciosamente</p>";
        \App\Services\Email::send($member->getEmail(), $member->getName(), 'Indicação para dirigente', $msg);
    }


    public static function sendBenefit(UserPoint $userPoint)
    {
        $msg = "<p>Olá {$userPoint->getUser()->getName()}!</p>";
        $msg .= "<p>Parabéns por ter resgatado o Benefício {$userPoint->getBenefit()->getName()}!  </p>";
        $msg .= "<p>Estamos felizes em confirmar que <b>sua solicitação foi atendida com sucesso</b>.</p>";
        $msg .= "<p>Seu engajamento e dedicação ao NOVO contribuem diretamente para a construção de um Brasil mais justo e próspero para todos. </p>";
        $msg .= "<p>Cada passo que você dá com a gente fortalece nossa missão e nos aproxima de um país com mais liberdade e transparência.</p>";
        $msg .= "<p>Verifique outras missões que você pode cumprir e continue fazendo a diferença em nosso movimento.</p>";
        $msg .= "<p>Sua participação é fundamental para alcançarmos nossos objetivos e transformar o futuro do nosso país. Contamos com você!</p>";
        $msg .= "<p>A gente respeita o Brasil.</p>";
        $msg .= "<p>Equipe NOVO</p>";
        \App\Services\Email::send($userPoint->getUser()->getEmail(), $userPoint->getUser()->getName(), "Benefício {$userPoint->getBenefit()->getName()} Resgatado", $msg);
    }


}