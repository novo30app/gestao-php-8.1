<?php

namespace App\Helpers;
use App\Models\Entities\Directory;
use App\Models\Entities\User;
use App\Models\Entities\IndicatedDirectories;

class Messages
{
    public static function refiliationConfirmed($DataAction)
    {
        return "Olá {$DataAction->getName()}.
                Confirmamos sua refiliação junto ao Partido Novo na data de hoje.<br>
                Seu número de filiado permanece o mesmo: <b>{$DataAction->getFiliadoId()}</b>.<br>
                A partir deste momento você volta a ter acesso completo ao Espaço NOVO.<br><br>
                Nele você poderá:<br>
                <ul>
                    <li>Alterar seus dados cadastrais ou de pagamento da sua contribuição;</li>
                    <li>Consultar e fazer o pagamento de contribuições em aberto;</li>
                    <li>Consultar e fazer o download de recibos emitidos;</li>
                    <li>Acessar comunicados importantes que o NOVO disponibiliza para você;</li>
                    <li>Canal direto com nossa equipe e muito mais;</li>
                </ul>
                Acesse o Espaço NOVO pelo link: https://novo.org.br/espaconovo,<br>
                Atenção: este é um e-mail automático; por favor não responda.<br><br>
                Caso tenha alguma dúvida, escreva-nos por meio do Fale Conosco https://novo.org.br/acompanhe/fale-conosco/.<br><br>
                Equipe NOVO!";
    }

    public static function refiliationConfirmedFiliation($DataAction, $date)
    {
        return "Favor reativar este usuário no Filiaweb:<br><br>
                Nome: {$DataAction->getName()}<br>
                Email: {$DataAction->getEmail()}<br>
                Municipio eleitoral: {$DataAction->getTituloEleitoralMunicipioId()->getCidade()}<br>
                UF eleitoral: {$DataAction->getTituloEleitoralUfId()->getEstado()}<br>
                Titulo de Eleitor: {$DataAction->getTituloEleitoral()}<br>
                Zona: {$DataAction->getTituloEleitoralZona()}<br>
                Seção: {$DataAction->getTituloEleitoralSecao()}<br>
                Data da refiliação: {$date}<br><br>
                Atenção: este é um e-mail automático; por favor não responda.";
    }

    public static function refiliationConfirmedCEP($DataAction)
    {
        return "Atenção, <br>
                Há uma nova refiliação que se encontrava com um ou mais processos em aberto para análise da CEP, <br><br>
                Nome: " . $DataAction->getName() . "<br>
                CPF: " . $DataAction->getCpf() . "<br>
                Email: " . $DataAction->getEmail() . "<br> 
                Municipio eleitoral: {$DataAction->getTituloEleitoralMunicipioId()->getCidade()}<br>
                UF eleitoral: {$DataAction->getTituloEleitoralUfId()->getEstado()}<br>
                Data da refiliação: {$DataAction->getDataRefiliacao()->format('d/m/Y')}<br><br>
                Equipe NOVO!";
    }

    public static function titleIndication($DataAction)
    {
        return "Olá {$DataAction->getName()}.<br>
                Verificamos que seu Título de Eleitor encontra-se suspenso, cancelado ou com divergência nos dados cadastrados.<br>
                Por gentileza, entre em contato conosco para mais informações através do email: <a href='mailto:filiacao@novo.org.br'>filiacao@novo.org.br</a><br><br>
                Equipe NOVO!<br>
                Atenção: este é um e-mail automático; por favor não responda.";
    }

    public static function disaffiliation($DataAction)
    {
        return "Olá {$DataAction->getName()},<br>
                Sua desfiliação foi realizada com sucesso, O NOVO lamenta que você tenha se desfiliado.<br>
                Lembre-se de que as portas do NOVO sempre estarão abertas para seu retorno.<br>
                Atenção: este é um e-mail automático; por favor não responda.<br><br>
                Equipe NOVO!";
    }
    
    public static function conciliationIugu(array $resume, ?array $transactionsAbsent): string
    {
        $msg = "Bom dia,<br>
                Seguem informações da conciliação diária do IUGU.<br><br>
                - Total de transações analisadas: <b>{$resume['total']}</b><br>
                - Total de transações existentes: <b>{$resume['exists']}</b><br>
                - Total de transações ausentes ou divergentes: <b>{$resume['absent']}</b><br><br>";
        if($transactionsAbsent){
            $msg .= "Abaixo seguem transações não encontradas.<br><br>";
            foreach($transactionsAbsent as $a) {
                $msg .= "Invoice: <b>" . $a . "</b><br>";
            }
            $msg .= "A equipe de tecnologia já está copiada neste e-mail e irá atuar nas correções.<br><br>Equipe NOVO!";
        }
        return $msg;
    }

    public static function receivablesSuccess(string $sumBase, string $sumRede, array $transactions, string $start): string
    {
        $msg = "Bom dia,<br>
                Conciliação de Recebíveis do dia <b>$start</b> realizada com sucesso, seguem detalhes:<br><br>
                - Valor Depósito Base: <b>R$ $sumBase</b><br>
                - Valor Depósito Rede: <b>R$ $sumRede</b><br><br>";
        if($transactions) {
            $msg .= "Abaixo seguem problemas encontrados e já corrigidos pelo sistema:<br><br>
                    <ul>";
            foreach($transactions as $t) {
                $msg .= "<li>Valor: ". number_format($t['amount'], 2, ',', '.') ."</li>
                         <li>Taxa: ". number_format($t['mdrAmount'], 2, ',', '.') ."</li>
                         <li>Valor sem Taxas: ". number_format($t['netAmount'], 2, ',', '.') ."</li>
                         <li>Status: {$t['status']}<br>
                         <li>Data Venda: " . date('d/m/Y', strtotime($t['saleDate'])) . "</li>
                         <li>NSU: {$t['nsu']}</li>
                         <li>Invoice: {$t['orderNumber']}</li>";
            }
            $msg .= "</ul>";
        }
        $msg .= "Dúvidas entrar em contato com ti@novo.org.br!<br><br>
                 Equipe NOVO!";
        return $msg;
    }

    public static function accountingRegisterPaymentReproved($requerimentId): string
    {
        $msg = "Atenção,<br>
                A solicitação de pagamento abaixo foi recusada!<br>
                <a href='https://gestao.novo.org.br/financeiro/pagamentos/{$requerimentId}'>Clique aqui</a> para acessar o registro do pagamento e confira as informações.<br><br>
                Dúvidas, entre em contato com contabilidade@novo.org.br.<br>
                Equipe NOVO!";
        return $msg;
    }

    public static function newMessageIndication(IndicatedDirectories $indication): string
    {
        $msg = "Olá,<br>
                Há uma nova mensagem para você na indicação do diretório para {$indication->getCity()->getCidade()}/{$indication->getState()->getEstado()}.<br>
                <a href='https://gestao.novo.org.br/indicacoes/visualiza/{$indication->getId()}'>Clique aqui</a> para visualizar.<br><br>
                Equipe NOVO!";
        return $msg;
    }

    public static function affiliationConfirm(User $user, string $link): string
    {
        $msg = "Olá {$user->getFirstName()},<br>
                Sua solicitação de Filiação junto ao Partido NOVO foi solicitada com sucesso.<br>
                {$link}
                Caso não tenha solicitado nenhum tipo de filiação ao NOVO entre em contato com filiacao@novo.org.br<br><br>
                Atenção: este é um e-mail automático; por favor não responda.<br><br>
                Equipe NOVO!";
        return $msg;
    }

    public static function affiliationAlert(User $user): string
    {
        $msg = "Olá,<br>
                Uma nova indicação de filiação foi realizada para {$user->getName()},<br>
                Confira as informações <a href='https://gestao.novo.org.br/filiados/{$user->getId()}/'>Clicando aqui!</a><br><br>
                Atenção: este é um e-mail automático; por favor não responda.<br><br>
                Equipe NOVO!";
        return $msg;
    }

    public static function refiliationReprovalFiliation(User $DataAction): string
    {
        $msg = "Olá,<br>
                A refiliação abaixo foi recusada pelo Diretório:<br><br>
                Nome: {$DataAction->getName()}<br>
                Email: {$DataAction->getEmail()}<br>
                Municipio eleitoral: {$DataAction->getTituloEleitoralMunicipioId()->getCidade()}<br>
                UF eleitoral: {$DataAction->getTituloEleitoralUfId()->getEstado()}<br>
                Titulo de Eleitor: {$DataAction->getTituloEleitoral()}<br>
                Zona: {$DataAction->getTituloEleitoralZona()}<br>
                Seção: {$DataAction->getTituloEleitoralSecao()}<br>
                Data da solicitação: {$DataAction->getDataSolicitacaoRefiliacao()->format('d/m/Y')}<br><br>
                Favor providenciar os devidos estornos e comunicação ao filiado.<br>
                Atenção: este é um e-mail automático; por favor não responda.<br><br>
                Equipe NOVO!";
        return $msg;
    }

    public static function alertNewFileIndication($indication)
    {
        return "Atenção,<br>
                Nova Ata de Indicação de Novos Membros anexada na indicação para {$indication->getCity()->getCidade()}/{$indication->getState()->getSigla()}.<br>
                <a href='https://gestao.novo.org.br/indicacoes/visualiza/{$indication->getId()}'>Clique aqui</a> para conferir!<br><br>
                Equipe NOVO!";
    }

    public static function receiptReserved(array $receipts, Directory $directory): string
    {
        $msg = "Recibos reservados para este mês para o {$directory->getName()}.<br><br>";
        foreach($receipts as $r) {
            $msg .= "Recibo: <b>" . $r . "</b><br>";
        }
        $msg .= "<br>Equipe NOVO!";
        return $msg;
    }

    public static function alertRejectIndication(IndicatedDirectories $indication): string
    {
        return "Atenção,<br>
                Ata de indicação de novos membros rejeitada. Motivo: " . $indication->getRejectReason() . "<br>
                <a href='https://gestao.novo.org.br/indicacoes/visualiza/{$indication->getId()}'>Clique aqui</a> para conferir!<br><br>
                Equipe NOVO!";
    }
}