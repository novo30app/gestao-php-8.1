<?php

namespace App\Helpers;

use App\Models\Entities\SystemFeatures;

class Validator
{
    public static function validDate(string $date)
    {
        $dateArr = explode('/', $date);
        if (!checkdate($dateArr[1], $dateArr[0], $dateArr[2])) {
            throw new \Exception("Data inválida: {$date}");
        }
        return true;
    }

    public static function validateRangeNumber($number, $field, $min = PHP_FLOAT_MIN, $max = PHP_FLOAT_MAX)
    {
        if ($number < $min) {
            throw new \Exception("Valor minimo para o campo {$field} é {$min}");
        }
        if ($number > $max) {
            throw new \Exception("Valor máximo para o campo {$field} é {$max}");
        }
    }

    public static function validateSubscription($data, $type)
    {
        $data['card'] ??= 0;
        $min = 10;
        if ($type == 2) {
            if ($data['frequency'] == 1) {
                $min = MINMONTHLY;
            } else {
                $min = MINYEARLY;
            }
        }
        if ($data['value'] < $min) {
            $min = number_format($min, 2, ',', '');
            throw new \Exception("Valor minimo para a doação é R$ {$min}");
        }

        if ($data['value'] > MAXVALUE) {
            throw new \Exception('Valor máximo para a doação é R$ ' . number_format(MAXVALUE, 2, ',', '.'));
        }
        if ($data['day'] <= 0 || $data['day'] > 30) {
            throw new \Exception('Informe um dia entre 1 e 30');
        }
        if ($data['paymentMethod'] == 1 && !$data['card']) {
            throw new \Exception('Selecione um cartão de crédito');
        }
        if ($data['day'] <= 0 || $data['day'] > 30) {
            throw new \Exception('Informe um dia entre 1 e 30');
        }
    }

    public static function validateEmail($data)
    {
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Email inválido.');
        }
        if ($data['email2'] != $data['email']) {
            throw new \Exception('Os emails informados são diferentes.');
        }

    }

    public static function validatePassword($data)
    {
        if ($data['password2'] != $data['password']) {
            throw new \Exception('As senhas digitadas são diferentes');
        }
        if (strlen($data['password']) < 8) {
            throw new \Exception('A senha deve ter pelo menos 8 caracteres');
        }

    }

    public static function requireValidator($fields, $data)
    {
        foreach ($fields as $key => $value) {
            if (!array_key_exists($key, $data) || (is_string($data[$key]) && trim($data[$key]) === '') || $data[$key] === null) {
                throw new \Exception('O campo ' . $value . ' é obrigátorio');
            }
        }
    }

    public static function validateCPF(string $cpf)
    {
        if (empty($cpf)) {
            throw new \Exception('CPF inválido');
        }
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11) {
            throw new \Exception('CPF inválido');
        } else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            throw new \Exception('CPF inválido');
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf[$c] != $d) {
                    throw new \Exception('CPF inválido');
                }
            }
        }
    }

    public static function validateAccess($admin, $directoryArea, $financialReport, $events, $libertasCourse)
    {
        if ($admin == 0 && $directoryArea == 0 && $financialReport == 0 && $events == 0 && $libertasCourse == 0) {
            throw new \Exception("Selecione pelo menos um dos acessos!");
        }
    }


    public static function validatePermission(int $permission, bool $redirect = false, bool $str = false): bool|null|string
    {
        $permission = in_array($permission, (array)Session::get('permissions'));
        if (Session::get('userType') == 3 && Session::get('userLevel') == 3) $permission = true;
        if (!$permission && $redirect) {
            Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo.');
            header('Location: ' . BASEURL);
            exit;
        }
        if ($str) return $permission ? 'true' : 'false';
        return $permission;
    }

    public static function validateMenuPermission(string $menu): bool
    {
        if (Session::get('userType') == 3 && Session::get('userLevel') == 3) return true;
        $permissions = (array)Session::get('permissions');
        $permissionsMenu = [
            'administrativo' => [
                SystemFeatures::ADMINISTRATIVO_ORCADOXREALIZADO_VISUALIZACAO,
            ],
            'cadastros' => [
                SystemFeatures::CADASTROS_ACESSOSADMIN_VISUALIZACAO,
                SystemFeatures::CADASTROS_COMUNICACAO_VISUALIZACAO,
                SystemFeatures::CADASTROS_CONTRATOS_VISUALIZACAO,
                SystemFeatures::CADASTROS_DOCUMENTOSLOCAIS_VISUALIZACAO,
                SystemFeatures::CADASTROS_FORNECEDORES_VISUALIZACAO,
                SystemFeatures::CADASTROS_FUNCIONALIDADESDOSISTEMA_VISUALIZACAO,
                SystemFeatures::CADASTROS_GESTAODECOMUNICADOS_VISUALIZACAO,
                SystemFeatures::CADASTROS_MESORREGIOES_VISUALIZACAO,
                SystemFeatures::CADASTROS_ORCAMENTOS_AREA_SUB_AREA_VISUALIZACAO,
                SystemFeatures::CADASTROS_ORCAMENTOS_CENTRODECUSTO_VISUALIZACAO,
                SystemFeatures::CADASTROS_ORCAMENTOS_ORCAMENTOANUAL_VISUALIZACAO,
                SystemFeatures::CADASTROS_PLANODECONTAS_VISUALIZACAO,
                SystemFeatures::CADASTROS_PERMISSOES_VISUALIZACAO,
                SystemFeatures::CADASTROS_CATEGORIAS_VISUALIZACAO,
                SystemFeatures::CADASTROS_SETORES_VISUALIZACAO,
                SystemFeatures::CADASTROS_TIPOSDEUSUARIOS_VISUALIZACAO,
            ],
            'consultas' => [
                SystemFeatures::CONSULTAS_ADIMP_INADIMP_VISUALIZACAO,
                SystemFeatures::CONSULTAS_CANDIDATOS_VISUALIZACAO,
                SystemFeatures::CONSULTAS_DIRETORIOS_VISUALIZACAO,
                SystemFeatures::CONSULTAS_DESFILIACOES_VISUALIZACAO,
                SystemFeatures::CONSULTAS_DIRIGENTES_VISUALIZACAO,
                SystemFeatures::CONSULTAS_GESTAODECADASTROS_VISUALIZACAO,
                SystemFeatures::CONSULTAS_ULTIMASFILIACOES_VISUALIZACAO,
            ],
            'dam' => [
                SystemFeatures::DAM_DOSSIE_VISUALIZACAO,
                SystemFeatures::DAM_GABINETES_VISUALIZACAO,
                SystemFeatures::DAM_INTERACOES_VISUALIZACAO,
                SystemFeatures::DAM_PROJETOS_VISUALIZACAO,
                SystemFeatures::DAM_TAGS_VISUALIZACAO,
            ],
            'diretorios' => [
                SystemFeatures::DIRETORIOS_CONTASDEDIRETORIOS_VISUALIZACAO,
                SystemFeatures::DIRETORIOS_CONVENCOES_VISUALIZACAO,
                SystemFeatures::DIRETORIOS_EVENTOS_VISUALIZACAO,
                SystemFeatures::DIRETORIOS_INDICACAODEDIRETORIOS_VISUALIZACAO,
                SystemFeatures::DIRETORIOS_RELATORIOFINANCEIRO_VISUALIZACAO,
                SystemFeatures::DIRETORIOS_REUNIOES_VISUALIZACAO,
                SystemFeatures::DIRETORIOS_TRILHASDOCONHECIMENTO_VISUALIZACAO,
            ],
            'educacao' => [
                SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_AUTOINSCRICAOMOODLE_VISUALIZACAO,
                SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_CUPONS_VISUALIZACAO,
                SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_CURSOS_VISUALIZACAO,
                SystemFeatures::EDUCACAO_INSTITUTOLIBERTAS_MOODLEREDIRECTS_VISUALIZACAO,
            ],
            'filiados' => [
                SystemFeatures::FILIADOS_COMOCONHECEU_VISUALIZACAO,
                SystemFeatures::FILIADOS_INDICACOESDEFILIACAO_VISUALIZACAO,
                SystemFeatures::FILIADOS_PEDIDOSDEIMPUGNACAO_VISUALIZACAO,
                SystemFeatures::FILIADOS_PEDIDOSDEREFILIACAO_VISUALIZACAO,
                SystemFeatures::FILIADOS_QUEMINDICOU_VISUALIZACAO,
            ],
            'financeiro' => [
                SystemFeatures::FINANCEIRO_APROVACOESPENDENTES_VISUALIZACAO,
                SystemFeatures::FINANCEIRO_CONCILIACAO_EXTRATOS_VISUALIZACAO,
                SystemFeatures::FINANCEIRO_CONCILIACAO_IMPORTACAO_CRIACAO_EDICAO,
                SystemFeatures::FINANCEIRO_CONCILIACAO_IUGU_CRIACAO_EDICAO,
                SystemFeatures::FINANCEIRO_CONCILIACAO_PENDENTES_VISUALIZACAO,
                SystemFeatures::FINANCEIRO_CONCILIACAO_REDE_CRIACAO_EDICAO,
                SystemFeatures::FINANCEIRO_CONTASAPAGARETRANSFERENCIAS_VISUALIZACAO,
                SystemFeatures::FINANCEIRO_DOADORES_VISUALIZACAO,
                SystemFeatures::FINANCEIRO_RECIBOS_CRIACAO_EDICAO,
                SystemFeatures::FINANCEIRO_RELATORIODETRANSACOES_VISUALIZACAO,
            ],
            'gestao' => [
                SystemFeatures::GESTAO_ACESSOS_VISUALIZACAO,
                SystemFeatures::GESTAO_PAINELDEGESTAO_VISUALIZACAO,
                SystemFeatures::GESTAO_PLANEJAMENTOELEITORAL_VISUALIZACAO,
            ],
            'juventude' => [
                SystemFeatures::JUVENTUDE_CONSULTAS_VISUALIZACAO,
                SystemFeatures::JUVENTUDE_REUNIOES_VISUALIZACAO,
            ],
            'orientacoes' => [
                SystemFeatures::ORIENTACOES_JURIDICOLEGAL_VISUALIZACAO,
                SystemFeatures::ORIENTACOES_LGPDNEWS_VISUALIZACAO,
                SystemFeatures::ORIENTACOES_MINUTASCONTRATUAIS_VISUALIZACAO,
                SystemFeatures::ORIENTACOES_PROCESSOSFINANCEIROS_VISUALIZACAO,
            ],
            'ouvidoria' => [
                SystemFeatures::OUVIDORIA_RESPOSTASFREQUENTES_VISUALIZACAO,
                SystemFeatures::OUVIDORIA_SOLICITACOES_VISUALIZACAO,
            ],
            'sussecoDoFiliado' => [
                SystemFeatures::SUCESSODOFILIADO_CAMPANHASDEFILIACAO_VISUALIZACAO,
                SystemFeatures::SUCESSODOFILIADO_CAMPANHASDONOVO_VISUALIZACAO,
                SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_BENEFICIOS_VISUALIZACAO,
                SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOES_VISUALIZACAO,
                SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_MISSOESREALIZADAS_VISUALIZACAO,
                SystemFeatures::SUCESSODOFILIADO_NOVOMOBILIZA_SOLICITACOESDEBENEFICIOS_VISUALIZACAO,
            ],
        ];
        foreach ($permissions as $permission) {
            if (in_array($permission, $permissionsMenu[$menu])) return true;
        }
        return false;
    }


}
