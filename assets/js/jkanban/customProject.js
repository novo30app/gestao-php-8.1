var kanban = new jKanban({
    element: '#cards',
    gutter: '15px',
    click: function (el) {
        alert(el.innerHTML);
    },
    boards: [{
        'id': 'status1',
        'title': 'Tramitando',
        'class': 'bg-warning',
    },
        {
            'id': 'status2',
            'title': 'Arquivado',
            'class': 'bg-danger',
        },
        {
            'id': 'status3',
            'title': 'Aprovado',
            'class': 'bg-success',
        }
    ]
});