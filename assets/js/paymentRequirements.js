function validateForm(step) {
    let errors = '';
    const formData = formDataToJson('msform');
    let dateMin = moment(new Date()).add(-24, 'month');
    if (formData.requestStatus == 1 && formData.directory != "Loja") {
        dateMin = moment(new Date()).add(1, 'day');
    }
    const dateMax = moment(new Date()).add(8, 'month');
    switch (step) {
        case 1:
            switch (parseInt(formData.type)) {
                case 1:
                    if (moment(formData.accommodationStart, 'DD/MM/YYYY', true).isValid() &&
                        moment(formData.accommodationStart, 'DD/MM/YYYY').format('YYYY/MM/DD') < dateMin.format('YYYY/MM/DD')) {
                        errors += `<b>Início da Hospedagem</b> data minima ${dateMin.format('DD/MM/YYYY')} <br>`;
                    }
                    if (moment(formData.accommodationStart, 'DD/MM/YYYY', true).isValid() &&
                        moment(formData.accommodationStart, 'DD/MM/YYYY').format('YYYY/MM/DD') > dateMax.format('YYYY/MM/DD')) {
                        errors += `<b>Início da Hospedagem</b> data máxima ${dateMin.format('DD/MM/YYYY')} <br>`;
                    }
                    if (moment(formData.accommodationEnd, 'DD/MM/YYYY', true).isValid() &&
                        moment(formData.accommodationEnd, 'DD/MM/YYYY').format('YYYY/MM/DD') > dateMax.format('YYYY/MM/DD')) {
                        errors += `<b>Fim da Hospedagem</b> data máxima ${dateMax.format('DD/MM/YYYY')} <br>`;
                    }
                    if (moment(formData.accommodationEnd, 'DD/MM/YYYY', true).isValid() &&
                        moment(formData.accommodationEnd, 'DD/MM/YYYY').format('YYYY/MM/DD') < dateMin.format('YYYY/MM/DD')) {
                        errors += `<b>Fim da Hospedagem</b> data minima ${dateMin.format('DD/MM/YYYY')} <br>`;
                    }
                    if (formData.accommodationStart > formData.accommodationEnd) {
                        errors += `<b>Datas</b> inválidas<br>`;
                    }
                    break;
            }
            break;
        case 2:
            //if (formData.origin == 2) {
            //    if ($('#attachmentContract').val() == '') errors += '<b>Anexe um Contrato</b><br>';
            //}
            if(formData.type == 22) break;
            if (formData.paymentMethod.trim() != '0') {
                switch (parseInt(formData.paymentMethod)) {
                    case 1:
                        if (!moment(formData.paymentSlipDueDate, 'DD/MM/YYYY', true).isValid()) errors += '<b>Vencimento</b> valor inválido<br>';
                        if (moment(formData.paymentSlipDueDate, 'DD/MM/YYYY', true).isValid() &&
                            moment(formData.paymentSlipDueDate, 'DD/MM/YYYY').format('YYYY/MM/DD') < dateMin.format('YYYY/MM/DD')) {
                            errors += `<b>Vencimento</b> data minima ${dateMin.format('DD/MM/YYYY')} <br>`;
                        }
                        if (moment(formData.paymentSlipDueDate, 'DD/MM/YYYY', true).isValid() &&
                            moment(formData.paymentSlipDueDate, 'DD/MM/YYYY').format('YYYY/MM/DD') > dateMax.format('YYYY/MM/DD')) {
                            errors += `<b>Vencimento</b> data máxima ${dateMax.format('DD/MM/YYYY')} <br>`;
                        }
                        break;
                    case 2:
                        if (!moment(formData.depositMaturityDueDate, 'DD/MM/YYYY', true).isValid()) errors += '<b>Vencimento</b> valor inválido<br>';
                        if (moment(formData.depositMaturityDueDate, 'DD/MM/YYYY', true).isValid() &&
                            moment(formData.depositMaturityDueDate, 'DD/MM/YYYY').format('YYYY/MM/DD') < dateMin.format('YYYY/MM/DD')) {
                            errors += `<b>Vencimento</b> data minima ${dateMin.format('DD/MM/YYYY')} <br>`;
                        }
                        if (moment(formData.depositMaturityDueDate, 'DD/MM/YYYY', true).isValid() &&
                            moment(formData.depositMaturityDueDate, 'DD/MM/YYYY').format('YYYY/MM/DD') > dateMax.format('YYYY/MM/DD')) {
                            errors += `<b>Vencimento</b> data máxima ${dateMax.format('DD/MM/YYYY')} <br>`;
                        }
                        if (!validateCpfCnpj(formData.paymentDoc)) errors += '<b>CPF/CNPJ</b> inválido<br>';
                        if (formData.paymentName.replace(/\d+/g, '').trim() == '') errors += '<b>Nome/Razão Social</b> não existe razão somente números<br>';
                        break;
                }
            }
            break;
    }
    if (errors != '') {
        showNotify('danger', errors, 1500);
        return false;
    }
    return true;

}

function getOriginatingTransactions(requirement) {
    $("#table tbody").empty();
    const type = document.getElementById('type').value;

    let dateTransfer = document.getElementById('dateTransfer').value;
    if(type == 22) dateTransfer = document.getElementById('dateTransferCandidate').value;
    
    if(!dateTransfer) showNotify('danger', 'Digite a data da transferência', 1500);
    requirement = requirement ?? 0;
    if(type == 16) {
        value1 = "transferDirectoryOrigin";
        value2 = "valueTransfer"
    } else {
        value1 = "directoryCandidate";
        value2 = "valueTransferCandidate"
    }
    var directory = document.getElementById(value1).value;
    directory = directory.split('/')[0];
    var value = document.getElementById(value2).value;
    if(directory == "" || value == "") {
        showNotify('danger', 'Verifique se você indicou o diretório e o valor da transferência!');
        return false;
    } 
    var transaction = document.getElementById('transactions').value;
    var beginValue = document.getElementById('beginValue').value;
    var endValue = document.getElementById('endValue').value;
    fetch(`${baseUrl}api/financeiro/transacoes-originarios/?requirement=${requirement}&directory=${directory}&value=${value}
            &transaction=${transaction}&beginValue=${beginValue}&endValue=${endValue}&dateTransfer=${dateTransfer}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let transactionsList = json.message;
            let options = transactionsList.map(generateList);
            var total = json.total;
            total = total.toLocaleString('pt-br', {minimumFractionDigits: 2});
            document.getElementById("totalSpan").innerHTML = total;
            $("#totalSpanInput").val(json.total);
            $("#table tbody").append(options);
            $('addTransaction').modal('hide');
            $("#transactions").val('');
        });
    });
}

function generateList(transaction) {
    var split = transaction.validSplit;
    var color = disabled = "";
    if (split == 1) {
        color = "background-color: powderblue";
        disabled = "onclick='return false'";
    }
    return `<tr class="middle" style="${color}">
                <td class="text-center">
                    <input type="checkbox" class="checkbox" id="originatingTransactions" name="originatingTransactions[]" 
                        value="${transaction.transaction}" onchange="handleChange(event, ${transaction.value});" checked ${disabled}></td>
                <td class="text-center">${transaction.transaction}</td>
                <td class="text-center">${transaction.payDay}</td>
                <td class="text-center">${transaction.name}</td>
                <td class="text-center">${transaction.receipt}</td>
                <td class="text-center">${transaction.valueFormated}</td>
                <td class="text-center">${transaction.split}</td>
                <td class="text-center">${transaction.directory}</td>
            </tr>`;
}

function changeModal(modal, action) {
    $('#' + modal).modal(action);
}

const splitForm = document.getElementById('splitForm');
const messageFormSplit = document.getElementById('messageFormSplit');
if (splitForm) {
    splitForm.addEventListener('submit', e => {
        e.preventDefault();
        showLoading();
        let method = 'POST';
        let url = `${baseUrl}api/financeiro/split/`;
        let formData = formDataToJson('splitForm');
        document.getElementById("slipSave").disabled = true;
        fetch(url, {
            method: method,
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }).then(response => {
            response.json().then(json => {
                closeLoading();
                messageFormSplit.textContent = json.message;
                messageFormSplit.style.display = 'block';
                messageFormSplit.classList.add("alert-danger");
                messageFormSplit.classList.remove("alert-success");
                document.getElementById("slipSave").disabled = false;
                if (response.status === 201) {
                    var value = document.getElementById('totalSpanInput').value;
                    let discount = document.getElementById('splitTransactionDiscount').value;
                    discount = discount.replace(".", "");
                    discount = discount.replace(",", ".");
                    var newValue = parseFloat(value) - parseFloat(discount);
                    document.getElementById("slipSave").disabled = true;
                    messageFormSplit.classList.add("alert-success");
                    messageFormSplit.classList.remove("alert-danger");
                    document.getElementById('splitForm').reset();
                    setTimeout(function () {
                        $("#totalSpanInput").val(newValue.toFixed(2));
                        document.getElementById("totalSpan").innerHTML = newValue.toLocaleString('pt-br', {minimumFractionDigits: 2});
                        $('#splitModal').modal('hide');
                        document.getElementById("slipSave").disabled = false;
                    }, 1500);
                }
            });
        });
    });
}