const formAdd = document.getElementById('register');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('register');
    if (!ValidateForm('register')) {
        closeLoading();
        return;
    }
    let formData = formDataToJson('register');
    fetch(baseUrl + "questionarios/enviar", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                const message = `
                            Questionário envido com sucesso!<br> 
                            <p class="m-t-10">
                                <a data-clipboard-text="${baseUrl}questionarios/responder/2/${json.id}/${json.hash}" class="clipboard pointer">
                                    <b>Clique aqui</b>
                                </a> para copiar o link do questionário.
                            </p>`
                showNotify('success', message, 0, true);
                formAdd.reset();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

var clipboard = new ClipboardJS('.clipboard');
clipboard.on('success', function (e) {
    alert('Link copiado com sucesso!')
    $('.alert').hide();
    e.clearSelection();
});