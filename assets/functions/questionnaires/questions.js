$(document).ready(function () {
    setStorage('Questions', {
        'url': 'questionarios/questoes/json',
        'table': 'table',
        'pagination': 'pagination',
        'columns': 5,
        'loader': 'Table',
        'variables': {
            'index': 0,
            'active': 1,
            'responsible': '',
            'limit': 25
        }
    })

});

function generateLines(question) {
    let actions = '';
    actions += `<a href="${baseUrl}questionarios/questoes/cadastro/${question.id}"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    actions = `<td class="text-center">${actions} </td>`;

    return `<tr class="middle">
                    <td class="text-center">${question.id}</td>
                    <td class="text-center">${question.text}</td>
                    <td class="text-center">${question.userName}</td>
                    <td class="text-center">${question.active ? 'Ativo' : 'Inativo'}</td>
                    ${actions}
                </tr>`;
}

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    $("#tableQuestions tbody").empty();
    setStorage('Questions', {
        'url': 'questionarios/questoes/json',
        'table': 'table',
        'pagination': 'pagination',
        'columns': 5,
        'loader': 'Table',
        'variables': {
            'index': 0,
            'active': formData.active,
            'responsible': formData.responsible,
            'limit': $('#limit').val()
        }
    })
});

function resetTable() {
    filter.reset();
    $("#tableQuestions tbody").empty();
    setStorage('Questions', {
        'url': 'questionarios/questoes/json',
        'table': 'table',
        'pagination': 'pagination',
        'columns': 5,
        'loader': 'Table',
        'variables': {
            'index': 0,
            'active': 1,
            'responsible': '',
            'limit': $('#limit').val()
        }
    })
}