$(document).ready(function () {
    setStorage('Questionnaires', {
        'url': 'questionarios/json',
        'table': 'table',
        'pagination': 'pagination',
        'columns': 5,
        'loader': 'Table',
        'variables': {
            'index': 0,
            'active': 1,
            'responsible': '',
            'limit': 25
        }
    })

});

function generateLines(question) {
    let actions = '';
    actions += `<a class="btn btn-sm btn-success" href="${baseUrl}questionarios/enviar/${question.id}">Enviar</a>`;
    actions += `<a class="btn btn-sm btn-info" href="${baseUrl}questionarios/respostas/${question.id}">Respostas</a>`;
    actions += `<a class="btn btn-sm btn-warning" href="${baseUrl}questionarios/cadastro/${question.id}">Editar</a>`;
    actions += `<a class="btn btn-sm btn-danger" target="_blank" href="${baseUrl}questionarios/responder/1/${question.id}/${question.hash}/">Responder</a>`;
    return `<tr class="middle">
                    <td class="text-center">${question.id}</td>
                    <td class="text-center">${question.title}</td>
                    <td class="text-center">${question.userName}</td>
                    <td class="text-center">${question.active ? 'Ativo' : 'Inativo'}</td>
                    <td class="text-center"> <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">${actions}</div></td>
                </tr>`;
}

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    $("#tableQuestionnaires tbody").empty();
    setStorage('Questionnaires', {
        'url': 'questionarios/json',
        'table': 'table',
        'pagination': 'pagination',
        'columns': 5,
        'loader': 'Table',
        'variables': {
            'index': 0,
            'active': formData.active,
            'responsible': formData.responsible,
            'limit': $('#limit').val()
        }
    })
});

function resetTable() {
    filter.reset();
    $("#tableQuestionnaires tbody").empty();
    setStorage('Questionnaires', {
        'url': 'questionarios/json',
        'table': 'table',
        'pagination': 'pagination',
        'columns': 5,
        'loader': 'Table',
        'variables': {
            'index': 0,
            'active': 1,
            'responsible': '',
            'limit': $('#limit').val()
        }
    })
}