const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    $('#form').addClass('was-validated');
    showLoading('form');
    if (!ValidateForm('form')) {
        if ($('#name').val().trim() === '') {
            $('.tox').removeClass('is-valid').addClass('is-invalid');
        } else {
            $('.tox').removeClass('is-invalid').addClass('is-valid');
        }
        closeLoading();
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + "questionarios/questoes/cadastro", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(form)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(function () {
                    $('#form').removeClass('was-validated');
                    $('#form input, #form select, #form .tox').removeClass('is-valid is-invalid');
                    if ($('#questionId').val() > 0) {
                        location.reload();
                    } else {
                        formAdd.reset();
                    }
                }, 500)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});