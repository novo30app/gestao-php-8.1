const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    $('#form').addClass('was-validated');
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + "questionarios/cadastro", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(function () {
                    window.location.href = baseUrl + 'questionarios';
                }, 1500)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});