function delElement(url, element, id, method, button) {
    $('#confirmDelete').prop('onclick', null).off('click').click(function () {
        changeActive(url, id, button, method);
    });
    $('#elementDelete').empty().text(element);
    $('#modalDelete').modal('show');
}