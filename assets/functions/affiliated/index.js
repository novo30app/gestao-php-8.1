$(document).ready(function () {
    verifySession('Affiliated');
    if (stateUser > 0) getCities(stateUser, 'cityAffiliatedFilter');
    if (!term) $("#modal").modal('show');
    if (stateUser > 0) getCities(stateUser);
    $('#agroupAffiliatedFilter').change(function () {
        if ($('#agroupAffiliatedFilter').val() == 'data_refiliacao') {
            $("#statusAffiliatedFilter").val(8);
        } else if ($('#agroupAffiliatedFilter').val() == 'data_desfiliacao') {
            $("#statusAffiliatedFilter").val(9);
        } else if ($('#agroupAffiliatedFilter').val() == 'nome' || $('#agroupAffiliatedFilter').val() == 'data_filiacao') {
            $("#statusAffiliatedFilter").val(7);
        }
    });
})

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Affiliated');
})

function setStorageIndex() {
    setStorage('Affiliated', {
        'url': 'api/filiados/json',
        'columns': 8,
        'variables': {
            'index': 0,
            'name': '',
            'cpf': '',
            'email': '',
            'state': '',
            'city': '',
            'nome': '',
            'affiliateId': '',
            'end': '',
            'begin': '',
            'irpf': '',
            'exemption': '',
            'state2': '',
            'city2': '',
            'gender': '',
            'status': $("#statusAffiliatedFilter").val(),
            'ordination': '',
            'mesoregion': '',
            'ageBegin': '',
            'ageEnd': '',
            'situation': '',
            'typeDate': '',
            'agroup': '',
            'limit': 25
        }
    })
}

function generateLines(affiliated) {
    return `<tr class="middle">
                    <td class="text-center">${affiliated.affiliatedId}</td>
                    <td><a href="${baseUrl}filiados/${affiliated.id}" target="_blank">${affiliated.name}</a></td>
                    <td class="text-center">${affiliated.status}</td>
                    <td class="text-center">${affiliated.exemption}</td>
                    <td class="text-center">${affiliated.filiationSolicitation}</td>
                    <td class="text-center">${affiliated.filiation}</td>
                    <td class="text-center">${affiliated.disaffiliation}</td>
                    <td class="text-center">${affiliated.state}</td>
                    <td class="text-center">${affiliated.city}</td>
                    <td class="text-center">${affiliated.age}</td>
                </tr>`;
}

const agreementForm = document.getElementById('confidentialityAgreementForm');
agreementForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('confidentialityAgreementForm');
    fetch(baseUrl + 'api/sistema/termo/affiliated', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                document.getElementById("save").disabled = true;
                $('#modal').modal('hide');
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function csv() {
    let formData = formDataToJson('filter');
    let state = formData.state;
    let city = formData.city;
    let state2 = formData.state2;
    let city2 = formData.city2;
    let status = formData.status;
    let gender = formData.gender;
    let mesoregion = formData.mesoregion;
    let typeDate = formData.typeDate;
    let begin = formData.begin;
    let end = formData.end;
    let name = formData.name;
    let cpf = formData.cpf;
    let email = formData.email;
    let affiliatedId = formData.affiliatedId;
    let irpf = formData.irpf;
    let exemption = formData.exemption;
    let ageBegin = formData.ageBegin;
    let ageEnd = formData.ageEnd;
    let situation = formData.situation;
    window.open(
        `${baseUrl}api/filiados/exportCsv/?state=${state}&irpf=${irpf}&exemption=${exemption}&city=${city}&state2=${state2}&city2=${city2}&status=${status}&gender=${gender}&mesoregion=${mesoregion}&name=${name}&cpf=${cpf}&email=${email}&typeDate=${typeDate}&begin=${begin}&end=${end}&affiliatedId=${affiliatedId}&ageBegin=${ageBegin}&ageEnd=${ageEnd}&situation=${situation}`, 
        '_blank');
}