var index = 0;
var partial = 0;
var total = 1;
var name = '';
var state = 0;
var city = 0;
const form = document.getElementById('filter');

form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    name = formData.name;
    state = formData.state;
    city = formData.city;
    $("#table tbody").empty();
    generateTable();
})

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    name = '';
    state = 0;
    city = 0;
    $("#table tbody").empty();
    generateTable();
}

function generateMembershipList(reaffiliations) {
    var action = '';
    if (canUpdate) {
        action = `<a href="#" onclick="actionAproval('${reaffiliations.id}', '${reaffiliations.exemption}')"><i class="fa fa-thumbs-up" aria-hidden="true" style="font-size: small; color: green"> Aprovar</i> </a><br><br>
			  <a href="#" onclick="actionAproval('${reaffiliations.id}', '14')"><i class="fa fa-thumbs-down" aria-hidden="true" style="font-size: small; color: red"> Reprovar</i></a>`;
    }
    return `<tr class="middle">
                <td class="text-center">${reaffiliations.id}</td>
                <td class="text-center"><a href="${baseUrl}filiados/${reaffiliations.id}" target="_blank">
					${reaffiliations.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${reaffiliations.uf}</td>
                <td class="text-center">${reaffiliations.city}</td>
				<td class="text-center">${reaffiliations.date}</td>
                <td class="text-center">${reaffiliations.status}</td>
				<td class="text-center">${action}</td>
            </tr>`;
}

function setaDadosModal(valor) {
    document.getElementById('id').value = valor;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(baseUrl + `api/filiados/refiliacoes/?index=${index}&name=${name}&state=${state}&city=${city}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateMembershipList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

function actionAproval(id, exemption) {
    fetch(baseUrl + `api/filiados/aprova-refiliacoes/${id}/${exemption}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Trail');
                changeModal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}