const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Schedules');
})


function generateLines(requests) {

    let status = "";
    switch(requests.status) {
        case 'scheduled':
            status = '<button type="button" class="btn btn-sm btn-primary">Agendado</button>';
            break;
        case 'sent':
            status = '<button type="button" class="btn btn-sm btn-success">Enviado</button>';
            break;
        case 'error':
            status = '<button type="button" class="btn btn-sm btn-danger">Erro</button>';
            break;
    }

    const urlPaymentTransaction = 'https://novo.org.br/faturas/' + btoa(requests.transaction);

    let message = requests.message;
    if (requests.message === 'Cobrança') {
        message = '<span class="badge bg-danger">Cobrança</span>';
    } else if (requests.message === 'Lembrete') {
        message = '<span class="badge bg-success">Lembrete</span>';
    }

    return `<tr class="middle">
                    <td class="text-center">${status}</td>
                    <td class="text-right">
                        <a href="${baseUrl}/filiados/${requests.contact_id}" target="_blank">
                            ${requests.name} <i class="fa fa-external-link-alt"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="${urlPaymentTransaction}" target="_blank">
                            ${requests.transaction} <i class="fa fa-external-link"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="https://wa.me/${requests.contact_number}" target="_blank">
                            ${requests.contact_number} <i class="fa fa-external-link"></i>
                        </a>
                    </td>
                    <td class="text-center">${requests.collector}</td>
                    <td class="text-center">${message}</td>
                    <td class="text-center">${requests.scheduled_time}</td>
                </tr>`;
}

function setStorageIndex() {
    setStorage('Schedules', {
        'url': 'filiados/agendamentos',
        'columns': 5,
        'variables': {
            'index': 0,
            'name': '',
            'phone': '',
            'collector': '',
            'type': '',
            'status': '',
            'date': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    setStorageIndex();
});