const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    setStorage('MissionsCompletes', {
        'url': 'missoes-realizadas/json',
        'columns': 6,
        'variables': {
            'index': 0,
            'type': 'mission',
            'name': formData.name,
            'mission': formData.mission,
            'status': formData.status,
            'cpf': formData.cpf,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

function resetTable() {
    filter.reset();
    setStorageIndex();
}

function csv() {
    let formData = formDataToJson('filter');
   const variables = {
       'type': 'mission',
       'name': formData.name,
       'mission': formData.mission,
       'status': formData.status,
       'cpf': formData.cpf,
       'index': 0,
       'limit': 100000000
   }

   const url = getUrl(variables);

    window.open(`${baseUrl}missoes-realizadas/exportar/${url}`, '_blank');
}

function generateLines(item) {

    let status = '';

    if (item.data.status === 2) {
        status = `<div class="badge badge-danger" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-content="Top popover">Cancelada</div>`;
    } else if (item.data.status === 3) {
        status = `<div class="badge badge-success" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-content="Top popover">Realizada</div>`;
    } else {
        let text = 'Aguardando Aprovação';

        status = `<div class="badge badge-info" data-bs-container="body" data-bs-toggle="popover" data-bs-placement="top" data-bs-content="Top popover">${text}</div>`;
    }

    let attachment = '';



    if (item.data.doc && item.data.doc.length > 0) {
        let doc = item.data.doc.split('/');
        const index = doc.length;
        attachment = `<a class="fa fa-file" title="Visualizar Anexo" target="_blank" href="${baseUrl}uploads/${doc[index - 1]}"></a>`;
    }

    return `<tr class="middle" id="line${item.data.id}">                
                    <td class="text-center">${item.user}</td>                    
                    <td class="text-center">${item.mission}</td>                    
                    <td class="text-center">${convertDateText(item.data.created_at.date)}</td>        
                     <td class="text-center">${item.cpf}</td>                                  
                     <td class="text-center">${status}</td>                                  
                     <td class="text-center">${attachment}</td>                                  
                </tr>`;
}

$(document).ready(function () {
    verifySession('MissionsCompletes');
});

function setStorageIndex() {
    setStorage('MissionsCompletes', {
        'url': 'missoes-realizadas/json',
        'columns': 6,
        'variables': {
            'index': 0,
            'type': 'mission',
            'name': '',
            'mission': '',
            'cpf': '',
            'status': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    });
};
