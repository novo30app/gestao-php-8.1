const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    $("#table tbody").empty();
    setStorage('NovoMobilize', {
        'url': 'novo-mobiliza/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': formData.name,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    let method = 'POST';
    let formData = formDataToJson('form');
    fetch(baseUrl + "novo-mobiliza/cadastro", {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                formAdd.reset();
                resetTable();
                $('#modal').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function resetTable() {
    $("#table tbody").empty();
    filter.reset();
    setStorage('NovoMobilize', {
        'url': 'novo-mobiliza',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    })
}

function generateLines(item) {
    let actions = `<i class="fa fa-pencil text-info px-1" title="Editar" onclick="openModal(${item.id})"></i>`;
    actions = `<td class="text-center">${actions} </td>`;


    return `<tr class="middle" id="line${item.id}">
                    <td class="text-center">${item.id}</td>                    
                    <td class="text-center">${item.name}</td>                    
                    <td class="text-center">${buttonActive(item.active, 'novo-mobiliza', item.id)}</td>
                    ${actions}
                </tr>`;
}

function openModal(id) {
    document.getElementById('form').reset();
    document.getElementById('id').value = id;
    $('#modal').modal('show');
    if (id > 0) {
        showLoading();
        fetch(baseUrl + `novo-mobiliza/json/${id}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0];
                closeLoading();
                setValuesModal(json);
            });
        });
    }
}

$(document).ready(function () {
    setStorage('NovoMobilize', {
        'url': 'novo-mobiliza/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': '',
            'total': 1,
            'limit': 25
        }
    });
});
