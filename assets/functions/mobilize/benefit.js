const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    setStorage('Benefit', {
        'url': 'beneficios/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': formData.name,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    let method = 'POST';
    fetch(baseUrl + "beneficios/cadastro", {
        method: method,
        credentials: 'same-origin',
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                formAdd.reset();
                resetTable();
                $('#modal').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function resetTable() {
    filter.reset();
    setStorageIndex();
}

function generateLines(item) {
    let actions = '';
    if (canUpdate) actions = `<i class="fa fa-pencil text-info px-1" title="Editar" onclick="openModal(${item.id})"></i>`;
    actions = `<td class="text-center">${actions} </td>`;

    let activeStr = item.active ? 'Ativo' : 'Inativo';


    return `<tr class="middle" id="line${item.id}">
                    <td class="text-center">${item.id}</td>                    
                    <td class="text-center">${item.name}</td>                    
                    <td class="text-center">${item.points}</td>                    
                    <td class="text-center">${getTypeDelivery(item.type_delivery)}</td>                    
                    <td class="text-center">${canDelete ? buttonActive(item.active, 'beneficios', item.id, 'Benefit', 'put') : activeStr}</td>
                    ${actions}
                </tr>`;
}

function getTypeDelivery(type) {
    switch (parseInt(type)) {
        case 1:
            return 'Presencial';
        case 2:
            return 'Digital';
        case 3:
            return 'Externa';
        default:
            return 'Desconhecido';
    }
}

function showInputs(plan) {
    const check = document.getElementById(`chk-${plan}`).checked;

    if (check) {
        $('.inputs-' + plan).removeClass('d-none');
    } else {
        $('.inputs-' + plan).addClass('d-none').val('');
        $(`.inputs-${plan} input`).val('');
    }

    $(`.inputs-${plan} input`).prop('required', check);
}

function openModal(id) {
    document.getElementById('form').reset();
    document.getElementById('id').value = id;
    $('.inputs').addClass('d-none');
    $(`.inputs input`).prop('required', false);
    $('#img').prop('required', id === 0);
    $('#modal').modal('show');
    if (id > 0) {
        showLoading();
        fetch(baseUrl + `beneficios/${id}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message;
                setValuesModal(json);

                Object.keys(json.benefitPlan).forEach(plan => {
                    document.getElementById(`chk-${plan}`).checked = true;

                    document.getElementById(`planPoints-${plan}`).value = json.benefitPlan[plan].points;
                    document.getElementById(`minDays-${plan}`).value = json.benefitPlan[plan].minDays;
                    document.getElementById(`available-${plan}`).value = json.benefitPlan[plan].availables;

                    showInputs(plan);
                })


                closeLoading();
            });
        });
    }
}

$(document).ready(function () {
    verifySession('Benefit');
});

function setStorageIndex() {
    setStorage('Benefit', {
        'url': 'beneficios/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    });
};
