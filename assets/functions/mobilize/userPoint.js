const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    setStorage('UserPoint', {
        'url': 'beneficios-solicitacoes/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'type': 'benefits',
            'name': formData.name,
            'status': formData.status,
            'benefit': formData.benefit,
            'cpf': formData.cpf,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

function changeStatus() {
    showLoading();
    let formData = formDataToJson('form');

    if (!formData.benefitRequest || formData.benefitRequest.length === 0) {
        showNotify('danger', 'Selecione ao menos uma solicitação!', 1500);
        closeLoading();
        return;
    }

    const status = document.getElementById('status').value
    if (status === '') {
        showNotify('danger', 'Selecione um status!', 1500);
        closeLoading();
        return;
    }

    formData.status = status;

    fetch(baseUrl + "beneficios-solicitacoes/alterar-status", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
                resetTable();
                hideToast();
                document.getElementById('status').value = '';
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function submitForm() {
    showLoading();
    let formData = formDataToJson('form-change-status');

    fetch(baseUrl + "beneficios-solicitacoes/alterar-status", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
                resetTable();
                $('#modal').modal('hide')
                document.getElementById('status').value = '';
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function resetTable() {
    filter.reset();
    setStorageIndex();
}

function csv() {
    let formData = formDataToJson('filter');
    const variables = {
        'type': 'benefits',
        'name': formData.name,
        'status': formData.status,
        'benefit': formData.benefit,
        'cpf': formData.cpf,
        'index': 0,
        'limit': 100000000
    }

    const url = getUrl(variables);

    window.open(`${baseUrl}beneficios-solicitacoes/exportar/${url}`, '_blank');
}

function generateLines(item) {
    let status = '';

    if (item.data.status === 2) {
        status = `<div class="badge badge-danger">Cancelado</div>`;
    } else if (item.data.status === 5) {
        status = `<div class="badge badge-success">Entregue</div>`;
    } else {
        let text = 'Aguardando Aprovação';
        if (item.data.status === 3) {
            text = 'Em Preparo';
        } else if (item.data.status === 4) {
            text = 'A Caminho';
        }

        status = `<div class="badge badge-info">${text}</div>`;
    }

    let address = '';

    if (item.data.address[0]) {
        let complement = '';

        if (item.data.address[0].complement.length > 0) {
            complement = item.data.address[0].complement + ', ';
        }

        address = `
            <span class="d-block f-w-400">${item.data.address[0].zipCode}</span>
            <span class="d-block f-w-400">${item.data.address[0].address}, ${complement} ${item.data.address[0].number}, ${item.data.address[0].neighborhood}</span>
            <span class="d-block f-w-400">${item.data.address[0].city}/${item.data.address[0].uf}</span>
        `;
    }

    let changeStatus = '';
    if (canUpdate) changeStatus = `<input class="form-check-input" name="benefitRequest[]"  onchange="verifyToast()" type="checkbox" value="${item.data.id}" id="benefit-${item.data.id}">`;

    return `<tr class="middle" id="line${item.data.id}">
                    <td class="text-center">${changeStatus}</td>                    
                    <td class="text-center">
                        <a href="${baseUrl}filiados/${item.uId}" target="_blank">                    
                            ${item.user}
                        </a>
                    </td>                    
                    <td class="text-center">${item.benefit}</td>                    
                    <td class="text-center">${convertDateText(item.data.created_at.date)}</td>        
                     <td class="text-center">${item.cpf}</td>               
                    <td class="text-center">${address}</td>                    
                    <td class="text-center" onclick="showModal(${item.data.id})">${status}</td> 
                </tr>`;
}

function showModal(id) {
    document.getElementById('form-change-status').reset();
    document.getElementById('id').value = id;

    $('#modal').modal('show');
}

function checkAll(check) {
    var form = document.getElementById('form');
    var checkboxes = form.querySelectorAll('input[type="checkbox"]');

    checkboxes.forEach(checkbox => {
        checkbox.checked = check;
    })

    verifyToast();
}

function verifyToast() {
    var form = document.getElementById('form');
    var checkboxes = form.querySelectorAll('input[type="checkbox"]');
    var checked = Array.from(checkboxes).filter(checkbox => checkbox.checked && checkbox.id !== 'check');

    if (checked.length > 0) {
        showToast();
    } else {
        hideToast();
        document.getElementById('check').checked = false;
    }
}

function showToast() {
    const toast = document.getElementById('myToast');
    toast.style.opacity = '1';
    toast.style.visibility = 'visible'
}


function hideToast() {
    const toast = document.getElementById('myToast');
    toast.style.opacity = '0';
    toast.style.visibility = 'hidden'
}

$(document).ready(function () {
    verifySession('UserPoint');
});

function setStorageIndex() {
    setStorage('UserPoint', {
        'url': 'beneficios-solicitacoes/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'type': 'benefits',
            'name': '',
            'status': 1,
            'benefit': '',
            'cpf': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    });
};
