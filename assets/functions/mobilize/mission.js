// var classe = 'description'
// tinymce.init({
//     selector: "textarea",
//     menubar: false,
//     statusbar: false,
//     toolbar: 'bold | alignleft aligncenter alignright alignjustify | bullist | link image media | forecolor backcolor',
//     plugins: ["lists link textcolor media"],
//     // plugin_base_urls: `${baseUrl}assets/js/plugins/tinymce/plugins`,
//     external_plugins: {
//         'lists': `${baseUrl}assets/js/plugins/tinymce/plugins/lists/plugin.min.js`,
//         'link': `${baseUrl}assets/js/plugins/tinymce/plugins/link/plugin.min.js`,
//         'textcolor': `${baseUrl}assets/js/plugins/tinymce/plugins/textcolor/plugin.min.js`,
//         'media': `${baseUrl}assets/js/plugins/tinymce/plugins/media/plugin.min.js`,
//         'image': `${baseUrl}assets/js/plugins/tinymce/plugins/image/plugin.min.js`,
//     },
//     browser_spellcheck: true,
//     imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
//     setup: function (editor) {
//         editor.on('change', function () {
//             editor.save();
//         });
//     }
// });

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    setStorage('Mission', {
        'url': 'missoes/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'title': formData.title,
            'active': formData.active,
            'type': formData.type,
            'gender': formData.gender,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    let method = 'POST';
    let formData = formDataToJson('form');
    fetch(baseUrl + "missoes/cadastro", {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                formAdd.reset();
                resetTable();
                $('#modal').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function resetTable() {
    filter.reset();
    setStorageIndex();
}

function generateLines(item) {
    let actions = ms = '';
    if (canUpdate) {
        actions = `<i class="fa fa-pencil text-info px-1" title="Editar" onclick="openModal(${item.id})"></i>`;
        ms = `<input type="checkbox" ${item.week == 1 ? 'checked' : ''} id="check-mission-${item.id}" value="${item.id}" onchange="setMissionWeek(${item.id})">`;
    }
    if (canDelete) actions += `<i class="fa fa-trash text-danger px-1" title="Excluir" onclick="delElement('missoes', 'Contrato', ${item.id}, 'delete', 'Mission')"></i>`;

    actions = `<td class="text-center">${actions} </td>`;
    let activeStr = item.active ? 'Ativo' : 'Inativo';

    return `<tr class="middle" id="line${item.id}">
                    <td class="text-center">${item.id}</td>                    
                    <td class="text-center">${item.title}</td>                    
                    <td class="text-center">${item.points}</td>                    
                    <td class="text-center">${!!item.type ? 'Sistema' : 'Manual'}</td>                    
                    <td class="text-center">${canUpdate ? buttonActive(item.active, 'missoes', item.id, 'Mission', 'put') : activeStr}</td>
                    ${actions}
                    <td class="text-center">${ms}</td>
                </tr>`;
}

function setMissionWeek(id) {
    const formData = {
        id,
        status: document.getElementById('check-mission-' + id).checked
    }

    fetch(baseUrl + `missoes/set-missao-da-semana/`, {
        method: "POST",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        response.json().then(json => {
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function openModal(id) {
    document.getElementById('form').reset();
    document.getElementById('id').value = id;
    $('#modal').modal('show');
    if (id > 0) {
        showLoading();
        fetch(baseUrl + `missoes/${id}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message;
                closeLoading();
                setValuesModal(json);
            });
        });
    }
}

$(document).ready(function () {
    verifySession('Mission');
});

function setStorageIndex() {
    setStorage('Mission', {
        'url': 'missoes/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'title': '',
            'active': '',
            'type': '',
            'gender': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    });
}
