$(document).ready(function (){
    inputFile();
    verifySession('Store');
})

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Store');
})

function generateLines(requests) {
    return `<tr class="middle">
                    <td class="text-center">${requests.date}</td>
                    <td class="text-center">${requests.theme}</td>
                    <td class="text-center">
                        <a href="${baseUrl}uploads/comunicados/${requests.file}" target="_blank">
                            <i class="fa fa-download" aria-hidden="true"></i> Baixar
                        </a>
                    </td>
                </tr>`;
}

function setStorageIndex() {
    setStorage('Store', {
        'url': 'api/area-diretorio/loja',
        'columns': 3,
        'variables': {
            'index': 0,
            'title': '',
            'limit': 25
        }
    })
}

var formAdd = document.getElementById('formFile');
$("#formFile").submit(function (e) {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('formFile')) {
        closeLoading();
        return;
    }
    fetch(`${baseUrl}api/area-diretorio/loja/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                $('#formFile').trigger("reset");
                $('#addCommunicatedModal').modal('hide');
                resetTable('Store');
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});