const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Documents');
});

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}

function generateLines(documents) {
    var action = '';
    if (documents.active == 1 && canDelete) {
        action += `<a href="#" onclick="delElement('api/cadastros/altera-documentos-locais', 'Documents', '${documents.id}', 'delete', 'Documents')">
                        <i class="fa fa-times" aria-hidden="true" style="font-size: x-large;"></i> Remover
                    </a><br>`;
    }
    if (documents.active == 1 && canUpdate) {
        action += `<a href="#" onclick="updateClass('${documents.id}')"><i class="fa fa-pencil" aria-hidden="true" style="font-size: x-large;"></i> Editar</a>`;
    }
    var file = `<a href="${formatAttachment(documents.file)}" target="_blank"><i class="fa fa-download" aria-hidden="true" style="font-size: x-large;"></i> Download</a>`;
    return `<tr class="middle">
				<td class="text-center">${documents.created}</td>
				<td class="text-center">${documents.title}</td>
				<td class="text-center">${documents.author}</td>
                <td class="text-center">${documents.state}</td>
				<td class="text-center">${documents.city}</td>
                <td class="text-center">${documents.description}</td>
                <td class="text-center">${file}</td>
                <td class="text-center">${action}</td>
    		</tr>`;
}

function updateClass(document) {
    fetch(`${baseUrl}api/cadastros/pega-documento-local/${document}/`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            let d = json.message;
            $("#document").val(d['id']);
            $("#title").val(d['title']);
            $("#state").val(d['state']);
            getCities(d['state'], 'city');
            $("#description").val(d['description']);
            $('#viewFile').prop('href', formatAttachment(d['file']));
            setTimeout(function () {
                $("#city").val(d['city']);
            }, 1000);
        });
    });
    $('#file').prop('required', false);
    $('#viewFileDiv').show();
    $('#exampleModal').modal('show');
}

function changeModal(action) {
    $('#viewFileDiv').hide();
    document.getElementById('form').reset();
    $('#exampleModal').modal(action);
}

const form = document.getElementById('form');
$("#form").submit(function (e) {
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}api/cadastros/documentos-locais/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(form)
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Documents');
                changeModal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function setStorageIndex() {
    const columns = $('#tableDocuments th');
    setStorage('Documents', {
        'url': 'api/area-diretorio/documentos-locais',
        'columns': columns.length,
        'variables': {
            'index': 0,
            'keyWord': '',
            'state': '',
            'city': '',
            'limit': 10
        }
    })
}

$(document).ready(function () {
    inputFile();
    verifySession('Documents');
});