tinymce.init({
    selector: "textarea",
    menubar: false,
    statusbar: false,
    toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image media | forecolor backcolor table',
    plugins: ["lists link image media textcolor table"],
    browser_spellcheck: true,
    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});


const divMessageForm = document.getElementById('divMessageForm');
$("#formFile").submit(function (e) {
    event.preventDefault();
    showLoading();
    if (!ValidateForm('formFile')) {
        closeLoading();
        return;
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + 'api/area-diretorio/salva-relatorio-financeiro/',
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            showNotify('success', json.message, 0);
            setTimeout(function () {
                window.location.href = baseUrl + "area-diretorio/relatorio-financeiro/";
            }, 2000);
        })
        .fail(function (json) {
            closeLoading();
            showNotify('danger', json.responseJSON.message, 1500);
        });
});