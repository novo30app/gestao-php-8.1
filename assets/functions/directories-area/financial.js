function openCity(evt, cityName) {
    var i, tabcontent, tablinks;

    // Remove todas as tabs
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Remove todas as classes active das tabs
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Mostra o conteúdo atual e adiciona classe active no botão clicado
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

function openModal(theme, media) {
    document.getElementById('themeModal').textContent = theme;
    document.getElementById("idIframe").src = media;
    $('#meetingModal').modal('show');
}

const agreementForm = document.getElementById('confidentialityAgreementForm');
agreementForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('confidentialityAgreementForm');
    fetch(baseUrl + 'api/sistema/termo/financialReport', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                document.getElementById("save").disabled = true;
                $('#modal').modal('hide');
                showNotify('success', json.message, 1500)
            } else {
                showNotify('danger', json.message, 1500)
            }
        });
    });
});

function addReport(report) {
    document.getElementById('addFinancialHref').href = "";
    document.getElementById('addFinancialHref').href = baseUrl + "area-diretorio/financeiro-novo/financial-" + report;
    if(report == "detailing") report = "de Receitas/Despesas";
    document.getElementById('addFinancialHrefText').textContent = "Relatório " + report;
}

$(document).ready(function (){
    if (term == 0) $("#modal").modal('show');
    addReport("detailing");
})