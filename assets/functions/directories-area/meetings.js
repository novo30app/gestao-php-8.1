$(document).ready(function () {
    sessionStorage.clear();
    if (stateUser > 0)getCities(stateUser);
    verifySession('Meetings');
});

function setStorageIndex() {
    setStorage('Meetings', {
        'url': 'api/area-diretorio/reunioes',
        'columns': 6,
        'variables': {
            'index': 0,
            'date': '',
            'sector': sectorFilter,
            'theme': '',
            'subject': '',
            'limit': 25
        }
    })
}

var formAdd = document.getElementById('formFile');
$("#formFile").submit(function (e) {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('formFile')) {
        closeLoading();
        return;
    }
    fetch(`${baseUrl}api/area-diretorio/reunioes/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                $('#formFile').trigger("reset");
                $('#addMeetingdModal').modal('hide');
                resetTable('Meetings');
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Meetings');
})

function generateLines(meetings) {
    let actionsFile = '';
    let actionsMedia = '';
    actionsMedia =
        `<a href="${baseUrl}area-diretorio/video/${meetings.id}"><i class="fa fa-television" aria-hidden="true" style="font-size: xx-large;"></i> Assistir</a>`;
    actionsFile =
        `<a href="${baseUrl}uploads/reunioes/${meetings.file}" target="_blank"><i class="fa fa-download" aria-hidden="true" style="font-size: x-large;"></i> Baixar</a>`;


    //se for pela URL da juventude
    if (sectorFilter == 6){
        actionsMedia =
            `<a href="${baseUrl}area-diretorio/video/${meetings.id}/juventude"><i class="fa fa-television" aria-hidden="true" style="font-size: xx-large;"></i> Assistir</a>`;
    }


    if (meetings.status == 'Desativado') {
        actionsMedia = `<label style="color: darkgrey;">Desativado</label>`;
        actionsFile = `<label style="color: darkgrey;">Desativado</label>`;
    }

    if (meetings.media == null) {
        actionsMedia = `<label style="color: darkgrey;"><i class="fa fa-ban" aria-hidden="true"></i></label>`;
    }

    if (meetings.file == null) {
        actionsFile = `<label style="color: darkgrey;"><i class="fa fa-ban" aria-hidden="true"></i></label>`;
    }

    actionsMedia = `<td class="text-center">${actionsMedia} </td>`;
    actionsFile = `<td class="text-center">${actionsFile} </td>`;

    return `<tr class="middle">
                <td class="text-center">${meetings.date}</td>
                <td>${meetings.sector ?? ''}</td>
                <td>${meetings.theme}</td>
                <td>${meetings.subject}</td>
                ${actionsFile}
                ${actionsMedia}
            </tr>`;
}