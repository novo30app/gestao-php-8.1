var classe = 'description';

$(document).ready(function () {
    inputFile();
    setTimeout(function () {
        $('.description').addClass('is-valid');
    }, 500);
})

tinymce.init({
    selector: "textarea",
    menubar: false,
    statusbar: false,
    toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image media | forecolor backcolor table',
    browser_spellcheck: true,
    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});

$("#formFile").submit(function (e) {
    event.preventDefault();
    showLoading();
    if (!ValidateForm('formFile')) {
        closeLoading();
        return;
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + 'api/area-diretorio/salva-comunicado/',
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            showNotify('success', json.message, 0);
            setTimeout(function () {
                window.location.href = baseUrl + "area-diretorio/comunicado/" + page2;
            }, 2000);
        })
        .fail(function (json) {
            closeLoading();
            showNotify('danger', json.responseJSON.message, 1500);
        });
});

$("#formFileUpload").submit(function (e) {
    event.preventDefault();
    showLoading();
    $.ajax({
        type: 'POST',
        url: baseUrl + 'api/area-diretorio/salva-arquivo/',
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            showNotify('success', json.message, 1500);
        })
        .fail(function (json) {
            closeLoading();
            showNotify('danger', json.responseJSON.message, 1500);
        });
});