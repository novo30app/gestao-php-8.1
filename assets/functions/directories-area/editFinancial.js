$("#formFile").submit(function (e) {
    event.preventDefault();
    showLoading();
    if (!ValidateForm('formFile')) {
        closeLoading();
        return;
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + 'api/area-diretorio/salva-relatorio-financeiro/',
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            showNotify('success', json.message, 1500);
            setTimeout(function () {
                window.location.href = baseUrl + "area-diretorio/relatorio-financeiro/";
            }, 2000);
        })
        .fail(function (json) {
            closeLoading();
            showNotify('danger', json.message, 1500);
        });
});