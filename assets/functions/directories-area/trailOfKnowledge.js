const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Trail');
});

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}

function generateLines(trails) {
    var action = material = '';
    if (trails.material) {
        material = `<a href="${formatAttachment(trails.material)}" target="_blank"><i class="fa fa-download" aria-hidden="true" style="font-size: x-large;"></i> Baixar</a>`;
    }
    if (canUpdate) {
        action += `<a href="#" onclick="updateClass('${trails.id}')"><i class="fa fa-pencil" aria-hidden="true" style="font-size: x-large;"></i> Editar</a>`;
    }
    if (canDelete) {
        action += `<br><a href="#" onclick="delElement('api/cadastros/altera-trilha-do-conhecimento', 'Trilha', '${trails.id}', 'delete', 'Trail')">
                        <i class="fa fa-times" aria-hidden="true" style="font-size: x-large;"></i> Remover
                    </a>`;
    }
    let link = `https://www.youtube.com/embed/${trails.link}?modestbranding=1&controls=1&enablejsapi=1&autoplay=0`;
    let text = trails.link;
    if(text.includes("novo.org.br")) link = trails.link;
    return `<tr class="middle">
					<td class="text-center">${trails.title}</td>
					<td class="text-center">${trails.description}</td>
					<td class="text-center">${trails.module}</td>
					<td class="text-center">${material}</td>
					<td class="text-center">
						<iframe width="300" height="200" 
							src="${link}" allowfullscreen>
						</iframe>
					</td>
					<td class="text-center">${action}</td>
				</tr>`;
}

function updateClass(trail) {
    fetch(`${baseUrl}api/cadastros/pega-trilha-do-conhecimento/${trail}/`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            let t = json.message;
            $("#trail").val(t['id']);
            $("#title").val(t['class']);
            $("#module").val(t['module']);
            $("#link").val(t['link']);
            $("#description").val(t['description']);
            if(t['material'] != "") {
                $('#viewFileDiv').removeClass('d-none');
                $('#viewFile').prop('href', formatAttachment(t['material']))
            }
        });
    });
    //$('#materialDiv').hide();
    $('#fileBtn').removeClass('is-invalid');
    $('#material').prop('required', false);
    $('#viewFileDiv').show();
    $('#exampleModal').modal('show');
}

function changeClass(trail) {
    fetch(`${baseUrl}/${trail}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Trail');
                changeModal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function setStorageIndex() {
    const columns = $('#tableTrail th');
    setStorage('Trail', {
        'url': 'api/area-diretorio/trilha-do-conhecimento',
        'columns': columns.length,
        'variables': {
            'index': 0,
            'keyWord': '',
            'modules': '',
            'classes': '',
            'limit': 10
        }
    })
}

$(document).ready(function () {
    inputFile();
    verifySession('Trail');
});

function getClassOption(classes) {
    return `<option value="${classes.id}">${classes.name}</option>`;
}

function getClass(moduleType) {
    let element = 'classes';
    fetch(`${baseUrl}api/area-diretorio/trilha-do-conhecimento-aulas/${moduleType}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let c = json.message;
            let options;
            options += '<option value="">Selecione a Aula</option>' + c.map(getClassOption);
            document.getElementById(element).innerHTML = options;
        });
    });
}

function changeModal(action) {
    document.getElementById('form').reset();
    $('#fileBtn').addClass('btn-orange is-invalid').removeClass('btn-success');
    $('#materialLabel').text('Anexar Arquivo');
    $('#materialDiv').show();
    $('#material').prop('required', true);
    $('#viewFileDiv').hide();
    $('#exampleModal').modal(action);
}

const form = document.getElementById('form');
$("#form").submit(function (e) {
    e.preventDefault();
    showLoading();
    //if (!ValidateForm('form')) {
    //   closeLoading();
    //    return;
    //}
    fetch(`${baseUrl}api/cadastros/salva-trilha-do-conhecimento/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(form)
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Trail');
                changeModal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});