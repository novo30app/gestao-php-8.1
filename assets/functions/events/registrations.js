const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('EventsRegistration');
})

function generateLines(participants) {
    return `<tr>
                    <td class="text-center">${participants.nome}</td>
                    <td class="text-center">${participants.situation}</td>
                    <td class="text-center">${participants.email}</td>
                    <td class="text-center">
                        <a rel="noopener" href="https://api.whatsapp.com/send?phone=${participants.phone}" target="_blank">
                            ${participants.phone}
                       </a>
                    </td>                    
                    <td class="text-center">${participants.cpf}</td>
                    <td class="text-center">${participants.estado}</td>
                    <td class="text-center">${participants.cidade}</td>
                    <td class="text-center">${participants.ticket ? participants.ticket : 'Desconhecido'}</td>
                    <td class="text-center">${participants.valor}</td>
                    <td class="text-center">${participants.paymentMethod}</td>
                    <td class="text-center">${participants.data_criacao}</td>
                    <td class="text-center">${participants.recommendation}</td>
                    <td class="text-center">${participants.status[0].toUpperCase() + participants.status.substring(1)}</td>
                </tr>`;
}

function csv(id) {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let cpf = formData.cpf;
    let status = formData.status;
    let ticket = formData.ticket;
    window.open(`${baseUrl}api/eventos/exportCsv/?id=${id}&name=${name}&cpf=${cpf}&status=${status}&ticket=${ticket}`, '_blank');
}

function setStorageIndex() {
    let url = window.location.href;
    setStorage('EventsRegistration', {
        'url': 'api/eventos/participantes',
        'columns': 12,
        'variables': {
            'index': 0,
            'id': id,
            'name': '',
            'cpf': '',
            'status': '',
            'ticket': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    $('#cpf').mask("999.999.999-990");
    verifySession('EventsRegistration');
});