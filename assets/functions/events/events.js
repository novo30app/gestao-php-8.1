const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Events');
})

function generateLines(event) {
    press = editar = checkin = inscricoes = documentos = cupons = ``;
    if (event.id == 5700) {
        press = `<a type="button" class="btn btn-success" href="${baseUrl}eventos/imprensa/${event.id}" target="_blank" style="margin-top: 5px;"><i class="fa fa-camera" aria-hidden="true"></i> Imprensa</a>`;
    }
    if (canUpdate) {
        editar = `<a type="button" class="btn btn-primary" href="${baseUrl}eventos/editar-evento/${event.id}" target="_blank"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>`;
        checkin = `<a type="button" class="btn btn-secondary" href="${baseUrl}eventos/checkin/${event.id}" target="_blank"><i class="fa fa-check" aria-hidden="true"></i> Checkin</a>`;
        inscricoes = `<a type="button" class="btn btn-warning" href="${baseUrl}eventos/inscricoes/${event.id}" target="_blank"><i class="fa fa-users" aria-hidden="true"></i> Inscrições</a>`;
        documentos = `<a type="button" class="btn btn-success" href="${baseUrl}eventos/documentos/${event.id}" target="_blank"><i class="fa fa-file" aria-hidden="true"></i> Documentos</a>`;
        cupons = `<a class="btn btn-danger" type="button" href="${baseUrl}eventos/cupons/${event.id}" target="_blank"><i class="fa fa-ticket" aria-hidden="true"></i> Cupons</a>`;
    }
    return `<tr class="middle" id="line${event.id}">
                        <td class="text-center">${event.id}</td>
                        <td class="text-center"><a href="https://novo.org.br/inscricao/?id=${event.id}" target="_blank">${event.nome} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                        <td class="text-center">${event.dataInicio}</td>
                        <td class="text-center">${event.diretorio}</td>
                        <td class="text-center">${event.status}</td>
                        <td class="text-center">${event.tipo}</td>
                        <td class="text-center">${event.especie}</td>
                        <td class="text-center">${event.city}/${event.state}</td>
                        <td>${editar}</td>
                        <td>${checkin}</td>
                        <td>
                             ${inscricoes}
                             ${press}
                        </td>
                        <td>${documentos}</td>
                        <td>${cupons}</td>
                    </tr>`;
}

function showModalCancel(id) {
    $('#confirmCancel').prop('onclick', null).off('click').click(function () {
        cancela(id);
    });
    $('#modalCancel').modal('show');
}

function cancela(id) {
    showLoading();
    let formData = formDataToJson('eventForm');
    fetch(`${baseUrl}api/eventos/cancela/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                $('#line' + id).hide(300);
                $('#modalCancel').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function setStorageIndex() {
    setStorage('Events', {
        'url': 'api/eventos/json',
        'columns': 14,
        'variables': {
            'index': 0,
            'name': '',
            'directory': '',
            'specie': '',
            'status': 'ativo',
            'state': '',
            'city': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    if (userLevel == 2) getCities(userState, 'cityEventsFilter');
    verifySession('Events');
});