var classe = 'description';

$(document).ready(function () {
    setTimeout(function () {
        setBorder('description')
    }, 500);
    inputFile();
    $(".money").maskMoney({thousands: '.', decimal: ',', allowZero: true});
    $("#zipCode").keyup(function () {
        loadAddress('zipCode', 'street', 'district');
    });
    verifyEvent();
})

function verifyEvent() {
    let base_url = window.location.href;
    let arr = base_url.split('/');
    let index = arr.length - 2;
    if (arr[index] == 'editar-evento') {
        index = arr.length - 1;
        searchEvent(arr[index]);
    } else {
        $("#specie").val(5);
        appear('5');
    }
}

function searchEvent(id) {
    fetch(`${baseUrl}eventos/informacoes/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            getEvent(json.message);
            getPrices(json.prices)

        });
    });
}

function getEvent(event) {
    $('#model, #type, #category, #link, #responsible-affiliate').attr('disabled', true);
    switch (event.specie) {
        case '3':
            $('#link').attr('disabled', false);
            $('#divPrices').removeClass('d-none');
            validLocation(0);
            break;
        case '2':
        case '4':
            $('#responsible-affiliate').attr('disabled', false);
            break;
        case '5':
            $('#divPrices').removeClass('d-none');
            break;
        case '1':
        case '6':
            $('#divPrices').removeClass('d-none');
            validLocation(0);
            break;

    }

    //if (event.oficial == 0) $('#show').attr('disabled', true);

    if (event.type != 'gratuito' && (event.specie == 3 || event.specie == 5)) $('#informativeDiv').removeClass('d-none');

}

function getPrices(price) {
    let html = '';
    for (let i = 0; i < price.length; i++) {
        html +=
            `<div class="col-md-6" style="padding: 3px;">
                <div class="col-12 bg-secondary" style="border-radius: 5px; text-align: left;padding: 25px;">
                <i class="fa fa-times text-danger" onclick="removeValue(this)"></i>
                <ul>
                    <li>Descrição: ${ price[i].description }</li>
                    <li>Valor: R$ ${ price[i].valueFormated }</li> 
                    <li>Data de ínicio de venda: ${ price[i].dateBegin }</li>
                    <li>Data de Término de venda: ${ price[i].dateFinal }</li>
                    <li>Quantidade por Pessoa: ${ price[i].limit }</li>
                    <li>Quantidade Total: ${ price[i].limitTotal }</li>
                    <li>Público alvo: ${ price[i].publicString }</li> 
                </ul>
                <input type="hidden" id="descriptionValue[${values}]" name="descriptionValue[]" value="${ price[i].description }">
                <input type="hidden" id="value[${values}]" name="value[]" value="${ price[i].valueFormated }">
                <input type="hidden" id="datebeginValue[${values}]" name="datebeginValue[]" value="${ price[i].dateBegin }">
                <input type="hidden" id="datefinalValue[${values}]" name="datefinalValue[]" value="${ price[i].dateFinal }">
                <input type="hidden" id="amount[${values}]" name="amount[]" value="${ price[i].limit }">
                <input type="hidden" id="range[${values}]" name="range[]" value="${ price[i].limitTotal }">
                <input type="hidden" id="publicValue[${values}]" name="publicValue[]" value="${ price[i].publicString }">
                </div>
            </div>`;
    }
    $('#content').append(html);
}

tinymce.init({
    selector: "textarea",
    menubar: false,
    statusbar: false,
    toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image media | forecolor backcolor table',
    browser_spellcheck: true,
    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});

function appear(div) {
    $("#specie").val(div);
    document.getElementById('informativeDiv').classList.add("d-none");
    setOficial(1);
    emptyValueDiv();
    switch (div) {
        case '5':
            document.getElementById('divPrices').classList.remove("d-none");
            document.getElementById('alertWoman').classList.add("d-none");
            $("#type").val('');
            document.getElementById('type').removeAttribute('disabled', '');
            $("#category").val(4);
            document.getElementById('category').setAttribute('disabled', '');
            $('#oficial').val("");
            document.getElementById('oficial').removeAttribute('disabled', '');
            document.getElementById('link').setAttribute('disabled', '');
            document.getElementById('responsible-affiliate').setAttribute('disabled', '');
            validLocation(1);
            break;
        case '6':
        case '1':
            document.getElementById('divPrices').classList.remove("d-none");
            document.getElementById('alertWoman').classList.add("d-none");
            $("#type").val('pago');
            document.getElementById('type').setAttribute('disabled', '');
            $("#category").val(1);
            document.getElementById('category').setAttribute('disabled', '');
            $('#oficial').val(1);
            document.getElementById('oficial').setAttribute('disabled', '');
            document.getElementById('link').setAttribute('disabled', '');
            document.getElementById('responsible-affiliate').setAttribute('disabled', '');
            document.getElementById('informativeDiv').classList.add("d-none");
            document.getElementById('divPrices').classList.remove("d-none");
            validLocation(0);
            break;
        case '3':
            document.getElementById('divPrices').classList.remove("d-none");
            document.getElementById('alertWoman').classList.add("d-none");
            $("#type").val('');
            document.getElementById('type').removeAttribute('disabled', '');
            $("#category").val(4);
            document.getElementById('category').setAttribute('disabled', '');
            $('#oficial').val("");
            document.getElementById('oficial').removeAttribute('disabled', '');
            document.getElementById('link').removeAttribute('disabled', '');
            document.getElementById('responsible-affiliate').setAttribute('disabled', '');
            document.getElementById('informativeDiv').classList.add("d-none");
            document.getElementById('divPrices').classList.add("d-none");
            validLocation(0);
            break;
        case '2':
        case '4':
            document.getElementById('divPrices').classList.add("d-none");
            document.getElementById('alertWoman').classList.add("d-none");
            $("#type").val('gratuito');
            document.getElementById('type').setAttribute('disabled', '');
            $("#category").val(4);
            document.getElementById('category').setAttribute('disabled', '');
            $('#oficial').val("");
            document.getElementById('oficial').removeAttribute('disabled', '');
            document.getElementById('link').setAttribute('disabled', '');
            document.getElementById('responsible-affiliate').removeAttribute('disabled', '');
            document.getElementById('informativeDiv').classList.add("d-none");
            document.getElementById('divPrices').classList.add("d-none");
            validLocation(1);
            break;
    }
}

function appear2(div) {
    emptyValueDiv();
    switch (div) {
        case 'pago':
            document.getElementById('informativeDiv').classList.remove("d-none");
            document.getElementById('divPrices').classList.remove("d-none");
            $("#price-value").val('');
            document.getElementById('price-value').removeAttribute('readonly', '');
            break;
        case 'gratuito':
        case 'misto':
            document.getElementById('informativeDiv').classList.add("d-none");
            document.getElementById('divPrices').classList.remove("d-none");
            if(div == 'gratuito') {
                $("#price-value").val('0,00');
                document.getElementById('price-value').setAttribute('readonly', '');    
            }
            break;
    }
}

function validLocation(value) {
    if (value == 1) {
        document.getElementById('zipCode').removeAttribute('disabled', '');
        document.getElementById('street').removeAttribute('disabled', '');
        document.getElementById('local').removeAttribute('disabled', '');
        document.getElementById('district').removeAttribute('disabled', '');
        document.getElementById('complement').removeAttribute('disabled', '');
    } else {
        document.getElementById('zipCode').setAttribute('disabled', '');
        document.getElementById('street').setAttribute('disabled', '');
        document.getElementById('local').setAttribute('disabled', '');
        document.getElementById('district').setAttribute('disabled', '');
        document.getElementById('complement').setAttribute('disabled', '');
    }
}

function loadAddress(zipCode, street, district) {
    zipCode = document.getElementById(zipCode);
    street = document.getElementById(street);
    district = document.getElementById(district);
    var cep = $(zipCode).val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                $(street).val(dados.logradouro);
                $(district).val(dados.bairro);
            }
        });
    }
}

let values = 0;

function addValue() {
    if (values >= 32) {
        alert('Limite de ingressos');
        return;
    }
    let descriptionValue = $("#price-desc").val();
    let value = $("#price-value").val();
    let datebeginValue = $("#price-datebegin").val();
    let datefinalValue = $("#price-datefinal").val();
    let amount = $("#price-amount").val();
    let range = $("#price-range").val();
    let publicValue = $("#price-public").val();
    if (publicValue == 1) {
        publicValueString = 'Valor para filiados';
    } else {
        publicValueString = 'Valor para todos';
    }
    let array = [descriptionValue, value, datebeginValue, datefinalValue, amount, range, publicValue];
    if (datebeginValue.length < 16 || datefinalValue.length < 16) {
        alert('Digite as datas do ingresso com dia,mes,ano e horário!');
        return;
    }
    if (array.includes('')) {
        alert('Verifique se todos os campos do ingresso foram digitados!');
        return;
    }
    let html = ` <div class="btn btn-secondary" style="border: 1px black solid; border-radius: 5px; text-align: left; margin: 5px; padding: 20px;">
                        <i class="fa fa-times text-danger" onclick="removeValue(this)"></i>
                        <ul>
                        <li>Descrição: ${descriptionValue}</li>
                        <li>Valor: ${value}</li>
                        <li>Data de înicio de venda: ${datebeginValue}</li>
                        <li>Data de Término de venda: ${datefinalValue}</li>
                        <li>Qtd Pessoa: ${amount}</li>
                        <li>Qtde Total: ${range}</li>
                        <li>Público alvo: ${publicValueString}
                        </ul>
                    <input type="hidden" id="descriptionValue[${values}]" name="descriptionValue[]" value="${descriptionValue}">
                    <input type="hidden" id="value[${values}]" name="value[]" value="${value}">
                    <input type="hidden" id="datebeginValue[${values}]" name="datebeginValue[]" value="${datebeginValue}">
                    <input type="hidden" id="datefinalValue[${values}]" name="datefinalValue[]" value="${datefinalValue}">
                    <input type="hidden" id="amount[${values}]" name="amount[]" value="${amount}">
                    <input type="hidden" id="range[${values}]" name="range[]" value="${range}">
                    <input type="hidden" id="publicValue[${values}]" name="publicValue[]" value="${publicValue}">
                </div> `
    $("#valueDiv").append(html);
    values++;
}

function removeValue(i) {
    values--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

var formAdd = document.getElementById('paidEventForm');
$("#paidEventForm").submit(function (e) {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('paidEventForm')) {
        closeLoading();
        return;
    }
    fetch( baseUrl + 'api/eventos/salva-evento-pago/', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.href = baseUrl + "eventos/";
                }, 2000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function setOficial(value) {
    $("#show").val(value);
    if (value == 0) {
        document.getElementById('show').setAttribute('disabled', '');
    } else {
        document.getElementById('show').removeAttribute('disabled', '');
    }
}

function emptyValueDiv() {
    let valueDiv = document.getElementById("valueDiv");
    valueDiv.innerText = "";
}
