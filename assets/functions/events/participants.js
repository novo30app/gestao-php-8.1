var index = partial = 0;
var total = 1;
var name = cpf = status = ticket = gender = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    cpf = formData.cpf;
    status = formData.status;
    ticket = formData.ticket;
    gender = formData.gender;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = cpf = status = ticket = gender = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableList(participants) {   
    return `<tr class="middle">
                <td class="text-center">${participants.nome}</td>
                <td class="text-center">${participants.situation}</td>
                <td class="text-center">${participants.email}</td>
                <td class="text-center">
                    <a rel="noopener" href="https://api.whatsapp.com/send?phone=${participants.phone}" target="_blank">
                        ${participants.phone}
                    </a>
                </td>
                <td class="text-center">${participants.cpf}</td>
                <td class="text-center">${participants.estado}</td>
                <td class="text-center">${participants.cidade}</td>
                <td class="text-center">${participants.ticket ? participants.ticket : 'Desconhecido'}</td>
                <td class="text-center">${participants.valor}</td>
                <td class="text-center">${participants.paymentMethod}</td>
                <td class="text-center">${participants.data_criacao}</td>
                <td class="text-center">${participants.recommendation}</td>
                <td class="text-center">${participants.status[0].toUpperCase() + participants.status.substring(1)}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}api/eventos/participantes/?index=${index}&name=${name}&id=${id}&cpf=${cpf}&status=${status}&ticket=${ticket}&gender=${gender}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableList);
            $("#table tbody").append(options);
        });
    });
}

function csv(id) {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let cpf = formData.cpf;
    let status = formData.status;
    let ticket = formData.ticket;
    window.open(`${baseUrl}api/eventos/exporta-participantes/?id=${id}&name=${name}&cpf=${cpf}&status=${status}&ticket=${ticket}&gender=${gender}`, '_blank');
}

function csvTransactions(id) {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let cpf = formData.cpf;
    let status = formData.status;
    let ticket = formData.ticket;
    window.open(`${baseUrl}api/eventos/exporta-participantes-transacoes/?id=${id}&name=${name}&cpf=${cpf}&status=${status}&ticket=${ticket}&gender=${gender}`, '_blank');
}

$(document).ready(function() {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
    $('#cpf').mask("999.999.999-990");
});
