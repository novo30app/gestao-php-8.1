var index = partial = 0;
var total = 1;
var name = vehicle = phone = type = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    vehicle = formData.vehicle;
    phone = formData.phone;
    type = formData.type;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = vehicle = phone = type = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableCoupons(press) {
    let professionals = ``;
    if(press.camera == 1) professionals = 'Camera';
    if(press.camera == 1 && press.photographer == 1) professionals += ' / ';
    if(press.photographer == 1) professionals += 'Fotógrafo';

    return `<tr class="middle">
                <td>${press.reporter}</td>
                <td class="text-center">${press.vehicle}</a></td>
                <td class="text-center">${press.phone}</td>
                <td class="text-center">${professionals}</td>
                <td class="text-center">${press.created}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}api/eventos/imprensa/?index=${index}&name=${name}&vehicle=${vehicle}&phone=${phone}&type=${type}&eventId=${eventId}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableCoupons);
            $("#table tbody").append(options);
        });
    });
}

$(document).ready(function() {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});

function csv(id) {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let cpf = formData.cpf;
    let status = formData.status;
    let ticket = formData.ticket;
    window.open(`${baseUrl}api/eventos/exportCsvPress/?eventId=${eventId}&name=${name}&vehicle=${vehicle}&phone=${phone}&type=${type}`, '_blank');
}