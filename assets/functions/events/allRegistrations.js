const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('GeralRegistrations');
});

function generateLines(registrations) {
    let link = `${registrations.name}`;
    if(registrations.personId) link = `<a href="${baseUrl}filiados/${registrations.personId}" target="_blank">${registrations.name} <i class="fa fa-external-link" aria-hidden="true"></i></a>`;
    let event = registrations.event;
    if (canUpdate) event = `<a href="${baseUrl}eventos/editar-evento/${registrations.id}" target="_blank">${registrations.event} <i class="fa fa-external-link" aria-hidden="true"></i></a>`;
    return `<tr class="middle">
                <td>${link}</td>
				<td class="text-center">${registrations.gender}</td>
                <td class="text-center">${registrations.cpf}</td>
				<td class="text-center">${registrations.status}</td>
                <td class="text-center">${event}</td>
				<td class="text-center">${registrations.eventDate}</td>
                <td class="text-center">${registrations.location}</td>
                <td class="text-center" style="width: 20%">${registrations.recommendation}</td>
			</tr>`;
}

function setStorageIndex() {
    const columns = $('#tableGeralRegistrations th');
    setStorage('GeralRegistrations', {
        'url': 'api/eventos/participantes-geral',
        'columns': columns.length,
        'variables': {
            'index': 0,
            'event': '',
            'name': '',
            'gender': '',
            'cpf': '',
            'status': '',
            'state': '',
            'city': '',
            'limit': 25
        }
    })
}

function csv() {
    let formData = formDataToJson('filter');
    let event = formData.event;
    let name = formData.name;
    let cpf = formData.cpf;
    let status = formData.status;
    let state = formData.state;
    let city = formData.city    ;
    window.open(
        `${baseUrl}api/eventos/participantes-geral-exporta/?event=${event}&name=${name}&cpf=${cpf}&status=${status}&state=${state}&city=${city}`, 
        '_blank');
}

$(document).ready(function () {
    if (stateUser > 0) getCities(stateUser, 'cityGeralRegistrationsFilter');
    verifySession('GeralRegistrations');
});