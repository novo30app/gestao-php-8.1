const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('EventsCheckin');
})

function generateLines(participants) {
    let checkin = `<td class="text-center">
                    <button class="btn btn-primary" type="submit" onclick="doCheckin('${participants.id}')">
                        <i class="fa fa-hand-pointer" aria-hidden="true"></i> Realizar Check-in
                    </button>
                   </td>`;
    if (participants.data_checkin != 'n/r') checkin = `<td class="text-center">
                                                        <a class="btn btn-success text-white">
                                                            <i class="fa fa-check" aria-hidden="true"></i> Check-in Realizado
                                                        </a>
                                                       </td>`;
    let color = 'green';
    if(participants.status != 'confirmado') color = 'red';
    let status = `<td class="text-center" style="color: ${color}; font-size: large">${participants.status}</td>`;
    return `<tr class="middle">
                <td class="text-center">${participants.code}</td>
                <td class="text-center">${participants.nome}</td>
                <td class="text-center">${participants.cpf}</td>
                ${status}
                <td class="text-center">${participants.data_checkin}</td>
                ${checkin}
            </tr>`;
}

const eventForm = document.getElementById('eventForm');
function doCheckin(id) {
    eventForm.addEventListener('submit', e => {
        e.preventDefault();
        showLoading();
        let formData = formDataToJson('eventForm');
        fetch(`${baseUrl}api/eventos/faz-checkin/${id}`, {
            method: 'PUT',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }).then(response => {
            window.scrollTo(0, 0);
            closeLoading();
            response.json().then(json => {
                if (response.status === 201) {
                    showNotify('success', json.message, 1500);
                    setTimeout(function () {
                        resetTable('EventsCheckin');
                    }, 2000);
                } else {
                    showNotify('danger', json.message, 1500);
                }
            });
        });
    });
}

const formRegister = document.getElementById('formRegister');
formRegister.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('formRegister')) {
        closeLoading();
        return;
    }
    let formData = formDataToJson('formRegister');
    fetch(`${baseUrl}api/eventos/novo-participante/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function setStorageIndex() {
    let url = window.location.href;
    let id = url.replace(baseUrl + 'eventos/checkin/', '');
    setStorage('EventsCheckin', {
        'url': 'api/eventos/checkin',
        'columns': 7,
        'variables': {
            'index': 0,
            'id': id,
            'code': '',
            'name': '',
            'cpf': '',
            'status': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    $("#cpf").mask("999.999.999-99");
    verifySession('EventsCheckin');
});