var index = partial = 0;
var total = 1;
var name = event = status = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    event = formData.event;
    status = formData.status;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = event = status = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableCoupons(coupons) {
    let actions = ``;
    if(coupons.statusId == 1) {
        actions = `<a href="#" onclick="changeCoupons('${coupons.id}', 2)"><i class="fa fa-times" aria-hidden="true" style="font-size: small; color: red"> Remover</i></a><br>`;
    } else {
        actions = `<a href="#" onclick="changeCoupons('${coupons.id}', 1)"><i class="fa fa-check" aria-hidden="true" style="font-size: small; color: green"> Ativar</i></a><br>`;
    }
    
    return `<tr class="middle">
                <td class="text-center">${coupons.name}</td>
                <td class="text-center"><a href="https://novo.org.br/inscricao/?id=${coupons.eventId}" target="_blank">${coupons.event} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${coupons.code}</td>
                <td class="text-center">${coupons.user}</td>
                <td class="text-center">${coupons.created}</td>
                <td class="text-center">${coupons.status}</td>
                <td class="text-center">${coupons.limitPerPerson}</td>
                <td class="text-center">${coupons.used}</td>
                <td class="text-center">${actions}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}api/eventos/cupons/?index=${index}&name=${name}&event=${event}&status=${status}&eventId=${eventId}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableCoupons);
            $("#table tbody").append(options);
        });
    });
}

function changeModal(modal, action) {
    $(modal).modal(action);
}

function setType(type) {
    if(type == 2) {
        document.getElementById('valueDiv').classList.remove('d-none');
        document.getElementById('porcentDiv').classList.add('d-none');
    } else {
        document.getElementById('valueDiv').classList.add('d-none');
        document.getElementById('porcentDiv').classList.remove('d-none');
    }
}

$(document).ready(function() {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });

    $("#value").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
});

const formModal = document.getElementById('formModal');
const divMessageFormData = document.getElementById('divMessageFormData');

$("#formModal").submit(function(e) {
    document.getElementById("save").disabled = false;
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}eventos/salva-cupom/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formModal)
    }).then(response => {
        document.getElementById("save").disabled = false;
        response.json().then(json => {
            closeLoading();
            divMessageFormData.textContent = json.message;
            divMessageFormData.style.display = 'block';
            divMessageFormData.classList.add("alert-danger");
            divMessageFormData.classList.remove("alert-success");
            if (response.status === 201) {
                divMessageFormData.classList.add("alert-success");
                divMessageFormData.classList.remove("alert-danger");
                setTimeout(function() {
                    window.location.reload();
                }, 2000);
            }
        });
    });
});

const divMessage = document.getElementById('divMessage');
function changeCoupons(id, status) {
    showLoading();
    fetch(`${baseUrl}eventos/altera-cupom/${id}/${status}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            divMessage.textContent = json.message;
            divMessage.style.display = 'block';
            divMessage.classList.add("alert-danger");
            divMessage.classList.remove("alert-success");
            if (response.status === 201) {
                divMessage.classList.add("alert-success");
                divMessage.classList.remove("alert-danger");
                setTimeout(function() {
                    window.location.reload();
                }, 2000);
            }
        });
    });
}