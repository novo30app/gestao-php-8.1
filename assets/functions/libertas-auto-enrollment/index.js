function openModal(id) {
    form.reset();
    $('#autoEnrollmentId').val(id);
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form')
        fetch(baseUrl + 'libertas/auto-inscricao-moodle/json/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                closeLoading();
                json = json.message[0]
                $('#email').val(json.email);
                document.getElementById('auto_enrollment').checked = json.auto_enrollment;
                document.getElementById('mandatario_legislativo').checked = json.mandatario_legislativo;
                document.getElementById('mandatario_executivo').checked = json.mandatario_executivo;
            })
        })
    }
}

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + 'libertas/auto-inscricao-moodle/cadastro', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('AutoEnrollment');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function generateLines(autoEnrollment) {
    let actions = actions2 = ``;
    if (canUpdate) actions = `<a onclick="openModal(${autoEnrollment.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    if (canDelete) actions2 = `${buttonActive(autoEnrollment.active, 'libertas/auto-inscricao-moodle', autoEnrollment.id, 'AutoEnrollment', 'put')}`;
    return `<tr class="middle">
                     <td class="text-center">${autoEnrollment.id} </td>
                    <td class="text-center">${autoEnrollment.email} </td>
                    <td class="text-center">${autoEnrollment.auto_enrollment ? 'Sim' : 'Não'} </td>
                    <td class="text-center">${autoEnrollment.mandatario_legislativo ? 'Sim' : 'Não'} </td>
                    <td class="text-center">${autoEnrollment.mandatario_executivo ? 'Sim' : 'Não'} </td>
                    <td class="text-center">${actions2}</td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}


const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('AutoEnrollment');
})

$(document).ready(function () {
    sessionStorage.clear();
    verifySession('AutoEnrollment');
});

function setStorageIndex() {
    setStorage('AutoEnrollment', {
        'url': 'libertas/auto-inscricao-moodle/json',
        'columns': 4,
        'variables': {
            'index': 0,
            'email': '',
            'auto': '',
            'legislativeAgent': '',
            'executiveAgent': '',
            'status': '',
            'limit': 25
        }
    })
}

function csvImport() {
    $("#modalImport").modal('show');
}

const divMessageImport = document.getElementById('messageImport');
const formImport = document.getElementById('formImport');
formImport.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}libertas/auto-inscricao-moodle/importacao/`, {
        method: 'POST',
        credentials: "same-origin",
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(formImport),
    }).then(response => {
        closeLoading();
        divMessageImport.classList.add("alert-danger");
        divMessageImport.classList.remove("alert-success");
        response.json().then(json => {
            divMessageImport.innerHTML = json.message;
            divMessageImport.style.display = 'block';
            if (response.status === 201) {
                $("#modalImport").modal('hide');
                divMessageImport.classList.add("alert-success");
                divMessageImport.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        });
    });
});