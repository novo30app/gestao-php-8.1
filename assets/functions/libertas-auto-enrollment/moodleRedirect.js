function openModal(id) {
    form.reset();
    $('#moodleRedirectId').val(id);
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form')
        fetch(baseUrl + 'libertas/redirecionamentos-moodle/json/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                closeLoading();
                json = json.message[0]
                $('#shortLinkModal').val(json.shortLink);
                $('#moodleLinkModal').val(json.moodleLink);
                $('#statusModal').val(json.status);
            })
        })
    }
};

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + 'libertas/redirecionamentos-moodle/cadastro', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('MoodleRedirect');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('MoodleRedirect');
});

function generateLines(moodleLink) {
    let actions = `<a onclick="openModal(${moodleLink.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;

    return `<tr class="middle">
                <td class="text-center">${moodleLink.id}</td>
                <td class="text-center">https://espaco-novo.novo.org.br/moodle/${moodleLink.shortLink}</td>
                <td class="text-center">${moodleLink.moodleLink}</td>
                <td class="text-center">${canUpdate ? actions : ''}</td>
            </tr>`;
}

function setStorageIndex() {
    setStorage('MoodleRedirect', {
        'url': 'libertas/redirecionamentos-moodle/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'shortLink': '',
            'moodleLink': '',
            'limit': 25
        }
    })
};

$(document).ready(function () {
    sessionStorage.clear();
    verifySession('MoodleRedirect');
});