var index = 0;
var partial = 0;
var total = 1;
var name = state = year = '';

const form = document.getElementById('filter');

form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    name = formData.name;
    state = formData.state;
    year = formData.year;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    name = state = year = '';
    $("#table tbody").empty();
    generateTable();
}

function generateList(cabinets) {
    return `<tr>
                <td><a href="${baseUrl}dam/gabinete/${cabinets.id}" target="_blank">${cabinets.name}</a></td>
                <td class="text-center">${cabinets.state}</td>
                <td class="text-center">${cabinets.city}</td>
                <td class="text-center">${cabinets.elected}</td>
            </tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/dam/gabinetes/?index=${index}&name=${name}&state=${state}&year=${year}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let cabinetsList = json.message;
                let options = cabinetsList.map(generateList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

function openModal(id) {
	$("#modal").modal('show');
}

function changeMessageType(type) {
	if (type === 'ok') {
		message.classList.remove('alert-danger');
		message.classList.remove('d-none');
		message.classList.add('alert-info');
	}
	if (type === 'error') {
		message.classList.remove('alert-info');
		message.classList.remove('d-none');
		message.classList.add('alert-danger');
	}
}

const formAdd = document.getElementById('form');
const message = document.getElementById('messageModal');
formAdd.addEventListener('submit', e => {
	e.preventDefault();
	showLoading();
	let formData = formDataToJson('form');
	fetch(`${baseUrl}api/dam/gabinete`, {
		method: 'POST',
		credentials: "same-origin",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(formData),
	}).then(response => {
		closeLoading();
		response.json().then(json => {
			changeMessageType(json.message.status);
			message.innerText = json.message.message;
			if (json.message.status === 'ok') {
				$("#modal").modal('hide');
				setTimeout(function () {
					window.location.href = `<?= BASEURL ?>dam/gabinetes`;
				}, 1000);
			}
		});
	});
});

$(document).ready(function () {
	generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});