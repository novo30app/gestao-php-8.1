const form = document.getElementById('form');
const messageDiv = document.getElementById('messageModal');

let users = [];

function openModal(id) {
	messageDiv.classList.add('d-none');
	form.reset();
	$("#userId").val(id);
	if (id > 0) {
		for (var i = 0; i < users.length; i++) {
			if (users[i].id == id) {
				$("#modal #name").val(users[i].name);
				$("#modal #email").val(users[i].email);
				$("#modal #cabinet").val(users[i].cabinet);
			}
		}
	}
	$("#modal").modal('show');
}

form.addEventListener('submit', e => {
	e.preventDefault();
	showLoading();
	let formData = formDataToJson('form');
	let method = 'POST';
	if (formData.userId > 0) {
		method = 'PUT';
	}
	fetch(`${baseUrl}api/dam/assessores/`, {
		method: method,
    	credentials: "same-origin",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(formData),
	}).then(response => {
		closeLoading();
		response.json().then(json => {
			if (json.message.status == 'ok') {
				showNotify('success', json.message.message, 1500);
				getUsers();
				$("#modal").modal('hide');
			} else {
				showNotify('danger', json.message, 1500);
			}
		});
	});
});

function getUsers() {
	fetch(`${baseUrl}api/dam/assessores/` + cabinet, {
		method: 'GET',
		credentials: "same-origin",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		}
	}).then(response => {
		response.json().then(json => {
			users = json.message.message;
			let html = users.map(generateTable);
			$("#usersTable tbody").empty();
			$("#usersTable tbody").append(html);
		});
	});
}

function generateTable(json) {
    var action = ``;
    if(isAdminNational) action = `<i class="fa fa-pencil text-info" title="Editar" onclick="openModal(${json.id})"></i>`;
	return `<tr>
               <td>${json.name}</td>
               <td class="text-center">${json.email}</td>
               <td class="text-center">${action}</td>
           </tr>`;
}

$(document).ready(function () {
	getUsers();
});