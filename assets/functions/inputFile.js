function addDoc() {
    $('input:file').change(function (e) {
        let input = e.target;
        if (input.files.length > 0) {
            input.parentElement.classList.remove('btn-primary');
            input.parentElement.classList.remove('btn-danger');
            input.parentElement.classList.add('btn-success');
            input.text = input.value;
            let string = input.value.replace('fakepath', '');

            let labelId = input.getAttribute('id') + 'Label';
            document.getElementById(labelId).textContent = string.slice(4);
        } else {
            input.parentElement.classList.remove('btn-success');
            input.parentElement.classList.add('btn-danger');
            let labelId = input.getAttribute('id') + 'Label';
            document.getElementById(labelId).textContent = document.getElementById(labelId).getAttribute('default');
        }
    });
}