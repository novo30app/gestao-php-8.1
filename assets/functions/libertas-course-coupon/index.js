function openModal(id) {
    form.reset();
    $('#libertasCourseCouponId').val(id);
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form')
        fetch(baseUrl + 'libertas/cupons/json/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                closeLoading();
                json = json.message[0]
                $('#email').val(json.email);
            })
        })
    }
}

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + 'libertas/cupons/cadastro', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('LibertasCourseCoupon');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function generateLines(coupon) {
    let actions = `<a onclick="openModal(${coupon.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    let value = `${coupon.price}%`;
    if (coupon.type == 2) value = formatMoney(coupon.price);
    return `<tr class="middle">
                     <td class="text-center">${coupon.id} </td>
                    <td class="text-center">${coupon.courseName} </td>
                    <td class="text-center">${coupon.code} </td>
                    <td class="text-center">${value} </td>
                    <td class="text-center">${coupon.limitUse} </td>
                    <td class="text-center">${coupon.used} </td>
                      <td class="text-center">${canDelete ? buttonActive(coupon.active, 'libertas/cupons', coupon.id, 'AutoEnrollment', 'put') : ''}</td>
                  </tr>`;
}


const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('LibertasCourseCoupon');
})

$(document).ready(function () {
    sessionStorage.clear();
    verifySession('LibertasCourseCoupon');
    $("#price").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
});

function setStorageIndex() {
    setStorage('LibertasCourseCoupon', {
        'url': 'libertas/cupons/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'course': '',
            'active': '',
            'limit': 25
        }
    })
}

function setType(type) {
    if (type == 2) {
        document.getElementById('valueDiv').classList.remove('d-none');
        document.getElementById('porcentDiv').classList.add('d-none');
    } else {
        document.getElementById('valueDiv').classList.add('d-none');
        document.getElementById('porcentDiv').classList.remove('d-none');
    }
}