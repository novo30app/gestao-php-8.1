const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('LibertasCourses');
})


function generateLines(requests) {
    let status = 'Encerrado';
    if (requests.active == 1) status = 'Aberto';
    let actions = '';
    if (canUpdate) {
        actions = ` <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                             <a class="btn btn-sm btn-warning" href="${baseUrl}libertas/cursos/cadastro/${requests.id}">Editar</a>
                         </div>`;
    }
    return `<tr class="middle">
                    <td class="text-center">${requests.id}</td>
                    <td class="text-center">${requests.title}</td>
                    <td class="text-center">${formatMoney(requests.price)}</td>
                    <td class="text-center">${requests.active ? 'Aberto' : 'Encerrado'}</td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}

function setStorageIndex() {
    setStorage('LibertasCourses', {
        'url': 'libertas/cursos/json',
        'columns': 5,
        'variables': {
            'index': 0,
            'title': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    sessionStorage.clear();
    verifySession('LibertasCourses');
});