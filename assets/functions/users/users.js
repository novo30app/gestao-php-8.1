function openModal(id, a) {
    document.getElementById('form').reset();
    document.getElementById('userId').value = id;
    document.getElementById('edit').value = a;
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    document.getElementById('cityPlus').style.display = 'none';
    document.getElementById('mesoDiv').style.display = 'none';
    document.getElementById('cityDivAdd').innerHTML = "";
    $('#cardModal').modal('show');
    $('#messageForm').hide();
    if (id > 0) {
        fetch(`${baseUrl}api/cadastros/usuarios/${id}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                jsonCities = json.cities;
                jsonMesos = json.mesos;
                json = json.message[0];
                closeLoading();
                document.getElementById('typeView').value = json.typeView;
                document.getElementById('status').value = json.active;
                document.getElementById('name').value = json.name;
                document.getElementById('email').value = json.email;
                document.getElementById('level').value = json.level;
                document.getElementById('type').value = json.type;
                document.getElementById('state').value = json.state ?? '';
                if (json.adminAccess == 1) $('#adminAccess').prop('checked', true);
                if (json.directoryAreaAccess == 1) $('#directoryAreaAccess').prop('checked', true);
                if (json.exportAccess == 1) $('#exportAccess').prop('checked', true);
                if (json.financialReport == 1) $('#financialReport').prop('checked', true);
                if (json.events == 1) $('#events').prop('checked', true);
                if (json.libertasCourses == 1) $('#libertasCourses').prop('checked', true);
                getCities(json.state, 'city-plus');
                getCities(json.state, 'city');
                setTimeout(function () {
                    document.getElementById('city').value = json.city;
                }, 3000);
                jsonCities.map(({id, nome_cidade}) => {
                    addCityDiv(id, nome_cidade)
                });
                jsonMesos.map(({id, nome_meso}) => {
                    addMesoDiv(id, nome_meso)
                });
                showLocations();
            });
        });
    }
}

const formAdd = document.getElementById('form');
const divMessageForm = document.getElementById('messageForm');
formAdd.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    let method = 'POST';
    let formData = formDataToJson('form');
    if (formData.userId > 0) {
        method = 'PUT';
    }
    fetch(`${baseUrl}api/cadastros/usuarios/`, {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            divMessageForm.textContent = json.message;
            divMessageForm.style.display = 'block';
            if (response.status === 201) {
                divMessageForm.classList.add("alert-success");
                divMessageForm.classList.remove("alert-danger");
                formAdd.reset();
                generateTable();
                setTimeout(function () {
                    $('#cardModal').modal('hide');
                    window.location.reload();
                }, 3000);
            }
        });
    });
});

function remove(id) {
    const divMessage = document.getElementById('message');
    fetch(`${baseUrl}api/cadastros/usuarios/` + id, {
        method: "DELETE",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            divMessage.textContent = json.message;
            divMessage.classList.add("alert-danger");
            divMessage.classList.remove("alert-success");
            divMessage.classList.remove("d-none");
            if (response.status === 201) {
                divMessage.classList.add("alert-success");
                divMessage.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2500);
            }
        });
    });
}

function activate(id) {
    const divMessage = document.getElementById('message');
    fetch(`${baseUrl}api/cadastros/usuarios/` + id, {
        method: "PUT",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            divMessage.textContent = json.message;
            divMessage.classList.add("alert-danger");
            divMessage.classList.remove("alert-success");
            divMessage.classList.remove("d-none");
            if (response.status === 201) {
                divMessage.classList.add("alert-success");
                divMessage.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2500);
            }
        });
    });
}

function generateUsersTable(user) {
    let actions = '<i class="fa fa-ban" aria-hidden="true"></i>';
    if (canUpdate) {
        if (canDelete) actions = `<a href="#" onclick="remove(${user.id})" class="text-danger"><i class="fa fa-times" title="Inativar" ></i> Remover </a><br> `;
        actions += `<a href="${baseUrl}cadastros/edita-usuario/${user.id}"><i class="fa fa-pencil text-info" title="Editar"></i> Editar </a>`;
        if (user.active != 1) {
            actions = `<a href="#" onclick="activate(${user.id})"><i class="fa fa-refresh text-success" title="Ativar"></i> Ativar </a> `;
        }
    }
    actions = `<td class="text-center">${actions} </td>`;
    return `<tr>
            <td>${user.name}</td>
            <td class="text-center">${user.email}</td>
            <td class="text-center">${user.levelStr}</td>
            <td class="text-center">${user.ufStr}</td>
            <td class="text-center">${user.cityStr}</td>
            <td class="text-center">${user.typeStr}</td>
            <td class="text-center">${user.sector}</td>
            ${actions}
        </tr>`;
}

var index = partial = typeFilter = sectorFilter = 0;
var total = 1;
var nameFilter = emailFilter = '';

if (userLevel = 3) {
    var stateFilter = cityFilter = 0;
} else if (userLevel = 2) {
    var stateFilter = userState;
    var cityFilter = 0;
} else {
    var stateFilter = userState;
    var cityFilter = '';
}

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    nameFilter = formData.nameFilter;
    emailFilter = formData.emailFilter;
    typeFilter = formData.typeFilter;
    stateFilter = formData.stateFilter;
    cityFilter = formData.cityFilter;
    sectorFilter = formData.sectorFilter;
    $("#usersTable tbody").empty();
    generateTable();
})

function resetTable() {
    index = partial = level = typeFilter = sectorFilter = 0;
    total = 1;
    nameFilter = emailFilter = '';
    if (userLevel == 3) {
        stateFilter = cityFilter = 0;
    } else if (userLevel == 2) {
        stateFilter = userState;
        cityFilter = 0;
    } else {
        stateFilter = userState;
        cityFilter = '';
    }
    $("#usersTable tbody").empty();
    generateTable();
}

function generateTable() {
    fetch(`${baseUrl}api/cadastros/usuarios/?name=${nameFilter}&email=${emailFilter}&city=${cityFilter}&state=${stateFilter}&type=${typeFilter}&sector=${sectorFilter}&index=${index}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateUsersTable);
            $("#usersTable tbody").append(options);
        });
    });
}

function showLocations() {
    //getCities(document.getElementById('state').value)
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    document.getElementById('cityPlus').style.display = 'none';
    document.getElementById('cityDivAdd').style.display = 'none';
    document.getElementById('mesoDiv').style.display = 'none';
    const x = document.getElementById('level').value;
    if (x == '2') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityPlus').style.display = 'none';
        document.getElementById('mesoDiv').style.display = 'none';
    } else if (x == '1') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityDiv').style.display = 'block';
        if (userLevel != 1) {
            document.getElementById('cityPlus').style.display = 'block';
            document.getElementById('cityDivAdd').style.display = 'block';
            document.getElementById('mesoDiv').style.display = 'block';
        }
    }
}

let cities = 0;

function addCityDiv(id, city) {
    if (!id) {
        city = $("#city-plus").val();
        if (city == '') {
            alert('Campo Cidade Vazio');
            return;
        }
        id = city;
        city = $('#city-plus').find(":selected").text();
    }
    let html = `<div class="btn btn-success" style="border: 1px black solid; border-radius: 5px">${city} <i class="fa fa-times text-danger" onclick="removeCity(this)"></i>
                    <input class="m-input" type="hidden" id="cities[${id}]" name="cities[]" value="${id}">
                </div> `
    $("#cityDivAdd").append(html);
    cities++;
}

function removeCity(i) {
    cities--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

$(document).ready(function () {
    if (userLevel == 2) {
        getCities(userState, 'cityFilter');
        getCities(userState, 'city-plus');
        getCities(userState, 'city');
    }
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
    $(".select2").select2();
});

let mesos = 0;

function addMesoDiv(id, meso) {
    if (!id) {
        meso = $("#meso-plus").val();
        if (meso == '') {
            alert('Campo Mesorregião Vazio');
            return;
        }
        id = meso;
        meso = $('#meso-plus').find(":selected").text();
    }
    let html = `<div class="btn btn-warning" style="border: 1px black solid; border-radius: 5px">${meso} <i class="fa fa-times text-danger" onclick="removeMeso(this)"></i>
                    <input class="m-input" type="hidden" id="mesos[${id}]" name="mesos[]" value="${id}">
                </div> `
    $("#mesoDivAdd").append(html);
    mesos++;
}

function removeMeso(i) {
    mesos--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

function getMesosOption(meso) {
    return `<option value="${meso.id}">${meso.name}</option>`;
}

function getMesos(uf) {
    let element = 'meso-plus';
    fetch(`${baseUrl}api/cadastros/mesos/${uf}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let mesos = json.mesos;
            let options;
            options += '<option value="">Selecione a meso</option>' + mesos.map(getMesosOption);
            document.getElementById(element).innerHTML = options;
        });
    });
};