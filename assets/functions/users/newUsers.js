const formAdd = document.getElementById('form');
const divMessageForm = document.getElementById('messageForm');
formAdd.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let method = 'POST';
    let formData = formDataToJson('form');
    if (formData.userId > 0) {
        method = 'PUT';
    }
    fetch(`${baseUrl}api/cadastros/usuarios-funcionalidades/`, {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            divMessageForm.textContent = json.message;
            divMessageForm.style.display = 'block';
            if (response.status === 201) {
                divMessageForm.classList.add("alert-success");
                divMessageForm.classList.remove("alert-danger");
                formAdd.reset();
                setTimeout(function () {
                    $('#cardModal').modal('hide');
                    window.location.reload();
                }, 3000);
            }
        });
    });
});

function getFeatures() {
    let formData = formDataToJson('form');
    fetch(`${baseUrl}cadastros/pega-funcionalidades/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                $('.featureCheckbox').each(function(){$(this).prop("checked", false);});
                $.each(json.message, function(i, item) {
                    $('#feature' + item.feature).each(
                        function(){
                            $(this).prop("checked", true);            
                        }
                    );
                });
            };
        });
    });
};

function showLocations() {
    //getCities(document.getElementById('state').value)
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    document.getElementById('cityPlus').style.display = 'none';
    document.getElementById('cityDivAdd').style.display = 'none';
    document.getElementById('mesoDiv').style.display = 'none';
    const x = document.getElementById('level').value;
    if (x == '2') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityPlus').style.display = 'none';
        document.getElementById('mesoDiv').style.display = 'none';
    } else if (x == '1') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityDiv').style.display = 'block';
        if(userLevel != 1){
            document.getElementById('cityPlus').style.display = 'block';
            document.getElementById('cityDivAdd').style.display = 'block';
            document.getElementById('mesoDiv').style.display = 'block';   
        }
    }
}

let cities = 0;

function addCityDiv(id, city) {
    if (!id) {
        city = $("#city-plus").val();
        if (city == '') {
            alert('Campo Cidade Vazio');
            return;
        }
        id = city;
        city = $('#city-plus').find(":selected").text();
    }
    let html = `<div class="btn btn-success" style="border: 1px black solid; border-radius: 5px">${city} <i class="fa fa-times text-danger" onclick="removeCity(this)"></i>
                    <input class="m-input" type="hidden" id="cities[${id}]" name="cities[]" value="${id}">
                </div> `
    $("#cityDivAdd").append(html);
    cities++;
}

function removeCity(i) {
    cities--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

let mesos = 0;

function addMesoDiv(id, meso) {
    if (!id) {
        meso = $("#meso-plus").val();
        if (meso == '') {
            alert('Campo Mesorregião Vazio');
            return;
        }
        id = meso;
        meso = $('#meso-plus').find(":selected").text();
    }
    let html = `<div class="btn btn-secondary" style="border: 1px black solid; border-radius: 5px">${meso} <i class="fa fa-times text-danger" onclick="removeMeso(this)"></i>
                    <input class="m-input" type="hidden" id="mesos[${id}]" name="mesos[]" value="${id}">
                </div> `
    $("#mesoDivAdd").append(html);
    mesos++;
}

function removeMeso(i) {
    mesos--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

function getMesosOption(meso) {
    return `<option value="${meso.id}">${meso.name}</option>`;
}

function getMesos(uf) {
    let element = 'meso-plus';
    fetch(`${baseUrl}api/cadastros/mesos/${uf}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let mesos = json.mesos;
            let options;
            options += '<option value="">Selecione a meso</option>' + mesos.map(getMesosOption);
            document.getElementById(element).innerHTML = options;
        });
    });
};

function getData(){
    fetch(`${baseUrl}api/cadastros/pega-usuario/${userId}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let message = json.message;
            $("#name").val(message.name);
            $("#email").val(message.email);
            $("#userType").val(message.type);
            $("#level").val(message.level);
            $("#userSector").val(message.userSector);
            showLocations();
            $("#state").val(message.state);
            getCities(message.state, 'city');
            getCities(message.state, 'city-plus');
            getMesos(message.state);
            $("#status").val(message.active);
            $("#typeView").val(message.typeView);
            $('.featureCheckbox').each(function(){$(this).prop("checked", false);});
            if (message.adminAccess == 1) $('#adminAccess').prop('checked', true);
            if (message.directoryAreaAccess == 1) $('#directoryAreaAccess').prop('checked', true);
            if (message.exportAccess == 1) $('#exportAccess').prop('checked', true);
            if (message.financialReport == 1) $('#financialReport').prop('checked', true);
            if (message.events == 1) $('#events').prop('checked', true);
            if (message.libertasCourses == 1) $('#libertasCourses').prop('checked', true);
            $.each(json.features, function(i, item) {
                $('#feature' + item.feature).each(
                    function(){
                        $(this).prop("checked", true);            
                    }
                );
            });

            jsonCities = json.cities;
            jsonMesos = json.mesos;

            setTimeout(function () {
                $("#city").val(message.city);
            }, 1000);
            jsonCities.map(({id, nome_cidade}) => {
                addCityDiv(id, nome_cidade)
            });
            jsonMesos.map(({id, nome_meso}) => {
                addMesoDiv(id, nome_meso)
            });
        });
    });
}

$(document).ready(function () {
    if(userId) getData();
    if (userLevel == 2){
        getCities(userState, 'cityFilter');
        getCities(userState, 'city-plus');
        getCities(userState, 'city');
    }    
});