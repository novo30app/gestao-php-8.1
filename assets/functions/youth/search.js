$(document).ready(function () {
    sessionStorage.clear();
    verifySession('YouthSearch');
    if (stateUser > 0) getCities(stateUser, 'cityYouthSearchFilter');
    if (stateUser > 0) getCities(stateUser);
    $('#agroupYouthSearchFilter').change(function () {
        if ($('#agroupYouthSearchFilter').val() == 'data_refiliacao') {
            $("#statusYouthSearchFilter").val(8);
        } else if ($('#agroupYouthSearchFilter').val() == 'data_desfiliacao') {
            $("#statusYouthSearchFilter").val(9);
        } else if ($('#agroupYouthSearchFilter').val() == 'nome' || $('#agroupYouthSearchFilter').val() == 'data_filiacao') {
            $("#statusYouthSearchFilter").val(7);
        }
    });
})

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('YouthSearch');
})

function setStorageIndex() {
    setStorage('YouthSearch', {
        'url': 'api/juventude/consulta/json',
        'columns': 13,
        'variables': {
            'index': 0,
            'name': '',
            'cpf': '',
            'email': '',
            'state': '',
            'city': '',
            'nome': '',
            'affiliateId': '',
            'end': '',
            'begin': '',
            'irpf': '',
            'exemption': '',
            'state2': '',
            'city2': '',
            'gender': '',
            'phone': '',
            'status': '',
            'ordination': '',
            'mesoregion': '',
            'ageBegin': '',
            'situation': '',
            'typeDate': '',
            'agroup': '',
            'voluntaryInterest': '',
            'student': '',
            'candidateInterest': '',
            'joinedWhatsAppGroup': '',
            'municipalLeader': '',
            'orderBy': '',
            'limit': 25
        }
    })
}

function generateLines(affiliated) {
    let actions = `<a title="Novo Cometário" onclick="commentModal(${affiliated.id}, '${affiliated.name}')"><i class="fa fa-comment-medical text-muted mx-1"></i></a>`;
    actions += `<a title="Editar" onclick="searchModal(${affiliated.id}, '${affiliated.name}')"><i class="fa fa-pencil text-info mx-1"></i></a>`;
    actions += `<a title="Comentários" onclick="listComments(${affiliated.id})"><i class="fa fa-comment-dots text-primary mx-1"></i></a>`;
    return `<tr class="middle">
                    <td><a href="${baseUrl}filiados/${affiliated.id}" target="_blank">${affiliated.name}</a></td>
                    <td class="text-center">${affiliated.filiacao ?? '-'}</td>
                    <td class="text-center">${affiliated.state}</td>
                    <td class="text-center">${affiliated.city}</td>
                    <td class="text-center">${affiliated.age}</td>
                    <td class="text-center">${affiliated.status}</td>
                    <td class="text-center">
                        <a rel="noopener" href="https://api.whatsapp.com/send?phone=${affiliated.phone.replace(/[^\d]/g, '')}" target="_blank">
                             ${affiliated.phone}
                        </a>
                    </td>
                    <td class="text-center">${affiliated.municipalLeader ? affiliated.municipalLeader : '-'}</td>                  
                    <td class="text-center">${affiliated.voluntaryInterest ? affiliated.voluntaryInterest : '-'}</td>                  
                    <td class="text-center">${affiliated.candidateInterest ? affiliated.candidateInterest : '-'}</td>                    
                    <td class="text-center">${affiliated.student ? affiliated.student : '-'}</td>                    
                    <td class="text-center">${affiliated.joinedWhatsAppGroup ? affiliated.joinedWhatsAppGroup : '-'}</td>                  
                    <td class="text-center">${affiliated.usersAdmin ? affiliated.usersAdmin + '/' + affiliated.updatedAt : '-'}</td>                   
                    <td class="text-center text-nowrap">${actions}</td>              
                </tr>`;
}

function listComments(id) {
    $('#dataTable tbody').empty();
    $('#loaderTable').show();
    $('#commentListModal').modal('show');

    console.log('test')

    fetch(`${baseUrl}juventude/comentarios/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            json = json.message;

            let body = '';

            if (json.length > 0) {
                json.forEach(comment => {
                    body += `
                    <tr>
                        <td>${comment.date}</td>
                        <td>${comment.admin}</td>
                        <td>${comment.comment}</td>
                        <td>${comment.status}</td>
                    </tr>
                `;
                })
            } else {
                body = `
                    <tr>
                        <td colspan="4" class="text-center">Nenhum comentário cadastrado</td>
                    </tr>
                `
            }
            $('#loaderTable').hide();
            $('#dataTable tbody').append(body);
        });
    });
}

function searchModal(id, name) {
    document.getElementById('searchPessoaId').value = id;
    document.getElementById('searchNameModal').textContent = name;
    $('#searchModal').modal('show');
    showLoading();
    fetch(`${baseUrl}api/juventude/consulta/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            json = json.message;
            closeLoading();
            document.getElementById('voluntaryInterest').value = json.voluntaryInterest;
            document.getElementById('student').value = json.student;
            document.getElementById('candidateInterest').value = json.candidateInterest;
            document.getElementById('joinedWhatsAppGroup').value = json.joinedWhatsAppGroup;
            document.getElementById('municipalLeader').value = json.municipalLeader;
        });
    });
}

const searchRegister = document.getElementById('searchRegister');

searchRegister.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('searchRegister');
    fetch(`${baseUrl}api/juventude/consulta/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                filterTable('YouthSearch');
                showNotify('success', json.message, 1500);
                $('#searchModal').modal('hide');
                searchRegister.reset();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function commentModal(id, name) {
    document.getElementById('pessoaId').value = id;
    document.getElementById('commentName').value = name;
    $('#commentModal').modal('show');
}

const commentRegister = document.getElementById('commentRegister');

commentRegister.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('commentRegister');
    fetch(`${baseUrl}api/cadastros/salva-comentario/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                $('#commentModal').modal('hide');
                commentRegister.reset();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});