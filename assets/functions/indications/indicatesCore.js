var member = [];
var count = 0;

$(document).ready(function () {
    getMembers();
});

function setReadOnly() {
    $('#form :input').attr('readonly','readonly');
    $('#status, #state, #city, #office').attr("style", "pointer-events: none;");
    $("#add-leader, #removeButtonDiv, #buttonAlpprovalDiv").addClass('d-none');
}

function getMembers() {
    const url = window.location.href;
    let id = url.replace(baseUrl + 'indicacoes/visualiza/', '')
    id = id.replace(/\//g, '');

    //if (id !== url) {
        $('#loaderData').css('opacity', '1');
        fetch(`${baseUrl}indicacoes/listar-dados/${id}/1`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                if(json.indication.level == 1) getCitiesIndication(json.indication.state, 'city', json.indication.city);
                json.members.forEach((element) => {
                    getElement(element)
                });
                $('#loaderData').css('opacity', '0');
            });
        })
    //}
}

function changeModal(action) {
    $("#exampleModal").modal(action);
}

$("#add-leader").click(function () {
    let office = $('#office').val();
    if (office == "") {
        alert('Selecione um cargo!');
        return;
    }
    var officeString = "";
    officeString = `Líder ${office}`;
    getElement({20: office, 21: officeString});
});

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}

function getForm(form) {
    ++count;
    return `
        <div class="mb-3">
            <p><b>${count}. ${form.question}</b><br>
            ${form.answer}</p>
        </div>
    `;
}

function getElement(member) {
    const office = member[20];
    const officeString = member[21];
    let userLevel = member[22];

    let pointerEvents = ' ';
    if (userLevel != 3) {
        pointerEvents = "style='pointer-events: none;'";
    }
    let approval = ``;
    if(member[31] == 1 && member[30] == 1) {
        approval = `<button type="button" class="btn btn-success"><i class="fa fa-check" style="color: #13de3d"></i> Aprovado</button>`;
    }  
    if(userLevel == 2 && member[31] == 2 || userLevel == 3 && (member[31] == 2 || member[30] == 2)) {
        approval = `<button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Reprovado</button>`;
    }  

    if(member[12] != 5) {
        if(userLevel == 2){
            if([0, null].includes(member[31])) approval =  `<button type="button" class="btn btn-success" onclick="approval('2', ${member[0]}, ${office}, '1')"><i class="fa fa-thumbs-up"></i> Aprovar (DE)</button>
                <button type="button" class="btn btn-danger" onclick="approval('2', ${member[0]}, ${office}, '2')"><i class="fa fa-thumbs-down"></i> Reprovar (DE)</button>`;
        } 
        if(member[31] == 1 && userLevel == 3 && member[31] == 1) {
            if([0, null].includes(member[30])) approval = `<button type="button" class="btn btn-success" onclick="approval('3', ${member[0]}, ${office}, '1')"><i class="fa fa-thumbs-up"></i> Aprovar (DN)</button>
                <button type="button" class="btn btn-danger" onclick="approval('3', ${member[0]}, ${office}, '2')"><i class="fa fa-thumbs-down"></i> Reprovar  (DN)</button>`;
        }
    }
    
    let showMembersFileDiv = 'd-none';
    if(member && member[33] > 1) {
        if((!member[13] || !member[14]) && (!member[13] || !member[15] || !member[16])) showMembersFileDiv = '';
    }
    if(userLevel == 3 || member[33] == 1)  showMembersFileDiv = '';
    if(!member[0]) showMembersFileDiv = '';

    let form = '';
    if (member && member[26] && member[26].length > 0) {
        let answers = member[26].map(getForm)
        answers = answers.join('');
        form = `
            <div class="card">
                <div class="card-header bg-primary">
                    <h5 class="mb-0" data-bs-toggle="collapse" data-bs-target="#form${member[0]}"
                            aria-expanded="false" aria-controls="form${member[0]}">
                        <a class="btn btn-link text-white collapsed" >
                            Questionário 1 - ${member[28]}
                        </a>
                    </h5>
                </div>
                <div class="collapse p-20" id="form${member[0]}" aria-labelledby="form${member[0]}" data-bs-parent="#accordionoc">
                    <div class="card-body">
                        ${answers}
                    </div>
                </div>
            </div>
        `;
    }

    let linkQuestionarie = '';
    if (member && member[17] == '0' && member[12] != 5) {
        linkQuestionarie = `
            <div class="col-3">
                <div class="alert alert-danger" role="alert">Questionário pendente!</div>
            </div>`;
    }

    let resendEmail = '';
    if (member[12] != 5 && (member[23] == '' || member[17] == '0')) {
        resendEmail = `<button id="buttonResend${member[0]}" class="btn btn-orange text-white resend" 
            onclick="resendEmail(${member[0]})">Reenviar Links dos Formulários</button>`;
    }

    let termOfAcceptance = '';
    let resendEmailOfAcceptance = '';
    if (member && member[34] == false && member[12] != 5) {
        termOfAcceptance = `
            <div class="col-3">
                <div class="alert alert-secondary" role="alert">Termo de aceite pendente!</div>
            </div><br>`;

        resendEmailOfAcceptance = `<button id="buttonResendOfAcceptance${member[0]}" class="btn btn-secondary text-white resend" 
            onclick="resendEmailOfAcceptance(${member[0]})">Reenviar Termo de aceite</button>`;
    }

    let exatoDigital = '';
    if (member[29] != '') {
        exatoDigital = `<b>Background-check: ${member[29]}</b>`;
    }

    let number = member[10] ? member[10] : '';
    if(number == 0) number = 0;

    if(!userLevel) userLevel = levelUser;

    var elementRef = `<div id="divOffice-${office}">
                            <input type="hidden" id="officeId-${office}" name="officeId-${office}" value="${office}">
                            <input type="hidden" id="memberId-${office}" name="memberId-${office}" value="${member[0] ? member[0] : ''}">  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div style="display: none" id="messageFormApproval-${office}" class="alert" role="alert"></div>
                                    </div>
                                </div>
                            </div>                      
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="office-${office}">Cargo:</label>
                                        <input class="form-control" id="office-${office}" name="office-${office}" value="${officeString}" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="status-${office}">Status:</label>
                                        <select required name="status-${office}" id="status" class="form-control form-select">
                                            <option value="">Selecione</option>
                                            <option value="6" ${member && member[12] == 6 ? 'selected' : ''}>Análise DE</option>
                                            <option value="7" ${member && member[12] == 7 ? 'selected' : ''} ${userLevel != 3 ? 'disabled' : ''}>Análise DN</option>
                                            <option value="3" ${member && member[12] == 3 ? 'selected' : ''} ${userLevel != 3 ? 'disabled' : ''}>Aprovado</option>
                                            <option value="5" ${member && member[12] == 5 ? 'selected' : ''} ${userLevel != 3 ? 'disabled' : ''}>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col text-end" id="buttonAlpprovalDiv">
                                    <div class="form-group">
                                        ${approval}                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cpf-${office}">CPF:</label>
                                        <input class="form-control" id="cpf-${office}" name="cpf-${office}" value="${member[1] ? member[1] : ''}" type="number" onblur="verifyOffice('${office}', this.value);" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name-${office}">Nome completo:</label>
                                        <input class="form-control" id="name-${office}" name="name-${office}" type="text" value="${member[2] ? member[2] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email-${office}">E-mail:</label>
                                        <input class="form-control" id="email-${office}" name="email-${office}" type="email" value="${member[19] ? member[19] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="phone-${office}">Telefone:</label>
                                        <input class="form-control phone" id="phone-${office}" name="phone-${office}" value="${member[32] ? member[32] : ''}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                ${termOfAcceptance}
                                ${linkQuestionarie}
                            </div>
                            <div class="default-according style-1" id="accordionoc">
                                ${form}
                            </div>
                            <div class="row">
                                <div class="col text-end" id="buttonAlpprovalDiv">
                                    <div class="form-group">
                                        ${resendEmailOfAcceptance}                                       
                                        ${resendEmail}
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col">${exatoDigital}</div>
                            </div>
                            <div class="row" id="removeButtonDiv">
                                <div class="col text-end m-t-30">
                                    <button type="button" class="btn btn-danger text-end" onclick="removeElement('divOffice-${office}')">
                                    <i class="fa fa-times" style="color: #F36F21"></i> Remover</button>
                                </div>
                            </div>
                            <hr>
                        </div>`;
    $('#divCloned').append(elementRef);
    if(editValid) setReadOnly(member);
    $(`#phone-${office}`).mask('(99) 99999-9999');
}

function verifyOffice(office, cpf) {
    var office2 = office == 1 ? 4 : 1;
    setData(cpf, "#cpf-" + office);
}

function resendEmail(id) {
    $('.resend').attr('disabled', true);
    showNotify('success', 'E-mail sendo reenviado, aguarde.', 0);
    fetch(`${baseUrl}indicacoes/reenviar-emails/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('.notify-alert').hide(100);
            $('#buttonResend' + id).hide(100);
            $('.resend').attr('disabled', false);
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function resendEmailOfAcceptance(id) {
    $('.resend').attr('disabled', true);
    showNotify('success', 'E-mail sendo reenviado, aguarde.', 0);
    fetch(`${baseUrl}indicacoes/reenviar-emails-termo/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('.notify-alert').hide(100);
            $('#buttonResendOfAcceptance' + id).hide(100);
            $('.resend').attr('disabled', false);
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function removeElement(element) {
    $('#' + element).remove();
}

function setData(cpf, office) {
    typeOffice = office.split('-');
    fetch(`${baseUrl}indicacoes/pega-dados-dirigente/${cpf}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let a = json.message;
            $("#name-" + typeOffice[1]).val(a['name']);
            $("#email-" + typeOffice[1]).val(a['email']);
            $("#phone-" + typeOffice[1]).val(a['phone']);
           
        });
    });
}

$(document).on('click', 'button.remove', function () {
    $(this).closest('div.dep_fc').remove();
});

function getCityOption(city) {
    return `<option value="${city.id}">${city.name}</option>`;
}

function getCitiesIndication(state, divCities, city) {
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOption);
            document.getElementById('city').innerHTML = options;
            if (city) document.getElementById('city').value = city;
        });
    });
}

function getCityOption(city) {
    return `<option value="${city.id}">${city.name}</option>`;
}

function loadAddress(zipCode, office) {
    typeOffice = office.split('-');
    var cep = zipCode.replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                $("#street-" + typeOffice[1]).val(dados.logradouro);
                $("#district-" + typeOffice[1]).val(dados.bairro);
            }
        });
    }
}

function loadAddress2() {
    var cep = $('#zipCode').val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                $("#street").val(dados.logradouro);
                $("#district").val(dados.bairro);
            }
        });
    }
}

const form = document.getElementById('form');
const messageForm = document.getElementById('messageForm');

$("#form").submit(function (e) {
    document.getElementById("save").disabled = false;
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}indicacoes/adiciona-indicacao/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(form)
    }).then(response => {
        document.getElementById("save").disabled = false;
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 0)
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            } else {
                showNotify('danger', json.message, 1500)
            }
        });
    });
});

function setState() {
    $('#city').attr('readonly', false);
    $('#city').attr("style", "pointer-events: all;");
    var checkbox = $('#stateCheckbox:checked').length;
    if(checkbox === 1) {
        $('#city').attr('readonly', true); 
        $('#city').attr("style", "pointer-events: none;");
        document.getElementById('city').value = '';
    }  
}

function approval(level, member, office, value){
    var messageFormApproval = document.getElementById('messageFormApproval-' + office);
    fetch(`${baseUrl}indicacoes/aprovacao/${level}/${member}/${value}/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            messageFormApproval.textContent = json.message;
            messageFormApproval.style.display = 'block';
            messageFormApproval.classList.add("alert-danger");
            messageFormApproval.classList.remove("alert-success");
            if (response.status === 201) {
                messageFormApproval.classList.add("alert-success");
                messageFormApproval.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        });
    });
}