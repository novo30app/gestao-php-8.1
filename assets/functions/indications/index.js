var index = partial = 0;
var total = 1;
var city = status = type = "";
var order = 'createdAt';
var seq = 'desc';
var state = stateUser;

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    state = formData.state;
    city = formData.city;
    status = formData.status;
    type = formData.type;
    order = formData.order;
    seq = formData.seq;
    $("#tableIndicates tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    city = status = type = "";
    order = 'createdAt';
    seq = 'desc';
    state = stateUser;
    $("#tableIndicates tbody").empty();
    generateTable();
}

function generateTableIndicates(indicated) {
    let statusI = ``;

    if(canDelete) {
        statusI = `<div class="action-link">
                        <a href="#" onclick="changeDisabled('${indicated.id}', 0)" style="color: gray;">
                            <i class="fa fa-ban" aria-hidden="true" style="font-size: x-large;"></i>
                            <span>Encerrar</span>
                        </a>
                    </div>`;
    
        if(indicated.status == 'Encerrado' && canUpdate) {
            statusI = `<div class="action-link">
                            <a href="#" onclick="changeDisabled('${indicated.id}', 1)" style="color: orange;">
                                <i class="fa fa-check" aria-hidden="true" style="font-size: x-large;"></i>
                                <span>Reativar</span>
                            </a>
                        </div>`;
        }
    }
    
    action = `<div class="actions-container">
                <div class="action-link">
                    <a href="${baseUrl}indicacoes/visualiza/${indicated.id}/" target="_blank" style="color: green;">
                        <i class="fa fa-eye" aria-hidden="true" style="font-size: x-large;"></i>
                        <span>Visualizar</span>
                    </a>
                </div>
                ${statusI}`;

    if(canDelete) {
        action += `<div class="action-link">
                        <a href="#" onclick="changeIndication('${indicated.id}')" style="color: red;">
                            <i class="fa fa-trash" aria-hidden="true" style="font-size: x-large;"></i>
                            <span>Remover</span>
                        </a>
                    </div>`;
    }

    action += '</div>';
    
    return `<tr class="middle">
        <td class="text-center">${indicated.type}</td>
        <td class="text-center">${indicated.state}</td>
        <td class="text-center">${indicated.city}</td>
        <td class="text-center">${indicated.name}</td>
        <td class="text-center">${indicated.createdAt}</td>
        <td class="text-center">${indicated.status}</td>
        <td class="text-center">${indicated.refreshStatus}</td>
        <td class="text-begin" style="padding: 25px;">${action}</td>
    </tr>`;
}

const messageFormUpdate = document.getElementById('messageFormUpdate');

function changeDisabled(id, status) {
    let text = "Tem certeza que seguir?";
    if (confirm(text) == false) return;

    fetch(`${baseUrl}indicacoes/altera-encerramento-indicacao/${id}/${status}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Contracts');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function changeIndication(indication) {
    let text = "Tem certeza que deseja excluir?";
    if (confirm(text) == false) return;

    fetch(`${baseUrl}indicacoes/remove-indicacao/${indication}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            messageFormUpdate.textContent = json.message;
            messageFormUpdate.style.display = 'block';
            messageFormUpdate.classList.add("alert-danger");
            messageFormUpdate.classList.remove("alert-success");
            if (response.status === 201) {
                messageFormUpdate.classList.add("alert-success");
                messageFormUpdate.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        });
    });
}

function generateTable() {
    fetch(`${baseUrl}indicacoes/listagem/?index=${index}&state=${state}&city=${city}&status=${status}&type=${type}&order=${order}&seq=${seq}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableIndicates);
            $("#tableIndicates tbody").append(options);
        });
    });
}

function csv() {
    let formData = formDataToJson('filter');
    let state = formData.state;
    let city = formData.city;
    let status = formData.status;
    let type = formData.type;
    let order = formData.order;
    let seq = formData.seq;
    window.open(
        `${baseUrl}indicacoes/listagem-exportacao/?state=${state}&city=${city}&status=${status}&type=${type}&order=${order}&seq=${seq}`, 
        '_blank');
}


$(document).ready(function () {
    if (stateUser > 0) getCities(stateUser, 'city');
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});