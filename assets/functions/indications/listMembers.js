var index = partial = 0;
var total = 1;
var city = status = office = "";
var state = stateUser;

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    state = formData.state;
    city = formData.city;
    status = formData.status;
    office = formData.office;
    $("#membersTable tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    city = status = office = "";
    state = stateUser;
    $("#membersTable tbody").empty();
    generateTable();
}

function generateTableMembers(members) {
    let colorQuestionnaire2 = colorQuestionnaire1 = 'green';
    let iconQuestionnaire2 = iconQuestionnaire1 = 'check';
    if (members.questionnaire == 0) {
        colorQuestionnaire1 = 'red';
        iconQuestionnaire1 = 'times';
    }
    if (members.question1 == '' && members.question2 == '' && members.question3 == '') {
        colorQuestionnaire2 = 'red';
        iconQuestionnaire2 = 'times';
    }
    var pendences = `<p style="color:${colorQuestionnaire1}"><i class="fa fa-${iconQuestionnaire1}" aria-hidden="true" style="font-size: x-large;"></i> Questionário 1</p>
                    <p style="color:${colorQuestionnaire2}"><i class="fa fa-${iconQuestionnaire2}" aria-hidden="true" style="font-size: x-large;"></i> Questionário 2</p>`;
    let approvals = ``;
    if (members.approvalDN != 0 || members.approvalDE != 0) {
        if (members.approvalDN == 1) approvals = `<p style="color:green"><i class="fa fa-check" aria-hidden="true" style="font-size: x-large;"></i> Aprovado DN</p>`;
        if (members.approvalDN == 2) approvals = `<p style="color:red"><i class="fa fa-times" aria-hidden="true" style="font-size: x-large;"></i> Reprovado DN</p>`;
        if (members.approvalDE == 1) approvals += `<p style="color:green"><i class="fa fa-check" aria-hidden="true" style="font-size: x-large;"></i> Aprovado DE</p>`;
        if (members.approvalDE == 2) approvals += `<p style="color:red"><i class="fa fa-times" aria-hidden="true" style="font-size: x-large;"></i> Reprovado DE</p>`;
    }
    action = `<a href="${baseUrl}indicacoes/visualiza/${members.indication}/" target="_blank"><i class="fa fa-eye" aria-hidden="true" style="font-size: x-large;"></i> Visualizar</a>`;
    if (canDelete) action += ` <br><a href="#" onClick="changeMember('${members.id}')"><i class="fa fa-times" aria-hidden="true"
                                                           style="font-size: x-large;"></i> Remover</a>`;

    return `<tr class="middle">
                <td class="text-center">${members.state}</td>
                <td class="text-center">${members.city}</td>
                <td class="text-center">${members.name}</td>
                <td class="text-center">${members.office}</td>
                <td class="text-center">${members.statusString}</td>
                <td class="text-center">${pendences}</td>
                <td class="text-center">${approvals}</td>
                <td class="text-center">${action}</td>
            </tr>`;
}

const messageFormUpdate = document.getElementById('messageFormUpdate');

function changeMember(member) {
    fetch(`${baseUrl}indicacoes/remove-membro/${member}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            messageFormUpdate.textContent = json.message;
            messageFormUpdate.style.display = 'block';
            messageFormUpdate.classList.add("alert-danger");
            messageFormUpdate.classList.remove("alert-success");
            if (response.status === 201) {
                messageFormUpdate.classList.add("alert-success");
                messageFormUpdate.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        });
    });
}

function generateTable() {
    fetch(`${baseUrl}indicacoes/listagem-membros/?index=${index}&state=${state}&city=${city}&status=${status}&office=${office}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableMembers);
            $("#membersTable tbody").append(options);
        });
    });
}

function csv() {
    let formData = formDataToJson('filter');
    let state = formData.state;
    let city = formData.city;
    let status = formData.status;
    let office = formData.office;
    window.open(
        `${baseUrl}indicacoes/listagem-membros-exportacao/?state=${state}&city=${city}&status=${status}&office=${office}`,
        '_blank');
}

$(document).ready(function () {
    if (stateUser > 0) getCities(stateUser, 'city');
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});