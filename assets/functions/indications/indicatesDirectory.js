var member = [];
var count = 0;

$(document).ready(function () {
    getMembers();
});

function setReadOnly() {
    $('#form :input').attr('readonly','readonly');
    var fields = document.getElementsByClassName('attachments');
    for(var x=0;x<fields.length;x++) {
        fields[x].removeAttribute('readonly','readonly');
    }
    $('#status, #state, #city, #office').attr("style", "pointer-events: none;");
    $("#add-leader, #removeButtonDiv, #buttonAlpprovalDiv").addClass('d-none');
}

function getMembers(active = true) {
    const url = window.location.href;
    let id = url.replace(baseUrl + 'indicacoes/visualiza/', '');
    id = id.replace(/\//g, '');

    //if (id !== url) {
        $('#loaderData').css('opacity', '1');
        fetch(`${baseUrl}indicacoes/listar-dados/${id}/${active}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                if(json.indication.level == 1) getCitiesIndication(json.indication.state, 'city', json.indication.city);
                json.members.forEach((element) => {
                    if(active == true) getElement(element);
                    else getElement2(element);
                });
                $('#loaderData').css('opacity', '0');
            });
        })
    //}
}

function changeModal(action) {
    $("#exampleModal").modal(action);
}

$("#add-leader").click(function () {
    let office = $('#office').val();
    if (office == "") {
        alert('Selecione um cargo!');
        return;
    }
    var officeString = "";
    switch (office) {
        case "1":
            officeString = "Presidente";
            break;
        case "2":
            officeString = "Vice Presidente";
            break;
        case "3":
            officeString = "Secretário Administrativo";
            break;
        case "4":
            officeString = "Secretário de Finanças";
            break;
        case "5":
            officeString = "Secretário de Relações Institucionais e Legal";
            break;
    }
    getElement({20: office, 21: officeString});
});

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}

function getForm(form) {
    ++count;
    return `
        <div class="mb-3">
            <p><b>${count}. ${form.question}</b><br>
            ${form.answer}</p>
        </div>
    `;
}

function getElement(member) {
    const office = member[20];
    const officeString = member[21];
    let userLevel = member[22];

    let pointerEvents = ' ';
    if (userLevel != 3) {
        pointerEvents = "style='pointer-events: none;'";
    }
    let approval = ``;
    if(userLevel == 2 && member[30] == 1) {
        approval = `<button type="button" class="btn btn-success"><i class="fa fa-check" style="color: #13de3d"></i> Aprovado</button>`;
    }  
    if(userLevel == 2 && member[31] == 2 || userLevel == 3 && (member[31] == 2 || member[30] == 2)) {
        approval = `<button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Reprovado</button>`;
    }  

    if(member[12] != 5) {
        if(userLevel == 2){
            if([0, null].includes(member[31])) {
                approval =  `<button type="button" class="btn btn-success" onclick="approval('2', ${member[0]}, ${office}, '1')"><i class="fa fa-thumbs-up"></i> Aprovar (DE)</button>
                    <button type="button" class="btn btn-danger" onclick="approval('2', ${member[0]}, ${office}, '2')"><i class="fa fa-thumbs-down"></i> Reprovar (DE)</button>`;
            }
        } 
        if(member[31] == 1 && userLevel == 3 && member[31] == 1) {
            if([0, null].includes(member[30])) {
                approval = `<button type="button" class="btn btn-success" onclick="approval('3', ${member[0]}, ${office}, '1')"><i class="fa fa-thumbs-up"></i> Aprovar (DN)</button>
                    <button type="button" class="btn btn-danger" onclick="approval('3', ${member[0]}, ${office}, '2')"><i class="fa fa-thumbs-down"></i> Reprovar (DN)</button>`;
            }
        }
    }

    var attachments = '';
    if (member && member[13]) attachments += `<a type="button" class="btn btn-primary" style="margin: 5px;" href="${formatAttachment(member[13])}" target="_blank"><i class="fa fa-file" aria-hidden="true"></i> Comprovante de endereço</a>`;
    if (member && member[14]) attachments += `<a type="button" class="btn btn-success" style="margin: 5px;" href="${formatAttachment(member[14])}" target="_blank"><i class="fa fa-file-o" aria-hidden="true"></i> CNH</a>`;
    if (member && member[15]) attachments += `<a type="button" class="btn btn-warning" style="margin: 5px;" href="${formatAttachment(member[15])}" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true"></i> RG</a>`;
    if (member && member[16]) attachments += `<a type="button" class="btn btn-danger" style="margin: 5px;" href="${formatAttachment(member[16])}" target="_blank"><i class="fa fa-file-text" aria-hidden="true"></i> CPF</a>`;

    
    let showMembersFileDiv = 'd-none';
    if(member && member[33] > 1) {
        if((!member[13] || !member[14]) && (!member[13] || !member[15] || !member[16])) showMembersFileDiv = '';
    }
    if(userLevel == 3 || member[33] == 1)  showMembersFileDiv = '';
    if(!member[0]) showMembersFileDiv = '';

    /*
    let form2 = '';
    if (member && member[23] == '') {
        form2 = `
            <div class="col-12 mt-2">
                <div class="alert alert-danger" role="alert">Questionário 2 pendente!</div>
            </div>`;
    }
    if (member && member[23] && member[23].length > 0) {
        form2 = `
            <div class="card mt-2">
                <div class="card-header bg-primary">
                    <h5 class="mb-0">
                        <a class="btn btn-link text-white collapsed" data-bs-toggle="collapse" data-bs-target="#form2${member[0]}"
                            aria-expanded="false" aria-controls="form2${member[0]}">
                            Questionário 2
                        </a>
                    </h5>
                </div>
                <div class="collapse p-20" id="form2${member[0]}" aria-labelledby="form2${member[0]}" data-bs-parent="#accordionoc">
                    <div class="card-body">
                        <div class="mb-3">
                        <b>Nos conte sobre o contexto político local do seu município</b><br>
                            ${member[23]}
                        </div>
                        <div class="mb-3">
                        <b>Quais os principais desafios que o NOVO enfrentará em seu município?</b><br>
                            ${member[24]}
                        </div>
                        <div class="mb-3">
                        <b>Descreva como você já tem atuado para melhorar a sua comunidade e a qualidade de vida das pessoas em seu município</b><br>
                            ${member[25]}
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    */

    let form = '';
    if (member && member[26] && member[26].length > 0) {
        let answers = member[26].map(getForm)
        answers = answers.join('');
        form = ` <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="mb-0" data-bs-toggle="collapse" data-bs-target="#form${member[0]}"
                                aria-expanded="false" aria-controls="form${member[0]}">
                            <a class="btn btn-link text-white collapsed" >
                                Questionário 1 - ${member[28]}
                            </a>
                        </h5>
                    </div>
                    <div class="collapse p-20" id="form${member[0]}" aria-labelledby="form${member[0]}" data-bs-parent="#accordionoc">
                        <div class="card-body">
                            ${answers}
                        </div>
                    </div>
                </div>`;
    }

    let linkQuestionarie = '';
    if (member && member[17] == '0' && member[12] != 5) {
        linkQuestionarie = `
            <div class="col-3">
                <div class="alert alert-danger" role="alert">Questionário pendente!</div>
            </div>`;
    }

    let resendEmail = '';
    if (member[12] != 5 && (member[23] == '' || member[17] == '0')) {
        resendEmail = `<button id="buttonResend${member[0]}" class="btn btn-orange text-white resend" 
            onclick="resendEmail(${member[0]})">Reenviar Links dos Formulários</button>`;
    }

    let termOfAcceptance = '';
    let resendEmailOfAcceptance = '';

    if (member && member[34] == false && member[12] != 5) {
        termOfAcceptance = `
            <div class="col-3">
                <div class="alert alert-secondary" role="alert">Termo de aceite pendente!</div>
            </div><br>`;

        resendEmailOfAcceptance = `<button id="buttonResendOfAcceptance${member[0]}" class="btn btn-secondary text-white resend" 
            onclick="resendEmailOfAcceptance(${member[0]})">Reenviar Termo de aceite</button>`;
    }
    
    let exatoDigital = '';
    if (member[29] != '') {
        exatoDigital = `<b>Background-check: ${member[29]}</b>`;
    }

    let number = member[10] ? member[10] : '';
    if(number == 0) number = 0;

    if(!userLevel) userLevel = levelUser;

    let mandateBegin = '';
    let mandateEnd = '';
    if(userLevel == 3) {
        mandateBegin = `<div class="col-md-2">
                            <div class="form-group">
                                <label for="mandateBegin-${office}">Início Mandato:</label>
                                <input class="form-control" 
                                    id="mandateBegin-${office}" 
                                    name="mandateBegin-${office}" 
                                    type="date" 
                                    value="${member[35] ? member[35] : ''}">
                            </div>
                        </div>`;
        mandateEnd = `<div class="col-md-2">
                        <div class="form-group">
                            <label for="mandateEnd-${office}">Fim Mandato:</label>
                            <input class="form-control" 
                                id="mandateEnd-${office}" 
                                name="mandateEnd-${office}" 
                                type="date" 
                                value="${member[36] ? member[36] : ''}">
                        </div>
                    </div>`;
    }

    let statusByUser = `<input type="text" class="form-control" value="${member[37] ? member[37] : 'Análise DE'}" readonly>`;
    statusByUser += `<input type="hidden" name="status-${office}" id="status-${office}" value="${member[12] ? member[12] : '6'}">`;

    if(userLevel == 3) {
        statusByUser = `<select required name="status-${office}" id="status" class="form-control form-select">
                            <option value="">Selecione</option>
                            <option value="6" ${member && member[12] == 6 ? 'selected' : ''}>Análise DE</option>
                            <option value="7" ${member && member[12] == 7 ? 'selected' : ''}>Análise DN</option>
                            <option value="3" ${member && member[12] == 3 ? 'selected' : ''}>Aprovado</option>
                            <option value="5" ${member && member[12] == 5 ? 'selected' : ''}>Inativo</option>
                        </select>`;
    }

    var elementRef = `<div id="divOffice-${office}">
                            <input type="hidden" id="officeId-${office}" name="officeId-${office}" value="${office}">
                            <input type="hidden" id="memberId-${office}" name="memberId-${office}" value="${member[0] ? member[0] : ''}">  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div style="display: none" id="messageFormApproval-${office}" class="alert" role="alert"></div>
                                    </div>
                                </div>
                            </div>                      
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="office-${office}">Cargo:</label>
                                        <input class="form-control" id="office-${office}" name="office-${office}" value="${officeString}" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="status-${office}">Status:</label>
                                        ${statusByUser}
                                    </div>
                                </div>
                                <div class="col text-end" id="buttonAlpprovalDiv">
                                    <div class="form-group">
                                        ${approval}                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="cpf-${office}">CPF:</label>
                                        <input class="form-control" id="cpf-${office}" name="cpf-${office}" value="${member[1] ? member[1] : ''}" type="number" onblur="verifyOffice('${office}', this.value);" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name-${office}">Nome completo:</label>
                                        <input class="form-control" id="name-${office}" name="name-${office}" type="text" value="${member[2] ? member[2] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email-${office}">E-mail:</label>
                                        <input class="form-control" id="email-${office}" name="email-${office}" type="email" value="${member[19] ? member[19] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="rg-${office}">RG:</label>
                                        <input class="form-control" id="rg-${office}" name="rg-${office}" type="text" value="${member[3] ? member[3] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="phone-${office}">Telefone:</label>
                                        <input class="form-control phone" id="phone-${office}" name="phone-${office}" value="${member[32] ? member[32] : ''}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="zipCode-${office}">CEP:</label>
                                        <input class="form-control" id="zipCode-${office}" name="zipCode-${office}" data-mask="99999-999" type="text" onblur="loadAddress(this.value, 'cpf-${office}')" value="${member[7] ? member[7] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="street-${office}">Logradouro:</label>
                                        <input class="form-control" id="street-${office}" name="street-${office}" type="text" value="${member[8] ? member[8] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="district-${office}">Bairro:</label>
                                        <input class="form-control" id="district-${office}" name="district-${office}" type="text" value="${member[9] ? member[9] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="number-${office}">Número:</label>
                                        <input class="form-control" id="number-${office}" name="number-${office}" type="number" value="${number}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="complement-${office}">Complemento:</label>
                                        <input class="form-control" id="complement-${office}" name="complement-${office}" type="text" value="${member[11] ? member[11] : ''}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="voterTitle-${office}">Titulo Eleitoral:</label>
                                        <input class="form-control" id="voterTitle-${office}" name="voterTitle-${office}" value="${member[4] ? member[4] : ''}" type="number">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="voterTitleZone-${office}">Zona:</label>
                                        <input class="form-control" id="voterTitleZone-${office}" name="voterTitleZone-${office}" type="number" value="${member[5] ? member[5] : ''}">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label for="voterTitleSection-${office}">Seção:</label>
                                        <input class="form-control" id="voterTitleSection-${office}" name="voterTitleSection-${office}" type="number" value="${member[6] ? member[6] : ''}">
                                    </div>
                                </div>
                                ${mandateBegin}
                                ${mandateEnd}   
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="attachments">Anexos:</label>
                                        ${attachments}
                                    </div>
                                </div>
                            </div>
                            <div class="row ${showMembersFileDiv}" id="membersFileDiv">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="attachments">Documentos:</label>
                                        <select class="form-control form-select attachments" id="attachments-${office}" name="attachments-${office}" onchange="setAttachments(this.value, 'attachments-${office}')">
                                            <option value="">Selecione os Tipos</option>
                                            <option value="1">Comprovante de Endereço e CNH</option>
                                            <option value="2">Comprovante de Endereço, RG e CPF</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <!-- Comprovante de Residência -->
                                <div class="col-md-3 d-none" id="divProofOfResidence-${office}">
                                    <div class="form-group d-flex align-items-start gap-3">
                                        <span class="fileselector flex-grow-1">
                                            <label class="btn btn-block btn-orange text-white d-flex flex-column align-items-center w-100"
                                                    for="proofOfResidence-${office}"
                                                    id="proofOfResidenceBtn-${office}"
                                                    role="button"
                                                    aria-label="Selecionar comprovante de residência">
                                                <input class="upload-file-selector" 
                                                    id="proofOfResidence-${office}"
                                                    type="file"
                                                    name="proofOfResidence-${office}"
                                                    accept=".pdf,.jpg,.jpeg,.png"
                                                    aria-required="true"
                                                    onchange="handleFileButtons('proofOfResidence-${office}', 'proofOfResidenceBtn-${office}')">
                                                <i class="fa fa-paperclip margin-correction" aria-hidden="true"></i>
                                                <span id="proofOfResidence-${office}Label" 
                                                    class="text-truncate text-center" 
                                                    style="max-width: 180px; overflow: hidden;"></span>
                                                Anexar Comprovante
                                            </label>

                                        </span> 
                                        <button class='btn btn-danger remove' 
                                            type='button'
                                            aria-label="Remover arquivo"
                                            onclick="clean('proofOfResidence-${office}', 'proofOfResidenceBtn-${office}')">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>

                                <!-- CNH -->
                                <div class="col-md-3 d-none" id="divCNH-${office}">
                                    <div class="form-group d-flex align-items-start gap-3">
                                        <span class="fileselector flex-grow-1">
                                            <label class="btn btn-block btn-orange text-white d-flex flex-column align-items-center w-100"
                                                    for="cnh-${office}"
                                                    id="cnhBtn-${office}"
                                                    role="button"
                                                    aria-label="Selecionar CNH">
                                                <input class="upload-file-selector" 
                                                    id="cnh-${office}"
                                                    type="file"
                                                    name="cnh-${office}"
                                                    accept=".pdf,.jpg,.jpeg,.png"
                                                    aria-required="true"
                                                    onchange="handleFileButtons('cnh-${office}', 'cnhBtn-${office}')">
                                                <i class="fa fa-paperclip margin-correction" aria-hidden="true"></i>
                                                <span id="cnh-${office}Label" 
                                                    class="text-truncate text-center" 
                                                    style="max-width: 180px; overflow: hidden;"></span>
                                                Anexar CNH
                                            </label>
                                        </span> 
                                        <button class='btn btn-danger remove' 
                                            type='button'
                                            aria-label="Remover arquivo"
                                            onclick="clean('cnh-${office}', 'cnhBtn-${office}')">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>

                                <!-- RG -->
                                <div class="col-md-3 d-none" id="divRG-${office}">
                                    <div class="form-group d-flex align-items-start gap-3">
                                        <span class="fileselector flex-grow-1">
                                            <label class="btn btn-block btn-orange text-white d-flex flex-column align-items-center w-100"
                                                    for="rgFile-${office}"
                                                    id="rgFileBtn-${office}"
                                                    role="button"
                                                    aria-label="Selecionar RG">
                                                <input class="upload-file-selector" 
                                                    id="rgFile-${office}"
                                                    type="file"
                                                    name="rgFile-${office}"
                                                    accept=".pdf,.jpg,.jpeg,.png"
                                                    aria-required="true"
                                                    onchange="handleFileButtons('rgFile-${office}', 'rgFileBtn-${office}')">
                                                <i class="fa fa-paperclip margin-correction" aria-hidden="true"></i>
                                                <span id="rgFile-${office}Label"
                                                    class="text-truncate text-center" 
                                                    style="max-width: 180px; overflow: hidden;"></span>
                                                Anexar RG
                                            </label>
                                        </span> 
                                        <button class='btn btn-danger remove' 
                                            type='button'
                                            aria-label="Remover arquivo"
                                            onclick="clean('rgFile-${office}', 'rgFileBtn-${office}')">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>

                                    </div>
                                </div>

                                <!-- CPF -->
                                <div class="col-md-3 d-none" id="divCPF-${office}">
                                    <div class="form-group d-flex align-items-start gap-3">
                                        <span class="fileselector flex-grow-1">
                                            <label class="btn btn-block btn-orange text-white d-flex flex-column align-items-center w-100"
                                                    for="cpfFile-${office}"
                                                    id="cpfFileBtn-${office}"
                                                    role="button"
                                                    aria-label="Selecionar CPF">
                                                <input class="upload-file-selector" 
                                                    id="cpfFile-${office}"
                                                    type="file"
                                                    name="cpfFile-${office}"
                                                    accept=".pdf,.jpg,.jpeg,.png"
                                                    aria-required="true"
                                                    onchange="handleFileButtons('cpfFile-${office}', 'cpfFileBtn-${office}')">
                                                <i class="fa fa-paperclip margin-correction" aria-hidden="true"></i>
                                                <span id="cpfFile-${office}Label" 
                                                    class="text-truncate text-center" 
                                                    style="max-width: 180px; overflow: hidden;"></span>
                                                Anexar CPF
                                            </label>
                                        </span> 
                                        <button class='btn btn-danger remove' 
                                            type='button'
                                            aria-label="Remover arquivo"
                                            onclick="clean('cpfFile-${office}', 'cpfFileBtn-${office}')">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                ${termOfAcceptance}
                                ${linkQuestionarie}
                            </div>
                            <div class="default-according style-1" id="accordionoc">
                                ${form}
                            </div>
                            <div class="row">
                                <div class="col text-end" id="buttonAlpprovalDiv">
                                    <div class="form-group">
                                        ${resendEmailOfAcceptance}                                       
                                        ${resendEmail}
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col">${exatoDigital}</div>
                            </div>
                            <div class="row" id="removeButtonDiv">
                                <div class="col text-end m-t-30">
                                    <button type="button" class="btn btn-danger text-end" onclick="removeElement('divOffice-${office}')">
                                        <i class="fa fa-times" style="color: #F36F21"></i> Remover</button>
                                </div>
                            </div>
                            <hr>
                        </div>`;
    $('#divCloned').append(elementRef);
    if(editValid) setReadOnly(member);
    $(`#phone-${office}`).mask('(99) 99999-9999');
}

function verifyOffice(office, cpf) {
    var office2 = office == 1 ? 4 : 1;
    var value = $("#cpf-" + office2).val();
    if(office != "" && office == 1 || office == 4){
        if(cpf == value) {
            $("#cpf-" + office).val("");
            alert('Você não pode indicar a mesma pessoa para os cagos de Presidente e Secretário de Finanças!');
            return;
        } 
    }
    setData(cpf, "#cpf-" + office);
}

function resendEmail(id) {
    $('.resend').attr('disabled', true);
    showNotify('success', 'E-mail sendo reenviado, aguarde.', 0);
    fetch(`${baseUrl}indicacoes/reenviar-emails/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('.notify-alert').hide(100);
            $('#buttonResend' + id).hide(100);
            $('.resend').attr('disabled', false);
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function resendEmailOfAcceptance(id) {
    $('.resend').attr('disabled', true);
    showNotify('success', 'E-mail sendo reenviado, aguarde.', 0);
    fetch(`${baseUrl}indicacoes/reenviar-emails-termo/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('.notify-alert').hide(100);
            $('#buttonResendOfAcceptance' + id).hide(100);
            $('.resend').attr('disabled', false);
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function removeElement(element) {
    $('#' + element).remove();
}

function setData(cpf, office) {
    typeOffice = office.split('-');
    fetch(`${baseUrl}indicacoes/pega-dados-dirigente/${cpf}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let a = json.message;
            //$("#cpf-" + typeOffice[1]).val(a['cpf']);
            $("#name-" + typeOffice[1]).val(a['name']);
            $("#rg-" + typeOffice[1]).val(a['rg']);
            $("#email-" + typeOffice[1]).val(a['email']);
            $("#phone-" + typeOffice[1]).val(a['phone']);
            $("#voterTitle-" + typeOffice[1]).val(a['title']);
            $("#voterTitleZone-" + typeOffice[1]).val(a['zone']);
            $("#voterTitleSection-" + typeOffice[1]).val(a['section']);
            $("#zipCode-" + typeOffice[1]).val(a['zipCode']);
            $("#street-" + typeOffice[1]).val(a['address']);
            $("#number-" + typeOffice[1]).val(a['number']);
            $("#district-" + typeOffice[1]).val(a['district']);
            $("#complement-" + typeOffice[1]).val(a['complement']);
        });
    });
}

function setAttachments(value, office) {
    typeOffice = office.split('-');
    $('#divProofOfResidence-' + typeOffice[1]).addClass('d-none');
    $('#divCNH-' + typeOffice[1]).addClass('d-none');
    $('#divRG-' + typeOffice[1]).addClass('d-none');
    $('#divCPF-' + typeOffice[1]).addClass('d-none');
    if (value == 1) {
        $('#divProofOfResidence-' + typeOffice[1]).removeClass('d-none');
        $('#divCNH-' + typeOffice[1]).removeClass('d-none');
    } else if (value == 2) {
        $('#divProofOfResidence-' + typeOffice[1]).removeClass('d-none');
        $('#divRG-' + typeOffice[1]).removeClass('d-none');
        $('#divCPF-' + typeOffice[1]).removeClass('d-none');
    }
}

$(document).on('click', 'button.remove', function () {
    $(this).closest('div.dep_fc').remove();
});

function getCityOption(city) {
    return `<option value="${city.id}">${city.name}</option>`;
}

function getCitiesIndication(state, divCities, city) {
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOption);
            document.getElementById('city').innerHTML = options;
            if (city) document.getElementById('city').value = city;
        });
    });
}

function loadAddress(zipCode, office) {
    typeOffice = office.split('-');
    var cep = zipCode.replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                $("#street-" + typeOffice[1]).val(dados.logradouro);
                $("#district-" + typeOffice[1]).val(dados.bairro);
            }
        });
    }
}

function loadAddress2() {
    var cep = $('#zipCode').val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                $("#street").val(dados.logradouro);
                $("#district").val(dados.bairro);
            }
        });
    }
}

const form = document.getElementById('form');
const messageForm = document.getElementById('messageForm');

$("#form").submit(function (e) {
    document.getElementById("save").disabled = false;
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}indicacoes/adiciona-indicacao/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(form)
    }).then(response => {
        document.getElementById("save").disabled = false;
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 0)
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            } else {
                showNotify('danger', json.message, 1500)
            }
        });
    });
});

const formMessages = document.getElementById('formMessages');
const messageFormMessages = document.getElementById('messageFormMessages');

$("#formMessages").submit(function (e) {
    document.getElementById("saveMessages").disabled = false;
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}indicacoes/adiciona-mensagem/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formMessages)
    }).then(response => {
        document.getElementById("saveMessages").disabled = false;
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500)
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            } else {
                showNotify('danger', json.message, 1500)
            }
        });
    });
});

function setState() {
    $('#city').attr('readonly', false);
    $('#city').attr("style", "pointer-events: all;");
    var checkbox = $('#stateCheckbox:checked').length;
    if(checkbox === 1) {
        $('#city').attr('readonly', true); 
        $('#city').attr("style", "pointer-events: none;");
        document.getElementById('city').value = '';
    }  
}

function approval(level, member, office, value){
    var messageFormApproval = document.getElementById('messageFormApproval-' + office);
    fetch(`${baseUrl}indicacoes/aprovacao/${level}/${member}/${value}/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            messageFormApproval.textContent = json.message;
            messageFormApproval.style.display = 'block';
            messageFormApproval.classList.add("alert-danger");
            messageFormApproval.classList.remove("alert-success");
            if (response.status === 201) {
                messageFormApproval.classList.add("alert-success");
                messageFormApproval.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        });
    });
}

function eraseDoc(id, type){
    fetch(baseUrl + "/indicacoes/remove-documento/" + id + "/" + type, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
};

function viewPrevious() {
    getMembers(0, 'divCloned2');
}

function getElement2(member) {
    const office = member[20];
    const officeString = member[21];

    let form = '';
    if (member && member[26] && member[26].length > 0) {
        let answers = member[26].map(getForm)
        answers = answers.join('');
        form = `
            <div class="col-md-5">
                <div class="bg-primary p-2">
                    <h5 class="mb-0 d-flex" 
                        data-bs-toggle="collapse" 
                            data-bs-target="#form${member[0]}"
                            aria-expanded="false" 
                            aria-controls="form${member[0]}">
                        <a class="btn btn-link text-white collapsed w-100 text-start" >
                            Questionário 1 - ${member[28]}
                        </a>
                    </h5>
                </div>
                <div class="collapse p-20" 
                    id="form${member[0]}" 
                    aria-labelledby="form${member[0]}" 
                    data-bs-parent="#accordionoc">
                    <div class="card-body">
                        ${answers}
                    </div>
                </div>
            </div>
        `;
    }

    let linkQuestionarie = '';
    if (member && member[17] == '0' && member[12] != 5) {
        linkQuestionarie = `
            <div class="col-3">
                <div class="alert alert-danger" role="alert">Questionário pendente!</div>
            </div>`;
    }

    let termOfAcceptance = '';
    if (member && member[34] == false && member[12] != 5) {
        termOfAcceptance = `
            <div class="col-3">
                <div class="alert alert-secondary" role="alert">Termo de aceite pendente!</div>
            </div>`;
    }

    let exatoDigital = member[29] || '';
    if (exatoDigital) {
        exatoDigital = `<b>Background-check: ${member[29]}</b>`;
    }

    let proofOfResidence = cnh = rgFile = cpfFile = '';

    if (member[13]) {
        proofOfResidence = `
            <button type="button" 
                    class="btn btn-outline-secondary btn-sm"
                    onclick="window.open('${formatAttachment(member[13])}', '_blank')">
                <i class="fa fa-file me-1"></i>Comprovante
            </button>`;
    }

    if (member[14]) {
        cnh = `
            <button type="button" 
                    class="btn btn-outline-secondary btn-sm"
                    onclick="window.open('${formatAttachment(member[14])}', '_blank')">
                <i class="fa fa-file me-1"></i>CNH
            </button>`;
    }

    if (member[15]) {
        rgFile = `
            <button type="button" 
                    class="btn btn-outline-secondary btn-sm"
                    onclick="window.open('${formatAttachment(member[15])}', '_blank')">
                <i class="fa fa-file me-1"></i>RG
            </button>`;
    }

    if (member[16]) {
        cpfFile = `
            <button type="button" 
                    class="btn btn-outline-secondary btn-sm"
                    onclick="window.open('${formatAttachment(member[16])}', '_blank')">
                <i class="fa fa-file me-1"></i>CPF
            </button>`;
    }

    var elementRef = `<div id="divOffice-${office}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div style="display: none" id="messageFormApproval-${office}" class="alert" role="alert"></div>
                                </div>
                            </div>
                        </div>                      
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="office-${office}">Cargo:</label>
                                    <input class="form-control" 
                                        value="${officeString}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="status-${office}">Status:</label>
                                    <input class="form-control" 
                                        value="${member[37]}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="cpf-${office}">CPF:</label>
                                    <input class="form-control" 
                                        value="${member[1] ? member[1] : ''}" 
                                        type="number" 
                                        onblur="verifyOffice('${office}', this.value);" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name-${office}">Nome completo:</label>
                                    <input class="form-control" 
                                        type="text" 
                                        value="${member[2] ? member[2] : ''}" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="email-${office}">E-mail:</label>
                                    <input class="form-control" 
                                        type="email" 
                                        value="${member[19] ? member[19] : ''}" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="phone-${office}">Telefone:</label>
                                    <input class="form-control phone" 
                                        value="${member[32] ? member[32] : ''}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="status-${office}">Titulo Eleitoral:</label>
                                    <input class="form-control" 
                                        value="${member[4]}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="status-${office}">Zona:</label>
                                    <input class="form-control" 
                                        value="${member[5]}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="status-${office}">Secao:</label>
                                    <input class="form-control" 
                                        value="${member[6]}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="status-${office}">Início do Mandato:</label>
                                    <input class="form-control" 
                                        value="${member[35]}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="status-${office}">Fim do Mandato:</label>
                                    <input class="form-control" 
                                        value="${member[36]}" 
                                        type="text" 
                                        disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            ${termOfAcceptance}
                            ${linkQuestionarie}
                            ${form}
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="d-flex gap-2">
                                    ${proofOfResidence}
                                    ${cnh}
                                    ${rgFile}
                                    ${cpfFile}
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 mb-4">
                            <div class="col">
                                <div class="d-flex align-items-center p-3 bg-light rounded">
                                    <div class="me-3">
                                        <i class="fa fa-info-circle text-primary"></i>
                                    </div>
                                    <div class="text-dark">
                                        <strong>Background-check:</strong> 
                                        <span class="ms-2">${exatoDigital}</span>
                                    </div>
                                </div>
                            </div>
                        </div><hr>`;
    $("#divCloned2").append(elementRef);
    $("#export-data-div").removeClass('d-none');
}

function exportData() {
    let indication = $('#indicationId').val();
    window.open(
        `${baseUrl}indicacoes/exportacao-membros-desativados/?indication=${indication}`, 
        '_blank');
}

// Função para gerenciar mudança de arquivo
function handleFileButtons(inputId, btnId) {
    const input = document.getElementById(inputId);
    const btn = document.getElementById(btnId);
    const label = document.getElementById(inputId + 'Label');

    if (input.files[0]) {
        // Muda a cor e adiciona o nome do arquivo
        btn.classList.remove('btn-orange');
        btn.classList.add('btn-success');
        label.textContent = input.files[0].name;
    } else {
        // Retorna ao estado original
        btn.classList.remove('btn-success');
        btn.classList.add('btn-orange');
        label.textContent = getDefaultText(inputId);
    }
}

// Função para limpar arquivo
function clean(inputId, btnId) {
    const input = document.getElementById(inputId);
    const btn = document.getElementById(btnId);
    const label = document.getElementById(inputId + 'Label');
                
    // Limpa o input e retorna ao estado original
    input.value = '';
    btn.classList.remove('btn-success');
    btn.classList.add('btn-orange');
    label.textContent = getDefaultText(inputId);
}

// Função auxiliar para obter texto padrão
function getDefaultText(inputId) {
    if (inputId.includes('proofOfResidence')) return 'Anexar Comprovante';
    if (inputId.includes('cnh')) return 'Anexar CNH';
    if (inputId.includes('rg')) return 'Anexar RG';
    if (inputId.includes('cpf')) return 'Anexar CPF';
     return 'Anexar arquivo';
}

function analysis(id, value) {
    if (value == 2) {
        if (document.getElementById('rejectReason').value == '') {
            alert('Digite o motivo da rejeição');
            return;
        }
    }
    let rejectReason = document.getElementById('rejectReason').value;
    if (value == 1) {
        rejectReason = 'Aprovado';
    }
    fetch(`${baseUrl}indicacoes/analise-ata-indicacao-novos-membros/${id}/${value}/${rejectReason}`, {
        method: 'PUT',
        credentials: 'same-origin',
    }).then(response => {
        response.json().then(json => {
            $('.notify-alert').hide(100);
            $('#buttonResend' + id).hide(100);
            $('.resend').attr('disabled', false);
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}