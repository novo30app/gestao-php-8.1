$(document).ready(function() {
    $('#addressOutside').change(function() {
        if ($('#addressOutside').val() == 0) {
            $("#addressOutsideDiv").hide();
            $("#addressCountryDiv").hide();
            $("#addressInsideDiv").show();
        } else {
            $("#addressOutsideDiv").show();
            $("#addressCountryDiv").show();
            $("#addressInsideDiv").hide();
        };
    });

    $("#zipCode").blur(function() {
        loadAddress();
    });
});

const registerForm = document.getElementById('personalData');
registerForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('personalData')) {
        closeLoading();
        return;
    }
    let formData = formDataToJson('personalData');
    fetch(baseUrl + 'api/cadastros/novo-registro', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
               showNotify('success', json.message, 0);
                setTimeout(function() {
                    location.reload();
                }, 3000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function loadAddress() {
    var cep = $('#zipCode').val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#street").val(dados.logradouro);
                $("#district").val(dados.bairro);
                $(`#cityId option[value='${dados.localidade}']`).attr('selected', 'selected');
                if (dados.uf != $('#ufId').val()) {
                    $(`#ufId option[value=${dados.uf}]`).attr('selected', 'selected');
                    $('#ufId').trigger('change');
                }
            }
        });
    }
}

function getCityOptionName(city) {
    return `<option value="${city.name}">${city.name}</option>`;
}

function getCities2(state, divCities) {
    fetch(baseUrl + `api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOptionName);
            document.getElementById(divCities).innerHTML = options;
            loadAddress();
        });
    });
}