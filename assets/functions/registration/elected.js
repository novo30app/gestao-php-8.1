$(document).ready(function (){
    verifySession('Elected');
})

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Elected');
})

function setStorageIndex() {
    setStorage('Elected', {
        'url': 'api/cadastros/eleitos',
        'columns': 6,
        'variables': {
            'index': 0,
            'name': '',
            'email': '',
            'cpf': '',
            'post': '',
            'year': '',
            'city': '',
            'state': '',
            'limit': 25
        }
    })
}

function generateLines(elected) {
    let actions = "";
    if(userAdmin) {
        actions += `<a onclick="edit(
                        '${elected.id}', 
                        '${elected.name}', 
                        '${elected.email}', 
                        '${elected.cpf}', 
                        '${elected.postId}', 
                        '${elected.year}', 
                        '${elected.stateId}', 
                        '${elected.cityId}',
                        '${elected.votes}')">
                        <i class="fa fa-pencil" aria-hidden="true" title="Editar" 
                        style="font-size: 25px; margin-left: 15px;"></i>
                    </a>
                    <a onclick="remove('${elected.id}')">
                        <i class="fa fa-trash" aria-hidden="true" title="Remover" 
                        style="font-size: 25px; margin-left: 15px; color: red"></i>
                    </a>`;
    }

    return `<tr class="middle">
                    <td>${elected.id}</td>
                    <td>${elected.name}</td>
                    <td class="text-center">${elected.email}</td>
                    <td class="text-center">${elected.cpf}</td>
                    <td class="text-center">${elected.post}</td>
                    <td class="text-center">${elected.year}</td>
                    <td class="text-center">${elected.state}</td>
                    <td class="text-center">${elected.city}</td>
                    <td class="text-center">${elected.votes}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            ${actions}
                        </div>
                    </td>
                </tr>`;
}

function showLocations() {
    getCities(document.getElementById('state').value)
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    const x = document.getElementById('level').value;
    if (x == '2') {
        document.getElementById('stateDiv').style.display = 'block';
    } else if (x == '1') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityDiv').style.display = 'block';
    }
}

function edit(id, name, email, cpf, post, year, state, city, votes) {
    document.getElementById('formModal').reset();
    document.getElementById('electedId').value = "";
    
    if (id > 0) {
        document.getElementById('electedId').value = id;
        document.getElementById('nameModal').value = name;
        document.getElementById('emailModal').value = email;
        document.getElementById('cpfModal').value = cpf;
        document.getElementById('postModal').value = post;
        document.getElementById('yearModal').value = year;

        getCities(state, 'cityModal');
        document.getElementById('stateModal').value = state;
        setTimeout(() => {
            document.getElementById('cityModal').value = city;
        }, 250)

        document.getElementById('votesModal').value = votes;
    }

    $('#modal').modal('show');
}

function csv() {
    let formData = formDataToJson('filter');
    name = formData.name;
    email = formData.email;
    cpf = formData.cpf;
    post = formData.post;
    year = formData.city;
    city = formData.city;
    state = formData.state;
    window.open(
        `${baseUrl}api/cadastros/exporta-eleitos/?name=${name}&email=${email}&cpf=${cpf}&post=${post}&year=${year}&city=${city}&state=${state}`,
        '_blank');
}

const formModal = document.getElementById('formModal');
const divMessageForm = document.getElementById('messageForm');
formModal.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('formModal');
    fetch(`${baseUrl}api/cadastros/salva-eleitos`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(() => {
                    $('#modal').modal('hide');
                    window.location.reload();
                }, 250)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function remove(id){
    let text = "Deseja realmente remover este eleito?";
    if (confirm(text) == false) return;
    fetch(`${baseUrl}api/cadastros/remove-eleitos/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                window.location.reload();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function importElected() {
    $('#modalImport').modal('show');
}

const divMessageImport = document.getElementById('messageImport');
const formImport = document.getElementById('formImport');
formImport.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}api/cadastros/importa-eleitos/`, {
        method: 'POST',
        credentials: "same-origin",
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(formImport),
    }).then(response => {
        closeLoading();
        divMessageImport.classList.add("alert-danger");
        divMessageImport.classList.remove("alert-success");
        response.json().then(json => {
            divMessageImport.innerHTML = json.message;
            divMessageImport.style.display = 'block';
            if (response.status === 201) {
                divMessageImport.classList.add("alert-success");
                divMessageImport.classList.remove("alert-danger");
                setTimeout(function() {
                    window.location.reload();
                }, 2000);
            }
        });
    });
});

function resetTable() {
    filter.reset();
    setStorageIndex();
}