var arr = [];
//Configs mapa
var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 16,
        minZoom: 1
    }),

    latlng = L.latLng(-14.2350, -51.9253);

var map = L.map('map', {center: latlng, zoom: 4, layers: [tiles]});

// Setar nova latitude e longitude
function moveToLocation(lat, lng, zoom) {
    var newLatlng = L.latLng(lat, lng);
    map.setView(newLatlng, zoom, {
        animate: true
    });
}

var markers = L.markerClusterGroup({
    spiderfyOnMaxZoom: false,
    showCoverageOnHover: false,
    zoomToBoundsOnClick: false,
    iconCreateFunction: function (cluster) {
        var childMarkers = cluster.getAllChildMarkers();
        var sum = 0;

        childMarkers.forEach(function (marker) {
            var value = parseInt(marker.options.icon.options.html.match(/\d+/)[0]);
            sum += value;
        });

        return L.divIcon({
            html: `<div class="single-marker-cluster"><div>${sum}</div></div>`,
            className: 'marker-cluster-custom',
            iconSize: [30, 30],
            iconAnchor: [20, 20]
        });
    }
});

function getRandomLatLng(arr) {
    return L.latLng(arr.lat, arr.long);
}

// Ícones no mapa
function populate(arr) {
    for (var i = 0; i < arr.length; i++) {

        var icon;

        icon = L.divIcon({
            html: `<div class="single-marker-cluster"><div>${arr[i].total}</div></div>`,
            className: 'marker-cluster-custom',
            iconSize: [30, 30],
            iconAnchor: [20, 20]
        });

        var m = L.marker(getRandomLatLng(arr[i]), {icon: icon});
        m.zone = arr[i].total;
        m.on('click', onMarkerOrClusterClick);
        markers.addLayer(m);

    }
    return false;
}

function onMarkerOrClusterClick(a) {
    let values, isCluster;

    // Verifica se é um cluster
    if (a.layer) {
        values = a.layer.getAllChildMarkers();
        isCluster = true;
    } else {
        // Se não for um cluster, cria um array com o próprio marcador
        values = [a.target]; // 'a.target' refere-se ao marcador em que foi clicado
        isCluster = false;
    }
    let name = $('.btn.btn-outline-primary.active').text();
    name = name.trim();

    let candidatesVotes = [];
    let verify = [];
    values.forEach(marker => {
        if (verify.indexOf(marker._latlng.lat) < 0) {
            verify.push(marker._latlng.lat);
            const orderBy = Object.groupBy(arr, ({lat}) => lat);
            orderBy[marker._latlng.lat].forEach(votes => {

                if (candidatesVotes[votes.candidate]) {
                    candidatesVotes[votes.candidate].total += parseInt(votes.total)
                } else {
                    candidatesVotes[votes.candidate] = {
                        candidate: votes.candidate,
                        total: parseInt(votes.total)
                    }
                }
            })
        }
    })


    // candidatesVotes.sort((a, b) => b.total - a.total);

    let message = "<ul>";
    const keys = Object.keys(candidatesVotes);

    keys.forEach(element => {

        if (name === candidatesVotes[element].candidate) {
            message += `<li><b>${candidatesVotes[element].candidate} - ${candidatesVotes[element].total}</b></li>`;
        } else {
            message += `<li>${candidatesVotes[element].candidate} - ${candidatesVotes[element].total}</li>`;
        }
    })

    message += "</ul>";

    if (isCluster) {
        a.layer.bindPopup(message).openPopup()
    } else {
        this.bindPopup(message).openPopup();
    }
}

markers.on('clusterclick', onMarkerOrClusterClick);

map.addLayer(markers);

// Agrupar por nome do candidato
function groupBy(index) {
    markers.clearLayers();
    if (index !== '') {
        const orderBy = Object.groupBy(arr, ({candidate}) => candidate);
        populate(orderBy[index])
    }
}

// Busca dos dados por cargo
function getData() {
    markers.clearLayers();
    const position = document.getElementById('office').value;
    const city = document.getElementById('cityCandidate').value;
    if (position === '') {
        showNotify('error', 'Selecione um cargo!', 1500);
        return;
    }

    if (city === '') {
        return;
    }
    showLoading();
    $('#office, #candidate').attr('disabled', true);
    const formData = {
        year: 2020,
        city,
        turn: 1,
        position: position.toUpperCase().replace('_', ' ')
    };
    fetch(baseUrlMap + 'api/sistema/listar-resultados', {
        method: "POST",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })
        .then(response => {
            response.json().then(json => {
                arr = json.message;
                const orderBy = Object.groupBy(json.message, ({candidate}) => candidate);
                const keys = Object.keys(orderBy);

                let options = `<option value="">Selecione um Candidato</option>`;

                keys.forEach((element, index) => {
                    if (element !== 'TOTAL') {
                        options += `
                        <option value="${element}">${element}</option>
                    `;
                    }
                })

                $('#office, #candidate').attr('disabled', false);
                $('#candidate').html(options);

                const data = json.data;
                moveToLocation(data.lat, data.long, 9);

                groupBy('TOTAL');
            })
        })
        .finally(() => {
            closeLoading();
        })
}

// Busca das cidades vinculadas ao estado
function getCitiesByState(state) {
    $('#cityCandidate').attr('disabled', true)
    fetch(baseUrlMap + 'api/sistema/cidades-por-estado-votacao/' + state, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(response => {
            response.json().then(json => {

                console.log(json)

                let options = `<option value="">Selecione uma Cidade</option>`;

                json.message.forEach((element, index) => {
                        options += `
                        <option value="${element.city}">${element.city}</option>
                    `;
                })

                $('#cityCandidate').attr('disabled', false).html(options);
                closeLoading();
            })
        })
}
