function voterTitleFields(type) {
    $('#titleInternationalVoterTitleCountry').hide();
    $('#titleInternationalVoterTitleCityString').hide();
    $('#titleNationalVoterTitleUF').show();
    $('#titleNationalVoterTitleCity').show();
    if (type == 1) {
        $('#titleInternationalVoterTitleCountry').show();
        $('#titleInternationalVoterTitleCityString').show();
        $('#titleNationalVoterTitleUF').hide();
        $('#titleNationalVoterTitleCity').hide();
    }
}

function addressFields(type) {
    $('#addressInsideDiv').removeClass('d-none');
    $('#addressOutsideDiv').removeClass('d-none');
    $('#addressCountryDiv').removeClass('d-none');
    if (type == false) {
        $('#addressOutsideDiv').addClass('d-none');
        $('#addressCountryDiv').addClass('d-none');
    } else {
        $('#addressInsideDiv').addClass('d-none');
    }
}

var diaCode = -1;

var cellPhone = window.intlTelInput(document.querySelector('#cellPhone'), {
    "initialCountry": 'br'
});

function maskCellPhone() {
    if (diaCode == cellPhone.getSelectedCountryData().dialCode) {
        return;
    }
    diaCode = cellPhone.getSelectedCountryData().dialCode;
    $('#cellPhone').unmask();
    $('#cellPhone').mask('999999999999');
    if (diaCode == 55) {
        $('#cellPhone').mask('(99)99999-99999');
    }
}

$(document).ready(function() {
    maskCellPhone();
    $("#zipCode").keyup(function() {
        loadAddress();
    });
    $("#cpm").val(false);
    if(cpm) {
        $("#cpm").val(true);
        $("#exemption").val('1');
        $('#exemption').attr("style", "pointer-events: none;");
    }
});

function loadAddress() {
    var cep = $('#zipCode').val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    let errors = '';
    const formData = formDataToJson('msform');
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#street").val(dados.logradouro);
                $("#district").val(dados.bairro);
                $(`#cityId option[value='${dados.localidade}']`).attr('selected', 'selected');
                $("#uf").val(dados.uf);
                $("#city").val(dados.localidade);
                $(`#addressCountry option[value='33']`).attr('selected', 'selected');
                if (dados.uf != $('#ufId').val()) {
                    $(`#ufId option[value=${dados.uf}]`).attr('selected', 'selected');
                    $('#ufId').trigger('change');
                }
                divMessageForm.innerHTML = errors;
                divMessageForm.classList.add("d-none");
            } else {
                errors = "Atenção, CEP inválido para emissão de boletos!";
                divMessageForm.innerHTML = errors;
                divMessageForm.classList.add("alert-warning");
                divMessageForm.classList.remove("alert-success");
                divMessageForm.classList.remove("d-none");
            }
        });
    }
}

function getCityOptionName(city) {
    return `<option value="${city.name}">${city.name}</option>`;
}

function getCitiesByState(state, divCities) {
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            if (divCities == 'cityId') { //address em string
                options = '<option value="">Selecione a cidade</option>' + cities.map(
                    getCityOptionName);
                loadAddress();
            } else { //voter title int id
                options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOptionId);
            }
            document.getElementById(divCities).innerHTML = options;
        });
    });
}

function getCityOptionId(city) {
    return `<option value="${city.id}">${city.name}</option>`;
}

function validarCPF(cpf) {	
	cpf = cpf.replace(/[^\d]+/g,'');	
	if(cpf == '') return '<b>CPF</b> é obrigatório<br>';	
	// Elimina CPFs invalidos conhecidos	
	if (cpf.length != 11 || 
		cpf == "00000000000" || 
		cpf == "11111111111" || 
		cpf == "22222222222" || 
		cpf == "33333333333" || 
		cpf == "44444444444" || 
		cpf == "55555555555" || 
		cpf == "66666666666" || 
		cpf == "77777777777" || 
		cpf == "88888888888" || 
		cpf == "99999999999")
		return '<b>CPF</b> é inválido!<br>';		
	// Valida 1o digito	
	add = 0;	
	for (i=0; i < 9; i ++)		
		add += parseInt(cpf.charAt(i)) * (10 - i);	
		rev = 11 - (add % 11);	
		if (rev == 10 || rev == 11) rev = 0;	
		if (rev != parseInt(cpf.charAt(9)))	return '<b>CPF</b> é inválido!<br>';		
	// Valida 2o digito	
	add = 0;	
	for (i = 0; i < 10; i ++)		
		add += parseInt(cpf.charAt(i)) * (11 - i);	
	rev = 11 - (add % 11);	
	if (rev == 10 || rev == 11)	
		rev = 0;	
	if (rev != parseInt(cpf.charAt(10))) return '<b>CPF</b> é inválido!<br>';		
	return "";   
}

const divMessageForm = document.getElementById('divMessageFormRegister');

function IsEmail(email){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(email == '' || !er.test(email) ) { 
        return false; 
    } else {
        return true;
    }
}

function validateForm(step) {
    let errors = '';
    const formData = formDataToJson('msform');
    switch (step) {
        case 1:
            const dateMax = moment(new Date()).subtract(16, 'years');
            const dateMin = moment(new Date()).subtract(120, 'years');
            let validadeCpf = validarCPF(formData.cpf);
            if(validadeCpf != "") errors += validadeCpf;
            if (formData.name.trim() == '') errors += '<b>Nome Completo</b> é obrigatório<br>';
            if (!moment(formData.birth, 'DD/MM/YYYY', true).isValid()) errors +=
                '<b>Data de Nascimento</b> valor inválido<br>';
            if (IsEmail(formData.email) == false) errors += '<b>E-mail</b> é obrigatório<br>';
            if (formData.cellPhone.replace(/\D+/g, '').length < 8) errors += '<b>Whatsapp</b> inválido<br>';
            if (moment(formData.birth, 'DD/MM/YYYY', true).isValid() &&
                moment(formData.birth, 'DD/MM/YYYY').format('YYYY/MM/DD') > dateMax.format('YYYY/MM/DD')) {
                errors += '<b>Minimo 16 anos</b><br>';
            }
            if (moment(formData.birth, 'DD/MM/YYYY', true).isValid() &&
                moment(formData.birth, 'DD/MM/YYYY').format('YYYY/MM/DD') < dateMin.format('YYYY/MM/DD')) {
                errors += '<b>Máximo 120 anos</b><br>';
            }
            if (formData.sex.trim() == 0) errors += '<b>Sexo</b> é obrigatório<br>';
            if (!$('input[name="membershipForm"]').val()) errors += '<b>Ficha de Filiação</b> é obrigatório<br>';
            break;
        case 2:
            if (formData.zipCode.trim() == '') errors += '<b>CEP</b> é obrigatório<br>';
            if (formData.street.trim() == '') errors += '<b>Logradouro</b> é obrigatório<br>';
            if (formData.number.trim() == '') errors += '<b>Número</b> é obrigatório<br>';
            if (formData.district.trim() == '') errors += '<b>Bairro</b> é obrigatório<br>';
            if (formData.addressOutside == 1) {
                if (formData.addressCountry.trim() == '') errors += '<b>País</b> é obrigatório<br>';
                if (formData.city.trim() == '') errors += '<b>Cidade</b> é obrigatório<br>';
            } else {
                if (formData.ufId.trim() == '') errors += '<b>Estado</b> é obrigatório<br>';
                if (formData.cityId.trim() == '') errors += '<b>Cidade</b> é obrigatório<br>';
            }
            break;
        case 3:
            if (formData.mother.trim() == '') errors += '<b>Nome da Mãe</b> é obrigatório<br>';
            if (formData.voterTitleNumber.trim() == '') errors += '<b>Título de eleitor</b> é obrigatório<br>';
            if (formData.voterTitleZone.trim() == '') errors += '<b>Zona</b> é obrigatório<br>';
            if (formData.voterTitleSection.trim() == '') errors += '<b>Seção</b> é obrigatório<br>';
            if (formData.originVoterTitle == 2) { //brasil
                if (formData.voterTitleUF.trim() == '') errors += '<b>UF eleitoral</b> é obrigatório<br>';
                if (formData.voterTitleCity.trim() == '') errors += '<b>Município Eleitoral</b> é obrigatório<br>';
            } else {
                if (formData.voterTitleCountry.trim() == '') errors += '<b>País eleitoral</b> é obrigatório<br>';
                if (formData.voterTitleCityString.trim() == '') errors +=
                    '<b>Município Eleitoral</b> é obrigatório<br>';
            }
            if (!validateVoterTitle(formData.voterTitleNumber)) errors += '<b>Título de eleitor</b> inválido<br>';
            break;
        case 4:
            if (!$("input[type='radio'][name='term1']").is(':checked')) {
                errors += '<b>Termo 1</b> é obrigatório<br>';
            }
            if (formData.term2 != 1) errors += '<b>Termo 2</b> é obrigatório<br>';
            if (formData.term3 != 1) errors += '<b>Termo 3</b> é obrigatório<br>';
            if (formData.term4 != 1) errors += '<b>Termo 4</b> é obrigatório<br>';
            break;
    }
    if (errors != '') {
        divMessageForm.innerHTML = errors;
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        divMessageForm.classList.remove("d-none");
        return false;
    }
    return true;
}

function validateVoterTitle(inscricao) {
    var inscricao = ("000000000000" + inscricao).slice(-12);
    var paddedInsc = inscricao.replace(/\D+/g, '');
    var dig1 = 0;
    var dig2 = 0;
    var tam = paddedInsc.length;
    var digitos = paddedInsc.substr(tam - 2, 2);
    var estado = paddedInsc.substr(tam - 4, 2);
    var titulo = paddedInsc.substr(0, tam - 2);
    var exce = (estado == '01') || (estado == '02');
    dig1 = (titulo.charCodeAt(0) - 48) * 9 + (titulo.charCodeAt(1) - 48) * 8 +
        (titulo.charCodeAt(2) - 48) * 7 + (titulo.charCodeAt(3) - 48) * 6 +
        (titulo.charCodeAt(4) - 48) * 5 + (titulo.charCodeAt(5) - 48) * 4 +
        (titulo.charCodeAt(6) - 48) * 3 + (titulo.charCodeAt(7) - 48) * 2;
    var resto = (dig1 % 11);
    if (resto == 0) {
        if (exce) {
            dig1 = 1;
        } else {
            dig1 = 0;
        }
    } else {
        if (resto == 1) {
            dig1 = 0;
        } else {
            dig1 = 11 - resto;
        }
    }
    dig2 = (titulo.charCodeAt(8) - 48) * 4 + (titulo.charCodeAt(9) - 48) * 3 + dig1 * 2;
    resto = (dig2 % 11);
    if (resto == 0) {
        if (exce) {
            dig2 = 1;
        } else {
            dig2 = 0;
        }
    } else {
        if (resto == 1) {
            dig2 = 0;
        } else {
            dig2 = 11 - resto;
        }
    }
    if ((digitos.charCodeAt(0) - 48 == dig1) && (digitos.charCodeAt(1) - 48 == dig2)) {
        return true; // Titulo valido
    } else {
        return false;
    }
}

function changeStep(current, option) {
    if (option == 'next') {
        if (!validateForm(current)) {
            return;
        }
    }
    divMessageForm.classList.add("d-none");
    document.getElementById(`step-${current}`).style.display = 'none';
    const progressbar = document.querySelectorAll('#progressbar li');
    if (option == 'next') {
        if (!validateForm(current)) {
            return;
        }
        document.getElementById(`step-${current + 1}`).style.display = 'block';
        progressbar[current].classList.add('active');
    } else {
        document.getElementById(`step-${current - 1}`).style.display = 'block';
        progressbar[current - 1].classList.remove('active')
    }
}

function setData(cpf) {
    fetch(`${baseUrl}filiacao/consulta/${cpf}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let a = json.message;
            if(a['name']){
                $("#name").val(a['name']);
                $("#birth").val(a['birth']);
                $("#email").val(a['email']);
                $("#cellPhone").val(a['phone']);
                $("#sex").val(a['gender']);
                $("#zipCode").val(a['zipCode']);
                $(`#ufId option[value=${a['state']}]`).attr('selected', 'selected');
                $('#ufId').trigger('change');
                setTimeout(function() {
                    $("#cityId").val(a['city']);
                }, 1000);
                $("#street").val(a['address']);
                $("#number").val(a['number']);
                $("#district").val(a['district']);
                $("#complement").val(a['complement']);
                $("#mother").val(a['motherName']);
                $("#voterTitleNumber").val(a['title']);
                $("#voterTitleZone").val(a['zone']);
                $("#voterTitleSection").val(a['section']);
                $(`#addressCountry option[value='33']`).attr('selected', 'selected');
                $(`#voterTitleUF option[value=${a['voterTitleUf']}]`).attr('selected', 'selected');
                $('#voterTitleUF').trigger('change');
                setTimeout(function() {
                    $("#voterTitleCity").val(a['voterTitleCity']);
                }, 1000);
            }
        });
    });
};

const divMessageFormData = document.getElementById('divMessageFormData');
$("#msform").submit(function (e) {
    e.preventDefault();
    showLoading();
    $.ajax({
        type: 'POST',
        url: `${baseUrl}filiacao/solicita/`,
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            divMessageForm.classList.add("alert-success");
            divMessageForm.classList.remove("alert-danger");
            divMessageForm.textContent = json.message;
            divMessageForm.classList.remove("d-none");
            $('#irpfDiv').addClass('d-none');
            $('#finalDiv').removeClass('d-none');
        })
        .fail(function (json) {
            closeLoading();
            divMessageForm.classList.add("alert-danger");
            divMessageForm.classList.remove("alert-success");
            divMessageForm.textContent = json.responseJSON.message;
            divMessageForm.classList.remove("d-none");
        });
});