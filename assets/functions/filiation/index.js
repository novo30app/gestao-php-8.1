var index = partial = 0;
var total = 1;
var name = state = city = status = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    state = formData.state;
    city = formData.city;
    status = formData.status;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = state = city = status = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableIndicates(indicated) {

    var actions = '';
    if (canUpdate) actions += `<a href="${baseUrl}filiados/editar/${indicated.user}" target="_blank"><i class="fa fa-pencil" aria-hidden="true" style="font-size: x-large;"></i> Editar</a><br>`;
    if (canDelete) actions += `<a href="#" onclick="remove('${indicated.id}')"><i class="fa fa-times" aria-hidden="true" style="font-size: x-large;"></i> Excluir</a><br>`;

    if (indicated.status == 2 && canUpdate) {
        actions += `<a href="#" onclick="changeModal('show', '${indicated.id}')"><i class="fa fa-refresh" aria-hidden="true" style="font-size: x-large;"></i> Reenviar</a>`;
    }
    var membershipForm = indicated.membershipForm;
    var membershipForm = membershipForm.split("uploads/");
    var membershipForm = baseUrl + 'uploads/' + membershipForm[1];
    return `<tr class="middle">
                <td><a href='${baseUrl}/filiados/${indicated.user}' target='_blank'>${indicated.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${indicated.createdAt}</td>
                <td class="text-center">${indicated.state}</td>
                <td class="text-center">${indicated.city}</td>
                <td class="text-center">${indicated.userAdmin}</td>
                <td class="text-center"><a href="${membershipForm}" target="_blank"><i class="fa fa-download" aria-hidden="true" style="font-size: x-large;"></i> Baixar</a></td>
                <td class="text-center">${indicated.statusString}</td>
                <td class="text-center">${actions}</td>
            </tr>`;
}

function changeModal(action, id = null) {
    if (id) $("#modalId").val(id);
    $("#exampleModal").modal(action);
}

function remove(id) {
    fetch(`${baseUrl}filiacao/remove-indicacao/${id}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            messageFormUpdate.textContent = json.message;
            messageFormUpdate.style.display = 'block';
            messageFormUpdate.classList.add("alert-danger");
            messageFormUpdate.classList.remove("alert-success");
            if (response.status === 201) {
                messageFormUpdate.classList.add("alert-success");
                messageFormUpdate.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        });
    });
}

function generateTable() {
    fetch(`${baseUrl}filiacao/listagem/?index=${index}&name=${name}&state=${state}&city=${city}&status=${status}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableIndicates);
            $("#table tbody").append(options);
        });
    });
}

$(document).ready(function () {
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});

const formModal = document.getElementById('formModal');
const divMessageFormData = document.getElementById('divMessageFormData');

$("#formModal").submit(function (e) {
    document.getElementById("save").disabled = false;
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}filiacao/reenvia/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formModal)
    }).then(response => {
        document.getElementById("save").disabled = false;
        response.json().then(json => {
            closeLoading();
            divMessageFormData.textContent = json.message;
            divMessageFormData.style.display = 'block';
            divMessageFormData.classList.add("alert-danger");
            divMessageFormData.classList.remove("alert-success");
            if (response.status === 201) {
                divMessageFormData.classList.add("alert-success");
                divMessageFormData.classList.remove("alert-danger");
                //setTimeout(function() {
                //    window.location.reload();
                //}, 1200);
            }
        });
    });
});