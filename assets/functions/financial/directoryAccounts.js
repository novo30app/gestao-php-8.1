function add(id, directory, nickname, bank, agency, account, accountingAccount, date, valueStart) {
    document.getElementById('formModal').reset();
    document.getElementById('accountId').value = "";
    if (id > 0) {
        console.log(date)
        console.log(valueStart)
        document.getElementById('accountId').value = id;
        document.getElementById('directoryModal').value = directory;
        document.getElementById('nicknameModal').value = nickname;
        document.getElementById('bankModal').value = bank;
        document.getElementById('agencyModal').value = agency;
        //document.getElementById('agencyDigitModal').value = agencyDigit;
        document.getElementById('accountModal').value = account;
        //document.getElementById('accountDigitModal').value = accountDigit;
        document.getElementById('accountingAccount').value = accountingAccount;
        if (date !== '00/00/0000') document.getElementById('dateStart').value = date;
        if (valueStart !== 0) document.getElementById('valueStart').value = formatMoney(valueStart);
    }
    $('#modal').modal('show');
}

var index = 0;
var partial = 0;
var total = 1;
var directory = nickname = extract = state = city = begin = end = '';

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    directory = formData.directory;
    nickname = formData.nickname;
    extract = formData.extract;
    state = formData.state;
    city = formData.city;
    begin = formData.begin;
    end = formData.end;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    directory = nickname = extract = state = city = begin = end = '';
    $("#table tbody").empty();
    generateTable();
}

function generateList(accounts) {
    let paramsBegin = paramsEnd = "";
    if(begin != "") paramsBegin = `?begin=${begin}`;
    if(end != "") paramsEnd = `&end=${end}`;
    let url = `${baseUrl}financeiro/extrato-diretorios/${accounts.id}/${paramsBegin}${paramsEnd}`;
    if(type == 5) url = `${baseUrl}financeiro/extrato-diretorios/diretiva/${accounts.id}/${paramsBegin}${paramsEnd}`;
    var actions = `<a href="${url}" target="_blank">
        <i class="fa fa-file-text" aria-hidden="true" title="Extrato" style="font-size: 30px;"></i>
    </a>`;
    if(canUpdate) {
        actions += `<a onclick="add(
           	'${accounts.id}', 
            '${accounts.directory}', 
            '${accounts.nickname}', 
            '${accounts.bank}', 
            '${accounts.agency2}', 
            '${accounts.account2}', 
            '${accounts.accountingAccount}',
            '${accounts.dateStart}',
            '${accounts.valueStart}')">
            <i class="fa fa-pencil" aria-hidden="true" title="Editar" 
                style="font-size: 25px; margin-left: 15px;"></i></a>`;
    }

    return `<tr class="middle">
				<td>${accounts.directory}</td>
				<td class="text-center">${accounts.cnpj}</td>
				<td class="text-center">${accounts.nicknameString}</td>
				<td class="text-center">${accounts.bank}</td>
				<td class="text-center">${accounts.agency2}</td>
				<td class="text-center">${accounts.account2}</td>
				<td class="text-center">
					<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                        ${actions}
					</div>
				</td>
			</tr>`;
}

function generateTable() {
    if(begin == "" && end != "") {
        showNotify('danger', "Selecione uma data de inicio", 1500);
        return;
    }
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/financeiro/contas-diretorios/?index=${index}&userType=${type}&directory=${directory}&nickname=${nickname}&extract=${extract}&state=${state}&city=${city}&begin=${begin}&end=${end}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                $("#table tbody").append(json.message.map(generateList));
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

const formModal = document.getElementById('formModal');
const divMessageForm = document.getElementById('messageForm');
formModal.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('formModal');
    fetch(`${baseUrl}financeiro/salva-conta-diretorio`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable();
                setTimeout(() => {
                    $('#modal').modal('hide');
                    formModal.reset();
                }, 250)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function csv() {
    let formData = formDataToJson('filter');
    directory = formData.directory;
    nickname = formData.nickname;
    extract = formData.extract;
    state = formData.state;
    city = formData.city;
    begin = formData.begin;
    end = formData.end;
    window.open(
        `${baseUrl}api/financeiro/exporta-contas-diretorios/?directory=${directory}&nickname=${nickname}&extract=${extract}&state=${state}&city=${city}&begin=${begin}&end=${end}`,
        '_blank');
}

function csvExtract() {
    window.open(
        `${baseUrl}api/financeiro/exporta-extratos`,
        '_blank');
}

$(document).ready(function () {
    generateTable();
    if (stateUser > 0) getCities(stateUser, 'city');
    $("#valueStart").maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', allowZero: true});
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});
