var valueTotal = 0;

function teste($id) {
    var input = document.getElementById($id);
    var nome = document.getElementById($id).value;
    input.addEventListener("click", function () {
        input.click();
    });
    input.addEventListener("change", function () {
        if (input.files.length > 0) {
            input.innerHTML = nome;
        }
    });
}

function valorParcelas() {
    let nParcelas = parseInt(document.getElementById("installmentsNumber").value);
    let valorTotal = document.getElementById("billAmount").value;
    valorTotal = unformatMoney(valorTotal);
    let valorParcela = valorTotal / nParcelas;
    document.getElementById('installmentsValue').value = formatMoney(valorParcela)
}

function valorParcelasDeposito() {
    let nParcelas = parseInt(document.getElementById("installmentsNumberDeposit").value);
    let valorTotal = document.getElementById("depositAmount").value;
    valorTotal = unformatMoney(valorTotal);
    let valorParcela = valorTotal / nParcelas;
    document.getElementById('installmentsValueDeposit').value = formatMoney(valorParcela)
}

function changeStep(current, option) {
    const progressbar = document.querySelectorAll('#progressbar li');
    if (option == 'next') {
        const url = new URL(window.location.href);
        const params = new URLSearchParams(url.search);

        if (params.size == 0 && !validateForm(current)) return;
        if (!ValidateFormFinancial(current)) return;

        document.getElementById(`step-${current}`).style.display = 'none';
        document.getElementById(`step-${current + 1}`).style.display = 'block';
        progressbar[current].classList.add('active');
    } else {
        document.getElementById(`step-${current}`).style.display = 'none';
        document.getElementById(`step-${current - 1}`).style.display = 'block';
        progressbar[current - 1].classList.remove('active')
    }
}

function setValueInput() {
    var laborCPF = document.querySelector('#laborCPF').value;
    var laborName = document.querySelector('#laborName').value;
    var type = document.getElementById("type").value;
    $('.laborDiv').show();
    $("#providerDoc").val(laborCPF).attr('readonly', false);
    $("#providerName").val(laborName).attr('readonly', false);
    $("#paymentMethod option[value='4']").remove();
    $('#requestStatus').attr("style", "pointer-events: visibled;");
    $("#requestStatus9").addClass('d-none');
    $("#requestStatus1, #requestStatus3").removeClass('d-none');
    $("#requestStatus").val('1');
    if (type == 14 || type == 15) {
        $('.laborDiv').hide();
        $('.laborDiv input').prop('required', false);
    }
    if (type == 7) {
        // $("#providerDoc").val(laborCPF).attr('readonly', true);
        // $("#providerName").val(laborName).attr('readonly', true);
        $('.laborDiv').hide();
        $('.laborDiv input').prop('required', false);
    }
    if (type == "16") {
        var originDirectory = document.querySelector('#originDirectory').value;
        var splitOriginDirectory = originDirectory.split(" / ");
        $("#transferDirectoryOrigin").val(splitOriginDirectory[0]);
        $("#transferCnpjOrigin").val(splitOriginDirectory[1]);
        $("#directory").val(splitOriginDirectory[0]);
        var destinationDirectory = document.querySelector('#destinationDirectory').value;
        var splitDestinationDirectory = destinationDirectory.split(" / ");
        $("#transferDirectoryDestiny").val(splitDestinationDirectory[0]);
        $("#transferCnpjDestiny").val(splitDestinationDirectory[1]);
    }
    if (type == "18") {
        $("#gruTseTre").val("").attr('readonly', false);
        $("#gruCnpjTseTre").val("").attr('readonly', false);
    }
    $("#providerType, #typeDocument").val('');
    $('#providerType, #typeDocument').attr("style", "pointer-events: visibled;");
    if(type == "19") {
        $("#providerType").val('1');
        $("#typeDocument").val('2');
        $('#providerType, #typeDocument').attr("style", "pointer-events: none;");
        $("#requestStatus").val('9');
        $("#requestStatus9").removeClass('d-none');
        $("#requestStatus1, #requestStatus3").addClass('d-none');
    }
    $('#paymentMethod').attr("style", "pointer-events: visibled;");
    $("#specieDiv, #pixDiv, #depositDiv, #bankSlipDiv").hide(300);
    $("#paymentMethod").val('');
    if(type == "21") {
        const select = document.querySelector('#paymentMethod');
        select.options[select.options.length] = new Option('Espécie', '4');
        $("#origin").val('3');
        $("#paymentMethod").val('4');
        $("#requestStatus").val('3');
        $("#specieDiv").show(300);
        $("#divPayDay, #divPendency").hide(300);
        $('#paymentMethod, #requestStatus').attr("style", "pointer-events: none;");
    }
}

function getTseTreGru(directory) {
    var directory = document.querySelector('#directory').value;
    $("#gruDirectory").val(directory);
    fetch(`${baseUrl}api/financeiro/cnpj-gru/?directory=${directory}`, {
        method: "POST",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let a = json.message;
            if (a['treTse']) {
                $("#gruTseTre").val(a['treTse']);
                $("#gruCnpjTseTre").val(a['cnpj']);
            }
        });
    });
}

function resetIput() {
    $("#paymentMethod").val('');
    $('#bankSlipDiv, #depositDiv').hide(300);
}

function setData(paymentMethod) {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);

    if (params.size == 0) {
        $("#paymentDoc").attr('readonly', false);
        $("#paymentName").attr('readonly', false);
        $("#depositAmount").val('').attr('readonly', false);
        $("#billAmount").val('').attr('readonly', false);
        $("#pixAmount").val('').attr('readonly', false);
        if ($("#type").val() == 7) {
            if ($("#nfValue").val() == "") return;
            if (paymentMethod == "2") {
                $("#paymentDoc").val(document.querySelector('#laborCPF').value);
                $("#paymentName").val(document.querySelector('#laborName').value);
                $("#depositAmount").val(document.querySelector('#nfValue').value);
                if ($("#providerType").val() != 3) {
                    $("#paymentDoc").attr('readonly', true);
                    $("#paymentName").attr('readonly', true);
                    $("#depositAmount").attr('readonly', true);
                }
            } else if (paymentMethod = 1) {
                $("#billAmount").val(document.querySelector('#nfValue').value).attr('readonly', true);
            } else if (paymentMethod = 3) {
                $("#pixAmount").val(document.querySelector('#nfValue').value).attr('readonly', true);
            }
        }
    }
    if ($("#type").val() == 17) {
        $("#paymentDoc").val(document.querySelector('#providerDoc').value);
        $("#paymentName").val(document.querySelector('#providerName').value);
    }
}

var formAdd = document.getElementById('msform');
$("#msform").submit(function (e) {
    e.preventDefault();
    showLoading();
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);

    if (params.size > 0 && !validateForm(2)) {
        closeLoading();
        return;
    }
    if (!ValidateFormFinancial(2)) {
        closeLoading();
        return;
    }
    fetch(`${baseUrl}api/financeiro/pagamentos/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.href = baseUrl + 'financeiro/pagamentos';
                }, 2500)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

//jQuery extension method:
jQuery.fn.filterByText = function (textbox) {
    return this.each(function () {
        var select = this;
        var options = [];
        $(select).find('option').each(function () {
            options.push({
                value: $(this).val(),
                text: $(this).text()
            });
        });
        $(select).data('options', options);
        $(textbox).bind('change keyup', function () {
            var options = $(select).empty().data('options');
            var search = $.trim($(this).val());
            var regex = new RegExp(search, "gi");

            $.each(options, function (i) {
                var option = options[i];
                if (option.text.match(regex) !== null) {
                    $(select).append(
                        $('<option>').text(option.text).val(option.value)
                    );
                }
            });
        });
    });
};

$(function () {
    $('#paymentBank').filterByText($('#paymentText'));
});

$(document).ready(function () {
    inputFile();
    getValuesInput();
    $("#taxiValue, #fuelValue, #nfValue, #billAmount, #depositAmount, #pixAmount, #specieAmount, #packageValue, #totalStep, #valueTransfer, #valueTransferCandidate, #splitTransactionDiscount, #gruValue, #beginValue, #endValue").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    $("#penalty, #fees, #discounts, #inss, #irrf, #csll, #cofins, #pispasep, #iss").mask('#.##0,00', {reverse: true});  
});

var elementRef = `<div class='dep_fc'>
                    <label class="btn btn-block btn-success text-white" name="othersFilesDiv">
                        <input class="upload-file-selector" id="othersFiles" type="file" name="othersFiles[]">
                    </label>
                    <button class='btn btn-primary remove' type='button' ><i class="fa fa-trash" aria-hidden="true"></i></button>
                </div>`;

$("#add-campo").click(function () {
    $('#divCloned').append(elementRef);
});

$(document).on('click', 'button.remove', function () {
    $(this).closest('div.dep_fc').remove();
});

function changeModal(modal, action) {
    $('#' + modal).modal(action);
}

function handleChange(event, value) {
    const {checked} = event.target;
    var totalSpan = document.getElementById('totalSpanInput').value;
    newValue = parseFloat(totalSpan) + parseFloat(value);
    if (!checked) newValue = parseFloat(totalSpan) - (value);
    $("#totalSpanInput").val(newValue);
    var total = newValue.toLocaleString('pt-br', {minimumFractionDigits: 2});
    document.getElementById("totalSpan").innerHTML = total;
}

function validTypeAccount(typeDestiny) {
    let typeOrigin = document.getElementById('transferAccountTypeOrigin').value;
    let validation = true;
    if(typeOrigin == 1 && typeDestiny == 2) validation = false;
    if(typeOrigin == 1 && typeDestiny == 4) validation = false;
    if(typeOrigin == 2 && typeDestiny == 1) validation = false;
    if(typeOrigin == 2 && typeDestiny == 3) validation = false;
    if(typeOrigin == 3 && typeDestiny == 4) validation = false;
    if(typeOrigin == 3 && typeDestiny == 2) validation = false;
    if(typeOrigin == 4 && typeDestiny == 1) validation = false;
    if(typeOrigin == 4 && typeDestiny == 3) validation = false;
    return validation;
}

function getAccount(type, directory, origin) {
    var validation = validTypeAccount(type);
    if(!validation) {
        alert('Atenção, transferências entre estes tipos de contas não são permitidas, selecione novamente.');
        $("#transferAccountTypeDestiny").val("");
        $("#transferDirectoryDestiny").val("");
        $("#transferCnpjDestiny").val("");
        $("#transferDirectoryIdDestiny").val("");
        $("#transferBankDestiny").val("");
        $("#transferAgencyDestiny").val("");
        $("#transferAccountDestiny").val("");
        return;
    }
    $('#originatorsDivContent').hide(300);
    $("#table tbody").empty();
    document.getElementById("totalSpan").innerHTML = '0,00';
    var directory = document.getElementById(directory).value;
    var splitDirectory = directory.split(" / ");
    let account = '';
    if(origin == "Origin") {
        if(directory == "") {
            showNotify('danger', 'Indique um diretório de origem!');
            return false;
        }
        $("#transferDirectoryOrigin").val(splitDirectory[0]);
        $("#transferCnpjOrigin").val(splitDirectory[1]);
        account = $('#transferAccountOrigin').val();
    } else if(origin == "Destiny") {
        if(directory == "") {
            showNotify('danger', 'Indique um diretório de destino!');
            return false;
        }
        $("#transferDirectoryDestiny").val(splitDirectory[0]);
        $("#transferCnpjDestiny").val(splitDirectory[1]);
        account = $('#transferAccountDestiny').val();
    }
    if(document.getElementById("transferAccountTypeOrigin").value == "3" && document.getElementById("transferAccountTypeDestiny").value == "3") {
        $('#originatorsDivContent').show(300);
    }
    fetch(`${baseUrl}api/financeiro/contas/?type=${type}&directory=${splitDirectory[0]}&account=${account}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let a = json.message;
            if (a.length === 0) {
                alert('Conta não encontrada!');
                $("#transferAccountType" + origin).val('');
                $('#transferAccount' + origin).prop("readonly", false);
            } else {
                $('#transferAccount' + origin).prop('readonly', true);
                alert('Confira os dados da conta bancária. Se não for a desejada, retorne o campo de tipo e digite o número da conta no campo conta e selecione novamente o tipo');
            }
            $("#transferDirectoryId" + origin).val(a['id']);
            $("#transferBank" + origin).val(a['bank']);
            $("#transferAgency" + origin).val(a['agency']);
            $("#transferAccount" + origin).val(a['account']);
        });
    });
}

function contractRequired() {
    if ($('#directory').val() == 'Diretório Nacional') {
        $('#contract').attr('required', true)
    } else {
        $('#contract').attr('required', false)
    }
}

function getValuesInput() {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);

    if (params.size > 0) {
        $('#requestStatus').val('3').prop('readonly', true);
        $("#requestStatus option[value='1']").prop('disabled', true);
        $('#ofxId').val(params.get('ofx'));
        $('#ofxPendency').val(params.get('transaction_id'));
        $('#directory').val(params.get('directory')).prop('readonly', true);
        $('#description').val(params.get('description')).prop('readonly', true);
        $('#nfNumber').val(params.get('numberNf'));
        $('#billAmount, #depositAmount, #pixAmount').prop('readonly', true);
        $('#nfValue, #depositAmount, #billAmount, #pixAmount').val(formatMoney(params.get('value') * -1));
        $('#paymentSlipDueDate, #depositMaturityDueDate, #payDay').val(params.get('date')).prop('readonly', true);
        $('#origin').val(params.get('directoryAccountNickname'));

        // Setar valores de área e centro de custo para Instalações

        getCostCenter(params.get('directory'), 11);
        getArea(params.get('directory'), 22);

        let description = params.get('description');
        if(description.includes("TAR") || description.includes("Tar") || description.includes("Tarifa")) {
            var date = params.get('date').split('/');
            $('#type').val('11');
            $('#providerDoc').val('00.000.000/0001-91');
            $('#providerName').val('BANCO DO BRASIL S.A');
            $('#providerType').val(2);
            $('#typeDocument').val(4);
            $('#nfDate').val(date[2] + '-' + date[1] + '-' + date[0]);
            $('#paymentMethod').val(2);
            $('#paymentDoc').val('00.000.000/0001-91');
            $('#paymentName').val('BANCO DO BRASIL S.A');
            $('#paymentText').val('001');
            $('#paymentBank').val(1);
            $('#paymentBankAccountType').val('current');
            payments(2);
            expense();
            const paymentBankAgency = params.get('directoryAccountAgency').split("-");
            $('#paymentBankAgency').val(paymentBankAgency[0]);
            $('#paymentBankAgencyDigit').val(paymentBankAgency[1]);
            const paymentBankAccount = params.get('directoryAccountAccount').split("-");
            $('#paymentBankAccount').val(paymentBankAccount[0]);
            $('#paymentBankAccountDigit').val(paymentBankAccount[1]);
        }

        fetch(baseUrl + `api/financeiro/contas-por-diretorio/?directory=${params.get('directory')}&origin=`, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            response.json().then(json => {

                if (json.message.length === 0) {
                    showNotify('danger', 'Não existe conta vinculada a este Diretório e Origem de Despesa!');
                } else {

                    let options = `<option value="">Selecionar</option>`

                    options += json.message.map(getOptionsSelect);

                    $('#divPayDay').show();
                    $('#directoryAccount').html(options).val(params.get('account'));
                    $('#directoryAccount option').prop('disabled', true);
                    $(`#directoryAccount option[value='${params.get('account')}']`).prop('disabled', false);
                }
            });
        });
    }
}

function getOptionsSelect(option) {

    return `<option value="${option.id}">${option.name}</option>`;
}

$('#locationDirectory').change(function () {
    let n = $('#locationDirectory').val();
    $("#locationDirectory").attr('required', true);
    $("#divSetCommission").addClass('d-none');
    $("#locationCommission").val('');
    $("#locationCommission").attr('required', false);
    $('#locationCommissionDescription').val('');
    $("#locationCommissionDescription").attr('required', false);
    $("#comissionDiv").addClass('d-none');
    if(n == 1){
        $("#locationCommission").attr('required', true);
        $("#divSetCommission").removeClass('d-none');
    }
});

$('#locationCommission').change(function () {
    let v = $('#locationCommission').val();
    $('#locationCommissionDescription').val('');
    $("#locationCommissionDescription").attr('required', false);
    $("#comissionDiv").addClass('d-none');
    if(v == 1) {
        $("#locationCommissionDescription").attr('required', true);
        $("#comissionDiv").removeClass('d-none');
    }
});

$('#type').change(function () {
    let v = $('#type').val();
    $('#origin').attr("style", "pointer-events: visible;");
    if(['19', '21'].includes(v)) {
        $("#origin").val('3');
        $('#origin').attr("style", "pointer-events: none;");
    }
});

$('#origin').change(function () {
    if(['22'].includes(document.getElementById("type").value)){
        if(['4', '5', '6'].includes(document.getElementById("origin").value))  {
            $('#originatorsDivContent').hide(300);
        } else {
            $('#originatorsDivContent').show(300);
        }
    }
});

function setAutonomous() {
    const isAutonomous = $('#autonomous').is(':checked');
    const $providerType = $('#providerType');
    const $attachmentAutonomous = $('#autonomousDiv');

    $providerType
        .css('pointer-events', isAutonomous ? 'none' : 'auto')
        .val(isAutonomous ? '1' : '');

    $attachmentAutonomous.toggleClass('d-none', !isAutonomous);
}