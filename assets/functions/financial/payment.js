var type = paymentMethod = requestStatus = '';
var count = doc = 0;

function getArea(value, area) {
    const directory = value ? value : $('#directory').val();
    console.log('area ' + area)
    fetch(baseUrl + 'api/financeiro/area-por-diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`
            options += json.message.map(getOptions);
            if(document.getElementById("type").value == 18) {
                $('#gruArea').html(options);
            } else {
                $('#area, #transferArea, #transferAreaCandidate').html(options).val(area ?? '').prop('required', directory == 'Diretório Nacional');
            }
            if (area != 22){
                setTimeout(function() {
                    $('#area, #gruArea, #transferArea, #transferAreaCandidate').val("");
                }, 2000);
            }
        });
    });
}

function expense() {
    $('#divStatus').hide();
    $('#accommodationDiv, #taxiDiv, #campaignMaterialDiv, #fuelDiv, #eventDiv, #buyAssetsDiv, #laborDiv, ' +
        '#consumptionAccountsDiv, #locationDiv, #thrustDiv, #transferDiv, #transferCandidateDiv, #packageDiv, #transferDivContent, #originatorsDivContent, #GruDivContent').hide(300);
    $('#accommodationDiv input, #taxiDiv input, #campaignMaterialDiv input, #fuelDiv input, #eventDiv select, ' +
        '#buyAssetsDiv select, #laborDiv input, #consumptionAccountsDiv select, #locationDiv input, #thrustDiv input,' +
        '#laborDiv input, #transferDiv input, #transferCandidateDiv input, #transferCandidateDiv selected, #packageDiv input, #packageDiv select, #GruDivContent input').prop('required', false);
    $('#directoryDiv input, #expensesDiv input, #expensesDiv select, #paymentDiv select, #paymentDiv input').not('#contract, #area').prop('required', true);
    $('#directoryDiv, #expensesDiv, #paymentDiv').show(300);
    if (document.getElementById("type").value != "") {
        $('#descriptionDiv').show(300);
        $('#divStatus').show(300);
        $('#descriptionDiv textarea').prop('required', true);
    }
    if (document.getElementById("type").value == "1") {
        $('#accommodationDiv').show(300);
        $('#accommodationDiv input').prop('required', true);
        type = 'accommodationDiv';
    } else if (document.getElementById("type").value == "2") {
        $('#taxiDiv').show(300);
        $('#taxiDiv input').prop('required', true);
        type = 'taxiDiv';
    } else if (document.getElementById("type").value == "3") {
        $('#campaignMaterialDiv').show(300);
        $('#campaignMaterialDiv input').prop('required', true);
        type = 'campaignMaterialDiv';
    } else if (document.getElementById("type").value == "4") {
        $('#fuelDiv').show(300);
        $('#fuelDiv input').prop('required', true);
        type = 'fuelDiv';
    } else if (document.getElementById("type").value == "5") {
        $('#eventDiv').show(300);
        $('#eventDiv select').prop('required', true);
        type = 'eventDiv';
    } else if (document.getElementById("type").value == "6") {
        $('#buyAssetsDiv').show(300);
        $('#buyAssetsDiv select').prop('required', true);
        type = 'buyAssetsDiv';
    } else if (document.getElementById("type").value == "7") {
        $('#laborDiv').show(300);
        $('#laborDiv input').prop('required', true);
        // type = 'laborDiv';
    } else if (document.getElementById("type").value == "9") {
        $('#consumptionAccountsDiv').show(300);
        $('#consumptionAccountsDiv select').prop('required', true);
        type = 'consumptionAccountsDiv';
    } else if (document.getElementById("type").value == "12") {
        $('#locationDiv').show(300);
        $('#locationDiv input').prop('required', true);
        type = 'locationDiv';
    } else if (document.getElementById("type").value == "13") {
        $('#thrustDiv').show(300);
        $('#thrustDiv input').prop('required', true);
        type = 'thrustDiv';
    } else if (document.getElementById("type").value == "14") {
        document.getElementById("laborDiv").style.display = 'block';
        $('#laborDiv').show(300);
        $('#laborDiv input').prop('required', true);
        // type = 'laborDiv';
    } else if (document.getElementById("type").value == "15") {
        $('#laborDiv').show(300);
        $('#laborDiv input').prop('required', true);
        // type = 'laborDiv';
    } else if (document.getElementById("type").value == "16") {
        $('#transferDiv').show(300);
        $('#directoryDiv input, #expensesDiv input, #expensesDiv select').prop('required', false);
        $('#directoryDiv, #expensesDiv').hide(300);
        $('#transferDiV, #transferDivContent').show(300);
        $('#transferDiv input').prop('required', true);
        if(userLevel == 3) $('#originatorsDivContent').show(300);
        type = 'transferDiv';
    } else if (document.getElementById("type").value == "17") {
        $('#packageDiv').show(300);
        $('#packageDiv input, #packageDiv select').prop('required', true);
        type = 'packageDiv';
    } else if (document.getElementById("type").value == "18") {
        $('#origin').val(3);
        $('#expensesDiv').hide(300);
        $('#GruDivContent').show(300);
        $('#GruDivContent input').prop('required', true);
        $('#directoryDiv input, #expensesDiv input, #expensesDiv select').prop('required', false);
    } else if (document.getElementById("type").value == "19") {
        $('#locationDiv').show(300);
        $('#locationDiv input').prop('required', true);
        $('#paymentDiv').hide(300);
        $('#paymentDiv select, #paymentDiv input').prop('required', false);
        type = 'sessionDiv';
    } else if (document.getElementById("type").value == "22") {
        $('#originatorsDivContent').show(300);
        if(['4', '5', '6'].includes(document.getElementById("origin").value)) $('#originatorsDivContent').hide(300);
        $('#transferCandidateDiv').show(300);
        $('#directoryDiv input, #expensesDiv input, #expensesDiv select').prop('required', false);
        $('#directoryDiv, #expensesDiv').hide(300);
        $('#transferCandidateDiv input, #transferCandidateDiv select').prop('required', true);
        $('#pixCandidate').prop('required', false);
        $('#"paymentMethod"').val(2);
        type = 'transferCandidateDiv';
    }
    $('#toContract, #autonomous').prop('required', false);
}

function ValidateFormFinancial() {
    var inputs = [];
    var ids = [];
    jQuery('#startDiv [required]').each(function () {
        if (!jQuery(this).val()) {
            inputs.push(jQuery(this).attr('name'));
            ids.push(jQuery(this).attr('id'));
        }
    });
    jQuery('#descriptionDiv [required]').each(function () {
        if (!jQuery(this).val()) {
            inputs.push(jQuery(this).attr('name'));
            ids.push(jQuery(this).attr('id'));
        }
    });
    if (type != '') {
        jQuery('#' + type + ' [required]').each(function () {
            if (!jQuery(this).val()) {
                inputs.push(jQuery(this).attr('name'));
                ids.push(jQuery(this).attr('id'));
            }
        });
    }
    jQuery('.paymentMethodDiv [required]').each(function () {
        if (!jQuery(this).val()) {
            inputs.push(jQuery(this).attr('name'));
            ids.push(jQuery(this).attr('id'));
        }
    });
    if (paymentMethod != '') {
        if (paymentMethod == 'bankSlipDiv' && $('#barCode').val() == '' && $('#attachmentBillet').val() == '') {
            showNotify('danger', 'Anexe um boleto ou insira o Código de Barras!');
            return false;
        }

        jQuery('#' + paymentMethod + ' [required]').each(function () {
            if (!jQuery(this).val()) {
                inputs.push(jQuery(this).attr('name'));
                ids.push(jQuery(this).attr('id'));
            }
        });
    }
    if (requestStatus != '') {
        jQuery('#' + requestStatus + ' [required]').each(function () {
            if (!jQuery(this).val()) {
                inputs.push(jQuery(this).attr('name'));
                ids.push(jQuery(this).attr('id'));
            }
        });
    }
    console.log(inputs);
    console.log(ids);
    if (inputs.length > 0) {
        showNotify('danger', 'Preencha todos os campos obrigatórios!', 1000)
        return false;
    } else {
        if ($('#type').val() == 17) {
            calcValueTotal();
        }
        return true;
    }
}

function getRequired(id) {
    if (id == 'attachmentBillet' && $('#attachmentBillet').val() != '') {
        $('#barCode').attr('required', false);
    } else if (id == 'attachmentBillet' && $('#attachmentBillet').val() == '') {
        $('#barCode').attr('required', true);
    }

    if (id == 'barCode' && $('#barCode').val() != '') {
        $('#attachmentBillet').attr('required', false);
    } else if (id == 'barCode' && $('#barCode').val() == '') {
        $('#attachmentBillet').attr('required', true);
    }
}

function calcValueTotal() {
    valueTotal = 0;
    jQuery('.packageValue').each(function () {
        let val = jQuery(this).val();
        if (val != '') {
            val = val.replace('R$', '');
            val = val.replace('.', '');
            val = val.replace(',', '.');
            val = parseFloat(val);
        } else {
            val = 0;
        }

        valueTotal += val;

    });
}

function addPackge() {
    let html = "<div>";
    costCentersArray.forEach((costCentersArray, index) => {
        html += `<option value="${costCentersArray[0]}">${costCentersArray[1]}</option>`;
    });
    html += "</div>";
    
    $('#sectionAdd' + count).append(`
        <section class="${count}">
            <div class="row" style="margin-top: 35px;">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="packageType">Tipo:</label>
                        <select class="form-select" 
                            id="packageType${count}" 
                            name="packageType[]"
                            onchange="getPackageType(this.value, ${count})" required>
                            <option value="">Selecionar</option>
                            <option value="1">Passagens</option>
                            <option value="2">Hospedagens</option>
                            <option value="4">Mobilidade</option>
                            <option value="5">Despesas Gerais</option>
                            <option value="3">Outros</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="packageCpf${count}">CPF Beneficiário:</label>
                        <input class="form-control col-md-6" 
                            id="packageCpf${count}" required
                            name="packageCpf[]" 
                            placeholder="CPF Beneficiário" 
                            type="text" 
                            onblur="getName(this.value, '#packageName${count}')">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="packageName${count}">Nome Beneficiário:</label>
                        <input class="form-control" 
                            id="packageName${count}" 
                            name="packageName[]" 
                            placeholder="Nome Beneficiário" 
                            type="text" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="packageBond${count}">Vínculo com o NOVO:</label>
                            <select class="form-select" 
                                id="packageBond${count}" 
                                name="packageBond[]" required>
                            <option value="">Selecionar</option>
                            <option value="1">Filiado</option>
                            <option value="2">Mandatário</option>
                            <option value="3">Dirigente</option>
                            <option value="4">Prestador de Serviço</option>
                            <option value="5">Outros</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="packageValue${count}">Valor:</label>
                        <input class="form-control packageValue" 
                            id="packageValue${count}" required 
                            onkeyup="getValueTotal()"
                            name="packageValue[]" 
                            placeholder="Valor" 
                            type="text">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="packageCostCenter${count}">Centro de Custo:</label>
                        <select class="form-select" 
                            name="packageCostCenter[]" 
                            id="packageCostCenter${count}" required>
                            <option value="">Selecionar</option>
                            ${html}
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="packageReason${count}">Motivo:</label>
                        <input class="form-control" 
                            id="packageReason${count}" 
                            name="packageReason[]" 
                            placeholder="Motivo" 
                            type="text" required>
                    </div>
                </div>
            </div>

            <!-- begin: packagePassageDiv -->
            <div id="packagePassageDiv${count}" style="display: none">
                <div class="row">

                    <!-- begin: airlineCompany -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="packageAirlineCompany${count}">Empresa (Companhia aérea):</label>
                            <input class="form-control" 
                                id="packageAirlineCompany${count}" 
                                name="packageAirlineCompany[]" 
                                placeholder="Nome da Companhia" 
                                type="text" 
                                oninput="toUpper('packageAirlineCompany${count}')">
                        </div>
                    </div>

                    <!-- begin: ticketLocator -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="packageTicketLocator${count}">Nº Bilhete/Localizador (Loc):</label>
                            <input class="form-control" 
                                id="packageTicketLocator${count}" 
                                name="packageTicketLocator[]" 
                                placeholder="Ex: ABC123" 
                                type="text" 
                                oninput="toUpper('packageTicketLocator${count}')">
                        </div>
                    </div>

                    <!-- begin: flightRoute -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="packageFlightRoute${count}">Trecho:</label>
                            <input class="form-control" 
                                id="packageFlightRoute${count}" 
                                name="packageFlightRoute[]" 
                                placeholder="Ex: BSB/CGH" 
                                type="text" 
                                oninput="toUpper('packageFlightRoute${count}')">
                        </div>
                    </div>

                    <!-- begin: flightDate -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="packageFlightDate${count}">Data da Viagem:</label>
                            <input class="form-control" 
                            id="packageFlightDate${count}" 
                            name="packageFlightDate[]" 
                            placeholder="dd/mm/aaaa" 
                            type="date">
                        </div>
                    </div>
                </div>
            </div>

            <!-- begin: packageHospedageDiv -->
            <div id="packageHospedageDiv${count}" style="display: none">
                <div class="row">

                    <!-- begin: hotelCompany -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="packageHotelCompany${count}">Empresa (nome hotel):</label>
                            <input class="form-control" 
                                id="packageHotelCompany${count}" 
                                name="packageHotelCompany[]" 
                                placeholder="Nome do Hotel" 
                                type="text" 
                                oninput="toUpper('packageHotelCompany${count}')">
                        </div>
                    </div>

                    <!-- begin: invoiceNumber -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="packageInvoiceNumber${count}">Título (nota fiscal):</label>
                            <input class="form-control" 
                                id="packageInvoiceNumber${count}" 
                                name="packageInvoiceNumber[]" 
                                placeholder="Nº da Nota Fiscal" 
                                type="text" 
                                oninput="toUpper('packageInvoiceNumber${count}')">
                        </div>
                    </div>

                    <!-- begin: startDate -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="packageStartDate${count}">Data início:</label>
                            <input class="form-control" 
                                id="packageStartDate${count}" 
                                name="packageStartDate[]" 
                                placeholder="dd/mm/aaaa" 
                                type="date">
                        </div>
                    </div>

                    <!-- begin: endDate -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="packageEndDate${count}">Data fim:</label>
                            <input class="form-control" 
                                id="packageEndDate${count}" 
                                name="packageEndDate[]" 
                                placeholder="dd/mm/aaaa" 
                                type="date">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group d-flex align-items-start gap-3">
                        <span class="fileselector flex-grow-1">
                            <label class="btn btn-block btn-orange text-white d-flex flex-column align-items-center w-100"
                                    for="packageFile${count}"
                                    id="packageFileBtn${count}"
                                    role="button"
                                    aria-label="Selecionar arquivo de comprovante">
                                <input class="upload-file-selector" 
                                    id="packageFile${count}" 
                                    type="file"
                                    name="packageFile[]"
                                    accept=".pdf,.jpg,.jpeg,.png"
                                    aria-required="true">
                                <i class="fa fa-paperclip margin-correction" aria-hidden="true"></i>
                                <span id="packageFile${count}Label" 
                                    default="Anexar Comprovante" 
                                    class="text-truncate text-center w-100" 
                                    style="max-width: 180px; overflow: hidden;"></span>
                                Anexar Comprovante
                            </label>
                        </span>
                        <button class='btn btn-danger remove' 
                            type='button'
                            aria-label="Remover arquivo"
                            onclick="clean('packageFile${count}', 'packageFileBtn${count}', '')">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <div class="col text-end text-danger">
                    <i class="fa fa-trash m-t-35" title="Excluir Campos" onclick="delElements(${count})"></i>
                </div>
            </div>
        </section>
        <section id="sectionAdd${count + 1}" style="display: none"></section>
    `).show(300);
    $('#packageValue' + count).maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    $('#packageCpf' + count).mask('999.999.999-99');
    count++;
    inputFile();
}

function getValueTotal() {
    calcValueTotal();
    const method = $('#paymentMethod').val();
    if (method == 1) {
        $('#billAmount').val(formatMoney(valueTotal)).attr('readonly', true);
    } else if (method == 2) {
        $('#depositAmount').val(formatMoney(valueTotal)).attr('readonly', true);
    } else if (method == 3) {
        $('#pixAmount').val(formatMoney(valueTotal)).attr('readonly', true);
    }
    $('#nfValue').val(formatMoney(valueTotal)).attr('readonly', true);
    $('#totalStep').text(formatMoney(valueTotal));
}

function delElements(id) {
    $('.' + id).hide(300);
    setTimeout(function () {
        $('.' + id).remove();
        getValueTotal();
    }, 400)
}

function delSection(id) {
    $('#' + id).hide(300);
    setTimeout(function () {
        $('#' + id).remove();
        getValueTotal();
    }, 400)
}

function payments() {
    if ($("#nfValue").val() == "" && $("#type").val() == 7) {
        $("#paymentMethod").val('');
        return alert('Insira o valor da NF');
    }
    if ($("#type").val() == 7 && ($("#laborCPF").val() == "" || $("#laborName").val() == "")) {
        $("#paymentMethod").val('');
        return alert('Insira o CPF/CNPJ e Nome do Prestador!');
    }
    if ($("#type").val() == 17 && ($("#providerDoc").val() == "" || $("#providerName").val() == "")) {
        $("#paymentMethod").val('');
        return alert('Insira o CNPJ/CPF e Razão Social/Nome do Fornecedor!');
    }
    $('#bankSlipDiv, #depositDiv, #pixDiv').hide(300);
    if (document.getElementById("paymentMethod").value == "1") {
        $('#bankSlipDiv').show(300);
        $('#bankSlipDiv input, #bankSlipDiv select').prop('required', true);
        paymentMethod = 'bankSlipDiv';
        if ($('#type').val() == 17) {
            $('#depositAmount').val(0).attr('readonly', false);
            $('#pixAmount').val(0).attr('readonly', false);
            $('#billAmount').val(formatMoney(valueTotal)).attr('readonly', true);
        }
    } else if (document.getElementById("paymentMethod").value == "2") {
        $('#depositDiv').show(300);
        $('#depositDiv input, #depositDiv select').prop('required', true);
        paymentMethod = 'depositDiv';
        if ($('#type').val() == 17) {
            $('#billAmount').val(0).attr('readonly', false);
            $('#pixAmount').val(0).attr('readonly', false);
            $('#depositAmount').val(formatMoney(valueTotal)).attr('readonly', true);
        }
    } else if (document.getElementById("paymentMethod").value == "3") {
        $('#pixDiv').show(300);
        $('#pixDiv input, #pixDiv select').prop('required', true);
        paymentMethod = 'pixDiv';
        if ($('#type').val() == 17) {
            $('#billAmount').val(0).attr('readonly', false);
            $('#depositAmount').val(0).attr('readonly', false);
            $('#pixAmount').val(formatMoney(valueTotal)).attr('readonly', true);
        }
    } else if (document.getElementById("paymentMethod").value == "4") {
        $('#specieDiv').show(300);
        $('#specieDiv input, #specieDiv select').prop('required', true);
        paymentMethod = 'specieDiv';
    }
}

function statusPayment() {
    $('#divPendency, #divPayDay, #divScheduledDate, #divRefund').hide(300);
    $('#divPendency textarea, #divPayDay input, #divPayDay select, #divScheduledDate input, #divRefund input').prop('required', false);
    if (document.getElementById("requestStatus").value == '1') {
        $('#divPendency').show(300);
        $('#divPendency textarea').prop('required', true);
        requestStatus = 'divPendency';
    } else if (document.getElementById("requestStatus").value == '2') {
        $('#divScheduledDate').show(300);
        $('#divScheduledDate input').prop('required', true);
        requestStatus = 'divScheduledDate';
    } else if (document.getElementById("requestStatus").value == '3') {
        if (document.getElementById("type").value == 21) {
            let directory = $('#directory').val();
            if (directory == "") {
                $('#requestStatus').val("1");
                alert("Indique o Diretório!");
                return;
            } 
        } else {
            getDirectoryAccount();
            $('#divPayDay').show(300);
            $('#divPayDay .form-control').prop('required', true);
            requestStatus = 'divPayDay';
        }
    } else if (document.getElementById("requestStatus").value == '8') {
        getDirectoryAccount();
        $('#divRefund, #divPayDay').show(300);
        $('#divPayDay .form-control, #divRefund input').prop('required', true);
        requestStatus = 'divPayDay';
    }
}

function getDirectoryAccount() {
    let directory = $('#directory').val();
    if (document.getElementById("type").value == 16) {
        directory = $('#transferDirectoryOrigin').val();
        if (directory == "") alert("Indique o tipo da conta!");
    }
    const origin = $('#origin').val();
    fetch(baseUrl + `api/financeiro/contas-por-diretorio-superior/?directory=${directory}&origin=${origin}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {

            if (json.message.length === 0) {
                showNotify('danger', 'Não existe conta vinculada a este Diretório e Origem de Despesa!');
            } else {

                let options = `<option value="">Selecionar</option>`

                options += json.message.map(getOptions);

                $('#directoryAccount').html(options);
            }
        });
    });
}

function AddDoc(id) {
    $('#' + id).prepend(`
        <div class="col-md-4 text-center" id="doc${doc}" style="display: none">
            <div class="mb-1">
                <div class="form-group ">
                    <span class="fileselector">
                        <label class="btn btn-block btn-orange text-white"
                               for="packageFile${doc}"
                               id="packageFileBtn${doc}">
                            <input class="upload-file-selector"
                                   id="packageFile${doc}"
                                   type="file" name="packageFile[][]">
                            <i class="fa fa-paperclip margin-correction"></i>
                            <span id="packageFile${doc}Label"
                                  default="Anexar Beneficiário(s)">Anexar Comprovante</span>
                        </label>
                    </span>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <button type="button" class="btn btn-info text-white m-r-10"
                    onclick="clean('packageFile${doc}', 'packageFileBtn${doc}', 'Anexar Comprovante')">
                    <i class="fa fa-eraser" aria-hidden="true"></i> Limpar
                    </button>
                    <i class="fa fa-plus-circle text-primary" title="Adicionar Documento" onclick="AddDoc('${id}')"></i>
                </div>
            </div>
        </div>
    `);
    inputFile();
    $('#doc' + doc).show(300);
    doc++;
}

function showMessage(val) {
    $('#registerBtn').prop('disabled', false);
    let select = $('#contract option');
    let option = '';
    for (let i = 0; i < select.length; i++) {
        if (select[i].value == val) option = select[i];
    }

    let situation = option.getAttribute('datatype');
    let stage = option.getAttribute('data-action');

    if (situation < 3 && stage == 3) {
        return;
    } else if (situation < 3 && stage < 3) {
        showNotify('warning', 'Esse contrato ainda não foi assinado!', 1500);
    } else if (situation == 3) {
        showNotify('danger', 'Atenção, este contrato está expirado. Favor procurar o seu imediato superior ou o departamento de contratos do NOVO para normalização o mais breve possível!', 4500);
        //$('#registerBtn').prop('disabled', true);
    }
}

function getCostCenterTransfer(value) {
    const directory = value.split('/');
    $('#directory').val(directory[0]);
    getCostCenter();
    getArea(directory[0]);
}

function getCostCenter(value, costCenter) {
    const directory = value ? value : $('#directory').val();
    fetch(baseUrl + 'api/financeiro/centro-de-custo/diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`
            options += json.message.map(getOptions);
            $('#costCenter, #gruCostCenter, #transferCostCenter, #transferCostCenterCandidate').html(options).val(costCenter ?? '');
        });
    });
}

function getOptions(option) {

    return `<option value="${option.id}">${option.name}</option>`;
}

function verifyCpfCnpj(id) {
    let value = document.getElementById(id).value.replace(/\D/g, "");
    let directory = document.getElementById('directory').value;
    document.getElementById(id).value = maskCpfCnpj(value);
    if (validateCpfCnpj(value)) {
        fetch(baseUrl + `api/sistema/fornecedores/${value}/${directory}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                if (json.name != '') {
                    console.log('teste')
                    //if (document.getElementById('paymentDoc')) document.getElementById('paymentDoc').value = value;
                    //if (document.getElementById('paymentName')) document.getElementById('paymentName').value = json.name;
                    if (document.getElementById('providerName')) document.getElementById('providerName').value = json.name;
                    if (document.getElementById('providerType')) document.getElementById('providerType').value = json.type;
                    if (document.getElementById('paymentText')) document.getElementById('paymentText').value = json.cod;
                    if (document.getElementById('paymentBank') && document.getElementById('paymentBank').value == '') document.getElementById('paymentBank').value = json.bank;
                    if (document.getElementById('paymentBankAgency') && document.getElementById('paymentBankAgency').value == '') document.getElementById('paymentBankAgency').value = json.agency;
                    if (document.getElementById('paymentBankAccount') && document.getElementById('paymentBankAccount').value == '') document.getElementById('paymentBankAccount').value = json.account;
                    if (id == 'laborCPF') {
                        if (document.getElementById('laborName')) document.getElementById('laborName').value = json.name;
                        if (document.getElementById('providerDoc')) document.getElementById('providerDoc').value = value;
                    }
                } else if (json.contracts.length > 0) {
                    if (document.getElementById('providerName')) document.getElementById('providerName').value = json.contracts[0].name;
                }
                let options = `<option value="">Selecionar</option>`;
                options += json.contracts.map(generateOptions);
                $('#contract').html(options);
                if (contractId) $('#contract').val(contractId);
            });
        });
    }
}

function generateOptions(option) {

    return `<option value="${option.id}" datatype="${option.situation}" data-action="${option.stage}">${option.number} - ${option.name}</option>`;
}

function getName(cpf, inputNameId) {
    fetch(baseUrl + 'api/cadastros/pega-nome/' + cpf, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            n = json.name;
            $(inputNameId).val(n);
        });
    });
}

function getContractCostCenter(contract) {
    fetch(baseUrl + 'api/cadastros/pega-centro-custo-contrato/' + contract, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            costCenter = json.costCenter;
            $('#costCenter').val(costCenter);
            $('#laborOccupation').val(json.object);
        });
    });
}

function getPackageType(value, count, id) {
    if(id == undefined) id = '';
    if(count == undefined) count = '';

    // Divs
    const packagePassageDiv = document.getElementById('packagePassageDiv' + id + count);
    const packageHospedageDiv = document.getElementById('packageHospedageDiv' + id + count);
    packagePassageDiv.style.display = 'none';
    packageHospedageDiv.style.display = 'none';

    // Passage
    const packagePassageFields = [
        'packageAirlineCompany' + id + count,
        'packageTicketLocator' + id + count,
        'packageFlightRoute' + id + count,
        'packageFlightDate' + id + count
    ].map(id => document.getElementById(`${id}`));

    packagePassageFields.forEach(field => {
        if(field) field.removeAttribute('required');
    });

    // Hospedage
    const packageHospedageFields = [
        'packageHotelCompany' + id + count,
        'packageInvoiceNumber' + id + count,
        'packageStartDate' + id + count,
        'packageEndDate' + id + count
    ].map(id => document.getElementById(`${id}`));

    packageHospedageFields.forEach(field => {
        if(field) field.removeAttribute('required');
    });

    // Verifica se é passagem ou hospedagem
    let array = ["1", "2"];
    if(array.includes(value)) {
        if(value == 1) {
            packagePassageFields.forEach(field => {
                if(field) field.setAttribute('required', 'required');
            });

            packagePassageDiv.style.display = 'block';
            packageHospedageDiv.style.display = 'none';

        } else if(value == 2) {
            packageHospedageFields.forEach(field => {
                if(field) field.setAttribute('required', 'required');
            });

            packagePassageDiv.style.display = 'none';
            packageHospedageDiv.style.display = 'block';
        }
    }
}

// Verifica se o número da nota fiscal já não foi lançado para o fornecedor
function checkNfNumber(nf){
    let providerDoc = document.getElementById('providerDoc');
    if(providerDoc) {
        providerDoc = providerDoc.value.replace(/\D/g, "");
    } else {
        providerDoc = document.getElementById('laborCPF').value.replace(/\D/g, "");
    }
    if(['7', '14', '15'].includes($('#type').val())) providerDoc = document.getElementById('laborCPF').value.replace(/\D/g, "");
    if(providerDoc == ""){
        showNotify('danger', "Digite o CNPJ do Prestador/Fornecedor", 1500);
        document.getElementById('nfNumber').value = "";
        return;
    }
    fetch(baseUrl + "api/financeiro/verifica-numero-nf/" + nf + "/" + providerDoc, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status !== 201) {
                showNotify('danger', json.message, 4000);
                document.getElementById('nfNumber').value = "";
            }
        });
    });
}