/** BEGIN FORM 1 */
const formAccounting = document.getElementById('formAccounting');
const divMessageFormAccounting = document.getElementById('divMessageFormAccounting');
formAccounting.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formAccounting = formDataToJson('formAccounting');
    fetch(`${baseUrl}contabilidade/registro-contabil/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formAccounting)
    }).then(response => {
        closeLoading();
        divMessageFormAccounting.classList.add("alert-danger");
        divMessageFormAccounting.classList.remove("alert-success");
            $('#save').removeClass('disabled');
            response.json().then(json => {
            divMessageFormAccounting.innerHTML = json.message;
            $('#divMessageFormAccounting').removeClass('d-none');
            if (response.status === 201) {
                //$('#save').addClass('disabled');
                divMessageFormAccounting.classList.add("alert-success");
                divMessageFormAccounting.classList.remove("alert-danger");
            }
        });
    });
});

function changeRadio(type) {
    let div1 = document.getElementById('div1');
    let div2 = document.getElementById('div2');
    let div3 = document.getElementById('div3');
    div1.classList.add('d-none');
    div2.classList.add('d-none');
    div3.classList.add('d-none');
    switch (type) {
        case 1:
            div1.classList.remove('d-none');
            break;
        case 2:
            div2.classList.remove('d-none');
            break;
        case 3:
            div3.classList.remove('d-none');
            break;
        default:
            div1.classList.add('d-none');
            div2.classList.add('d-none');
            div3.classList.add('d-none');
            break;
    }
}

let countDuplicateDiv1 = 0;
function duplicateDiv1() {
    countDuplicateDiv1++;
    let html = `<input class="form-control" type="search" placeholder="Pesquisar" name="accountingDebit[]" list="accountsDebit"><br>`;
    $("#duplicatedAccountingDebit").append(html);
    html = `<input class="form-control maskMoney" name="valueAccountingDebit[]" id="valueAccountingDebit${countDuplicateDiv1}" type="text" onkeyup="formatarMoeda(this.id)"><br>`
    $("#duplicatedValueDebit").append(html);
    html = `<input class="form-control" type="search" placeholder="Pesquisar" name="accountingCredit[]" list="accountsCredit"><br>`
    $("#duplicatedAccountingCredit").append(html);
    html = `<input class="form-control" name="valueAccountingCredit[]" id="valueAccountingCredit${countDuplicateDiv1}" type="text" onkeyup="formatarMoeda(this.id)"><br>`
    $("#duplicatedValueCredit").append(html);
}

let countDuplicateDiv2 = 0;
function duplicateDiv2() {
    let html = `<input class="form-control" type="search" placeholder="Pesquisar" name="accounting[]" list="accountsDebit"><br>`;
    $("#duplicatedAccountingM").append(html);
    countDuplicateDiv2++;
    html = `<input class="form-control" name="valueAccounting[]" id="valueAccounting${countDuplicateDiv2}" type="text" onkeyup="formatarMoeda(this.id)"><br>`;
    $("#duplicatedValueM").append(html);
}

let countDuplicateDiv3 = 0;
function duplicateDiv3() {
    let html = `<input class="form-control" type="search" placeholder="Pesquisar" name="accounting[]" list="accountsCredit"><br>`;
    $("#duplicatedAccountingM2").append(html);
    countDuplicateDiv2++;
    html = `<input class="form-control" name="valueAccounting[]" id="valueAccounting${countDuplicateDiv2}" type="text" onkeyup="formatarMoeda(this.id)"><br>`;
    $("#duplicatedValueM2").append(html);
}
/** END FORM 1 */

/** BEGIN FORM 2 */
const formAccountingForm2 = document.getElementById('formAccountingForm2');
const divMessageFormAccountingForm2 = document.getElementById('divMessageFormAccountingForm2');
formAccountingForm2.addEventListener('submit', e => {
    $('#saveForm2').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formAccountingForm2 = formDataToJson('formAccountingForm2');
    fetch(`${baseUrl}contabilidade/registro-contabil/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formAccountingForm2)
    }).then(response => {
        closeLoading();
        divMessageFormAccountingForm2.classList.add("alert-danger");
        divMessageFormAccountingForm2.classList.remove("alert-success");
        $('#saveForm2').removeClass('disabled');
            response.json().then(json => {
            divMessageFormAccountingForm2.innerHTML = json.message;
            $('#divMessageFormAccountingForm2').removeClass('d-none');
            if (response.status === 201) {
                $('#saveForm2').addClass('disabled');
                divMessageFormAccountingForm2.classList.add("alert-success");
                divMessageFormAccountingForm2.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        });
    });
});

function changeRadioForm2(type) {
    let div1Form2 = document.getElementById('div1Form2');
    let div2Form2 = document.getElementById('div2Form2');
    let div3Form2 = document.getElementById('div3Form2');
    div1Form2.classList.add('d-none');
    div2Form2.classList.add('d-none');
    div3Form2.classList.add('d-none');
    switch (type) {
        case 1:
            div1Form2.classList.remove('d-none');
            break;
        case 2:
            div2Form2.classList.remove('d-none');
            break;
        case 3:
            div3Form2.classList.remove('d-none');
            break;
        default:
            div1Form2.classList.add('d-none');
            div2Form2.classList.add('d-none');
            div3Form2.classList.add('d-none');
            break;
    }
}

let countDuplicateDiv1Form2 = 0;
function duplicateDiv1Form2() {
    countDuplicateDiv1Form2++;
    let htmlForm2 = `<input class="form-control" type="search" placeholder="Pesquisar" name="accountingDebitForm2[]" list="accountsDebitForm2"><br>`;
    $("#duplicatedAccountingDebitForm2").append(htmlForm2);
    htmlForm2 = `<input class="form-control" name="valueAccountingDebitForm2[]" id="valueAccountingDebitForm2${countDuplicateDiv1Form2}" type="text" onkeyup="formatarMoeda(this.id)"><br>`
    $("#duplicatedValueDebitForm2").append(htmlForm2);
    htmlForm2 = `<input class="form-control" type="search" placeholder="Pesquisar" name="accountingCreditForm2[]" list="accountsCreditForm2"><br>`
    $("#duplicatedAccountingCreditForm2").append(htmlForm2);
    htmlForm2 = `<input class="form-control" name="valueAccountingCreditForm2[]" id="valueAccountingCreditForm2${countDuplicateDiv1Form2}" type="text" onkeyup="formatarMoeda(this.id)"><br>`
    $("#duplicatedValueCreditForm2").append(htmlForm2);
}

let countDuplicateDiv2Form2 = 0;
function duplicateDiv2Form2() {
    let htmlForm2 = `<input class="form-control" type="search" placeholder="Pesquisar" name="accountingForm2[]" list="accountsDebitForm2"><br>`;
    $("#duplicatedAccountingMForm2").append(htmlForm2);
    countDuplicateDiv2Form2++;
    htmlForm2 = `<input class="form-control" name="valueAccountingForm2[]" id="valueAccountingForm2${countDuplicateDiv2Form2}" type="text" onkeyup="formatarMoeda(this.id)"><br>`;
    $("#duplicatedValueMForm2").append(htmlForm2);
}

let countDuplicateDiv3Form2 = 0;
function duplicateDiv3Form2() {
    let htmlForm2 = `<input class="form-control" type="search" placeholder="Pesquisar" name="accountingForm2[]" list="accountsCreditForm2"><br>`;
    $("#duplicatedAccountingM2Form2").append(htmlForm2);
    countDuplicateDiv2Form2++;
    htmlForm2 = `<input class="form-control" name="valueAccountingForm2[]" id="valueAccountingForm2${countDuplicateDiv2Form2}" type="text" onkeyup="formatarMoeda(this.id)"><br>`;
    $("#duplicatedValueM2Form2").append(htmlForm2);
}
/** END FORM 2 */

function formatarMoeda(id) {
    var elemento = document.getElementById(id);
    var valor = elemento.value;
    valor = valor + '';
    valor = parseInt(valor.replace(/[\D]+/g,''));
    valor = valor + '';
    valor = valor.replace(/([0-9]{2})$/g, ",$1");
    if (valor.length > 6) {
      valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    }
    elemento.value = valor;
}

$(function() {
    $('#accountingValue, #accountingValueForm2, #valueAccountingDebit, #valueAccountingCredit, #valueM, #valueCreditM, #valueAccountingCreditForm2, #valueAccountingDebitForm2, #valueMForm2, #valueCreditMForm2').maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
});

function deleteRecord(id, button) {
    if(confirm("Esta operação não poderá ser desfeita, tem certeza que deseja remover este lançamento?") == false) return;
    $('#deleteRecord' + button).addClass('disabled');
    fetch(`${baseUrl}contabilidade/apaga-registro-contabil/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        const divMessageForm = button == 1 ? divMessageFormAccounting : divMessageFormAccountingForm2;
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");  
        response.json().then(json => {
        divMessageForm.innerHTML = json.message;  
        $(divMessageForm).removeClass('d-none');
            if (response.status === 201) {
                divMessageForm.classList.add("alert-success");
                divMessageForm.classList.remove("alert-danger");
                setTimeout(function () {
                    //window.location.reload();
                }, 5000);
            }
        });
    });
};

$(document).ready(function () {});