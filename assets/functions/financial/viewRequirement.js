var valueTotal = 0;

function approve() {
    showLoading();
    let url = window.location.href;
    url = url.split('/');
    let total = url.length;
    const id = url[total -1];
    console.log(id)
    fetch(baseUrl + "api/financeiro/pagamentos/aprovar/" + id, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.href = baseUrl +  "financeiro/pagamentos";
                }, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function parceledBill() {

    document.getElementById("installmentsValueDiv").style.display = 'none';
    document.getElementById("billetDiv").style.display = 'none';
    document.getElementById("inCashDiv").style.display = 'none';
    if (document.getElementById("paymentType").value == "1") {
        document.getElementById("inCashDiv").style.display = 'block';
    }
    if (document.getElementById("paymentType").value == "2") {
        document.getElementById("installmentsValueDiv").style.display = 'block';
        document.getElementById("billetDiv").style.display = 'block';
    }
}

window.onload = function () {
    statusPayment();
}

$(document).ready(function () {
    setValueTotal();
    inputFile();
    // expense();
    //showContract()
    const providerDoc = document.getElementById("providerDoc");
    if(providerDoc) verifyCpfCnpj('providerDoc');
    const laborCPF = document.getElementById("laborCPF");
    if(laborCPF) verifyCpfCnpj('laborCPF');
    //payments();
    $("#travelValue, #taxiValue, #fuelValue, #nfValue, #billAmount, #depositAmount, #pixAmount, .packageValue, #valueTransfer, #valueTransferCandidate, #beginValue, #endValue").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    if(!editMode) {
        if(typePayment == 22) {
            //$('#origin').attr("style", "pointer-events: none;");
            origin = $('#origin').val();
            if(['4', '5', '6'].includes(origin)) {
                document.getElementById("originatorsDivContent").style.display = 'none';
            }
        }
        $('#type').attr("style", "pointer-events: none;");
        $('#providerType').attr("style", "pointer-events: none;");
        $('#installmentsNumber').attr("style", "pointer-events: none;");
        $('#locationDirectory').attr("style", "pointer-events: none;");
        $('#locationCommission').attr("style", "pointer-events: none;");
        if(['2', '3'].includes(requestStatus)) {
            $('#paymentBank').attr("style", "pointer-events: none;");
            $('#paymentBankAccountType').attr("style", "pointer-events: none;");
        }
        $('#eventName').attr("style", "pointer-events: none;");
        $('#accountConsumer').attr("style", "pointer-events: none;");
        $('#buyAssets').attr("style", "pointer-events: none;");
        //$('#paymentMethod').attr("style", "pointer-events: none;");
        //$('#directory').attr("style", "pointer-events: none;");
        //$('#contract').attr("style", "pointer-events: none;");
        //$('#typeDocument').attr("style", "pointer-events: none;");
    }
    if(typePayment == 18) {
        //document.getElementById("detailsDiv").style.display = 'none';
        //document.getElementById("expensesDiv").style.display = 'none';
        document.getElementById("paymentDiv").style.display = 'none';
        document.getElementById("FilesDiv").style.display = 'none';
        document.getElementById("GRUDiv").style.display = 'block';
    } else if(typePayment == 19) {
        document.getElementById("paymentDiv").style.display = 'none';
    }
});

function mCNPJ(cnpj) {
    cnpj = cnpj.replace(/\D/g, "")
    cnpj = cnpj.replace(/^(\d{2})(\d)/, "$1.$2")
    cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3")
    cnpj = cnpj.replace(/\.(\d{3})(\d)/, ".$1/$2")
    cnpj = cnpj.replace(/(\d{4})(\d)/, "$1-$2")
    return cnpj.substring(0, 18)
}

function mCPF(cpf) {
    cpf = cpf.replace(/\D/g, "")
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
    return cpf
}

function maskCpfCnpj(id) {
    let value = document.getElementById(id).value.replace(/\D/g, "");
    var tamanho = value.length;
    if (tamanho <= 11) {
        document.getElementById(id).value = mCPF(value);
    } else {
        document.getElementById(id).value = mCNPJ(value);
    }
    if (id == 'providerDoc' && validateCpfCnpj(value)) {
        fetch(baseUrl + `api/sistema/fornecedores/${value}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                document.getElementById('providerName').value = json.name;
                document.getElementById('providerType').value = json.type;
            });
        });
    }
}

var formAdd = document.getElementById('msform');
$("#msform").submit(function (e) {
    e.preventDefault();
    showLoading();
    if (!validateForm(1)) {
        closeLoading();
        return;
    }
    if (!validateForm(2)) {
        closeLoading();
        return;
    }
    if (!ValidateFormFinancial(1)) {
        closeLoading();
        return;
    }
    if (!ValidateFormFinancial(2)) {
        closeLoading();
        return;
    }
    fetch(`${baseUrl}api/financeiro/pagamentos/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.href = baseUrl + 'financeiro/pagamentos';
                }, 2500)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

var elementRef = `<div class='dep_fc'>
                        <label class="btn btn-block btn-secondary text-white" name="othersFilesDiv">
                            <input class="upload-file-selector" id="othersFiles" type="file" name="othersFiles[]">
                        </label>
                        <button class='btn btn-primary remove' type='button' ><i class="fa fa-trash" aria-hidden="true"></i></button>
                      </div>`;
$("#add-campo").click(function () {
    $('#divCloned').append(elementRef);
});

$(document).on('click', 'button.remove', function () {
    $(this).closest('div.dep_fc').remove();
});

function setValueTotal(){
    const method = $('#paymentMethod').val();
    var valueTotal = "";
    if (method == 1) {
        valueTotal = $('#billAmount').val();
    } else if (method == 2) {
        valueTotal = $('#depositAmount').val();
    } else if (method == 3) {
        valueTotal = $('#pixAmount').val();
    }
    valueTotal = valueTotal.replace('R$', '');
    valueTotal = valueTotal.replace('.', '');
    valueTotal = valueTotal.replace(',', '.');
}

function originatingTransaction(requirement) {
    window.open(
        `${baseUrl}api/financeiro/exporta-originarios/?requirement=${requirement}`,
        '_blank');
}