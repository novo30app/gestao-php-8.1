function openModalArea(id) {
    formArea.reset();
    $('#areaId').val(id);
    $('#modalArea').modal('show')
    if (id > 0) {
        showLoading('formArea')
        fetch(baseUrl + 'api/financeiro/area/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0]
                $('#nameArea').val(json.name);
                json.directories.forEach(element => {
                    $('#directories' + element.id).prop('checked', true);
                })
                closeLoading('formArea');
            })
        })
    }
}

const formArea = document.getElementById('formArea');
formArea.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('formArea');
    if (!ValidateForm('formArea')) {
        closeLoading('formArea');
        return;
    }

    let formData = formDataToJson('formArea');

    if (!formData.directories) {
        showNotify('danger', 'Selecione ao menos um Diretório!', 1000);
        closeLoading('formArea');
        return;
    }

    fetch(baseUrl + 'api/financeiro/area/cadastrar', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('formArea');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Area');
                setTimeout(function () {
                    $('#modalArea').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function getAreas(area) {
    return `
        <option value="${area.id}">${area.name}</option>
    `;
}

function generateLinesArea(costCenter) {
    let actions = '';
    if (canUpdate) actions = `<a onclick="openModalArea(${costCenter.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    return `<tr class="middle">
                    <td class="text-center">${costCenter.name} </td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}

$(document).ready(function () {
    verifySession('Area');
    verifySession('SubArea');
});

function setStorageIndexArea() {
    setStorage('Area', {
        'url': 'api/financeiro/area',
        'columns': 2,
        'variables': {
            'index': 0,
            'limit': 25
        }
    })
}

function setStorageIndexSubArea() {
    setStorage('SubArea', {
        'url': 'api/financeiro/sub-area',
        'columns': 3,
        'variables': {
            'index': 0,
            'limit': 25
        }
    })
}

function openModalSubArea(id) {
    formSubArea.reset();
    $('#subAreaId').val(id);
    $('#modalSubArea').modal('show')
    if (id > 0) {
        showLoading('formSubArea')
        fetch(baseUrl + 'api/financeiro/sub-area/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0]
                $('#nameSubArea').val(json.name);
                $('#activeSubArea').val(json.responsible);
                $('#areaSubArea').val(json.area);
                closeLoading('form');
            })
        })
    }
}

const formSubArea = document.getElementById('formSubArea');
formSubArea.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('formSubArea');
    if (!ValidateForm('formSubArea')) {
        closeLoading('formSubArea');
        return;
    }
    let formData = formDataToJson('formSubArea');
    fetch(baseUrl + 'api/financeiro/sub-area/cadastrar', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('formSubArea');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('SubArea');
                setTimeout(function () {
                    $('#modalSubArea').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function generateLinesSubArea(subarea) {
    let actions = ``;
    if (canUpdate) actions = `<a onclick="openModalSubArea(${subarea.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    return `<tr class="middle">
                    <td class="text-center">${subarea.name} </td>
                    <td class="text-center">${subarea.nameArea} </td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}
