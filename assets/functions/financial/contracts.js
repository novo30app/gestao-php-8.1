function openModal(id) {
    form.reset();
    showValue(0);
    showDate(0);
    showContracts(0);
    $('#contractDoc').val('').prop('required', true);
    $('#contractId').val(id);
    $('#contractDocBtn').addClass('is-invalid');
    $('#number').val(availableNumber);
    resetFile();
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form')
        fetch(baseUrl + 'api/financeiro/contratos/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0];
                let contract = json.contract ? json.contract : 0;
                $('#contractDoc').prop('required', false);
                $('#contractDocBtn').removeClass('is-invalid');
                if (contract > 0) {
                    showContracts(1);
                    $('#additive').val(1);
                    $('#contract').val(json.descriptionContract);
                } else {
                    showContracts(0);
                    $('#additive').val(0);
                    $('#contract').val('');
                }
                $('#costCenter').val(json.costCenter);
                $('#name').val(json.name);
                $('#directory').val(json.directory);
                $('#InputAccounts').val(json.InputAccounts);
                $('#object').val(json.object);
                $('#cpf').val(maskCpfCnpj(json.cpf));
                $('#end').val(json.dateEnd);
                $('#start').val(json.dateStart);
                $('#situation').val(json.situation);
                $('#category').val(json.category);
                $('#stage').val(json.stage);
                $('#number').val(json.number);
                $('#signatureDate').val(json.signature);
                $('#value').val(json.value != null ? formatMoney(json.value) : '');
                $('#monthlyValue').val(json.monthlyValue != null ? formatMoney(json.monthlyValue) : '');
                showDate(json.situation);
                showValue(json.category)
                closeLoading('form');
            })
        })
    }
}

function showContracts(value) {
    $('#contractDiv, #numberAdditiveDiv').hide(300);
    $('#contractDiv input').prop('required', false).val('');
    $('#name, #cpf').prop('readonly', false).val('');
    $('#numberDiv').show(300);
    if (value == 1) {
        $('#contractDiv, #numberAdditiveDiv').show(300);
        $('#name, #cpf').prop('readonly', true);
        $('#numberDiv').hide(300);
        setTimeout(function () {
            $('#contractDiv input').prop('required', true);
        }, 300);
    }
}

function resetFile() {
    let input = document.getElementById('contractDoc');
    input.parentElement.classList.remove('btn-success');
    input.parentElement.classList.add('btn-orange');
    let labelId = input.getAttribute('id') + 'Label';
    document.getElementById(labelId).textContent = document.getElementById(labelId).getAttribute('default')
}

function showValue(value) {

    if (value == 1) {
        $('#monthlyValueDiv').show(300);
        $('#monthlyValueDiv input').attr('required', true);
        $('#valueDiv').hide(300);
        $('#valueDiv input').attr('required', false);
    } else if (value == 2) {
        $('#monthlyValueDiv,#valueDiv').show(300);
        $('#monthlyValueDiv input,#valueDiv input').attr('required', true);
    } else if (value == 3 || value == 4) {
        $('#valueDiv').show(300);
        $('#valueDiv input').attr('required', true);
        $('#monthlyValueDiv').hide(300);
        $('#monthlyValueDiv input').attr('required', false);
    } else {
        $('#monthlyValueDiv,#valueDiv').hide(300);
        $('#monthlyValueDiv input,#valueDiv input').attr('required', false);
    }
}

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    if (!validateCpfCnpj(document.getElementById('cpf').value)) {
        showNotify('danger', 'CNPJ/CPF inválido!', 1500);
        closeLoading('form');
        return;
    }
    fetch(baseUrl + 'api/financeiro/contratos/cadastrar', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(form)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Contracts');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function getNameCnpj(id) {
    id = id.split(' - ');
    fetch(baseUrl + `api/financeiro/detalhe-contrato/${id[2]}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('#cpf').val(json.message.cpf);
            $('#name').val(json.message.name);
        });
    });
}

function generateLines(contract) {
    let id = contract.id;
    if (contract.contract) id = contract.contract;
    let actions = '';

    if (canUpdate) actions = `<a onclick="openModal(${contract.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    actions += `<a href="${baseUrl}financeiro/contratos/${id}"><i class="fa fa-search text-info px-1" title="Detalhes do Contrato"></i></a>`;
    if (canDelete) actions += `<i class="fa fa-trash text-danger px-1" title="Excluir" onclick="delElement('financeiro/contratos', 'Contrato', ${contract.id}, 'delete', 'Contracts')"></i>`;
    return `<tr class="middle">
                    <td class="text-center">${contract.number ? contract.number : 'Sem registro'} </td>
                    <td class="text-center">${contract.name} </td>
                    <td class="text-center">${maskCpfCnpj(contract.cpf)} </td>
                    <td class="text-center">${contract.costCenterName} </td>
                    <td class="text-center">${contract.category == 1 ? formatMoney(contract.monthlyValue) : formatMoney(contract.value)} </td>
                    <td class="text-center">${contract.dateStart} </td>
                    <td class="text-center">${contract.dateEnd ? contract.dateEnd : 'Sem registro'} </td>
                    <td class="text-center">${getStage(contract.stage)} </td>
                    <td class="text-center">${getSituation(contract.situation)} </td>
                    <td class="text-center">
                        <a class="badge badge-success label-square pointer text-white f-12" target="_blank" href="${hrefDoc(contract.doc)}">Anexo</a>
                    </td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}

function getStage(stage) {
    if (stage == 1) {
        return 'Criado';
    } else if (stage == 2) {
        return 'Colhendo Assinatura';
    } else if (stage == 3) {
        return 'Assinado'
    } else {
        return 'Desconhecido';
    }
}

function getSituation(situation) {
    if (situation == 1) {
        return 'Vigente - Prazo Determinado';
    } else if (situation == 2) {
        return 'Vigente - Prazo Indeterminado';
    } else if (situation == 3) {
        return 'Expirado'
    } else {
        return 'Desconhecido';
    }
}

function showDate(value) {
    $('#endDiv').hide(300);
    $('#end').attr('required', false);
    if (value == 1 || value == 3) {
        $('#endDiv').show(300);
        $('#end').attr('required', true);
    }
}

const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Contracts');
})

function csv() {
    let formData = formDataToJson('filter');
    let state = formData.state;
    let number = formData.number;
    let cnpj = formData.cnpj;
    let name = formData.name;
    let situation = formData.situation;
    let order = formData.order;
    let seq = formData.seq;
    let costCenter = formData.costCenter;
    let value = formData.value;
    window.open(
        `${baseUrl}api/financeiro/contratos/exportar/?costCenter=${costCenter}&situation=${situation}&value=${value}&name=${name}&cnpj=${cnpj}&number=${number}&order=${order}&seq=${seq}&limit=0&index=0`,
        '_blank');
}

$(document).ready(function () {
    inputFile();
    verifySession('Contracts');
    $("#value, #monthlyValue, #valueContractsFilter").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
});

function setStorageIndex() {
    setStorage('Contracts', {
        'url': 'api/financeiro/contratos',
        'columns': 11,
        'variables': {
            'index': 0,
            'costCenter': '',
            'cnpj': '',
            'stage': '',
            'situation': '',
            'value': '',
            'name': '',
            'number': '',
            'order': 'id',
            'seq': 'desc',
            'limit': 25
        }
    })
}

function verifyCpfCnpj() {
    let value = document.getElementById('cpf').value.replace(/\D/g, "");
    document.getElementById('cpf').value = maskCpfCnpj(value);
    fetch(baseUrl + `api/sistema/fornecedores/${value}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            if (json.name != '') {
                document.getElementById('name').value = json.name;
            } else {
                showNotify('danger', 'CNPJ/CPF não encontrado, cadastre primeiro na tela de Fornecedores e tente novamente!', 3000);
                document.getElementById('cpf').value = '';
                document.getElementById('name').value = '';
                return;
            }
        });
    });
}