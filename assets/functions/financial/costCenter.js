function openModal(id) {
    form.reset();
    $('#costCenterId').val(id);
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form')
        fetch(baseUrl + 'api/financeiro/centro-de-custo/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0]
                $('#name').val(json.name);
                $('#responsible').val(json.responsible);
                $('#directory').val(json.directoryName);
                $('#value').val(formatMoney(json.value));
                setTimeout(function () {
                    $('#subarea').val(json.subArea);
                    closeLoading('form');
                }, 300)
            })
        })
    }
}

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + 'api/financeiro/centro-de-custo/cadastrar', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('CostCenter');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function generateLines(costCenter) {
    let actions = '';
    if (canUpdate) actions = `<a onclick="openModal(${costCenter.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    if (canDelete) actions += `<i class="fa fa-trash text-danger px-1" title="Excluir" onclick="delElement('api/financeiro/centro-de-custo', 'Centro de Custo', ${costCenter.id}, 'delete', 'CostCenter')"></i>`;
    return `<tr class="middle">
                    <td class="text-center">${getCode(costCenter.id, costCenter.directory, costCenter.area, costCenter.subArea)} </td>
                    <td class="text-center">${costCenter.directoryName} </td>
                    <td class="text-center">${costCenter.name} </td>
                    <td class="text-center">${costCenter.userName} </td>
                    <td class="text-center">${costCenter.nameArea ? costCenter.nameArea : ''} </td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}

function getCode(id, directory, area, subArea) {
    if (directory && area) {

        const newDirectory = ("000" + directory).slice(-3);
        const newArea = ("000" + area).slice(-3);
        let newSubArea = '000';
        if (subArea) {
            newSubArea = ("000" + subArea).slice(-3);
        }

        return id + '.' + newDirectory + '.' + newArea + '.' + newSubArea;
    } else {
        return 'Desconhecido';
    }
}

const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('CostCenter');
})

$(document).ready(function () {
    verifySession('CostCenter');
    $('#value').maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
});

function setStorageIndex() {
    setStorage('CostCenter', {
        'url': 'api/financeiro/centro-de-custo',
        'columns': 6,
        'variables': {
            'index': 0,
            'name': '',
            'responsible': '',
            'directory': '',
            'limit': 25
        }
    })
}
