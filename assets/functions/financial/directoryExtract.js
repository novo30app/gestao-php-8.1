function add(id) {
    $('#accountId').val(id);
    $('#modal').modal('show');
}

var index = 0;
var partial = 0;
var total = 1;
var begin = end = validation = '';

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    begin = formData.begin;
    end = formData.end;
    validation = formData.validation;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    begin = end = validation = '';
    $("#table tbody").empty();
    generateTable();
}

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}

function generateList(extract) {
    let validation = '<i class="fa fa-exclamation-triangle" title="Não válido" aria-hidden="true" style="font-size: 30px; color: #FDD700;"></i>';
    let deleteExtract = '';
    if (canDelete){
        deleteExtract =     `<a href="#" type="button" style="margin-right: 10px;" onclick="deleteExtract(${extract.id})">
                                <i class="fa fa-trash" aria-hidden="true" style="font-size: 20px; color: red"></i>
                            </a>`;
    }

    let edit =  '';
    if(type == 3 && canUpdate) {
        edit =  `<a href="#" type="button" style="margin-right: 10px;" onclick="edit('${extract.id}', '${extract.dateStart}', '${extract.dateEnd}', '${extract.validation}')">
                    <i class="fa fa-pencil" aria-hidden="true" style="font-size: 20px; color: blue"></i>
                </a>`;
    }   
    if(extract.validation == 1) {
        validation = '<i class="fa fa-check-circle" title="Válido" aria-hidden="true" style="font-size: 30px; color:green;"></i>';
        deleteExtract = '';
    } else if(extract.validation == 0) {
        validation = '<i class="fa fa-times-circle" title="Inválido" aria-hidden="true" style="font-size: 30px; color: red;"></i>';
    };
    return `<tr class="middle">
				<td>${extract.name}</td>
                <td class="text-center">${extract.created}</td>
				<td class="text-center">${extract.dateStart}</td>
				<td class="text-center">${extract.dateEnd}</td>
                <td class="text-center">${validation}</td>
				<td class="text-center">
					<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                        <a href="${formatAttachment(extract.attachmentExtract)}" target="_blank">
                            <i class="fa fa-file-text" aria-hidden="true" title="Extrato" style="font-size: 30px;"></i>
                        </a>
					</div>
				</td>
                <td class="text-center">${edit} ${deleteExtract}</td>
			</tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/financeiro/extrato-diretorios/?index=${index}&userType=${type}&begin=${begin}&end=${end}&validation=${validation}&accountID=${accountID}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                $("#table tbody").append(json.message.map(generateList));
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

const formModal = document.getElementById('formModal');
formModal.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('formModal');
    fetch(baseUrl + 'financeiro/salva-extrato', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(formModal)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Contracts');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function deleteExtract(extract) {
    let text = "Tem certeza que deseja excluir?";
    if (confirm(text) == false) return;

    fetch(`${baseUrl}/financeiro/desativa-extrato/${extract}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Trail');
                changeModal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function edit(id, begin, end, validation) {
    $("#accountIdModal").val(id);
    let splitBegin = begin.split('/');
    $("#beginModal").val(splitBegin[2] + '-' + splitBegin[1] + '-' + splitBegin[0]);
    let splitEnd = end.split('/');
    $("#endModal").val(splitEnd[2] + '-' + splitEnd[1] + '-' + splitEnd[0]);
    validation = validation == 0 ? '-1' : 2;
    $("#validationModal").val(validation);
    $("#editModal").modal("show");
}

const formEditModal = document.getElementById('formEditModal');
formEditModal.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('formEditModal');
    fetch(baseUrl + 'financeiro/edita-extrato', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(formEditModal)
    }).then(response => {
        closeLoading('formEditModal');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

$(document).ready(function () {
    if(beginFilter) {
        begin = beginFilter;
        $("#begin").val(beginFilter);
    } 
    if(endFilter) {
        end = endFilter;
        $("#end").val(endFilter);
    } 
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});