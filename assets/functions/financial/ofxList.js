var conciliation = '';
$(document).ready(() => {
    const url = window.location.href;
    conciliation = url.replace(baseUrl + 'financeiro/conciliacao/', '');
    conciliation = conciliation.replace('/', '');
    verifySession('OfxList' + conciliation);
});

function setStorageIndex() {
    setStorage('OfxList' + conciliation, {
        'url': 'api/financeiro/pagamentos',
        'columns': 12,
        'variables': {
            'index': 0,
            'status': -1,
            'origin': 3,
            'cpfCnpj': '',
            'provider': '',
            'type': '',
            'directory': '',
            'dueDateBegin': '',
            'dueDateEnd': '',
            'accounting': '',
            'user': '',
            'value': '',
            'limit': 25,
            'conciliation': conciliation
        }
    })
}