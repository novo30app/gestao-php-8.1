$(document).ready(function () {
    $("#valuebillsToPayFilter, #paymentValuebillsToPayFilter").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    verifySession(sessions);
});

function setStorageIndex() {
    setStorage(sessions, {
        'url': 'api/financeiro/pagamentos',
        'columns': 10,
        'variables': {
            'index': 0,
            'id': '',
            'status': '-1',
            'conciliation': '-1',
            'origin': 0,
            'cpfCnpj': '',
            'provider': '',
            'docNumber': '',
            'type': '',
            'userType': type,
            'costCenter': '',
            'directory': '',
            'accounted': '',
            'directoryPaying': '',
            'dateType': '1',
            'dateBegin': '',
            'dateEnd': '',
            'accounting': '',
            'user': '',
            'value': '',
            'paymentValue': '',
            'area': '',
            'bbStatus': '',
            'limit': 25
        }
    })
}

function csv(type = '') {
    let obj = JSON.parse(sessionStorage.getItem(sessions));
    const get = getUrl(obj.variables);
    window.open(baseUrl + `api/financeiro/exportBillsToPay/${type}${get}`, '_blank');
}

function csvAccounting() {
    let obj = JSON.parse(sessionStorage.getItem(sessions));
    const get = getUrl(obj.variables);
    window.open(baseUrl + `contabilidade/exporta-relatorio-contabil/${get}`, '_blank');
}

function csvFiscal() {
    let obj = JSON.parse(sessionStorage.getItem(sessions));
    const get = getUrl(obj.variables);
    window.open(baseUrl + `contabilidade/exporta-relatorio-fiscal/${get}`, '_blank');
}
