const formFilter = document.getElementById('filter');
var account = directory = start = end = conciliate = accounted = '';
var partial = index = balance = 0;
var total = 1;
var load = true;

formFilter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;

    account = formData.account;
    directory = formData.directory;
    start = formData.start;
    end = formData.end;
    conciliate = formData.conciliate;
    accounted = formData.accounted;
    generateTable();
});

function resetTable() {
    account = directory = start = end = conciliate = accounted = '';
    partial = index = balance = 0;
    total = 1;
    $("#table tbody").empty();
    $('#startDateBalance').html(``);
    $('#startBalance').html(``);
    $('#totalEntries').html(``);
    $('#totalExits').html(``);
    $('#endBalance').html(``);
}

function generateLines(transaction) {

    let classe = '';
    let pre = '';
    let preTotal = parseFloat(balance.total) >= 0 ? '' : '-';
    let button = accountedStatus = '-';
    if (transaction.category == false) {
        classe = 'text-danger';
        pre = '-';
        balance = parseFloat(balance) - parseFloat(transaction.value);
        accountedStatus = 'Não';
        btnAccountedStatus = 'btn-danger';
        if (transaction.ar != null) {
            accountedStatus = 'Sim';
            btnAccountedStatus = 'btn-success';
        } 
        button = `<a class="float-end btn btn-orange text-white btn-sm" 
        href="${baseUrl}financeiro/pagamentos/novo-pagamento/?transaction_id=${transaction.id}&date=${formatDate(transaction.date)}&value=${transaction.value}&description=${transaction.description}&numberNf=${transaction.extractNumber}&ofx=${transaction.ofx}&directory=${directory}&account=${account}&directoryAccountAgency=${transaction.directoryAccountAgency}&directoryAccountAccount=${transaction.directoryAccountAccount}"
        target="_blank">Cadastrar</a>`;
        button = '<span class="text-danger">Pendente</span>';
        if (transaction.payment != null) {
            button = `<a href="${baseUrl}financeiro/contas-a-pagar/${transaction.payment}" target="_blank"
                class="text-white btn btn-primary btn-sm  my-1" title="Visualizar"> <i class="fa fa-search"></i></a>`;
            accountedStatus = `<a href="${baseUrl}contabilidade/pagamento/${transaction.payment}" target="_blank"
                class="text-white btn ${btnAccountedStatus} btn-sm  my-1" title="Visualizar"> <i class="fa fa-search"></i> ${accountedStatus}</a>`;
        }
    } else {
        classe = 'text-success';
        balance = parseFloat(balance) + parseFloat(transaction.value);
    }


    let html = `<tr id="line${transaction.id}" class="middle">
                    <td class="text-center" style="white-space: nowrap">${formatDate(transaction.date)}</td>
                    <td class="text-center">${transaction.type}</td>
                    <td class="text-center">${transaction.uniqueIdentifier}</td>                    
                    <td class="text-center">${transaction.extractNumber}</td>       
                    <td class="text-center">${transaction.description}</td>             
                    <td class="text-center">${button}</td>             
                    <td class="text-center ${classe}">${accountedStatus}</td>             
                    <td class="text-center ${classe}" style="white-space: nowrap"><b>${pre}${floatToMoney(transaction.value)}</b></td>
                    <td class="text-center" style="white-space: nowrap">${floatToMoney(balance)}</td>
                </tr>`;


    return html;
}

function getDate(date) {
    const mounths = [
        'Jan', 'Fev', 'Mar', 'Abr',
        'Mai', 'Jun', 'Jul', 'Ago',
        'Set', 'Out', 'Nov', 'Dez',
    ];

    const arr = date.split('-');

    return `${arr[2]} ${mounths[parseInt(arr[1]) - 1]} ${arr[0]}`;
}

function generateTable() {
    $("#table tbody").empty();
    $('.loaderTable').css('opacity', 1);
    fetch(`${baseUrl}api/financeiro/conciliacao/extrato/json/?index=${index}&directory=${directory}&account=${account}&start=${start}&end=${end}&conciliate=${conciliate}&accounted=${accounted}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        $('.loaderTable').css('opacity', 0);
        response.json().then(json => {
            if (response.status === 200) {
                $('#startDateBalance').html(`<b>(${json.startDateBalance})</b>`);
                $('#startBalance').html(`<b>${floatToMoney(json.startBalance)}</b>`);
                $('#totalEntries').html(`<b>${floatToMoney(json.totalEntries)}</b>`);
                $('#totalExits').html(`<b>${floatToMoney(json.totalExits)}</b>`);
                $('#endBalance').html(`<b>${floatToMoney(json.endBalance)}</b>`);
                total = json.total;
                partial = json.partial;
                balance = json.startBalance;
                if (json.message.length > 0) {
                    let options = `<tr><td class="text-center">${json.startDateBalance}</td><td colspan="7"></td><td class="text-center">${floatToMoney(json.startBalance)}</td></tr>`
                    options += json.message.map(generateLines);
                    $("#table tbody").append(options);
                } else {
                    $("#table tbody").append(`<tr><td colspan="9" class="text-center">Nenhum resultado encontrado</td></tr>`);
                }
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });

}

function setDateStart(value) {
    var date = $('#directoryAccount option[value="' + value + '"]').attr('startDate');
    $('#startFilter').val(date);
}

function getAccount() {
    const directory = $('#directoryFilter').val();
    $('#startFilter').val('');
    fetch(baseUrl + `api/financeiro/contas-por-diretorio/?directory=${directory}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {

            if (json.message.length === 0) {
                showNotify('danger', 'Não existe conta vinculada a este Diretório e Origem de Despesa!');
            } else {

                let options = `<option value="" startDate=''>Selecionar</option>`

                options += json.message.map(getOptionsAccount);

                $('#directoryAccount').html(options);
            }
        });
    });
}

function getOptionsAccount(option) {

    return `<option value="${option.id}" startDate="${option.startDate}">${option.name}</option>`;
}


$('.loaderTable').css('opacity', 0);
