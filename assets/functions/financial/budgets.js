function openModal(id) {
    form.reset();
    $('#budgetId').val(id);
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form')
        fetch(baseUrl + 'api/financeiro/orcamentos/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0];
                console.log(json)
                getCostCenter('costCenter', json.directory, json.costCenter);
                getArea('area', json.directory, json.area);
                getSubArea(json.area, json.subArea);
                $('#directory').val(json.directory);
                $('#total').val(formatMoney(json.data.total));
                $('#year').val(json.data.year);

                json.data.values.forEach(element => {
                    document.getElementById('mon' + element.month).value = formatMoney(element.value);
                })

                closeLoading('form');
            })
        })
    }
}

function getCostCenter(select, value, cost) {
    const directory = value ? value : $('#directory').val();
    const result = select ? $('#' + select) : $('#costCenter');
    fetch(baseUrl + 'api/financeiro/centro-de-custo/diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`

            options += json.message.map(getOptions);

            result.html(options).val(cost ?? '')
        });
    });
}

function getArea(select, value, area) {
    const directory = value ? value : $('#directory').val();
    const result = select ? $('#' + select) : $('#area');
    fetch(baseUrl + 'api/financeiro/area-por-diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`

            options += json.message.map(getOptions);

            result.html(options).val(area ?? '')
        });
    });
}

function getSubArea(value, subArea) {
    const area = value ? value : $('#area').val();
    console.log('subArea: ', subArea)
    fetch(baseUrl + 'api/financeiro/sub-areas-por-area/' + area, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`

            options += json.message.map(getOptions);

            $('#subArea').html(options).val(subArea ?? '')
        });
    });
}

function getOptions(option) {

    return `<option value="${option.id}">${option.name}</option>`;
}

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    fetch(baseUrl + 'api/financeiro/orcamentos/cadastrar', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(form)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Budgets');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function generateLines(budget) {
    const session = JSON.parse(sessionStorage.getItem('Budgets'));
    let id = budget.id;
    if (budget.budget) id = budget.budget;

    let actions = '';
    if (canUpdate) actions = `<a onclick="openModal(${budget.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    if (canDelete) actions += `<i class="fa fa-trash text-danger px-1" title="Excluir" onclick="delElement('financeiro/orcamentos', 'Contrato', ${budget.id}, 'delete', 'Budgets')"></i>`;

    if (session.variables.groupBy == 0) {
        const cols = ['Código', 'Diretório', 'Centro de Custo', 'Área', 'Ano', 'Orçado', 'Realizado', 'Aprovisionado', 'Ações'];
        getCols(cols);

        return `<tr class="middle" id="line${budget.id}">
                    <td class="text-center">${getCode(budget.directoryId, budget.areaId, budget.costCenterId)} </td>
                    <td class="text-center">${budget.directoryName} </td>
                    <td class="text-center">${budget.costCenterName} </td>
                    <td class="text-center">${budget.area} </td>                    
                    <td class="text-center">${budget.year} </td>
                     <td class="text-center">${formatMoney(budget.total)} </td>
                    <td class="text-center">${formatMoney(budget.realized || 0)}</td>
                    <td class="text-center">${formatMoney(budget.provisioned || 0)}</td>
                    <td class="text-center">${actions}</td>
                </tr>`;
    } else if (session.variables.groupBy == 1) {
        const cols = ['Diretório', 'Centro de Custo', 'Ano', 'Orçado', 'Realizado', 'Aprovisionado'];
        getCols(cols);

        return `<tr class="middle">
                    <td class="text-center">${budget.directoryName} </td>
                    <td class="text-center">${budget.costCenterName} </td>              
                    <td class="text-center">${budget.year} </td>
                    <td class="text-center">${formatMoney(budget.total)} </td>
                    <td class="text-center">${formatMoney(budget.realized || 0)}</td>
                    <td class="text-center">${formatMoney(budget.provisioned || 0)}</td>
                </tr>`;
    } else if (session.variables.groupBy == 2) {
        const cols = ['Diretório', 'Ano', 'Orçado', 'Realizado', 'Aprovisionado'];
        getCols(cols);

        return `<tr class="middle">
                    <td class="text-center">${budget.directoryName} </td>            
                    <td class="text-center">${budget.year} </td>
                    <td class="text-center">${formatMoney(budget.total)} </td>
                    <td class="text-center">${formatMoney(budget.realized || 0)}</td>
                    <td class="text-center">${formatMoney(budget.provisioned || 0)}</td>
                </tr>`;
    }
}

function getCols(cols) {
    let th = `<tr class="middle">`;

    cols.forEach(col => {
        th += `<th class="text-center">${col}</th>`;
    })

    th += `</tr>`;

    $('#tableBudgets thead').html(th);
}

function getCode(directory, area, costCenter) {
    if (area) {
        const newDirectory = String(directory).padStart(3, '0');
        const newArea = String(area).padStart(3, '0');
        const newCostCenter = String(costCenter).padStart(3, '0');
        return newDirectory + '.' + newCostCenter + '.' + newArea;
    } else {
        return 'Desconhecido';
    }
}

const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Budgets');
})

function setTotal() {
    const inputs = $('#form .money');

    let total = 0;
    inputs.each(function () {

        var value = $(this).val().replace('R$', '')
            .replaceAll('.', '')
            .replaceAll(',', '.');

        value = parseFloat(value.trim());

        if (!isNaN(value)) total += value;
    });

    $('#total').val(formatMoney(total));
}

function resetTableBudgets() {
    const value = document.getElementById('groupByBudgetsFilter').value;
    setStorageIndex(value);
}

function filterBudgets() {
    const formData = formDataToJson('filter');
    const arr = {
        'url': 'api/financeiro/orcamentos-por-centro-de-custo',
        'variables': {
            'area': formData.area,
            'directory': formData.directory,
            'year': '2024',
        }
    };

    setStorage('Budgets', {
        'url': 'api/financeiro/orcamentos',
        'columns': 11,
        'variables': {
            'index': 0,
            'costCenter': formData.costCenter,
            'area': formData.area,
            'directory': formData.directory,
            'year': formData.year,
            'groupBy': document.getElementById('groupByBudgetsFilter').value,
            'order': 'id',
            'seq': 'desc',
            'limit': document.getElementById('limitBudgets').value
        }
    })
}

$(document).ready(function () {
    verifySession('Budgets');
    $(".money").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false,
        allowZero: true,
    });
});

function setStorageIndex(value) {
    setStorage('Budgets', {
        'url': 'api/financeiro/orcamentos',
        'columns': 11,
        'variables': {
            'index': 0,
            'costCenter': '',
            'area': '',
            'directory': '',
            'groupBy': '0',
            'year': '2024',
            'order': 'id',
            'seq': 'desc',
            'limit': 25
        }
    })

    // if (value) {
    //     setTimeout(() => {
    //         document.getElementById('groupByBudgetsFilter').value = value;
    //     }, 250)
    // }
}
