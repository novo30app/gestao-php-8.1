var index = 0;
var partial = 0;
var total = 1;
var reduced = surname = account = description = type = category = specie = '';

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    reduced = formData.reduced;
    surname = formData.surname;
    account = formData.account;
    description = formData.description;
    type = formData.type;
    category = formData.category;
    specie = formData.specie;
    $("#table tbody").empty();
    generateTable();
})

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    reduced = surname = account = description = type = category = specie = '';
    $("#table tbody").empty();
    generateTable();
}

function generateList(accounts) {
    let actions = '';
    if (accounts.active == 1) {
        if (canDelete) actions = `<a href="#" onclick="change(${accounts.id}, 0)" class="text-dark"><i class="fa fa-times" title="Inativar" ></i> Desativar </a><br> `;
        if (canUpdate) actions += `<a href="#" onclick="openModal(${accounts.id})"><i class="fa fa-pencil text-info" title="Editar"></i> Editar </a>`;
    } else {
        if (canUpdate) actions = `<a href="#" onclick="change(${accounts.id}, 1)"><i class="fa fa-refresh text-success" title="Ativar"></i> Ativar </a> `;
    }
    actions = `<td class="text-center">${actions}</td>`;
    return `<tr class="middle">
                    <td class="text-center">${accounts.reduced}</td>
                    <td class="text-center">${accounts.surname}</td>
                    <td class="text-center">${accounts.account}</td>
                    <td class="text-center">${accounts.description}</td>
                    <td class="text-center">${accounts.type}</td>
                    <td class="text-center">${accounts.category}</td>
                    <td class="text-center">${accounts.created_at}</td>
                    ${actions}
                </tr>`;
}

function openModal(id) {
    document.getElementById('form').reset();
    document.getElementById('accountId').value = id;
    document.getElementById('reducedModal').value = "";
    document.getElementById('surnameModal').value = "";
    document.getElementById('typeModal').value = "";
    document.getElementById('accountModal').value = "";
    document.getElementById('categoryModal').value = "";
    document.getElementById('descriptionModal').value = "";
    $('#modal').modal('show');
    $('#message').hide();
    showLoading();
    fetch(`${baseUrl}api/financeiro/consulta-conta/${id}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            json = json.message;
            closeLoading();
            document.getElementById('accountId').value = id;
            document.getElementById('reducedModal').value = json.reduced;
            document.getElementById('surnameModal').value = json.surname;
            document.getElementById('typeModal').value = json.type;
            document.getElementById('accountModal').value = json.account;
            document.getElementById('categoryModal').value = json.category;
            document.getElementById('descriptionModal').value = json.description;
        });
    });
}

const divMessage = document.getElementById('message');

function change(id, status) {
    showLoading();
    fetch(`${baseUrl}api/financeiro/altera-conta/` + id + `/` + status, {
        method: "DELETE",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        closeLoading();
        divMessage.classList.add("alert-danger");
        divMessage.classList.remove("alert-success");
        response.json().then(json => {
            divMessage.innerHTML = json.message;
            divMessage.style.display = 'block';
            if (response.status === 201) {
                divMessage.classList.add("alert-success");
                divMessage.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        });
    });
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/financeiro/plano-de-contas/?index=${index}&reduced=${reduced}&surname=${surname}&account=${account}&description=${description}&type=${type}&category=${category}&specie=${specie}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

const formModal = document.getElementById('form');
const divMessageForm = document.getElementById('messageForm');
formModal.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('form');
    fetch(`${baseUrl}financeiro/salva-conta`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            divMessageForm.innerHTML = json.message;
            divMessageForm.style.display = 'block';
            if (response.status === 201) {
                $('#save').addClass('disabled');
                divMessageForm.classList.add("alert-success");
                divMessageForm.classList.remove("alert-danger");
                formModal.reset();
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        });
    });
});

function csv() {
    let formData = formDataToJson('filter');
    reduced = formData.reduced;
    surname = formData.surname;
    account = formData.account;
    description = formData.description;
    type = formData.type;
    category = formData.category;
    window.open(
        `${baseUrl}api/financeiro/exportCsvAccounts/?reduced=${reduced}&surname=${surname}&account=${account}&description=${description}&type=${type}&category=${category}&specie=${specie}`,
        '_blank');
}

function importAccount() {
    $('#modalImport').modal('show');
}

const divMessageImport = document.getElementById('messageImport');
const formImport = document.getElementById('formImport');
formImport.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}financeiro/importa-contas/`, {
        method: 'POST',
        credentials: "same-origin",
        headers: {
            'Accept': 'application/json'
        },
        body: new FormData(formImport),
    }).then(response => {
        closeLoading();
        divMessageImport.classList.add("alert-danger");
        divMessageImport.classList.remove("alert-success");
        response.json().then(json => {
            divMessageImport.innerHTML = json.message;
            divMessageImport.style.display = 'block';
            if (response.status === 201) {
                divMessageImport.classList.add("alert-success");
                divMessageImport.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        });
    });
});

function changeModal(action) {
    $('#modal').modal(action);
}

$(document).ready(function () {
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});