function originString(origin) {
    switch (parseInt(origin)) {
        case 1:
            return 'OR CAMPANHA ELEITORAL - DOAÇÕES PRIVADAS';
        case 2:
            return 'FP - POLÍTICA DA MULHER';
        case 3:
            return 'OR - OUTROS RECURSOS - DOAÇÕES PRIVADAS';
        case 4:
            return 'FP - FUNDO PARTIDÁRIO';
        case 5:
            return 'FP CAMPANHA ELEITORAL - FUNDO PARTIDÁRIO PARA CAMPANHA ELEITORAL'
        case 6:
            return 'FEFC GERAL'
        case 7:
            return 'FEFC MULHER'
        case 8:
            return 'FEFC MULHER NEGRA'
        case 9:
            return 'FEFC HOMEM NEGRO'
        case 10:
            return 'FP POLÍTICA DA MULHER CAMPANHA ELEITORAL'
        case 11:
            return 'FEFC INDÍGENA'
        default:
            return 'Desconhecido';
    }
}

function statusString(origin, ofx) {
    switch (parseInt(origin)) {
        case 0:
            requestStatus = "Análise";
            break;
        case 1:
            requestStatus = "Pendência";
            break;
        case 2:
            requestStatus = "Agendado";
            break;
        case 3:
            requestStatus = "Efetuado";
            break;
        case 4:
            requestStatus = "Aguardando Aprovação";
            break;
        case 5:
            requestStatus = "Cancelado";
            break;
        case 6:
            requestStatus = "Análise OK";
            break;
        case 7:
            requestStatus = "Conciliado";
            break;
        case 8:
            requestStatus = "Estornado";
            break;
        case 9:
            requestStatus = "Estimável";
            break;
        default:
            requestStatus = "Desconhecido";
    }
    return requestStatus;
}

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}

function generateLines(paymentRequirements) {

    let attachmentNf = attachmentBillet = attachmentContract = attachmentPayment = '';
    var edit = type;
    if (paymentRequirements.attachmentNf && paymentRequirements.attachmentNf != '') {
        attachmentNf =
            ` <a class="btn btn-sm btn-success my-1" 
                href="${formatAttachment(paymentRequirements.attachmentNf)}" target="_blank">NF</a><br> `;
    }
    if (paymentRequirements.attachmentBillet && paymentRequirements.attachmentBillet != '') {
        attachmentBillet =
            ` <a class="btn btn-sm btn-primary my-1" 
                href="${formatAttachment(paymentRequirements.attachmentBillet)}" target="_blank">Boleto</a><br> `;
    }
    if (paymentRequirements.attachmentContract && paymentRequirements.attachmentContract != '') {
        attachmentContract =
            ` <a class="btn btn-sm btn-secondary my-1" 
                href="${formatAttachment(paymentRequirements.attachmentContract)}" target="_blank">Contrato</a><br> `;
    }
    if (paymentRequirements.attachmentPayment != '' && (paymentRequirements.requestStatus === 3 || paymentRequirements.requestStatus === 7)) {
        attachmentPayment =
            ` <a class="btn btn-sm btn-warning text-white my-1" 
                href="${formatAttachment(paymentRequirements.attachmentPayment)}" target="_blank">Comprovante</a><br> `;
    }
    let viewPayment = ` <a class="btn btn-primary text-end my-1" target="_blank" 
        href="${baseUrl}financeiro/contas-a-pagar/${paymentRequirements.id}" title="Visualizar">
            <i class="fa fa-search" aria-hidden="true"></i></a><br>`;

    if (type == 5) viewPayment = ` <a class="btn btn-primary text-end my-1" target="_blank" 
        href="${baseUrl}financeiro/contas-a-pagar/${paymentRequirements.id}/1" title="Visualizar">
            <i class="fa fa-search" aria-hidden="true"></i></a><br>`;

    const types = [2, 3, 4];
    if (paymentRequirements.user === currentUser.name || types.includes(edit)) {
        edit = `<a class="btn btn-success text-end my-1" target="_blank" 
            href="${baseUrl}financeiro/pagamentos/${paymentRequirements.id}" title="Editar">
                <i class="fa fa-pencil" aria-hidden="true"></i></a><br>`;
    } else {
        edit = '';
    }

    const types2 = [18];
    if (!types2.includes(paymentRequirements.type) && type != 5) {
        duplicate = ` <a class="btn btn-danger text-end my-1" target="_blank" 
            href="${baseUrl}financeiro/pagamentos/duplicar/${paymentRequirements.id}" title="Duplicar">
                <i class="fa fa-clone" aria-hidden="true"></i></a>`;
    } else {
        duplicate = '';
    }

    let accounting = '';
    if (accountingPermission && type != 5) {
        accounting = `<a class="btn btn-warning text-end my-1" target="_blank" 
            href="${baseUrl}contabilidade/pagamento/${paymentRequirements.id}" title="Contabilidade">
                <i class="fa fa-calculator" aria-hidden="true"></i></a>`;
    }

    let beneficiary = paymentRequirements.prName;
    if (beneficiary == '') beneficiary = paymentRequirements.beneficiary;

    let cpfCnpj = paymentRequirements.cpfCnpj;
    if (cpfCnpj == '') cpfCnpj = paymentRequirements.cpf ?? '';
    
    var cpfCnpjFormated = cpfCnpj ? cpfCnpj.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4") : '';
    if (cpfCnpj && cpfCnpj.length > 11) var cpfCnpjFormated = cpfCnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1$2$3/$4-$5");

    let bbStatus = "-";
    if(paymentRequirements.requestStatus != 3 && [21, 23, 44, 60].includes(userId)) {
        if([21, 23, 44, 60].includes(userId)) {
            if(paymentRequirements.statusString === 'PAGO') 
                bbStatus = `<button class="btn btn-success">${paymentRequirements.statusString}</button>`;
        
            if(paymentRequirements.statusString === 'DEBITADO') 
                bbStatus = `<button class="btn btn-success">${paymentRequirements.statusString}</button>`;

            if(paymentRequirements.statusString === 'PENDENTE') 
                bbStatus = `<button class="btn btn-secondary">${paymentRequirements.statusString}</button>`;
            
            if(paymentRequirements.statusString === 'CONSISTENTE') 
                bbStatus = `<button class="btn btn-warning">${paymentRequirements.statusString} </button>`;
        
            if(paymentRequirements.statusString === 'AGENDADO') 
                bbStatus = `<button class="btn btn-success">${paymentRequirements.statusString} </button>`;

            const errBB = ['ERRO', 'INCONSISTENTE', 'CANCELADO', 'REJEITADO', 'DEVOLVIDO', 'BLOQUEADO'];
            if(errBB.includes(paymentRequirements.statusString)) 
                bbStatus = `<button class="btn btn-danger">${paymentRequirements.statusString}</button>`;
        }
    }
    bbStatus = `<td class="text-center">${bbStatus}</td>`

    return `<tr class="middle">
                    <td class="text-center">${paymentRequirements.id}</td>
                    <td class="text-center">${paymentRequirements.created}</td>
                    <td class="text-center">${paymentRequirements.paymentValue}</td>
                    <td class="text-center">${paymentRequirements.dueDate}</td>
                    <td class="text-center">${paymentRequirements.costCenter}</td>
                    <td class="text-center">${paymentRequirements.directory}</td>
                    <td class="text-center">${paymentRequirements.user}</td>
                    <td class="text-center">${originString(paymentRequirements.origin)}</td>
                    <td class="text-center">${beneficiary ? beneficiary.toUpperCase() : ''}</td>
                    <td class="text-center">${cpfCnpjFormated}</td>
                    <td class="text-center">${statusString(paymentRequirements.requestStatus, paymentRequirements.ofxFiles)}</td>
                    ${bbStatus}
                    <td class="text-center">${attachmentNf}
                                            ${attachmentBillet}
                                            ${attachmentContract}
                                            ${attachmentPayment}
                    </td>
                    <td class="text-center">        
                    ${viewPayment}            
                            ${edit}                            
                            ${duplicate}
                            ${accounting}
                    </td>
                </tr>`;
}

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable(sessions);
})
