function statusPayment() {
    if (document.getElementById("divPendency")) document.getElementById("divPendency").style.display = 'none';
    if (document.getElementById("divPayDay")) document.getElementById("divPayDay").style.display = 'none';
    if (document.getElementById("divScheduledDate")) document.getElementById("divScheduledDate").style.display = 'none';
    if (document.getElementById("apportionmentDiv")) document.getElementById("apportionmentDiv").style.display = 'none';
    $('#destinyType, #calcType, #payDay, #directoryAccount, #divRefund input').prop('required', false);
    $("#buttonDiv").show();
    $("#divRefund, #divPendency").hide();
    $('#apportionmentDiv select, #apportionmentDiv input').prop('readonly', false).prop('disabled', false).filter('.input-value').prop({'readonly': true, 'disabled': true});
    if (document.getElementById("requestStatus").value == '1') {
        document.getElementById("divPendency").style.display = 'block';
    } else if (document.getElementById("requestStatus").value == '2') {
        document.getElementById("divScheduledDate").style.display = 'block';
    } else if (document.getElementById("requestStatus").value == '3') {
        getDirectotyAccount2();
        $('#destinyType, #calcType, #payDay, #directoryAccount').prop('required', true);
        document.getElementById("divPayDay").style.display = 'block';
        document.getElementById("apportionmentDiv").style.display = 'block';
        document.getElementById("divPendency").style.display = 'block';
    } else if (document.getElementById("requestStatus").value == '7') {
        getDirectotyAccount2();
        $('#destinyType, #calcType, #payDay, #directoryAccount').prop('required', true);
        document.getElementById("divPayDay").style.display = 'block';
        document.getElementById("apportionmentDiv").style.display = 'block';
    } else if (document.getElementById("requestStatus").value == '8') {
        getDirectotyAccount2();
        $('#destinyType, #calcType, #payDay, #directoryAccount, #divRefund input').prop('required', true);
        $('#divRefund').show(300);
        document.getElementById("divPayDay").style.display = 'block';
        document.getElementById("apportionmentDiv").style.display = 'block';
        document.getElementById("divPendency").style.display = 'block';
    }
}

function getDirectotyAccount2() {
    const directory = $('#directory').val();
    const origin = $('#origin').val();
    fetch(baseUrl + `api/financeiro/contas-por-diretorio-superior/?directory=${directory}&origin=${origin}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            if (json.message.length === 0) {
                showNotify('danger', 'Não existe conta vinculada a este Diretório e Origem de Despesa!');
            } else {
                let options = `<option value="">Selecionar</option>`
                options += json.message.map(getOptions);
                $('#directoryAccount').html(options).val(account);
            }
        });
    });
}

window.onload = function () {
    if (document.getElementById("requestStatus")) statusPayment();
}

$(document).ready(function () {
    getCalcTotal();
    $("#accountingValue, #valueAccountingDebit, #valueAccountingCredit, #valueAccountingDebit2, #valueAccountingCredit2, #penalty, #fees, #discounts, #inss, #irrf, #csll, #cofins, #pispasep, #iss").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    if (type == 18) {
        // document.getElementById("detailsDiv").style.display = 'none';
        document.getElementById("paymentDiv").style.display = 'none';
        // document.getElementById("expensesDiv").style.display = 'none';
        document.getElementById("FilesDiv").style.display = 'none';
        document.getElementById("GRUDiv").style.display = 'block';
    } else if (type == 19) {
        document.getElementById("paymentDiv").style.display = 'none';
    } else if(type == 16) {
        $('#costCenters').prop('required', false);
    } else if(type == 22) {
        $('#costCenters').prop('required', false);
        document.getElementById("expensesDiv").style.display = 'none';
    }
    if (costCentertId) getCostCenter();
    if (contractId) verifyCpfCnpj('providerDoc');
    $(".money").maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', allowZero: true});
    $(".percent").maskMoney({thousands: '.', decimal: ',', allowZero: true});
    statusPayment();
});

function getCostCenter() {
    const directory = $('#directory').val();
    fetch(baseUrl + 'api/financeiro/centro-de-custo/diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`

            options += json.message.map(getOptions);

            $('#costCenter, .cost-center').html(options).prop('required', false).val(costCentertId);

            array.forEach((element, index) => {
                $("#cost-center" + index).html(options).val(element);
            })
        });
    });
}

function getOptions(option) {

    return `<option value="${option.id}">${option.name}</option>`;
}

function verifyCpfCnpj(id) {
    let value = document.getElementById(id).value.replace(/\D/g, "");
    let directory = document.getElementById('directory').value;
    document.getElementById(id).value = maskCpfCnpj(value);
    if (validateCpfCnpj(value)) {
        fetch(baseUrl + `api/sistema/fornecedores/${value}/${directory}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                let options = `<option value="">Selecionar</option>`;
                options += json.contracts.map(generateOptions);
                $('#contract').html(options).val(contractId);
                if (contractId){
                    document.getElementById('contractLink').innerHTML = `<a class="badge badge-info" target="_blank" href="${baseUrl}financeiro/contratos/${contractId}">Visualizar</a>`;
                }
            });
        });
    }
}

function generateOptions(option) {

    return `<option value="${option.id}" datatype="${option.situation}" data-action="${option.stage}">${option.number} - ${option.name}</option>`;
}

if (document.getElementById('formAccounting')) {
    const formAccounting = document.getElementById('formAccounting');
    const divMessageFormAccounting = document.getElementById('divMessageFormAccounting');
    formAccounting.addEventListener('submit', e => {
        $('#saveAccounting').addClass('disabled');
        e.preventDefault();
        showLoading();
        let formAccounting = formDataToJson('formAccounting');
        fetch(`${baseUrl}financeiro/registro-contabil/`, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formAccounting)
        }).then(response => {
            closeLoading();
            divMessageFormAccounting.classList.add("alert-danger");
            divMessageFormAccounting.classList.remove("alert-success");
            $('#saveAccounting').removeClass('disabled');
            response.json().then(json => {
                divMessageFormAccounting.innerHTML = json.message;
                $('#divMessageFormAccounting').removeClass('d-none');
                if (response.status === 201) {
                    $('#saveAccounting').addClass('disabled');
                    divMessageFormAccounting.classList.add("alert-success");
                    divMessageFormAccounting.classList.remove("alert-danger");
                    if (json.validCompetitionDate == true) $('#formAccounting2Div').removeClass('d-none');
                }
            });
        });
    });
}

if (document.getElementById('formAccounting2')) {
    const formAccounting2 = document.getElementById('formAccounting2');
    const divMessageFormAccounting2 = document.getElementById('divMessageFormAccounting2');
    formAccounting2.addEventListener('submit', e => {
        //$('#saveAccounting2').addClass('disabled');
        e.preventDefault();
        showLoading();
        let formAccounting2 = formDataToJson('formAccounting2');
        fetch(`${baseUrl}financeiro/registro-contabil/`, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formAccounting2)
        }).then(response => {
            closeLoading();
            divMessageFormAccounting2.classList.add("alert-danger");
            divMessageFormAccounting2.classList.remove("alert-success");
            $('#saveAccounting2').removeClass('disabled');
            response.json().then(json => {
                divMessageFormAccounting2.innerHTML = json.message;
                $('#divMessageFormAccounting2').removeClass('d-none');
                if (response.status === 201) {
                    //$('#saveAccounting2').addClass('disabled');
                    divMessageFormAccounting2.classList.add("alert-success");
                    divMessageFormAccounting2.classList.remove("alert-danger");
                }
            });
        });
    });
}

let accountsDebit = 0;
let accountsDebit2 = 0;

function addAccountDebitDiv() {
    account = $("#accountingDebit").val();
    valueAccount = $("#valueAccountingDebit").val();
    if (account == '' || valueAccount == '') {
        alert('Conta ou Valor Vazio!');
        return;
    }
    let html = `<div class="btn btn-success" style="border: 1px black solid; border-radius: 5px; margin: 10px; text-align: left">
                    Conta: ${account} <i class="fa fa-times text-danger" onclick="removeAccountDebit(this)"></i><br> 
                    Valor: R$ ${valueAccount}<br>
                    <input class="m-input" type="hidden" id="accountsDebit" name="accountsDebit[]" value="${account} | ${valueAccount}">
                </div>`;
    $("#accountingDebitDiv").append(html);
    accountsDebit++;
    $('#accountingDebit').val('');
    $('#valueAccountingDebit').val('');
}

function addAccountDebitDiv2() {
    account = $("#accountingDebit2").val();
    valueAccount = $("#valueAccountingDebit2").val();
    if (account == '' || valueAccount == '') {
        alert('Conta ou Valor Vazio!');
        return;
    }
    let html = `<div class="btn btn-success" style="border: 1px black solid; border-radius: 5px; margin: 10px; text-align: left">
                    Conta: ${account} <i class="fa fa-times text-danger" onclick="removeAccountDebit(this)"></i><br> 
                    Valor: R$ ${valueAccount}<br>
                    <input class="m-input" type="hidden" id="accountsDebit" name="accountsDebit[]" value="${account} | ${valueAccount}">
                </div>`;
    $("#accountingDebitDiv2").append(html);
    accountsDebit++;
    $('#accountingDebit2').val('');
    $('#valueAccountingDebit2').val('');
}

function removeAccountDebit(i) {
    accountsDebit--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

let accountsCredit = 0;
let accountsCredit2 = 0;

function addAccountCreditDiv(id, account) {
    account = $("#accountingCredit").val();
    valueAccount = $("#valueAccountingCredit").val();
    if (account == '' || valueAccount == '') {
        alert('Conta ou Valor Vazio!');
        return;
    }
    let html = `<div class="btn btn-success" style="border: 1px black solid; border-radius: 5px; margin: 10px; text-align: left">
                    Conta: ${account} <i class="fa fa-times text-danger" onclick="removeAccountCredit(this)"></i><br> 
                    Valor: R$ ${valueAccount}<br>
                    <input class="m-input" type="hidden" id="accountsCredit" name="accountsCredit[]" value="${account} | ${valueAccount}">
                </div>`;
    $("#accountingCreditDiv").append(html);
    accountsCredit++;
    $('#accountingCredit').val('');
    $('#valueAccountingCredit').val('');
}

function addAccountCreditDiv2(id, account) {
    account = $("#accountingCredit2").val();
    valueAccount = $("#valueAccountingCredit2").val();
    if (account == '' || valueAccount == '') {
        alert('Conta ou Valor Vazio!');
        return;
    }
    let html = `<div class="btn btn-success" style="border: 1px black solid; border-radius: 5px; margin: 10px; text-align: left">
                    Conta: ${account} <i class="fa fa-times text-danger" onclick="removeAccountCredit(this)"></i><br> 
                    Valor: R$ ${valueAccount}<br>
                    <input class="m-input" type="hidden" id="accountsCredit" name="accountsCredit[]" value="${account} | ${valueAccount}">
                </div>`;
    $("#accountingCreditDiv2").append(html);
    accountsCredit++;
    $('#accountingCredit2').val('');
    $('#valueAccountingCredit2').val('');
}

function removeAccountCredit(i) {
    accountsCredit--;
    ((i.parentNode).parentNode).removeChild(i.parentNode);
}

function originatingTransaction(requirement) {
    window.open(
        `${baseUrl}api/financeiro/exporta-originarios/?requirement=${requirement}`,
        '_blank');
}

var formAdd = document.getElementById('submitStamp');
$('#submitStamp').submit(function (e) {
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}api/financeiro/aprovacao/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function emptyValues() {
    $("#insertValues").empty();
}

var values = 0;

function addValues() {
    const destiny = $('#destinyType').val();
    const calc = $('#calcType').val();

    if (destiny === '' || calc === '') {
        showNotify('danger', 'Selecione a Forma de Distribuição e a Forma de Cálculo!', 1500);
        return;
    }

    let options = '';
    let select = '';
    let inputs = '';
    if (destiny === '1') {
        options = $("#directory").html();
        options = options.replaceAll('selected=""', '');
        select = `
            <select class="form-select"  name="directories[]" required>
                ${options}
            </select>
        `;
    } else {
        options = $("#costCenter").html();
        options = options.replaceAll('selected=""', '');
        select = `
            <select class="form-select" name="costCenters[]" required>
                ${options}
            </select>
        `;
    }

    if (calc === '1') {
        inputs = `
            <div class="col-md-3 mb-2">
                <div class="input-group">
                    <input name="percentage[]" placeholder="Porcentagem" type="text" class="form-control percent" 
                            height="35px" required onkeyup="calcValue(this.value, 'valuePercent${values}')">
                    <span class="input-group-text" style="height: 35px">%</span>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                    <input readonly placeholder="R$ 0,00" type="text" class="form-control" id="valuePercent${values}">  
            </div>
        `
    } else {
        let value = moneyToFloat($("#totalValue").val());
        let total = 0;

        document.querySelectorAll('#insertValues .money').forEach(el => {
            total += moneyToFloat(el.value);
        })

        value -= total;
        value = formatMoney(value);

        inputs = `
            <div class="col-md-4 mb-2">
                <input placeholder="R$ 0,00" type="text" value="${value}" class="form-control money" name="values[]" required>
            </div>
        `
    }

    const html = `
        <div class="row" id="addValues${values}">
            <div class="col-md-4 mb-2">
                ${select}
            </div>
            ${inputs}
            <div class="col-md-2 my-auto">
                <i class="fa fa-trash text-danger" onclick="removeDiv('addValues${values}')"></i>
            </div>
        </div>  
    `;

    $("#insertValues").append(html);
    values++;
    $(".money").maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', allowZero: true});
    $(".percent").maskMoney({thousands: '.', decimal: ',', allowZero: true});
}

function calcValue(percent, id) {
    let value = moneyToFloat($("#totalValue").val());
    percent = percent.replaceAll('.', '');
    percent = parseFloat(percent.replace(',', '.'));

    const money = formatMoney(value * (percent / 100))

    $("#" + id).val(money);
}

function verifyTotal() {
    const calc = $('#calcType').val();
    document.getElementById('totalValue').classList.remove('is-invalid')
    let nf = moneyToFloat($("#totalValue").val());
    let total = 0;
    let inputs = '';
    if (calc === '1') {
        inputs = document.querySelectorAll(".percent");
    } else {
        inputs = document.querySelectorAll(".money");
    }

    inputs.forEach(element => {
        let value = element.value;
        value = value.replaceAll('R$ ', '');
        value = value.replaceAll('.', '');
        value = parseFloat(value.replace(',', '.'))
        total += isNaN(value) ? 0 : value;
    })
    total = total.toFixed(2);

    if (calc === '1' && total !== 100) {
        showNotify('danger', 'A Porcentagem total deve ser iagual a 100%, corrija os valores!', 1500);
        return false;
    } else if (calc === '0' && parseFloat(total) !== nf) {
        showNotify('danger', 'A Soma dos valores não é igual ao Valor Total, corrija os valores!', 1500);
        document.getElementById('totalValue').classList.add('is-invalid')
        return false;
    } else {
        return true;
    }
}

function getCalcTotal() {
    const total = moneyToFloat(document.getElementById('nfValue').value);
    const penalty = moneyToFloat(document.getElementById('penalty').value);
    const fees = moneyToFloat(document.getElementById('fees').value);
    const discounts = moneyToFloat(document.getElementById('discounts').value);

    const value = total + penalty + fees - discounts;

    console.log(value)

    document.getElementById('totalValue').value = formatMoney(value);
}

var formBBModal = document.getElementById('formBBModal');
$('#formBBModal').submit(function (e) {
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}api/financeiro/envia-pagamento-bb/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formBBModal)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function liberation(id) {
    let text = "Tem certeza que liberar?";
    if (confirm(text) == false) return;

    showLoading();
    fetch(`${baseUrl}api/financeiro/libera-pagamento-bb/${id}`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formBBModal)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
};

function cancel(id) {
    let text = "Tem certeza que deseja cancelar?";
    if (confirm(text) == false) return;

    showLoading();
    fetch(`${baseUrl}api/financeiro/cancela-pagamento-bb/${id}`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formBBModal)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.reload();
                }, 2000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
};

const sendBankModalMessageForm = document.getElementById('sendBankModalMessageForm');
function modalBank(id) {
    $("#scheduleDate").val(scheduledDate);
    $('#digitableLineInput').addClass('d-none');
    if($("#paymentMethod").val() == 1) $('#digitableLineInput').removeClass('d-none');
    checksDoublePayment(id);
    $('#modal').modal('show');
}

function checksDoublePayment(id) {
    fetch(baseUrl + `api/financeiro/pesquisa-pagamentos-bb/${id}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        sendBankModalMessageForm.classList.add("alert-danger");
        sendBankModalMessageForm.classList.remove("alert-success");
        response.json().then(json => {
            valid = json.valid;
            message = json.message;
            if(valid == false) {
                sendBankModalMessageForm.innerHTML = json.message;
                sendBankModalMessageForm.style.display = 'block';
            }
        });
    });
}