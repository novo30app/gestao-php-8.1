var pendencies = [];
$(document).ready(function () {
    inputFile();
});

$('#formOfx').submit(e => {
    e.preventDefault();
    refreshTable();
})

function refreshTable() {
    $('.card').css('opacity', '.2');
    $('.spinner-card').show();
    const form = document.getElementById('formOfx');
    if ($('#directoryAccount').val() === '') {
        showNotify('danger', 'Selecione um Diretório e uma Conta!')
        return;
    }
    let url = `${baseUrl}api/financeiro/ofx/`;
    const id = document.getElementById('ofxFileId').value;
    // se já existir ofx carregado, acrescentar o id na rota para apenas atualizar a tabela
    if (parseInt(id) > 0) {
        url += id;
    }
    fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(form)
    }).then(response => {
        response.json().then(json => {

            //se status 201 aparece card, senão esconde
            if (response.status === 201) {
                let data = json.message;
                if (id == 0) {
                    window.location.href = baseUrl + 'financeiro/conciliacao/pendentes/' + data.id
                    $('#submitDiv').hide();
                    $('#refreshDiv').show();
                }

                // $('#detailsTransactions').show();
                //
                // // setando informações no front end
                // const keys = Object.keys(data);
                // for (let i = 0; i < keys.length; i++) {
                //     const span = document.getElementById(keys[i]);
                //     if (span && data[keys[i]] != '') span.innerText = data[keys[i]];
                // }
                //
                // // setando valor do id do ofxFile no input hidden
                // $('#ofxFileId').val(data.id);
                //
                // // gerando as linhas da tabela
                // let rows = json.transactions.map(getRows);
                //
                // $('#table tbody').html(rows);
                showRowSave();
            } else {
                $('#detailsTransactions').hide();
                showNotify('danger', json.message, 2000);
                resetForm();
            }

            $('.spinner-card').hide();
            $('.card').css('opacity', '1');
        });
    });
}

function showRowSave() {
    const verify = $("#formTable input[type='checkbox']:checked, #formTable input[type='radio']:checked");
    if (verify.length > 0) {
        $('#saveDiv').show();
    } else {
        $('#saveDiv').hide();
    }
}

function getRows(transaction) {

    // determinando a classe que deve ser aplicada em cada linha de acordo com o número de requerimentos
    const classe = !transaction.requeriments || transaction.requeriments.length == 0 ? 'tr-danger' : (transaction.requeriments.length == 1 ? 'tr-success' : 'tr-warning');

    const ofx = document.getElementById('ofxFileId').value;
    const directory = document.getElementById('directory').value;
    const account = document.getElementById('directoryAccount').value;
    let button = '';
    if (classe === 'tr-danger') {
        button = `<a class="float-end btn btn-orange text-white btn-sm" 
        href="${baseUrl}financeiro/pagamentos/novo-pagamento/?date=${formatDate(transaction.date)}&value=${transaction.amount}&description=${transaction.memo}&numberNf=${transaction.checkNumber[0]}&ofx=${ofx}&directory=${directory}&account=${account}"
         target="_blank">Cadastrar</a>`;
        pendencies.push(transaction);
    }

    let row = `
        <tr class="middle ${classe}">
            <td class="text-center">${formatDate(transaction.date)}</td>
            <td class="text-center">${transaction.type}</td>
            <td class="text-center">${transaction.amount.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}</td>
            <td class="text-center">${transaction.uniqueId}</td>
            <td class="text-center">${transaction.checkNumber[0]}</td>
            <td class="text-center">${transaction.memo} ${button}</td>
        </tr>
    `;

    // craindo as sub-tabelas com os detalhes dos requerimentos
    if (transaction.requeriments) {
        const requeriments = transaction.requeriments;
        let tbody = '';

        for (let i = 0; i < requeriments.length; i++) {
            let checked = 'disabled';
            let buttonCancel = '';

            if (requeriments[i].status != 5) {
                buttonCancel = `
                <a id="buttonCancel${requeriments[i].id}" class="text-white btn btn-danger btn-sm my-1" title="Cancelar" onclick="showModalCancel(${requeriments[i].id})">
                    <i class="fa fa-times-circle"></i>
                </a>
            `;
            }

            if (requeriments[i].status == 3) checked = 'checked';

            let input = '';
            if (classe === 'tr-warning') {
                input = `<input type="radio" name="radio${transaction.uniqueId}" onChange="showRowSave()"
                       value="${transaction.ofxLineId},${requeriments[i].id},${transaction.checkNumber[0]}" id="radio${requeriments.id}">`;
            } else {
                input = `<input type="checkbox" name="requeriments[]" onChange="showRowSave()"
                       value="${transaction.ofxLineId},${requeriments[i].id},${transaction.checkNumber[0]}" ${checked} id="checked${requeriments[i].id}">`;
            }

            tbody += `
                <tr class="middle">
                    <td>
                       ${input}
                    </td>
                    <td class="text-center">${requeriments[i].id}</td>
                    <td class="text-center">${requeriments[i].user}</td>
                    <td class="text-center">${requeriments[i].origin}</td>
                    <td class="text-center">${requeriments[i].type}</td>
                    <td class="text-center">${requeriments[i].beneficiary}</td>
                    <td class="text-center">${requeriments[i].attachments}</td>
                    <td class="text-center"><span id="statusStr${requeriments[i].id}">${requeriments[i].statusStr}</span></td>
                    <td class="text-center">
                       ${buttonCancel}
                        <a href="${baseUrl}financeiro/contas-a-pagar/${requeriments[i].id}" target="_blank" class="text-white btn btn-success btn-sm  my-1" title="Visualizar">
                            <i class="fa fa-search"></i>
                        </a>
                    </td>
                </tr>
            `;
        }


        row += `
        <tr>
            <td colspan="6">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-center">ID</th>
                            <th class="text-center">Usuário</th>
                            <th class="text-center">Origem</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Fornecedor</th>
                            <th class="text-center">Anexos</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${tbody}
                    </tbody>
                </table>
            </td>
        </tr>
        `
    }

    return row;
}

function showModalCancel(id) {
    $('#confirmCancel').prop('onclick', null).off('click').click(function () {
        setStatusCancel(id);
    });
    $('#modalCancel').modal('show');
}

function setStatusCancel(id) {
    fetch(baseUrl + `financeiro/pagamentos/novo-status/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
                $('#buttonCancel' + id).hide();
                $('#statusStr' + id).text('Cancelado');
                $('#checked' + id).prop({'checked': false, 'disabled': true})
                $('#modalCancel').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function getAccount() {
    const directory = $('#directory').val();
    fetch(baseUrl + `api/financeiro/contas-por-diretorio/?directory=${directory}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {

            if (json.message.length === 0) {
                showNotify('danger', 'Não existe conta vinculada a este Diretório e Origem de Despesa!');
            } else {

                let options = `<option value="">Selecionar</option>`

                options += json.message.map(getOptionsSelect);

                $('#directoryAccount').html(options);
            }
        });
    });
}

function getOptionsSelect(option) {

    return `<option value="${option.id}">${option.name}</option>`;
}

function resetForm() {
    document.getElementById('formOfx').reset();
    document.getElementById('ofxFileId').value = 0;
    $('.fileselector label').addClass('btn-orange').removeClass('btn-success');
    $('#fileLabel').text('Anexar Arquivo');
    $('#table tbody').html('');
    $('#refreshDiv, #detailsTransactions').hide();
    $('#submitDiv').show();
    showRowSave();
}

$('#formTable').submit(e => {
    e.preventDefault();
    $('#formTable').css('opacity', '.2');
    $('.spinner-card').show();
    const formData = formDataToJson('formTable');
    let valuesRadio = $("#formTable input[type='radio']:checked").map(function () {
        return $(this).val();
    }).get();
    if (formData.hasOwnProperty('requeriments')) {
        formData.requeriments = formData.requeriments.concat(valuesRadio);
    } else {
        formData.requeriments = valuesRadio;
    }
    formData.pendencies = pendencies;
    formData.account = $("#directoryAccount").val();
    fetch(`${baseUrl}api/financeiro/vincular-ofx-requerimentos/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        response.json().then(json => {
            $('#formTable').css('opacity', '1');
            $('.spinner-card').hide();
            if (response.status === 201) {
                showNotify('success', json.message, 2000);
                // $("#saveDiv").hide();
            } else {
                showNotify('danger', json.message, 2000);
            }
        });
    });
})

