var totalBudget = 0;
var totalAccomplished = 0;

function resetTableBudgets() {
    document.getElementById('filter').reset();
    setStorageIndexBudgets();
    setStorageIndexDetails();
}

function filterTables() {
    const formData = formDataToJson('filter');

    const variables = {
        'index': 0,
        'status': formData.status,
        'cpfCnpj': formData.cpfCnpj,
        'provider': formData.provider,
        'costCenter': formData.costCenter,
        'directory': formData.directory,
        'area': formData.area,
        'dateType': formData.dateType,
        'dateBegin': formData.dateBegin,
        'dateEnd': formData.dateEnd,
        'year': formData.year,
        'groupBy': formData.groupBy,
    };

    setStorage(sessions, {
        'url': 'api/financeiro/pagamentos',
        'columns': 13,
        variables: {
            ...variables,
            'limit': 25
        }
    });

    setStorage('DetailsBudgets', {
        'url': 'api/financeiro/orcamentos-detalhes',
        'columns': 17,
        variables: {
            ...variables,
            'limit': 10
        }
    })
}

$(document).ready(function () {
    $("#valuebillsToPayFilter, #paymentValuebillsToPayFilter").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    verifySession(sessions);
    verifySession('DetailsBudgets');
});

function setStorageIndexBudgets() {
    const date = new Date();
    setStorage(sessions, {
        'url': 'api/financeiro/pagamentos',
        'columns': 13,
        'variables': {
            'index': 0,
            'status': 'ef_or_conc',
            'cpfCnpj': '',
            'provider': '',
            'costCenter': '',
            'directory': '',
            'area': '',
            'dateBegin': '',
            'dateEnd': '',
            'dateType': 2,
            'year': date.getFullYear(),
            'limit': 25
        }
    });
}

function setStorageIndexDetails() {
    const date = new Date();
    setStorage('DetailsBudgets', {
        'url': 'api/financeiro/orcamentos-detalhes',
        'columns': 17,
        'variables': {
            'index': 0,
            'status': 'ef_or_conc',
            'cpfCnpj': '',
            'provider': '',
            'costCenter': '',
            'directory': '',
            'area': '',
            'dateBegin': '',
            'dateEnd': '',
            'groupBy': '0',
            'dateType': 2,
            'year': date.getFullYear(),
            'limit': 10
        }
    })
}

function getCostCenter(select, value, costCenter) {
    const directory = value ? value : $('#directory').val();
    const result = select ? $('#' + select) : $('#costCenter');
    return fetch(baseUrl + 'api/financeiro/centro-de-custo/diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`

            json.message.forEach(element => {
                options += `<option value="${element.name}">${element.name}</option>`;
            })

            result.html(options);

            $('#costCenterBudgetDetailsFilter').val(costCenter ?? '')

            return true;
        });
    });
}

function getArea(select, value, area) {
    const directory = value ? value : $('#directory').val();
    const result = select ? $('#' + select) : $('#area');
    return fetch(baseUrl + 'api/financeiro/area-por-diretorio/' + directory, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {
            let options = `<option value="">Selecionar</option>`

            options += json.message.map(getOptions);

            result.html(options).val(area ?? '');

            return true;
        });
    });
}

function getOptions(option) {

    return `<option value="${option.id}">${option.name}</option>`;
}

function csv() {
    let obj = JSON.parse(sessionStorage.getItem(sessions));
    const get = getUrl(obj.variables);
    window.open(baseUrl + `api/financeiro/exportBillsToPay/${get}`, '_blank');
}

function generateDetailsBudgets(budget) {
    totalBudget = totalAccomplished = 0;

    let columns = '';

    for (let i = 0; i < budget.values.length; i++) {
        let element = budget.values[i]
        columns += `
            <td onclick="setFilter('${budget.directory}', '${budget.aId}', '${budget.costCenter}', '${i + 1}', '${budget.year}')">
                <span class="badge badge-info">${floatToMoney(parseFloat(element.value))}</span><br>
                <span class="badge badge-danger">${floatToMoney(element.accomplished)}</span><br>
                <span class="badge badge-primary">${floatToMoney(parseFloat(element.value) - element.accomplished)}</span><br>
            </td>
        `;

        totalBudget += parseFloat(element.value);
        totalAccomplished += element.accomplished;
    }

    return `
            <tr class="middle">
                <td class="text-center">${ budget.directory }</td>
                <td class="text-center">${ budget.costCenter || '-' }</td>
                <td class="text-center">${ budget.area || '-' }</td>
                <td class="text-center">${ budget.year }</td>
                ${columns}
                <td>
                    <span class="badge badge-info">${floatToMoney(totalBudget)}</span><br>
                    <span class="badge badge-danger">${floatToMoney(totalAccomplished)}</span><br>
                    <span class="badge badge-primary">${floatToMoney(totalBudget - totalAccomplished)}</span><br>
                </td>
            </tr>
       
    `;

}

async function setFilter(directory, area, costCenter, month, year) {
    $('#directoryBudgetDetailsFilter').val(directory)
    // await getArea('areaBudgetsDetailsFilter', directory, area);
    // await getCostCenter('costCenterList', directory, costCenter);
    // Aguardar pelas funções assíncronas getArea e getCostCenter
    try {
        await getArea('areaBudgetsDetailsFilter', directory, area);
        await getCostCenter('costCenterList', directory, costCenter);
    } catch (error) {
        console.error("Error setting filters:", error);
        // Opcional: Tratar erro, exibir mensagem para o usuário, etc.
        return; // Interrompe a execução em caso de erro
    }


    $('#dateBeginBudgetDetailsFilter').val(`${year}-${String(month).padStart(2, '0')}-01`);

    const endDate = new Date(year, parseInt(month), 0);

    const date = `${year}-${String(endDate.getMonth() + 1).padStart(2, '0')}-${String(endDate.getDate()).padStart(2, '0')}`;

    $('#dateEndBudgetDetailsFilter').val(date)

    setTimeout(() => {
        filterTables();
    }, 150)
}
