var indexList = 0;

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('OfxPendency');
})

function generateLines(transaction) {

    // determinando a classe que deve ser aplicada em cada linha de acordo com o número de requerimentos
    let classe = !transaction.payments || transaction.payments.length == 0 ? 'tr-danger' : (transaction.payments.length == 1 ? 'tr-success' : 'tr-warning');
    if (transaction.refund || transaction.multiple) classe = 'tr-success';
    const ofx = transaction.ofxFiles;
    const directory = transaction.directoryName;
    const account = transaction.directoryAccount;
    let button = '';
    if (classe === 'tr-danger') {
        button = `<a class="float-end btn btn-orange text-white btn-sm" 
        href="${baseUrl}financeiro/pagamentos/novo-pagamento/?transaction_id=${transaction.id}&date=${transaction.date}&value=${transaction.value}&description=${transaction.description}&numberNf=${transaction.extractNumber}&ofx=${ofx}&directory=${directory}&account=${account}&directoryAccountAgency=${transaction.directoryAccountAgency}&directoryAccountAccount=${transaction.directoryAccountAccount}&directoryAccountNickname=${transaction.directoryAccountNickname}"
         target="_blank">Cadastrar</a>`;
        button += `<button onclick="linkModal(${transaction.id})" class="float-end btn btn-warning text-white btn-sm">Vincular</button>`;
    }

    let row = `
        <tr class="middle ${classe}" id="ofxLine${transaction.id}">
            <td class="text-center">${transaction.date}</td>
            <td class="text-center">${transaction.type}</td>
            <td class="text-center">${transaction.value.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})}</td>
            <td class="text-center">${transaction.directoryName}</td>
            <td class="text-center">${transaction.directoryAccountAccount}</td>
            <td class="text-center">${transaction.uniqueIdentifier}</td>
            <td class="text-center">${transaction.extractNumber}</td>
            <td class="text-center">${transaction.description} ${button}</td>
        </tr>
    `;

    // craindo as sub-tabelas com os detalhes dos requerimentos
    if (transaction.payments.length > 0) {
        let tbody = '';
        const requeriments = transaction.payments;

        for (let i = 0; i < requeriments.length; i++) {
            let buttonConfirm = '';

            if (transaction.payment === null && canUpdate) {
                buttonConfirm = `
                        <a class="text-white btn btn-success btn-sm my-1 btn${transaction.id}" 
                            title="Conciliar" onclick="conciliation(${requeriments[i].id}, ${transaction.id}, 1)">
                            <i class="fa fa-check-circle"></i>
                        </a>
                `;
            }


            tbody += `
                <tr class="middle">
                  
                    <td class="text-center">${requeriments[i].id}</td>
                    <td class="text-center">${requeriments[i].user}</td>
                    <td class="text-center">${requeriments[i].origin}</td>
                    <td class="text-center">${requeriments[i].type}</td>
                    <td class="text-center">${requeriments[i].beneficiary}</td>
                    <td class="text-center">${requeriments[i].attachments}</td>
                    <td class="text-center"><span id="statusStr${requeriments[i].id}">${requeriments[i].statusStr}</span></td>
                    <td class="text-center">
                        <a href="${baseUrl}financeiro/contas-a-pagar/${requeriments[i].id}" target="_blank" class="text-white btn btn-info btn-sm  my-1" title="Visualizar">
                            <i class="fa fa-search"></i>
                        </a>
                        ${buttonConfirm}
                    </td>
                </tr>
            `;
        }

        row += `
        <tr>
            <td colspan="8">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            
                            <th class="text-center">ID</th>
                            <th class="text-center">Usuário</th>
                            <th class="text-center">Origem</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Fornecedor</th>
                            <th class="text-center">Anexos</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody id="line${transaction.id}">
                        ${tbody}
                    </tbody>                    
                </table>
            </td>
        </tr>
        `
    }

    if (transaction.reversals.length > 0) {
        let tbodyReversals = '';
        const reversals = transaction.reversals;

        for (let i = 0; i < reversals.length; i++) {
            let buttonConfirm = '';

            if (transaction.refund === null) {
                buttonConfirm = `
                        <a class="text-white btn btn-success btn-sm my-1 btn${transaction.id}" 
                            title="Conciliar" onclick="conciliation(${reversals[i].id}, ${transaction.id}, 2)">
                            <i class="fa fa-check-circle"></i>
                        </a>
                `;
            }


            tbodyReversals += `
                <tr class="middle">             
                    <td class="text-center"><b>Entrada</b></td>     
                    <td class="text-center">${reversals[i].date}</td>                    
                    <td class="text-center">${reversals[i].value}</td>
                    <td class="text-center">${reversals[i].uniqueIdentifier}</td>
                    <td class="text-center">${reversals[i].extractNumber}</td>
                    <td class="text-center">${reversals[i].description}</td>
                    <td class="text-center">
                        ${buttonConfirm}
                    </td>
                </tr>
            `;
        }

        row += `
        <tr>
            <td colspan="8">
                <table class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>                         
                            <th></th>   
                            <th class="text-center">Data</th>
                            <th class="text-center">Valor</th>                            
                            <th class="text-center">Identificador único</th>
                            <th class="text-center">Número do extrato</th>
                            <th class="text-center">Descrição</th>
                            <th class="text-center">Ações</th>
                        </tr>
                    </thead>
                    <tbody id="line${transaction.id}">
                        ${tbodyReversals}
                    </tbody>                    
                </table>
            </td>
        </tr>
        `
    }

    return row;
}

function showModalCancel(requeriment, pendency) {
    $('#confirmCancel').prop('onclick', null).off('click').click(function () {
        removeConciliation(requeriment, pendency);
    });
    $('#modalCancel').modal('show');
}

function removeConciliation(requeriment, pendency) {
    showLoading();
    const formData = {
        requeriment,
        pendency
    };
    fetch(`${baseUrl}financeiro/conciliacao/excluir`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 200) {
                $('#modalCancel').modal('hide');
                showNotify('success', json.message, 1500);
                $("#line" + pendency).hide();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function conciliation(requeriment, pendency, type) {
    showLoading();
    let formData = new FormData();
    formData.requeriment = requeriment;
    formData.pendency = pendency;
    formData.type = type;
    console.log(formData)
    fetch(`${baseUrl}financeiro/conciliacao/pendentes`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
                // $(".btn" + pendency).hide();
                // $("#statusStr" + requeriment).text('Conciliado');
                let arr = [];
                listPendency.forEach(element => {
                    if (element.id !== pendency) {
                        element.payments.forEach((el, index) => {
                            if (el.id === requeriment) element.payments.splice(index, 1);
                        })
                        arr.push(element);
                    }
                })
                listPendency = arr;

                const options = listPendency.map(generateLines);
                $("#tableOfxPendency tbody").html(options).css('opacity', '1');

            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function setStorageIndex() {
    const url = window.location.href;
    let ofx = url.replace(baseUrl + 'financeiro/conciliacao/pendentes', '');
    if (ofx.length > 0) ofx = ofx.replace('/', '');
    setStorage('OfxPendency', {
        'url': 'api/financeiro/pendencias-ofx/json',
        'columns': 8,
        'variables': {
            'index': 0,
            'limit': 25,
            'directory': '',
            'directoryAccount': '',
            'status': '1',
            'start': '',
            'end': '',
            ofx
        }
    })
}

$(document).ready(function () {
    setStorageIndex();
});

function getAccount(directory) {
    fetch(baseUrl + `api/financeiro/contas-por-diretorio/?directory=${directory}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        response.json().then(json => {

            if (json.message.length === 0) {
                showNotify('danger', 'Não existe conta vinculada a este Diretório!');
            } else {

                let options = `<option value="">Selecionar</option>`

                options += json.message.map(getOptionsSelect);

                $('#directoryAccountOfxPendencyFilter').html(options);
            }
        });
    });
}

function linkModal(id) {
    document.getElementById('linkForm').reset();
    document.getElementById('ofxLineId').value = id;
    $('#linkModal').modal('show');
}


const linkForm = document.getElementById('linkForm');
linkForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    const formData = formDataToJson('linkForm');
    fetch(`${baseUrl}financeiro/conciliacao/pendentes-multiplos`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            if (response.status === 200) {
                $('#linkModal').modal('hide');
                showNotify('success', json.message, 1500);
                $("#ofxLine" + formData.ofxLineId).hide();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
})