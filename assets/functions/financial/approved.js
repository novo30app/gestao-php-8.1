const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Approved');
})

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return baseUrl + `uploads/${attachment[1]}`;
}


function generateLines(approved) {
    console.log(approved)
    let attachmentNf = attachmentBillet = attachmentContract = attachmentPayment = '';
    if (approved.attachmentNf && approved.attachmentNf != '') {
        attachmentNf =
            ` <a class="btn btn-sm btn-success my-1" href="${formatAttachment(approved.attachmentNf)}" target="_blank">NF</a><br> `;
    }
    if (approved.attachmentBillet && approved.attachmentBillet != '') {
        attachmentBillet =
            ` <a class="btn btn-sm btn-primary my-1" href="${formatAttachment(approved.attachmentBillet)}" target="_blank">Boleto</a><br> `;
    }
    if (approved.attachmentContract && approved.attachmentContract != '') {
        attachmentContract =
            ` <a class="btn btn-sm btn-secondary my-1" href="${formatAttachment(approved.attachmentContract)}" target="_blank">Contrato</a><br> `;
    }

    let cpfCnpj = approved.cpfCnpj;
    var cpfCnpjFormated = cpfCnpj.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    if(cpfCnpj.length > 11) var cpfCnpjFormated = cpfCnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1$2$3/$4-$5");
    const value = approved.approved + 1;
    return `<tr class="middle">
                    <td class="text-center">${approved.created}</td>
                    <td class="text-center">${approved.paymentValue}</td>
                    <td class="text-center">${approved.dueDate}</td>
                    <td class="text-center">${approved.directory}</td>
                    <td class="text-center">${approved.ccName || ''}</td>
                    <td class="text-center">${approved.area}</td>
                    <td class="text-center">${approved.user}</td>
                    <td class="text-center">${originString(approved.origin)}</td>
                    <td class="text-center">${approved.prName}</td>
                    <td class="text-center">${cpfCnpjFormated}</td>
                    <td class="text-center">${attachmentNf}
                                            ${attachmentBillet}
                                            ${attachmentContract}
                                            ${attachmentPayment}
                    </td>
                    <td class="text-center">
                            <a class="btn btn-info text-end my-1" target="_blank" href="${baseUrl}financeiro/contas-a-pagar/${approved.id}" title="Visualizar"><i class="fa fa-search" aria-hidden="true"></i></a><br>
                            <a class="btn btn-primary text-end my-1" target="_blank" href="${baseUrl}financeiro/pagamentos/${approved.id}" title="Editar"><i class="fa fa-pencil" aria-hidden="true"></i></a><br>
                            <button class="btn btn-success text-end my-1" onclick="updateApproved(${approved.id}, ${value})" title="Autorizar"><i class="fa fa-check-circle" aria-hidden="true"></i></button><br>
                    </td>
                </tr>`;
}

function updateApproved(requerimentId, approved) {
    const formData = {
        requerimentId,
        approved
    };
    fetch(`${baseUrl}api/financeiro/aprovacao/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1000);
                setTimeout(function () {
                    setStorageIndex();
                }, 1000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function originString(origin) {
    switch (parseInt(origin)) {
        case 1:
            return 'Campanha Eleitoral';
        case 2:
            return 'Política da Mulher';
        case 3:
            return 'Outros recursos';
        case 4:
            return 'Fundo Partidário';
        default:
            return 'Desconhecido';
    }
}

function setStorageIndex() {
    setStorage('Approved', {
        'url': 'api/financeiro/aprovacao',
        'columns': 12,
        'variables': {
            'index': 0,
            'title': '',
            'limit': 25,
            'directory': '',
            'user': '',
            'cpfCnpj': '',
            'provider': '',
            'costCenter': '',
        }
    })
}

$(document).ready(function () {
    sessionStorage.removeItem('Approved');
    verifySession('Approved');
});
