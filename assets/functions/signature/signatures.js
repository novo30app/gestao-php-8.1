const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('SignatureSignatures');
})


function generateLines(requests) {
    let button = '';
    let copy = '';
    if (requests.signed == '0') {
        button = `
            <div class="badge badge-secondary label-square pointer m-x-5 my-1"
                onclick="duplicate(${requests.id}, '${requests.name}')">
                <span class="f-14">Segunda via</span>
            </div>
        `;

        copy = `
            <div class="badge badge-info label-square pointer m-x-5 my-1 clipboard"
                data-clipboard-text="${baseUrl}assinatura-eletronica/documentos/assinar/${requests.id}/${requests.hash}">
                <span class="f-14">Copiar Link</span>
            </div>
        `;
    }
    var document = '';
    if (requests.signed == 1) document = `<a href="${baseUrl}assinatura-eletronica/documentos/assinaturas/${requests.id}/${requests.hash}" target="_blank"><i class="fa fa-file-text" aria-hidden="true" style="font-size: x-large;"></i> Documento</a>`;
    return `<tr class="middle">
                    <td class="text-center">${requests.name}</td>
                    <td class="text-center">${requests.email}</td>
                    <td class="text-center">${requests.created}</td>
                    <td class="text-center">${requests.date ?? ''}</td>
                    <td class="text-center">${getSignedString(requests.signed)}${button} ${copy}</td>
                    <td class="text-center">${document}</td>
                </tr>`;
}

var clipboard = new ClipboardJS('.clipboard');
clipboard.on('success', function (e) {
    showNotify('success','Link copiado com sucesso!', 1000);
    e.clearSelection();
});

function getSignedString(status) {
    if (status == 0) {
        return 'Pendente';
    } else if (status == 1) {
        return 'Assinado';
    } else if (status == 2) {
        return 'Segunda Via';
    } else {
        return 'Desconhecido';
    }
}

function setStorageIndex() {
    setStorage('SignatureSignatures', {
        'url': 'assinatura-eletronica/documentos/assinaturas/' + idDoc + '/api',
        'columns': 5,
        'variables': {
            'index': 0,
            'name': '',
            'email': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    setStorageIndex();
});

function duplicate(id, name) {
    document.getElementById('duplicateForm').reset();
    document.getElementById('doc').value = id;
    document.getElementById('name').textContent = name;
    $('#duplicateModal').modal('show');
    $('#messageFormDuplicate').hide();
}


const duplicateForm = document.getElementById('duplicateForm');
duplicateForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('duplicateForm');
    let formData = formDataToJson('duplicateForm');
    fetch(baseUrl + "assinatura-eletronica/documentos/segunda-via", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        response.json().then(json => {
            $('#duplicateModal').modal('hide');
            closeLoading();
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});
