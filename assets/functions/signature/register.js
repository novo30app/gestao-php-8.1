tinymce.init({
    selector: "textarea",
    menubar: false,
    statusbar: false,
    toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image media | forecolor backcolor table',
    plugins: ["lists link image media textcolor table"],
    browser_spellcheck: true,
    imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    }
});

$(document).ready(function () {
    setTimeout(function () {
        setBorder('description')
    }, 500);
})


var classe = 'description'
const formAdd = document.getElementById('register');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('register')) {
        closeLoading();
        return;
    }
    let formData = formDataToJson('register');
    fetch(baseUrl + "assinatura-eletronica/documentos", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.href = baseUrl + "assinatura-eletronica/documentos";
                }, 1000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});