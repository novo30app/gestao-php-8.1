$(document).ready(function () {
    inputFile();
})

$("#formFile").submit(function (e) {
    e.preventDefault();
    if ($('#file').val() == '') showNotify('danger', 'Você precisa anexar um documento!', 1500);
    showLoading();
    $.ajax({
        type: 'POST',
        url:  baseUrl + 'assinatura-eletronica/documentos/importacao',
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            showNotify('success', json.message, 0)
            setTimeout(function () {
                window.location.href = baseUrl + "assinatura-eletronica/documentos/assinaturas/<?= $doc->getId(); ?>";
            }, 1500);
        })
        .fail(function (json) {
            closeLoading();
            showNotify('success', json.responseJSON.message, 1500)
        });
});