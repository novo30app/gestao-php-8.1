const formAdd = document.getElementById('register');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('register');
    if (!ValidateForm('register')) {
        closeLoading();
        return;
    }
    let formData = formDataToJson('register');
    fetch(baseUrl + "assinatura-eletronica/documentos/enviar", {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 0);
                setTimeout(function () {
                    window.location.href = baseUrl + "assinatura-eletronica/documentos";
                }, 1000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});