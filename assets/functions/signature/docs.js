const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('SignatureDocs');
})



function generateLines(requests) {
    let status = 'Encerrado';
    if (requests.active == 1) status = 'Aberto';
    return `<tr class="middle">
                    <td class="text-center">${requests.id}</td>
                    <td class="text-center">${requests.created}</td>
                    <td class="text-center">${requests.title}</td>
                    <td class="text-center">${requests.name}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            <a class="btn btn-sm btn-success" href="${baseUrl}assinatura-eletronica/documentos/enviar/${requests.id}">Enviar</a>
                            <a class="btn btn-sm btn-info" href="${baseUrl}assinatura-eletronica/documentos/assinaturas/${requests.id}">Assinaturas</a>
                            <a class="btn btn-sm btn-warning" href="${baseUrl}assinatura-eletronica/documentos/cadastro/${requests.id}">Editar</a>
                            <a class="btn btn-sm btn-success" title="Importar Documento" href="${baseUrl}assinatura-eletronica/documentos/importacao/${requests.id}"><i class="fa fa-cloud-upload"></i></a>
                        </div>
                     </td>
                </tr>`;
}

function setStorageIndex() {
    setStorage('SignatureDocs', {
        'url': 'api/assinatura-eletronica/documentos',
        'columns': 5,
        'variables': {
            'index': 0,
            'title': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    verifySession('SignatureDocs');
});