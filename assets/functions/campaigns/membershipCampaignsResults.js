var index = partial = 0;
var total = 1;
var name = indicator = state = city = status = beginDate = finalDate = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    indicator = formData.indicator; 
    state = formData.state;
    city = formData.city;
    status = formData.status;
    beginDate = formData.beginDate;
    finalDate = formData.finalDate;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = indicator = state = city = status = beginDate = finalDate = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableCampaigns(campaigns) {
    return `<tr class="middle">
                <td><a href="${baseUrl}/filiados/${campaigns.user}" target="_blank">${campaigns.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td><a href="${baseUrl}/filiados/${campaigns.indicatorId}" target="_blank">${campaigns.indicator} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${campaigns.status}</td>
                <td class="text-center">${campaigns.solicitationDate}</td>
                <td class="text-center">${campaigns.affiliationDate}</td>
                <td class="text-center">${campaigns.uf}</td>
                <td class="text-center">${campaigns.city}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}campanhas/campanhas-filiacao-resultados-listagem/?index=${index}&name=${name}&indicator=${indicator}&state=${state}&city=${city}&status=${status}&beginDate=${beginDate}&finalDate=${finalDate}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableCampaigns);
            $("#table tbody").append(options);
        });
    });
}

function csv() {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let indicator = formData.indicator;
    let state = formData.state;
    let city = formData.city;
    let status = formData.status;
    let beginDate = formData.beginDate;
    let finalDate = formData.finalDate;
    window.open(
        `${baseUrl}campanhas/campanhas-filiacao-resultados-exportacao/?name=${name}&indicator=${indicator}&state=${state}&city=${city}&status=${status}&beginDate=${beginDate}&finalDate=${finalDate}`, 
        '_blank');
}

$(document).ready(function() {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});