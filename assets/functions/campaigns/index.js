var index = partial = 0;
var total = 1;
var nameFilter = descriptionFilter = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    nameFilter = formData.nameFilter;
    descriptionFilter = formData.descriptionFilter;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    nameFilter = descriptionFilter = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableCampaigns(campaigns) {
    let actions = '';
    if (canUpdate) actions = `<a href="#" onclick="editCampaign('${campaigns.id}', '${campaigns.name}', '${campaigns.description}')"><i class="fa fa-pencil" aria-hidden="true" style="font-size: x-large;"></i> Editar</a><br>`;
    if (campaigns.status == 1 && canDelete) actions += `<a href="#" onclick="change('${campaigns.id}', 2)"><i class="fa fa-times" aria-hidden="true" style="font-size: x-large;"></i> Remover</a><br>`;
    return `<tr class="middle">
                <td>${campaigns.name}</td>
                <td class="text-center">${campaigns.created}</td>
                <td class="text-center">${campaigns.user}</td>
                <td class="text-center">${campaigns.description}</td>
                <td class="text-center"><a href="https://novo.org.br/filiacao/?hash=${campaigns.hash}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${campaigns.collection}</td>
                <td class="text-center">${actions}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}campanhas/listagem/?index=${index}&name=${nameFilter}&description=${descriptionFilter}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableCampaigns);
            $("#table tbody").append(options);
        });
    });
}

function changeModal(action) {
    $("#campaign, #name, #description").val('');
    $("#exampleModal").modal(action);
}

const formModal = document.getElementById('formModal');
const divMessageModal = document.getElementById('divMessageModal');
$("#formModal").submit(function (e) {
    document.getElementById("save").disabled = false;
    e.preventDefault();
    showLoading();
    fetch(`${baseUrl}campanhas/salva/`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
        body: new FormData(formModal)
    }).then(response => {
        document.getElementById("save").disabled = false;
        response.json().then(json => {
            closeLoading();
            divMessageModal.textContent = json.message;
            divMessageModal.style.display = 'block';
            divMessageModal.classList.add("alert-danger");
            divMessageModal.classList.remove("alert-success");
            if (response.status === 201) {
                divMessageModal.classList.add("alert-success");
                divMessageModal.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        });
    });
});

const divMessage = document.getElementById('divMessage');

function change(id) {
    fetch(`${baseUrl}campanhas/muda-status/${id}/`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
        },
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            divMessage.textContent = json.message;
            divMessage.style.display = 'block';
            divMessage.classList.add("alert-danger");
            divMessage.classList.remove("alert-success");
            if (response.status === 201) {
                divMessage.classList.add("alert-success");
                divMessage.classList.remove("alert-danger");
                setTimeout(function () {
                    window.location.reload();
                }, 1200);
            }
        });
    });
}

function editCampaign(id, name, description) {
    $("#campaign").val(id);
    $("#name").val(name);
    $("#description").val(description);
    $("#exampleModal").modal('show');
}

$(document).ready(function () {
    generateTable();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});