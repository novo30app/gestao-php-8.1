var index = partial = 0;
var total = 1;
var name = campaign = status = exemption = "";

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    campaign = formData.campaign;
    status = formData.status;
    exemption = formData.exemption;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = campaign = status = exemption = "";
    $("#table tbody").empty();
    generateTable();
}

function generateTableAffiliations(affiliations) {
    return `<tr class="middle">
                <td><a href="${baseUrl}filiados/${affiliations.id}" target="_blank">${affiliations.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${affiliations.created}</td>
                <td class="text-center">${affiliations.status}</td>
                <td class="text-center">${affiliations.exemption}</td>
                <td class="text-center">${affiliations.campaign}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}campanhas/listagem-filiacoes/?index=${index}&name=${name}&campaign=${campaign}&status=${status}&exemption=${exemption}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableAffiliations);
            $("#table tbody").append(options);
        });
    });
}

$(document).ready(function() {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});