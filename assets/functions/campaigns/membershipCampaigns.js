var index = partial = 0;
var total = 1;
var name = state = city = beginDate = finalDate = "";
var ordernation = 'DESC';

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = index = 0;
    total = 1;
    name = formData.name;
    state = formData.state;
    city = formData.city;
    ordernation = formData.ordernation;
    beginDate = formData.beginDate;
    finalDate = formData.finalDate;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = state = city = beginDate = finalDate = "";
    ordernation = 'DESC';
    $("#table tbody").empty();
    generateTable();
}

function generateTableCampaigns(campaigns) {
    label =  campaigns.result == 1 ? ' Solicitação' : ' Solicitações';
    let file = '';
    if(campaigns.file != '---') {
        file = `<a href="https://espaco-novo.novo.org.br/${campaigns.file}" target="_blank"> <i class="fa fa-file-text" aria-hidden="true" target="_blank" style="font-size: 200%;"></i></a>`;
    }
    return `<tr class="middle">
                <td><a href="${baseUrl}/filiados/${campaigns.indicator}" target="_blank">${campaigns.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${campaigns.result + label}</td>
                <td class="text-center">${campaigns.uf}</td>
                <td class="text-center">${campaigns.city}</td>
                <td class="text-center">
                    <a rel="noopener" href="https://api.whatsapp.com/send?phone=${campaigns.phone.replace(/\\D+/g, '')}" target="_blank">
                        ${campaigns.phone}
                    </a>
                </td>
                <td class="text-center"><a href="${campaigns.link}" target="_blank">${campaigns.link} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${file}</td>
            </tr>`;
}

function generateTable() {
    fetch(`${baseUrl}campanhas/campanhas-filiacao-listagem/?index=${index}&name=${name}&state=${state}&city=${city}&ordernation=${ordernation}&beginDate=${beginDate}&finalDate=${finalDate}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            closeLoading();
            $('#total').text(json.total);
            $('#partial').text(json.partial);
            let options = json.message.map(generateTableCampaigns);
            $("#table tbody").append(options);
        });
    });
}

function csv() {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let state = formData.state;
    let city = formData.city;
    let ordernation = formData.ordernation;
    window.open(
        `${baseUrl}campanhas/campanhas-filiacao-listagem-exportacao/?name=${name}&state=${state}&city=${city}&ordernation=${ordernation}`, 
        '_blank');
}

$(document).ready(function() {
    generateTable();
    if(userState != '') getCities(userState);
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});