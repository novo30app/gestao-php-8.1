/**
 * @param session string com nome da sessionStorage
 * @param array com os valores a serem salvos na sessionStorage
 */
function setStorage(session, array) {
    // converter array em string e salvar no storage
    sessionStorage.setItem(session, JSON.stringify(array));
    generateTable(session);
}

function generateTable(session) {
    let obj = JSON.parse(sessionStorage.getItem(session));
    $('.loader' + session).css('opacity', 1);
    const get = getUrl(obj.variables);
    fetch(`${baseUrl}${obj.url}/${get}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('.loader' + session).css('opacity', 0);
            if (response.status === 403) {
                showNotify('danger', 'Você foi logado em outro lugar, será redirecionado para a página de login!', 0);
                setTimeout(function () {
                    window.location.href = baseUrl + 'logout';
                }, 1000)
                return;
            }
            $('#total' + session).text(json.total);
            $('#start' + session).text(json.total > 0 ? obj.variables.index * obj.variables.limit + 1 : 0);
            $('#end' + session).text(json.partial);
            if (json.message.length > 0) {
                setFunction(json.message, session, json);
            } else {
                $("#table" + session + " tbody").html(`<tr><td colspan="${obj.columns}" class="text-center">Nenhum resultado encontrado</td></tr>`);
            }
            if (json.message.length > 0) generatePagination(json.total, obj.variables.index, obj.variables.limit, session);

            sessionStorage.setItem(session, JSON.stringify(obj));
        });
    });
}

function getUrl(obj) {
    let url = '?';

    const keys = Object.keys(obj);

    for (let i = 0; i < keys.length; i++) {
        if (i > 0) url += '&';
        let result = obj[keys[i]];
        url += `${keys[i]}=${result}`;
    }

    return url;
}

/**
 * @param message objeto json retornado na busca
 * @param session string com nome da sessionStorage
 * @param json json com outros valores que podem ser necessários que não estão no objeto message
 * Função criada para selecionar as funções que geram as tabelas na view, é necessária quando temos mais de
 * uma tabela na mesma página
 */
function setFunction(message, session, json) {
    let options = '';
    if (session == 'Area') {
        options = message.map(generateLinesArea);
        let select = `<option value="">Selecionar</option>`;
        select += message.map(getAreas);
        $('#areaSubArea').empty().append(select);
    } else if (session == 'SubArea') {
        options = message.map(generateLinesSubArea);
    } else if (session === 'OfxPendency') {
        listPendency = message;
        options = message.map(generateLines);
    } else if (session == 'DetailsBudgets') {
        options = message.map(generateDetailsBudgets);
    } else {
        options = message.map(generateLines);
    }
    console.log(session)
    $("#table" + session + " tbody").html(options).css('opacity', '1');
}

/**
 * @param session string com nome da sessionStorage
 * @param obj objeto com as variáveis da url
 * Função para setar os valores do formulário assim como os valores das
 * variáveis da url
 */
function getFilter(session, obj) {
    const keys = Object.keys(obj);

    for (let i = 0; i < keys.length; i++) {
        let result = obj[keys[i]];
        if (document.getElementById(keys[i] + session + 'Filter')) $('#' + keys[i] + session + 'Filter').val(result)

        if (keys[i] == 'state' && result > 0) {
            getCities(result, 'city' + session + 'Filter');
        }

        if (keys[i] == 'city') {
            setTimeout(function () {
                $('#' + keys[i] + session + 'Filter').val(result)
            }, 500)
        }
    }
}

/**
 * @param session string com nome da sessionStorage
 * Função para alterar as variáveis da url conforme os valores do formulário
 */
function filterTable(session) {
    // Pegando a sessionStorage
    let obj = JSON.parse(sessionStorage.getItem(session));
    // Pegando as variáveis que vão para a url
    let array = obj.variables;
    // Pegando o array das chaves das variáveis da url
    const keys = Object.keys(array);

    // Selecionando apenas os valores do formulário, os demais permanecem iguais
    for (let i = 0; i < keys.length; i++) {
        if (document.getElementById(keys[i] + session + 'Filter')) {
            obj.variables[keys[i]] = $('#' + keys[i] + session + 'Filter').val();
        }
    }

    obj.variables.index = 0;

    if (session == 'EventsCheckin' || session == 'EventsRegistration') {
        let url = window.location.href;
        obj.variables.id = url.replace(baseUrl + 'eventos/checkin/', '');
    }

    // Salvando a sessionStorage
    sessionStorage.setItem(session, JSON.stringify(obj));
    $('#table' + session + ' tbody').empty();
    // Chamando a função que gera a tabela
    generateTable(session);
}

/**
 * @param session string com nome da sessionStorage
 * Função para resetar formulário e variáveis da url
 */
function resetTable(session) {
    // Pegando a sessionStorage
    let obj = JSON.parse(sessionStorage.getItem(session));
    // Pegando as variáveis que vão para a url
    let array = obj.variables;
    // Pegando o array das chaves das variáveis da url
    const keys = Object.keys(array);

    // Selecionando apenas os valores do formulário para resetá-los
    for (let i = 0; i < keys.length; i++) {
        if (document.getElementById(keys[i] + session + 'Filter')) {
            obj.variables[keys[i]] = '';
            $('#' + keys[i] + session + 'Filter').val('');
        }
    }

    // Setando valores específicos das sessions
    if (session === 'Affiliated') {
        obj.variables.agroup = 'nome';
        obj.variables.status = 7;
        obj.variables.ordination = 'ASC';
        obj.variables.typeDate = 'data_filiacao';
    } else if (session === 'billsToPay' || session === 'payments') {
        obj.variables.status = "-1";
        obj.variables.origin = 0;
    } else if (session === 'Events') {
        obj.variables.status = 'ativo';
    } else if (session === 'EventsCheckin' || session === 'EventsRegistration') {
        let url = window.location.href;
        obj.variables.id = url.replace(baseUrl + 'eventos/checkin/', '');
    } else if (session === 'Contracts') {
        obj.variables.order = 'id';
        obj.variables.seq = 'desc';
    } else if (session === 'OfxPendency') {
        obj.variables.status = '1';
        obj.variables.ofx = '';
    }else if (session === 'Meetings') {
        obj.variables.sector = sectorFilter;
    }

    obj.variables.index = 0;

    // Salvando a sessionStorage
    sessionStorage.setItem(session, JSON.stringify(obj));
    $('#table' + session + ' tbody').empty();
    // Chamando a função que gera a tabela
    generateTable(session);
}

function newOrder(order, seq, hide, show, session) {
    let obj = JSON.parse(sessionStorage.getItem(session));
    $('#' + obj.table + ' tbody').empty();
    obj.variables.index = 0;
    if (obj.variables.seq === 'asc') {
        obj.variables.seq = 'desc'
    } else {
        obj.variables.seq = 'asc';
    }
    obj.variables.order = order;
    // obj.variables.seq =  seq;
    sessionStorage.setItem(session, JSON.stringify(obj));
    $('#' + hide).hide();
    $('#' + show).show();
    generateTable(session);
}

/**
 * @param session string com nome da sessionStorage
 */
function verifySession(session) {
    // Verificando se a session já existe, se já existir ele dá os valores do formulário e do limite
    if (sessionStorage.getItem(session)) {
        let obj = JSON.parse(sessionStorage.getItem(session));
        if (session === 'SignatureSignatures') {
            obj.url = 'assinatura-eletronica/documentos/assinaturas/' + idDoc;
            sessionStorage.setItem(session, JSON.stringify(obj));
        }
        getFilter(session, obj.variables);
        $('#limit' + session).val(obj.variables.limit);
        generateTable(session)
    } else {
        if (session == 'Area') {
            setStorageIndexArea();
        } else if (session == 'SubArea') {
            setStorageIndexSubArea();
        } else if (session == 'BudgetDetails') {
            setStorageIndexBudgets();
        } else if (session == 'DetailsBudgets') {
            setStorageIndexDetails();
        } else {
            setStorageIndex();
        }
    }
}
