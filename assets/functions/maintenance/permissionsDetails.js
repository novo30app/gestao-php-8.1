const form = document.getElementById('form');
form.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('form');
    fetch(`${baseUrl}api/cadastros/salva-permissao`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        $('#save').removeClass('disabled');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(() => {
                    $('#modal').modal('hide');
                    window.location.reload();
                }, 250)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

$(document).ready(function (){});