const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Permissions');
});

function setStorageIndex() {
    setStorage('Permissions', {
        'url': 'api/cadastros/permissoes',
        'columns': 6,
        'variables': {
            'index': 0,
            'userType': '',
            'sector': '',
            'limit': 25
        }
    })
};

function generateLines(permission) {
    let actions = "";
    if(canUpdate) {
        actions += `<a href="${baseUrl}cadastros/permissoes-detalhes/${permission.id}" target="_blank">
                        <i class="fa fa-pencil" aria-hidden="true" title="Editar" 
                        style="font-size: 20px; margin-left: 15px;"></i>
                    </a>`;
    }
    if(canDelete) {
        actions += `<a onclick="remove('${permission.id}')">
                        <i class="fa fa-trash" aria-hidden="true" title="Remover" 
                        style="font-size: 20px; margin-left: 15px; color: red"></i>
                    </a>`;
    }

    return `<tr class="middle">
                    <td class="text-center">${permission.id}</td>
                    <td class="text-center">${permission.userTypeName}</td>
                    <td class="text-center">${permission.sectorName}</td>
                    <td class="text-center">${permission.created}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            ${actions}
                        </div>
                    </td>
                </tr>`;
}

function remove(id){
    let text = "Deseja realmente remover esta permissão?";
    if (confirm(text) == false) return;
    fetch(`${baseUrl}api/cadastros/remove-permissao/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                window.location.reload();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function resetTable() {
    filter.reset();
    setStorageIndex();
}

$(document).ready(function (){
    verifySession('Permissions');
});