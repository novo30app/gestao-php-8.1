function edit(id, name) {
    document.getElementById('formModal').reset();
    document.getElementById('sectorId').value = "";
    
    if (id > 0) {
        document.getElementById('sectorId').value = id;
        document.getElementById('nameSectorsModal').value = name;
    }

    $('#modal').modal('show');
};

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Sectors');
});

function setStorageIndex() {
    setStorage('Sectors', {
        'url': 'api/cadastros/setores',
        'columns': 6,
        'variables': {
            'index': 0,
            'name': '',
            'limit': 25
        }
    })
};

function generateLines(sector) {
    let actions = "";
    if(canUpdate) {
        actions += `<a onclick="edit(
                        '${sector.id}', 
                        '${sector.name}')">
                        <i class="fa fa-pencil" aria-hidden="true" title="Editar" 
                        style="font-size: 20px; margin-left: 15px;"></i>
                    </a>`;
    }

    if(canDelete) {
        actions += `<a onclick="remove('${sector.id}')">
                        <i class="fa fa-trash" aria-hidden="true" title="Remover" 
                        style="font-size: 20px; margin-left: 15px; color: red"></i>
                    </a>`;
    }
    return `<tr class="middle">
                    <td>${sector.id}</td>
                    <td>${sector.name}</td>
                    <td class="text-center">${sector.created}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            ${actions}
                        </div>
                    </td>
                </tr>`;
}

const formModal = document.getElementById('formModal');
const divMessageForm = document.getElementById('messageForm');
formModal.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('formModal');
    fetch(`${baseUrl}api/cadastros/salva-setor`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(() => {
                    $('#modal').modal('hide');
                    window.location.reload();
                }, 250)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function remove(id){
    let text = "Deseja realmente remover este setor?";
    if (confirm(text) == false) return;
    fetch(`${baseUrl}api/cadastros/remove-setor/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                window.location.reload();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function resetTable() {
    filter.reset();
    setStorageIndex();
}

$(document).ready(function (){
    verifySession('Sectors');
});