function edit(id, name) {
    document.getElementById('formModal').reset();
    document.getElementById('userTypeId').value = "";
    
    if (id > 0) {
        document.getElementById('userTypeId').value = id;
        document.getElementById('nameUserTypeModal').value = name;
    }

    $('#modal').modal('show');
};

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('UserType');
});

function setStorageIndex() {
    setStorage('UserType', {
        'url': 'api/cadastros/tipos-de-usuario',
        'columns': 6,
        'variables': {
            'index': 0,
            'name': '',
            'limit': 25
        }
    })
};

function generateLines(userType) {
    let actions = "";
    if(canUpdate) {
        actions += `<a onclick="edit(
                        '${userType.id}', 
                        '${userType.name}')">
                        <i class="fa fa-pencil" aria-hidden="true" title="Editar" 
                        style="font-size: 20px; margin-left: 15px;"></i>
                    </a>`;
    }
    if(canDelete) {
        actions += `<a onclick="remove('${userType.id}')">
                        <i class="fa fa-trash" aria-hidden="true" title="Remover" 
                        style="font-size: 20px; margin-left: 15px; color: red"></i>
                    </a>`;
    }

    return `<tr class="middle">
                    <td>${userType.id}</td>
                    <td>${userType.name}</td>
                    <td class="text-center">${userType.created}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            ${actions}
                        </div>
                    </td>
                </tr>`;
}

const formModal = document.getElementById('formModal');
const divMessageForm = document.getElementById('messageForm');
formModal.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('formModal');
    fetch(`${baseUrl}api/cadastros/salva-tipo-de-usuario`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        $('#save').removeClass('disabled');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                setTimeout(() => {
                    $('#modal').modal('hide');
                    window.location.reload();
                }, 250)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function remove(id){
    let text = "Deseja realmente remover este tipo de usuário?";
    if (confirm(text) == false) return;
    fetch(`${baseUrl}api/cadastros/remove-tipo-de-usuario/${id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                window.location.reload();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

function resetTable() {
    filter.reset();
    setStorageIndex();
}

$(document).ready(function (){
    verifySession('UserType');
});