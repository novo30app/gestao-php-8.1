function buttonActive(status, url, id, button, method) {
    if (status == 1) {
        return `
            <div class="badge badge-success label-square pointer pointer" onclick="changeActive('${url}', '${id}', '${button}', '${method}')" id="button${button}${id}">
                <span class="f-14">Ativo</span>
            </div>
            `;
    } else if (status == 0) {
        return `
            <div class="badge badge-danger label-square pointer" onclick="changeActive('${url}', '${id}', '${button}', '${method}')" id="button${button}${id}">
               <span class="f-14">Inativo</span>
            </div>
            `;
    }
}

function changeActive(url, id, button, method) {
    if (method == 'delete') {
        $('.spinner-card').show();
        $('.modal-body').css('opacity', .2);
        $('.btn').attr('disabled', true);
    }
    fetch(`${baseUrl}${url}/novo-status/${id}`, {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        response.json().then(json => {
            $('.spinner-card').hide();
            $('.modal-body').css('opacity', 1);
            $('.btn').attr('disabled', false);
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
                if (method == 'put') {
                    if ($('#button' + button + id).hasClass('badge-success')) {
                        $('#button' + button + id).removeClass('badge-success').addClass('badge-danger').empty().html(`<span class="f-14">Inativo</span>`);
                    } else {
                        $('#button' + button + id).removeClass('badge-danger').addClass('badge-success').empty().html(`<span class="f-14">Ativo</span>`);
                    }
                } else if (method == 'delete') {
                    const start = $('#start' + button).text();
                    const end = $('#end' + button).text();
                    const total = $('#total' + button).text();
                    $('#line' + id).hide("slow");
                    if (start === end) {
                        let obj = JSON.parse(sessionStorage.getItem(button));
                        $('#start' + button + ', #end' + button + ', #total' + button).empty().text(0);
                        setTimeout(function () {
                            $("#table" + button + " tbody").empty().append(`<tr><td colspan="${obj.columns}" class="text-center">Nenhum resultado encontrado</td></tr>`);
                        }, 500);
                    }
                    if (end === total) {
                        $('#end' + button + ', #total' + button).empty().text(total - 1);
                    } else {
                        $('#end' + button).empty().text(total - 1);
                    }
                    $('#modalDelete').modal('hide');
                }

            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}
