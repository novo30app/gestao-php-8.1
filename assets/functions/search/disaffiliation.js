var index = 0;
var partial = 0;
var total = 1;
var name = '';
var status = 0;
var state = 0;
var city = 0;
var typeDate = 'data_desfiliacao';
var begin = '';
var end = '';

const form = document.getElementById('filter');

form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    name = formData.name;
    status = formData.status;
    state = formData.state;
    city = formData.city;
    typeDate = formData.typeDate;
    begin = formData.begin;
    end = formData.end;
    $("#table tbody").empty();
    generateTable();
})

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    name = '';
    status = 0;
    state = 0;
    city = 0;
    typeDate = 'data_desfiliacao';
    begin = '';
    end = '';
    $("#table tbody").empty();
    generateTable();
}

function generateMembershipList(affiliates) {
    return `<tr>
                    <td class="text-center">${affiliates.filiado_id}</td>
                    <td><a href="${baseUrl}filiados/${affiliates.id}" target="_blank">${affiliates.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                    <td class="text-center">
                        <a rel="noopener" href="https://api.whatsapp.com/send?phone=${affiliates.phone.replace(/\\D+/g, '')}" target="_blank">
                            ${affiliates.phone}
                       </a>
                    </td> 
                    <td class="text-center">${affiliates.city}</td>
                    <td class="text-center">${affiliates.state}</td>
                    <td class="text-center">${affiliates.filiation}</td>
                    <td class="text-center">${affiliates.disaffiliation}</td>
                    <td class="text-center">${affiliates.reason}</td>
                    <td class="text-center" style="width: 400px;">${affiliates.description}</td>
                </tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/filiados/desfiliacoes/?index=${index}&name=${name}&status=${status}&state=${state}&city=${city}&typeDate=${typeDate}&begin=${begin}&end=${end}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateMembershipList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

$(document).ready(function() {
    if (level == 2) getCities(userState);
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});

function csv() {
    let formData = formDataToJson('filter');
    state = formData.state;
    city = formData.city;
    name = formData.name;
    typeDate = formData.typeDate;
    begin = formData.begin;
    end = formData.end;
    name = formData.name;
    affiliatedId = formData.affiliatedId;
    window.open(
        `${baseUrl}api/filiados/exportar-desfiliacoes/?index=${index}&name=${name}&status=${status}&state=${state}&city=${city}&typeDate=${typeDate}&begin=${begin}&end=${end}`,
        '_blank');
}

if(!term) {
    $(window).on('load', function() {
        $("#modal").modal({
            backdrop: "static", //remove abilidfsaty to close modal with click
            keyboard: false, //remove option to close with keyboard
            show: true //Display loader!
        });
    });
}

const agreementForm = document.getElementById('confidentialityAgreementForm');
const divMessageForm = document.getElementById('messageForm');
agreementForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('confidentialityAgreementForm');
    fetch(`${baseUrl}api/sistema/termo/disaffiliation`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        response.json().then(json => {
            if (response.status === 201) {
                document.getElementById("save").disabled = true;
                $('#modal').modal('hide');
            }
        });
    });
});