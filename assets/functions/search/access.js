$(document).ready(function (){
    verifySession('Access');
})

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Access');
})

function setStorageIndex() {
    setStorage('Access', {
        'url': 'api/cadastros/acessos',
        'columns': 6,
        'variables': {
            'index': 0,
            'name': '',
            'state': '',
            'city': '',
            'limit': 25
        }
    })
}

function generateLines(leaders) {
    return `<tr class="middle">
                    <td>${leaders.created}</td>
                    <td>${leaders.name}</td>
                    <td class="text-center">${leaders.ufStr}</td>
                    <td class="text-center">${leaders.cityStr}</td>
                    <td class="text-center">${leaders.device}</td>
                    <td class="text-center">${leaders.so}</td>
                </tr>`;
}

function showLocations() {
    getCities(document.getElementById('state').value)
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    const x = document.getElementById('level').value;
    if (x == '2') {
        document.getElementById('stateDiv').style.display = 'block';
    } else if (x == '1') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityDiv').style.display = 'block';
    }
}