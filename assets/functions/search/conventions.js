var index = partial = state = city = 0;
var total = 1;
var name = cpf = type = '';
var situation = 'Votante';

const form = document.getElementById('filter');

form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    name = formData.name;
    cpf = formData.cpf;
    state = formData.state;
    city = formData.city;
    situation = formData.situation;
    type = formData.type;
    $("#table tbody").empty();
    generateTable();
})

function resetTable() {
    index = partial = state = city = 0;
    total = 1;
    name = cpf = type = '';
    situation = 'Votante';
    $("#table tbody").empty();
    generateTable();
}

function generateMembershipList(affiliated) {
    let type = '';
    let situationTable = '';
    type = `<button type='button' class='btn btn-success'>Filiação Normal</button>`;
    if (affiliated.type == 'Filiação Isenta') type =
        `<button type='button' class='btn btn-info'>Filiação Isenta</button>`;
    situationTable = `<button type='button' class='btn btn-success'>Votante</button>`;
    if (affiliated.situation == 'Não Votante') situationTable =
        `<button type='button' class='btn btn-danger'>Não Votante</button>`;
    return `<tr>
                    <td><a href="${baseUrl}filiados/${affiliated.id}" target="_blank">${affiliated.name} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                    <td class="text-center">${affiliated.filiado_id}</td>
                    <td class="text-center">${affiliated.status}</td>
                    <td class="text-center">${affiliated.cpf}</td>
                    <td class="text-center">${affiliated.state}</td>
                    <td class="text-center">${affiliated.city}</td>
                    <td class="text-center">${affiliated.diff}</td>   
                    <td class="text-center">${situationTable}</td>
                    <td class="text-center">${type}</td>
                </tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/filiados/convencoes/?index=${index}&name=${name}&cpf=${cpf}&state=${state}&city=${city}&situation=${situation}&type=${type}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateMembershipList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

$(document).ready(function() {
    if (level == 2){
    	getCities(userState);
	}
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    if(exportAccess == 0) {
    	document.getElementById("export").disabled = true;
    	document.getElementById("i").style.display = "";
	} else {
    	document.getElementById("export").disabled = false;
    }
});

function csv() {
    let formData = formDataToJson('filter');
    name = formData.name;
    cpf = formData.cpf;
    state = formData.state;
    city = formData.city;
    situation = formData.situation;
    type = formData.type;
    window.open(
        `${baseUrl}api/filiados/exportCsvConventions/?name=${name}&cpf=${cpf}&state=${state}&city=${city}&situation=${situation}&type=${type}`,
        '_blank');
}

if(!term){
	$(window).on('load', function() {
		$("#modal").modal({
			backdrop: "static", //remove abilidfsaty to close modal with click
			keyboard: false, //remove option to close with keyboard
			show: true //Display loader!
		});
	});
}

const agreementForm = document.getElementById('confidentialityAgreementForm');
const divMessageForm = document.getElementById('messageForm');
agreementForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('confidentialityAgreementForm');
    fetch(`${baseUrl}api/sistema/termo/conventions`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        divMessageForm.classList.add("alert-danger");
        divMessageForm.classList.remove("alert-success");
        response.json().then(json => {
            if (response.status === 201) {
                document.getElementById("save").disabled = true;
                $('#modal').modal('hide');
            }
        });
    });
});

function list() {
    let formData = formDataToJson('filter');
    name = formData.name;
    cpf = formData.cpf;
    state = formData.state;
    city = formData.city;
    situation = formData.situation;
    type = formData.type;
    window.open(
        `${baseUrl}api/filiados/lista-presenca-convencao/?name=${name}&cpf=${cpf}&state=${state}&city=${city}&situation=${situation}&type=${type}`,
        '_blank');
}