$(document).ready(function (){
    sessionStorage.clear();
    verifySession('Defaulters');
    if (stateUser > 0) getCities(stateUser, 'cityDefaultersFilter');
    if (!term) $("#modal").modal('show');
})

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Defaulters');
})

function setStorageIndex() {
    setStorage('Defaulters', {
        'url': 'api/financeiro/adimplencia',
        'columns': 9,
        'variables': {
            'index': 0,
            'name': '',
            'email': '',
            'cpf': '',
            'state': '',
            'city': '',
            'situation': '',
            'meso': '',
            'paymentMethod': '',
            'limit': 25
        }
    })
}

function generateLines(affiliated) {
    let button = '';
    if (affiliated.situation == 'Adimplente') {
        button = `<button type='button' class='btn btn-success'>Adimplente</button>`;
    } else if (affiliated.situation == 'Inadimplente') {
        button = `<button type='button' class='btn btn-danger'>Inadimplente</button>`;
    } else {
        button = `<button type='button' class='btn btn-secondary'>Isento</button>`;
    }
    return `
            <tr class="middle">
                <td><a href="${baseUrl}filiados/${affiliated.id}" target="_blank">${affiliated.nome} <i class="fa fa-external-link" aria-hidden="true"></i></a></td>
                <td class="text-center">${affiliated.filiacao}</td>
                <td class="text-center">${affiliated.plan}</td>
                <td class="text-center">${affiliated.paymentMethod}</td>
                <td class="text-center">${affiliated.opened}</td>
                <td class="text-center">${affiliated.paid}</td>
                <td class="text-center">
                    <a rel="noopener" href="https://api.whatsapp.com/send?phone=${affiliated.phone.replace(/\\D+/g, '')}" target="_blank">
                        ${affiliated.phone}
                    </a>
                </td>    
                <td class="text-center">${affiliated.estado}</td>
                <td class="text-center">${affiliated.cidade}</td>
                <td class="text-center">${button}</td> 
            </tr>`;
}

function csv() {
    let formData = formDataToJson('filter');
    let state = formData.state;
    let city = formData.city;
    let name = formData.name;
    let email = formData.email;
    let cpf = formData.cpf;
    let situation = formData.situation;
    let paymentMethod = formData.paymentMethod;
    window.open(
        `${baseUrl}api/financeiro/exporta-adimplencia/?state=${state}&city=${city}&name=${name}&situation=${situation}&email=${email}&cpf=${cpf}&paymentMethod=${paymentMethod}`,
        '_blank');
}

const agreementForm = document.getElementById('confidentialityAgreementForm');
agreementForm.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('confidentialityAgreementForm');
    fetch(baseUrl + 'api/sistema/termo/defaulters', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                document.getElementById("save").disabled = true;
                $('#modal').modal('hide');
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});