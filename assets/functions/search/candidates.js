$(document).ready(function (){
    verifySession('Candidates');
})

function setStorageIndex() {
    setStorage('Candidates', {
        'url': 'api/cadastros/candidatos',
        'columns': 6,
        'variables': {
            'index': 0,
            'name': '',
            'state': '',
            'city': '',
            'status': '',
            'occupation': '',
            'elected': '',
            'year': '',
            'limit': 25
        }
    })
}

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Candidates');
})

function generateLines(leaders) {
    return `<tr class="middle">
                    <td>${leaders.name}</td>
                    <td>${leaders.email}</td>
                    <td class="text-center">${leaders.status}</td>
                    <td class="text-center">${leaders.disaffiliation}</td>
                    <td class="text-center">${leaders.phone}</td>
                    <td class="text-center">${leaders.occupationStr}</td>
                    <td class="text-center">${leaders.statusStr}</td>
                    <td class="text-center">${leaders.ufStr}</td>
                    <td class="text-center">${leaders.cityStr}</td>
                    <td class="text-center">${leaders.year}</td>
                    <td class="text-center">${leaders.numberOfVotes}</td>
                </tr>`;
}

function showLocations() {
    getCities(document.getElementById('state').value)
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    const x = document.getElementById('level').value;
    if (x == '2') {
        document.getElementById('stateDiv').style.display = 'block';
    } else if (x == '1') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityDiv').style.display = 'block';
    }
}

function csv() {
    let formData = formDataToJson('filter');
    let name = formData.name;
    let state = formData.state;
    let city = formData.city;
    let status = formData.status;
    let occupation = formData.occupation;
    let elected = formData.elected;
    let year = formData.year;
    window.open(
        `${baseUrl}api/cadastros/exporta-candidatos/?name=${name}&state=${state}&city=${city}&status=${status}&occupation=${occupation}&elected=${elected}&year=${year}`, 
        '_blank');
}