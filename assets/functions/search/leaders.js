var index = partial = city = level = occupation = 0;
var status = name = '';
var total = 1;

if (userLevel == 3) {
    var state = cityFilter = 0;
} else if (userLevel == 2) {
    var state = userState;
    var cityFilter = 0;
} else {
    var state = userState;
    var cityFilter = userCity;
}

const form = document.getElementById('filter');

form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    state = formData.stateFilter;
    cityFilter = formData.cityFilter;
    occupation = formData.occupation;
    level = formData.level;
    status = formData.status;
    name = formData.name;
    $("#leadersTable tbody").empty();
    generateTable();
})

function resetTable() {
    index = 0;
    partial = 0;
    total = 1;
    state = 0;
    cityFilter = 0;
    level = 0;
    status = '';
    name = '';
    occupation = 0;
    if (userLevel == 3) {
        state = cityFilter = 0;
    } else if (userLevel == 2) {
        state = userState;
        cityFilter = 0;
    } else {
        state = userState;
        cityFilter = userCity;
    }

    $("#leadersTable tbody").empty();
    generateTable();
}

function generateLeadersTable(leaders) {
    return `<tr>
                <td>${leaders.name}</td>
                <td>${leaders.email}</td>
                <td class="text-center">
                    <a href="https://wa.me/${leaders.phone}" target="_blank">
                        ${leaders.phone} <i class="fa fa-external-link"></i>
                    </a>
                </td>
                <td class="text-center">${leaders.office}</td>
        		<td class="text-center">${leaders.status}</td>
                <td class="text-center">${leaders.type}</td>
                <td class="text-center">${leaders.state}</td>
                <td class="text-center">${leaders.city}</td>
                <td class="text-center">${leaders.mandateBegin}</td>
                <td class="text-center">${leaders.mandateEnd}</td>
            </tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}api/cadastros/dirigentes/?index=${index}&name=${name}&state=${state}&city=${cityFilter}&level=${level}&status=${status}&occupation=${occupation}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                $('#total').text(json.total);
                $('#partial').text(json.partial);
                let options = json.message.map(generateLeadersTable);
                $("#leadersTable tbody").append(options);
            });
        });
    }
}

$(document).ready(function () {
    generateTable();
    if (userLevel == 2) {
        getCities(userState, 'cityFilter');
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});

function showLocations() {
    getCities(document.getElementById('state').value)
    document.getElementById('stateDiv').style.display = 'none';
    document.getElementById('cityDiv').style.display = 'none';
    const x = document.getElementById('level').value;
    if (x == '2') {
        document.getElementById('stateDiv').style.display = 'block';
    } else if (x == '1') {
        document.getElementById('stateDiv').style.display = 'block';
        document.getElementById('cityDiv').style.display = 'block';
    }
}

function csv() {
    let formData = formDataToJson('filter');
    state = formData.state;
    cityFilter = formData.city;
    occupation = formData.occupation;
    level = formData.level;
    status = formData.status;
    name = formData.name;
    window.open(`${baseUrl}api/cadastros/exporta-dirigentes/?index=${index}&name=${name}&state=${state}&city=${cityFilter}&level=${level}&status=${status}&occupation=${occupation}`, '_blank');
}