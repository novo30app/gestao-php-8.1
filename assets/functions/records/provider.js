function openModal(id) {
    form.reset();
    $('#supplierId').val(id);
    $('#modal').modal('show')
    if (id > 0) {
        showLoading('form');
        fetch(baseUrl + 'cadastros/fornecedores/' + id, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message[0]
                const keys = Object.keys(json);

                for (let i = 0; i < keys.length; i++) {
                    const input = document.getElementById(keys[i]);
                    if (input && json[keys[i]] != '') input.value = json[keys[i]];
                }
                if (json.paymentMethod == 1) {
                    $("#billet").prop("checked", true);
                } else {
                    $("#deposit").prop("checked", true);
                }
                closeLoading('form');
            })
        })
    }
}

const form = document.getElementById('form');
form.addEventListener('submit', e => {
    e.preventDefault();
    showLoading('form');
    if (!ValidateForm('form')) {
        closeLoading('form');
        return;
    }
    let formData = formDataToJson('form');
    fetch(baseUrl + 'cadastros/fornecedores/registrar', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading('form');
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                resetTable('Provider');
                setTimeout(function () {
                    $('#modal').modal('hide');
                }, 500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function generateLines(supplier) {
    let actions = ``;
    if (canUpdate) actions += `<a onclick="openModal(${supplier.id})"><i class="fa fa-pencil text-info px-1" title="Editar"></i></a>`;
    if (canDelete) actions += `<i class="fa fa-trash text-danger px-1" title="Excluir" onclick="delElement('cadastros/fornecedores', 'Fornecedor', ${supplier.id}, 'delete', 'Provider')"></i>`;
    return `<tr class="middle" id="line${supplier.id}">
                    <td class="text-center">${supplier.name} </td>
                    <td class="text-center">${supplier.nameResponsible || 'Sem registro'} </td>
                    <td class="text-center">${maskCpfCnpj(supplier.cpfCnpj)} </td>
                    <td class="text-center">${supplier.phone || 'Sem registro'} </td>
                    <td class="text-center">${canUpdate ? buttonActive(supplier.active, 'cadastros/fornecedores', supplier.id, 'Provider', 'put') : ''}</td>
                    <td class="text-center">${actions}</td>
                </tr>`;
}

function getBank(bank) {
    if (bank.length === 3) {
        fetch(baseUrl + 'api/sistema/banco-por-codigo/' + bank, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => {
            response.json().then(json => {
                if (response.status === 200) {
                    document.getElementById('bank').value = json.message;
                } else {
                    showNotify('danger', json.message, 1500);
                }
            });
        });
    }
}

const formFilter = document.getElementById('filter');
formFilter.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('Provider');
})

$(document).ready(function () {
    verifySession('Provider');
});

function setStorageIndex() {
    setStorage('Provider', {
        'url': 'cadastros/fornecedores/listar',
        'columns': 6,
        'variables': {
            'index': 0,
            'cpfCnpj': '',
            'corporateName': '',
            'nameResponsible': '',
            'limit': 25
        }
    })
}

function importProvider() {
    showNotify('success', 'Aguarde! A operação pode demorar alguns minutos', 0);
    fetch(baseUrl + 'cadastros/fornecedores/importar', {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(response => {
        $('.alert').hide();
        response.json().then(json => {
            if (response.status === 200) {
                showNotify('success', json.message, 1500);
                resetTable('Provider')
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}

$("input[name='paymentMethod']").click(function () {
    $('#cod, #bank, #agency, #account').prop('required', false);
    if ($(this).prop('checked')) {
        $("#bankData").hide();
        if ($(this).val() == 2) {
            $('#cod, #bank, #agency, #account').prop('required', true);
            $("#bankData").show();
        }
    }
});

function csv() {
    let formData = formDataToJson('filter');
    nameResponsible = formData.nameResponsible;
    corporateName = formData.corporateName;
    cpfCnpj = formData.cpfCnpj;
    window.open(
        `${baseUrl}cadastros/fornecedores/exportar/?nameResponsible=${nameResponsible}&corporateName=${corporateName}&cpfCnpj=${cpfCnpj}`,
        '_blank');
}