<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

//$app->options('/{routes:.+}', function ($request, $response, $args) {
//    return $response;
//});

$app->group('/api/sistema', function () use ($app) {

    $app->get('/cidades/{state}/', function (Request $request, Response $response) {
        return $this->ApiController->getCitiesByState($request, $response);
    });

    $app->options('/cidades/{state}/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/fornecedores/{cpfCnpj}/[{directory}/]', function (Request $request, Response $response) {
        return $this->ApiController->provider($request, $response);
    });


    $app->post('/termo/{report}/', function (Request $request, Response $response) {
        return $this->UserController->confidentialityAgreement($request, $response);
    });

	$app->get('/banco-por-codigo/{cod}/', function (Request $request, Response $response) {
		return $this->ApiController->bankByCode($request, $response);
	});

    $app->get('/latitude-longitude/', function (Request $request, Response $response) {
        return $this->ApiController->getLatLong($request, $response);
    });

    $app->get('/importar-resultados/csv/', function (Request $request, Response $response) {
        return $this->ApiController->importResults($request, $response);
    });
});



