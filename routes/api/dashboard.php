<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/dashboard', function () use ($app) {

    $app->get('/filiados-por-status/', fn(Request $request, Response $response) => $this->DashboardController->graphicAffiliatedsByStatus($request, $response));
    $app->get('/filiados-por-estado/', fn(Request $request, Response $response) => $this->DashboardController->graphicAffiliatedsByState($request, $response));
    $app->get('/filiados-por-cidade/', fn(Request $request, Response $response) => $this->DashboardController->graphicAffiliatedsByCity($request, $response));
    $app->get('/evolucao/', fn(Request $request, Response $response) => $this->DashboardController->evolution($request, $response));
    $app->get('/ouvidoria-por-status/', fn(Request $request, Response $response) => $this->DashboardController->graphicOmbudsmanByStatus($request, $response));
    $app->get('/ouvidoria-por-destino/', fn(Request $request, Response $response) => $this->DashboardController->graphicOmbudsmanByDestiny($request, $response));
    $app->get('/ouvidoria-por-assunto/', fn(Request $request, Response $response) => $this->DashboardController->graphicOmbudsmanBySubject($request, $response));

});



