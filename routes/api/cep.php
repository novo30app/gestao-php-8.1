<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/cep', function () use ($app) {

    $app->get('/membros/', fn(Request $request, Response $response) => $this->ApiController->membersList($request, $response));

    $app->put('/atualiza-status/{id}/', fn(Request $request, Response $response) => $this->CepController->refreshStatus($request, $response));

    $app->get('/pega-membro/{id}/', fn(Request $request, Response $response) => $this->CepController->getMember($request, $response));

    $app->post('/cadastra-membro/', fn(Request $request, Response $response) => $this->CepController->register($request, $response));

});