<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/area-diretorio', function () use ($app) {

    $app->get('/reunioes/', fn(Request $request, Response $response) => $this->ApiController->meetingsRecords($request, $response));

    $app->get('/edita-registro/[{id}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->editRegister($request, $response));

    $app->get('/edita-financeiro/[{id}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->editFinancial($request, $response));
    
    $app->get('/registro/[{page}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->communicated($request, $response));

    $app->post('/reunioes/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->saveMeetings($request, $response));

    $app->post('/loja/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->saveCommunicatedStore($request, $response));
    
	$app->get('/loja/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->listStore($request, $response));

    $app->post('/salva-comunicado/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->saveCommunicated($request, $response));

    $app->post('/salva-relatorio-financeiro/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->saveFinancial($request, $response));

    $app->post('/salva-arquivo/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->saveFile($request, $response));

    $app->get('/trilha-do-conhecimento-aulas/{module}/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->trailOfKnowledgeClass($request, $response));

    $app->get('/trilha-do-conhecimento/', fn(Request $request, Response $response) => $this->ApiController->trailOfKnowledge($request, $response));

    $app->get('/documentos-locais/', fn(Request $request, Response $response) => $this->ApiController->localDocuments($request, $response));

});
