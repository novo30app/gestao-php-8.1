<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/filiados', function () use ($app) {

    $app->put('/alterar-senha/', fn(Request $request, Response $response) => $this->AffiliatedController->savePassword($request, $response));

    $app->put('/alterar-email/', fn(Request $request, Response $response) => $this->AffiliatedController->saveEmail($request, $response));

    $app->get('/json/', fn(Request $request, Response $response) => $this->ApiController->affiliateds($request, $response));

    $app->get('/convencoes/', fn(Request $request, Response $response) => $this->ApiController->conventions($request, $response));

    $app->get('/lista-presenca-convencao/', fn(Request $request, Response $response) => $this->ApiController->listPresenceConventions($request, $response));

    $app->get('/consulta/', fn(Request $request, Response $response) => $this->ApiController->quiz($request, $response));

    $app->get('/consulta/csv/', fn(Request $request, Response $response) => $this->AffiliatedController->quizCsv($request, $response));

    $app->get('/desfiliacoes/', fn(Request $request, Response $response) => $this->ApiController->disaffiliation($request, $response));

    $app->get('/exportar-desfiliacoes/', fn(Request $request, Response $response) => $this->ApiController->exportCsvDisaffiliation($request, $response));

    $app->get('/doadores/', fn(Request $request, Response $response) => $this->ApiController->donors($request, $response));

    $app->get('/exportar-doadores/', fn(Request $request, Response $response) => $this->ApiController->exportCsvDonors($request, $response));

    $app->get('/exportCsv/', fn(Request $request, Response $response) => $this->ApiController->exportCsv($request, $response));

    $app->get('/exportCsvCobranca/', fn(Request $request, Response $response) => $this->ApiController->exportCsvOverdueChanges($request, $response));

    $app->get('/exportCsvConventions/', fn(Request $request, Response $response) => $this->ApiController->exportCsvConventions($request, $response));

    $app->get('/como-conheceu/', fn(Request $request, Response $response) => $this->ApiController->knowledge($request, $response));

    $app->get('/quem-indicou/', fn(Request $request, Response $response) => $this->ApiController->whoIndicated($request, $response));

    $app->get('/exportComoConheceu/', fn(Request $request, Response $response) => $this->ApiController->exportCsvKnowledge($request, $response));

    $app->get('/exporta-quem-indicou/', fn(Request $request, Response $response) => $this->ApiController->exportCsvWhoIndicated($request, $response));

    $app->get('/solicitations/', fn(Request $request, Response $response) => $this->ApiController->solicitations($request, $response));
    
    $app->get('/ultimas-filiacoes/', fn(Request $request, Response $response) => $this->ApiController->latestAffiliations($request, $response));

    $app->get('/agendamento/[{id}/]', fn(Request $request, Response $response) => $this->AppointmentController->getAppointments($request, $response));

    $app->get('/refiliacoes/', fn(Request $request, Response $response) => $this->ApiController->reaffiliation($request, $response));

    $app->put('/aprova-refiliacoes/{id}/{button}/', fn(Request $request, Response $response) => $this->UserController->Actions($request, $response));

    $app->post('/agendamento/', fn(Request $request, Response $response) => $this->AppointmentController->makeAnAppointment($request, $response));

    $app->post('/editar/', function (Request $request, Response $response) {
        return $this->AffiliatedController->savePersonalData($request, $response);
    });

    $app->post('/impugnar/', function (Request $request, Response $response) {
        return $this->AffiliatedController->saveImpeachmentData($request, $response);
    });

    $app->put('/blacklist/{id}/', function (Request $request, Response $response) {
        return $this->AffiliatedController->addBlackList($request, $response);
    });

    $app->get('/cidade/{city}/', fn(Request $request, Response $response) => $this->ApiController->affiliatedsByCity($request, $response));

});

