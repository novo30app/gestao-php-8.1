<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/financeiro', function () use ($app) {

    $app->post('/pagamentos/', function (Request $request, Response $response) {
        return $this->FinancialController->savePayment($request, $response);
    });

    $app->get('/transacoes/', function (Request $request, Response $response) {
        return $this->ApiController->transactions($request, $response);
    });

    $app->get('/orcamentos-por-centro-de-custo/', function (Request $request, Response $response) {
        return $this->ApiController->budgetByCostCenter($request, $response);
    });

    $app->get('/orcamentos/', function (Request $request, Response $response) {
        return $this->ApiController->budgets($request, $response);
    });

    $app->get('/orcamentos-detalhes/', function (Request $request, Response $response) {
        return $this->ApiController->budgetsDetails($request, $response);
    });

    $app->get('/orcamentos/{id}/', function (Request $request, Response $response) {
        return $this->ApiController->budgetData($request, $response);
    });

    $app->post('/orcamentos/cadastrar/', function (Request $request, Response $response) {
        return $this->FinancialController->saveBudgets($request, $response);
    });

    $app->get('/pagamentos/', function (Request $request, Response $response) {
        return $this->FinancialController->paymenstList($request, $response);
    });

    $app->get('/adimplencia/', function (Request $request, Response $response) {
        return $this->ApiController->defaulters($request, $response);
    });

    $app->get('/sub-areas-por-area/{id}/', fn(Request $request, Response $response) => $this->FinancialController->subAreaByArea($request, $response));

    $app->get('/centro-de-custo/diretorio/[{directory}/]', fn(Request $request, Response $response) => $this->ApiController->costCenterByDirectory($request, $response));

    $app->get('/centro-de-custo/[{id}/]', fn(Request $request, Response $response) => $this->FinancialController->costCenterList($request, $response));

    $app->post('/centro-de-custo/cadastrar/', fn(Request $request, Response $response) => $this->FinancialController->costCenterSave($request, $response));

    $app->delete('/centro-de-custo/novo-status/{id}/', fn(Request $request, Response $response) => $this->FinancialController->costCenterDelete($request, $response));

    $app->get('/contratos/exportar/', fn(Request $request, Response $response) => $this->ApiController->exportContracts($request, $response));

    $app->get('/contratos/[{id}/]', fn(Request $request, Response $response) => $this->FinancialController->contractsList($request, $response));

    $app->get('/detalhe-contrato/{id}/', fn(Request $request, Response $response) => $this->FinancialController->detailContract($request, $response));

    $app->post('/contratos/cadastrar/', fn(Request $request, Response $response) => $this->FinancialController->contractsSave($request, $response));

    $app->delete('/contratos/novo-status/{id}/', fn(Request $request, Response $response) => $this->FinancialController->contractsDelete($request, $response));

    $app->get('/area/', fn(Request $request, Response $response) => $this->FinancialController->areaList($request, $response));

    $app->get('/area/{id}/', fn(Request $request, Response $response) => $this->FinancialController->areaData($request, $response));

    $app->get('/area-por-diretorio/{id}/', fn(Request $request, Response $response) => $this->ApiController->areaByDirectory($request, $response));

    $app->post('/area/cadastrar/', fn(Request $request, Response $response) => $this->FinancialController->areaSave($request, $response));

    $app->get('/sub-area/[{id}/]', fn(Request $request, Response $response) => $this->FinancialController->subAreaList($request, $response));

    $app->post('/sub-area/cadastrar/', fn(Request $request, Response $response) => $this->FinancialController->subAreaSave($request, $response));

    $app->get('/exporta-adimplencia/', fn(Request $request, Response $response) => $this->ApiController->exportCsvDefaulters($request, $response));

    $app->get('/exportCsvTransactions/', fn(Request $request, Response $response) => $this->ApiController->exportCsvTransactions($request, $response));

    $app->get('/exportCsvFinancialReport/', fn(Request $request, Response $response) => $this->ApiController->exportCsvFinancialReport($request, $response));

    $app->get('/assinaturas/{id}/', fn(Request $request, Response $response) => $this->FinancialController->apiSubscriptions($request, $response));

    $app->post('/assinaturas/', fn(Request $request, Response $response) => $this->FinancialController->saveSubscription($request, $response));

    $app->put('/assinaturas/', fn(Request $request, Response $response) => $this->FinancialController->saveSubscription($request, $response));

    $app->delete('/cartoes/{id}/', fn(Request $request, Response $response) => $this->FinancialController->stopCard($request, $response));

    $app->put('/cartoes/ativar/{id}/', fn(Request $request, Response $response) => $this->FinancialController->activateCard($request, $response));

    $app->post('/pagamento/', fn(Request $request, Response $response) => $this->FinancialController->chargeTransaction($request, $response));

    $app->put('/pagamentos/aprovar/{id}/', fn(Request $request, Response $response) => $this->FinancialController->approvePayment($request, $response));

    $app->get('/extrato/{id}/', fn(Request $request, Response $response) => $this->FinancialController->apiExtract($request, $response));

    $app->put('/extrato/', fn(Request $request, Response $response) => $this->FinancialController->saveExtract($request, $response));

    $app->get('/plano-de-contas/', fn(Request $request, Response $response) => $this->ApiController->chartOfAccounts($request, $response));

    $app->get('/conciliacao-iugu/', fn(Request $request, Response $response) => $this->FinancialController->conciliationIugu($request, $response));

    $app->get('/conciliacao/extrato/json/', fn(Request $request, Response $response) => $this->FinancialController->listExtract($request, $response));

    $app->delete('/altera-conta/{id}/{status}/', fn(Request $request, Response $response) => $this->FinancialController->changeAccount($request, $response));

    $app->get('/consulta-conta/{id}/', fn(Request $request, Response $response) => $this->FinancialController->getAccount($request, $response));

    $app->get('/exportCsvAccounts/', fn(Request $request, Response $response) => $this->ApiController->exportCsvAccounts($request, $response));

    $app->get('/transacoes-resgate/{rescue}/', fn(Request $request, Response $response) => $this->ApiController->exportTransactionsRescue($request, $response));

    $app->get('/exportBillsToPay/[{diretiva}/]', fn(Request $request, Response $response) => $this->ApiController->exportBillsToPay($request, $response));

    $app->get('/contas-diretorios/', fn(Request $request, Response $response) => $this->ApiController->directoryAccounts($request, $response));

    $app->get('/extrato-diretorios/', fn(Request $request, Response $response) => $this->ApiController->extractAccounts($request, $response));

    $app->get('/exporta-contas-diretorios/', fn(Request $request, Response $response) => $this->ApiController->exportDirectoryAccounts($request, $response));

    $app->get('/exporta-extratos/', fn(Request $request, Response $response) => $this->ApiController->exportExtract($request, $response));

    $app->get('/contas/', fn(Request $request, Response $response) => $this->FinancialController->getDirectoryAccounts($request, $response));

    $app->get('/contas-por-diretorio/', fn(Request $request, Response $response) => $this->FinancialController->accountsByDirectory($request, $response, false));

    $app->get('/contas-por-diretorio-superior/', fn(Request $request, Response $response) => $this->FinancialController->accountsByDirectory($request, $response, true));

    $app->get('/transacoes-originarios/', fn(Request $request, Response $response) => $this->FinancialController->originatingTransactions($request, $response));

    $app->post('/split/', fn(Request $request, Response $response) => $this->FinancialController->splitTransactions($request, $response));

    $app->get('/exporta-originarios/', fn(Request $request, Response $response) => $this->ApiController->exportOriginatingTransaction($request, $response));

    $app->post('/cnpj-gru/', fn(Request $request, Response $response) => $this->FinancialController->getCnpjGru($request, $response));

    $app->get('/exporta-relatorio-contabil/', fn(Request $request, Response $response) => $this->ApiController->exportAccountReport($request, $response));

    $app->get('/exporta-relatorio-fiscal/', fn(Request $request, Response $response) => $this->ApiController->exportFiscalReport($request, $response));

    $app->get('/aprovacao/', fn(Request $request, Response $response) => $this->ApiController->tableApproved($request, $response));

    $app->post('/aprovacao/', fn(Request $request, Response $response) => $this->FinancialController->updateApproved($request, $response));

    $app->post('/ofx/', fn(Request $request, Response $response) => $this->FinancialController->saveOfx($request, $response));

    $app->get('/pendencias-ofx/json/', fn(Request $request, Response $response) => $this->ApiController->listOfxPendencies($request, $response));

    $app->post('/ofx/{id}/', fn(Request $request, Response $response) => $this->FinancialController->refreshOfx($request, $response));

    $app->post('/vincular-ofx-requerimentos/', fn(Request $request, Response $response) => $this->FinancialController->linkOfxRequeriments($request, $response));
    
    $app->put('/remover-conciliacao/', fn(Request $request, Response $response) => $this->FinancialController->removeConciliation($request, $response));

    $app->get('/macro-despesas/', fn(Request $request, Response $response) => $this->FinancialController->getDebits($request, $response));

    $app->post('/chatbot/registra/', fn(Request $request, Response $response) => $this->RegistrationController->chatbotSave($request, $response));

    $app->put('/verifica-numero-nf/{nf}/{providerDoc}/', fn(Request $request, Response $response) => $this->FinancialController->checkNfNumber($request, $response));

    $app->post('/envia-pagamento-bb/', fn(Request $request, Response $response) => $this->FinancialController->launchPayments($request, $response));

    $app->post('/libera-pagamento-bb/{id}/', fn(Request $request, Response $response) => $this->FinancialController->liberationPayments($request, $response));

    $app->post('/cancela-pagamento-bb/{id}/', fn(Request $request, Response $response) => $this->FinancialController->cancelPayments($request, $response));

    $app->get('/pesquisa-pagamentos-bb/{id}/', fn(Request $request, Response $response) => $this->FinancialController->checksDoublePayment($request, $response));

    $app->get('/exporta-relatorio-financeiro/', fn(Request $request, Response $response) => $this->ApiController->exportCsvFinancialReport($request, $response));
});