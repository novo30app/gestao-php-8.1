<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/cadastros', function () use ($app) {

    $app->get('/usuarios/[{id}/]', fn(Request $request, Response $response) => $this->ApiController->users($request, $response));

    $app->put('/usuarios/{id}/', fn(Request $request, Response $response) => $this->UserController->changeActive($request, $response, 1));

    $app->delete('/usuarios/{id}/', fn(Request $request, Response $response) => $this->UserController->changeActive($request, $response, 0));

    $app->post('/usuarios/', fn(Request $request, Response $response) => $this->UserController->save($request, $response));

    $app->put('/usuarios/', fn(Request $request, Response $response) => $this->UserController->save($request, $response));

    $app->post('/dirigentes/', fn(Request $request, Response $response) => $this->RegistrationController->saveLeaders($request, $response));

    $app->get('/dirigentes/[{id}/]', fn(Request $request, Response $response) => $this->ApiController->leaders($request, $response));

    $app->get('/exporta-dirigentes/', fn(Request $request, Response $response) => $this->ApiController->exportLeaders($request, $response));

    $app->get('/candidatos/', fn(Request $request, Response $response) => $this->ApiController->candidates($request, $response));

    $app->get('/exporta-candidatos/', fn(Request $request, Response $response) => $this->ApiController->exportCandidates($request, $response));

    $app->get('/acessos/', fn(Request $request, Response $response) => $this->ApiController->access($request, $response));

    $app->post('/diretorios/', fn(Request $request, Response $response) => $this->RegistrationController->saveDirectories($request, $response));

    $app->put('/diretorios/', fn(Request $request, Response $response) => $this->RegistrationController->saveDirectories($request, $response));

    $app->get('/diretorios/[{id}/]', fn(Request $request, Response $response) => $this->ApiController->directories($request, $response));

    $app->get('/gestao-cadastros/', fn(Request $request, Response $response) => $this->ApiController->managementRecords($request, $response));

    $app->get('/gestao-comunicados/', fn(Request $request, Response $response) => $this->ApiController->managementCommunicated($request, $response));

    $app->post('/salva-comunicado/', fn(Request $request, Response $response) => $this->CommunicatedController->save($request, $response));

    $app->put('/desativa-comunicado/{id}/', fn(Request $request, Response $response) => $this->CommunicatedController->DisableCommunicated($request, $response));

    $app->post('/salva-comentario/', fn(Request $request, Response $response) => $this->CommentController->save($request, $response));

    $app->put('/acoes/{id}/{button}/{action}/', fn(Request $request, Response $response) => $this->UserController->Actions($request, $response));

    $app->post('/desfiliar/', fn(Request $request, Response $response) => $this->UserController->disaffection($request, $response));

    $app->post('/reclassificar/', fn(Request $request, Response $response) => $this->UserController->reclassification($request, $response));

    $app->post('/cep/', fn(Request $request, Response $response) => $this->UserController->cep($request, $response));

    $app->post('/novo-registro/', fn(Request $request, Response $response) => $this->RegistrationController->saveNewRegister($request, $response));

    $app->get('/mesorregioes/', fn(Request $request, Response $response) => $this->ApiController->mesoregions($request, $response));

    $app->get('/mesorregioes-cidades/', fn(Request $request, Response $response) => $this->ApiController->mesoregionsCities($request, $response));

    $app->post('/mesorregiao/', fn(Request $request, Response $response) => $this->RegistrationController->saveMesoregion($request, $response));

    $app->get('/mesos/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->getMesos($request, $response));

    $app->post('/salva-trilha-do-conhecimento/', fn(Request $request, Response $response) => $this->RegistrationController->saveTrailOfKnowledge($request, $response));

    $app->delete('/altera-trilha-do-conhecimento/novo-status/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->updateTrailOfKnowledge($request, $response));

    $app->get('/pega-trilha-do-conhecimento/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->getTrailOfKnowledge($request, $response));

    $app->post('/documentos-locais/', fn(Request $request, Response $response) => $this->RegistrationController->saveLocalDocuments($request, $response));

    $app->get('/pega-documento-local/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->getLocalDocument($request, $response));

    $app->delete('/altera-documentos-locais/novo-status/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->updateLocalDocument($request, $response));

    $app->get('/pega-nome/{cpf}/', fn(Request $request, Response $response) => $this->UserController->getName($request, $response));

    $app->get('/pega-centro-custo-contrato/{contract}/', fn(Request $request, Response $response) => $this->UserController->getContractCostCenter($request, $response));

    $app->get('/eleitos/', fn(Request $request, Response $response) => $this->ApiController->elected($request, $response));

    $app->get('/exporta-eleitos/', fn(Request $request, Response $response) => $this->ApiController->electedExport($request, $response));

    $app->post('/salva-eleitos/', fn(Request $request, Response $response) => $this->RegistrationController->electedSave($request, $response));

    $app->put('/remove-eleitos/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->electedRemove($request, $response));

    $app->post('/importa-eleitos/', fn(Request $request, Response $response) => $this->RegistrationController->electedImport($request, $response));
   
    $app->get('/setores/', fn(Request $request, Response $response) => $this->ApiController->sectors($request, $response));

    $app->post('/salva-setor/', fn(Request $request, Response $response) => $this->RegistrationController->sectorSave($request, $response));

    $app->put('/remove-setor/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->sectorRemove($request, $response));

    $app->get('/funcionalidades/', fn(Request $request, Response $response) => $this->ApiController->features($request, $response));

    $app->post('/salva-funcionalidade/', fn(Request $request, Response $response) => $this->RegistrationController->featureSave($request, $response));

    $app->put('/remove-funcionalidade/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->featureRemove($request, $response));

    $app->get('/tipos-de-usuario/', fn(Request $request, Response $response) => $this->ApiController->usersType($request, $response));

    $app->post('/salva-tipo-de-usuario/', fn(Request $request, Response $response) => $this->RegistrationController->userTypeSave($request, $response));

    $app->put('/remove-tipo-de-usuario/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->userTypeRemove($request, $response));

    $app->get('/permissoes/', fn(Request $request, Response $response) => $this->ApiController->permissions($request, $response));

    $app->post('/salva-permissao/', fn(Request $request, Response $response) => $this->RegistrationController->permissionSave($request, $response));

    $app->put('/remove-permissao/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->permissionRemove($request, $response));

    $app->post('/usuarios-funcionalidades/', fn(Request $request, Response $response) => $this->RegistrationController->usersPermissionSave($request, $response));

    $app->put('/usuarios-funcionalidades/', fn(Request $request, Response $response) => $this->RegistrationController->usersPermissionSave($request, $response));

    $app->get('/pega-usuario/{id}/', fn(Request $request, Response $response) => $this->RegistrationController->getUserData($request, $response));

    $app->get('/colapso-2025/', fn(Request $request, Response $response) => $this->ApiController->exportCollapse2025($request, $response));

});