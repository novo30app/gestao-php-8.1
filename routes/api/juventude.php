<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/juventude', function () use ($app) {
    $app->post('/consulta/', fn(Request $request, Response $response) => $this->YouthController->interactionSave($request, $response));
    $app->get('/consulta/json/', fn(Request $request, Response $response) => $this->YouthController->searchList($request, $response));
    $app->get('/consulta/{id}/', fn(Request $request, Response $response) => $this->YouthController->searchView($request, $response));
});