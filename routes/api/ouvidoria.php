<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/ouvidoria', function () use ($app) {

    $app->get('/categorias/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->getCategories($request, $response);
    });

    $app->get('/categorias/mostra-usuarios/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->getUserAdmin($request, $response);
    });

    $app->put('/categorias/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->saveCategory($request, $response);
    });

    $app->put('/redirecionar/{id}/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->redirectOmbudsmanCategory($request, $response);
    });

    $app->post('/categorias/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->saveCategory($request, $response);
    });

    $app->put('/categorias/{id}/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->changeCategoryStatus($request, $response, 1);
    });

    $app->delete('/categorias/{id}/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->changeCategoryStatus($request, $response, 0);
    });

    $app->get('/respostas/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->getAnswers($request, $response);
    });

    $app->put('/respostas/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->saveAnswer($request, $response);
    });

    $app->post('/respostas/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->saveAnswer($request, $response);
    });

    $app->put('/respostas/{id}/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->changeAnswerStatus($request, $response, 1);
    });

    $app->delete('/respostas/{id}/', function (Request $request, Response $response) {
        return $this->OmbudsmanController->changeAnswerStatus($request, $response, 0);
    });

    $app->get('/solicitacoes/[{id}/]', function (Request $request, Response $response) {
        return $this->OmbudsmanController->requestsTable($request, $response);
    });
});