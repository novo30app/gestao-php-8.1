<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/dam', function () use ($app) {
    $app->post('/assessores/', fn(Request $request, Response $response) => $this->DamController->registerUser($request, $response));
    $app->put('/assessores/', fn(Request $request, Response $response) => $this->DamController->registerUser($request, $response));
    $app->get('/assessores/{id}/', fn(Request $request, Response $response) => $this->DamController->getUsers($request, $response));
    $app->post('/gabinete/', fn(Request $request, Response $response) => $this->DamController->registerCabinet($request, $response));
    $app->get('/gabinetes/', fn(Request $request, Response $response) => $this->ApiController->getCabinets($request, $response));
    $app->get('/tags/', fn(Request $request, Response $response) => $this->DamController->getTags($request, $response));
    $app->post('/tags/', fn(Request $request, Response $response) => $this->DamController->registerTag($request, $response));
    $app->put('/tags/', fn(Request $request, Response $response) => $this->DamController->registerTag($request, $response));
    $app->post('/dossie/', fn(Request $request, Response $response) => $this->DamController->saveDossie($request, $response));
    $app->get('/interacoes/{id}/', fn(Request $request, Response $response) => $this->DamController->interactionsView($request, $response));
    $app->get('/interacoes/', fn(Request $request, Response $response) => $this->DamController->interactionsList($request, $response));
    $app->post('/interacoes/', fn(Request $request, Response $response) => $this->DamController->interactionSave($request, $response));
    $app->post('/interacoes/reuniao/', fn(Request $request, Response $response) => $this->DamController->meetingSave($request, $response));
});