<?php
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/eventos', function () use ($app) {

    $app->get('/json/', fn(Request $request, Response $response) => $this->ApiController->eventsFilter($request, $response));

    $app->get('/participantes/', fn(Request $request, Response $response) => $this->ApiController->RegistrationsFilter($request, $response));

    $app->get('/checkin/', fn(Request $request, Response $response) => $this->ApiController->CheckinFilter($request, $response));

    $app->get('/exportCsvPress/', fn(Request $request, Response $response) => $this->ApiController->exportCsvPress($request, $response));
    
    $app->get('/exporta-participantes/', fn(Request $request, Response $response) => $this->ApiController->exportCsvEventsRegistrations($request, $response));

    $app->get('/exporta-participantes-transacoes/', fn(Request $request, Response $response) => $this->ApiController->exportEventsTransactions($request, $response));

    $app->get('/cupons/', fn(Request $request, Response $response) => $this->ApiController->coupons($request, $response));

    $app->get('/imprensa/', fn(Request $request, Response $response) => $this->ApiController->press($request, $response));

    $app->put('/cancela/{id}/', function (Request $request, Response $response) {
        return $this->EventsController->CancelEvents($request, $response);
    });

    $app->put('/faz-checkin/{id}/', function (Request $request, Response $response) {
        return $this->EventsController->doCheckin($request, $response);
    });

    $app->put('/valida-comprovante/{id}/', function (Request $request, Response $response) {
        return $this->EventsController->validadeVoucher($request, $response);
    });

    $app->post('/salva-evento-pago/', function (Request $request, Response $response) {
        return $this->EventsController->SaveEventPaid($request, $response);
    });

    $app->post('/salva-evento-livre/', function (Request $request, Response $response) {
        return $this->EventsController->SaveEventFree($request, $response);
    });

    $app->post('/novo-participante/', function (Request $request, Response $response) {
        return $this->EventsController->NewParticipant($request, $response);
    });

    $app->post('/confere-cupom/{coupon}/{event}/', fn(Request $request, Response $response) => $this->EventsController->checkCoupon($request, $response));

    $app->get('/participantes-geral/', fn(Request $request, Response $response) => $this->ApiController->allRegistrations($request, $response));

    $app->get('/participantes-geral-exporta/', fn(Request $request, Response $response) => $this->ApiController->allRegistrationsExport($request, $response));

});