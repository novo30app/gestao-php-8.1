<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/seletivo-2022', function () use ($app) {
    $app->get('/modulo-1/questoes/{id}/', fn(Request $request, Response $response) => $this->SelectiveController->apiSelective2022Module1($request, $response));
    $app->get('/modulo-1/{id}/', fn(Request $request, Response $response) => $this->SelectiveController->apiSelective2022Module1Questions($request, $response));
});