<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/missoes-realizadas', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->UserPointController->missions($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->UserPointController->list($request, $response));
    $app->get('/exportar/', fn(Request $request, Response $response) => $this->UserPointController->export($request, $response));
});
