<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/eventos', function () use ($app) {

    $app->get('/', fn (Request $request, Response $response) => $this->EventsController->events($request, $response));

    $app->get('/adicionar-evento/', fn (Request $request, Response $response) => $this->EventsController->NewEvent($request, $response));

    $app->get('/editar-evento/{id}/', fn (Request $request, Response $response) => $this->EventsController->editEvent($request, $response));

    $app->get('/checkin/{id}/', fn (Request $request, Response $response) => $this->EventsController->checkin($request, $response));

    $app->get('/inscricoes/{id}/', fn (Request $request, Response $response) => $this->EventsController->registrations($request, $response));

    $app->get('/transacoes/{id}/', fn (Request $request, Response $response) => $this->EventsController->transactions($request, $response));

    $app->get('/documentos/{id}/', fn(Request $request, Response $response) => $this->EventsController->documents($request, $response));

	$app->get('/informacoes/{id}/', fn(Request $request, Response $response) => $this->EventsController->infomationsEvent($request, $response));
 
    $app->get('/cupons/{id}/', fn(Request $request, Response $response) => $this->EventsController->coupons($request, $response));

    $app->post('/salva-cupom/', fn(Request $request, Response $response) => $this->EventsController->saveCoupons($request, $response));

    $app->put('/altera-cupom/{id}/{status}/', fn(Request $request, Response $response) => $this->EventsController->changeCoupons($request, $response));

	$app->post('/salva-documento/', fn(Request $request, Response $response) => $this->EventsController->saveDocuments($request, $response));
 
    $app->get('/imprensa/{id}/', fn (Request $request, Response $response) => $this->EventsController->press($request, $response));

    $app->get('/participantes-geral/', fn (Request $request, Response $response) => $this->EventsController->allRegistrations($request, $response));

});