<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/beneficios', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->BenefitController->index($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->BenefitController->save($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->BenefitController->list($request, $response));
    $app->get('/seeder/', fn(Request $request, Response $response) => $this->BenefitController->seeder($request, $response));
    $app->get('/{id}/', fn(Request $request, Response $response) => $this->BenefitController->show($request, $response));
    $app->put('/novo-status/{id}/', fn(Request $request, Response $response) => $this->BenefitController->changeActive($request, $response));
    $app->delete('/{id}/', fn(Request $request, Response $response) => $this->BenefitController->destroy($request, $response));

});
