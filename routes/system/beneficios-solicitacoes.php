<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/beneficios-solicitacoes', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->UserPointController->index($request, $response));
    $app->post('/alterar-status/', fn(Request $request, Response $response) => $this->UserPointController->changeStatus($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->UserPointController->list($request, $response));
    $app->get('/exportar/', fn(Request $request, Response $response) => $this->UserPointController->csv($request, $response));
    $app->get('/{id}/', fn(Request $request, Response $response) => $this->UserPointController->show($request, $response));
});
