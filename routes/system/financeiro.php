<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/financeiro', function () use ($app) {

    $app->get('/orcamentos/', fn(Request $request, Response $response) => $this->FinancialController->budgets($request, $response));

    $app->get('/orcamentos-detalhes/', fn(Request $request, Response $response) => $this->FinancialController->budgetsDetails($request, $response));

    $app->delete('/orcamentos/novo-status/{id}/', fn(Request $request, Response $response) => $this->FinancialController->deleteBudget($request, $response));

    $app->get('/gerenciamento/', fn(Request $request, Response $response) => $this->FinancialController->management($request, $response, 1));

    $app->get('/conciliacao-rede/', fn(Request $request, Response $response) => $this->FinancialController->conciliationRede($request, $response, 1));

    $app->post('/conciliacao-rede/', fn(Request $request, Response $response) => $this->FinancialController->conciliationRede($request, $response, 1));

    $app->get('/pagamentos/', fn(Request $request, Response $response) => $this->FinancialController->billsToPay($request, $response, 1));

    $app->get('/diretiva/', fn(Request $request, Response $response) => $this->FinancialController->billsToPay($request, $response, 3));

    $app->get('/centro-de-custo/', fn(Request $request, Response $response) => $this->FinancialController->costCenter($request, $response));

    $app->get('/contratos/', fn(Request $request, Response $response) => $this->FinancialController->contracts($request, $response));

    $app->get('/contratos/{id}/', fn(Request $request, Response $response) => $this->FinancialController->contractsView($request, $response));

    $app->delete('/contratos/novo-status/{id}/', fn(Request $request, Response $response) => $this->FinancialController->delete($request, $response));

    $app->get('/adimplencia/', fn(Request $request, Response $response) => $this->FinancialController->defaulters($request, $response));

    $app->get('/contas-a-pagar/', fn(Request $request, Response $response) => $this->FinancialController->billsToPay($request, $response));

    $app->get('/contas-a-pagar/{id}/[{diretiva}/]', fn(Request $request, Response $response) => $this->FinancialController->viewBillToPay($request, $response));

    $app->post('/contas-a-pagar/{id}/', fn(Request $request, Response $response) => $this->FinancialController->updatePaymentStatus($request, $response));

    $app->get('/contas-a-receber/', fn(Request $request, Response $response) => $this->FinancialController->billsToReceive($request, $response));

    $app->get('/pagamentos/novo-pagamento/', fn(Request $request, Response $response) => $this->FinancialController->addPayments($request, $response));

    $app->get('/pagamentos/{id}/', fn(Request $request, Response $response) => $this->FinancialController->viewRequeriment($request, $response, 1));

    $app->get('/pagamentos/duplicar/{id}/', fn(Request $request, Response $response) => $this->FinancialController->viewRequeriment($request, $response, 2));

    $app->get('/transacoes/', fn(Request $request, Response $response) => $this->FinancialController->transactions($request, $response));

    $app->get('/relatorio-financeiro/', fn(Request $request, Response $response) => $this->FinancialController->financialReport($request, $response));

    $app->get('/csv/', fn(Request $request, Response $response) => $this->FinancialController->transactionsCsv($request, $response));

    $app->get('/recibos/{id}/', fn(Request $request, Response $response) => $this->FinancialController->receipt($request, $response));

    $app->post('/antecipacao/', fn(Request $request, Response $response) => $this->FinancialController->antecipation($request, $response));

    $app->post('/quitacao/', fn(Request $request, Response $response) => $this->FinancialController->payoff($request, $response));

    $app->post('/importa-planilha-financeiro/', fn(Request $request, Response $response) => $this->FinancialController->importFinancialReport($request, $response));

    $app->get('/plano-de-contas/', fn(Request $request, Response $response) => $this->FinancialController->chartOfAccounts($request, $response));

    $app->post('/salva-conta/', fn(Request $request, Response $response) => $this->FinancialController->SaveAccounts($request, $response));

    $app->get('/conciliacao-iugu/', fn(Request $request, Response $response) => $this->FinancialController->conciliationIugu($request, $response));

    $app->post('/conciliacao-iugu/', fn(Request $request, Response $response) => $this->FinancialController->conciliationIugu2($request, $response));

    $app->post('/conciliacao-resgate/', fn(Request $request, Response $response) => $this->FinancialController->conciliationRescue($request, $response));

    $app->post('/importa-contas/', fn(Request $request, Response $response) => $this->FinancialController->importAccounts($request, $response));

    $app->get('/recibos/', fn(Request $request, Response $response) => $this->FinancialController->receipts($request, $response));

    $app->post('/gera-recibo/', fn(Request $request, Response $response) => $this->FinancialController->generateReceipts($request, $response));

    $app->post('/lanca-recibos/', fn(Request $request, Response $response) => $this->FinancialController->importReceipts($request, $response));

    $app->post('/lanca-doacoes/', fn(Request $request, Response $response) => $this->FinancialController->importDonations($request, $response));

    $app->post('/registro-contabil/', fn(Request $request, Response $response) => $this->FinancialController->accountingRegister($request, $response));

    $app->get('/contas-diretorios/', fn(Request $request, Response $response) => $this->FinancialController->directoryAccounts($request, $response, 1));

    $app->get('/contas-diretorios/diretiva/', fn(Request $request, Response $response) => $this->FinancialController->directoryAccounts($request, $response, 3));

    $app->get('/extrato-diretorios/{account}/', fn(Request $request, Response $response) => $this->FinancialController->directoryExtract($request, $response, 1));

    $app->get('/extrato-diretorios/diretiva/{account}/', fn(Request $request, Response $response) => $this->FinancialController->directoryExtract($request, $response, 3));

    $app->get('/aprovacao/', fn(Request $request, Response $response) => $this->FinancialController->approved($request, $response));

    $app->get('/conciliacao/importar/', fn(Request $request, Response $response) => $this->FinancialController->ofx($request, $response));

    $app->get('/conciliacao/pendentes/[{id}/]', fn(Request $request, Response $response) => $this->FinancialController->ofxPendency($request, $response));

    $app->post('/conciliacao/pendentes/', fn(Request $request, Response $response) => $this->FinancialController->ofxPendencyConciliation($request, $response));
    $app->post('/conciliacao/pendentes-multiplos/', fn(Request $request, Response $response) => $this->FinancialController->ofxPendencyConciliationMultiple($request, $response));

    $app->post('/conciliacao/excluir/', fn(Request $request, Response $response) => $this->FinancialController->ofxPendencyRemove($request, $response));

    $app->get('/conciliacao/extrato/', fn(Request $request, Response $response) => $this->FinancialController->extract($request, $response));

    $app->get('/conciliacao/{status}/', fn(Request $request, Response $response) => $this->FinancialController->listByOfx($request, $response));

    $app->post('/ofx/', fn(Request $request, Response $response) => $this->FinancialController->ofx($request, $response));

    $app->put('/pagamentos/novo-status/{id}/', fn(Request $request, Response $response) => $this->FinancialController->setStatusCancel($request, $response));

    $app->post('/salva-conta-diretorio/', fn(Request $request, Response $response) => $this->FinancialController->SaveDirectoryAccount($request, $response));

    $app->get('/extrato/', fn(Request $request, Response $response) => $this->FinancialController->getExtract($request, $response));

    $app->post('/salva-extrato/', fn(Request $request, Response $response) => $this->FinancialController->SaveDirectoryExtract($request, $response));

    $app->put('/desativa-extrato/{extract}/', fn(Request $request, Response $response) => $this->FinancialController->DeleteExtract($request, $response));

    $app->post('/edita-extrato/', fn(Request $request, Response $response) => $this->FinancialController->editDirectoryExtract($request, $response));

});
