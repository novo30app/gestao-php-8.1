<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/area-diretorio', function () use ($app) {

    $app->get('/loja/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->store($request, $response));

    $app->get('/sistemas/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->systems($request, $response));

    $app->get('/relatorio-financeiro/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->financial($request, $response));

    $app->get('/reunioes/[{sector}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->meetings($request, $response));

    $app->get('/trilha/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->trail($request, $response));

    $app->get('/financeiro-novo/[{details}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->registerFinancial($request, $response));

    $app->get('/video/{id}/[{sector}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->playVideo($request, $response));

    $app->get('/comunicado/[{window}/]', fn(Request $request, Response $response) => $this->DirectoriesAreaController->index($request, $response));

    $app->get('/trilha-do-conhecimento/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->trailOfKnowledge($request, $response));

    $app->get('/documentos-locais/', fn(Request $request, Response $response) => $this->DirectoriesAreaController->localDocuments($request, $response));

});