<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/cron', function () use ($app) {
    $app->get('/evolucao/', fn(Request $request, Response $response) => $this->CronCrontoller->evolution($request, $response));
    $app->get('/banco-do-brasil/extrato/', fn(Request $request, Response $response) => $this->CronCrontoller->bancoDoBrasilExtrato($request, $response));
    $app->get('/rede/vendas/', fn(Request $request, Response $response) => $this->CronCrontoller->rede());
    $app->get('/rede/refresh-token/', fn(Request $request, Response $response) => $this->CronCrontoller->redeRefreshAccessToken());
    $app->get('/iugu/vendas/', fn(Request $request, Response $response) => $this->CronCrontoller->conciliationIugu());
    $app->get('/rede/recebiveis/', fn(Request $request, Response $response) => $this->CronCrontoller->getReceivables());
    $app->get('/contratos/atualizar-status/', fn(Request $request, Response $response) => $this->CronCrontoller->changeStatusContract($request, $response));
    $app->get('/transacoes-ausentes/', fn(Request $request, Response $response) => $this->CronCrontoller->verifyJourney2024($request, $response));
    $app->get('/loja/consulta-vendas/', fn(Request $request, Response $response) => $this->CronCrontoller->salesConsultation($request, $response));
    $app->get('/loja/consulta-vendas-detalhes/', fn(Request $request, Response $response) => $this->CronCrontoller->salesDetails($request, $response));
    $app->get('/loja/cria-pedido/', fn(Request $request, Response $response) => $this->CronCrontoller->salesCreateOrder($request, $response));
    $app->get('/chatbot/', fn(Request $request, Response $response) => $this->CronCrontoller->chatbot($request, $response));
    $app->get('/consulta-pagamentos/', fn(Request $request, Response $response) => $this->CronCrontoller->consultPayments($request, $response));
});