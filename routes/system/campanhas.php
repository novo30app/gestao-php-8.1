<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/campanhas', function () use ($app) {

    $app->get('/', function (Request $request, Response $response) {
        return $this->CampaignsController->index($request, $response);
    });

    $app->get('/listagem/', function (Request $request, Response $response) {
        return $this->CampaignsController->list($request, $response);
    });

    $app->get('/filiacoes/', function (Request $request, Response $response) {
        return $this->CampaignsController->affiliations($request, $response);
    });

    $app->get('/listagem-filiacoes/', function (Request $request, Response $response) {
        return $this->CampaignsController->affiliationsList($request, $response);
    });

    $app->put('/muda-status/{id}/', function (Request $request, Response $response) {
        return $this->CampaignsController->changeStatus($request, $response);
    });

    $app->post('/salva/', function (Request $request, Response $response) {
        return $this->CampaignsController->save($request, $response);
    });

    $app->get('/campanhas-filiacao/', function (Request $request, Response $response) {
        return $this->CampaignsController->membershipCampaigns($request, $response);
    });

    $app->get('/campanhas-filiacao-listagem/', function (Request $request, Response $response) {
        return $this->CampaignsController->membershipCampaignsList($request, $response);
    });

    $app->get('/campanhas-filiacao-listagem-exportacao/', function (Request $request, Response $response) {
        return $this->CampaignsController->membershipCampaignsListExport($request, $response);
    });

    $app->get('/campanhas-filiacao-resultados/', function (Request $request, Response $response) {
        return $this->CampaignsController->membershipCampaignsResults($request, $response);
    });

    $app->get('/campanhas-filiacao-resultados-listagem/', function (Request $request, Response $response) {
        return $this->CampaignsController->membershipCampaignsResultsList($request, $response);
    });

    $app->get('/campanhas-filiacao-resultados-exportacao/', function (Request $request, Response $response) {
        return $this->CampaignsController->membershipCampaignsResultsExport($request, $response);
    });
});

