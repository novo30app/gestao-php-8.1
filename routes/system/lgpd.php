<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/lgpd', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->LgpdController->index($request, $response));

    $app->get('/news/', fn(Request $request, Response $response) => $this->LgpdController->getNews($request, $response));

    $app->put('/atualiza-news/{id}/', fn(Request $request, Response $response) => $this->LgpdController->refreshStatus($request, $response));

    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->LgpdController->register($request, $response));

});