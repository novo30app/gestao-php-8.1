<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/contabilidade', function () use ($app) {

    $app->get('/pagamento/{id}/', fn (Request $request, Response $response) => $this->AccountingController->index($request, $response, 2));

    $app->get('/exporta-relatorio-contabil/', fn(Request $request, Response $response) => $this->AccountingController->exportAccountReport($request, $response));

    $app->get('/exporta-relatorio-fiscal/', fn(Request $request, Response $response) => $this->AccountingController->exportFiscalReport($request, $response));

    $app->get('/pega-contas/{payment}/', fn(Request $request, Response $response) => $this->AccountingController->getAccounts($request, $response));

    $app->post('/registro-contabil/', fn(Request $request, Response $response) => $this->AccountingController->accountingRegister($request, $response));

    $app->put('/apaga-registro-contabil/{id}/', fn(Request $request, Response $response) => $this->AccountingController->deleteRegister($request, $response));

});