<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/indicacoes', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->IndicationsController->index($request, $response));
    
    $app->get('/exato-digital/', fn(Request $request, Response $response) => $this->IndicationsController->exatoDigital());
    
    $app->get('/responder/{id}/{hash}/', fn(Request $request, Response $response) => $this->IndicationsController->respond($request, $response));
    
    $app->post('/responder/', fn(Request $request, Response $response) => $this->IndicationsController->respondSave($request, $response));

    $app->get('/indica-nucleo/', fn(Request $request, Response $response) => $this->IndicationsController->indicatesCore($request, $response));

    $app->get('/indica-diretorio/', fn(Request $request, Response $response) => $this->IndicationsController->indicatesDirectory($request, $response));

    $app->get('/lista-membros/', fn(Request $request, Response $response) => $this->IndicationsController->listMembers($request, $response));

    $app->get('/pega-dados-dirigente/{cpf}/', fn(Request $request, Response $response) => $this->IndicationsController->getManagerData($request, $response));

    $app->get('/listagem/', fn(Request $request, Response $response) => $this->IndicationsController->indicationsList($request, $response));

    $app->get('/listagem-exportacao/', fn(Request $request, Response $response) => $this->IndicationsController->indicationsListExport($request, $response));

    $app->get('/listagem-membros/', fn(Request $request, Response $response) => $this->IndicationsController->indicationsMembersList($request, $response));

    $app->get('/listagem-membros-exportacao/', fn(Request $request, Response $response) => $this->IndicationsController->indicationsMembersListExport($request, $response));

    $app->get('/visualiza/{id}/', fn(Request $request, Response $response) => $this->IndicationsController->getIndication($request, $response));
    
	$app->get('/listar-dados/{id}/{active}/', fn(Request $request, Response $response) => $this->IndicationsController->getMembers($request, $response));
	
    $app->get('/reenviar-emails/{id}/', fn(Request $request, Response $response) => $this->IndicationsController->resendEmail($request, $response));

    $app->get('/reenviar-emails-termo/{id}/', fn(Request $request, Response $response) => $this->IndicationsController->resendEmailOfAcceptance($request, $response));

    $app->put('/remove-indicacao/{id}/', fn(Request $request, Response $response) => $this->IndicationsController->removeIndication($request, $response));

    $app->put('/altera-encerramento-indicacao/{id}/{status}/', fn(Request $request, Response $response) => $this->IndicationsController->changeIndication($request, $response));

    $app->put('/remove-membro/{id}/', fn(Request $request, Response $response) => $this->IndicationsController->removeMember($request, $response));

    $app->post('/adiciona-indicacao/', fn(Request $request, Response $response) => $this->IndicationsController->saveIndication($request, $response));

    $app->post('/adiciona-mensagem/', fn(Request $request, Response $response) => $this->IndicationsController->saveMessages($request, $response));

    $app->post('/aprovacao/{level}/{member}/{value}/', fn(Request $request, Response $response) => $this->IndicationsController->approval($request, $response));

    $app->put('/remove-documento/{id}/{type}/', fn(Request $request, Response $response) => $this->IndicationsController->removeDocument($request, $response));

    $app->get('/exportacao-membros-desativados/', fn(Request $request, Response $response) => $this->IndicationsController->exportMembersDisabled($request, $response));

    $app->put('/analise-ata-indicacao-novos-membros/{id}/{value}/{rejectReason}/', fn(Request $request, Response $response) => $this->IndicationsController->analysisAtaIndicationNewMembers($request, $response));

});