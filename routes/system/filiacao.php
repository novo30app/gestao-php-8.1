<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/filiacao', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->AffiliationController->index($request, $response));

    $app->get('/listagem/', fn(Request $request, Response $response) => $this->AffiliationController->list($request, $response));

    $app->get('/nova-indicacao/', fn(Request $request, Response $response) => $this->AffiliationController->newIndication($request, $response));

    $app->get('/nova-indicacao/{cpm}/', fn(Request $request, Response $response) => $this->AffiliationController->newIndication($request, $response));

    $app->get('/consulta/{cpf}/', fn(Request $request, Response $response) => $this->AffiliationController->getData($request, $response));

    $app->put('/remove-indicacao/{id}/', fn(Request $request, Response $response) => $this->AffiliationController->removeIndication($request, $response));

    $app->post('/solicita/', fn(Request $request, Response $response) => $this->AffiliationController->SaveAffiliation($request, $response));

    $app->post('/reenvia/', fn(Request $request, Response $response) => $this->AffiliationController->resend($request, $response));

});