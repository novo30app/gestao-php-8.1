<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/cep', function () use ($app) {
    $app->get('/membros/', fn(Request $request, Response $response) => $this->CepController->members($request, $response));
});
