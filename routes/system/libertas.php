<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/libertas/cursos', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->LibertasCoursesController->index($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->LibertasCoursesController->list($request, $response));
    $app->get('/cadastro/[{id}/]', fn(Request $request, Response $response) => $this->LibertasCoursesController->register($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->LibertasCoursesController->save($request, $response));
});

$app->group('/libertas/auto-inscricao-moodle', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->index($request, $response));
    $app->get('/json/[{id}/]', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->list($request, $response));
    $app->put('/novo-status/{id}/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->changeActive($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->save($request, $response));
    $app->get('/{email}/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->isAutoEnrollment($request, $response));
    $app->post('/importacao/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->csvImport($request, $response));
});

$app->group('/libertas/cupons', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->LibertasCourseCouponController->index($request, $response));
    $app->get('/json/[{id}/]', fn(Request $request, Response $response) => $this->LibertasCourseCouponController->list($request, $response));
    $app->put('/novo-status/{id}/', fn(Request $request, Response $response) => $this->LibertasCourseCouponController->changeActive($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->LibertasCourseCouponController->save($request, $response));
});

$app->group('/libertas/redirecionamentos-moodle', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->moodleRedirect($request, $response));
    $app->get('/json/[{id}/]', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->moodleRedirectList($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->saveMoodleRedirectList($request, $response));
});

$app->get('/libertas/mandatario-legislativo/{email}/{cpf}/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->isMandatarioLegislativo($request, $response));
$app->get('/libertas/mandatario-executivo/{email}/{cpf}/', fn(Request $request, Response $response) => $this->LibertasAutoEnrollmentController->isMandatarioExecutivo($request, $response));