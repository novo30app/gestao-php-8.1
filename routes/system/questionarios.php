<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/questionarios', function () use ($app) {
	$app->get('/', fn(Request $request, Response $response) => $this->QuestionnairesController->questionnaires($request, $response));
	$app->get('/json/', fn(Request $request, Response $response) => $this->QuestionnairesController->questionnairesList($request, $response));
	$app->get('/cadastro/[{id}/]', fn(Request $request, Response $response) => $this->QuestionnairesController->questionnaireView($request, $response));
	$app->get('/enviar/{id}/', fn(Request $request, Response $response) => $this->QuestionnairesController->sendQuestionnaire($request, $response));
	$app->post('/enviar/', fn(Request $request, Response $response) => $this->QuestionnairesController->sendQuestionnaireSave($request, $response));
	$app->post('/responder/', fn(Request $request, Response $response) => $this->QuestionnairesController->replySave($request, $response));
    $app->get('/responder/{type}/{id}/{hash}/', fn(Request $request, Response $response) => $this->QuestionnairesController->reply($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->QuestionnairesController->questionnaireSave($request, $response));

});

$app->group('/questionarios/questoes', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->QuestionnairesController->questions($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->QuestionnairesController->questionsList($request, $response));
    $app->get('/cadastro/[{id}/]', fn(Request $request, Response $response) => $this->QuestionnairesController->questionView($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->QuestionnairesController->questionSave($request, $response));
});