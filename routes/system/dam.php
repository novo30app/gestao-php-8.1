<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/dam', function () use ($app) {
    $app->get('/tags/', fn(Request $request, Response $response) => $this->DamController->tagsIndex($request, $response));
    $app->get('/projetos/', fn(Request $request, Response $response) => $this->DamController->projectsIndex($request, $response));
    $app->get('/projeto/{id}/', fn(Request $request, Response $response) => $this->DamController->projectIndex($request, $response));
    $app->get('/gabinetes/', fn(Request $request, Response $response) => $this->DamController->cabinets($request, $response));
    $app->get('/gabinete/{id}/', fn(Request $request, Response $response) => $this->DamController->cabinetIndex($request, $response));
    $app->get('/assessores/', fn(Request $request, Response $response) => $this->DamController->users($request, $response));
    $app->get('/dossie/cadastro/[{id}/]', fn(Request $request, Response $response) => $this->DamController->dossie($request, $response));
    $app->get('/dossie/{id}/', fn(Request $request, Response $response) => $this->DamController->dossieIndex($request, $response));
    $app->get('/dossie/', fn(Request $request, Response $response) => $this->DamController->dossieIndex($request, $response));
    $app->get('/interacoes/', fn(Request $request, Response $response) => $this->DamController->interactionsIndex($request, $response));
    $app->get('/interacoes/reuniao/[{id}/]', fn(Request $request, Response $response) => $this->DamController->meetingIndex($request, $response));
});