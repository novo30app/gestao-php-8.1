<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/juventude', function () use ($app) {
    $app->get('/consulta/', fn(Request $request, Response $response) => $this->YouthController->searchIndex($request, $response));
    $app->get('/comentarios/{id}/', fn(Request $request, Response $response) => $this->YouthController->listComments($request, $response));
});