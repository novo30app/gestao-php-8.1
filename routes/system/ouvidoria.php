<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/ouvidoria', function () use ($app) {

    $app->get('/categorias/', fn(Request $request, Response $response) => $this->OmbudsmanController->category($request, $response));

    $app->get('/respostas-frequentes/', fn(Request $request, Response $response) => $this->OmbudsmanController->frequentAnswers($request, $response));

    $app->get('/solicitacoes/', fn(Request $request, Response $response) => $this->OmbudsmanController->requests($request, $response));

    $app->get('/solicitacoes/{id}/', fn(Request $request, Response $response) => $this->OmbudsmanController->viewRequests($request, $response));

    $app->post('/resposta/', fn(Request $request, Response $response) => $this->OmbudsmanController->saveResponse($request, $response));

});