<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/cadastros', function () use ($app) {

    $app->get('/usuarios/', fn(Request $request, Response $response) => $this->UserController->register($request, $response));

    $app->get('/novo-usuario/', fn(Request $request, Response $response) => $this->UserController->newUser($request, $response));

    $app->get('/edita-usuario/{id}/', fn(Request $request, Response $response) => $this->UserController->newUser($request, $response));

    $app->get('/dirigentes/', fn(Request $request, Response $response) => $this->RegistrationController->leaders($request, $response));

    $app->get('/dirigentes/{directory}/', fn(Request $request, Response $response) => $this->SearchController->leaders($request, $response));

    $app->get('/diretorios/', fn(Request $request, Response $response) => $this->RegistrationController->directories($request, $response));

    $app->get('/gestao-cadastros/', fn (Request $request, Response $response) => $this->RegistrationController->managementRecords($request, $response));

    $app->get('/gestao-comunicados/', fn (Request $request, Response $response) => $this->RegistrationController->managementCommunicated($request, $response));

    $app->get('/setores/', fn (Request $request, Response $response) => $this->RegistrationController->sectors($request, $response));

    $app->get('/tipos-usuario/', fn (Request $request, Response $response) => $this->RegistrationController->usersType($request, $response));

    $app->get('/funcionalidades-sistema/', fn (Request $request, Response $response) => $this->RegistrationController->systemFeatures($request, $response));

    $app->get('/permissoes/', fn (Request $request, Response $response) => $this->RegistrationController->permissions($request, $response));

    $app->get('/permissoes-detalhes/', fn (Request $request, Response $response) => $this->RegistrationController->permissionsDetails($request, $response));

    $app->get('/permissoes-detalhes/{id}/', fn (Request $request, Response $response) => $this->RegistrationController->permissionsDetails($request, $response));

    $app->post('/pega-funcionalidades/', fn (Request $request, Response $response) => $this->UserController->getFeatureByUserSector($request, $response));

});

$app->group('/cadastros/fornecedores', function () use ($app) {
	
	$app->get('/', fn(Request $request, Response $response) => $this->RecordsController->supplier($request, $response));
	
	$app->get('/listar/', fn(Request $request, Response $response) => $this->RecordsController->list($request, $response));
	
    $app->get('/exportar/', fn(Request $request, Response $response) => $this->RecordsController->exportProviders($request, $response));

	$app->post('/registrar/', fn(Request $request, Response $response) => $this->RecordsController->save($request, $response));
	
	$app->get('/{id}/', fn(Request $request, Response $response) => $this->RecordsController->data($request, $response));
	
	$app->put('/novo-status/{id}/', fn (Request $request, Response $response) => $this->RecordsController->changeActive($request, $response, true));
	
	$app->delete('/novo-status/{id}/', fn (Request $request, Response $response) => $this->RecordsController->changeActive($request, $response, false));

});
