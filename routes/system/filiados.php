<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/filiados', function () use ($app) {

    $app->get('/', function (Request $request, Response $response) {
        return $this->AffiliatedController->index($request, $response);
    });

    $app->get('/tse/', function (Request $request, Response $response) {
        return $this->AffiliatedController->tseFile($request, $response);
    });

    $app->get('/submissao/', function (Request $request, Response $response) {
        return $this->AffiliatedController->submission($request, $response);
    });

    $app->get('/csv/{affiliation}/{disaffiliation}/', function (Request $request, Response $response) {
        return $this->AffiliatedController->csv($request, $response);
    });

    $app->get('/blacklist/', function (Request $request, Response $response) {
        return $this->AffiliatedController->blackList($request, $response);
    });

    $app->get('/impugnacao/', function (Request $request, Response $response) {
        return $this->AffiliatedController->impeachment($request, $response);
    });

    $app->get('/como-conheceu/', function (Request $request, Response $response) {
        return $this->AffiliatedController->knowledge($request, $response);
    });

    $app->get('/quem-indicou/', function (Request $request, Response $response) {
        return $this->AffiliatedController->whoIndicated($request, $response);
    });

    $app->get('/ultimas-solicitacoes/', function (Request $request, Response $response) {
        return $this->AffiliatedController->latestAffiliations($request, $response);
    });

    $app->get('/ultimas-filiacoes/', function (Request $request, Response $response) {
        return $this->AffiliatedController->latestAffiliations($request, $response, 1);
    });

    $app->get('/agendamento/', function (Request $request, Response $response) {
        return $this->AppointmentController->appointment($request, $response);
    });

    $app->get('/consulta/', function (Request $request, Response $response) {
        return $this->AffiliatedController->quiz($request, $response);
    });

    $app->get('/consulta/{id}/', function (Request $request, Response $response) {
        return $this->AffiliatedController->showQuiz($request, $response);
    });

    $app->get('/editar/{id}/', function (Request $request, Response $response) {
        return $this->AffiliatedController->edit($request, $response);
    });

    $app->get('/mensagens-agendadas/', function (Request $request, Response $response) {
        return $this->AffiliatedController->scheduleMessages($request, $response);
    });

    $app->get('/agendamentos/', function (Request $request, Response $response) {
        return $this->AffiliatedController->scheduleMessagesRegisters($request, $response);
    });

    $app->get('/{id}/', function (Request $request, Response $response) {
        return $this->AffiliatedController->view($request, $response);
    });

});

