<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/dashboard', function () use ($app) {

    $app->get('/gestao/', fn (Request $request, Response $response) => $this->DashboardController->management($request, $response));

    $app->get('/planejamento-eleitoral/', fn (Request $request, Response $response) => $this->DashboardController->electoralPlanning($request, $response));

});