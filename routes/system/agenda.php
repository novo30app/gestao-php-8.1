<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/agenda', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->ScheduleController->roomsIndex($request, $response));
});