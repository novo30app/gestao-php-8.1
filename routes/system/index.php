<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->get('/', fn(Request $request, Response $response) => $this->UserController->index($request, $response));
$app->get('/crud/{model}/{url}/[{folder}/]', fn(Request $request, Response $response) => $this->CrudController->index($request, $response));
