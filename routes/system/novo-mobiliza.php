<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/novo-mobiliza', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->NovoMobilizeController->index($request, $response));
    $app->post('/', fn(Request $request, Response $response) => $this->NovoMobilizeController->save($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->NovoMobilizeController->list($request, $response));
    $app->get('/{id}/', fn(Request $request, Response $response) => $this->NovoMobilizeController->show($request, $response));
    $app->put('/novo-status/{id}/', fn(Request $request, Response $response) => $this->NovoMobilizeController->changeActive($request, $response));
    $app->delete('/{id}/', fn(Request $request, Response $response) => $this->NovoMobilizeController->destroy($request, $response));
});
