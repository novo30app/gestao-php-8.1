<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/assinatura-eletronica', function () use ($app) {
    $app->get('/documentos/', fn(Request $request, Response $response) => $this->SignatureController->docs($request, $response));
    $app->get('/documentos/enviar/{id}/', fn(Request $request, Response $response) => $this->SignatureController->sendDoc($request, $response));
    $app->get('/documentos/importacao/{id}/', fn(Request $request, Response $response) => $this->SignatureController->import($request, $response));
    $app->get('/documentos/assinaturas/{id}/api/', fn(Request $request, Response $response) => $this->SignatureController->signaturesList($request, $response));
    $app->get('/documentos/assinaturas/{id}/', fn(Request $request, Response $response) => $this->SignatureController->signatures($request, $response));
    $app->get('/documentos/assinaturas/{id}/{hash}/', fn(Request $request, Response $response) => $this->SignatureController->signaturePdf($request, $response));
    $app->get('/documentos/assinar/{id}/{hash}/', fn(Request $request, Response $response) => $this->SignatureController->signature($request, $response));
    $app->get('/documentos/cadastro/[{id}/]', fn(Request $request, Response $response) => $this->SignatureController->register($request, $response));
    $app->post('/documentos/', fn(Request $request, Response $response) => $this->SignatureController->saveDoc($request, $response));
    $app->post('/documentos/assinar/', fn(Request $request, Response $response) => $this->SignatureController->saveSignature($request, $response));
    $app->post('/documentos/enviar/', fn(Request $request, Response $response) => $this->SignatureController->saveDocSigned($request, $response));
    $app->post('/documentos/segunda-via/', fn(Request $request, Response $response) => $this->SignatureController->duplicate($request, $response));
    $app->post('/documentos/importacao/', fn(Request $request, Response $response) => $this->SignatureController->saveDocSignedImportation($request, $response));
});

$app->get('/api/assinatura-eletronica/documentos/', fn(Request $request, Response $response) => $this->SignatureController->list($request, $response));
