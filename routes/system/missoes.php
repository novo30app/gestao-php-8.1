<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/missoes', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->MissionController->index($request, $response));
    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->MissionController->save($request, $response));
    $app->post('/set-missao-da-semana/', fn(Request $request, Response $response) => $this->MissionController->updateWeekStatus($request, $response));
    $app->get('/json/', fn(Request $request, Response $response) => $this->MissionController->list($request, $response));
    $app->put('/novo-status/{id}/', fn(Request $request, Response $response) => $this->MissionController->changeActive($request, $response));
    $app->get('/{id}/', fn(Request $request, Response $response) => $this->MissionController->show($request, $response));
    $app->delete('/novo-status/{id}/', fn(Request $request, Response $response) => $this->MissionController->destroy($request, $response));
});
