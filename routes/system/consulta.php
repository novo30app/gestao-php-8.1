<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/consulta', function () use ($app) {
    $app->get('/candidatos/', fn(Request $request, Response $response) => $this->SearchController->candidates($request, $response));
    $app->get('/dirigentes/', fn(Request $request, Response $response) => $this->SearchController->leaders($request, $response));
    $app->get('/convencoes/', fn(Request $request, Response $response) => $this->SearchController->conventions($request, $response));
    $app->get('/acessos/', fn(Request $request, Response $response) => $this->SearchController->access($request, $response));
    $app->get('/desfiliacoes/', fn(Request $request, Response $response) => $this->SearchController->disaffiliation($request, $response));
    $app->get('/doadores/', fn(Request $request, Response $response) => $this->SearchController->donors($request, $response));
    $app->get('/novo-cadastro/', fn(Request $request, Response $response) => $this->RegistrationController->newRegister($request, $response));
    $app->get('/planejamento-estrategico-municipal-2022/', fn(Request $request, Response $response) => $this->SearchController->municipalStrategicPlanning2022($request, $response));
    $app->get('/planejamento-estrategico-municipal-2022/list/', fn(Request $request, Response $response) => $this->SearchController->municipalStrategicPlanning2022List($request, $response));
    $app->get('/planejamento-estrategico-municipal-2022/{id}/', fn(Request $request, Response $response) => $this->SearchController->municipalStrategicPlanning2022View($request, $response));
    $app->get('/mesorregioes/', fn(Request $request, Response $response) => $this->SearchController->Mesoregions($request, $response));
    $app->get('/eleitos/', fn(Request $request, Response $response) => $this->RegistrationController->elected($request, $response));
    $app->get('/refiliacao/', fn(Request $request, Response $response) => $this->AffiliatedController->reaffiliation($request, $response));
});
